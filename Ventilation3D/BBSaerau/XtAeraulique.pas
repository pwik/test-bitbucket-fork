Unit XtAeraulique;

Interface

Uses
  Windows, BBS_Message, SysUtils, Variants, Classes, Graphics, Controls, Forms, ActnList,
  Dialogs, Menus, ExtCtrls, ComCtrls, ToolWin, ImgList,
  Buttons, Grids, BaseGrid, advObj, AdvGrid,
  BBScad_Interface_Aeraulique,

  Ventil_Types, Ventil_Etude, Ventil_Const, Ventil_Debug, VirtualTrees, AdvToolBar, 
  AdvToolBarStylers, AdvMenus, AdvMenuStylers;

Type
  TXtAerauliqueMainForm = class(TForm)
    ImageList1: TImageList;
    Panel_Fond: TPanel;
    StatusBar1: TStatusBar;
    OpenDialog1: TOpenDialog;
    SaveDialog1: TSaveDialog;
    Panel_CAD: TPanel;
    Panel_VMC: TPanel;
    Panel_Resize: TPanel;
    Button_AllCad: TSpeedButton;
    Panel_TitreRes: TPanel;
    Panel_Resultats: TPanel;
    SpeedButton2: TSpeedButton;
    Panel2: TPanel;
    Panel_TitreBatiment: TPanel;
    Panel_Batiment: TPanel;
    SpeedButton4: TSpeedButton;
    SpeedButton1: TSpeedButton;
    ImageList2: TImageList;
    Grid_ResultatsReseau: TAdvStringGrid;
    PanelReseau: TPanel;
    Grid_Properties: TAdvStringGrid;
    Grid_ResBat: TAdvStringGrid;
    ImageList3: TImageList;
    Panel3: TPanel;
    ArbreBatiment: TVirtualStringTree;
    ToolBar2: TToolBar;
    ToolBtn_SuppLgt: TToolButton;
    ToolBtn_AddLgt: TToolButton;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Grid_ResultatsTroncon: TAdvStringGrid;
    Quitter1: TMenuItem;
    Enregistrer1: TMenuItem;
    Ouvrir1: TMenuItem;
    AdvToolBarOfficeStyler1: TAdvToolBarOfficeStyler;
    AdvDockPanel1: TAdvDockPanel;
    AdvToolBar1: TAdvToolBar;
    Menu_Enregistrersous: TMenuItem;
    Ajouterunlogement1: TMenuItem;
    Supprimerlelogementslectionn1: TMenuItem;
    Rsultats1: TMenuItem;
    Schmas1: TMenuItem;
    Img_Menus: TImageList;
    Btn_Quitter: TAdvToolBarButton;
    Btn_NouvEtude: TAdvToolBarButton;
    AdvToolBarSeparator1: TAdvToolBarSeparator;
    Btn_OuvrirEtude: TAdvToolBarButton;
    Btn_SauverEtude: TAdvToolBarButton;
    MainMenu1: TMainMenu;
    AdvToolBar2: TAdvToolBar;
    AdvToolBarButton_AddLgt: TAdvToolBarButton;
    AdvToolBarButton_SuppLgt: TAdvToolBarButton;
    AdvToolBarSeparator2: TAdvToolBarSeparator;
    Btn_Recalcul: TAdvToolBarButton;
    Btn_Arbre: TAdvToolBarButton;
    ToolBtn_Chiffrage: TAdvToolBarButton;
    N2: TMenuItem;
    Recalculer1: TMenuItem;
    Calculerlematriel1: TMenuItem;
    Arbredecalcul1: TMenuItem;
    Btn_Print: TAdvToolBarButton;
    ToolBtn_Verrou: TAdvToolBarButton;
    AdvToolBarSeparator3: TAdvToolBarSeparator;
    Donnesgnrales1: TMenuItem;
    AdvBtn_Print: TAdvToolBarButton;
    Vrifierlesmisesjour1: TMenuItem;
    Nouvelle12: TMenuItem;
    ToolBtn_DuplicateLgt: TToolButton;
    Grid_Batiment: TAdvStringGrid;
    AdvToolBarButton_DuplicateLgt: TAdvToolBarButton;
    Dupliquerlelogementslection1: TMenuItem;
    Grid_Acoust: TAdvStringGrid;
    TimerAutoSave: TTimer;
    Supprimer1: TMenuItem;
    Aide1: TMenuItem;
    AdvToolBarButton5: TAdvToolBarButton;
    AdvToolBarSeparator4: TAdvToolBarSeparator;
    TabSheet3: TTabSheet;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ToolBarButtonClick(Sender: TObject);
    procedure Button_AllCadClick(Sender: TObject);
    procedure Panel_TitreResClick(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure Panel_CADResize(Sender: TObject);
    procedure Grid_BatimentGetEditorProp(Sender: TObject; ACol, ARow: Integer; AEditLink: TEditLink);    
    procedure Grid_BatimentGetEditorType(Sender: TObject; ACol, ARow: Integer; var AEditor: TEditorType);
    procedure Grid_BatimentCellValidate(Sender: TObject; ACol, ARow: Integer; var Value: String; var Valid: Boolean);
    procedure Grid_BatimentComboCloseUp(Sender: TObject; ARow, ACol: Integer);
    procedure ArbreBatimentChange(Sender: TBaseVirtualTree; Node: PVirtualNode);
    procedure ArbreBatimentGetText(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex; TextType: TVSTTextType; var CellText: String);
    procedure ArbreBatimentGetImageIndex(Sender: TBaseVirtualTree; Node: PVirtualNode; Kind: TVTImageKind; Column: TColumnIndex; var Ghosted: Boolean; var ImageIndex: Integer);
    procedure Panel_VMCResize(Sender: TObject);
    procedure Grid_BatimentHasComboBox(Sender: TObject; ACol, ARow: Integer; var HasComboBox: Boolean);
    procedure Grid_ResultatsReseauCanClickCell(Sender: TObject; ARow, ACol: Integer; var Allow: Boolean);
    procedure Grid_ResultatsTronconSelectCell(Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
    procedure Grid_ResultatsTronconClickCell(Sender: TObject; ARow, ACol: Integer);
    Procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure Grid_PropertiesEllipsClick(Sender: TObject; ACol, ARow: Integer; var S: String);
    procedure Grid_BatimentEllipsClick(Sender: TObject; ACol, ARow: Integer; var S: String);
    procedure TimerAutoSaveTimer(Sender: TObject);
  Private

    RepEtudes   : String;
    AutoriseLoad: Boolean;
    Launcher    : TForm;
    IdReseau: String;
    RefReseau: String;
    Function Sortie: Boolean;
    Function  PropositionSauvegarde : Integer;
    Function  AOuvrir : Boolean;
    Function  IdASupprimer: String;
    Procedure Ouvrir(_Nom: String = '');
    Procedure Nouveau;
    Procedure Sauvegarder;
    Procedure SauvegarderSous;
    Procedure Supprimer;
    Procedure ViloMontre(_IdReseau: String);
    Procedure Imprimer(Sender: TObject);
    Procedure Editer;
    Procedure LockUnlock;
    Procedure AddLogement;
    Procedure SuppLogement;
    Procedure DuplicateLogement;
    Function  ArbreBatimentNode(_Node: PVirtualNode): TArbreBatimentNode;
    Function  ArbreBatimentNodeData(_Node: PVirtualNode): TVentil_Objet;
    Procedure ClearGrid(_Grid: TAdvStringGrid);
    Procedure InitGridResSysteme;
    Function  GridProp(_GridDep: TObject): TAdvStringGrid;
    Function  GridRes(_GridDep: TObject): TAdvStringGrid;
    Procedure InfosReseauPremierPlan;
    Function  InitialDir: String;
    Procedure MergeCadMenu;
    Procedure AddSubMenu(Const _ItemSource: TMenuItem;Var _ItemDest: TMenuItem);
    Procedure AddAutoMenuImage(Const _Action: TAction; Var _MenuItem: TMenuItem);
    Procedure SetAutoMenuItemVisible(Var _MenuItem: TMenuItem);
    Function DemarrageAeraulique: Boolean;
    Procedure SortieAeraulique;
    Procedure Load_GammesChiffrage;
  Public
    Procedure HighLightButtons;
    Procedure ClearAffichage;
  End;

Var
  XtAerauliqueMainForm : TXtAerauliqueMainForm;
Function LaunchAerauliqueAutonome(_Launcher: TForm) : Boolean;

Implementation
Uses
  BBS_Progress,
  XtAeraulique_ListChoix,
  BBsBdd,
  Ventil_Edibatec, Ventil_EdibatecProduits, Ventil_Utils, Ventil_Options, Ventil_Register, Ventil_resultatsAccidents, Ventil_Impressions, Ventil_ChoixImpressions,
  Ventil_Troncon, Ventil_Caisson, Ventil_Batiment, Ventil_Logement, Ventil_Firebird, Ventil_SystemeVMC,
  Ventil_Declarations, Ventil_ConsultationSystemes, Ventil_ParametresGeneraux, Ventil_ChiffrageTypes, Ventil_FonctionnementReseau, Ventil_Chiffrage,
  Ventil_NavigateurWeb,

  Ventil_CADCallBack,

  XtAeraulique_ChoixTypeEtude;

{$R *.dfm}
{ ******************************************************************* TXtAerauliqueMainForm ******************************************************************* }
Function TXtAerauliqueMainForm.Sortie: Boolean;
Begin
  Result := True;
  Case PropositionSauvegarde Of
    IdCancel: Begin
      Result := False;
      Exit;
    End;
    IdNo: ;
    IdYes: Sauvegarder;
  End;
   SortieAeraulique;
  If IsNotNil(Etude) Then Begin
  try
   FreeAndNil(Etude);
  except
  end;
   FreeAndNil(SystemesVMC);
   FreeAndNil(FabricantsVMC);
  End;
  Options.SaveOptions;
  FreeAndNil(Options);
  Launcher.Show;
End;
{ ************************************************************************************************************************************************* }
Procedure TXtAerauliqueMainForm.HighLightButtons;
Begin
  Enregistrer1.Enabled := IsNotNil(Etude);
  Menu_Enregistrersous.Enabled := IsNotNil(Etude);
  Btn_SauverEtude.Enabled := IsNotNil(Etude);
  Arbredecalcul1.Enabled := IsNotNil(Etude) And IsNil(Dlg_Chiffrage);
  Btn_Arbre.Enabled := Arbredecalcul1.Enabled;
  Recalculer1.Enabled := IsNotNil(Etude) And IsNil(Dlg_Chiffrage);
  Calculerlematriel1.Enabled := IsNotNil(Etude) And IsNil(Dlg_Chiffrage);
{$IfDef BIM_METRE}
  ToolBtn_Chiffrage.Visible := True;
{$Else}
  ToolBtn_Chiffrage.Visible := False;
{$EndIf}
  ToolBtn_Chiffrage.Enabled := IsNotNil(Etude) And IsNil(Dlg_Chiffrage);
//  Ajouterunlogement1.Enabled := IsNotNil(Etude) And (Etude.Batiment.TypeBat = c_BatimentCollectif) And IsNil(Dlg_Chiffrage);
  Ajouterunlogement1.Enabled := IsNotNil(Etude) And IsNil(Dlg_Chiffrage);
  Supprimerlelogementslectionn1.Enabled := IsNotNil(ArbreBatiment) And IsNotNil(ArbreBatimentNodeData(ArbreBatiment.GetFirstSelected)) And
                                           ArbreBatimentNodeData(ArbreBatiment.GetFirstSelected).InheritsFrom(TVentil_Logement) And IsNil(Dlg_Chiffrage);
  Dupliquerlelogementslection1.Enabled := Supprimerlelogementslectionn1.Enabled;
  ToolBtn_AddLgt.Enabled := Ajouterunlogement1.Enabled;
  AdvToolBarButton_AddLgt.Enabled := Ajouterunlogement1.Enabled;;
  ToolBtn_SuppLgt.Enabled:= Supprimerlelogementslectionn1.Enabled;
  AdvToolBarButton_SuppLgt.Enabled := Supprimerlelogementslectionn1.Enabled;
  ToolBtn_DuplicateLgt.Enabled := Dupliquerlelogementslection1.Enabled;
  AdvToolBarButton_DuplicateLgt.Enabled := Dupliquerlelogementslection1.Enabled;
  Rsultats1.Enabled      := IsNotNil(Etude) And IsNil(Dlg_Chiffrage);
  Schmas1.Enabled        := IsNotNil(Etude) And IsNil(Dlg_Chiffrage);
  Btn_Print.Enabled      := IsNotNil(Etude) And IsNil(Dlg_Chiffrage);
  Btn_Recalcul.Enabled   := Recalculer1.Enabled;
  ToolBtn_Verrou.Visible := False;
  ToolBtn_Verrou.Enabled := True;
  AdvBtn_Print.Enabled   := True;
//  ToolBtn_Verrou.Enabled := IsNotNil(Etude) And IsNotNil(Etude.Batiment) And IsNil(Dlg_Chiffrage);
//  AdvBtn_Print.Enabled   := IsNotNil(Etude) And IsNotNil(Etude.Batiment) And (Etude.Batiment.ChiffrageVerouille = c_OUI);
  Donnesgnrales1.Enabled := AdvBtn_Print.Enabled;
  If IsNotNil(Etude) And IsNotNil(Etude.Batiment) Then Begin
    If Etude.Batiment.ChiffrageVerouille  = c_OUI Then Begin
      ToolBtn_Verrou.ImageIndex := 10;
      ToolBtn_Verrou.Hint := 'D�verrouiller le calcul';
    End Else Begin
      ToolBtn_Verrou.ImageIndex := 11;
      ToolBtn_Verrou.Hint := 'Verrouiller le calcul';
    End;
  End;
End;
{ ************************************************************************************************************************************************* }
Procedure TXtAerauliqueMainForm.FormClose(Sender: TObject; var Action: TCloseAction);
Begin
  If Not Sortie Then action := caNone
  Else Begin
//   Deconnection_Etude;
   Deconnection_Logiciel;

  InterfaceCAD := Nil;
   Action := caFree;
  End;
End;
{ ************************************************************************************************************************************************* }
Procedure TXtAerauliqueMainForm.Panel_VMCResize(Sender: TObject);
Begin
  If Panel_Batiment.Visible Then
    XtAerauliqueMainForm.Panel_Batiment.Height := XtAerauliqueMainForm.Panel_TitreRes.Top - 2 * XtAerauliqueMainForm.Panel_TitreRes.Height;
End;
{ ************************************************************************************************************************************************* }
Procedure TXtAerauliqueMainForm.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
Var
  No_N : Word;
  No_S : Word;
  No_O : Word;
  No_P : Word;
Begin
  No_N := Ord('N');
  No_S := Ord('S');
  No_O := Ord('O');
  No_P := Ord('P');
  If (Key = No_N) And (ssCtrl In Shift) Then Nouveau
  Else If (Key = No_P) And IsNotNil(Etude) Then If ssCtrl In Shift Then ImpressionComplete
  Else If (Key = No_S) And IsNotNil(Etude) And (ssCtrl In Shift) Then Sauvegarder
  Else If (Key = No_O) And (ssCtrl In Shift) Then Ouvrir;

  If InterfaceCAD.IBBScad_Commun_Focused(false) Then InterfaceCAD.IBBScad_Events_KeyDown(Key, Shift);
End;
{ ************************************************************************************************************************************************* }
Procedure TXtAerauliqueMainForm.FormKeyPress(Sender: TObject; var Key: Char);
Begin
  If (Key = #13) And IsNil(Etude) Then Ouvrir

  Else If InterfaceCAD.IBBScad_Commun_Focused(false)  Then InterfaceCAD.IBBScad_Events_KeyPressed(Key);
End;
{ ************************************************************************************************************************************************* }
Procedure TXtAerauliqueMainForm.ToolBarButtonClick(Sender: TObject);
Begin
  If (Sender.InheritsFrom(TMenuItem)) Or Sender.InheritsFrom(TToolButton) Or Sender.InheritsFrom(TAdvMainMenu) Or Sender.InheritsFrom(TAdvToolBarButton) Then Begin
    Case TComponent(Sender).Tag Of
      TagButton_Quitter                 : Close;
      TagButton_NouveauEtude            : Nouveau;
      TagButton_OuvrirEtude             : Ouvrir;
      TagButton_SauverEtude             : Sauvegarder;
      TagButton_SupprimerEtude          : Supprimer;
      TagButton_CalcEtude               : Etude.Calculer;
      TagButton_AddLogement             : AddLogement;
      TagButton_ConsulSyst              : ConsultationDesSystemes;
      TagButton_TreeView                : ShowDebug;
      TagButton_SuppLogement            : SuppLogement;
      TagButton_EngtSous                : SauvegarderSous;
      TagButton_ImpressionLog,
      TagButton_ImpressionQMax,
      TagButton_ImpressionQMin,
      TagButton_ImpChiffrage,
      TagButton_ImpEtages,
      TagButton_ImpPLimites,
      TagButton_ImpPdcCaisson,
      TagButton_ImpressionCol,
      TagButton_ImpDataGen              : Imprimer(Sender);
      TagButton_Edition                 : Editer;
      TagButton_ImpComplete             : ImpressionComplete;
      TagButton_Catalogues              : ConsultationEdibatec;
      TagButton_Chiffrage               : Begin
                                                If Etude.Batiment.CodeFabricant = 'VIM' Then
{$IfDef BIM_METRE}
                                                        Chiffrage(Panel_Fond, TimerAutoSave, False)
{$Else}
                                                        Chiffrage(Panel_Fond, TimerAutoSave)
{$EndIf}
                                                Else
{$IfDef BIM_METRE}
                                                        Chiffrage(Panel_Fond, TimerAutoSave, False);
{$Else}
                                                        SelectionCaissonCourbe(TVentil_Caisson(Etude.Batiment.Reseaux[0]));
{$EndIf}
                                          End;
      TagButton_Courbe                  : AfficheFonctionnement(nil);
      806                               : AfficheFonctionnement(nil, True);
      TagButton_Options                 : ChangeOptions(TimerAutoSave);
      TagButton_LockUnlock              : LockUnlock;
      TagButton_DuplicateLogement       : DuplicateLogement;
      TagButton_Help                    : Aide(Help_CW5);

    End;
    If TComponent(Sender).Tag <> TagButton_Quitter Then HighLightButtons;
  End;

End;
{ ************************************************************************************************************************************************* }
Procedure TXtAerauliqueMainForm.Nouveau;
var
        IsTertiaire : boolean;
        GridParent  : TWinControl;
        m_Nom : String;
Begin

//Base_Etude := Base_Affaire;

  Case PropositionSauvegarde Of
    IdCancel: Exit;
    IdNo: ;
    IdYes: Etude.Sauvegarder;
  End;

  If IsNotNil(Etude) Then
        FreeAndNil(Etude);

  m_Nom := 'Nouveau r�seau';
  If InputQuery('Nom du r�seau', 'Saisissez le nom du r�seau', m_Nom) Then Begin
    RefReseau := m_Nom;

  Caption := 'Aeraulique - ' + RefReseau;
  Etude := TVentil_Etude.Create(Grid_Properties, Grid_ResultatsTroncon, Grid_ResultatsReseau, Grid_Acoust, TabSheet2, InfosReseauPremierPlan, nil, nil,
                                False);
  Etude.AutoSaveAutorisee := False;
  Etude.Reference := m_Nom;
     GridParent := XtAerauliqueMainForm.Grid_Batiment.Parent;
     AutoriseLoad := CreationEtude(XtAerauliqueMainForm.Grid_Batiment) = 0;
     XtAerauliqueMainForm.Grid_Batiment.Parent := GridParent;
     If Not AutoriseLoad Then FreeAndNil(Etude);

  ClearAffichage;
  If Etude = nil Then Exit;

  Etude.Fichier := RefReseau;

  IdReseau := GuidToString(Etude.Id);
  Etude.IdReseau := IdReseau;
  InterfaceCAD.IBBScad_Etude_Add(IdReseau);
  InterfaceCAD.IBBScad_Etude_Activate(IdReseau);

  ViloMontre(IdReseau);
  Etude.FillTree(ArbreBatiment);
  Etude.AutoSaveAutorisee := True;
  End;
End;
{
Procedure TXtAerauliqueMainForm.Nouveau;
var
        IsTertiaire : boolean;
        GridParent  : TWinControl;
Begin
  Case PropositionSauvegarde Of
    IdCancel: Exit;
    IdNo: ;
    IdYes: Etude.Sauvegarder;
  End;

  If IsNotNil(Etude) Then
        FreeAndNil(Etude);
  Etude := TVentil_Etude.Create(Grid_Properties, Grid_ResultatsTroncon, Grid_ResultatsReseau, Grid_Acoust, TabSheet2, InfosReseauPremierPlan, nil, nil,
                                False);
  Etude.AutoSaveAutorisee := False;

     GridParent := XtAerauliqueMainForm.Grid_Batiment.Parent;
     AutoriseLoad := CreationEtude(XtAerauliqueMainForm.Grid_Batiment) = 0;
     XtAerauliqueMainForm.Grid_Batiment.Parent := GridParent;
     If Not AutoriseLoad Then FreeAndNil(Etude);

  ClearAffichage;
  If Etude = nil Then Exit;
  Caption := 'Aeraulique - Fichier non sauvegard�';
  // if ETude.Batiment.TypeBat = c_BatimentCollectif then Etude.AddLogement;
  Aero_Vilo_Update.IVilo_Update_AddEtude(GuidToString(Etude.Id));
  Aero_Vilo_Update.IVilo_Update_SetEtude(GuidToString(Etude.Id));
  Etude.FillTree(ArbreBatiment);
  Etude.AutoSaveAutorisee := True;
End;
}
{ ************************************************************************************************************************************************* }
Function TXtAerauliqueMainForm.PropositionSauvegarde : Integer;
Begin
  If IsNil(Etude) Or (Not Etude.ToSave) Then Result := 1
  Else Result := BBS_ShowMessage(PChar('Souhaitez-vous enregistrer l''�tude en cours?'), '', MB_ICONQUESTION + MB_YESNOCANCEL + MB_DEFBUTTON2)
End;
{ ************************************************************************************************************************************************* }
Function TXtAerauliqueMainForm.AOuvrir: Boolean;
Var
  m_No      : Integer;
  m_IntList : TStringList;
  m_IntList2: TStringList;
  m_Ref     : String;
  m_Cpt     : Integer;
Begin
  m_IntList := TStringList.Create;
  m_IntList2:= TStringList.Create;
  Result := False;
  Choix_ListeXtApp.Grid_Choix.Clear;
  Table_Etude_Generale.Ouvrir;
  Table_Etude_Generale.Donnees.First;
  m_Cpt := 1;
  While Not Table_Etude_Generale.Donnees.Eof Do Begin
    m_Ref := Table_Etude_Generale.Donnees.FieldByName('REFERENCE').AsWideString;
    If m_Ref = '' Then Begin
      m_Ref := 'R�seau sans nom n� ' + IntToStr(m_Cpt);
      Inc(m_Cpt);
    End;
    Choix_ListeXtApp.AddElement(m_Ref);
    m_IntList.Add(Table_Etude_Generale.Donnees.FieldByName('IDRESEAU').AsWideString);
    m_IntList2.Add(m_Ref);
    Table_Etude_Generale.Donnees.Next;
  End;
  Choix_ListeXtApp.Grid_Choix.Row := 0;
  m_No := ChoixElementSurListe('R�seau � ouvrir');
  If m_No > - 1 Then Begin
    Result := True;
    RefReseau := m_IntList2[m_No];
    IdReseau  := m_IntList[m_No];
//    Etude.IdReseau := IdReseau;
//    SetCaption(RefReseau);
  End;
  Table_Etude_Generale.Fermer;
  FreeAndNil(m_IntList);
  FreeAndNil(m_IntList2);
End;
{ ************************************************************************************************************************************************* }
Procedure TXtAerauliqueMainForm.Ouvrir(_Nom: String = '');
Begin
Enabled := False;
        If AOuvrir Then
                Begin
                        If Etude <> nil Then
                                Begin
                                Etude.AutoSaveAutorisee := False;
                                        Case PropositionSauvegarde Of
                                                IdCancel: Begin
                                                          Enabled := True;
                                                          Exit;
                                                          End;
                                                IdNo: ;
                                                IdYes: Begin
                                                       Etude.Sauvegarder;
                                                       Etude.AutoSaveAutorisee := False;
                                                       End;
                                        End;
                                End;
                FreeAndNil(Etude);
                ClearAffichage;
                Repaint;
                Etude := TVentil_Etude.Create(Grid_Properties, Grid_ResultatsTroncon, Grid_ResultatsReseau, Grid_Acoust, TabSheet2, InfosReseauPremierPlan);
                Etude.AutoSaveAutorisee := False;
                Etude.Fichier := RefReseau;
                Etude.IdReseau := IdReseau;
//                Connection_Etude(Etude.Fichier);
                Etude.Ouvrir;
                ViloMontre(IdReseau);
                Etude.FillTree(ArbreBatiment);
//                Deconnection_Etude;
                Caption := 'Aeraulique - ' + ExtractFileName(Etude.Fichier);

                InterfaceCAD.IBBScad_Commun_Focused(true);
                End;
  Enabled := True;
        if Etude <> nil then
                Etude.AutoSaveAutorisee := True;
End;
{
Procedure TXtAerauliqueMainForm.Ouvrir(_Nom: String = '');
Var
  m_Nom : String;
Begin
  Enabled := False;
  Try
    m_Nom := '';
    OpenDialog1.InitialDir := InitialDir;
    If (_Nom = '') And OpenDialog1.Execute Then m_Nom := OpenDialog1.FileName
                                           Else m_Nom := _Nom;
    If m_Nom <> '' Then Begin
      If Etude <> nil Then
        Begin
        Etude.AutoSaveAutorisee := False;
        Case PropositionSauvegarde Of
         IdCancel: Begin
           Enabled := True;
           Exit;
         End;
         IdNo: ;
         IdYes: Begin
                Etude.Sauvegarder;
                Etude.AutoSaveAutorisee := False;
                End;
        End;
        End;
      FreeAndNil(Etude);
      ClearAffichage;
      Repaint;
      Etude := TVentil_Etude.Create(Grid_Properties, Grid_ResultatsTroncon, Grid_ResultatsReseau, Grid_Acoust, TabSheet2, InfosReseauPremierPlan);
      Etude.AutoSaveAutorisee := False;
      Etude.Fichier := m_Nom;
      Connection_Etude(Etude.Fichier);
      Etude.Ouvrir;
      Etude.FillTree(ArbreBatiment);
      Deconnection_Etude;
      Caption := 'Aeraulique - ' + ExtractFileName(Etude.Fichier);
      FormCAD.FocusSelf;
    End;
  Except
    on E:Exception Do Raise Exception.Create('Erreur � l''ouverture' +  E.message);
  End;
  Enabled := True;
        if Etude <> nil then
                Etude.AutoSaveAutorisee := True;
End;
}
{ ************************************************************************************************************************************************* }
Procedure TXtAerauliqueMainForm.AddLogement;
Begin
  Etude.AddLogement;
  Etude.FillTree(ArbreBatiment, Etude.Batiment.Logements.Last);
End;
{ ************************************************************************************************************************************************* }
Procedure TXtAerauliqueMainForm.DuplicateLogement;
Var
  m_Idx : Integer;
Begin
  If IsNotNil(ArbreBatimentNodeData(ArbreBatiment.GetFirstSelected)) And
     ArbreBatimentNodeData(ArbreBatiment.GetFirstSelected).InheritsFrom(TVentil_Logement) Then Begin
     m_Idx := Etude.Batiment.Logements.IndexOf(ArbreBatimentNodeData(ArbreBatiment.GetFirstSelected));
     Etude.AddLogement;
     TVentil_Logement(Etude.Batiment.Logements.Last).Paste(Etude.Batiment.Logements[m_Idx]);
     Etude.FillTree(ArbreBatiment, Etude.Batiment.Logements.Last);
  End;
End;
{ ************************************************************************************************************************************************* }
Procedure TXtAerauliqueMainForm.SuppLogement;
Var
  m_Idx : Integer;
Begin
  If IsNotNil(ArbreBatimentNodeData(ArbreBatiment.GetFirstSelected)) And
     ArbreBatimentNodeData(ArbreBatiment.GetFirstSelected).InheritsFrom(TVentil_Logement) Then Begin
     m_Idx := Etude.Batiment.Logements.IndexOf(ArbreBatimentNodeData(ArbreBatiment.GetFirstSelected));
     If Not Etude.Batiment.Logements[m_Idx].UtiliseCalcul Then Begin
       Etude.Batiment.Logements[m_Idx].Destroy;
       Etude.Batiment.Logements[m_Idx] := Nil;
       Etude.Batiment.Logements.Delete(m_Idx);
       Etude.FillTree(ArbreBatiment);
     End Else BBS_ShowMessage('Le logement "' + Etude.Batiment.Logements[m_Idx].Reference + '"' + #13 +
                          ' est utilis� dans les calculs, sa suppression est impossible', mtError, [mbOK], 0);
  End;
End;                       
{ ************************************************************************************************************************************************* }
Procedure TXtAerauliqueMainForm.SauvegarderSous;
Begin
  If IsNotNil(Etude) Then Begin
    Etude.Fichier := '';
    Sauvegarder;
  End;
End;
{ ************************************************************************************************************************************************* }
Procedure TXtAerauliqueMainForm.ClearAffichage;
Var
  m_NoComp : Integer;
Begin
  For m_NoComp := 0 To ComponentCount - 1 Do If Components[m_NoComp] Is TAdvStringGrid Then Begin
    TAdvStringGrid(Components[m_NoComp]).Objects[0, 0] := Nil;
    ClearGrid(TAdvStringGrid(Components[m_NoComp]));
  End;
  ArbreBatiment.Clear;
  InitGridResSysteme;
End;
{ ************************************************************************************************************************************************* }
Procedure TXtAerauliqueMainForm.Imprimer(Sender: TObject);
Begin
  Case TComponent(Sender).Tag Of
    TagButton_ImpressionLog : Impression_Logements(False, Self);
    TagButton_ImpressionQMax: Impression_FonctionnnementSelonDebit(False, True, Self);
    TagButton_ImpressionQMin: Impression_FonctionnnementSelonDebit(False, False, Self);
    TagButton_ImpressionCol : Impression_SchemasColonnes(False, Self);
    TagButton_ImpEtages     : Impression_SchemasEtages(False, Self);
    TagButton_ImpPLimites   : Impression_PressionsLimitesPourBouches(False, Self);
    TagButton_ImpPdcCaisson : Impression_PdcCaissonBouches(False, Self);
    TagButton_ImpDataGen    : ImpressionComplete;// Impression_DonneesGenerales(False);
  End;
End;
{ ************************************************************************************************************************************************* }
Procedure TXtAerauliqueMainForm.Editer;
Begin
  If IsNil(Dlg_ParametresGeneraux) Then Application.CreateForm(TDlg_ParametresGeneraux, Dlg_ParametresGeneraux);
  Dlg_ParametresGeneraux.Visible := Not Dlg_ParametresGeneraux.Visible;
  If Dlg_ParametresGeneraux.Visible Then Etude.Affiche(Dlg_ParametresGeneraux.Grid_Properties);
End;
{ ************************************************************************************************************************************************* }
Procedure TXtAerauliqueMainForm.LockUnlock;
Begin
  If Etude.Batiment.ChiffrageVerouille = c_OUI Then Etude.Batiment.ChiffrageVerouille := c_NON
                                               Else Etude.Batiment.ChiffrageVerouille := c_OUI;
  If Etude.SelectedObject <> Nil Then Etude.SelectedObject.Affiche(Grid_Properties);
  HighLightButtons;
End;
{ ************************************************************************************************************************************************* }
Procedure TXtAerauliqueMainForm.Sauvegarder;
Begin

  If IsNotNil(Etude) Then
  Begin
    Try
    FormatSettings.DecimalSeparator := '.';
    Etude.AutoSaveAutorisee := False;
    Enabled := False;
    Repaint;
        If IdReseau <> '' Then
                Begin
{
                Table_Etude_Generale.Vider('ID_ETUDE='''+IdReseau+'''');
                Table_Etude_Generale.Donnees.Insert;
                Table_Etude_Generale.Donnees.FieldByName('ID_ETUDE').AsString := IdReseau;
                Table_Etude_Generale.Donnees.FieldByName('REFERENCE').AsString := RefReseau;
                Table_Etude_Generale.Donnees.Post;
                Table_Etude_Generale.Sauvegarder;
}
//                Connection_Etude(Etude.Fichier);
                Etude.Sauvegarder;
                Etude.AutoSaveAutorisee := False;
//                Deconnection_Etude;
{
                Table_Etude_Generale.Fermer;
}                
                
                Caption := 'Aeraulique - ' + ExtractFileName(Etude.Fichier);

                End;
    Except
    End;
  Enabled := True;
  Etude.AutoSaveAutorisee := True;
  End;

End;
{
Procedure TXtAerauliqueMainForm.Sauvegarder;
Begin
  If IsNotNil(Etude) Then Begin
    Try
    DecimalSeparator := '.';
      Etude.AutoSaveAutorisee := False;
      Enabled := False;
      //SaveDialog1.InitialDir := InitialDir;
      If Etude.Fichier = '' Then
        If SaveDialog1.Execute Then Etude.Fichier := SaveDialog1.FileName;
      Repaint;
      If Etude.Fichier <> '' Then Begin
        If Ventil_FichierSurDisque(Etude.Fichier) Then Ventil_RenameFile(Etude.Fichier, Etude.Fichier + '~2');
        Connection_Etude(Etude.Fichier);
        Etude.Sauvegarder;
        Etude.AutoSaveAutorisee := False;
        Deconnection_Etude;
        If Ventil_FichierSurDisque(Etude.Fichier + '~') then Ventil_DeleteFile(Etude.Fichier + '~');
        If Ventil_FichierSurDisque(Etude.Fichier + '~2') then Ventil_RenameFile(Etude.Fichier + '~2', Etude.Fichier + '~');
        Caption := 'Aeraulique - ' + ExtractFileName(Etude.Fichier);
      End;
    Except End;
    Enabled := True;
    Etude.AutoSaveAutorisee := True;
  End;
End;
}
{ ************************************************************************************************************************************************** }
Function TXtAerauliqueMainForm.IdASupprimer: String;
Var
  m_No      : Integer;
  m_IntList : TStringList;
  m_Ref     : String;
  m_Cpt     : Integer;
Begin
  m_IntList := TStringList.Create;
  Result := '';
  Choix_ListeXtApp.Grid_Choix.Clear;
  Table_Etude_Generale.Ouvrir;
  Table_Etude_Generale.Donnees.First;
  m_Cpt := 1;
  While Not Table_Etude_Generale.Donnees.Eof Do Begin
    m_Ref := Table_Etude_Generale.Donnees.FieldByName('REFERENCE').AsWideString;
    If m_Ref = '' Then Begin
      m_Ref := 'R�seau sans nom n� ' + IntToStr(m_Cpt);
      Inc(m_Cpt);
    End;
    Choix_ListeXtApp.AddElement(m_Ref);
    m_IntList.Add(Table_Etude_Generale.Donnees.FieldByName('IDRESEAU').AsWideString);
    Table_Etude_Generale.Donnees.Next;
  End;
  Choix_ListeXtApp.Grid_Choix.Row := 0;
  m_No := ChoixElementSurListe('R�seau � supprimer');
  If m_No > - 1 Then Result := m_IntList[m_No];
  Table_Etude_Generale.Fermer;
  FreeAndNil(m_IntList);
End;
{ ************************************************************************************************************************************************** }
Procedure TXtAerauliqueMainForm.Supprimer;
Var
  m_Id : String;
Begin
  m_Id := IdASupprimer;
  If m_Id <> '' Then Begin

  if m_Id = idReseau then
        Begin
        BBS_ShowMessage(PChar('Suppression de l''�tude actuellement ouverte impossible'), '', MB_ICONSTOP);
        Exit;
        End;

ViderLesTablesAvecCondition('IDRESEAU='''+m_Id+'''');
{
OuvrirLesTables;
SauverLesTables;

//    Base_Etude := Base_Affaire;
    Aero_Vilo2Fredo_Save(Base_Etude);

//        if Etude <> nil then
//                Etude.Sauvegarder;
FermerLesTables;
}
//        If m_Id = IdReseau Then

                InterfaceCAD.IBBScad_Etude_Delete(m_Id);

    IdReseau := '';
    RefReseau := '';                     
//    SetCaption'');
  End;
End;
{ ************************************************************************************************************************************************** }
Procedure TXtAerauliqueMainForm.ViloMontre(_IdReseau: String);
Begin

//todo regis
  InterfaceCAD.IBBScad_Divers_Montre;
End;
{ ************************************************************************************************************************************************* }
Procedure TXtAerauliqueMainForm.Button_AllCadClick(Sender: TObject);
Begin
  Panel_VMC.Visible := Not Panel_VMC.Visible;
  If Panel_VMC.Visible Then Button_AllCad.Caption := '>>'
                       Else Button_AllCad.Caption := '<<';
End;
{ ************************************************************************************************************************************************* }
Procedure TXtAerauliqueMainForm.Panel_TitreResClick(Sender: TObject);
Begin
  Panel_Resultats.Visible := not Panel_Resultats.Visible;
  If Panel_Resultats.Visible Then Panel_Resultats.Top := Panel_TitreRes.Top - 1;
  If Panel_Batiment.Visible  Then Panel_Batiment.Height := XtAerauliqueMainForm.Panel_TitreRes.Top - 2 * XtAerauliqueMainForm.Panel_TitreRes.Height;
End;
{ ************************************************************************************************************************************************* }
Procedure TXtAerauliqueMainForm.SpeedButton4Click(Sender: TObject);
Begin
  Panel_Batiment.Visible := Not Panel_Batiment.Visible;
  Grid_Properties.Visible:= Not Panel_Batiment.Visible;
  Panel_Batiment.Height  := Panel_TitreRes.Top - 2 * Panel_TitreRes.Height;
  Grid_ResultatsReseau.Visible := Not Panel_Batiment.Visible;
  Grid_ResBat.Visible    := Panel_Batiment.Visible;
  If Grid_ResBat.Visible Then Panel_TitreRes.Caption := 'R�sultats (B�timent / Logements)'
                         Else Panel_TitreRes.Caption := 'R�sultats (R�seau courant)';
End;
{ ************************************************************************************************************************************************* }
Procedure TXtAerauliqueMainForm.Panel_CADResize(Sender: TObject);
Begin
  If InterfaceCAD <> Nil Then InterfaceCAD.IBBScad_Events_Resize;
End;
{ ************************************************************************************************************************************************* }
Function TXtAerauliqueMainForm.GridProp(_GridDep: TObject): TAdvStringGrid;
Begin
  If TAdvStringGrid(_GridDep).Tag = TagGrid_Batiment Then Result := Grid_Batiment
                                                     Else Result := Grid_Properties;
End;
{ ************************************************************************************************************************************************* }
Function TXtAerauliqueMainForm.GridRes(_GridDep: TObject): TAdvStringGrid;
Begin
  If TAdvStringGrid(_GridDep).Tag = TagGrid_Batiment Then Result := Grid_ResBat
                                                     Else Result := Grid_ResultatsTroncon;
End;
{ ************************************************************************************************************************************************* }
Procedure TXtAerauliqueMainForm.InfosReseauPremierPlan;
Begin
  If Panel_VMC.Visible Then Begin
    If Panel_Batiment.Visible Then Begin
      Panel_Batiment.Hide;
      Grid_ResultatsReseau.Show;
      Grid_Properties.Show;
      Grid_ResBat.Hide;
      Panel_TitreRes.Caption := 'R�sultats (R�seau courant)';
    End;
  End;
End;
{ ************************************************************************************************************************************************* }
Function TXtAerauliqueMainForm.InitialDir: String;
Var
  m_Str : String;
Begin
  m_Str := ExtractFilePath(Application.ExeName);
  m_Str := m_Str + 'Etudes';
  If Not DirectoryExists(m_Str) Then CreateDir(m_Str);
  Result := m_Str;
End;
{ ************************************************************************************************************************************************** }
procedure TXtAerauliqueMainForm.Grid_BatimentGetEditorProp(Sender: TObject;
  ACol, ARow: Integer; AEditLink: TEditLink);
begin
try
  If IsNotNil(GridProp(Sender).Objects[0, 0]) Then TVentil_Objet(GridProp(Sender).Objects[0, 0]).PropEdition(GridProp(Sender), ARow);
except
end;
end;
{ ************************************************************************************************************************************************** }
Procedure TXtAerauliqueMainForm.Grid_BatimentGetEditorType(Sender: TObject; ACol, ARow: Integer; var AEditor: TEditorType);
Begin
  AEditor := EdNone;
  If IsNotNil(GridProp(Sender).Objects[0, 0]) Then AEditor := TVentil_Objet(GridProp(Sender).Objects[0, 0]).TypeEdition(GridProp(Sender), ARow);
End;
{ ************************************************************************************************************************************************* }
Procedure TXtAerauliqueMainForm.Grid_BatimentHasComboBox(Sender: TObject; ACol, ARow: Integer; var HasComboBox: Boolean);
Begin
  HasComboBox := False;
  If IsNotNil(GridProp(Sender).Objects[0, 0]) Then HasComboBox := TVentil_Objet(GridProp(Sender).Objects[0, 0]).HasComboBox(ARow);
End;
{ ************************************************************************************************************************************************* }
Procedure TXtAerauliqueMainForm.Grid_ResultatsReseauCanClickCell(Sender: TObject; ARow, ACol: Integer; var Allow: Boolean);
Begin
  If IsNotNil(Grid_ResultatsReseau.Objects[0,0]) And (Grid_ResultatsReseau.Objects[0,0] Is TVentil_Caisson) Then Begin
    If ARow = 9 Then Case ACol Of
      2: If IsNotNil(TVentil_Caisson(Grid_ResultatsReseau.Objects[0,0]).Reseau_BouchePdcMaximum) Then
           TVentil_Caisson(Grid_ResultatsReseau.Objects[0,0]).Reseau_BouchePdcMaximum.LienCad.IAero_Vilo_Base_Select;
      3: If IsNotNil(TVentil_Caisson(Grid_ResultatsReseau.Objects[0,0]).Reseau_BouchePdcMinimum) Then
           TVentil_Caisson(Grid_ResultatsReseau.Objects[0,0]).Reseau_BouchePdcMinimum.LienCad.IAero_Vilo_Base_Select;
    End;
  End;
End;
{ ************************************************************************************************************************************************* }
Procedure TXtAerauliqueMainForm.Grid_ResultatsTronconClickCell(Sender: TObject; ARow, ACol: Integer);
Var                                                     
 m_No  : Integer;
Begin
  If (ARow <> 9) And (ACol <> 2) Then Exit;
  If IsNotNil(Grid_Properties.Objects[0,0]) And (TVentil_Troncon(Grid_Properties.Objects[0,0]).Accidents.Count > 1) Then Begin
    If Dlg_AffichAccidents.Grid_accidentsAssocies.Visible Then Begin
      Dlg_AffichAccidents.Grid_accidentsAssocies.Hide;
      Exit;
    End Else Begin
      Dlg_AffichAccidents.Grid_accidentsAssocies.Show;
      Dlg_AffichAccidents.Grid_accidentsAssocies.Top := Grid_ResultatsTroncon.Top + Grid_ResultatsTroncon.Height + 5; 
    End;
    If Dlg_AffichAccidents.Grid_accidentsAssocies.Visible Then Begin
       For m_No := 2 To Dlg_AffichAccidents.Grid_accidentsAssocies.RowCount - 1 Do Begin
        Dlg_AffichAccidents.Grid_accidentsAssocies.Cells[0, m_No] := '';
        Dlg_AffichAccidents.Grid_accidentsAssocies.Cells[1, m_No] := '';
      End;
      For m_No := 0 To TVentil_Troncon(Grid_Properties.Objects[0,0]).Accidents.Count - 1 Do
        TVentil_Troncon(Grid_Properties.Objects[0,0]).Accidents[m_No].AfficheResultats(Dlg_AffichAccidents.Grid_accidentsAssocies, 0, m_No + 2);
    End;
  End;
End;
{ ************************************************************************************************************************************************* }
Procedure TXtAerauliqueMainForm.Grid_ResultatsTronconSelectCell(Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
Begin
  CanSelect := False;
End;
{ ************************************************************************************************************************************************* }
Procedure TXtAerauliqueMainForm.Grid_BatimentCellValidate(Sender: TObject; ACol, ARow: Integer; var Value: String; var Valid: Boolean);
Begin
  If IsNotNil(GridProp(Sender).Objects[0, 0]) Then Begin
    TVentil_Objet(GridProp(Sender).Objects[0, 0]).UpdateFromGrid(GridProp(Sender), ARow);
    If GridProp(Sender).Cells[ACol, ARow] <> Value Then Value := GridProp(Sender).Cells[ACol, ARow];
    Etude.Calculer;
    TVentil_Objet(GridProp(Sender).Objects[0, 0]).AfficheResultats(GridRes(Sender));

GridProp(Sender).Row := 1;
GridProp(Sender).Col := 1;
TVentil_Objet(GridProp(Sender).Objects[0, 0]).TypeEdition(GridProp(Sender), GridProp(Sender).Row);
    
  End;
  HighLightButtons;
End;
{ ************************************************************************************************************************************************* }
Procedure TXtAerauliqueMainForm.Grid_PropertiesEllipsClick(Sender: TObject; ACol, ARow: Integer; var S: String);
Begin
  If IsNotNil(Grid_Properties.Objects[0, 0]) Then Begin
    S := TVentil_Objet(Grid_Properties.Objects[0, 0]).EllipsClick(Grid_Properties, ARow);
    Etude.Calculer;
    TVentil_Objet(GridProp(Sender).Objects[0, 0]).AfficheResultats(GridRes(Sender));

Grid_Properties.Row := 1;
Grid_Properties.Col := 1;
TVentil_Objet(Grid_Properties.Objects[0, 0]).TypeEdition(Grid_Properties, Grid_Properties.Row);

  End;
End;
{ ************************************************************************************************************************************************* }
Procedure TXtAerauliqueMainForm.Grid_BatimentEllipsClick(Sender: TObject; ACol, ARow: Integer; var S: String);
Begin
  If IsNotNil(Grid_Batiment.Objects[0, 0]) Then Begin
    S := TVentil_Objet(Grid_Batiment.Objects[0, 0]).EllipsClick(Grid_Batiment, ARow);
    TVentil_Objet(GridProp(Sender).Objects[0, 0]).AfficheResultats(GridRes(Sender));

Grid_Batiment.row := 1;
Grid_Batiment.col := 1;
TVentil_Objet(Grid_Batiment.Objects[0, 0]).TypeEdition(Grid_Batiment, Grid_Batiment.row);    
  End;
{
  If IsNotNil(Grid_Batiment.Objects[0, 0]) Then Begin
    S := TVentil_Objet(Grid_Batiment.Objects[0, 0]).EllipsClick(Grid_Batiment, ARow);
  End;
}
End;
{ ************************************************************************************************************************************************* }
Procedure TXtAerauliqueMainForm.Grid_BatimentComboCloseUp(Sender: TObject; ARow, ACol: Integer);
Begin
  If IsNotNil(GridProp(Sender).Objects[0, 0]) Then Begin
    TVentil_Objet(GridProp(Sender).Objects[0, 0]).UpdateFromGrid(GridProp(Sender), ARow);            
    Etude.Calculer;
    TVentil_Objet(GridProp(Sender).Objects[0, 0]).AfficheResultats(GridRes(Sender));

GridProp(Sender).Row := 1;
GridProp(Sender).Col := 1;
TVentil_Objet(GridProp(Sender).Objects[0, 0]).TypeEdition(GridProp(Sender), GridProp(Sender).Row);    
  End;
  HighLightButtons;
End;
{ ************************************************************************************************************************************************* }
Function TXtAerauliqueMainForm.ArbreBatimentNode(_Node: PVirtualNode): TArbreBatimentNode;
Begin
  If _Node <> Nil Then Result := ArbreBatiment.GetNodeData(_Node)
                  Else Result := Nil;
End;
{ ************************************************************************************************************************************************* }
Function TXtAerauliqueMainForm.ArbreBatimentNodeData(_Node: PVirtualNode): TVentil_Objet;
Begin
  If ArbreBatimentNode(_Node) <> Nil Then Result := ArbreBatimentNode(_Node)^.Data
                                     Else Result := nil;
End;
{ ************************************************************************************************************************************************* }
Procedure TXtAerauliqueMainForm.ClearGrid(_Grid: TAdvStringGrid);
Var
  m_Row : Integer;
  m_Col : Integer;
Begin
  If _Grid = Grid_ResultatsTroncon Then Begin
    _Grid.Cells[0, 0] := '';
    _Grid.Cells[1, 0] := '';
    _Grid.Cells[2, 0] := '';
  End;
  For m_Row := 1 To _Grid.RowCount - 1 Do For m_Col := 0 To _Grid.ColCount - 1 Do Begin
    _Grid.Cells[m_Col, m_Row] := '';
    _Grid.Cells[m_Row, m_Row] := '';
  End;
End;
{ ************************************************************************************************************************************************* }
Procedure TXtAerauliqueMainForm.InitGridResSysteme;
Const
  c_RowHeaders : Array[1..4] Of String = ('Colonne de la bouche', 'Collecteur de la bouche', 'Bouche', 'S�lectionner');
Var
  m_Row : Integer;
  m_Col : Integer;
Begin
  { Resultats syst�me de ventilation }
  Grid_ResBat.RowCount := 10;
  For m_Col := 0 To Grid_ResBat.ColCount - 1 Do For m_Row := 0 To Grid_ResBat.RowCount - 1 Do Grid_ResBat.Cells[m_Col, m_Row] := '';
  Grid_ResBat.ColWidths[0] := 70; Grid_ResBat.ColWidths[2] := 70;
  Grid_ResBat.ColWidths[1] := 111; Grid_ResBat.ColWidths[3] := 111;
  Grid_ResBat.MergeCells(0, 0, 2, 1);
  Grid_ResBat.MergeCells(2, 0, 2, 1);
  { Resultats Troncons }
  Grid_ResultatsTroncon.Cells[1, 5] := 'Tron�on';
  Grid_ResultatsTroncon.Cells[2, 5] := 'Totales';
  Grid_ResultatsTroncon.CellProperties[1, 5].BrushColor := clSilver;
  Grid_ResultatsTroncon.CellProperties[2, 5].BrushColor := clSilver;
  Grid_ResultatsTroncon.CellProperties[1, 5].FontColor := clWhite;
  Grid_ResultatsTroncon.CellProperties[2, 5].FontColor := clWhite;
  For m_Row := 0 To Grid_ResultatsTroncon.RowCount - 1 Do Begin
    Grid_ResultatsTroncon.CellProperties[1, m_Row].Alignment := taCenter;
    Grid_ResultatsTroncon.CellProperties[2, m_Row].Alignment := taCenter;
  End;
  { Resultats R�seaux }
  For m_Row := 0 To Grid_ResultatsReseau.RowCount - 1 Do For m_Col := 0 To Grid_ResultatsReseau.ColCount - 1 Do Grid_ResultatsReseau.CellProperties[m_Col, m_Row].Alignment := taCenter;
  Grid_ResultatsReseau.MergeCells(0, 0, 4, 1);
  Grid_ResultatsReseau.MergeCells(0, 1, 2, 1);
  Grid_ResultatsReseau.MergeCells(2, 1, 2, 1);
  Grid_ResultatsReseau.MergeCells(0, 2, 4, 1);
  Grid_ResultatsReseau.MergeCells(1, 3, 3, 1);
  Grid_ResultatsReseau.MergeCells(1, 4, 3, 1);
  For m_Row := 6 To Grid_ResultatsReseau.RowCount - 1 Do Begin 
    Grid_ResultatsReseau.MergeCells(0, m_Row, 2, 1);
  End;
  Grid_ResultatsReseau.Cells[0, 0] := 'Pertes de charge';
  Grid_ResultatsReseau.Cells[0, 2] := 'Plage de fonctionnement du caisson';
  Grid_ResultatsReseau.CellProperties[0, 0].BrushColor := clSilver;
  Grid_ResultatsReseau.CellProperties[0, 2].BrushColor := clSilver;
  Grid_ResultatsReseau.CellProperties[0, 0].FontColor := clWhite;
  Grid_ResultatsReseau.CellProperties[0, 2].FontColor := clWhite;
  Grid_ResultatsReseau.CellProperties[0, 0].FontStyle := [fsBold];
  Grid_ResultatsReseau.CellProperties[0, 2].FontStyle := [fsBold];

  Grid_ResultatsReseau.Cells[0, 5] := '';
  Grid_ResultatsReseau.Cells[2, 5] := 'Bouche d�f.';
  Grid_ResultatsReseau.Cells[3, 5] := 'Bouche fav.';
  For m_Col := 0 To 3 Do Begin
     Grid_ResultatsReseau.CellProperties[m_Col, 5].BrushColor := clSilver;
     Grid_ResultatsReseau.CellProperties[m_Col, 5].FontColor  := clWhite;
     Grid_ResultatsReseau.CellProperties[m_Col, 5].FontStyle := [fsBold];
  End;
  For m_Row := 6 To Grid_ResultatsReseau.RowCount - 1 Do Begin
   Grid_ResultatsReseau.Cells[0, m_Row] := c_RowHeaders[m_Row - 5];
   Grid_ResultatsReseau.CellProperties[0, m_Row].Alignment  := taLeftJustify;
   Grid_ResultatsReseau.CellProperties[0, m_Row].BrushColor := clSilver;
   Grid_ResultatsReseau.CellProperties[0, m_Row].FontColor  := clWhite;
   Grid_ResultatsReseau.CellProperties[0, m_Row].FontStyle  := [fsBold];
  End;

End;
{ ************************************************************************************************************************************************* }
Procedure TXtAerauliqueMainForm.ArbreBatimentChange(Sender: TBaseVirtualTree; Node: PVirtualNode);
Begin
  If IsNotNil(ArbreBatimentNodeData(Node)) Then Begin
    ArbreBatimentNodeData(Node).Affiche(Grid_Batiment);
    ArbreBatimentNodeData(Node).AfficheResultats(Grid_ResBat);
  End Else Begin
    ClearGrid(Grid_Batiment);
    InitGridResSysteme;
  End;
  HighLightButtons;
End;
{ ************************************************************************************************************************************************* }
Procedure TXtAerauliqueMainForm.ArbreBatimentGetText(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex; TextType: TVSTTextType; Var CellText: String);
Begin
  If IsNotNil(ArbreBatimentNodeData(Node)) Then CellText := ArbreBatimentNodeData(Node).Reference
                                           Else CellText := '';
End;
{ ************************************************************************************************************************************************* }
Procedure TXtAerauliqueMainForm.ArbreBatimentGetImageIndex(Sender: TBaseVirtualTree; Node: PVirtualNode; Kind: TVTImageKind; Column: TColumnIndex;
                                               var Ghosted: Boolean; var ImageIndex: Integer);
Begin
  If ArbreBatimentNode(Node) <> Nil  Then ImageIndex := ArbreBatimentNode(Node).ImageIndex
                                     Else ImageIndex := - 1;
End;
{ ******************************************************************* \ TXtAerauliqueMainForm ***************************************************************** }


{ ************************************************************************************************************************************************* }
Function LaunchAerauliqueAutonome(_Launcher: TForm) : Boolean;
Var
  DateVersion: String;
  Chn        : String;
Begin
  Result := True;

  ModeAero := maCW;

  Application.CreateForm(TXtAerauliqueMainForm, XtAerauliqueMainForm);
  XtAerauliqueMainForm.Launcher := _Launcher;
  FormatSettings.DecimalSeparator := '.';
  Try
    If IsAdmin Then RegisterExt('.BBS Slama', 'Aeraulique', 'Etude d''un r�seau de VMC');
  Except { la routine IsAdmin semble poser parfois probl�me } End;
  XtAerauliqueMainForm.TimerAutoSave.Enabled  := False;
  DateVersion:=FormatDateTime('dd mmmm yyyy',FileDateToDateTime(FileAge(Application.EXEName)));
  XtAerauliqueMainForm.StatusBar1.Panels[0].Text := 'Aeraulique version du ' + DateVersion;
  Options := TVentil_Options.Create;
  Options.LoadOptions;
  FBeginProgress(4, 'Initialisation');
  Application.CreateForm(TDlg_AffichAccidents, Dlg_AffichAccidents);
  FStepProgress('Connexion aux bases de donn�es');
  EdibatecDispo := ConnexionEdibatec;
  XtAerauliqueMainForm.InitGridResSysteme;
  FStepProgress('Connexion aux bases de donn�es');
  If Connection_Logiciel Then Begin

    FStepProgress('Chargement des banques de donn�es');
{$IfDef BIM_METRE}
//    Table_Biblio_DataCaisson.Ouvrir;
    BaseEdibatec := TEdibatec_ListeClasses.Create;
//    Table_Biblio_DataCaisson.Fermer;
{$EndIf}
    FabricantsVMC := TEdibatec_ListeFabricants.Create;    
    SystemesVMC := TListeSystemes.Create;
    SystemesVMC.Load;

    XtAerauliqueMainForm.Load_GammesChiffrage;

    Base_Etude := Base_Affaire;
//    Base_Etude.Connection;
    Chargement_Structure_Etude;


    FStepProgress('Cr�ation des outils graphiques');

    InterfaceCAD := Nil;
    //todo regis
    InterfaceCAD := IBBScad_Aeraulique_Create(TCallBackAeraulique.Create as IBBScad_Aeraulique_Callback);
    InterfaceCAD.IBBScad_Commun_InitCAD(XtAerauliqueMainForm.Panel_CAD.Handle);
    InterfaceCAD.IBBScad_Etude_Add('0');
//    InterfaceCAD.IBBScad_Etude_Activate('0');
    InterfaceCAD.IBBScad_Events_Resize;
    XtAerauliqueMainForm.MergeCadMenu;
    XtAerauliqueMainForm.ArbreBatiment.NodeDataSize := SizeOf(ArbreBatimentVNode);
    XtAerauliqueMainForm.Grid_ResBat.Parent         := XtAerauliqueMainForm.Panel_Resultats;
    XtAerauliqueMainForm.Launcher.Hide;
    XtAerauliqueMainForm.Show;
    Application.ProcessMessages;
    XtAerauliqueMainForm.WindowState := wsMaximized;
    Result := XtAerauliqueMainForm.DemarrageAeraulique;
    If Not Result Then Exit;
    Dlg_AffichAccidents.Grid_accidentsAssocies.Parent := XtAerauliqueMainForm.TabSheet3;
    XtAerauliqueMainForm.HighLightButtons;

    XtAerauliqueMainForm.TimerAutoSave.Enabled  := Options.AutoSaveActive;
    XtAerauliqueMainForm.TimerAutoSave.Interval := Options.AutoSaveDelai;

    XtAerauliqueMainForm.Panel_Batiment.Visible := True;
    XtAerauliqueMainForm.Grid_Properties.Visible:= Not   XtAerauliqueMainForm.Panel_Batiment.Visible;
    XtAerauliqueMainForm.Panel_Batiment.Height  :=   XtAerauliqueMainForm.Panel_TitreRes.Top - 2 *   XtAerauliqueMainForm.Panel_TitreRes.Height;
    XtAerauliqueMainForm.Grid_ResultatsReseau.Visible := Not   XtAerauliqueMainForm.Panel_Batiment.Visible;
    XtAerauliqueMainForm.Grid_ResBat.Visible    :=   XtAerauliqueMainForm.Panel_Batiment.Visible;
    XtAerauliqueMainForm.Panel_TitreRes.Caption := 'R�sultats (B�timent / Logements)';


    FEndProgress;
  End Else Begin
    FEndProgress;
    BBS_ShowMessage('Des fichiers n�cessaires au fonctionnement du logiciel sont manquants' + #13 + 'Aeraulique ne peut pas d�marrer', mtError, [mbYes], 0);
  End;
End;
{ ************************************************************************************************************************************************* }
Procedure TXtAerauliqueMainForm.AddAutoMenuImage(Const _Action: TAction; Var _MenuItem: TMenuItem);
Var
  m_Bmp    : TBitmap;
Begin
  If (_Action <> nil) And (_Action.ImageIndex > -1) Then Begin
    m_Bmp := TBitmap.Create;
    m_Bmp.Width := 16;
    m_Bmp.Height := 16;
    _Action.ActionList.Images.GetBitmap(_Action.ImageIndex, m_Bmp);
    MainMenu1.Images.Add(m_Bmp, nil);
    _MenuItem.ImageIndex := MainMenu1.Images.Count - 1;
  End;
End;
{ ************************************************************************************************************************************************* }
Procedure TXtAerauliqueMainForm.AddSubMenu(Const _ItemSource: TMenuItem;Var _ItemDest: TMenuItem);
Var
  m_NoFils : Integer;
  m_Item   : TMenuItem;
  m_Action : TAction;
Begin
  For m_NoFils := 0 To _ItemSource.Count - 1 Do Begin
    m_Action :=Taction (_ItemSource.Items[m_NoFils].Action); 
    If (m_Action <> nil) Or (_ItemSource.Items[m_NoFils].Count > 0) Then Begin
      m_Item := TMenuItem.Create(MainMenu1);
      m_Item.Caption := _ItemSource.Items[m_NoFils].Caption;
      m_Item.Action  := m_Action;
      AddAutoMenuImage(m_Action, m_Item);
      AddSubMenu(_ItemSource.Items[m_NoFils], m_Item);
      _ItemDest.Add(m_Item);
      SetAutoMenuItemVisible(m_Item);
    End;
  End;
End;
{ ************************************************************************************************************************************************* }
Procedure TXtAerauliqueMainForm.MergeCadMenu;
Var
  m_CadMenu : TMainMenu;
  m_NoMenu : Integer;
  m_Item   : TMenuItem;
  m_Action : TAction;
Begin
//todo regis
End;
{ ************************************************************************************************************************************************* }
Procedure TXtAerauliqueMainForm.SetAutoMenuItemVisible(var _MenuItem: TMenuItem);
Var
  m_NoFils : Integer;
Begin
  If _MenuItem.Count > 0 Then Begin
    _MenuItem.Visible := False;
    m_NoFils := 0;
    While (m_NoFils < _MenuItem.Count) And Not _MenuItem.Visible Do Begin
      _MenuItem.Visible := _MenuItem.Visible Or _MenuItem.Items[m_NoFils].Visible; 
      Inc(m_NoFils);
    End;
  End;
End;
{ ************************************************************************************************************************************************* }
Function TXtAerauliqueMainForm.DemarrageAeraulique: Boolean;
Begin
  Result := True;
  Btn_NouvEtude.Visible        := True;
  Btn_OuvrirEtude.Visible      := True;
  Ouvrir1.visible              := True;
  Nouvelle12.Visible           := True;

        MainMenu1.Items.Find('Banques').Visible := True;

//        Vrifierlesmisesjour1.Visible := False;

  Menu_Enregistrersous.Visible := False;
  RepEtudes := ExtractFilePath(Application.ExeName) + cst_RepEtudeOptair + '\';
End;
{ ************************************************************************************************************************************************* }
Procedure TXtAerauliqueMainForm.SortieAeraulique;
Begin

End;
{ ************************************************************************************************************************************************* }
Procedure TXtAerauliqueMainForm.Load_GammesChiffrage;
Begin
{$IfDef BIM_METRE}
  Table_Syst_Gammes.Ouvrir;
  Table_Syst_Gammes.Donnees.First;
  While Not Table_Syst_Gammes.Donnees.Eof Do Begin
    If Table_Syst_Gammes.Donnees.FieldByName('CODE_MODIFIE').AsWideString <> '' Then
      GammesChiffrage[Table_Syst_Gammes.Donnees.FieldByName('NO_GAMME').AsInteger] := Table_Syst_Gammes.Donnees.FieldByName('CODE_MODIFIE').AsWideString
    Else GammesChiffrage[Table_Syst_Gammes.Donnees.FieldByName('NO_GAMME').AsInteger] := Table_Syst_Gammes.Donnees.FieldByName('CODE_EDIBATEC').AsWideString;
    Table_Syst_Gammes.Donnees.Next;
  End;
  Table_Syst_Gammes.Fermer;
{$EndIf}
End;
{ ************************************************************************************************************************************************* }
procedure TXtAerauliqueMainForm.TimerAutoSaveTimer(Sender: TObject);
begin


  //todo regis

        if Options.AutoSaveActive And
           (Etude <> nil) And Etude.AutoSaveAutorisee then
                Begin
                Etude.IsAutoSaving := True;
                Sauvegarder;
                Etude.IsAutoSaving := False;                
                End;
                
end;
{ ************************************************************************************************************************************************* }

End.



