unit Ventil_ProprietesFlottantes;
                                  
{$Include 'Ventil_Directives.pas'}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, AdvGrid, BaseGrid, Grids, StdCtrls, Buttons;

type
  TFormProprietesFlottantes = class(TForm)
    PanelGrid: TPanel;
    PanelBoutonClose: TPanel;
    BitBtnClose: TBitBtn;
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BitBtnCloseClick(Sender: TObject);
  private
    { D�clarations priv�es }
    SaveAlignFGrid : TAlign;
    SaveParent     : TWinControl;    
    FGRID: TAdvStringGrid;
  public
    { D�clarations publiques }
  end;

Procedure AfficherProprietesFlottantes(_Grid: TAdvStringGrid);

var
  FormProprietesFlottantes: TFormProprietesFlottantes;

implementation
Uses Ventil_Declarations, Ventil_Const, Ventil_Types;

{$R *.dfm}

Procedure AfficherProprietesFlottantes(_Grid: TAdvStringGrid);
Var
  TempTroncon : TVentil_Objet;
Begin
        if (FormProprietesFlottantes <> nil) and (_Grid.Parent = FormProprietesFlottantes.PanelGrid) then
                Begin
                FormProprietesFlottantes.Show;
                Exit;
                End;

  Application.CreateForm(TFormProprietesFlottantes, FormProprietesFlottantes);
  FormProprietesFlottantes.FormStyle := fsStayOnTop;
  FormProprietesFlottantes.SaveParent := _Grid.Parent;
  FormProprietesFlottantes.FGRID := _Grid;
  FormProprietesFlottantes.SaveAlignFGrid := FormProprietesFlottantes.FGRID.Align;
  FormProprietesFlottantes.FGRID.Align := alClient;
  FormProprietesFlottantes.Show;
  TempTroncon := TVentil_Objet(FormProprietesFlottantes.FGRID.Objects[0,0]);
  //bidouille pour redimensionenr la grille (pas trouv� d'autre solution pour le moment)
  TempTroncon.SetUnSelected;
  TempTroncon.SetSelected;
End;

procedure TFormProprietesFlottantes.FormShow(Sender: TObject);
begin
  FGRID.Parent := PanelGrid;
        If TVentil_Objet(FGRID.Objects[0,0]) = Nil then
                Exit;

Caption := 'Propri�t�s de l''objet ' + TVentil_Objet(FGRID.Objects[0,0]).FamilleObjet;

  TVentil_Objet(FGRID.Objects[0,0]).Affiche(FGRID);
//  FGRID.ROW := 1;
end;

procedure TFormProprietesFlottantes.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  FGRID.Align := SaveAlignFGrid;
  FGRID.Parent := SaveParent;
//  FreeAndNil(FormProprietesFlottantes);
end;

procedure TFormProprietesFlottantes.BitBtnCloseClick(Sender: TObject);
begin
Close;
end;

end.
