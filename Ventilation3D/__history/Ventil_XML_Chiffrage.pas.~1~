
{**********************************************************************************************}
{                                                                                              }
{                                    Liaison de donn�es XML                                    }
{                                                                                              }
{         G�n�r� le : 22/01/2018 09:11:28                                                      }
{       G�n�r� depuis : D:\Developpement\DelphSou\Ventilation3D\XSD\Ventil_XML_Chiffrage.xsd   }
{                                                                                              }
{**********************************************************************************************}

unit Ventil_XML_Chiffrage;

interface

uses xmldom, XMLDoc, XMLIntf;

type

{ D�cl. Forward }

  IXMLCorresp_Chiffrage = interface;
  IXMLProduits = interface;
  IXMLProduit = interface;
  IXMLGammes = interface;
  IXMLGamme = interface;

{ IXMLCorresp_Chiffrage }

  IXMLCorresp_Chiffrage = interface(IXMLNode)
    ['{95F08626-865E-4A76-A69A-968214482A37}']
    { Accesseurs de propri�t�s }
    function Get_Produits: IXMLProduits;
    function Get_Gammes: IXMLGammes;
    { M�thodes & propri�t�s }
    property Produits: IXMLProduits read Get_Produits;
    property Gammes: IXMLGammes read Get_Gammes;
  end;

{ IXMLProduits }

  IXMLProduits = interface(IXMLNodeCollection)
    ['{B7138F4C-5BEE-4034-BC38-3D0C88DD7DE0}']
    { Accesseurs de propri�t�s }
    function Get_Produit(Index: Integer): IXMLProduit;
    { M�thodes & propri�t�s }
    function Add: IXMLProduit;
    function Insert(const Index: Integer): IXMLProduit;
    property Produit[Index: Integer]: IXMLProduit read Get_Produit; default;
  end;

{ IXMLProduit }

  IXMLProduit = interface(IXMLNode)
    ['{9FC0E53E-337D-43CD-9D89-5FD9ACE72D68}']
    { Accesseurs de propri�t�s }
    function Get_Code_Logiciel: UnicodeString;
    function Get_Code_Aerau: UnicodeString;
    function Get_Code_Article: UnicodeString;
    procedure Set_Code_Logiciel(Value: UnicodeString);
    procedure Set_Code_Aerau(Value: UnicodeString);
    procedure Set_Code_Article(Value: UnicodeString);
    { M�thodes & propri�t�s }
    property Code_Logiciel: UnicodeString read Get_Code_Logiciel write Set_Code_Logiciel;
    property Code_Aerau: UnicodeString read Get_Code_Aerau write Set_Code_Aerau;
    property Code_Article: UnicodeString read Get_Code_Article write Set_Code_Article;
  end;

{ IXMLGammes }

  IXMLGammes = interface(IXMLNodeCollection)
    ['{25FD3F34-506A-4632-B2AE-033DB3BE22AB}']
    { Accesseurs de propri�t�s }
    function Get_Gamme(Index: Integer): IXMLGamme;
    { M�thodes & propri�t�s }
    function Add: IXMLGamme;
    function Insert(const Index: Integer): IXMLGamme;
    property Gamme[Index: Integer]: IXMLGamme read Get_Gamme; default;
  end;

{ IXMLGamme }

  IXMLGamme = interface(IXMLNode)
    ['{B3C967FA-90D0-4244-BF09-E3AAAE95BCCE}']
    { Accesseurs de propri�t�s }
    function Get_Code_Logiciel: UnicodeString;
    function Get_Code_Aerau: UnicodeString;
    procedure Set_Code_Logiciel(Value: UnicodeString);
    procedure Set_Code_Aerau(Value: UnicodeString);
    { M�thodes & propri�t�s }
    property Code_Logiciel: UnicodeString read Get_Code_Logiciel write Set_Code_Logiciel;
    property Code_Aerau: UnicodeString read Get_Code_Aerau write Set_Code_Aerau;
  end;

{ D�cl. Forward }

  TXMLCorresp_Chiffrage = class;
  TXMLProduits = class;
  TXMLProduit = class;
  TXMLGammes = class;
  TXMLGamme = class;

{ TXMLCorresp_Chiffrage }

  TXMLCorresp_Chiffrage = class(TXMLNode, IXMLCorresp_Chiffrage)
  protected
    { IXMLCorresp_Chiffrage }
    function Get_Produits: IXMLProduits;
    function Get_Gammes: IXMLGammes;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLProduits }

  TXMLProduits = class(TXMLNodeCollection, IXMLProduits)
  protected
    { IXMLProduits }
    function Get_Produit(Index: Integer): IXMLProduit;
    function Add: IXMLProduit;
    function Insert(const Index: Integer): IXMLProduit;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLProduit }

  TXMLProduit = class(TXMLNode, IXMLProduit)
  protected
    { IXMLProduit }
    function Get_Code_Logiciel: UnicodeString;
    function Get_Code_Aerau: UnicodeString;
    function Get_Code_Article: UnicodeString;
    procedure Set_Code_Logiciel(Value: UnicodeString);
    procedure Set_Code_Aerau(Value: UnicodeString);
    procedure Set_Code_Article(Value: UnicodeString);
  end;

{ TXMLGammes }

  TXMLGammes = class(TXMLNodeCollection, IXMLGammes)
  protected
    { IXMLGammes }
    function Get_Gamme(Index: Integer): IXMLGamme;
    function Add: IXMLGamme;
    function Insert(const Index: Integer): IXMLGamme;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLGamme }

  TXMLGamme = class(TXMLNode, IXMLGamme)
  protected
    { IXMLGamme }
    function Get_Code_Logiciel: UnicodeString;
    function Get_Code_Aerau: UnicodeString;
    procedure Set_Code_Logiciel(Value: UnicodeString);
    procedure Set_Code_Aerau(Value: UnicodeString);
  end;

{ Fonctions globales }

function GetCorresp_Chiffrage(Doc: IXMLDocument): IXMLCorresp_Chiffrage;
function LoadCorresp_Chiffrage(const FileName: string): IXMLCorresp_Chiffrage;
function NewCorresp_Chiffrage: IXMLCorresp_Chiffrage;

const
  TargetNamespace = '';

implementation

{ Fonctions globales }

function GetCorresp_Chiffrage(Doc: IXMLDocument): IXMLCorresp_Chiffrage;
begin
  Result := Doc.GetDocBinding('Corresp_Chiffrage', TXMLCorresp_Chiffrage, TargetNamespace) as IXMLCorresp_Chiffrage;
end;

function LoadCorresp_Chiffrage(const FileName: string): IXMLCorresp_Chiffrage;
begin
  Result := LoadXMLDocument(FileName).GetDocBinding('Corresp_Chiffrage', TXMLCorresp_Chiffrage, TargetNamespace) as IXMLCorresp_Chiffrage;
end;

function NewCorresp_Chiffrage: IXMLCorresp_Chiffrage;
begin
  Result := NewXMLDocument.GetDocBinding('Corresp_Chiffrage', TXMLCorresp_Chiffrage, TargetNamespace) as IXMLCorresp_Chiffrage;
end;

{ TXMLCorresp_Chiffrage }

procedure TXMLCorresp_Chiffrage.AfterConstruction;
begin
  RegisterChildNode('Produits', TXMLProduits);
  RegisterChildNode('Gammes', TXMLGammes);
  inherited;
end;

function TXMLCorresp_Chiffrage.Get_Produits: IXMLProduits;
begin
  Result := ChildNodes['Produits'] as IXMLProduits;
end;

function TXMLCorresp_Chiffrage.Get_Gammes: IXMLGammes;
begin
  Result := ChildNodes['Gammes'] as IXMLGammes;
end;

{ TXMLProduits }

procedure TXMLProduits.AfterConstruction;
begin
  RegisterChildNode('Produit', TXMLProduit);
  ItemTag := 'Produit';
  ItemInterface := IXMLProduit;
  inherited;
end;

function TXMLProduits.Get_Produit(Index: Integer): IXMLProduit;
begin
  Result := List[Index] as IXMLProduit;
end;

function TXMLProduits.Add: IXMLProduit;
begin
  Result := AddItem(-1) as IXMLProduit;
end;

function TXMLProduits.Insert(const Index: Integer): IXMLProduit;
begin
  Result := AddItem(Index) as IXMLProduit;
end;

{ TXMLProduit }

function TXMLProduit.Get_Code_Logiciel: UnicodeString;
begin
  Result := ChildNodes['Code_Logiciel'].Text;
end;

procedure TXMLProduit.Set_Code_Logiciel(Value: UnicodeString);
begin
  ChildNodes['Code_Logiciel'].NodeValue := Value;
end;

function TXMLProduit.Get_Code_Aerau: UnicodeString;
begin
  Result := ChildNodes['Code_Aerau'].Text;
end;

procedure TXMLProduit.Set_Code_Aerau(Value: UnicodeString);
begin
  ChildNodes['Code_Aerau'].NodeValue := Value;
end;

function TXMLProduit.Get_Code_Article: UnicodeString;
begin
  Result := ChildNodes['Code_Article'].Text;
end;

procedure TXMLProduit.Set_Code_Article(Value: UnicodeString);
begin
  ChildNodes['Code_Article'].NodeValue := Value;
end;

{ TXMLGammes }

procedure TXMLGammes.AfterConstruction;
begin
  RegisterChildNode('Gamme', TXMLGamme);
  ItemTag := 'Gamme';
  ItemInterface := IXMLGamme;
  inherited;
end;

function TXMLGammes.Get_Gamme(Index: Integer): IXMLGamme;
begin
  Result := List[Index] as IXMLGamme;
end;

function TXMLGammes.Add: IXMLGamme;
begin
  Result := AddItem(-1) as IXMLGamme;
end;

function TXMLGammes.Insert(const Index: Integer): IXMLGamme;
begin
  Result := AddItem(Index) as IXMLGamme;
end;

{ TXMLGamme }

function TXMLGamme.Get_Code_Logiciel: UnicodeString;
begin
  Result := ChildNodes['Code_Logiciel'].Text;
end;

procedure TXMLGamme.Set_Code_Logiciel(Value: UnicodeString);
begin
  ChildNodes['Code_Logiciel'].NodeValue := Value;
end;

function TXMLGamme.Get_Code_Aerau: UnicodeString;
begin
  Result := ChildNodes['Code_Aerau'].Text;
end;

procedure TXMLGamme.Set_Code_Aerau(Value: UnicodeString);
begin
  ChildNodes['Code_Aerau'].NodeValue := Value;
end;

end.