unit Ventil_Utils;

interface

{$Include 'Ventil_Directives.pas'}

Uses ShellApi,
     Windows,
     Ventil_SystemeVMC,
     Types,
     SysUtils,
     Classes,
     StrUtils,
     Math,
     Controls;

type
 Tcomplexe=record re,im:double; end;
 TBuffer=array[0..3] of TComplexe;
 Pbuffer=^tbuffer;

function Premierdegre(a,b:double;buffer:pbuffer):integer;
function Deuxiemedegre(a,b,c:double;buffer:pbuffer):integer;
function Troisiemedegre(a,b,c,d:double;buffer:pbuffer):integer;
function Quatriemedegre(a,b,c,d,e:double;var buffer:pbuffer):integer;
function Complexetostr(c:tcomplexe):string;
function ComplexetoFloat(c:tcomplexe):Real;
Procedure GetSolutions3emeDegreDansR(Var _ListeSolution : TStringList; a,b,c,d:double);

function CalculeValeurIntermedaireTableau(EMin : Real; EMax : Real; SMin : Real; SMax : Real; EReel : Real) : Real;

Function TempRep: String;
Function BbsCopyFile(_Source, _Destination: String): Boolean;
Function IntSumTab(_Tab: Array Of Integer; _Low: Integer = -1; _High: Integer = -1): Integer;

Function GetSubStr(Chaine: WideString; Position: Integer; Separateur: String): String;
Function StrToStringList(Chaine: WideString; Separateur: String): TStringList;

Function Bool2Str(B: Boolean): String;
Function Str2Bool(Str: String): Boolean;
Function WithoutBlanks(Str: String) : String;
Function Str2GUID(Str: String): TGUID;

Function IsNil(_Obj: TObject): Boolean;
Function IsNotNil(_Obj: TObject): Boolean;
Function UsedList(_List: TList): Boolean;

Function StringToInt(Const S: String; Def: Integer): Integer;
Function Str2Int(Const S: String): Integer;
Function Int2Bool(Const I: Integer): Boolean;
Function Bool2Int(Const I: Boolean): Integer;
Function Str2Float(Const S: String): Extended;
Function StringToFloat(Const S: String; Def: Extended): Extended;
Function Float2Str(F: Real; Decimales: Smallint) : String;

Function XML2Bool(_Val : Integer) : Boolean;
Function XML2Real(_Val : UnicodeString) : Real;
Function XML2Date(_Val : UnicodeString) : TDate;
Function XML2VentilBool(_Val : Integer) : Integer;
Function XML2SystemeVMC(_Val : Integer) : TVentil_Systeme;

Function Bool2XML(_Val : Boolean) : Integer;
Function Real2XML(_Val : Real; _Decimales : Integer = 2) : UnicodeString;
Function Date2XML(_Val : TDate) : UnicodeString;
Function VentilBool2XML(_Val : Integer) : Integer;
Function SystemeVMC2XML(_Val : TVentil_Systeme) : Integer;

Function GetIndiceDiametreEquivalent(_Dim1 : Integer; _Dim2 : Integer) : Integer;
Function GetDiametreEquivalent(_Dim1 : Integer; _Dim2 : Integer) : Integer;
Function GetRealDiametreEquivalent(_Dim1 : Integer; _Dim2 : Integer) : Integer;

Procedure ThemingForm(_Component : TComponent; _IDTheme : Integer; _GererPanel : Boolean = True);

Function GetChrDiam : String;

{ ************************************************************************************************************************************************** }
procedure SaveClipboard(S: TStream);
procedure LoadClipboard(S: TStream);

implementation


Uses
        Graphics, Forms, ExtCtrls, StdCtrls, VirtualTrees, AdvGrid, ComCtrls, AdvToolBar, AdvToolBarStylers,
        BBScad_Interface_Commun,
        Ventil_Declarations,
        Clipbrd,
        Ventil_Const;

{ ************************************************************************************************************************************************** }
procedure CopyStreamToClipboard(fmt: Cardinal; S: TStream);
var
  hMem: THandle;
  pMem: Pointer;
begin
  Assert(Assigned(S));
  S.Position := 0;
  hMem       := GlobalAlloc(GHND or GMEM_DDESHARE, S.Size);
  if hMem <> 0 then
  begin
    pMem := GlobalLock(hMem);
    if pMem <> nil then
    begin
      try
        S.Read(pMem^, S.Size);
        S.Position := 0;
      finally
        GlobalUnlock(hMem);
      end;
      Clipboard.Open;
      try
        Clipboard.SetAsHandle(fmt, hMem);
      finally
        Clipboard.Close;
      end;
    end { If }
    else
    begin
      GlobalFree(hMem);
      OutOfMemoryError;
    end;
  end { If }
  else
    OutOfMemoryError;
end; { CopyStreamToClipboard }
{ ************************************************************************************************************************************************** }
procedure CopyStreamFromClipboard(fmt: Cardinal; S: TStream);
var
  hMem: THandle;
  pMem: Pointer;
begin
  Assert(Assigned(S));
  hMem := Clipboard.GetAsHandle(fmt);
  if hMem <> 0 then
  begin
    pMem := GlobalLock(hMem);
    if pMem <> nil then
    begin
      try
        S.Write(pMem^, GlobalSize(hMem));
        S.Position := 0;
      finally
        GlobalUnlock(hMem);
      end;
    end { If }
    else
      raise Exception.Create('CopyStreamFromClipboard: could not lock global handle ' +
        'obtained from clipboard!');
  end; { If }
end; { CopyStreamFromClipboard }
{ ************************************************************************************************************************************************** }
procedure SaveClipboardFormat(fmt: Word; writer: TWriter);
var
  fmtname: array[0..128] of Char;
  ms: TMemoryStream;
begin
  Assert(Assigned(writer));
  if 0 = GetClipboardFormatName(fmt, fmtname, SizeOf(fmtname)) then
    fmtname[0] := #0;
  ms := TMemoryStream.Create;
  try
    CopyStreamFromClipboard(fmt, ms);
    if ms.Size > 0 then
    begin
      writer.WriteInteger(fmt);
      writer.WriteString(fmtname);
      writer.WriteInteger(ms.Size);
      writer.Write(ms.Memory^, ms.Size);
    end; { If }
  finally
    ms.Free
  end; { Finally }
end; { SaveClipboardFormat }
{ ************************************************************************************************************************************************** }
procedure LoadClipboardFormat(reader: TReader);
var
  fmt: Integer;
  fmtname: string;
  Size: Integer;
  ms: TMemoryStream;
begin
  Assert(Assigned(reader));
  fmt     := reader.ReadInteger;
  fmtname := reader.ReadString;
  Size    := reader.ReadInteger;
  ms      := TMemoryStream.Create;
  try
    ms.Size := Size;
    reader.Read(ms.memory^, Size);
    if Length(fmtname) > 0 then
      fmt := RegisterCLipboardFormat(PChar(fmtname));
    if fmt <> 0 then
      CopyStreamToClipboard(fmt, ms);
  finally
    ms.Free;
  end; { Finally }
end; { LoadClipboardFormat }
{ ************************************************************************************************************************************************** }
procedure SaveClipboard(S: TStream);
var
  writer: TWriter;
  i: Integer;
begin
  Assert(Assigned(S));
  writer := TWriter.Create(S, 4096);
  try
    Clipboard.Open;
    try
      writer.WriteListBegin;
      for i := 0 to Clipboard.formatcount - 1 do
        SaveClipboardFormat(Clipboard.Formats[i], writer);
      writer.WriteListEnd;
    finally
      Clipboard.Close;
    end; { Finally }
  finally
    writer.Free
  end; { Finally }
end; { SaveClipboard }
{ ************************************************************************************************************************************************** }
procedure LoadClipboard(S: TStream);
var
  reader: TReader;
begin
  S.seek(0,soFromBeginning );
  Assert(Assigned(S));
  reader := TReader.Create(S, 4096);
  try
    Clipboard.Open;
    try
      clipboard.Clear;
      reader.ReadListBegin;
      while not reader.EndOfList do
        LoadClipboardFormat(reader);
      reader.ReadListEnd;
    finally
      Clipboard.Close;
    end; { Finally }
  finally
    reader.Free
  end; { Finally }
end; { LoadClipboard }
{ ************************************************************************************************************************************************** }
Function XML2Bool(_Val : Integer) : Boolean;
begin
  Result := Int2Bool(_Val);
end;
{ ************************************************************************************************************************************************** }
Function XML2Real(_Val : UnicodeString) : Real;
begin
  Result := Str2Float(_Val);
end;
{ ************************************************************************************************************************************************** }
Function XML2Date(_Val : UnicodeString) : TDate;
Var
  Fmt     : TFormatSettings;
begin

  fmt.ShortDateFormat     :='dd/mm/yyyy';
  fmt.DateSeparator       :='/';
//  fmt.LongTimeFormat      :='hh:nn:ss';
//  fmt.TimeSeparator       :=':';
  Result                  := StrToDateTime(_Val,Fmt);

end;
{ ************************************************************************************************************************************************** }
Function XML2VentilBool(_Val : Integer) : Integer;
begin
    if XML2Bool(_Val) then
      Result := c_Oui
    else
      Result := c_Non;
end;
{ ************************************************************************************************************************************************** }
Function XML2SystemeVMC(_Val : Integer) : TVentil_Systeme;
begin
  Result := Nil;

  case _Val of
    1 : Result := SystemesVMC.SystemeHygroANonGaz;
    2 : Result := SystemesVMC.SystemeHygroBNonGaz;
    3 : Result := SystemesVMC.SystemeHygroGaz;
    4 : Result := SystemesVMC.SystemeHygroANonGaz;
    5 : Result := SystemesVMC.SystemeAutoNonGaz;
  end;

end;
{ ************************************************************************************************************************************************** }
Function Bool2XML(_Val : Boolean) : Integer;
begin
  Result := Bool2Int(_Val);
end;
{ ************************************************************************************************************************************************** }
Function Real2XML(_Val : Real; _Decimales : Integer = 2) : UnicodeString;
begin
  Result := Float2Str(_Val, _Decimales);
end;
{ ************************************************************************************************************************************************** }
Function Date2XML(_Val : TDate) : UnicodeString;
begin
  Result                  := DateTimeToStr(_Val);
end;
{ ************************************************************************************************************************************************** }
Function VentilBool2XML(_Val : Integer) : Integer;
begin
  if _val = c_Oui then
    Result := Bool2XML(True)
  else
    Result := Bool2XML(False);
end;
{ ************************************************************************************************************************************************** }
Function SystemeVMC2XML(_Val : TVentil_Systeme) : Integer;
begin
//  Result := 2;

  case _Val of
    0 : Result := 1;
    1 : Result := 2;
    2 : Result := 3;
    3 : Result := 4;
    4 : Result := 5;
  end;

end;
{ ************************************************************************************************************************************************** }
Function GetIndiceDiametreEquivalent(_Dim1 : Integer; _Dim2 : Integer) : Integer;
Var
  cpt : Integer;
  DiamCalc : Single;
begin
  Result := c_Diam_125;
  DiamCalc := GetRealDiametreEquivalent(_Dim1, _Dim2);

    if DiamCalc <= c_ListeDiametres[Low(c_ListeDiametres)] then
      Exit;

    for cpt := High(c_ListeDiametres) DownTo Low(c_ListeDiametres) do
      if DiamCalc >= c_ListeDiametres[cpt] then
      begin
      Result := Min(High(c_ListeDiametres), Cpt);
      Exit;
      end;


end;
{ ************************************************************************************************************************************************** }
Function GetDiametreEquivalent(_Dim1 : Integer; _Dim2 : Integer) : Integer;
begin
  Result := (c_ListeDiametres[GetIndiceDiametreEquivalent(_Dim1, _Dim2)])
end;
{ ************************************************************************************************************************************************** }
Function GetRealDiametreEquivalent(_Dim1 : Integer; _Dim2 : Integer) : Integer;
begin
  Result := Round(2 * (_Dim1 * _Dim2) / (_Dim1 + _Dim2));
end;
{ ************************************************************************************************************************************************** }
// converti un complexe en string
function Complexetostr(c:tcomplexe):string;
begin
 result:=floattostr(c.re);
 if c.im<>0 then result:=result+' + i'+floattostr(c.im);
end;
{ ************************************************************************************************************************************************** }
function ComplexetoFloat(c:tcomplexe):Real;
Begin
Result := 0;
Str2Float(Complexetostr(c));
End;
{ ************************************************************************************************************************************************** }
Procedure GetSolutions3emeDegreDansR(Var _ListeSolution : TStringList; a,b,c,d:double);
var
 n,m,i:integer;
 solutions:Pbuffer;
Begin
if _ListeSolution = nil then
_ListeSolution := TStringList.Create;

_ListeSolution.Clear;

 new(solutions);
 n:=Troisiemedegre(a,b,c,d,solutions);

   m:=0;
   for i:=0 to n-1 do
    if solutions[i].im=0 then
     begin
      solutions[m]:=solutions[i];
      inc(m);
     end;
    n:=m;

 if n>0 then _ListeSolution.Add(Complexetostr(solutions[0]));
 if n>1 then _ListeSolution.Add(Complexetostr(solutions[1]));
 if n>2 then _ListeSolution.Add(Complexetostr(solutions[2]));
 if n>3 then _ListeSolution.Add(Complexetostr(solutions[3]));
End;
{ ************************************************************************************************************************************************** }
// calcul la racine cubique sans bug pour les nombres n�gatifs
function radcubique(nb:double):double;
begin
 if nb>0 then
  result:=power(nb,1.0/3.0)
 else
  result:=-power(-nb,1.0/3.0);
end;

{ ************************************************************************************************************************************************** }
//Donne la racine d'un polynome de degr� 1, retourne le nombre de racines
function Premierdegre(a,b:double;buffer:pbuffer):integer;
begin
 buffer[0].re := -b/a;
 buffer[0].im := 0;
 result:=1;
end;

{ ************************************************************************************************************************************************** }
//Resolution d'une �quation du deuxieme degr�, retourne le nombre de racines
function Deuxiemedegre(a,b,c:double;buffer:pbuffer):integer;
var
 delta:double;
begin
 if (a = 0) then
  begin
   result:=Premierdegre(b,c,buffer);
   exit;
  end;
 // calcul du d�terminant
 delta := b*b-4*a*c;
 // positif, deux racines dans R
 if (delta>=0) then
  begin
   buffer[0].re := (-b-sqrt(delta))/(2*a);
   buffer[0].im := 0;
   buffer[1].re := (-b+sqrt(delta))/(2*a);
   buffer[1].im := 0;
  end
 else
 // sinon deux racines dans C
  begin
   buffer[0].re := -b/(2*a);
   buffer[0].im := (-sqrt(-delta))/(2*a);
   buffer[1].re := buffer[0].re;
   buffer[1].im := -buffer[0].im;
  end;
 result:=2;
end;

{ ************************************************************************************************************************************************** }
//Resolution d'une �quation du troisi�me degr�, retourne le nombre de racines
// M�thode de Cardan
function Troisiemedegre(a,b,c,d:double;buffer:pbuffer):integer;
var
 t,arg,p,q,delta:double;
// r:double;
 i:integer;
begin
 if (a = 0) then
  begin
   result:=Deuxiemedegre(b,c,d,buffer);
   exit;
  end;

  // passe passe pour ramener � un degr� 2
 p := (-b*b)/(3*a*a) + c/a;
 q := (2*b*b*b)/(27*a*a*a) - (b*c)/(3*a*a) + d/a;

 delta := q*q/4.0 + p*p*p/27.0;

 if (delta>0) then
  begin
   buffer[0].re := radcubique(-q/2.0+sqrt(delta))+radcubique(-q/2.0-sqrt(delta));
   buffer[0].im := 0;
   if (buffer[0].re <> 0) then
    Deuxiemedegre(1,buffer[0].re,-q/buffer[0].re,pbuffer(@buffer[1]))
   else
    Deuxiemedegre(1,0,p,pbuffer(@buffer[1]));

   buffer[0].re := buffer[0].re-b/(3*a);
   buffer[1].re := buffer[1].re-b/(3*a);
   buffer[2].re := buffer[2].re-b/(3*a);
  end
 else
 if (delta = 0) then
  begin
   buffer[0].re := sqrt(-p/3);
   buffer[0].im := 0;
   if (buffer[0].re <> 0) then
    Deuxiemedegre(1,buffer[0].re,-q/buffer[0].re,pbuffer(@buffer[1]))
   else
    Deuxiemedegre(1,0,p,pbuffer(@buffer[1]));

   buffer[0].re := buffer[0].re-b/(3*a);
   buffer[1].re := buffer[1].re-b/(3*a);
   buffer[2].re := buffer[2].re-b/(3*a);
  end
 else
  begin
//   r := sqrt(-p/3);
   arg := -q/(2*sqrt(-p*p*p/27));
   t := arccos(arg);
   for i:=0 to 2 do
    begin
     buffer[i].re := 2*sqrt(-p/3)*cos((t+2*i*PI)/3.0)-b/(3*a);
     buffer[i].im := 0;
    end;
  end;
 result:= 3;
end;

{ ************************************************************************************************************************************************** }
// Resoud une �quation du quatri�me degr�, renvoye le nombre de racines
// M�thode de Ferrari
function Quatriemedegre(a,b,c,d,e:double;var buffer:pbuffer):integer;
var
 Aa,Bb,Cc:double;
 Z:double;
 u:double;
 x,y:double;
 i:integer;
 sol3:array[0..3] of tcomplexe;
 sol2:array[0..2] of tcomplexe;
begin
 if (a = 0) then
  begin
   result:=Troisiemedegre(b,c,d,e,buffer);
   exit;
  end;

 Aa := (-3*b*b)/(8*a*a) + c/a;
 Bb := (b*b*b)/(8*a*a*a) - (b*c)/(2*a*a) + d/a;
 Cc := -3*(b*b*b*b)/(256*a*a*a*a) + (c*b*b)/(16*a*a*a) - (d*b)/(4*a*a) +e/a;

 if (Bb = 0) then
  begin
   Deuxiemedegre(1,Aa,Cc,@sol2);
   for i:=0 to 3 do
    begin
     x := sol2[i mod 2].re;
     y := sol2[i mod 2].im;

     buffer[i].re := sqrt(2*(sqrt(x*x + y*y)+x))/2*(1-(i/2)*2) - b/(4*a);
     buffer[i].im := sqrt(2*(sqrt(x*x + y*y)-x))/2*(1-(i/2)*2);
     if (y<0) then buffer[i].im := -buffer[i].im;
    end
  end
 else
  begin
   Troisiemedegre(1,-Aa,-4*Cc,4*Aa*Cc-Bb*Bb,@sol3);
   u:=sol3[0].re;
   for i:=0 to 2 do if (sol3[i].im = 0) and (sol3[i].re>u) then u := sol3[i].re;

   Z := Bb/(2*(u-Aa));

   Deuxiemedegre(1,-sqrt(u-Aa),u/2 + Z*sqrt(u-Aa),buffer);
   Deuxiemedegre(1, sqrt(u-Aa),u/2 - Z*sqrt(u-Aa),pbuffer(@buffer[2]));

  //on fait -4b/a pour chaque solution pour avoir les x
  for i:=0 to 3 do buffer[i].re := buffer[i].re -b/(4*a);
 end;
 result:=4;
end;
{ ************************************************************************************************************************************************** }
function CalculeValeurIntermedaireTableau(EMin : Real; EMax : Real; SMin : Real; SMax : Real; EReel : Real) : Real;
Var
  DeltaE : Real;
  DeltaS : Real;
  DeltaReel : Real;
  Coefficient : Real;
begin
  DeltaE := EMax - EMin;
  DeltaS := SMax - SMin;
  DeltaReel := EReel - EMin;
  Coefficient := DeltaReel / DeltaE;
  Result := SMin + (Coefficient * DeltaS);
end;
{ ************************************************************************************************************************************************** }
Function BbsCopyFile(_Source, _Destination: String): Boolean;
Var
  fos : TSHFileOpStruct;
Begin
  Result := False;
  If DirectoryExists(ExtractFilePath(_Destination)) Then Begin
    FillChar(fos, SizeOf(fos), 0);
    With fos Do Begin
      wFunc := FO_COPY;
      pFrom := PChar(_Source + #0);
      pTO   := PChar(_Destination + #0);
      fFlags:= FOF_ALLOWUNDO Or FOF_NOCONFIRMATION Or FOF_SILENT;
    End;
    Result := (0 = ShFileOperation(fos));
  End;
End;
{ ************************************************************************************************************************************************** }
Function IntSumTab(_Tab: Array Of Integer; _Low: Integer = -1; _High: Integer = -1): Integer;
Var
  m_No  : Integer;
  m_Low : Integer;
  m_High: Integer;
  m_Sum : Integer;
Begin
  m_Sum := 0;
  If _Low = - 1 Then m_Low := Low(_Tab)
                Else m_Low := _Low;
  If _High = - 1 Then m_High := High(_Tab) - 1
                 Else m_High := _High;

  For m_No := m_Low To m_High Do m_Sum := m_Sum + _Tab[m_No];
  Result := m_Sum;
End;
{ ************************************************************************************************************************************************** }
Function IsNil(_Obj: TObject): Boolean;
Begin
  Result := _Obj = Nil;
End;
{ ************************************************************************************************************************************************** }
Function IsNotNil(_Obj: TObject): Boolean;
Begin
  Result := _Obj <> Nil;
End;
{ ************************************************************************************************************************************************** }
Function UsedList(_List: TList): Boolean;
Begin
  Result := IsNotNil(_List) And (_List.Count > 0);
End;
{ ************************************************************************************************************************************************** }
{ ************************************************************************************************************************************************** }
{ Convertit une chaine en entier }
{ ************************************************************************************************************************************************** }
Function StringToInt(Const S: String; Def: Integer): Integer;
Var
  i : Integer;
Begin
  Val(S, Result, i);
  If i <> 0 Then
    Result := Def;
End;
{ ************************************************************************************************************************************************** }
Function Str2Int(Const S: String): Integer;
Var
  i : Integer;
Begin
  Val(S, Result, i);
  If i <> 0 Then Result := 0;
End;
{ ************************************************************************************************************************************************** }
Function Int2Bool(Const I: Integer): Boolean;
Begin
  Result := I = 1;
End;
{ ************************************************************************************************************************************************** }
Function Bool2Int(Const I: Boolean): Integer;
Begin
  If I Then Result := 1
       Else Result := 0;
End;
{ ************************************************************************************************************************************************** }
Function Str2Float(Const S: String): Extended;
Var
  Str: String;
Begin
  // Conversion des ',' en '.'
  Str := AnsiReplaceStr(S, ',', '.');
  If Not TextToFloat(PChar(Str), Result, fvExtended) Then Result := 0;
End;
{ ************************************************************************************************************************************************** }
Function StringToFloat(Const S: String; Def: Extended): Extended;
Var
  IndPoint : SmallInt;
  Ch       : String;
Begin
  Ch := S;
  IndPoint := Pos(',', Ch);
  If IndPoint > 0 Then Ch[IndPoint] := '.';
  If Not TextToFloat(PChar(Ch), Result, fvExtended) Then
    Result := Def;
End;
{ ************************************************************************************************************************************************** }
Function Float2Str(F: Real; Decimales: Smallint) : String;
Begin
  Result := FloatToStrF(F, ffFixed, 99, Decimales);
End;
{ ************************************************************************************************************************************************** }
{ R�pertoire temporaire de windows }
Function TempRep: String;
Var
  TempDir : Array[0..MAX_PATH] Of AnsiChar;
  nSize   : DWord;
Begin
  nSize := SizeOf(TempDir);
  GetTempPathA(nSize, @TempDir);
  result := TempDir;
End;
{ ************************************************************************************************************************************************** }
Function GetSubStr(Chaine: WideString; Position: Integer; Separateur: String): String;
Var
  I, Indice       : Integer;
  StrTemp, SubStr : String;
Begin
  If Chaine <> '' Then Begin
    StrTemp := Chaine;
    If StrTemp[Length(StrTemp)] <> Separateur Then
      StrTemp := StrTemp + Separateur;
    For i := 1 To Position Do
    Begin
      Indice := Pos(Separateur, StrTemp);
      SubStr := Copy(StrTemp, 0, Indice - 1);
      StrTemp := Copy(StrTemp, Indice + 1, Length(StrTemp));
    End;
    GetSubstr := SubStr;
  End Else Result := '';
End;
{ ************************************************************************************************************************************************** }
Function StrToStringList(Chaine: WideString; Separateur: String): TStringList;
Var
  Indice  : Integer;
  Lst     : TStringList;
  StrTemp: String;
Begin
  StrTemp := Chaine;
  Lst := TStringList.Create;
  If StrTemp <> '' Then Begin
    If Chaine[Length(StrTemp)] <> Separateur Then StrTemp := StrTemp + Separateur;
    While Pos(Separateur, StrTemp) <> 0 Do Begin
      Indice := Pos(Separateur, StrTemp);
      Lst.Add(Copy(StrTemp, 0, Indice - 1));
      StrTemp := Copy(StrTemp, Indice + 1, Length(StrTemp));
    End;
  End;
  Result := Lst;
End;
{ ************************************************************************************************************************************************** }
Function Bool2Str(B: Boolean): String;
Begin
  If B Then Bool2Str := 'TRUE'
       Else Bool2Str := 'FALSE';
End;
{ ************************************************************************************************************************************************** }
Function Str2Bool(Str: String): Boolean;
Begin
  If WithoutBlanks(Str) = 'TRUE' Then Str2Bool := True
                                 Else Str2Bool := False;
End;
{ ************************************************************************************************************************************************** }
Function WithoutBlanks(Str: String) : String;
Var
  len,s : integer;
Begin
  len := length(Str);
  s := len;
  While (s > 0) And (Str[s] = ' ') Do s := s - 1;
  If s > 0 Then WithoutBlanks := Copy(Str, 1, s)
           Else WithoutBlanks := '';
End;
{ ********************************************************************************************************************************************* }
Function Str2GUID(Str: String): TGUID;
Begin
  Str2GUID := StringToGUID(WithoutBlanks(Str));
End;
{ ************************************************************************************************************************************************** }
Procedure ThemingForm(_Component : TComponent; _IDTheme : Integer; _GererPanel : Boolean = True);
Var
        i:Integer;
        UsedColor : TColor;
        UsedColorGradientFrom : TColor;
        UsedColorGradientTo : TColor;
        UsedColorTextToolBar : TColor;
Const
        Couleur1 = $00E0C7AD;
        CouleurGradientFrom1 = $00FCE1CB;
        CouleurGradientTo1 = $00E0A57D;
        CouleurTextTooleBar1 = ClWhite;

        Couleur2 = ClWhite;
        CouleurGradientFrom2 = $00CECECE;
        CouleurGradientTo2 = $00DBDBDB;
        CouleurTextTooleBar2 = ClBlack;

        Couleur3 = clMedGray;
        CouleurGradientFrom3 = $00CFF0EA;
        CouleurGradientTo3 = $008CC0B1;
        CouleurTextTooleBar3 = ClBlack;
Begin
        Case _IDTheme of
        c_BatimentCollectif :
                Begin
                UsedColor := Couleur1;
                UsedColorGradientFrom := CouleurGradientFrom1;
                UsedColorGradientTo := CouleurGradientTo1;
                UsedColorTextToolBar := CouleurTextTooleBar1;
                End;
        c_BatimentTertiaire :
                Begin
                UsedColor := Couleur2;
                UsedColorGradientFrom := CouleurGradientFrom2;
                UsedColorGradientTo := CouleurGradientTo2;
                UsedColorTextToolBar := CouleurTextTooleBar2;
                End;
        c_BatimentHotel :
                Begin
                UsedColor := Couleur3;
                UsedColorGradientFrom := CouleurGradientFrom3;
                UsedColorGradientTo := CouleurGradientTo3;
                UsedColorTextToolBar := CouleurTextTooleBar3;
                End;
        End;


        if (_Component is TForm) then
                Begin
                TForm(_Component).Color := UsedColor;
                End
        else
        If (_Component Is TPanel) and _GererPanel Then
                Begin
                TPanel(_Component).Color := UsedColor;
                End
        Else
        If _Component Is TLabel Then
                Begin
                End

        Else
        If _Component Is TPageControl Then
                Begin
                End

        Else
        If _Component Is TTabSheet Then
                Begin
                End

        Else
        If _Component Is TAdvToolBarOfficeStyler Then
                Begin
                TAdvToolBarOfficeStyler(_Component).Color.Color := UsedColorGradientFrom;
                TAdvToolBarOfficeStyler(_Component).Color.ColorTo := UsedColorGradientTo;
                TAdvToolBarOfficeStyler(_Component).CaptionAppearance.CaptionColor := UsedColor;
                TAdvToolBarOfficeStyler(_Component).CaptionAppearance.CaptionColorTo := UsedColor;
                TAdvToolBarOfficeStyler(_Component).CaptionAppearance.CaptionBorderColor := UsedColor;
                TAdvToolBarOfficeStyler(_Component).CaptionAppearance.CaptionTextColor := UsedColorTextToolBar;
                TAdvToolBarOfficeStyler(_Component).DockColor.Color := UsedColorGradientFrom;
                TAdvToolBarOfficeStyler(_Component).DockColor.ColorTo := UsedColorGradientTo;
                End

        Else
        If _Component Is TAdvDockPanel Then
                Begin
                End
        Else
        If _Component Is TVirtualStringTree Then
                Begin
                End

        Else
        If _Component Is TButton Then
                Begin
                End
        Else
        If _Component Is TComboBox Then
                Begin
                End
        Else
        If _Component Is TAdvStringGrid Then
                Begin
                TAdvStringGrid(_Component).ControlLook.FixedGradientFrom := UsedColorGradientFrom;
                TAdvStringGrid(_Component).ControlLook.FixedGradientTo := UsedColorGradientTo;
                End;


        For i:=0 To _Component.ComponentCount-1 Do
                ThemingForm(_Component.Components[i], _IDTheme, _GererPanel);



End;
{ ************************************************************************************************************************************************** }
Function GetChrDiam : String;
begin
  if Options.TypeEtiquette = e3Dm_Vectoriel then
    Result := Chr(Cst_CharDiamEtVct)
  else
  if Options.TypeEtiquette = e3Dm_Bitmap then
    Result := Chr(Cst_CharDiamEtImg);
end;
{ ************************************************************************************************************************************************** }
end.
