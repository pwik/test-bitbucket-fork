Unit Ventil_Colonne;
      
{$Include 'Ventil_Directives.pas'}

Interface
Uses
  Classes, SysUtils, advGrid, BaseGrid, AdvObj,
  XMLIntf,
  Ventil_TeSouche,
  BBScad_Interface_Aeraulique,
  Ventil_Types, Ventil_Troncon, Ventil_Logement, Ventil_Collecteur, Ventil_Bouche, Ventil_Utils, Ventil_Bifurcation;

Type
{ ************************************************************************************************************************************************** }
  TVentil_Colonne = Class(TVentil_TeSouche, IAero_Fredo_Colonne)
    Constructor Create; Override;
    Destructor Destroy; Override;
  Public
    PetitCoteRect         : Integer;
    GrandCoteRect         : Integer;
    IndiceLignePlenum     : Integer;
    IndiceColonnePlenum   : Integer;

    ColonneRectangulaire : Integer;

    class Function FamilleObjet : String; Override;
    Procedure ImportXMLActhys(_Node : IXMLNode); Override;
    Procedure ExportXMLActhys(_Node : IXMLNode); Override;
    Procedure ExportXMLActhys2ndPasse(_Node : IXMLNode); Override;
    Procedure AddEtagesDesservis(Var _ListEtages : TStringList);

    Procedure Affiche(_Grid: TAdvStringGrid); Override;
    Procedure AfficheResultats(_Grid: TAdvStringGrid); Override;
    Procedure Recupere(_Grid: TAdvStringGrid; _Ligne: Integer); Override;
    Function  TypeEdition(_Grid: TAdvStringGrid; _Ligne: Integer): TEditorType; Override;
    Procedure PropEdition(_Grid: TAdvStringGrid; _Ligne: Integer); Override;
    Function  HasComboBox(_Ligne: Integer): Boolean; Override;
    Function  AfficheQuestion(_NoQuestion: Integer): Boolean; Override;
    Function  EllipsClick(_Grid: TAdvStringGrid; _Ligne: Integer): String; Override;
    Function Get_TauxFoisonnement : Real; Override;
    Procedure Paste(_Source: TVentil_Objet); Override;
    Procedure Load; Override;


  Protected
    class Function NomParDefaut : String; Override;
    Procedure IAero_Fredo_Base_RemplacerPar(Objet : IAero_Fredo_Base); Override;
    Function  ImageAssociee: String; Override;
    Procedure GenerationAccidents; Override;
    Procedure Save; Override;
    Procedure InitEtiquette; Override;
    Function IsEtiquetteVisible : boolean; override;
    Procedure CalculerQuantitatifInternal; Override;
    function CanAddTrappePiedColonne : Boolean; Override;
    Function GetDiametre: Integer; Override;
    Procedure SetDiametre(_Value: Integer); Override;
  Private
    FTypeColonne : Integer;
    Procedure SetTypeColonne(_Value : Integer);
    Procedure AddAccidentShunt(_Porteur: TVentil_Troncon);
    Procedure AddAccidentBrRectligine(_Porteur: TVentil_Troncon);
    Procedure AddAccidentBrRectligineShunt(_Porteur: TVentil_Troncon);
    Procedure AddAccidentBrRectligineAlsace(_Porteur: TVentil_Troncon);
    Procedure AffecterTexteEtiquette;
  Public
    Property TypeColonne : Integer read FTypeColonne write SetTypeColonne;
  End;
{ ************************************************************************************************************************************************** }

Implementation
Uses
  Grids,
  BbsgFonc,
{$IFDEF ACTHYS_DIMVMBP}
  ImportEVenthysXSD,
  ExportEVenthysXSD,
{$ENDIF}
  Ventil_Plenum,
  Ventil_TourellePlenum,
  Ventil_Firebird,
  Ventil_EdibatecProduits,
  Ventil_Edibatec,
  Ventil_Const, Ventil_Accident, Ventil_Caisson,
  Ventil_Declarations, Ventil_Options,
  Ventil_SaisieMemo,
  Math, DB, Ventil_Etude;

{ ******************************************************************* TVentil_Colonne ***************************************************************** }
Constructor TVentil_Colonne.Create;
Begin
  Inherited;
  NbQuestions := NbQuestions + c_NbQ_TeSouche + c_NbQ_Colonne;
  ColonneRectangulaire := c_Non;
  PetitCoteRect := 200;
  GrandCoteRect := 300;
  IndiceLignePlenum := 0;
  IndiceColonnePlenum := 0;
  TypeColonne := c_TypeColonneVMC;
End;
{ ************************************************************************************************************************************************** }
class Function TVentil_Colonne.NomParDefaut : String;
Begin
Result := 'Colonne';
End;
{ ************************************************************************************************************************************************** }
class Function TVentil_Colonne.FamilleObjet : String;
Begin
Result := 'Colonne';
End;
{ ************************************************************************************************************************************************** }
Destructor TVentil_Colonne.Destroy;
Begin
  FreeAndNil(TexteLibre);
  Inherited;
End;
{ ************************************************************** ************************************************************************************ }
Function TVentil_Colonne.ImageAssociee: String;
Begin
  Result := 'TESOUCHE.jpg';
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Colonne.GenerationAccidents;
Var
  m_NoTr : Integer;
Begin
  Inherited;

  For m_NoTr := 0 To FilsCalcul.Count - 1 Do
    if FilsCalcul[m_NoTr].CanAddAccidentAmont then
    begin
        If m_NoTr > 0 Then
                Begin
                  case TypeColonne of
                    c_TypeColonneVMC                      : AddAccidentBrRectligine(FilsCalcul[m_NoTr]);
//                    c_TypeColonneShunt                    : AddAccidentShunt(FilsCalcul[m_NoTr]);
                    c_TypeColonneShunt                    : Case NatureConduit of
                                                              c_NatureConduitMaconne    : AddAccidentBrRectligineShunt(FilsCalcul[m_NoTr]);
                                                              c_NatureConduitMetallique : AddAccidentBrRectligine(FilsCalcul[m_NoTr]);
                                                            End;
                    c_TypeColonneConduitIndividuel        : AddAccidentBrRectligine(FilsCalcul[m_NoTr]);
                    c_TypeColonneAlsace                   : AddAccidentBrRectligineAlsace(FilsCalcul[m_NoTr]);
                    c_TypeColonneRamon                    : AddAccidentBrRectligine(FilsCalcul[m_NoTr]);
                  end;

                End
        Else
                Begin
                //AddAccidentBrRectligine(FilsCalcul[m_NoTr]);
                End;
    end;

End;
{ ************************************************************************************************************************************************** }
function TVentil_Colonne.CanAddTrappePiedColonne : Boolean;
begin
  Result := False;
end;
{ ************************************************************************************************************************************************** }
Function TVentil_Colonne.GetDiametre: Integer;
begin
  if (Parent <> Nil) and Parent.inheritsfrom(TVentil_TourellePlenum) then
    Result := 225
  else
    Result := Inherited;
end;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Colonne.SetDiametre(_Value: Integer);
begin
  if (Parent <> Nil) and Parent.inheritsfrom(TVentil_TourellePlenum) then
    begin
    FDiametre := 225;
    Vitesse := DebitMaxi / FDiametre / FDiametre *
        353.67765132 { 3600*1000*2*1000*2/Pi};
    end
  else
    Inherited;

end;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Colonne.SetTypeColonne(_Value : Integer);
Var
  OldValue : Integer;
begin

  OldValue := FTypeColonne;
  FTypeColonne := _Value;
{
  if (Parent <> Nil) then
    begin
      case FTypeColonne of
        c_TypeColonneVMC                      : LongGaine := 0.2;
        c_TypeColonneShunt                    : LongGaine := 2;
        c_TypeColonneConduitIndividuel        : LongGaine := 0;
        c_TypeColonneAlsace                   : LongGaine := 0.2;
        c_TypeColonneRamon                    : LongGaine := 0;
      end;
    end;
}

  if FTypeColonne <> OldValue then
    begin
      if Etude.Batiment.IsFabricantMVN then
        begin
          case FTypeColonne of
            c_TypeColonneVMC                      : Begin
//                                                    PetitCoteRect := 0;
//                                                    GrandCoteRect := 0;
                                                    End;
            c_TypeColonneShunt                    : Begin
                                                    PetitCoteRect := 200;
                                                    GrandCoteRect := 200;
                                                    End;
            c_TypeColonneConduitIndividuel        : Begin
                                                    PetitCoteRect := 125;
                                                    GrandCoteRect := 125;
                                                    End;
            c_TypeColonneAlsace                   : Begin
                                                    PetitCoteRect := 200;
                                                    GrandCoteRect := 200;
                                                    End;
            c_TypeColonneRamon                    : Begin
                                                    PetitCoteRect := 125;
                                                    GrandCoteRect := 200;
                                                    End;
          end;
        end;
//        if GetParentPlenum <> Nil then
//          TVentil_Plenum(GetParentPlenum).AutoCalculeDimension;
    InterfaceCAD.Aero_Vilo2Fredo_Refresh(rtColors);

    end;


end;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Colonne.AddAccidentBrRectligine(_Porteur: TVentil_Troncon);
Var
  m_Accident : TVentil_Accident;
Begin
  m_Accident := TVentil_Accident_TeSoucheBRect.Create;
  m_Accident.TronconPorteur := _Porteur;
  _Porteur.Accidents.Add(m_Accident);
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Colonne.AddAccidentBrRectligineShunt(_Porteur: TVentil_Troncon);
Var
  m_Accident : TVentil_Accident;
Begin
  m_Accident := TVentil_Accident_TeSoucheBRectShunt.Create;
  m_Accident.TronconPorteur := _Porteur;
  _Porteur.Accidents.Add(m_Accident);
end;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Colonne.AddAccidentBrRectligineAlsace(_Porteur: TVentil_Troncon);
Var
  m_Accident : TVentil_Accident;
Begin
  m_Accident := TVentil_Accident_TeSoucheBRectAlsace.Create;
  m_Accident.TronconPorteur := _Porteur;
  _Porteur.Accidents.Add(m_Accident);
end;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Colonne.AddAccidentShunt(_Porteur: TVentil_Troncon);
Var
  m_Accident : TVentil_Accident;
Begin
  m_Accident := TVentil_Accident_TeSouchePlenumColonneShuntPiquage.Create;
  m_Accident.TronconPorteur := _Porteur;
  _Porteur.Accidents.Add(m_Accident);
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Colonne.AffecterTexteEtiquette;
Begin
  FEtiquette := GetChrDiam + IntToStr(Diametre);
if (Etude.Batiment.IsFabricantVIM) then
  begin
  FEtiquette := Reference;//chr(Cst_CharDiam) + IntToStr(Diametre);
  end
else
  begin
  FEtiquette := Reference;
  //if Options.AffEtiq_GaineReseauV = false then
  FEtiquette := FEtiquette + #13 + GetChrDiam + IntToStr(Diametre);
  FEtiquette := FEtiquette + #13 + Float2Str(DebitMaxi, 0) + ' m3/h';
  end;

  if LienCad <> nil then
        LienCad.IAero_Vilo_Base_SetTextEtiquette(FEtiquette, Options.TypeEtiquette)

End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Colonne.InitEtiquette;
Begin
  AffecterTexteEtiquette;
  Inherited;
End;
{ ************************************************************************************************************************************************** }
Function TVentil_Colonne.IsEtiquetteVisible : boolean;
Begin
Result := Options.AffEtiq_ColonneSsPlenum;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Colonne.CalculerQuantitatifInternal;
begin
  Inherited;
end;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Colonne.Save;
Begin
  Table_Etude_Colonne.Donnees.Insert;
  Table_Etude_Colonne.Donnees.FieldByName('ID_COLONNE').AsWideString := GUIDToString(Id);
  {$IfDef AERAU_CLIMAWIN}
  Table_Etude_Colonne.Donnees.FieldByName('IDRESEAU').AsWideString := Etude.IdReseau;
  {$EndIf}
  Table_Etude_Colonne.Donnees.FieldByName('INSONORISE').AsInteger := Insonorise;
  Table_Etude_Colonne.Donnees.FieldByName('TEXTELIBRE').AsWideString  := TexteLibre.Text;
  Table_Etude_Colonne.Donnees.FieldByName('COLONNERECTANGULAIRE').AsInteger := ColonneRectangulaire;
  Table_Etude_Colonne.Donnees.FieldByName('TYPECOLONNE').AsInteger := TypeColonne;
  Table_Etude_Colonne.Donnees.FieldByName('NATURECONDUIT').AsInteger := NatureConduit;
  Table_Etude_Colonne.Donnees.FieldByName('PETITCOTERECT').AsInteger := PetitCoteRect;
  Table_Etude_Colonne.Donnees.FieldByName('GRANDCOTERECT').AsInteger := GrandCoteRect;
  Table_Etude_Colonne.Donnees.FieldByName('LIGNEPLENUM').AsInteger := IndiceLignePlenum;
  Table_Etude_Colonne.Donnees.FieldByName('COLONNEPLENUM').AsInteger := IndiceColonnePlenum;
  Inherited;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Colonne.ImportXMLActhys(_Node : IXMLNode);
Var
  Dim1 : Integer;
  Dim2 : Integer;
begin
  Inherited;
    if _Node = Nil then
      Exit;
{$IFDEF ACTHYS_DIMVMBP}
  Reference := _Node.ChildValues['Ref_Colonne'];
    Case _Node.ChildValues['Type_Conduit'] of
      1 : TypeColonne := c_TypeColonneConduitIndividuel;
      2 : TypeColonne := c_TypeColonneShunt;
      3 : TypeColonne := c_TypeColonneAlsace;
      4 : TypeColonne := c_TypeColonneVMC;
//      5 : TypeColonne := c_TypeColonneRamon;
    else
      TypeColonne := c_TypeColonneShunt;
    End;

  Dim1 :=  _Node.ChildValues['Dim1'] * 10;
  Dim2 :=  _Node.ChildValues['Dim2'] * 10;
  PetitCoteRect :=  Min(Dim1, Dim2);
  GrandCoteRect :=  Max(Dim1, Dim2);

    Case _Node.ChildValues['Forme_Conduit'] of
      1 : ColonneRectangulaire := C_Oui;
      2 : ColonneRectangulaire := C_Non;
    End;

    IndiceLignePlenum := _Node.ChildValues['Indice_Ligne_Plenum'];
    IndiceColonnePlenum := _Node.ChildValues['Indice_Colonne_Plenum'];

{$ENDIF}
end;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Colonne.ExportXMLActhys(_Node : IXMLNode);
var
  cpt : Integer;
begin
//Surtout pas de inherited pour l'export
//  Inherited;
{$IFDEF ACTHYS_DIMVMBP}

  (_Node as ExportEVenthysXSD.IXMLCOLONNE).Ref_Colonne := Reference;

    Case TypeColonne of
      c_TypeColonneConduitIndividuel : (_Node as ExportEVenthysXSD.IXMLCOLONNE).Type_Conduit := 1;
      c_TypeColonneShunt : (_Node as ExportEVenthysXSD.IXMLCOLONNE).Type_Conduit := 2;
      c_TypeColonneAlsace : (_Node as ExportEVenthysXSD.IXMLCOLONNE).Type_Conduit := 3;
      c_TypeColonneVMC : (_Node as ExportEVenthysXSD.IXMLCOLONNE).Type_Conduit := 4;
      c_TypeColonneRamon : (_Node as ExportEVenthysXSD.IXMLCOLONNE).Type_Conduit := 5;
    End;


    Case NatureConduit of
      c_NatureConduitMaconne : (_Node as ExportEVenthysXSD.IXMLCOLONNE).Nature_Conduit := 1;
      c_NatureConduitMetallique : (_Node as ExportEVenthysXSD.IXMLCOLONNE).Nature_Conduit := 2;
    End;

  (_Node as ExportEVenthysXSD.IXMLCOLONNE).Nb_Etages_Desservis := FilsCalcul.Count;

  (_Node as ExportEVenthysXSD.IXMLCOLONNE).Diametre_Fixe := Bool2XML(IndiceDiametre <> c_Diam_Calc);

    case ColonneRectangulaire of
      c_Oui : begin
              (_Node as ExportEVenthysXSD.IXMLCOLONNE).Forme_Conduit := 1;
              (_Node as ExportEVenthysXSD.IXMLCOLONNE).Dim1 := Round(PetitCoteRect / 10);
              (_Node as ExportEVenthysXSD.IXMLCOLONNE).Dim2 := Round(GrandCoteRect / 10);
              end;
      c_Non : begin
              (_Node as ExportEVenthysXSD.IXMLCOLONNE).Forme_Conduit := 2;
              (_Node as ExportEVenthysXSD.IXMLCOLONNE).Dim1 := Round(Diametre / 10);
              (_Node as ExportEVenthysXSD.IXMLCOLONNE).Dim2 := Round(Diametre / 10);
              end;
    end;

  (_Node as ExportEVenthysXSD.IXMLCOLONNE).Indice_Ligne_Plenum := IndiceLignePlenum;
  (_Node as ExportEVenthysXSD.IXMLCOLONNE).Indice_Colonne_Plenum := IndiceColonnePlenum;

    for cpt := 0 to FilsCalcul.Count - 1 do
      FilsCalcul[cpt].ExportXMLActhys((_Node as ExportEVenthysXSD.IXMLCOLONNE).ETAGES.Add);
{$ENDIF}
end;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Colonne.ExportXMLActhys2ndPasse(_Node : IXMLNode);
var
  cpt : Integer;
begin
//Surtout pas de inherited pour l'export
//  Inherited;
{$IFDEF ACTHYS_DIMVMBP}

    for cpt := 0 to FilsCalcul.Count - 1 do
      FilsCalcul[cpt].ExportXMLActhys2ndPasse((_Node as ExportEVenthysXSD.IXMLCOLONNE).ETAGES.ETAGE[cpt]);
{$ENDIF}
end;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Colonne.AddEtagesDesservis(Var _ListEtages : TStringList);
Var
  m_NoTr : Integer;
  tempEtage : String;
Begin
  For m_NoTr := 0 To FilsCalcul.Count - 1 Do
    begin
    tempEtage := IntToStr((FilsCalcul[m_NoTr].LienCad As IAero_Vilo_Base).IAero_Vilo_Base_Etage);
      if _ListEtages.IndexOf(tempEtage) = -1 then
        _ListEtages.Add(tempEtage);
    end;
end;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Colonne.Load;
Begin
  Insonorise      := Table_Etude_Colonne.Donnees.FieldByName('INSONORISE').AsInteger;
  TexteLibre.Text := Table_Etude_Colonne.Donnees.FieldByName('TEXTELIBRE').AsWideString;

  TypeColonne   := Table_Etude_Colonne.Donnees.FieldByName('TYPECOLONNE').AsInteger;
  NatureConduit   := Table_Etude_Colonne.Donnees.FieldByName('NATURECONDUIT').AsInteger;
  ColonneRectangulaire   := Table_Etude_Colonne.Donnees.FieldByName('COLONNERECTANGULAIRE').AsInteger;
  PetitCoteRect   := Table_Etude_Colonne.Donnees.FieldByName('PETITCOTERECT').AsInteger;
  GrandCoteRect   := Table_Etude_Colonne.Donnees.FieldByName('GRANDCOTERECT').AsInteger;
  IndiceLignePlenum   := Table_Etude_Colonne.Donnees.FieldByName('LIGNEPLENUM').AsInteger;
  IndiceColonnePlenum   := Table_Etude_Colonne.Donnees.FieldByName('COLONNEPLENUM').AsInteger;

  Inherited;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Colonne.Paste(_Source: TVentil_Objet);
Begin
  Inherited;
  TypeColonne           := TVentil_Colonne(_Source).TypeColonne;
  NatureConduit         := TVentil_Colonne(_Source).NatureConduit;
  TexteLibre.Text       := TVentil_Colonne(_Source).TexteLibre.Text;
  Insonorise            := TVentil_Colonne(_Source).Insonorise;
  ColonneRectangulaire  := TVentil_Colonne(_Source).ColonneRectangulaire;
  PetitCoteRect         := TVentil_Colonne(_Source).PetitCoteRect;
  GrandCoteRect         := TVentil_Colonne(_Source).GrandCoteRect;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Colonne.IAero_Fredo_Base_RemplacerPar(Objet : IAero_Fredo_Base);
Begin
inherited;
TVentil_Colonne(Objet.IAero_Fredo_Base_Objet).TexteLibre.Text := TexteLibre.Text;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Colonne.Affiche(_Grid: TAdvStringGrid);
Var
  m_Row      : Integer;
  m_Question : Integer;
  m_Str      : String;
Begin
  Inherited;
  m_Row := GetNbQuestionAfficheTroncon + GetNbQuestionAfficheTeSouche  + 1;
  For m_Question := Low(c_LibellesQ_Colonne) To High(c_LibellesQ_Colonne) Do If AfficheQuestion(m_Question) Then Begin
    _Grid.Cells[0, m_Row] := c_LibellesQ_Colonne[m_Question];
    Case m_Question Of
      c_QColonne_TypeCol : m_Str := c_TypeColonne[TypeColonne];
      c_QColonne_ColonneRectangulaire : m_Str := c_OuiNon[ColonneRectangulaire];
      c_QColonne_PetitCoteRect : m_Str := IntToStr(PetitCoteRect);
      c_QColonne_GrandCoteRect : m_Str := IntToStr(GrandCoteRect);
      Else m_Str := '';
    End;
    _Grid.Cells[1, m_Row] := m_Str;
    Inc(m_Row);
  End;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Colonne.AfficheResultats(_Grid: TAdvStringGrid);
begin
  inherited;
  If Etude.DTU2014 and Not(Etude.Batiment.IsFabricantMVN) Then Begin
    _Grid.Cells[0, 1] := 'Coef. Fois.';
    _Grid.Cells[1, 1] := Float2Str(TauxFoisonnement, 2);
  End;
end;
{ ************************************************************************************************************************************************** }
Function TVentil_Colonne.AfficheQuestion(_NoQuestion: Integer): Boolean;
Begin
  If _NoQuestion <= c_NbQ_Troncon + c_NbQ_TeSouche Then Result := Inherited AfficheQuestion(_NoQuestion)
  Else Case _NoQuestion Of
    c_QColonne_TypeCol  : Result := Etude.Batiment.IsFabricantMVN or Etude.Batiment.IsFabricantACT;
    c_QColonne_ColonneRectangulaire  : Result := Etude.Batiment.IsFabricantMVN or Etude.Batiment.IsFabricantACT;
    c_QColonne_PetitCoteRect  : Result := ColonneRectangulaire = c_Oui;
    c_QColonne_GrandCoteRect  : Result := ColonneRectangulaire = c_Oui;
    Else Result := False;
  End;

  if _NoQuestion = c_QTroncon_NatureGaine then
    Result := False;
  if _NoQuestion = c_QTroncon_Diametre then
    Result := not((Parent <> Nil) and Parent.inheritsfrom(TVentil_TourellePlenum));
End;
{ ************************************************************************************************************************************************** }
Function TVentil_Colonne.EllipsClick(_Grid: TAdvStringGrid; _Ligne: Integer): String;
Begin
inherited;
{
  Case NoQuestion(_Ligne) Of
  End;
}
//  Recupere(_Grid, _Ligne);

End;
{ ************************************************************************************************************************************************** }
Function TVentil_Colonne.Get_TauxFoisonnement : Real;
Begin
  if Etude.Batiment.IsFabricantACT then
    Result := Inherited
  else
  if Etude.Batiment.IsFabricantMVN then
    begin
      if GetParentPlenum <> nil then
        Result :=  TVentil_Plenum(GetParentPlenum).TauxFoisonnement
      else
        Result := Inherited;
    end;
End;
{ ************************************************************************************************************************************************** }
Function TVentil_Colonne.HasComboBox(_Ligne: Integer): Boolean;
Begin
  If NoQuestion(_Ligne) <= c_NbQ_Troncon + c_NbQ_TeSouche  Then Result := Inherited HasComboBox(_Ligne)
  Else Result := AfficheQuestion(NoQuestion(_Ligne)) And (NoQuestion(_Ligne) In[c_QColonne_TypeCol, c_QColonne_ColonneRectangulaire]);
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Colonne.Recupere(_Grid: TAdvStringGrid; _Ligne: Integer);
Begin
  If NoQuestion(_Ligne) <= c_NbQ_Troncon + c_NbQ_TeSouche  Then Inherited
  Else If AfficheQuestion(NoQuestion(_Ligne)) Then Case NoQuestion(_Ligne) Of
    c_QColonne_TypeCol   : TypeColonne := _Grid.Combobox.ItemIndex;
    c_QColonne_ColonneRectangulaire   : ColonneRectangulaire := _Grid.Combobox.ItemIndex;
    c_QColonne_PetitCoteRect   : PetitCoteRect := Min(_Grid.Ints[1, _Ligne], GrandCoteRect);
    c_QColonne_GrandCoteRect   : GrandCoteRect := Max(_Grid.Ints[1, _Ligne], PetitCoteRect);
  End;
  Inherited;
End;
{ ************************************************************************************************************************************************** }
Function TVentil_Colonne.TypeEdition(_Grid: TAdvStringGrid; _Ligne: Integer): TEditorType;
Var
  m_NoChoix : Integer;
Begin
  Result := edNone;
  If Etude.Batiment.ChiffrageVerouille  = c_OUI Then Exit;
    //Parfois le GoEditing saute sans raison...
  _Grid.Options := _Grid.Options + [goEditing];
  If NoQuestion(_Ligne) <= c_NbQ_Troncon + c_NbQ_TeSouche  Then Result := Inherited TypeEdition(_Grid, _Ligne)
  Else If AfficheQuestion(NoQuestion(_Ligne)) Then Case NoQuestion(_Ligne) Of
    c_QColonne_TypeCol  : Begin
      Result := edComboList;
    End;
    c_QColonne_ColonneRectangulaire  : Begin
      Result := edComboList;
    End;
    c_QColonne_PetitCoteRect : Result := edPositiveNumeric;
    c_QColonne_GrandCoteRect : Result := edPositiveNumeric;
  End;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Colonne.PropEdition(_Grid: TAdvStringGrid; _Ligne: Integer);
Var
  m_NoChoix : Integer;
Begin
  If Etude.Batiment.ChiffrageVerouille  = c_OUI Then Exit;
  If NoQuestion(_Ligne) <= c_NbQ_Troncon + c_NbQ_TeSouche  Then Inherited
  Else If AfficheQuestion(NoQuestion(_Ligne)) Then Case NoQuestion(_Ligne) Of
    c_QColonne_TypeCol  : Begin
      _Grid.ClearComboString;
      For m_NoChoix := Low(c_TypeColonne) To High(c_TypeColonne) Do _Grid.AddComboString(c_TypeColonne[m_NoChoix]);
    End;
    c_QColonne_ColonneRectangulaire  : Begin
      _Grid.ClearComboString;
      For m_NoChoix := Low(c_OUINON) To High(c_OUINON) Do _Grid.AddComboString(c_OUINON[m_NoChoix]);
    End;
  End;
End;
{ ******************************************************************* \ TVentil_Colonne *************************************************************** }

End.

