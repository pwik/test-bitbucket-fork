Unit Ventil_TourelleTete;
  
{$Include 'Ventil_Directives.pas'}

Interface
Uses
  Classes, SysUtils, advGrid, BaseGrid, AdvObj, Dialogs, Math,
  BBScad_Interface_Aeraulique,
  XMLDoc,
  XMLIntf,
  AccesBDD,
  Ventil_Const,
  Ventil_Logement,
  Ventil_ChiffrageTypes,
  Ventil_SelectionCaisson,
  Ventil_Caisson,
  Ventil_Types, Ventil_Troncon, Ventil_TronconDessin, Ventil_TeSouche, Ventil_EdibatecProduits, Ventil_Utils;

Type



{ ************************************************************************************************************************************************** }
  TVentil_TourelleTete = Class(TVentil_Caisson, IAero_Fredo_TourelleTete)
    Constructor Create; Override;
    Destructor Destroy; Override;
  Private
  Public
    class Function FamilleObjet : String; Override;

    Procedure UpdateDimensionsCaisson; Override;
    Procedure Load; Override;
    Function  AfficheQuestion(_NoQuestion: Integer): Boolean; Override;
    Function GetTypeSortieTourelleXMLVIM : Integer;
  Protected
    Procedure CalculerQuantitatifInternalCaisson; Override;
    Function  ImageAssociee: String; Override;
    function GetLibIDTableDeDonnees : String; Override;
    function GetTableDeDonnees : TTableDeDonneesBBS; Override;
    Procedure Save; Override;
    Function  InternalLienCADCaisson : IAero_Caisson_Base; Override;
    class Function NomParDefaut : String; Override;
  End;



{ ************************************************************************************************************************************************** }



Implementation
Uses
  BbsgTec,
  Ventil_TourelleBifurcation,
  DateUtils,
  Ventil_Firebird,
  Grids;

{ ******************************************************************* TVentil_TourelleTete ***************************************************************** }
Function TVentil_TourelleTete.GetTypeSortieTourelleXMLVIM : Integer;
begin
  if HasChildrenClass(tventil_TourelleBifurcation) then
    begin
      if GetFirstChildrenClass(tventil_TourelleBifurcation).HasParentClass(TVentil_TronconDessin_Conduit) then
        Result := c_TourelleTete_TypeSortieXMLVIM_PlenumIndirect
      else
        Result := c_TourelleTete_TypeSortieXMLVIM_PlenumDirect;
    end
  else
    Result := c_TourelleTete_TypeSortieXMLVIM_SansPlenum;
end;
{ ************************************************************************************************************************************************** }
Procedure TVentil_TourelleTete.CalculerQuantitatifInternalCaisson;
begin
    {$IFDEF  VIM_SELECTIONCAISSON}
    Inherited;
    {$ENDIF}
end;
{ ************************************************************************************************************************************************** }
Procedure TVentil_TourelleTete.UpdateDimensionsCaisson;
begin

end;
{ ************************************************************************************************************************************************** }
Procedure TVentil_TourelleTete.Load;
begin
  Inherited;
end;
{ ************************************************************************************************************************************************** }
Function  TVentil_TourelleTete.AfficheQuestion(_NoQuestion: Integer): Boolean;
begin
  Result := Inherited;
    if _NoQuestion in [c_QCaisson_Position, c_QCaisson_TypeCaisson, c_QCaisson_SaisieDimensions, c_QCaisson_DiametreExtract, c_QCaisson_DiametreRefoul] then
      Result := False;
end;
{ ************************************************************************************************************************************************** }
function TVentil_TourelleTete.GetLibIDTableDeDonnees : String;
begin
  Result := 'ID_TOURTETE';
end;
{ ************************************************************************************************************************************************** }
function TVentil_TourelleTete.GetTableDeDonnees : TTableDeDonneesBBS;
begin
  Result := Table_Etude_TourelleTete;
end;
{ ************************************************************************************************************************************************** }
Procedure TVentil_TourelleTete.Save;
Var
  m_Str : String;
  m_No : Integer;
begin
  Inherited;
  Table_Etude_TourelleTete.Donnees.Insert;
  Table_Etude_TourelleTete.Donnees.FieldByName('ID_TOURTETE').AsWideString := GUIDToString(Id);

  Table_Etude_Caisson.Donnees.FieldByName('EMPLACEMENT').AsInteger     := Position;
  Table_Etude_TourelleTete.Donnees.FieldByName('GAINESGAZ').AsInteger       := TypeGaineGaz;
  Table_Etude_TourelleTete.Donnees.FieldByName('ELEVATION').AsFloat         := Elevation;
  Table_Etude_TourelleTete.Donnees.FieldByName('TYPECAISSON').AsInteger     := TypeCaisson;
  Table_Etude_TourelleTete.Donnees.FieldByName('TYPERESEAU').AsInteger      := TypeReseau;
  Table_Etude_TourelleTete.Donnees.FieldByName('DIAMREFOUL').AsInteger      := DiametreRefoulement;
  Table_Etude_TourelleTete.Donnees.FieldByName('TEXTELIBRE').AsWideString       := TexteLibre.Text;
  Table_Etude_TourelleTete.Donnees.FieldByName('OBSTACLEREJET').AsInteger := PresenceObstacleRejet;

  try
  Table_Etude_TourelleTete.Donnees.FieldByName('SAISIEDIMENSIONS').AsInteger := SaisieDimensions;
  Table_Etude_TourelleTete.Donnees.FieldByName('LARGEUR').AsFloat          := Largeur;
  Table_Etude_TourelleTete.Donnees.FieldByName('LONGUEUR').AsFloat         := Longueur;
  Table_Etude_TourelleTete.Donnees.FieldByName('HAUTEUR').AsFloat          := Hauteur;
  except
  end;

  Table_Etude_TourelleTete.Donnees.FieldByName('FIXEPLAGEPRESSION').AsInteger := FixePlagePression;
  Table_Etude_TourelleTete.Donnees.FieldByName('PMINFIXE20').AsFloat         := PMinFixe[ttc20Deg];
  Table_Etude_TourelleTete.Donnees.FieldByName('PMAXFIXE20').AsFloat         := PMaxFixe[ttc20Deg];
  Table_Etude_TourelleTete.Donnees.FieldByName('PMINFIXE7').AsFloat          := PMinFixe[ttc7Deg];
  Table_Etude_TourelleTete.Donnees.FieldByName('PMAXFIXE7').AsFloat          := PMaxFixe[ttc7Deg];

  If IsNotNil(ProduitAssocie) Then m_Str := ProduitAssocie.CodeProduit
                              Else m_Str := '';
  Table_Etude_TourelleTete.Donnees.FieldByName('CAISSON_EDIBATEC').AsWideString := m_Str;

  Table_Etude_TourelleTete.Donnees.FieldByName('CHIFFRAGE_CAISSON').AsInteger := ChiffrageCaisson;

  If ProduitAssocie <> nil Then Begin
    Table_Etude_TourelleTete.Donnees.FieldByName('DP_CALC_QMAX').AsFloat := ProduitAssocie.DpCalcQMax;
    Table_Etude_TourelleTete.Donnees.FieldByName('DP_CALC_QMIN').AsFloat := ProduitAssocie.DpCalcQMin;
    Table_Etude_TourelleTete.Donnees.FieldByName('PUISS_CALC_QMAX').AsFloat := ProduitAssocie.PuissCalcQMax;
    Table_Etude_TourelleTete.Donnees.FieldByName('PUISS_CALC_QMIN').AsFloat := ProduitAssocie.PuissCalcQMin;
    Table_Etude_TourelleTete.Donnees.FieldByName('POSITION_VENTIL').AsWideString := ProduitAssocie.Position;
    Table_Etude_TourelleTete.Donnees.FieldByName('POSITION_CHOISIE').AsInteger := ProduitAssocie.PositionChoisie;
    Table_Etude_TourelleTete.Donnees.FieldByName('COEFF_RT').AsFloat := ProduitAssocie.CoeffRT;
    Table_Etude_TourelleTete.Donnees.FieldByName('PMOY').AsFloat := ProduitAssocie.PMoy;
    Table_Etude_TourelleTete.Donnees.FieldByName('QMOY').AsFloat := ProduitAssocie.QMoy;
    Table_Etude_TourelleTete.Donnees.FieldByName('QMAXRT').AsFloat := ProduitAssocie.QMaxRT;
    Table_Etude_TourelleTete.Donnees.FieldByName('QMINRT').AsFloat := ProduitAssocie.QMinRT;
    Table_Etude_TourelleTete.Donnees.FieldByName('PMAXRT').AsFloat := ProduitAssocie.PMaxRT;
    Table_Etude_TourelleTete.Donnees.FieldByName('PMINRT').AsFloat := ProduitAssocie.PMinRT;

    m_Str := '';
    if ProduitAssocie.CourbeDpCalc <> nil then
    For m_No := 0 To ProduitAssocie.CourbeDpCalc.Count - 1 Do m_Str := m_Str + ProduitAssocie.CourbeDpCalc[m_No] + '\';
    Table_Etude_TourelleTete.Donnees.FieldByName('COURBEDPCALC').AsWideString := m_Str;

    m_Str := '';
    if ProduitAssocie.CourbePuissCalc <> nil then
    For m_No := 0 To ProduitAssocie.CourbePuissCalc.Count - 1 Do m_Str := m_Str + ProduitAssocie.CourbePuissCalc[m_No] + '\';
    Table_Etude_TourelleTete.Donnees.FieldByName('COURBEPUISSCALC').AsWideString := m_Str;

    m_Str := '';
    if ProduitAssocie.CourbeDebitCalc <> nil then
    For m_No := 0 To ProduitAssocie.CourbeDebitCalc.Count - 1 Do m_Str := m_Str + ProduitAssocie.CourbeDebitCalc[m_No] + '\';
    Table_Etude_TourelleTete.Donnees.FieldByName('COURBEDEBITCALC').AsWideString := m_Str;

    m_Str := '';
    if ProduitAssocie.CourbeDebitCalcCourbeMontante <> nil then
    For m_No := 0 To ProduitAssocie.CourbeDebitCalcCourbeMontante.Count - 1 Do m_Str := m_Str + ProduitAssocie.CourbeDebitCalcCourbeMontante[m_No] + '\';
    Table_Etude_TourelleTete.Donnees.FieldByName('COURBEDEBITCALCCM').AsWideString := m_Str;

  End Else Begin
    Table_Etude_TourelleTete.Donnees.FieldByName('DP_CALC_QMAX').AsFloat := 0;
    Table_Etude_TourelleTete.Donnees.FieldByName('DP_CALC_QMIN').AsFloat := 0;
    Table_Etude_TourelleTete.Donnees.FieldByName('PUISS_CALC_QMAX').AsFloat := 0;
    Table_Etude_TourelleTete.Donnees.FieldByName('PUISS_CALC_QMIN').AsFloat := 0;
    Table_Etude_TourelleTete.Donnees.FieldByName('POSITION_VENTIL').AsWideString := '';
    Table_Etude_TourelleTete.Donnees.FieldByName('POSITION_CHOISIE').AsInteger := 0;
    Table_Etude_TourelleTete.Donnees.FieldByName('COEFF_RT').AsFloat := 0;
    Table_Etude_TourelleTete.Donnees.FieldByName('PMOY').AsFloat := 0;
    Table_Etude_TourelleTete.Donnees.FieldByName('QMOY').AsFloat := 0;
    Table_Etude_TourelleTete.Donnees.FieldByName('QMAXRT').AsFloat := 0;
    Table_Etude_TourelleTete.Donnees.FieldByName('QMINRT').AsFloat := 0;
    Table_Etude_TourelleTete.Donnees.FieldByName('PMAXRT').AsFloat := 0;
    Table_Etude_TourelleTete.Donnees.FieldByName('PMINRT').AsFloat := 0;
  End;

  m_Str := '';
  For m_No := 0 To Chiff_TYPE_VENTILATEUR.Count - 1 Do m_Str := m_Str + Chiff_TYPE_VENTILATEUR[m_No] + '\';
  Table_Etude_TourelleTete.Donnees.FieldByName('TYPE_VENTILATEUR').AsWideString := m_Str;

  m_Str := '';
  For m_No := 0 To Chiff_ENTRAINEMENT.Count - 1 Do m_Str := m_Str + Chiff_ENTRAINEMENT[m_No] + '\';
  Table_Etude_TourelleTete.Donnees.FieldByName('ENTRAINEMENT').AsWideString := m_Str;

  m_Str := '';
  For m_No := 0 To Chiff_VITESSE.Count - 1 Do m_Str := m_Str + Chiff_VITESSE[m_No] + '\';
  Table_Etude_TourelleTete.Donnees.FieldByName('VITESSE').AsWideString := m_Str;

  m_Str := '';
  For m_No := 0 To Chiff_AGREMENT.Count - 1 Do m_Str := m_Str + Chiff_AGREMENT[m_No] + '\';
  Table_Etude_TourelleTete.Donnees.FieldByName('AGREMENT').AsWideString := m_Str;

  m_Str := '';
  For m_No := 0 To Chiff_ALIMENTATION.Count - 1 Do m_Str := m_Str + Chiff_ALIMENTATION[m_No] + '\';
  Table_Etude_TourelleTete.Donnees.FieldByName('ALIMENTATION').AsWideString := m_Str;

  try
    if Table_Etude_TourelleTete.Donnees.FieldByName('VENTILECO').AsWideString <> '' then
      Table_Etude_TourelleTete.Donnees.FieldByName('VENTILECO').AsInteger := Chiff_VENTILECO;
    if Table_Etude_TourelleTete.Donnees.FieldByName('NECOADEPR').AsWideString <> '' then
      Table_Etude_TourelleTete.Donnees.FieldByName('NECOADEPR').AsInteger := Chiff_NECOADEPR;
    if Table_Etude_TourelleTete.Donnees.FieldByName('NECOSDEPR').AsWideString <> '' then
      Table_Etude_TourelleTete.Donnees.FieldByName('NECOSDEPR').AsInteger := Chiff_NECOSDEPR;
  except
  end;


//  Table_Etude_TourelleTete.Donnees.FieldByName('DEPRESSOSTAT').AsInteger := Chiff_DEPRESSOSTAT;
//  Table_Etude_TourelleTete.Donnees.FieldByName('VENTILECO').AsInteger := Chiff_VENTILECO;


    if SaveOptions_SelectionCaisson <> nil then
      SaveOptions_SelectionCaisson.Sauvegarder(Table_Etude_TourelleTete.Donnees);

    if Etude.Batiment.IsFabricantVIM then
      begin
      {$IFDEF  VIM_SELECTIONCAISSON}
      Table_Etude_TourelleTete.Donnees.FieldByName('FC_ListCaract').AsWideString := FicheCaisson_ListCaract.Save;
      Table_Etude_TourelleTete.Donnees.FieldByName('FC_LegCourbes').AsWideString := FicheCaisson_LegendesCourbes.Save;

      Table_Etude_TourelleTete.Donnees.FieldByName('NOMCOURT').AsWideString := NomCourt;

      Table_Etude_TourelleTete.Donnees.FieldByName('BR_63').AsFloat    := BR_63;
      Table_Etude_TourelleTete.Donnees.FieldByName('BR_125').AsFloat   := BR_125;
      Table_Etude_TourelleTete.Donnees.FieldByName('BR_250').AsFloat   := BR_250;
      Table_Etude_TourelleTete.Donnees.FieldByName('BR_500').AsFloat   := BR_500;
      Table_Etude_TourelleTete.Donnees.FieldByName('BR_1000').AsFloat  := BR_1000;
      Table_Etude_TourelleTete.Donnees.FieldByName('BR_2000').AsFloat  := BR_2000;
      Table_Etude_TourelleTete.Donnees.FieldByName('BR_4000').AsFloat  := BR_4000;
      Table_Etude_TourelleTete.Donnees.FieldByName('BR_8000').AsFloat  := BR_8000;

      Table_Etude_TourelleTete.Donnees.FieldByName(UpperCase('SFP')).AsInteger     := SFP;
      Table_Etude_TourelleTete.Donnees.FieldByName(UpperCase('CL_SFP')).AsInteger  := CL_SFP;

        if ProduitAssocie <> Nil then
          begin

            if XMLSelectVIM <> Nil then
              (Table_Etude_TourelleTete.Donnees.FieldByName('XMLSELECTVIM') As TBlobField).LoadFromStream(XMLSelectVIM);

          Table_Etude_TourelleTete.Donnees.FieldByName('CaisSel_CodeProduit').AsString := ProduitAssocie.CodeProduit;
          Table_Etude_TourelleTete.Donnees.FieldByName('CaisSel_CodeArticle').AsString := ProduitAssocie.CodeArticle;
          Table_Etude_TourelleTete.Donnees.FieldByName('CaisSel_Reference').AsString := ProduitAssocie.Reference;
          Table_Etude_TourelleTete.Donnees.FieldByName('CaisSel_Prix').AsFloat := ProduitAssocie.Prix;
          Table_Etude_TourelleTete.Donnees.FieldByName('CaisSel_Famille').AsString := ProduitAssocie.Famille;
          end
        else
          begin
          Table_Etude_TourelleTete.Donnees.FieldByName('CaisSel_CodeProduit').AsString := '';
          Table_Etude_TourelleTete.Donnees.FieldByName('CaisSel_CodeArticle').AsString := '';
          Table_Etude_TourelleTete.Donnees.FieldByName('CaisSel_Reference').AsString := '';
          Table_Etude_TourelleTete.Donnees.FieldByName('CaisSel_Prix').AsFloat := 0;
          Table_Etude_TourelleTete.Donnees.FieldByName('CaisSel_Famille').AsString := '';
          end;

      {$ENDIF}
      end;

{$IfNDef AERAU_CLIMAWIN}
  Chiffrages.Save;
{$EndIf}

  Inherited;
end;
{ ************************************************************************************************************************************************** }
Function  TVentil_TourelleTete.InternalLienCADCaisson : IAero_Caisson_Base;
begin
Result := (LienCad.IAero_Vilo_Base_Pere As IAero_EXT_Tourelle);
end;
{ ************************************************************************************************************************************************** }
Constructor TVentil_TourelleTete.Create;
Begin
  Inherited;
  SaisieDimensions := c_Non;
End;
{ ************************************************************************************************************************************************** }
class Function TVentil_TourelleTete.NomParDefaut : String;
Begin
Result := 'Tourelle';
End;
{ ************************************************************************************************************************************************** }
class Function TVentil_TourelleTete.FamilleObjet : String;
Begin
  Result := 'Tourelle';
End;
{ ************************************************************************************************************************************************** }
Function TVentil_TourelleTete.ImageAssociee: String;
Begin
    Result := 'TOURELLETETE.jpg';
End;
{ ************************************************************************************************************************************************** }
Destructor TVentil_TourelleTete.Destroy;
Begin
  Inherited;
End;
{ ******************************************************************* \TVentil_TourelleTete **************************************************************** }




End.
