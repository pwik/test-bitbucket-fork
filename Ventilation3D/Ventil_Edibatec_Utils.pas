Unit Ventil_Edibatec_Utils;
        
{$Include 'Ventil_Directives.pas'}

Interface

{ Nom d'une gamme }
Function NomGammeEdibatec(_CodeGamme: String): String;

Implementation
Uses
  Ventil_EdibatecProduits, Ventil_Declarations;

{ ************************************************************************************************************************************************* }
Function NomGammeEdibatec(_CodeGamme: String): String;
Var
  m_Gamme  : TEdibatec_Gamme;
Begin
  m_Gamme := BaseEdibatec.Gamme(_CodeGamme);
  If m_Gamme <> Nil Then Result := m_Gamme.Reference
                    Else Result := '';
End;
{ ************************************************************************************************************************************************* }
End.
