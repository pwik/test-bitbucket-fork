Unit Ventil_Etude;
 
{$Include 'Ventil_Directives.pas'}

Interface
Uses
  SysUtils, AdvGrid, BaseGrid, AdvObj, VirtualTrees, Classes, Registry, Windows, Forms, ExtCtrls, Controls,
  ComCtrls,
  XMLIntf,
  Ventil_Const,
  BBScad_Interface_Aeraulique,
  DB,
  Ventil_ChiffrageTypes, Ventil_EdibatecProduits, Ventil_Types, Ventil_Batiment, Ventil_Logement, Ventil_SystemeVMC, Ventil_Caisson, Ventil_Troncon,
  Ventil_SelectionCaisson;

Type
{ ************************************************************************************************************************************************** }
  TVentil_Etude = Class(TVentil_Objet)
    Constructor Create(_GridProp, _GridRes, _GridResReseau, _GridAcoust : TAdvStringGrid;  _TabAcoust: TTabSheet; _ProcPremierPlan: TBringToFront; _Image: TImage = nil;
                       _PageControl: TPageControl = nil; _TypeBat : Integer = c_BatimentCollectif; _GridSynthese207: TAdvStringGrid = Nil);  Reintroduce;
    Destructor  Destroy; Override;
 Private
   FDeltaCalculeMax              : Real;
   FDeltaCalculeMin              : Real;
   Function GetFDeltaCalculeMax : Real;
   Function GetFDeltaCalculeMin : Real;
   Procedure Analyse(_ElementCad : IAero_Vilo_Base; _Parent : TVentil_Troncon);
   Procedure Sauvegarder_DataExport;
   Procedure Vider_DataExport;
   Procedure ViderTables;
   Procedure OuvrirTables;
   Procedure SauverTables;
   Procedure FermerTables;
  Public
    VersionSave  : Integer;
    ModeIteratif : Boolean;
    FromCallBack : Boolean;
    IsAutoSaving : Boolean;
    AutoSaveAutorisee : Boolean;
    DTUActive     : Boolean;
    Image         : TImage;
    Fichier       : String;
    InternalFichier       : String;
    ToSave        : Boolean;
    Batiment      : TVentil_Batiment;
    GridProp      : TAdvStringGrid;
    GridRes       : TAdvStringGrid;
    GridResReseau : TAdvStringGrid;
    GridAcoust    : TAdvStringGrid;
    GridSynthese207 : TAdvStringGrid;
    TabAcoust     : TTabSheet;
    PageControl   : TPageControl;

    InfosPremierPlan      : TBringToFront;
    NouvellePageImpInutile: Boolean;
    SelectedObject        : TVentil_Objet;
    AfterUptadeTroncon    : TVentil_Troncon;

    NbRecalcul     : Integer;
    BesoinRecalcul : Boolean;
    Loading        : Boolean;
    WithTronconsGazHorizontal: Boolean;
    WithTronconsGazVertical: Boolean;
    ToutGazHorizontal: Boolean;
    ToutGazVertical: Boolean;

   { Param�tres g�n�raux de l'�tude }
    DateEtude    : TDate;
    Adresse      : String;
    Descriptif   : String;
    MOuvrage     : String;
    MOeuvre      : String;
    Installateur : String;
    Objet        : String;
    Remarques    : TStringList;
    RefDossier   : String;

    { Donn�es d'impression qui seront sauvegard�es dans la base pour VIM }
    DataImp_DonneesGenerale   : TStringList;
    DataImp_AvisTechniques    : TStringList;
    DataImp_DonneesEntreesAir : TStringList;
    DataImp_CalculRetour      : TStringList;
    DataImp_SchemasEtgs       : TList;
    DataImp_SchemasTout       : TList;
    DataImp_SchemasTerrasses  : TList;
    DataImp_SchemasColonnes   : TList;
    DataImp_Quantitatif       : TStringList;
    DataImp_Erreurs           : TStringList;
    DataImp_FicheCaisson      : TStringList;
    DataImp_CourbesCaisson    : TList;
    DataImp_ImagesCaisson     : TList;
    DataImp_Acoustique        : TStringList;

    IdReseau : String;

    //M�morisation des options de s�lection du caisson. Anciennement un seul caisson dans une �tude pour vim, maintenant sauvegarde dans la table caissons
    SaveOptions_SelectionCaissonVIM : TSelectionCaisson_SaveOptions;

    {Nvx DTU 2014 }
    DTU2014 : Boolean;

    Procedure ImportXMLActhys(_Node : IXMLNode); Override;
    Procedure ExportXMLActhys(_Node : IXMLNode); Override;
    Procedure ExportXMLActhys2ndPasse(_Node : IXMLNode); Override;
    Procedure Sauvegarder;
    Procedure Ouvrir;
    Procedure Charger_DataExport_ImagesCaissons(Var _Caisson : TVentil_Caisson);
    Procedure Affiche (_Grid: TAdvStringGrid); Override;
    Procedure Recupere(_Grid: TAdvStringGrid; _Ligne: Integer); Override;
    Function  TypeEdition(_Grid: TAdvStringGrid; _Ligne: Integer): TEditorType; Override;
    Procedure PropEdition(_Grid: TAdvStringGrid; _Ligne: Integer); Override;

    Procedure AddLogement;
    Procedure CalculerInternal(_Iteratif : Boolean);
    procedure Calculer(_ForceMode : Boolean = False; _ForceIteratif : Boolean = False);
    procedure CalculerIteratif;
    Procedure MiseAjourEtiquettesVisibles(_Obj : TVentil_Troncon = nil);

    Procedure FillTree(_Tree: TBaseVirtualTree; _Selection : TVentil_Objet = Nil);

    Function Has_VMCGaz: Boolean;
    Function Has_DoubleFlux: Boolean;
    Function Is_FullVeloduct : Boolean;
    Function GetTauxFuiteRT : Real;

    Procedure GetAssociationManquantes(var _AssociationsManquantes : TStringList);

    { Interaction avec le CAD }
    Function NumCADInterface(_CADInterface: IAero_Vilo_Base): Integer;
    Function NbEtageBat : Integer;
    Function HtEtageDef : Real;

    Function  ObjFromGUID(_GUID: String; _ObjetCad: IAero_Vilo_Base): IAero_Fredo_Base;
    Function  AddComposant(_ObjetCad: IAero_Vilo_Base): IAero_Fredo_Base;
    Procedure RemoveComposant(_Objet: TVentil_Objet);
    Function  ReseauActif    : TVentil_Caisson;

//    Function  ChiffrageActif : TChiffrage;
//    Procedure AddChiffrage(_Produit: TEdibatec_Produit; _Quantite: Real; _CategorieChiffrage, _CodeClasseChiffrage: Integer);
//    Procedure AddChiffrageErreur(Const _TypeProduit, _Supplement: String; _Supplement2: String = '');

    Property DeltaCalculeMax : Real read GetFDeltaCalculeMax write FDeltaCalculeMax ;
    Property DeltaCalculeMin : Real read GetFDeltaCalculeMin write FDeltaCalculeMin ;
 End;
    Procedure OuvrirLesTables;
    Procedure SauverLesTables;
    Procedure FermerLesTables;
    Procedure ViderLesTablesAvecCondition(Condition : String);
{ ************************************************************************************************************************************************** }

Implementation
Uses
  Math,
  BBS_Message,
  Dialogs,
  BBS_Progress, Jpeg,
  Ventil_TourelleTete,
  Ventil_Colonne,
  Ventil_Declarations,
  Ventil_Utils, Ventil_Firebird,
  Ventil_Impressions,
  BbsImpFiche,
  AccesBDD_Helper,
  CAD_ViloUtils,
  BbsgFonc,
{$IFDEF ACTHYS_DIMVMBP}
  ImportEVenthysXSD,
  ExportEVenthysXSD,
{$ENDIF}
   Ventil_Bouche, Ventil_TronconDessin;

{ ************************************************************************************************************************************************** }
Procedure ViderLesTablesAvecCondition(Condition : String);
Begin
  Table_Etude_Generale.Vider(Condition);
  Table_Etude_Batiment.Vider(Condition);
  Table_Etude_Logement.Vider(Condition);
  Table_Etude_DimLogement.Vider(Condition);
  Table_Etude_DnLogement.Vider(Condition);
  Table_Etude_Troncon.Vider(Condition);
  Table_Etude_Objet.Vider(Condition);
  Table_Etude_Caisson.Vider(Condition);
  Table_Etude_Bifurcation.Vider(Condition);
  Table_Etude_TeSouche.Vider(Condition);
  Table_Etude_Colonne.Vider(Condition);
  Table_Etude_Plenum.Vider(Condition);
  Table_Etude_Collecteur.Vider(Condition);
  Table_Etude_Bouche.Vider(Condition);
  Table_Etude_ChgtDiam.Vider(Condition);
  Table_Etude_ClapetCoupeFeu.Vider(Condition);
  Table_Etude_AccGeneric.Vider(Condition);
  Table_Etude_Coudes.Vider(Condition);  
  Table_Etude_SortieToit.Vider(Condition);
  Table_Etude_RegulateurDebit.Vider(Condition);
  Table_Etude_Silencieux.Vider(Condition);
  Table_Etude_Registre.Vider(Condition);
  Table_Etude_TronconDessinConduit.Vider(Condition);
  Table_Etude_TronconDessinSortieCaisson.Vider(Condition);
  Table_Etude_TourelleTete.Vider(Condition);
  Table_Etude_TourelleIntermediaire.Vider(Condition);
  Table_Etude_TourellePlenum.Vider(Condition);
  Table_Etude_TourelleBifurcation.Vider(Condition);
{$IfNDef AERAU_CLIMAWIN}
{$IfDef VIM_OPTAIR}
  Table_Etude_DataExport.Vider('');
{$EndIf}
{$EndIf}
End;
{ ************************************************************************************************************************************************** }
Procedure OuvrirLesTables;
Begin
  Table_Etude_Generale.Ouvrir;
  Table_Etude_Batiment.Ouvrir;
  Table_Etude_Logement.Ouvrir;
  Table_Etude_DimLogement.Ouvrir;
  Table_Etude_DnLogement.Ouvrir;
  Table_Etude_Objet.Ouvrir;
  Table_Etude_Troncon.Ouvrir;
  Table_Etude_Caisson.Ouvrir;
  Table_Etude_Bifurcation.Ouvrir;
  Table_Etude_TeSouche.Ouvrir;
  Table_Etude_Colonne.Ouvrir;
  Table_Etude_Plenum.Ouvrir;
  Table_Etude_Collecteur.Ouvrir;
  Table_Etude_Bouche.Ouvrir;
  Table_Etude_ChgtDiam.Ouvrir;
  Table_Etude_ClapetCoupeFeu.Ouvrir;
  Table_Etude_Coudes.Ouvrir;
  Table_Etude_AccGeneric.Ouvrir;
  Table_Etude_SortieToit.Ouvrir;
  Table_Etude_RegulateurDebit.Ouvrir;
  Table_Etude_Silencieux.Ouvrir;
  Table_Etude_Registre.Ouvrir;
  Table_Etude_TronconDessinConduit.Ouvrir;
  Table_Etude_TronconDessinSortieCaisson.Ouvrir;
  Table_Etude_TourelleTete.Ouvrir;
  Table_Etude_TourelleIntermediaire.Ouvrir;
  Table_Etude_TourellePlenum.Ouvrir;
  Table_Etude_TourelleBifurcation.Ouvrir;
{$IfNDef AERAU_CLIMAWIN}
{$IfDef VIM_OPTAIR}
  Table_Etude_DataExport.Ouvrir;
{$EndIf}
{$EndIf}
End;
{ ************************************************************************************************************************************************** }
Procedure FermerLesTables;
Begin
  Table_Etude_Generale.Fermer;
  Table_Etude_Batiment.Fermer;
  Table_Etude_Logement.Fermer;
  Table_Etude_DimLogement.Fermer;
  Table_Etude_DnLogement.Fermer;
  Table_Etude_Troncon.Fermer;
  Table_Etude_Objet.Fermer;
  Table_Etude_Caisson.Fermer;
  Table_Etude_Bifurcation.Fermer;
  Table_Etude_TeSouche.Fermer;
  Table_Etude_Colonne.Fermer;
  Table_Etude_Plenum.Fermer;
  Table_Etude_Collecteur.Fermer;
  Table_Etude_Bouche.Fermer;
  Table_Etude_ChgtDiam.Fermer;
  Table_Etude_ClapetCoupeFeu.Fermer;
  Table_Etude_Coudes.Fermer;
  Table_Etude_AccGeneric.Fermer;
  Table_Etude_SortieToit.Fermer;
  Table_Etude_RegulateurDebit.Fermer;
  Table_Etude_Silencieux.Fermer;
  Table_Etude_Registre.Fermer;
  Table_Etude_TronconDessinConduit.Fermer;
  Table_Etude_TronconDessinSortieCaisson.Fermer;
  Table_Etude_TourelleTete.Fermer;
  Table_Etude_TourelleIntermediaire.Fermer;
  Table_Etude_TourellePlenum.Fermer;
  Table_Etude_TourelleBifurcation.Fermer;
{$IfNDef AERAU_CLIMAWIN}
{$IfDef VIM_OPTAIR}
  Table_Etude_DataExport.Fermer;
{$EndIf}
{$EndIf}
End;
{ ************************************************************************************************************************************************** }
Procedure SauverLesTables;
Begin
  Table_Etude_Generale.Sauvegarder;
  Table_Etude_Batiment.Sauvegarder;
  Table_Etude_Logement.Sauvegarder;
  Table_Etude_DimLogement.Sauvegarder;
  Table_Etude_DnLogement.Sauvegarder;
  Table_Etude_Troncon.Sauvegarder;
  Table_Etude_Objet.Sauvegarder;
  Table_Etude_Caisson.Sauvegarder;
  Table_Etude_Bifurcation.Sauvegarder;
  Table_Etude_TeSouche.Sauvegarder;
  Table_Etude_Colonne.Sauvegarder;
  Table_Etude_Plenum.Sauvegarder;
  Table_Etude_Collecteur.Sauvegarder;
  Table_Etude_Bouche.Sauvegarder;
  Table_Etude_ChgtDiam.Sauvegarder;
  Table_Etude_ClapetCoupeFeu.Sauvegarder;
  Table_Etude_Coudes.Sauvegarder;  
  Table_Etude_AccGeneric.Sauvegarder;
  Table_Etude_SortieToit.Sauvegarder;
  Table_Etude_RegulateurDebit.Sauvegarder;
  Table_Etude_Silencieux.Sauvegarder;
  Table_Etude_Registre.Sauvegarder;
  Table_Etude_TronconDessinConduit.Sauvegarder;
  Table_Etude_TronconDessinSortieCaisson.Sauvegarder;
  Table_Etude_TourelleTete.Sauvegarder;
  Table_Etude_TourelleIntermediaire.Sauvegarder;
  Table_Etude_TourellePlenum.Sauvegarder;
  Table_Etude_TourelleBifurcation.Sauvegarder;
{$IfNDef AERAU_CLIMAWIN}
{$IfDef VIM_OPTAIR}
  Table_Etude_DataExport.Sauvegarder;
{$EndIf}
{$EndIf}
End;
{ ******************************************************************* TVentil_Etude ******************************************************************* }
Procedure TVentil_Etude.Affiche(_Grid: TAdvStringGrid);
Begin
  //Inherited;
  _Grid.Cells[0, 1] := 'Reference';
  _Grid.Cells[1, 1] := Reference;
  _Grid.Cells[0, 2] := 'Date';
  _Grid.Cells[1, 2] := DateToStr(Date);

End;
{ ************************************************************************************************************************************************** }
Constructor TVentil_Etude.Create(_GridProp, _GridRes, _GridResReseau, _GridAcoust : TAdvStringGrid;  _TabAcoust: TTabSheet; _ProcPremierPlan: TBringToFront; _Image: TImage = nil;
                                 _PageControl: TPageControl = nil; _TypeBat : Integer = c_BatimentCollectif; _GridSynthese207: TAdvStringGrid = Nil);
Begin

  Inherited Create;
  ModeIteratif := False;
  FromCallBack := False;
  DTU2014 := True;
  Etude := Self;
  Image            := _Image;
  Reference        := 'Nouvelle �tude';
  Fichier          := '';
  InternalFichier          := '';
  ToSave           := True;
  Batiment         := TVentil_Batiment.Create(_TypeBat);
  GridProp         := _GridProp;
  GridRes          := _GridRes;
  GridResReseau    := _GridResReseau;
  GridAcoust       := _GridAcoust;
  GridSynthese207  := _GridSynthese207;
  TabAcoust        := _TabAcoust;
  InfosPremierPlan := _ProcPremierPlan;
  PageControl      := _PageControl;
  DateEtude        := Now;
  DeltaCalculeMax     := 0;
  DeltaCalculeMin     := 0;
  SaveOptions_SelectionCaissonVIM := TSelectionCaisson_SaveOptions.Create;
  DTUActive           := True;
  Remarques        := TStringList.Create;

End;
{ ************************************************************************************************************************************************** }
Destructor TVentil_Etude.Destroy;
Begin

  FreeAndNil(Remarques);
  FreeAndNil(Batiment);

{$IfNDef AERAU_CLIMAWIN}
{$IfDef VIM_OPTAIR}
  Vider_DataExport;
{$EndIf}
{$EndIf}

  FreeAndNil(SaveOptions_SelectionCaissonVIM);

  Inherited;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Etude.Recupere(_Grid: TAdvStringGrid; _Ligne: Integer);
Begin

End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Etude.Ouvrir;
var
        m_No : Integer;
        m_Str : String;
        cptCaisson : Integer;
        Condition : String;
Begin

AutoSaveAutorisee := False;
  Loading := True;

  OuvrirTables;


    {$IfDef AERAU_CLIMAWIN}
    Condition := 'IDRESEAU=''' + IdReseau  + '''';
    Table_Etude_Generale.Donnees.Filter := Condition;
    Table_Etude_Generale.Donnees.Filtered := True;
    {$EndIf}

  Table_Etude_Generale.Donnees.First;
  Id := Str2GUID(Table_Etude_Generale.Donnees.FieldByName('ID_ETUDE').AsWideString);
  Reference := Table_Etude_Generale.Donnees.FieldByName('REFERENCE').AsWideString;
  DateEtude := Table_Etude_Generale.Donnees.FieldByName('DATE_ETUDE').AsDateTime;
  Descriptif   := Table_Etude_Generale.Donnees.FieldByName('DESCRIPTIF').AsWideString;
  Adresse      := Table_Etude_Generale.Donnees.FieldByName('ADRESSE').AsWideString;
  MOuvrage     := Table_Etude_Generale.Donnees.FieldByName('M_OUVRAGE').AsWideString;
  MOeuvre      := Table_Etude_Generale.Donnees.FieldByName('M_OEUVRE').AsWideString;
  Installateur := Table_Etude_Generale.Donnees.FieldByName('INSTALLATEUR').AsWideString;
  Remarques.Text := Table_Etude_Generale.Donnees.FieldByName('REMARQUES').AsWideString;
  Objet        := Table_Etude_Generale.Donnees.FieldByName('OBJET').AsWideString;
  RefDossier      := Table_Etude_Generale.Donnees.FieldByName('REFDOSSIER').AsWideString;
  FDeltaCalculeMax:= Table_Etude_Generale.Donnees.FieldByName('DELTACALCULEMAX').AsFloat;
  FDeltaCalculeMin:= Table_Etude_Generale.Donnees.FieldByName('DELTACALCULEMIN').AsFloat;
  VersionSave := Table_Etude_Generale.Donnees.FieldByName('VERSIONSAVE').AsInteger;
    if Table_Etude_Generale.Donnees.FieldByName('DTUACTIVE').AsWideString <> '' then
      DTUActive := Int2Bool(Table_Etude_Generale.Donnees.FieldByName('DTUACTIVE').AsInteger);



  if Batiment.IsFabricantVIM then
    begin
    m_Str := Table_Etude_Generale.Donnees.FieldByName('OPTIONSCAISSON').AsWideString;
      If m_Str <> '' Then
        begin
          if SaveOptions_SelectionCaissonVIM <> nil then
            SaveOptions_SelectionCaissonVIM.Ouvrir(Table_Etude_Generale.Donnees);
        end
      else
        FreeAndNil(SaveOptions_SelectionCaissonVIM);
    end
  else
    FreeAndNil(SaveOptions_SelectionCaissonVIM);


  FBeginProgress(9, 'Chargement: ' + Reference);
  Batiment.Load;
  FStepProgress('�l�ments graphiques');

  {$IfDef AERAU_CLIMAWIN}
  InterfaceCAD.IBBScad_BDD_Load(Base_Etude.AsIBaseDeDonnees, '0', False);
  {$ELSE}
  InterfaceCAD.IBBScad_BDD_Load(Base_Etude.AsIBaseDeDonnees, '0', False);
  {$EndIf}

  FEndProgress;
  FermerTables;


  {$IfDef AERAU_CLIMAWIN}
    InterfaceCAD.IBBScad_Etude_Activate(IdReseau);
  {$ELSE}
    InterfaceCAD.IBBScad_Etude_Activate(GUIDToString(Id));
  {$EndIf}


  if Batiment.IsFabricantVIM and DTU2014 then
    if InterfaceCAD.Aero_Vilo2Fredo_SupprimeClasseObjet(IAero_Accident_Registre) then
      BBS_ShowMessage('Les registres de ''�tude ont �t� supprim�s', mtInformation, [mbOK], 0);

  Calculer(true, false);

  InterfaceCAD.Aero_Vilo2Fredo_Refresh(rtAll);

for cptCaisson := 0 to Batiment.Reseaux.count - 1 do
  begin
  TVentil_Caisson(Batiment.Reseaux[cptCaisson].Caisson).UpdateDimensionsCaisson;
    if Batiment.IsFabricantVIM or ((Batiment.IsFabricantACT or Batiment.IsFabricantACT) and (TVentil_Caisson(Batiment.Reseaux[cptCaisson].Caisson).ProduitAssocie <> nil)) then
      begin
        if TVentil_Caisson(Batiment.Reseaux[cptCaisson].Caisson).ProduitAssocie <> nil then
          For m_No := 0 To TVentil_Caisson(Batiment.Reseaux[cptCaisson].Caisson).Fils.Count - 1 Do
            Begin
              if TVentil_TronconDessin_SortieCaisson(TVentil_Caisson(Batiment.Reseaux[cptCaisson].Caisson).Fils[m_No]).TypeSortie = c_CaissonSortie_Refoulement then
                Begin
                TVentil_TronconDessin_SortieCaisson(TVentil_Caisson(Batiment.Reseaux[cptCaisson].Caisson).Fils[m_No]).Diametre := TVentil_Caisson(Batiment.Reseaux[cptCaisson].Caisson).ProduitAssocie.DiametreRejet;
                End
              else
                Begin
                TVentil_TronconDessin_SortieCaisson(TVentil_Caisson(Batiment.Reseaux[cptCaisson].Caisson).Fils[m_No]).Diametre := TVentil_Caisson(Batiment.Reseaux[cptCaisson].Caisson).ProduitAssocie.DiametreAspiration;
                  if Batiment.IsFabricantVIM and TVentil_Caisson(Batiment.Reseaux[cptCaisson].Caisson).InheritsFrom(TVentil_TourelleTete) and (TVentil_Caisson(Batiment.Reseaux[cptCaisson].Caisson).GetFirstChildrenClass(TVentil_Colonne) <> Nil) then
                    TVentil_Caisson(Batiment.Reseaux[cptCaisson].Caisson).GetFirstChildrenClass(TVentil_Colonne).Diametre := TVentil_Caisson(Batiment.Reseaux[cptCaisson].Caisson).ProduitAssocie.DiametreAspiration;
                End;
//        if TVentil_Caisson(Batiment.Reseaux[cptCaisson].Caisson).SaisieDimensions = C_Non then
            TVentil_TronconDessin_SortieCaisson(TVentil_Caisson(Batiment.Reseaux[cptCaisson].Caisson).Fils[m_No]).LienCad.IAero_Vilo_Base_SetDiametre(TVentil_TronconDessin_SortieCaisson(TVentil_Caisson(Batiment.Reseaux[cptCaisson].Caisson).Fils[m_No]).Diametre);
//        Aero_Vilo2Fredo_UpdateChangementsDeDiametre(Nil);
                if Batiment.IsFabricantVIM and TVentil_Caisson(Batiment.Reseaux[cptCaisson].Caisson).InheritsFrom(TVentil_TourelleTete) and (TVentil_Caisson(Batiment.Reseaux[cptCaisson].Caisson).GetFirstChildrenClass(TVentil_Colonne) <> Nil) then
                  TVentil_Caisson(Batiment.Reseaux[cptCaisson].Caisson).GetFirstChildrenClass(TVentil_Colonne).LienCad.IAero_Vilo_Base_SetDiametre(TVentil_TronconDessin_SortieCaisson(TVentil_Caisson(Batiment.Reseaux[cptCaisson].Caisson).Fils[m_No]).Diametre);
            End;


      end
    else
        For m_No := 0 To TVentil_Caisson(Batiment.Reseaux[cptCaisson].Caisson).Fils.Count - 1 Do
          Begin
            if TVentil_TronconDessin_SortieCaisson(TVentil_Caisson(Batiment.Reseaux[cptCaisson].Caisson).Fils[m_No]).TypeSortie = c_CaissonSortie_Refoulement then
              Begin
                If TVentil_Caisson(Batiment.Reseaux[cptCaisson].Caisson).DiametreRefoulement <> c_Diam_Calc then
                  TVentil_TronconDessin_SortieCaisson(TVentil_Caisson(Batiment.Reseaux[cptCaisson].Caisson).Fils[m_No]).Diametre := c_ListeDiametres[TVentil_Caisson(Batiment.Reseaux[cptCaisson].Caisson).DiametreRefoulement];
              End
            else
              Begin
                If TVentil_Caisson(Batiment.Reseaux[cptCaisson].Caisson).IndiceDiametre <> c_Diam_Calc then
                  begin
                  TVentil_TronconDessin_SortieCaisson(TVentil_Caisson(Batiment.Reseaux[cptCaisson].Caisson).Fils[m_No]).Diametre := TVentil_Caisson(Batiment.Reseaux[cptCaisson].Caisson).Diametre;
                  if Batiment.IsFabricantVIM and TVentil_Caisson(Batiment.Reseaux[cptCaisson].Caisson).InheritsFrom(TVentil_TourelleTete) and (TVentil_Caisson(Batiment.Reseaux[cptCaisson].Caisson).GetFirstChildrenClass(TVentil_Colonne) <> Nil) then
                    TVentil_Caisson(Batiment.Reseaux[cptCaisson].Caisson).GetFirstChildrenClass(TVentil_Colonne).Diametre := TVentil_Caisson(Batiment.Reseaux[cptCaisson].Caisson).Diametre;
                  end;
              End;
//        if TVentil_Caisson(Batiment.Reseaux[cptCaisson].Caisson).SaisieDimensions = C_Non then
          TVentil_TronconDessin_SortieCaisson(TVentil_Caisson(Batiment.Reseaux[cptCaisson].Caisson).Fils[m_No]).LienCad.IAero_Vilo_Base_SetDiametre(TVentil_TronconDessin_SortieCaisson(TVentil_Caisson(Batiment.Reseaux[cptCaisson].Caisson).Fils[m_No]).Diametre);
//        Aero_Vilo2Fredo_UpdateChangementsDeDiametre(Nil);
                if Batiment.IsFabricantVIM and TVentil_Caisson(Batiment.Reseaux[cptCaisson].Caisson).InheritsFrom(TVentil_TourelleTete) and (TVentil_Caisson(Batiment.Reseaux[cptCaisson].Caisson).GetFirstChildrenClass(TVentil_Colonne) <> Nil) then
                  TVentil_Caisson(Batiment.Reseaux[cptCaisson].Caisson).GetFirstChildrenClass(TVentil_Colonne).LienCad.IAero_Vilo_Base_SetDiametre(TVentil_TronconDessin_SortieCaisson(TVentil_Caisson(Batiment.Reseaux[cptCaisson].Caisson).Fils[m_No]).Diametre);
          End;
  end;

  Calculer;

AutoSaveAutorisee := True;
Loading := False;

    {$IfDef AERAU_CLIMAWIN}
    Table_Etude_Generale.Donnees.Filtered := False;
    {$EndIf}

  If (Batiment <> Nil) and (Batiment.ChiffrageVerouille = c_Oui) Then
    InterfaceCAD.Aero_Vilo2Fredo_LockSaisie;

End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Etude.ImportXMLActhys(_Node : IXMLNode);
begin
  Inherited;
    if _Node = Nil then
      Exit;

{$IFDEF ACTHYS_DIMVMBP}
    if (_Node as IXMLEXPORT_EVENTHYS).ADMINISTRATIF = Nil then
      Exit;
  Reference       := (_Node as IXMLEXPORT_EVENTHYS).ADMINISTRATIF.Ref_Export;
  DateEtude       := XML2Date((_Node as IXMLEXPORT_EVENTHYS).ADMINISTRATIF.Date);
  Adresse         := (_Node as IXMLEXPORT_EVENTHYS).ADMINISTRATIF.Adresse;
  MOuvrage        := (_Node as IXMLEXPORT_EVENTHYS).ADMINISTRATIF.Maitre_ouvrage;
  MOeuvre         := (_Node as IXMLEXPORT_EVENTHYS).ADMINISTRATIF.Maitre_oeuvre;
  Installateur    := (_Node as IXMLEXPORT_EVENTHYS).ADMINISTRATIF.Installateur;
  Objet           := (_Node as IXMLEXPORT_EVENTHYS).ADMINISTRATIF.Objet;
  Remarques.Text  := (_Node as IXMLEXPORT_EVENTHYS).ADMINISTRATIF.Remarques;
    if Batiment <> Nil then
      Batiment.ImportXMLActhys(_Node);
{$ENDIF}
end;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Etude.ExportXMLActhys(_Node : IXMLNode);
begin
  Inherited;
    if _Node = Nil then
      Exit;

{$IFDEF ACTHYS_DIMVMBP}
  (_Node as IXMLIMPORT_EVENTHYS).ADMINISTRATIF.Ref_Export := Reference;
  (_Node as IXMLIMPORT_EVENTHYS).ADMINISTRATIF.Date := Date2XML(DateEtude);
  (_Node as IXMLIMPORT_EVENTHYS).ADMINISTRATIF.Adresse := Adresse;
//  (_Node as IXMLIMPORT_EVENTHYS).ADMINISTRATIF.Ref_Export := Descriptif;
  (_Node as IXMLIMPORT_EVENTHYS).ADMINISTRATIF.Maitre_ouvrage := MOuvrage;
  (_Node as IXMLIMPORT_EVENTHYS).ADMINISTRATIF.Maitre_oeuvre := MOeuvre;
  (_Node as IXMLIMPORT_EVENTHYS).ADMINISTRATIF.Installateur := Installateur;
  (_Node as IXMLIMPORT_EVENTHYS).ADMINISTRATIF.Objet := Objet;
  (_Node as IXMLIMPORT_EVENTHYS).ADMINISTRATIF.Remarques := Remarques.Text;
    if Batiment <> Nil then
      begin
      //Premiers r�sultats en mode standard
      Calculer(True, False);
      Batiment.ExportXMLActhys(_Node);
      end;
{$ENDIF}
end;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Etude.ExportXMLActhys2ndPasse(_Node : IXMLNode);
begin
    if (Batiment <> Nil) and Options.Iterate_Export then
      begin
      //Seconds r�sultats en mode it�ratif
      Calculer(True, True);
      Batiment.ExportXMLActhys2ndPasse(_Node);
      //On se remet sur le mode de calcul utilis� dans le logiciel
      Calculer;
      end;
end;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Etude.Sauvegarder;
Begin

AutoSaveAutorisee := False;

  If Fichier <> '' Then Begin
    ViderTables;
    OuvrirTables;

    Table_Etude_Generale.Donnees.Insert;
    Table_Etude_Generale.Donnees.FieldByName('ID_ETUDE').AsWideString := GUIDToString(Id);
    {$IfDef AERAU_CLIMAWIN}
    Table_Etude_Generale.Donnees.FieldByName('IDRESEAU').AsWideString := IdReseau;
    {$EndIf}
    Table_Etude_Generale.Donnees.FieldByName('REFERENCE').AsWideString := Reference;
    Table_Etude_Generale.Donnees.FieldByName('DATE_ETUDE').AsDateTime := DateEtude;
    Table_Etude_Generale.Donnees.FieldByName('DESCRIPTIF').AsWideString := Descriptif;
    Table_Etude_Generale.Donnees.FieldByName('ADRESSE').AsWideString := Adresse;
    Table_Etude_Generale.Donnees.FieldByName('M_OUVRAGE').AsWideString := MOuvrage;
    Table_Etude_Generale.Donnees.FieldByName('M_OEUVRE').AsWideString := MOeuvre;
    Table_Etude_Generale.Donnees.FieldByName('INSTALLATEUR').AsWideString := Installateur;
    Table_Etude_Generale.Donnees.FieldByName('REMARQUES').AsWideString := Remarques.Text;
    Table_Etude_Generale.Donnees.FieldByName('OBJET').AsWideString := Objet;
    Table_Etude_Generale.Donnees.FieldByName('REFDOSSIER').AsWideString := RefDossier;
    Table_Etude_Generale.Donnees.FieldByName('DELTACALCULEMAX').AsFloat := FDeltaCalculeMax;
    Table_Etude_Generale.Donnees.FieldByName('DELTACALCULEMIN').AsFloat := FDeltaCalculeMin;
    Table_Etude_Generale.Donnees.FieldByName('DTUACTIVE').AsInteger := Bool2Int(DTUActive);
    Table_Etude_Generale.Donnees.FieldByName('OPTIONSCAISSON').AsInteger := Bool2Int(True);
    Table_Etude_Generale.Donnees.FieldByName('VERSIONSAVE').AsInteger := cst_VersionSave;


  {$IfNDef AERAU_CLIMAWIN}
  if Batiment.IsFabricantVIM and not(Batiment.IsFabricantMVN) and not(Batiment.IsFabricantACT) then
        begin
        if SaveOptions_SelectionCaissonVIM <> nil then //devrait toujours �tre � nil donc ne jamais sauvegarder
                SaveOptions_SelectionCaissonVIM.Sauvegarder(Table_Etude_Generale.Donnees);

    FBeginProgress(Batiment.Composants.Count + 9, 'sauvegarde : ' + Reference);

        if Not(IsAutoSaving) then
                Begin
                SetDataImp;
                Sauvegarder_DataExport;
                Vider_DataExport;
                End;

        end
        else
  {$EndIf}        
        begin
            FBeginProgress(Batiment.Composants.Count + 2, 'sauvegarde : ' + Reference);
        end;
    Batiment.Save;
    FStepProgress('�l�ments graphiques');
    InterfaceCAD.IBBScad_BDD_Save(Base_Etude.AsIBaseDeDonnees, '0');
    FEndProgress;
    InfosPremierPlan;
    SauverTables;
    FermerTables;
  End;
AutoSaveAutorisee := True;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Etude.OuvrirTables;
Begin
OuvrirLesTables;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Etude.ViderTables;
Var
        Condition : String;
Begin

        {$IfDef AERAU_CLIMAWIN}
        Condition := 'IDRESEAU=''' + IdReseau + '''';
        {$Else}
        Condition := '';
        {$EndIf}

ViderLesTablesAvecCondition(Condition);
End;

{ ************************************************************************************************************************************************** }
Procedure TVentil_Etude.FermerTables;
Begin
FermerLesTables;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Etude.SauverTables;
Begin
SauverLesTables;
End;
{ ************************************************************************************************************************************************** }
Function TVentil_Etude.TypeEdition(_Grid: TAdvStringGrid; _Ligne: Integer): TEditorType;
Begin
  Result := edNone;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Etude.PropEdition(_Grid: TAdvStringGrid; _Ligne: Integer);
begin
end;
{ ************************************************************************************************************************************************** }
Function TVentil_Etude.NumCADInterface(_CADInterface: IAero_Vilo_Base): Integer;
Begin
  Result := 0;
  If Supports(_CADInterface, IAero_Caisson)                          Then Result := c_CAD_Caisson Else
  If Supports(_CADInterface, IAero_Collecteur)                       Then Result := c_CAD_Collecteur Else
  If Supports(_CADInterface, IAero_Bouche)                           Then Result := c_CAD_Bouche Else
  If Supports(_CADInterface, IAero_EntreeAir)                        Then Result := c_CAD_EA Else
  If Supports(_CADInterface, IAero_RegulateurDeDebit)                Then Result := c_CAD_RegulateurDebit Else
  If Supports(_CADInterface, IAero_Accident_SilencieuxCirculaire)    Then Result := c_CAD_Silencieux Else
  If Supports(_CADInterface, IAero_Accident_SilencieuxRectangulaire) Then Result := c_CAD_Silencieux Else
  If Supports(_CADInterface, IAero_Accident_Registre)                Then Result := c_CAD_Registre Else
  If Supports(_CADInterface, IAero_Conduit)                          Then Result := c_CAD_Dessin Else
  If Supports(_CADInterface, IAero_Devoiement)                       Then Result := c_cad_Devoiement Else
  If Supports(_CADInterface, IAero_EXT_T�te)                         Then Result := c_CAD_TourelleTete Else
  If Supports(_CADInterface, IAero_EXT_Interm�diaire)                Then Result := c_CAD_TourelleIntermediaire Else
  If Supports(_CADInterface, IAero_EXT_Plenum)                       Then Result := c_CAD_TourellePlenum Else
  If Supports(_CADInterface, IAero_EXT_CaissonBifurcation)           Then Result := c_CAD_TourelleBifurcation Else
  If Supports(_CADInterface, IAero_EXT_Sortie)                       Then Result := c_CAD_TourelleSortie Else
  If Supports(_CADInterface, IAero_Plenum)                           Then Result := c_CAD_Plenum Else
  If Supports(_CADInterface, IAero_TSoucheActhys)                    Then Result := c_CAD_Colonne Else
  If Supports(_CADInterface, IAero_TSoucheEtageCoude)                Then Result := c_cad_CoudeSouche Else
  If Supports(_CADInterface, IAero_TSoucheEtagePiquage)              Then Result := c_CAD_TeSouche Else
  If Supports(_CADInterface, IAero_TSoucheToiture)                   Then Result := c_CAD_TeSouche Else
  If Supports(_CADInterface, IAero_Coude)                            Then Result := c_CAD_Coude Else
  If Supports(_CADInterface, IAero_Accident_Generic)                 Then Result := c_Cad_AccGeneric Else
  If Supports(_CADInterface, IAero_Accident_ClapetCoupeFeu)          Then Result := c_Cad_ClapetCoupeFeu Else
  If Supports(_CADInterface, IAero_Coude)                            Then Result := c_CAD_Coude Else
  If Supports(_CADInterface, IAero_Accident_ChangementDiametre)      Then Result := c_CAD_ChgtDiam Else
  If Supports(_CADInterface, IAero_Caisson_Sortie)                   Then Result := c_CAD_SortieCaisson Else
  If Supports(_CADInterface, IAero_SortieToiture)                    Then Result := c_CAD_SortieToit Else
  If Supports(_CADInterface, IAero_Bifurcation)                      Then Result := c_CAD_Bifurcation;
  If Supports(_CADInterface, IAero_EXT_Tourelle)                      Then Result := c_CAD_Tourelle;

End;
{ ************************************************************************************************************************************************** }
Function TVentil_Etude.NbEtageBat: Integer;
Begin
  Result := 0;
  If IsNotNil(Batiment) Then Result := Batiment.NbEtages;
End;
{ ************************************************************************************************************************************************** }
Function TVentil_Etude.HtEtageDef : Real;
Begin
  Result := 0;
  If IsNotNil(Batiment) Then Result := Batiment.Ht_DefautEtages;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Etude.AddLogement;
Begin
  Batiment.Logements.Add(TVentil_Logement.Create);
  Batiment.Logements[Batiment.Logements.Count - 1].Reference := Batiment.Logements[Batiment.Logements.Count - 1].Reference + ' n� ' + IntToSTr(Batiment.Logements.Count);
End;
{ ************************************************************************************************************************************************** }
procedure TVentil_Etude.Calculer(_ForceMode : Boolean = False; _ForceIteratif : Boolean = False);
var
  tempIteratif : Boolean;
begin
  if _ForceMode then
   tempIteratif := _ForceIteratif
  else
    tempIteratif := Options.Iterate_Actif;

FromCallBack := False;
  if tempIteratif then
    begin
    ModeIteratif := False;
    CalculerInternal(False);
    ModeIteratif := True;
    CalculerIteratif;
    end
  else
    begin
    NbRecalcul := 0;
    ModeIteratif := False;
    CalculerInternal(False);
    end;
FromCallBack := False;
end;
{ ************************************************************************************************************************************************** }
procedure TVentil_Etude.CalculerIteratif;
var
  m_Caissons : TlisteCaissons;
  cpt : Integer;
  tempIteration : Integer;
  FindResult : Boolean;
begin
FBeginProgress(100, 'Calcul it�ratif', True, '', True);
FindResult := True;
NbRecalcul := 0;
  Etude.Batiment.Reseaux.GetCaissons(m_Caissons);
    if Not(FromCallBack) then
    for cpt := 0 to m_Caissons.Count - 1 do
      begin
      TVentil_Caisson(m_Caissons[cpt]).NbIterationCalcul := 0;
      TVentil_Caisson(m_Caissons[cpt]).FinIterationCalcul := False;
      TVentil_Caisson(m_Caissons[cpt]).ResultatIterationCalcul := False;
      end;
  CalculerInternal(True);
  tempIteration := 0;
    for cpt := 0 to m_Caissons.Count - 1 do
      begin
      tempIteration := Max(tempIteration, TVentil_Caisson(m_Caissons[cpt]).NbIterationCalcul);
      FindResult := FindResult and TVentil_Caisson(m_Caissons[cpt]).ResultatIterationCalcul;
      end;
  m_Caissons.Destroy;
FEndProgress;

  if FindResult then
    begin
//    BBS_ShowMessage('Le calcul it�ratif a abouti en ' + IntToStr(tempIteration) + ' it�rations');
    end
  else
    begin
    BBS_ShowMessage('Le calcul it�ratif n''a pas pu aboutir avec les param�tres sp�cifi�s dans les options du logiciel');
    Calculer(True, False);
    end;
end;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Etude.CalculerInternal(_Iteratif : Boolean);
Var
  m_Lst  : IInterfaceList;
  m_NoTr : Integer;
  SaveCursor : TCursor;

  m_Caissons : TlisteCaissons;
  cpt : Integer;
  tempDelta : Single;
  tempIteration : Integer;
Begin

  if Not(_Iteratif) then
    begin
    inc(NbRecalcul);
      if (NbRecalcul >= 15) then
        Exit;
    end;

SaveCursor := Screen.Cursor;
Screen.Cursor := crHourGlass;

AutoSaveAutorisee := False;
FormatSettings.DecimalSeparator := '.';

  WithTronconsGazHorizontal := False;
  WithTronconsGazVertical := False;
  ToutGazHorizontal := True;
  ToutGazVertical:= True;
  BesoinRecalcul := False;
  Batiment.Reset;

  m_Lst := TInterfaceList.Create;
  For m_NoTr := 0 To Batiment.Composants.Count - 1 Do Begin
    Batiment.Composants[m_NoTr].UtiliseCalcul := False;
    Batiment.Composants[m_NoTr].Reseau := Nil;
  End;
  InterfaceCAD.Aero_Vilo2Fredo_GetArbo(m_Lst);
  For m_NoTr := 0 To m_Lst.Count - 1 Do
    Analyse(IAero_Vilo_Base(m_Lst[m_NoTr]), Nil);
  Batiment.Reseaux.FormattePourCalcul;
  Batiment.Reseaux.Calculer(_Iteratif);
  For m_NoTr := 0 To Batiment.Composants.Count - 1 Do Begin
    If Batiment.Composants[m_NoTr].SurTerrasse Then Begin
       If Batiment.Composants[m_NoTr].TronconGaz Then WithTronconsGazHorizontal := True;
       If Not Batiment.Composants[m_NoTr].TronconGaz Then ToutGazHorizontal := False;
    End Else Begin
       If Batiment.Composants[m_NoTr].TronconGaz Then WithTronconsGazVertical := True;
       If Not Batiment.Composants[m_NoTr].TronconGaz Then ToutGazVertical := False;
    End;
  End;

  m_Lst := Nil;

//    if Not(_Iteratif) then
  If BesoinRecalcul Then CalculerInternal(_Iteratif);
  InterfaceCAD.Aero_Vilo2Fredo_CalculFredoFinished;

        if batiment.reseaux.count > 0 then
                InterfaceCAD.Aero_Vilo2Fredo_UpdateChangementsDeDiametre(nil);

if (AfterUptadeTroncon <> Nil) And ((AfterUptadeTroncon.Parentcalcul <> Nil) or (AfterUptadeTroncon.InheritsFrom(TVentil_Caisson))) then
        Begin
        AfterUptadeTroncon.UpdateAfterCreate;
        AfterUptadeTroncon := Nil;
        End;


  if _Iteratif and Not(FromCallBack) then
    begin
    Etude.Batiment.Reseaux.GetCaissons(m_Caissons);
    tempDelta := 0;
    tempIteration := 0;
      for cpt := 0 to m_Caissons.Count - 1 do
        begin
          if not(TVentil_Caisson(m_Caissons[cpt]).FinIterationCalcul) then
            begin
            CalculerInternal(_Iteratif);
            tempDelta := Max(tempDelta, TVentil_Caisson(m_Caissons[cpt]).DeltaMax);
            tempIteration := Max(tempIteration, TVentil_Caisson(m_Caissons[cpt]).NbIterationCalcul);
            break;
            end;
        end;
    m_Caissons.Destroy;

    FStepProgress('It�ration n�' + IntToStr(tempIteration) + ' - Convergence � '+ Float2Str(tempDelta, 1) + 'Pa');

    end;


AutoSaveAutorisee := True;

Screen.Cursor := SaveCursor;

End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Etude.MiseAjourEtiquettesVisibles;
var
        m_Lst  : IInterfaceList;
        m_No  : Integer;
Begin

m_Lst := TInterfaceList.Create;
InterfaceCAD.Aero_Vilo2Fredo_GetArbo(m_Lst);

        For m_No := 0 To m_Lst.Count - 1 Do
                If IAero_Vilo_Base(m_Lst[m_No]).IAero_Vilo_Base_ObjFredo  <> nil then
                        Begin
                        TVentil_Troncon(IAero_Vilo_Base(m_Lst[m_No]).IAero_Vilo_Base_ObjFredo.IAero_Fredo_Base_Objet).MiseAjourEtiquettesVisibles;
                        End;

  m_Lst := Nil;
//Batiment.Reseaux.MiseAjourEtiquettesVisibles;
End;
{ ************************************************************************************************************************************************** }
Function TVentil_Etude.ReseauActif : TVentil_Caisson;
Var
  m_Parent : TVentil_Troncon;
Begin
  Result := nil;
  If IsNotNil(SelectedObject) Then Begin
    If SelectedObject.InheritsFrom(TVentil_Caisson) Then Result := TVentil_Caisson(SelectedObject)
    Else Begin
      m_Parent := TVentil_Troncon(SelectedObject).Parent;
      While IsNotNil(m_Parent) And Not m_Parent.InheritsFrom(TVentil_Caisson) Do m_Parent := m_Parent.Parent;
      If IsNotNil(m_Parent) And m_Parent.InheritsFrom(TVentil_Caisson) Then Result := TVentil_Caisson(m_Parent);
    End;
  End;
End;
{ ************************************************************************************************************************************************** }
(*
Function TVentil_Etude.ChiffrageActif : TChiffrage;
Begin
  Result := Batiment.Reseaux.Chiffrages.Last;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Etude.AddChiffrage(_Produit: TEdibatec_Produit; _Quantite: Real; _CategorieChiffrage, _CodeClasseChiffrage: Integer);
Begin
  ChiffrageActif.AddProduitChiffrage(_Produit, _Quantite, _CategorieChiffrage, _CodeClasseChiffrage);
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Etude.AddChiffrageErreur(Const _TypeProduit, _Supplement: String; _Supplement2: String = '');
var
        TempText : String;
Begin
TempText := _TypeProduit + ': ' + _Supplement;
        if Trim(_Supplement2) <> '' then
                TempText := TempText + ', ' + _Supplement2;
ChiffrageActif.Erreurs.Add(TempText);
End;
*)
{ ************************************************************************************************************************************************** }
Function TVentil_Etude.GetFDeltaCalculeMax : Real;
var
        cptCaisson : Integer;
Begin
Result := 0;
  if Batiment.IsFabricantVIM or Batiment.IsFabricantMVN then
  begin
  for cptCaisson := 0 to Batiment.reseaux.count - 1 do
        if (TVentil_Caisson(Batiment.Reseaux[cptCaisson].Caisson) = nil) or (TVentil_Caisson(Batiment.Reseaux[cptCaisson].Caisson).ProduitAssocie = nil) then
                Result := 0
        Else
        if (Batiment.ChiffrageVerouille = c_Non) then
                Result := 0
        else
                Result := FDeltaCalculeMax;
  end
  else
  begin
        Result := 0;
  end;
End;
{ ************************************************************************************************************************************************** }
Function TVentil_Etude.GetFDeltaCalculeMin : Real;
var
        cptCaisson : Integer;
Begin
Result := 0;
  if Batiment.IsFabricantVIM or Batiment.IsFabricantMVN then
  begin
  for cptCaisson := 0 to Batiment.reseaux.count - 1 do
        if (TVentil_Caisson(Batiment.Reseaux[cptCaisson].Caisson) = nil) or (TVentil_Caisson(Batiment.Reseaux[cptCaisson].Caisson).ProduitAssocie = nil) then
                Result := 0
        Else
        if (Batiment.ChiffrageVerouille = c_Non) then
                Result := 0
        else
                Result := FDeltaCalculeMin;
  end
  else
  begin
        Result := 0;
  end;
End;
{ ************************************************************************************************************************************************** }
Function TVentil_Etude.Has_VMCGaz: Boolean;
Var
  m_No : Integer;
  m_Sys: TVentil_Systeme;
Begin
  Result := False;
  For m_No := 0 To Batiment.Composants.Count - 1 Do If Batiment.Composants[m_No].InheritsFrom(TVentil_Bouche) Then Begin
     m_Sys := TVentil_Bouche(Batiment.Composants[m_No]).Systeme;
     If IsNotNil(m_Sys) And m_Sys.Gaz Then Begin
       Result := True;
       Break;
     End;
  End;
End;
{ ************************************************************************************************************************************************** }
Function TVentil_Etude.Has_DoubleFlux: Boolean;
Var
  m_No : Integer;
  m_Sys: TVentil_Systeme;
Begin
  Result := False;
  For m_No := 0 To Batiment.Composants.Count - 1 Do If Batiment.Composants[m_No].InheritsFrom(TVentil_Bouche) Then Begin
     m_Sys := TVentil_Bouche(Batiment.Composants[m_No]).Systeme;
     If IsNotNil(m_Sys) And m_Sys.DoubleFlux Then Begin
       Result := True;
       Break;
     End;
  End;
End;
{ ************************************************************************************************************************************************** }
Function TVentil_Etude.Is_FullVeloduct: Boolean;
Begin
        if (Batiment = nil) or (Batiment.SystemeCollectif = nil) then
                begin
                result := false;
                exit;
                end;

Result := (not(Batiment.SystemeCollectif.Gaz) and (Batiment.MatiereAccessoires = c_AccessoiresVeloduct)) or ((Batiment.SystemeCollectif.Gaz) and ((Batiment.MatiereAccessoiresHor = c_AccessoiresSiGazVeloduct) or (Batiment.MatiereAccessoiresVert = c_AccessoiresSiGazVeloduct)))

End;
{ ************************************************************************************************************************************************** }
Function TVentil_Etude.GetTauxFuiteRT : Real;
Var
  _KRes : Real;
begin
  if (Batiment <> Nil) and Batiment.IsFabricantMVN and (Batiment.SystemeCollectif <> Nil) and Batiment.SystemeCollectif.BassePression then
    Result := 0.3
  else
    begin
      if Is_FullVeloduct then
        _KRes := 3 * power(10, -6)
      else
  //      _KRes := 27 * power(10, -6);
        begin
          if (Batiment <> Nil) and Batiment.IsFabricantVIM then
            _KRes := 9 * power(10, -6)
          else
            _KRes := 675 * power(10, -7);
        end;

    Result := 5314 * _KRes;
    end;
end;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Etude.GetAssociationManquantes(var _AssociationsManquantes : TStringList);
Var
  m_No : Integer;
  m_NoAsso : Integer;
Begin
        For m_No := 0 To Batiment.Composants.Count - 1 Do
                If Batiment.Composants[m_No].InheritsFrom(TVentil_Bouche) Then
                        Begin
                                If (TVentil_Bouche(Batiment.Composants[m_No]).AssociationsNonTrouvees <> Nil) And (TVentil_Bouche(Batiment.Composants[m_No]).AssociationsNonTrouvees.Count > 0) Then
                                        For m_NoAsso := 0 to TVentil_Bouche(Batiment.Composants[m_No]).AssociationsNonTrouvees.Count - 1 do
                                                        _AssociationsManquantes.Add(TVentil_Bouche(Batiment.Composants[m_No]).AssociationsNonTrouvees[m_NoAsso]);
                        End;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Etude.FillTree(_Tree: TBaseVirtualTree; _Selection : TVentil_Objet = Nil);
Var
  m_BatNode : PVirtualNode;
  m_LogNode : PVirtualNode;
  m_NoLog   : Integer;
Begin
  _Tree.Clear;
  m_BatNode := _Tree.AddChild(Nil);
  With TArbreBatimentNode(_Tree.GetNodeData(m_BatNode))^ Do Begin
    Texte      := Batiment.Reference;
    Data       := Batiment;
    ImageIndex := 0;
  End;
  _Tree.BeginUpdate;
  For m_NoLog := 0 To Batiment.Logements.Count - 1 Do Begin
    m_LogNode := _Tree.AddChild(m_BatNode);
    With TArbreBatimentNode(_Tree.GetNodeData(m_LogNode))^ Do Begin
      Texte      := Batiment.Logements[m_NoLog].Reference;
      Data       := Batiment.Logements[m_NoLog];
      ImageIndex := 1;
    End;
    If Batiment.Logements[m_NoLog] = _Selection Then Begin
      _Tree.Selected[m_LogNode] := True;
      _Tree.FocusedNode := m_LogNode;
    End;
  End;
  _Tree.FullExpand;
  _Tree.EndUpdate;
  If _Selection = Nil Then Begin
    _Tree.Selected[m_BatNode] := True;
    _Tree.FocusedNode := m_BatNode;
  End;
End;
{ ************************************************************************************************************************************************** }
Function TVentil_Etude.ObjFromGUID(_GUID: String; _ObjetCad: IAero_Vilo_Base): IAero_Fredo_Base;
Begin
  Result := Batiment.ComposantFromGUID(_GUID, _ObjetCad);
End;
{ ************************************************************************************************************************************************** }
Function TVentil_Etude.AddComposant(_ObjetCad: IAero_Vilo_Base): IAero_Fredo_Base;
Begin
  Result := Batiment.AddTroncon(_ObjetCad);
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Etude.RemoveComposant(_Objet: TVentil_Objet);
Begin
If Batiment <> nil then
  Batiment.RemoveComposant(_Objet);
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Etude.Sauvegarder_DataExport;
{$IfNDef AERAU_CLIMAWIN}
Var
  m_ImgImageCaisson    : TPrintImageCaisson;
  m_Prefix  : String;
  m_Chemin  : String;
  m_Stream  : TMemoryStream;
  m_NoImg   : Integer;
  m_StrNo   : String;
  m_Img     : TJPEGImage;
{$Else}
{$EndIf}
Begin
{$IfNDef AERAU_CLIMAWIN}
  m_Prefix := NoOptair;
  m_Chemin := ExtractFilePath(Application.ExeName) + cst_RepEtudeOptair + '\';
  { Donn�es g�n�rales }
  If DataImp_DonneesGenerale <> nil Then Begin
    Table_Etude_DataExport.Donnees.Insert;
    Table_Etude_DataExport.Donnees.FieldByName('TYPE_EXPORT').AsWideString := m_Prefix + cst_ImpDonneesGen;
    m_Stream := TMemoryStream.Create;
    DataImp_DonneesGenerale.SaveToStream(m_Stream);
    (Table_Etude_DataExport.Donnees.FieldByName('DATA_EXPORT') As TBlobField).LoadFromStream(m_Stream);
    FreeAndNil(m_Stream);
    Table_Etude_DataExport.Donnees.Post;
  End;
FreeAndNil(DataImp_DonneesGenerale);

  { Donn�es avis techniques }
  If DataImp_AvisTechniques <> nil Then Begin
    Table_Etude_DataExport.Donnees.Insert;
    Table_Etude_DataExport.Donnees.FieldByName('TYPE_EXPORT').AsWideString := m_Prefix + cst_ImpAvisTech;
    m_Stream := TMemoryStream.Create;
    DataImp_AvisTechniques.SaveToStream(m_Stream);
    (Table_Etude_DataExport.Donnees.FieldByName('DATA_EXPORT') As TBlobField).LoadFromStream(m_Stream);
    FreeAndNil(m_Stream);
    Table_Etude_DataExport.Donnees.Post;
  End;
FreeAndNil(DataImp_AvisTechniques);

  { Entr�es d''air }
  If DataImp_DonneesEntreesAir <> nil Then Begin
    Table_Etude_DataExport.Donnees.Insert;
    Table_Etude_DataExport.Donnees.FieldByName('TYPE_EXPORT').AsWideString := m_Prefix + cst_ImpEA;
    m_Stream := TMemoryStream.Create;
    DataImp_DonneesEntreesAir.SaveToStream(m_Stream);
    (Table_Etude_DataExport.Donnees.FieldByName('DATA_EXPORT') As TBlobField).LoadFromStream(m_Stream);
    FreeAndNil(m_Stream);
    Table_Etude_DataExport.Donnees.Post;
  End;
FreeAndNil(DataImp_DonneesEntreesAir);

  { Calcul retour }
  If DataImp_CalculRetour <> nil Then Begin
    Table_Etude_DataExport.Donnees.Insert;
    Table_Etude_DataExport.Donnees.FieldByName('TYPE_EXPORT').AsWideString := m_Prefix + cst_ImpCalcRetour;
    m_Stream := TMemoryStream.Create;
    DataImp_CalculRetour.SaveToStream(m_Stream);
    (Table_Etude_DataExport.Donnees.FieldByName('DATA_EXPORT') As TBlobField).LoadFromStream(m_Stream);
    FreeAndNil(m_Stream);
    Table_Etude_DataExport.Donnees.Post;
  End;
FreeAndNil(DataImp_CalculRetour);

  { Sch�mas �tages }
  If DataImp_SchemasEtgs <> Nil Then Begin
    For m_NoImg := 0 To DataImp_SchemasEtgs.Count - 1 Do Begin
      Table_Etude_DataExport.Donnees.Insert;
      m_StrNo := IntToStr(m_NoImg + 1);
      If Length(m_StrNo) = 1 Then m_StrNo := '0' + m_StrNo;
      Table_Etude_DataExport.Donnees.FieldByName('TYPE_EXPORT').AsWideString := m_Prefix + cst_ImpSchEtg + m_StrNo;
      m_Img := TJPEGImage(DataImp_SchemasEtgs[m_NoImg]);
      m_Stream := TMemoryStream.Create;
      m_Img.SaveToStream(m_Stream);
      (Table_Etude_DataExport.Donnees.FieldByName('DATA_EXPORT') As TBlobField).LoadFromStream(m_Stream);
      FreeAndNil(m_Stream);
      Table_Etude_DataExport.Donnees.Post;
    End;
  End;
  If DataImp_SchemasEtgs <> Nil Then Begin
    For m_NoImg := DataImp_SchemasEtgs.Count - 1 DownTo 0 Do
        TJpegImage(DataImp_SchemasEtgs[m_NoImg]).Destroy;
    FreeAndNil(DataImp_SchemasEtgs);
  End;

  { Sch�mas tout }
  If DataImp_SchemasTout <> Nil Then Begin
    For m_NoImg := 0 To DataImp_SchemasTout.Count - 1 Do Begin
      Table_Etude_DataExport.Donnees.Insert;
      m_StrNo := IntToStr(m_NoImg + 1);
      If Length(m_StrNo) = 1 Then m_StrNo := '0' + m_StrNo;
      Table_Etude_DataExport.Donnees.FieldByName('TYPE_EXPORT').AsWideString := m_Prefix + cst_ImpSchTout + m_StrNo;
      m_Img := TJPEGImage(DataImp_SchemasTout[m_NoImg]);
      m_Stream := TMemoryStream.Create;
      m_Img.SaveToStream(m_Stream);
      (Table_Etude_DataExport.Donnees.FieldByName('DATA_EXPORT') As TBlobField).LoadFromStream(m_Stream);
      FreeAndNil(m_Stream);
      Table_Etude_DataExport.Donnees.Post;
    End;
  End;
  If DataImp_SchemasTout <> Nil Then Begin
    For m_NoImg := DataImp_SchemasTout.Count - 1 DownTo 0 Do
        TJpegImage(DataImp_SchemasTout[m_NoImg]).Destroy;
    FreeAndNil(DataImp_SchemasTout);
  End;

  { Sch�mas terrasses }
  If DataImp_SchemasTerrasses <> Nil Then Begin
    For m_NoImg := 0 To DataImp_SchemasTerrasses.Count - 1 Do Begin
      Table_Etude_DataExport.Donnees.Insert;
      m_StrNo := IntToStr(m_NoImg + 1);
      If Length(m_StrNo) = 1 Then m_StrNo := '0' + m_StrNo;
      Table_Etude_DataExport.Donnees.FieldByName('TYPE_EXPORT').AsWideString := m_Prefix + cst_ImpSchTerrasse + m_StrNo;
      m_Img := TJPEGImage(DataImp_SchemasTerrasses[m_NoImg]);
      m_Stream := TMemoryStream.Create;
      m_Img.SaveToStream(m_Stream);
      (Table_Etude_DataExport.Donnees.FieldByName('DATA_EXPORT') As TBlobField).LoadFromStream(m_Stream);
      FreeAndNil(m_Stream);
      Table_Etude_DataExport.Donnees.Post;
    End;
  End;
  If DataImp_SchemasTerrasses <> Nil Then Begin
    For m_NoImg := DataImp_SchemasTerrasses.Count - 1 DownTo 0 Do
        TJpegImage(DataImp_SchemasTerrasses[m_NoImg]).Destroy;
    FreeAndNil(DataImp_SchemasTerrasses);
  End;

  { Sch�mas colonnes }
  If DataImp_SchemasColonnes <> Nil Then Begin
    For m_NoImg := 0 To DataImp_SchemasColonnes.Count - 1 Do Begin
      Table_Etude_DataExport.Donnees.Insert;
      m_StrNo := IntToStr(m_NoImg + 1);
      If Length(m_StrNo) = 1 Then m_StrNo := '0' + m_StrNo;
      Table_Etude_DataExport.Donnees.FieldByName('TYPE_EXPORT').AsWideString := m_Prefix + cst_ImpSchColonn + m_StrNo;
      m_Img := TJPEGImage(DataImp_SchemasColonnes[m_NoImg]);
      m_Stream := TMemoryStream.Create;
      m_Img.SaveToStream(m_Stream);
      (Table_Etude_DataExport.Donnees.FieldByName('DATA_EXPORT') As TBlobField).LoadFromStream(m_Stream);
      FreeAndNil(m_Stream);
      Table_Etude_DataExport.Donnees.Post;
    End;
  End;
  If DataImp_SchemasColonnes <> Nil Then Begin
    For m_NoImg := DataImp_SchemasColonnes.Count - 1 DownTo 0 Do
        TJpegImage(DataImp_SchemasColonnes[m_NoImg]).Destroy;
    FreeAndNil(DataImp_SchemasColonnes);
  End;


  { Quantitatif }
  If DataImp_Quantitatif <> nil Then Begin
    Table_Etude_DataExport.Donnees.Insert;
    Table_Etude_DataExport.Donnees.FieldByName('TYPE_EXPORT').AsWideString := m_Prefix + cst_ImpQuantites;
    m_Stream := TMemoryStream.Create;
    DataImp_Quantitatif.SaveToStream(m_Stream);
    (Table_Etude_DataExport.Donnees.FieldByName('DATA_EXPORT') As TBlobField).LoadFromStream(m_Stream);
    FreeAndNil(m_Stream);
    Table_Etude_DataExport.Donnees.Post;
  End;
FreeAndNil(DataImp_Quantitatif);

  { Fiche Caisson }
  If DataImp_FicheCaisson <> nil Then Begin
    Table_Etude_DataExport.Donnees.Insert;
    Table_Etude_DataExport.Donnees.FieldByName('TYPE_EXPORT').AsWideString := m_Prefix + cst_ImpFicheCaisson;
    m_Stream := TMemoryStream.Create;
    DataImp_FicheCaisson.SaveToStream(m_Stream);
    (Table_Etude_DataExport.Donnees.FieldByName('DATA_EXPORT') As TBlobField).LoadFromStream(m_Stream);
    FreeAndNil(m_Stream);
    Table_Etude_DataExport.Donnees.Post;
  End;
FreeAndNil(DataImp_FicheCaisson);

{$IFDEF  VIM_SELECTIONCAISSON}
  { Courbes Caisson }
  If DataImp_CourbesCaisson <> Nil Then Begin
    For m_NoImg := 0 To DataImp_CourbesCaisson.Count - 1 Do Begin
      Table_Etude_DataExport.Donnees.Insert;
      m_StrNo := IntToStr(m_NoImg + 1);
      If Length(m_StrNo) = 1 Then m_StrNo := '0' + m_StrNo;
      Table_Etude_DataExport.Donnees.FieldByName('TYPE_EXPORT').AsWideString := m_Prefix + cst_ImpCourbesCaisson + m_StrNo;
      m_ImgImageCaisson := TPrintImageCaisson(DataImp_CourbesCaisson[m_NoImg]);
        if m_ImgImageCaisson <> nil then
                Begin
                {$IFDEF  VIM_SELECTIONCAISSON}
                Table_Etude_DataExport.Donnees.FieldByName('ID_CAISSON').AsWideString := m_ImgImageCaisson.IDCaisson;
                Table_Etude_DataExport.Donnees.FieldByName('REFERENCE').AsWideString := m_ImgImageCaisson.Reference;
                {$ENDIF}
                m_Stream := TMemoryStream.Create;
                try
                m_ImgImageCaisson.SaveToStream(m_Stream);
                (Table_Etude_DataExport.Donnees.FieldByName('DATA_EXPORT') As TBlobField).LoadFromStream(m_Stream);
                except
                end;
                FreeAndNil(m_Stream);
                End;
      Table_Etude_DataExport.Donnees.Post;
    End;
  End;
  If DataImp_CourbesCaisson <> Nil Then Begin
{
    For m_NoImg := DataImp_CourbesCaisson.Count - 1 DownTo 0 Do
        Begin
        if DataImp_CourbesCaisson[m_NoImg] <> nil then
                TPrintImageCaisson(DataImp_CourbesCaisson[m_NoImg]).Destroy;
        End;
}
    FreeAndNil(DataImp_CourbesCaisson);
  End;

  { Images Caisson }
  If DataImp_ImagesCaisson <> Nil Then Begin
    For m_NoImg := 0 To DataImp_ImagesCaisson.Count - 1 Do Begin
      Table_Etude_DataExport.Donnees.Insert;
      m_StrNo := IntToStr(m_NoImg + 1);
      If Length(m_StrNo) = 1 Then m_StrNo := '0' + m_StrNo;
      Table_Etude_DataExport.Donnees.FieldByName('TYPE_EXPORT').AsWideString := m_Prefix + cst_ImpImagesCaisson + m_StrNo;
      m_ImgImageCaisson := TPrintImageCaisson(DataImp_ImagesCaisson[m_NoImg]);
        if m_ImgImageCaisson <> nil then
                Begin
                {$IFDEF  VIM_SELECTIONCAISSON}
                Table_Etude_DataExport.Donnees.FieldByName('ID_CAISSON').AsWideString := m_ImgImageCaisson.IDCaisson;
                Table_Etude_DataExport.Donnees.FieldByName('REFERENCE').AsWideString := m_ImgImageCaisson.Reference;
                {$ENDIF}
                m_Stream := TMemoryStream.Create;
                try
                m_ImgImageCaisson.SaveToStream(m_Stream);
                (Table_Etude_DataExport.Donnees.FieldByName('DATA_EXPORT') As TBlobField).LoadFromStream(m_Stream);
                except
                end;
                FreeAndNil(m_Stream);
                End;
      Table_Etude_DataExport.Donnees.Post;
    End;
  End;
  If DataImp_ImagesCaisson <> Nil Then Begin
{
    For m_NoImg := DataImp_ImagesCaisson.Count - 1 DownTo 0 Do
        Begin
                if DataImp_ImagesCaisson[m_NoImg] <> nil then
                        TPrintImageCaisson(DataImp_ImagesCaisson[m_NoImg]).Destroy;
        End;
}
    FreeAndNil(DataImp_ImagesCaisson);
  End;

{$ELSE}
  { Courbes Caisson }
  If DataImp_CourbesCaisson <> Nil Then Begin
    For m_NoImg := 0 To DataImp_CourbesCaisson.Count - 1 Do Begin
      Table_Etude_DataExport.Donnees.Insert;
      m_StrNo := IntToStr(m_NoImg + 1);
      If Length(m_StrNo) = 1 Then m_StrNo := '0' + m_StrNo;
      Table_Etude_DataExport.Donnees.FieldByName('TYPE_EXPORT').AsWideString := m_Prefix + cst_ImpCourbesCaisson + m_StrNo;
      m_Img := TJPEGImage(DataImp_CourbesCaisson[m_NoImg]);
        if m_Img <> nil then
                Begin
                m_Stream := TMemoryStream.Create;
                try
                m_Img.SaveToStream(m_Stream);
                (Table_Etude_DataExport.Donnees.FieldByName('DATA_EXPORT') As TBlobField).LoadFromStream(m_Stream);
                except
                end;
                FreeAndNil(m_Stream);
                End;
      Table_Etude_DataExport.Donnees.Post;
    End;
  End;
  If DataImp_CourbesCaisson <> Nil Then Begin
    For m_NoImg := DataImp_CourbesCaisson.Count - 1 DownTo 0 Do
        Begin
        if DataImp_CourbesCaisson[m_NoImg] <> nil then
                TJpegImage(DataImp_CourbesCaisson[m_NoImg]).Destroy;
        End;
    FreeAndNil(DataImp_CourbesCaisson);
  End;

  { Images Caisson }
  If DataImp_ImagesCaisson <> Nil Then Begin
    For m_NoImg := 0 To DataImp_ImagesCaisson.Count - 1 Do Begin
      Table_Etude_DataExport.Donnees.Insert;
      m_StrNo := IntToStr(m_NoImg + 1);
      If Length(m_StrNo) = 1 Then m_StrNo := '0' + m_StrNo;
      Table_Etude_DataExport.Donnees.FieldByName('TYPE_EXPORT').AsWideString := m_Prefix + cst_ImpImagesCaisson + m_StrNo;
      m_Img := TJPEGImage(DataImp_ImagesCaisson[m_NoImg]);
        if m_Img <> nil then
                Begin
                m_Stream := TMemoryStream.Create;
                try
                m_Img.SaveToStream(m_Stream);
                (Table_Etude_DataExport.Donnees.FieldByName('DATA_EXPORT') As TBlobField).LoadFromStream(m_Stream);
                except
                end;
                FreeAndNil(m_Stream);
                End;
      Table_Etude_DataExport.Donnees.Post;
    End;
  End;
  If DataImp_ImagesCaisson <> Nil Then Begin
    For m_NoImg := DataImp_ImagesCaisson.Count - 1 DownTo 0 Do
        Begin
                if DataImp_ImagesCaisson[m_NoImg] <> nil then
                        TJpegImage(DataImp_ImagesCaisson[m_NoImg]).Destroy;
        End;
    FreeAndNil(DataImp_ImagesCaisson);
  End;
{$ENDIF}

  { Donn�es acoustiques }
  If DataImp_Acoustique <> nil Then Begin
    Table_Etude_DataExport.Donnees.Insert;
    Table_Etude_DataExport.Donnees.FieldByName('TYPE_EXPORT').AsWideString := m_Prefix + cst_ImpAcoustique;
    m_Stream := TMemoryStream.Create;
    DataImp_Acoustique.SaveToStream(m_Stream);
    (Table_Etude_DataExport.Donnees.FieldByName('DATA_EXPORT') As TBlobField).LoadFromStream(m_Stream);
    FreeAndNil(m_Stream);
    Table_Etude_DataExport.Donnees.Post;
  End;
FreeAndNil(DataImp_Acoustique);

  { Erreurs }
  If (DataImp_Erreurs <> nil) And (DataImp_Erreurs.Count > 0) Then  Begin
    Table_Etude_DataExport.Donnees.Insert;
    Table_Etude_DataExport.Donnees.FieldByName('TYPE_EXPORT').AsWideString := m_Prefix + cst_ImpErreurs;
    m_Stream := TMemoryStream.Create;
    DataImp_Erreurs.SaveToStream(m_Stream);
    (Table_Etude_DataExport.Donnees.FieldByName('DATA_EXPORT') As TBlobField).LoadFromStream(m_Stream);
    FreeAndNil(m_Stream);
    Table_Etude_DataExport.Donnees.Post;
  End;
FreeAndNil(DataImp_Erreurs);
{$EndIf}
End;


{ ************************************************************************************************************************************************** }
Procedure TVentil_Etude.Vider_DataExport;
{$IfNDef AERAU_CLIMAWIN}
Var
  m_NoImg : Integer;
{$EndIf}
Begin
{$IfNDef AERAU_CLIMAWIN}
  FreeAndNil(DataImp_DonneesGenerale);
  FreeAndNil(DataImp_AvisTechniques);
  FreeAndNil(DataImp_DonneesEntreesAir);
  FreeAndNil(DataImp_CalculRetour);
  FreeAndNil(DataImp_Erreurs);
  FreeAndNil(DataImp_Acoustique);
  FreeAndNil(DataImp_Quantitatif);
  FreeAndNil(DataImp_FicheCaisson);

  If DataImp_SchemasColonnes <> Nil Then Begin
    For m_NoImg := DataImp_SchemasColonnes.Count - 1 DownTo 0 Do
        TJpegImage(DataImp_SchemasColonnes[m_NoImg]).Destroy;
    FreeAndNil(DataImp_SchemasColonnes);
  End;

  If DataImp_SchemasEtgs <> Nil Then Begin
    For m_NoImg := DataImp_SchemasEtgs.Count - 1 DownTo 0 Do
        TJpegImage(DataImp_SchemasEtgs[m_NoImg]).Destroy;
    FreeAndNil(DataImp_SchemasEtgs);
  End;

  If DataImp_SchemasTout <> Nil Then Begin
    For m_NoImg := DataImp_SchemasTout.Count - 1 DownTo 0 Do
        TJpegImage(DataImp_SchemasTout[m_NoImg]).Destroy;
    FreeAndNil(DataImp_SchemasTout);
  End;

  If DataImp_SchemasTerrasses <> Nil Then Begin
    For m_NoImg := DataImp_SchemasTerrasses.Count - 1 DownTo 0 Do
        TJpegImage(DataImp_SchemasTerrasses[m_NoImg]).Destroy;
    FreeAndNil(DataImp_SchemasTerrasses);
  End;

  If DataImp_CourbesCaisson <> Nil Then Begin
    For m_NoImg := DataImp_CourbesCaisson.Count - 1 DownTo 0 Do
        Begin
        if DataImp_CourbesCaisson[m_NoImg] <> nil then
                TPrintImageCaisson(DataImp_CourbesCaisson[m_NoImg]).Destroy;
        End;
    FreeAndNil(DataImp_CourbesCaisson);
  End;

  If DataImp_ImagesCaisson <> Nil Then Begin
    For m_NoImg := DataImp_ImagesCaisson.Count - 1 DownTo 0 Do
        Begin
                if DataImp_ImagesCaisson[m_NoImg] <> nil then
                        TPrintImageCaisson(DataImp_ImagesCaisson[m_NoImg]).Destroy;
        End;
    FreeAndNil(DataImp_ImagesCaisson);
  End;

{$EndIf}
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Etude.Charger_DataExport_ImagesCaissons(Var _Caisson : TVentil_Caisson);
  function GenereTempFile : String;
  Var
    tempID : TGUID;
  begin
  CreateGuid(tempID);
    Result := TempPath + GUIDToString(tempID) + '.jpg';
  end;
{$IFDEF  VIM_SELECTIONCAISSON}
Var
  m_Prefix : String;
  tempType_Export : String;
  tempNumber : Integer;
  tempFile : String;
{$EndIf}
begin
{$IFDEF  VIM_SELECTIONCAISSON}
    m_Prefix := NoOptair;

      if m_Prefix = '' then
        begin
        m_Prefix := ExtractFileName(Fichier);
        m_Prefix := ChangeFileExt(m_Prefix, '');
        end;
{
    Condition := 'ID_CAISSON=''' + GUIDToString(_Caisson.ID)  + '''';
    Table_Etude_DataExport.Donnees.Filter := Condition;
    Table_Etude_DataExport.Donnees.Filtered := True;
}
    _Caisson.FicheCaisson_Courbe.Clear;

  Table_Etude_DataExport.Donnees.First;
  While Not Table_Etude_DataExport.Donnees.Eof Do
    begin
    tempType_Export := Table_Etude_DataExport.Donnees.FieldByName('TYPE_EXPORT').AsWideString;
      if Pos(cst_ImpCourbesCaisson, tempType_Export) > 0 then
        begin
//        tempType_Export := StrReplace(tempType_Export, m_Prefix + cst_ImpCourbesCaisson, '');
          if Pos(m_Prefix, tempType_Export) > 0 then
            tempType_Export := StrReplace(tempType_Export, m_Prefix, '')
          else
            tempType_Export := StrReplace(tempType_Export, Copy(tempType_Export, 1, Pos('_BBS', tempType_Export) - 1), '');
        tempType_Export := StrReplace(tempType_Export, cst_ImpCourbesCaisson, '');
        tempNumber := Str2Int(tempType_Export);
          while _Caisson.FicheCaisson_Courbe.Count < tempNumber do
            begin
            _Caisson.FicheCaisson_Courbe.Add;
            end;
        _Caisson.FicheCaisson_Courbe[tempNumber - 1].IDCaisson := GUIDToString(_Caisson.Id);
        _Caisson.FicheCaisson_Courbe[tempNumber - 1].Reference := Table_Etude_DataExport.Donnees.FieldByName('REFERENCE').AsWideString;
        tempFile := GenereTempFile;
        (Table_Etude_DataExport.Donnees.FieldByName('DATA_EXPORT') As TBlobField).SaveToFile(tempFile);
        _Caisson.FicheCaisson_Courbe[tempNumber - 1].LoadFromFile(tempFile);
          if FileExists(tempFile) then
            SysUtils.DeleteFile(tempFile);
        end
      else
      if Pos(cst_ImpImagesCaisson, tempType_Export) > 0 then
        begin
//        tempType_Export := StrReplace(tempType_Export, m_Prefix + cst_ImpImagesCaisson, '');
          if Pos(m_Prefix, tempType_Export) > 0 then
            tempType_Export := StrReplace(tempType_Export, m_Prefix, '')
          else
            tempType_Export := StrReplace(tempType_Export, Copy(tempType_Export, 1, Pos('_BBS', tempType_Export) - 1), '');
        tempType_Export := StrReplace(tempType_Export, cst_ImpImagesCaisson, '');
        tempNumber := Str2Int(tempType_Export);
        tempFile := GenereTempFile;
        (Table_Etude_DataExport.Donnees.FieldByName('DATA_EXPORT') As TBlobField).SaveToFile(tempFile);
          Case tempNumber of
            1 : begin
                  if _Caisson.FicheCaisson_Schema = Nil then
                    _Caisson.FicheCaisson_Schema := TPrintImageCaisson.Create;
                _Caisson.FicheCaisson_Schema.LoadFromFile(tempFile);
                _Caisson.FicheCaisson_Schema.IDCaisson := GUIDToString(_Caisson.Id);
                _Caisson.FicheCaisson_Schema.Reference := Table_Etude_DataExport.Donnees.FieldByName('REFERENCE').AsWideString;
                end;
            2 : begin
                  if _Caisson.FicheCaisson_Photo = Nil then
                    _Caisson.FicheCaisson_Photo := TPrintImageCaisson.Create;
                _Caisson.FicheCaisson_Photo.LoadFromFile(tempFile);
                _Caisson.FicheCaisson_Photo.IDCaisson := GUIDToString(_Caisson.Id);
                _Caisson.FicheCaisson_Photo.Reference := Table_Etude_DataExport.Donnees.FieldByName('REFERENCE').AsWideString;
                end;
          End;
          if FileExists(tempFile) then
            SysUtils.DeleteFile(tempFile);
        end;
    Table_Etude_DataExport.Donnees.Next;
    end;


//  Table_Etude_Caisson.Donnees.Filtered := False;
{$ENDIF}
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Etude.Analyse(_ElementCad : IAero_Vilo_Base; _Parent : TVentil_Troncon);
Var
  m_No     : Integer;
  m_Tr     : TVentil_Troncon;

  tempstrParent : String;
  tempstr : String;

  temParent : TVentil_Troncon;
Begin
  If _ElementCad.IAero_Vilo_Base_ObjFredo <> Nil Then m_Tr := TVentil_Troncon(_ElementCad.IAero_Vilo_Base_ObjFredo.IAero_Fredo_Base_Objet)
                                                 Else m_Tr := Nil;
  If IsNotNil(m_Tr) Then Begin
    m_Tr.UtiliseCalcul := True;
    If m_Tr.InheritsFrom(TVentil_Caisson) Then Begin
      Batiment.Reseaux.Add(m_Tr);
      m_Tr.Reseau := Batiment.Reseaux;
    End;
    If IsNotNil(_Parent) Then Begin
    tempstrParent := GUIDToString(_Parent.Id);
    tempstr := GUIDToString(m_Tr.Id);
//      if _Parent.Fils.IndexOf(m_Tr) = -1 then
      _Parent.Fils.Add(m_Tr);
      m_Tr.Parent := _Parent;
      m_Tr.InfosFromCad;
      m_Tr.Reseau := _Parent.Reseau;
    End;
  End Else m_Tr := _Parent;
  For m_No := 0 To _ElementCad.IAero_Vilo_Base_Count - 1 Do
    begin
      If Supports(_ElementCad.IAero_Vilo_Base_Fils(m_No), IAero_EXT_Interm�diaire) then
        temParent := TVentil_Troncon(_ElementCad.IAero_Vilo_Base_Fils(0).IAero_Vilo_Base_ObjFredo.IAero_Fredo_Base_Objet)
      else
      if Supports(_ElementCad.IAero_Vilo_Base_Fils(m_No), IAero_EXT_Plenum) then
        temParent := TVentil_Troncon(_ElementCad.IAero_Vilo_Base_Fils(1).IAero_Vilo_Base_ObjFredo.IAero_Fredo_Base_Objet)
      else
        temParent := m_Tr;
    Analyse(_ElementCad.IAero_Vilo_Base_Fils(m_No), temParent);
    end;
End;
{ ******************************************************************* \ TVentil_Etude ***************************************************************** }

End.
