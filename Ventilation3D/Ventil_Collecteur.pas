Unit Ventil_Collecteur;

{$Include 'Ventil_Directives.pas'}

Interface
Uses
  Classes, SysUtils, advGrid, BaseGrid, AdvObj,

  Ventil_Types, Ventil_Troncon, Ventil_Logement, Ventil_Utils,
  XMLIntf,
  BBScad_Interface_Aeraulique;

Type
{ ************************************************************************************************************************************************** }
  TVentil_Collecteur = Class(TVentil_Troncon)

    Logement       : TVentil_Logement;
    AnglePiquages  : Integer;
    PiquageExpress : Integer;
    TypeTrainasse : Integer;
    Constructor Create; Override;
  Private
    Function  GetElevation : Real;
    Procedure SetElevation(_Value: Real);
    Function  TypeCollecteur: Integer;
    Procedure AddQuantitatifReduction;
  Public
    HasReduction : Boolean;
    DiametreReduit : Integer;

    class Function FamilleObjet : String; Override;
    
    Property  Elevation : Real read GetElevation write setelevation;
    Procedure CalculeRecapitulatif(_Iteratif : Boolean); Override;
    Procedure ImportXMLActhys(_Node : IXMLNode); Override;
    Procedure ExportXMLActhys(_Node : IXMLNode); Override;
    Procedure ExportXMLActhys2ndPasse(_Node : IXMLNode); Override;
    Procedure InfosFromCad; Override;
    Procedure UpdateAfterCreate; Override;
    Procedure Paste(_Source: TVentil_Objet); Override;
    Procedure Load; Override;

    Procedure Affiche (_Grid: TAdvStringGrid); Override;
    Procedure AfficheResultats(_Grid: TAdvStringGrid); Override;
    Procedure Recupere(_Grid: TAdvStringGrid; _Ligne: Integer); Override;
    Function  TypeEdition(_Grid: TAdvStringGrid; _Ligne: Integer): TEditorType; Override;
    Procedure PropEdition(_Grid: TAdvStringGrid; _Ligne: Integer); Override;
    Function  HasComboBox(_Ligne: Integer): Boolean; Override;
    Function DessertTrainasseRectangulaire : Boolean;
    Function  AfficheQuestion(_NoQuestion: Integer): Boolean; Override;
    Procedure MiseAjourPiquageExpressVertical; Override;
    Procedure MiseAjourPiquageExpressHorizontal; Override;
    Function SortieRectiligne : TVentil_Troncon;
    Function SommeVitesseMaxiSortiesLaterales : Single;
    Function SommeDebitMaxiSortiesLaterales : Single;
    Function SommeDebitMiniSortiesLaterales : Single;
    Function PremiereSortielaterale : TVentil_Troncon;
    Function  FilsIsBrancheLaterale(_Fils: TVentil_Troncon): Boolean;
    Procedure MakeLienCalculs; Override;
    Function NeedDiminutionEmmanchement : Boolean; Override;

    Function CanAddAccidentAmont : Boolean; Override;
  Protected
    class Function NomParDefaut : String; Override;
    Procedure IAero_Fredo_Base_RemplacerPar(Objet : IAero_Fredo_Base); Override;
    Function  ImageAssociee: String; Override;
    Procedure GenerationAccidents; Override;
    Procedure Save; Override;

    Procedure AddAccidentColonneVMC(_Porteur: TVentil_Troncon);
    Procedure AddAccidentColonneShunt(_Porteur: TVentil_Troncon);
    Procedure AddAccidentColonneIndividuel(_Porteur: TVentil_Troncon);
    Procedure AddAccidentColonneAlsace(_Porteur: TVentil_Troncon);
    Procedure AddAccidentColonneRamon(_Porteur: TVentil_Troncon);

    Procedure CalculerQuantitatifInternal; Override;
    Function IsEtiquetteVisible : boolean; Override;
    Procedure InitEtiquette; Override;
    Function GetListBouchesFilles : TList;

    function GetLogementTroncon : TVentil_Objet; Override;
  End;
{ ************************************************************************************************************************************************** }

Implementation
Uses
  Math,
  Grids,
  Ventil_Caisson,
  BbsgFonc,
{$IFDEF ACTHYS_DIMVMBP}
  Variants,
  ImportEVenthysXSD,
  ExportEVenthysXSD,
{$ENDIF}
  Ventil_Firebird, Ventil_Const,
  Ventil_TronconDessin,
  Ventil_TeSouche, Ventil_Accident, Ventil_Declarations, Ventil_EdibatecProduits,
  Ventil_Options,
  Ventil_Edibatec, Ventil_Batiment, Ventil_SystemeVMC,
  Ventil_Bifurcation,
  Ventil_Colonne,
  Ventil_Plenum,
  Ventil_Bouche;

{ ******************************************************************* TVentil_Collecteur *************************************************************** }
Procedure TVentil_Collecteur.Paste(_Source: TVentil_Objet);
Begin
  Inherited;
  Logement      := TVentil_Collecteur(_Source).Logement;
  AnglePiquages := TVentil_Collecteur(_Source).AnglePiquages;
//  Elevation     := TVentil_Collecteur(_Source).Elevation;
  PiquageExpress     := TVentil_Collecteur(_Source).PiquageExpress;
  HasRegulateur := TVentil_Collecteur(_Source).HasRegulateur;
  TypeTrainasse := TVentil_Collecteur(_Source).TypeTrainasse;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Collecteur.IAero_Fredo_Base_RemplacerPar(Objet : IAero_Fredo_Base);
Begin
inherited;
        if Objet.IAero_Fredo_Base_Objet.InheritsFrom(TVentil_Collecteur) then
                Begin
                TVentil_Collecteur(Objet.IAero_Fredo_Base_Objet).Logement       := Logement;
                TVentil_Collecteur(Objet.IAero_Fredo_Base_Objet).HasRegulateur  := HasRegulateur;
                TVentil_Collecteur(Objet.IAero_Fredo_Base_Objet).PiquageExpress := PiquageExpress;
                TVentil_Collecteur(Objet.IAero_Fredo_Base_Objet).TypeTrainasse  := TypeTrainasse;
                End
        Else
        if Objet.IAero_Fredo_Base_Objet.InheritsFrom(TVentil_Bifurcation) then
                Begin
                TVentil_Bifurcation(Objet.IAero_Fredo_Base_Objet).HasRegulateur  := HasRegulateur;
                TVentil_Bifurcation(Objet.IAero_Fredo_Base_Objet).PiquageExpress := PiquageExpress;
                End;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Collecteur.Affiche(_Grid: TAdvStringGrid);
Var
  m_Row      : Integer;
  m_Question : Integer;
  m_Str      : String;
Begin
  Inherited;
  m_Row := GetNbQuestionAfficheTroncon + 1;
  For m_Question := Low(c_LibellesQ_Collecteur) To High(c_LibellesQ_Collecteur) Do If AfficheQuestion(m_Question) Then Begin
    _Grid.Cells[0, m_Row] := c_LibellesQ_Collecteur[m_Question];
    Case m_Question Of
      c_QCollecteur_Logement  : If IsNotNil(Logement) Then m_Str := Logement.Reference
                                                      Else m_Str := ' *** ';
      c_QCollecteur_Elevation : m_Str := Float2Str(Elevation, 2);
      c_QCollecteur_PiquageExpress : m_Str := c_OuiNon[PiquageExpress];
      c_QCollecteur_TypeTrainasse : m_Str := c_CollecteurTypeTrainasseActhys[TypeTrainasse];
      Else m_Str := '';
    End;
    _Grid.Cells[1, m_Row] := m_Str;
    Inc(m_Row);
  End;
End;
{ ************************************************************************************************************************************************** }
{$IFDEF ACTHYS_DIMVMBP}
Procedure TVentil_Collecteur.AfficheResultats(_Grid: TAdvStringGrid);
Begin
  Inherited;
  If IsNotNil(Logement) Then Begin
    _Grid.Cells[1, 0] := Logement.Reference ;
    _Grid.Cells[3, 0] := Logement.LibTypeLogement;
  End;
End;
{$Else}
{$IFDEF MVN_MVNAIR}
Procedure TVentil_Collecteur.AfficheResultats(_Grid: TAdvStringGrid);
Begin
  Inherited;
  If IsNotNil(Logement) Then Begin
    _Grid.Cells[1, 0] := Logement.Reference ;
    _Grid.Cells[3, 0] := Logement.LibTypeLogement;
  End;
End;
{$Else}
Procedure TVentil_Collecteur.AfficheResultats(_Grid: TAdvStringGrid);
Begin
  Inherited;
  If IsNotNil(Logement) Then Begin
    _Grid.Cells[1, 0] := Logement.Reference ;
    _Grid.Cells[2, 0] := Logement.LibTypeLogement;
  End;
End;
{$EndIf}
{$EndIf}
{ ************************************************************************************************************************************************** }
Procedure TVentil_Collecteur.MiseAjourPiquageExpressVertical;
Begin
        if Not(SurTerrasse) then
                PiquageExpress := Etude.Batiment.PiquageExpressVertical;
inherited;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Collecteur.MiseAjourPiquageExpressHorizontal;
Begin
        if SurTerrasse then
                PiquageExpress := Etude.Batiment.PiquageExpressHorizontal;
inherited;
End;
{ ************************************************************************************************************************************************** }
Function TVentil_Collecteur.SortieRectiligne : TVentil_Troncon;
Var
  m_NoSortie : Integer;
Begin
  Result := Nil;
  For m_NoSortie := 0 To Fils.Count - 1 Do If Supports(Fils[m_NoSortie].LienCad, IAero_Collecteur_SortieR) Then
        Begin
//                if Fils[m_NoSortie].Fils.Count > 0 then
                        Begin
                        Result := Fils[m_NoSortie];
                        Break;
                        End;
        End;
End;
{ ************************************************************************************************************************************************** }
Function TVentil_Collecteur.SommeVitesseMaxiSortiesLaterales : Single;
Var
  m_NoSortie : Integer;
Begin
  Result := 0;
  For m_NoSortie := 0 To Fils.Count - 1 Do If Supports(Fils[m_NoSortie].LienCad, IAero_Collecteur_SortieL) Then
    Result := Result + Fils[m_NoSortie].Vitesse;
End;
{ ************************************************************************************************************************************************** }
Function TVentil_Collecteur.SommeDebitMaxiSortiesLaterales : Single;
Var
  m_NoSortie : Integer;
Begin
  Result := 0;
  For m_NoSortie := 0 To Fils.Count - 1 Do If Supports(Fils[m_NoSortie].LienCad, IAero_Collecteur_SortieL) Then
    Result := Result + Fils[m_NoSortie].DebitMaxi;
end;
{ ************************************************************************************************************************************************** }
Function TVentil_Collecteur.SommeDebitMiniSortiesLaterales : Single;
Var
  m_NoSortie : Integer;
Begin
  Result := 0;
  For m_NoSortie := 0 To Fils.Count - 1 Do If Supports(Fils[m_NoSortie].LienCad, IAero_Collecteur_SortieL) Then
    Result := Result + Fils[m_NoSortie].DebitMini;
end;
{ ************************************************************************************************************************************************** }
Function TVentil_Collecteur.PremiereSortielaterale : TVentil_Troncon;
Var
  m_NoSortie : Integer;
Begin
  Result := Nil;
  For m_NoSortie := 0 To Fils.Count - 1 Do If Supports(Fils[m_NoSortie].LienCad, IAero_Collecteur_SortieL) Then
        Begin
//                if Fils[m_NoSortie].Fils.Count > 0 then
                        Begin
                        Result := Fils[m_NoSortie];
                        Break;
                        End;
        End;
End;
{ ************************************************************************************************************************************************** }
Function TVentil_Collecteur.DessertTrainasseRectangulaire : Boolean;
begin
Result := False;
  if Etude.Batiment.IsFabricantACT then
    Result := HasParentColonnePlenum and (TypeTrainasse = c_TypeTrainasseT2A)
  else
  if Etude.Batiment.IsFabricantMVN then
    begin
      if HasParentColonnePlenum then
        begin
          if GetParentPlenum <> Nil then
            Result := TVentil_Plenum(GetParentPlenum).TypeTrainasse = c_TypeTrainassePlThAir;
        end;
    end
  else
    Result := False;
end;
{ ************************************************************************************************************************************************** }
Function TVentil_Collecteur.AfficheQuestion(_NoQuestion: Integer): Boolean;
Begin
  If _NoQuestion <= c_NbQ_Troncon Then Result := Inherited AfficheQuestion(_NoQuestion)
  Else Case _NoQuestion Of
    c_QCollecteur_Logement       : Result := True;
    c_QCollecteur_Elevation      : Result := True;

    c_QCollecteur_PiquageExpress : {$IfNDef AERAU_CLIMAWIN}
                                  If Etude.Batiment.IsFabricantVIM then
                                        Result := not((not(Etude.Batiment.SystemeCollectif.Gaz) and (Etude.Batiment.MatiereAccessoires = c_AccessoiresVeloduct)) or ((Etude.Batiment.SystemeCollectif.Gaz) and ((Etude.Batiment.MatiereAccessoiresHor = c_AccessoiresSiGazVeloduct) or (Etude.Batiment.MatiereAccessoiresVert = c_AccessoiresSiGazVeloduct))))
                                   Else
                                   {$EndIf}
                                        Result := False;
    c_QCollecteur_TypeTrainasse : if Etude.Batiment.IsFabricantACT and HasParentColonnePlenum then
                                    Result := False
                                  else
                                    Result := False;
    Else Result := False;
  End;

  if Etude.Batiment.IsFabricantACT or Etude.Batiment.IsFabricantMVN then
  if _NoQuestion = c_QTroncon_NatureGaine then
    Result := False;
End;
{ ************************************************************************************************************************************************** }
Constructor TVentil_Collecteur.Create;
Begin
  inherited;
  NbQuestions := NbQuestions + c_NbQ_Collecteur;
  If Etude.Batiment.Logements.Count > 0 Then Logement := Etude.Batiment.Logements[0];
        If SurTerrasse then
                PiquageExpress := Etude.Batiment.PiquageExpressHorizontal
        else
                PiquageExpress := Etude.Batiment.PiquageExpressVertical;
    If Etude.Batiment.IsFabricantACT then
      TypeTrainasse := c_TypeTrainasseT2A
    else
      TypeTrainasse := c_TypeTrainasseAuto;
  HasReduction := False;
End;
{ ************************************************************************************************************************************************** }
class Function TVentil_Collecteur.NomParDefaut : String;
Begin
Result := 'Collecteur d''�tage';
End;
{ ************************************************************************************************************************************************** }
class Function TVentil_Collecteur.FamilleObjet : String;
Begin
Result := 'Collecteur d''�tage';
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Collecteur.ImportXMLActhys(_Node : IXMLNode);
{$IFDEF ACTHYS_DIMVMBP}
var
  _Logement: OleVariant;
  _TypeTrainasse : OleVariant;
  _tempParentColonne : TVentil_Troncon;
{$ENDIF}
begin
  Inherited;
    if _Node = Nil then
      Exit;
{$IFDEF ACTHYS_DIMVMBP}
  Reference := _Node.ChildValues['Ref_Etage'];
  _Logement := _Node.ChildValues['Id_Type_Logement_Collecteur'];
    if VarIsNull(_Logement) then
      Logement  := Nil
    else
      Logement  := Etude.Batiment.Logements.GetLogementFromIDXML(_Logement);

  _TypeTrainasse := _Node.ChildValues['Type_Trainasse'];
    if Not(VarIsNull(_TypeTrainasse)) then
      case _TypeTrainasse of
        1 : TypeTrainasse := c_TypeTrainasseT2A;
        2 : TypeTrainasse := c_TypeTrainasse125;
        3 : TypeTrainasse := c_TypeTrainasse160;
        4 : TypeTrainasse := c_TypeTrainasseAuto;
      end
    else
      begin
      //temporaire : tant que la balise n'est pas pr�sente, on d�duit la valeur de TypeTrainasse
      _tempParentColonne := GetParentTeSouche;
        if _tempParentColonne <> Nil then
          if _tempParentColonne.InheritsFrom(TVentil_Colonne) or _tempParentColonne.InheritsFrom(TVentil_Plenum) then
            TypeTrainasse := c_TypeTrainasseT2A
          else
            TypeTrainasse := c_TypeTrainasseAuto;
      end;


{$ENDIF}
end;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Collecteur.ExportXMLActhys(_Node : IXMLNode);
{$IFDEF ACTHYS_DIMVMBP}
Var
  _tempParentColonne : TVentil_Troncon;
  TempListBouches : TList;
  cpt : Integer;
{$EndIf}
begin
  Inherited;
{$IFDEF ACTHYS_DIMVMBP}
  (_Node as ExportEVenthysXSD.IXMLETAGE).Ref_Etage := Reference;
  (_Node as ExportEVenthysXSD.IXMLETAGE).Hauteur_Collecteur := Real2XML(Elevation - Etude.Batiment.HauteurDalleActhys);
  (_Node as ExportEVenthysXSD.IXMLETAGE).Num_Etage := (LienCad As IAero_Collecteur).IAero_Vilo_Base_Etage;
  (_Node as ExportEVenthysXSD.IXMLETAGE).Id_Type_Collecteur := (LienCad As IAero_Collecteur).IAero_Collecteur_TypeActhys;
    if Logement <> Nil then
      begin
        if Logement.IdImportXML > -1 then
          (_Node as ExportEVenthysXSD.IXMLETAGE).Id_Type_Logement_Collecteur := Logement.IdImportXML
        else
          (_Node as ExportEVenthysXSD.IXMLETAGE).Id_Type_Logement_Collecteur := Etude.Batiment.Logements.IndexOf(Logement);
      end;

  //temporaire : tant que l'option n'est pas visible, on d�duit la valeur de TypeTrainasse
  _tempParentColonne := GetParentTeSouche;
    if _tempParentColonne <> Nil then
      if _tempParentColonne.InheritsFrom(TVentil_Colonne) or _tempParentColonne.InheritsFrom(TVentil_Plenum) then
        TypeTrainasse := c_TypeTrainasseT2A
      else
        TypeTrainasse := c_TypeTrainasseAuto;

    case TypeTrainasse of
      c_TypeTrainasseT2A : (_Node as ExportEVenthysXSD.IXMLETAGE).Type_Trainasse := 1;
      c_TypeTrainasse125 : (_Node as ExportEVenthysXSD.IXMLETAGE).Type_Trainasse := 2;
      c_TypeTrainasse160 : (_Node as ExportEVenthysXSD.IXMLETAGE).Type_Trainasse := 3;
      c_TypeTrainasseAuto : (_Node as ExportEVenthysXSD.IXMLETAGE).Type_Trainasse := 4;
    end;

  TempListBouches := GetListBouchesFilles;
    for cpt := 0 to TempListBouches.Count - 1 do
      TVentil_Bouche(TempListBouches[cpt]).ExportXMLActhys((_Node as ExportEVenthysXSD.IXMLETAGE).BOUCHES.Add);
  TempListBouches.Destroy;
{$ENDIF}
end;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Collecteur.ExportXMLActhys2ndPasse(_Node : IXMLNode);
Var
  TempListBouches : TList;
  cpt : Integer;
begin
  Inherited;
{$IFDEF ACTHYS_DIMVMBP}
  TempListBouches := GetListBouchesFilles;
    for cpt := 0 to TempListBouches.Count - 1 do
      TVentil_Bouche(TempListBouches[cpt]).ExportXMLActhys2ndPasse((_Node as ExportEVenthysXSD.IXMLETAGE).BOUCHES.BOUCHE[cpt]);
  TempListBouches.Destroy;
{$ENDIF}
end;
{ ************************************************************************************************************************************************** }
Function TVentil_Collecteur.GetElevation : Real;
Begin
  Result := (LienCad As IAero_Collecteur).IAero_Collecteur_GetElevation;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Collecteur.SetElevation(_Value: Real);
Begin
  If (_Value > 0) And (_Value < Etude.HtEtageDef) Then Begin
    If _Value <> (LienCad As IAero_Collecteur).IAero_Collecteur_GetElevation Then
    (LienCad As IAero_Collecteur).IAero_Collecteur_SetElevation(_value);
  End;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Collecteur.Recupere(_Grid: TAdvStringGrid; _Ligne: Integer);
Begin
  If NoQuestion(_Ligne) <= c_NbQ_Troncon Then Inherited
  Else If AfficheQuestion(NoQuestion(_Ligne)) Then Case NoQuestion(_Ligne) Of
    c_QCollecteur_Logement  : Logement := Etude.Batiment.Logements[_Grid.Combobox.ItemIndex];
    c_QCollecteur_Elevation : Elevation := _Grid.Floats[1, _Ligne];
    c_QCollecteur_PiquageExpress : PiquageExpress       := _Grid.Combobox.ItemIndex;
    c_QCollecteur_TypeTrainasse : TypeTrainasse         := _Grid.Combobox.ItemIndex;
  End;
  Inherited;
End;
{ ************************************************************************************************************************************************** }
Function TVentil_Collecteur.TypeEdition(_Grid: TAdvStringGrid; _Ligne: Integer): TEditorType;
Var
  m_NoChoix : Integer;
Begin

  Result := edNone;
  If Etude.Batiment.ChiffrageVerouille  = c_OUI Then Exit;
    //Parfois le GoEditing saute sans raison...
  _Grid.Options := _Grid.Options + [goEditing];
  If NoQuestion(_Ligne) <= c_NbQ_Troncon Then Result := Inherited TypeEdition(_Grid, _Ligne)
  Else If AfficheQuestion(NoQuestion(_Ligne)) Then Case NoQuestion(_Ligne) Of
    c_QCollecteur_Logement  : If Etude.Batiment.Logements.Count = 0 Then Result := edNone
                                                                    Else Begin
                                                                      Result := edComboList;
                                                                    End;
    c_QCollecteur_Elevation : Result := edFloat;
    c_QCollecteur_PiquageExpress : Begin
      Result := edComboList;
    End;
    c_QCollecteur_TypeTrainasse : Begin
      Result := edComboList;
    End;
  End;

If Not(Etude.Batiment.IsFabricantVIM) then
 _Grid.Combobox.DropDownCount := Min(_Grid.Combobox.DropDownCount, 20);

End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Collecteur.PropEdition(_Grid: TAdvStringGrid; _Ligne: Integer);
Var
  m_NoChoix : Integer;
Begin

  If Etude.Batiment.ChiffrageVerouille  = c_OUI Then Exit;
  If NoQuestion(_Ligne) <= c_NbQ_Troncon Then Inherited
  Else If AfficheQuestion(NoQuestion(_Ligne)) Then Case NoQuestion(_Ligne) Of
    c_QCollecteur_Logement  : If Etude.Batiment.Logements.Count = 0 Then begin end
                                                                    Else Begin
                                                                      _Grid.ClearComboString;
                                                                      For m_NoChoix := 0 To Etude.Batiment.Logements.Count - 1 Do _Grid.AddComboString(Etude.Batiment.Logements[m_NoChoix].Reference);
                                                                    End;
    c_QCollecteur_PiquageExpress : Begin
      _Grid.ClearComboString;
      For m_NoChoix := Low(C_OUINON) To High(C_OUINON) Do _Grid.AddComboString(C_OUINON[m_NoChoix]);
    End;
    c_QCollecteur_TypeTrainasse : Begin
      _Grid.ClearComboString;
      For m_NoChoix := Low(c_CollecteurTypeTrainasseActhys) To High(c_CollecteurTypeTrainasseActhys) Do _Grid.AddComboString(C_CollecteurTypeTrainasseActhys[m_NoChoix]);
    End;
  End;

If Not(Etude.Batiment.IsFabricantVIM) then
 _Grid.Combobox.DropDownCount := Min(_Grid.Combobox.DropDownCount, 20);

End;
{ ************************************************************************************************************************************************** }
Function TVentil_Collecteur.HasComboBox(_Ligne: Integer): Boolean;
Begin
  If NoQuestion(_Ligne) <= c_NbQ_Troncon Then Result := Inherited HasComboBox(_Ligne)
  Else Result := AfficheQuestion(NoQuestion(_Ligne)) And (NoQuestion(_Ligne) In[c_QCollecteur_Logement, c_QCollecteur_PiquageExpress, c_QCollecteur_TypeTrainasse]);
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Collecteur.InfosFromCad;
Begin
  Inherited;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Collecteur.UpdateAfterCreate;
Begin

if (Etude = nil) or (Etude.Batiment = nil) then exit;
        If SurTerrasse then
                PiquageExpress := Etude.Batiment.PiquageExpressHorizontal
        Else
                PiquageExpress := Etude.Batiment.PiquageExpressVertical;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Collecteur.MakeLienCalculs;
Var
  m_NoTr : Integer;
  m_Pere : TVentil_Troncon;
Begin
  m_Pere := Parent;
  While IsNotNil(m_Pere) And ( Not (m_Pere.InheritsFrom(TVentil_TeSouche)) And Not ( Supports(m_Pere.LienCad, IAero_TSoucheEtage)) ) Do m_Pere := m_Pere.Parent;
  If IsNotNil(m_Pere) and m_Pere.InheritsFrom(TVentil_Accident_TeSoucheCoude) { Cas du te souche etage coude :-( }  Then Begin
    While (IsNotNil(m_Pere)) And ((m_Pere.InheritsFrom(TVentil_TronconDessin))  Or ( (m_Pere.InheritsFrom(TVentil_Accident)) And
                                                                                     (Not m_Pere.InheritsFrom(TVentil_Accident_ChangementDeDiametre)) ) ) Do m_Pere := m_Pere.Parent;
  End;
  If (m_Pere <> Nil) Then Begin
    If (m_Pere.InheritsFrom(TVentil_TeSouche)) And Not (Supports(m_Pere.LienCad, IAero_TSoucheEtage)) Then Begin
      m_Pere.FilsCalcul.Add(Self);
      m_Pere.FilsCalcul[m_Pere.FilsCalcul.Count - 1].Reseau := m_Pere.Reseau;
      Parentcalcul := m_Pere;
    End Else If Supports(m_Pere.LienCad, IAero_TSoucheEtage) Then Begin
      m_Pere.FilsCalcul.Add(Self);
      m_Pere.FilsCalcul[m_Pere.FilsCalcul.Count - 1].Reseau := m_Pere.Reseau;
      Parentcalcul := m_Pere;
    End Else Begin
      m_Pere.FilsCalcul.Add(Self);
      m_Pere.FilsCalcul[m_Pere.FilsCalcul.Count - 1].Reseau := m_Pere.Reseau;
      Parentcalcul := m_Pere;
      SurTerrasse  := False;
    End;
  End;
  For m_NoTr := 0 To Fils.Count - 1 Do Fils[m_NoTr].MakeLienCalculs;
End;
{ ************************************************************************************************************************************************** }
Function TVentil_Collecteur.NeedDiminutionEmmanchement : Boolean;
var
        m_ObjetTypeReseauAval : TVentil_Troncon;
Begin

Result := False;

         m_ObjetTypeReseauAval := Self;
                while (m_ObjetTypeReseauAval.Parent <> nil) and (m_ObjetTypeReseauAval.LienCad.IAero_Vilo_Base_TypeReseauAval = rNone) do
                        m_ObjetTypeReseauAval := m_ObjetTypeReseauAval.Parent;
//Result := (m_ObjetTypeReseauAval.LienCad.IAero_Vilo_Base_TypeReseauAval = rVertical) And (InheritsFrom(TVentil_Collecteur)) and (TVentil_Collecteur(Self).PiquageExpress = C_NON) and (FilsCalcul <> nil) and (parent <> nil) and (parent.parent <> nil) and ((FilsCalcul.Count > 0) and not(Supports(parent.parent.LienCad, IAero_TSouche_SortieL)));
Result := (m_ObjetTypeReseauAval.LienCad.IAero_Vilo_Base_TypeReseauAval = rVertical) and (FilsCalcul <> nil) and (parent <> nil) and (parent.parent <> nil) and ((Fils.Count > 0) {$IfDef VIM_OPTAIR} and not(Supports(parent.parent.LienCad, IAero_TSouche_SortieL)) {$EndIf});
Result := Result And (Etude.Batiment.IsFabricantVIM  and (PiquageExpress = C_NON)) or (not(Etude.Batiment.IsFabricantVIM) and Not(Options.ColonnesPiquagesExpress));
//demande modif Bergeron 17/02/2012
//ConditionValide := ConditionValide and (LongGaine <= 2.7);
//ConditionValide := ConditionValide and (LongGaine <= 2.3) and (Etude.Batiment.Ht_DefautEtages = 2.7);
//ConditionValide := ConditionValide and (round(LongGaine * 100) <= 230) and (round(Etude.Batiment.Ht_DefautEtages * 100) = 270);
//demande modif Bergeron 10/05/2012
//ConditionValide := ConditionValide and (round(LongGaine * 100) = 250) and (round(Etude.Batiment.Ht_DefautEtages * 100) = 270);
//demande modif Bergeron 08/06/2012
{$IfDef VIM_OPTAIR}
Result := Result and (round(LongGaine * 100) = 230) and (round(Etude.Batiment.Ht_DefautEtages * 100) = 270);
{$EndIf}

End;
{ ************************************************************************************************************************************************** }
Function TVentil_Collecteur.ImageAssociee: String;
Begin
  Result := 'COLLECTEUR.jpg';
End;
{ ************************************************************************************************************************************************** }
Function TVentil_Collecteur.CanAddAccidentAmont : Boolean;
begin
  Result := True;
{
    if Etude.Batiment.IsFabricantACT then
      if HasParentColonnePlenum then //and (TVentil_Colonne(GetParentColonnePlenum).TypeColonne <> c_TypeColonneVMC) then
        Result := False;
}
end;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Collecteur.GenerationAccidents;
Var
  m_NoTr     : Integer;
  m_Tr       : TVentil_Troncon;
  m_Accident : TVentil_Accident;
Begin              
  Inherited;

    For m_NoTr := 0 To FilsCalcul.Count - 1 Do
      Begin
      m_Tr := FilsCalcul[m_NoTr];
        While (m_Tr.FilsCalcul.Count > 0) And (m_Tr.InheritsFrom(TVentil_Accident_Dessin)) Do
          m_Tr := m_Tr.FilsCalcul[0];

        if (Etude.Batiment.IsFabricantACT or Etude.Batiment.IsFabricantMVN) and HasParentColonnePlenum then //and (TVentil_Colonne(GetParentColonnePlenum).TypeColonne <> c_TypeColonneVMC) then
          begin
            case TVentil_Colonne(GetParentColonnePlenum).TypeColonne of
              c_TypeColonneVMC                      : AddAccidentColonneVMC(FilsCalcul[m_NoTr]);
              c_TypeColonneShunt                    : AddAccidentColonneShunt(FilsCalcul[m_NoTr]);
              c_TypeColonneConduitIndividuel        : AddAccidentColonneIndividuel(FilsCalcul[m_NoTr]);
              c_TypeColonneAlsace                   : AddAccidentColonneAlsace(FilsCalcul[m_NoTr]);
              c_TypeColonneRamon                    : AddAccidentColonneRamon(FilsCalcul[m_NoTr]);
            end;
          end
        else
          begin
            if m_Tr.CanAddAccidentAmont then
              begin
              m_Accident := TVentil_Accident_BrancheRaccord.Create;
              m_Accident.TronconPorteur := m_Tr;
              m_Tr.Accidents.Add(m_Accident);
              end;
          end;
      End;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Collecteur.AddAccidentColonneVMC(_Porteur: TVentil_Troncon);
Var
  m_Accident : TVentil_Accident;
Begin
  m_Accident := TVentil_Accident_TeSouchePlenumColonneVMC.Create;
  m_Accident.TronconPorteur := _Porteur;
  _Porteur.Accidents.Add(m_Accident);
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Collecteur.AddAccidentColonneShunt(_Porteur: TVentil_Troncon);
Var
  m_Accident : TVentil_Accident;
Begin
  m_Accident := TVentil_Accident_TeSouchePlenumColonneShunt.Create;
  m_Accident.TronconPorteur := _Porteur;
  _Porteur.Accidents.Add(m_Accident);
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Collecteur.AddAccidentColonneIndividuel(_Porteur: TVentil_Troncon);
Var
  m_Accident : TVentil_Accident;
Begin
  m_Accident := TVentil_Accident_TeSouchePlenumColonneIndividuel.Create;
  m_Accident.TronconPorteur := _Porteur;
  _Porteur.Accidents.Add(m_Accident);
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Collecteur.AddAccidentColonneAlsace(_Porteur: TVentil_Troncon);
Var
  m_Accident : TVentil_Accident;
Begin
  m_Accident := TVentil_Accident_TeSouchePlenumColonneAlsace.Create;
  m_Accident.TronconPorteur := _Porteur;
  _Porteur.Accidents.Add(m_Accident);
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Collecteur.AddAccidentColonneRamon(_Porteur: TVentil_Troncon);
Var
  m_Accident : TVentil_Accident;
Begin
  m_Accident := TVentil_Accident_TeSouchePlenumColonneRamon.Create;
  m_Accident.TronconPorteur := _Porteur;
  _Porteur.Accidents.Add(m_Accident);
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Collecteur.Load;
Var
  m_Str : String;
  m_No  : Integer;
Begin
  m_Str := Table_Etude_Collecteur.Donnees.FieldByName('ID_LOGEMENT').AsWideString;
  For m_No := 0 To Etude.Batiment.Logements.Count - 1 Do
    If GUIDToString(Etude.Batiment.Logements[m_No].Id) = m_Str Then Begin
      Logement := Etude.Batiment.Logements[m_No];
    End;

  If Etude.Batiment.IsFabricantVIM then
  PiquageExpress  := Table_Etude_Collecteur.Donnees.FieldByName('PIQUAGE_EXPRESS').AsInteger;
  TypeTrainasse   := Table_Etude_Collecteur.Donnees.FieldByName('TYPETRAINASSE').AsInteger;

  Inherited;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Collecteur.Save;
Begin
  Table_Etude_Collecteur.Donnees.Insert;
  Table_Etude_Collecteur.Donnees.FieldByName('ID_COLLECTEUR').AsWideString := GUIDToString(Id);
  {$IfDef AERAU_CLIMAWIN}
  Table_Etude_Collecteur.Donnees.FieldByName('IDRESEAU').AsWideString := Etude.IdReseau;
  {$EndIf}
  If IsNotNil(Logement) Then Table_Etude_Collecteur.Donnees.FieldByName('ID_LOGEMENT').AsWideString := GUIDToString(Logement.Id)
                        Else Table_Etude_Collecteur.Donnees.FieldByName('ID_LOGEMENT').AsWideString := 'none';

  Table_Etude_Collecteur.Donnees.FieldByName('PIQUAGE_EXPRESS').AsInteger := PiquageExpress;
  Table_Etude_Collecteur.Donnees.FieldByName('TYPETRAINASSE').AsInteger := TypeTrainasse;

  Inherited;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Collecteur.CalculeRecapitulatif(_Iteratif : Boolean);
var
        m_No : integer;
        cptTemperature : TTemperatureCalc;
Begin
  If IsNotNil(Parentcalcul) And (Parentcalcul.FilsCalcul.IndexOf(Self) = 0) Then
  Begin
    PdcReguliereTotaleMini  := PdcReguliereMini;
    PdcReguliereTotaleMaxi  := PdcReguliereMaxi;
    PdcSinguliereTotaleMini := PdcSinguliereMini;
    PdcSinguliereTotaleMaxi := PdcSinguliereMaxi;
    PdcTotaleTotaleMini     := PdcTotaleMini;
    PdcTotaleTotaleMaxi     := PdcTotaleMaxi;
    For m_No := 0 To FilsCalcul.Count - 1 Do
    begin

for cptTemperature := Low(TTemperatureCalc) to High(TTemperatureCalc) do
begin
If Etude.Batiment.IsFabricantVIM then
        Begin

        If DeltaP[cptTemperature].MaxAQmax > FilsCalcul[m_No].DeltaP[cptTemperature].MaxAQmax Then DeltaP[cptTemperature].MaxAQmax := FilsCalcul[m_No].DeltaP[cptTemperature].MaxAQmax;
        If DeltaP[cptTemperature].MaxAQmin > FilsCalcul[m_No].DeltaP[cptTemperature].MaxAQmin Then DeltaP[cptTemperature].MaxAQmin := FilsCalcul[m_No].DeltaP[cptTemperature].MaxAQmin;

        If DeltaP[cptTemperature].MinAQmax < FilsCalcul[m_No].DeltaP[cptTemperature].MinAQmax Then DeltaP[cptTemperature].MinAQmax := FilsCalcul[m_No].DeltaP[cptTemperature].MinAQmax;
        If DeltaP[cptTemperature].MinAQmin < FilsCalcul[m_No].DeltaP[cptTemperature].MinAQmin Then DeltaP[cptTemperature].MinAQmin := FilsCalcul[m_No].DeltaP[cptTemperature].MinAQmin;

        End
Else
        Begin
        If DeltaP[cptTemperature].MaxAQmax < FilsCalcul[m_No].DeltaP[cptTemperature].MaxAQmax Then DeltaP[cptTemperature].MaxAQmax := FilsCalcul[m_No].DeltaP[cptTemperature].MaxAQmax;
        If DeltaP[cptTemperature].MaxAQmin < FilsCalcul[m_No].DeltaP[cptTemperature].MaxAQmin Then DeltaP[cptTemperature].MaxAQmin := FilsCalcul[m_No].DeltaP[cptTemperature].MaxAQmin;

        If DeltaP[cptTemperature].MinAQmax > FilsCalcul[m_No].DeltaP[cptTemperature].MinAQmax Then DeltaP[cptTemperature].MinAQmax := FilsCalcul[m_No].DeltaP[cptTemperature].MinAQmax;
        If DeltaP[cptTemperature].MinAQmin > FilsCalcul[m_No].DeltaP[cptTemperature].MinAQmin Then DeltaP[cptTemperature].MinAQmin := FilsCalcul[m_No].DeltaP[cptTemperature].MinAQmin;

        End;
end;
        FilsCalcul[m_No].CalculeRecapitulatif(_Iteratif);
for cptTemperature := Low(TTemperatureCalc) to High(TTemperatureCalc) do
begin
If Etude.Batiment.IsFabricantVIM then
        Begin
        If PdcVentil[cptTemperature].MinAQmax < FilsCalcul[m_No].PdcVentil[cptTemperature].MinAQmax Then PdcVentil[cptTemperature].MinAQmax := FilsCalcul[m_No].PdcVentil[cptTemperature].MinAQmax;
        If PdcVentil[cptTemperature].MinAQmin < FilsCalcul[m_No].PdcVentil[cptTemperature].MinAQmin Then PdcVentil[cptTemperature].MinAQmin := FilsCalcul[m_No].PdcVentil[cptTemperature].MinAQmin;
        If (PdcVentil[cptTemperature].MaxAQmin >= 30000000000) Or (PdcVentil[cptTemperature].MaxAQmin < FilsCalcul[m_No].PdcVentil[cptTemperature].MaxAQmin) Then PdcVentil[cptTemperature].MaxAQmin := FilsCalcul[m_No].PdcVentil[cptTemperature].MaxAQmin;
        If (PdcVentil[cptTemperature].MaxAQmax >= 30000000000) Or (PdcVentil[cptTemperature].MaxAQmax < FilsCalcul[m_No].PdcVentil[cptTemperature].MaxAQmax) Then PdcVentil[cptTemperature].MaxAQmax := FilsCalcul[m_No].PdcVentil[cptTemperature].MaxAQmax;
        End
else
        Begin
        If PdcVentil[cptTemperature].MinAQmax < FilsCalcul[m_No].PdcVentil[cptTemperature].MinAQmax Then PdcVentil[cptTemperature].MinAQmax := FilsCalcul[m_No].PdcVentil[cptTemperature].MinAQmax;
        If PdcVentil[cptTemperature].MinAQmin < FilsCalcul[m_No].PdcVentil[cptTemperature].MinAQmin Then PdcVentil[cptTemperature].MinAQmin := FilsCalcul[m_No].PdcVentil[cptTemperature].MinAQmin;
        If (PdcVentil[cptTemperature].MaxAQmin < 0.0001) Or (PdcVentil[cptTemperature].MaxAQmin > FilsCalcul[m_No].PdcVentil[cptTemperature].MaxAQmin) Then PdcVentil[cptTemperature].MaxAQmin := FilsCalcul[m_No].PdcVentil[cptTemperature].MaxAQmin;
        If (PdcVentil[cptTemperature].MaxAQmax < 0.0001) Or (PdcVentil[cptTemperature].MaxAQmax > FilsCalcul[m_No].PdcVentil[cptTemperature].MaxAQmax) Then PdcVentil[cptTemperature].MaxAQmax := FilsCalcul[m_No].PdcVentil[cptTemperature].MaxAQmax;
        End;
end;
         inherited;
    end;
  End Else
  inherited;
End;
{ ************************************************************************************************************************************************** }
Function TVentil_Collecteur.TypeCollecteur: Integer;
Begin
  If Supports(LienCad, IAero_CollecteurT1)       Then Result := 1
  Else If Supports(LienCad, IAero_CollecteurT2)  Then Result := 2
  Else If Supports(LienCad, IAero_CollecteurT3)  Then Result := 3
  Else If Supports(LienCad, IAero_CollecteurT3b) Then Result := 30
  Else If Supports(LienCad, IAero_CollecteurT4)  Then Result := 4
  Else If Supports(LienCad, IAero_CollecteurT5)  Then Result := 5
  Else If Supports(LienCad, IAero_CollecteurT6)  Then Result := 6
  Else If Supports(LienCad, IAero_CollecteurT7)  Then Result := 7
  Else If Supports(LienCad, IAero_CollecteurT8)  Then Result := 8
  Else Result := 6;
End;
{ ************************************************************************************************************************************************** }
Function TVentil_Collecteur.FilsIsBrancheLaterale(_Fils: TVentil_Troncon): Boolean;
Var
  m_Sortie : TVentil_Troncon;
Begin
  Result := False;
  m_Sortie := _Fils.Parent;
  While IsNotNil(m_Sortie) And (m_Sortie.Parent <> Self) Do m_Sortie := m_Sortie.Parent;
  If IsNotNil(m_Sortie) And Supports(m_Sortie.LienCad, IAero_Collecteur_SortieL) Then Result := True;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Collecteur.AddQuantitatifReduction;
Var
  m_Classe : TEdibatec_Classe;
  m_Gamme  : TEdibatec_Gamme;
  m_Produit: TEdibatec_Reduction;
  m_NoGam  : Integer;
  m_NoProd : Integer;
  m_Matiere: Integer;
  m_Erreur : Boolean;
  m_Genre : integer;
  m_LibErreur : String;
Begin

        if Etude.Batiment.IsFabricantVIM and (PiquageExpress = c_OUI) then
                Begin
                m_Genre := 1;
                m_LibErreur := 'MM';
                End
        else
                Begin
                m_Genre := 4;
                m_LibErreur := 'FM';
                End;
  m_Erreur := True;
m_Matiere := GetMatiereAccessoire;
  m_Classe := BaseEdibatec.Classe(c_Edibatec_Reductions);
  For m_NoGam := 0 To m_Classe.Gammes.Count - 1 Do Begin
    m_Gamme := m_Classe.Gammes.Gamme(m_NoGam);
    For m_NoProd := 0 To m_Gamme.Produits.Count - 1 Do Begin
      m_Produit := TEdibatec_Reduction(m_Gamme.Produits.Produit(m_NoProd));
      If ( m_Produit.Matiere = m_Matiere) And //  Mati�re OK
         (m_Produit.GrandDiametre = Max(Diametre, DiametreReduit)) And // Diametre Grand OK
         (m_Produit.PetitDiametre = Min(Diametre, DiametreReduit)) And // Diametre petit OK
         (((Etude.Batiment.IsFabricantVIM And (m_Produit.Genre = m_Genre)) or (Not(Etude.Batiment.IsFabricantVIM))) and (m_Produit.TypeReduction = 2))
                 Then Begin
           If SurTerrasse Then AddChiffrage(m_Produit, 1, cst_Horizontal, cst_Chiff_AC)
                          Else AddChiffrage(m_Produit, 1, cst_Vertical, cst_Chiff_AC);
           m_Erreur := False;
           Break;
      End;
    End;
  End;
  If m_Erreur Then AddChiffrageErreur('R�duction', IntToStr(Diametre) + '\' + IntToStr(DiametreReduit) + ' ' + c_Matiere[m_Matiere] + ' ' + m_LibErreur{$IfDeF VIM_OPTAIR}, 'CC - ' +inttostr(1) + ' unit�'{$EndIf});
End;
{ ************************************************************************************************************************************************** }
function TVentil_Collecteur.GetLogementTroncon : TVentil_Objet;
begin
  Result := Logement;
end;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Collecteur.CalculerQuantitatifInternal;
  Function EquivalenceAngle: Integer;
  Begin
    Result := 1;
    Case AnglePiquages Of
      c_Angle90  : Result := 1;
      c_Angle45  : Result := 2;
    End;
  End;

  Function NbSortiesLibelle(_NbSorties : Integer): String;
  Begin
    Result := IntToStr(_NbSorties) + ' sortie';
      if _NbSorties > 1 then
        Result := Result + 's';
  End;

Const
  c_Coll_Angle90  = 1;
  c_Coll_Angle180 = 3;
Var
  m_Perim     : Real;
  m_Gamme     : TEdibatec_Gamme;
  m_Collier   : TEdibatec_ACCRESO;
  m_NoColl    : Integer;
  m_Angle     : Integer;
  m_NbSorties : Integer;
  m_Matiere   : Integer;
  m_NoProd    : Integer;
  m_Produit   : TEdibatec_Confluence;
  m_Erreur    : Boolean;
  m_Confluence : Integer;
  m_Classe     : TEdibatec_Classe;
  m_NoGam      : Integer;
  m_Qte        : Integer;
  m_DiamBrancheLat : Integer;
  m_BrLat      : TVentil_Troncon;
  ConditionValide : Boolean;

m_Bouchon : TEdibatec_ACCRESO;  
Begin
  Inherited;

If Etude.Batiment.IsFabricantVIM then
Begin
  If (FilsCalcul.count > 0) and (FilsIsBrancheLaterale(FilsCalcul[0])) Then m_BrLat := FilsCalcul[0]
                                          Else If FilsCalcul.Count > 1 Then m_BrLat := FilsCalcul[1]
                                                                       Else m_BrLat := nil;

  if m_BrLat = nil then
        exit;
End;

  { Longueur isolant pour traversee de mur }
  m_Perim := Pi * Diametre / 1000;
  TVentil_Caisson(Caisson).LongueurIsolTraver := TVentil_Caisson(Caisson).LongueurIsolTraver + 1.2 * m_Perim;
  { Colliers (isol�s ou non isol�s) }
  {$IfDef VIM_Optair}
  Case TVentil_Caisson(Caisson).TypeColliers Of
    cst_ColliersNonIsol : m_Gamme := BaseEdibatec.Gamme(BaseEdibatec.Assistant.Gamme('c_NoGamme_CollierNonIsoles'));
    cst_ColliersIsol    : m_Gamme := BaseEdibatec.Gamme(BaseEdibatec.Assistant.Gamme('c_NoGamme_CollierIsoles'));
    Else m_Gamme := nil;
  End;
  {$Else}
        if Options.ColliersIso then
                m_Gamme := BaseEdibatec.Gamme(BaseEdibatec.Assistant.Gamme('c_NoGamme_CollierIsoles'))
        else
                m_Gamme := BaseEdibatec.Gamme(BaseEdibatec.Assistant.Gamme('c_NoGamme_CollierNonIsoles'));
  {$EndIf}

    if Etude.Batiment.IsFabricantACT and Not(TVentil_Caisson(Caisson).ACT_CUC) then
      m_Gamme := Nil;
  If IsNotNil(m_Gamme) Then Begin
    m_Erreur := True;
    For m_NoColl := 0 To m_Gamme.Produits.Count - 1 Do Begin
      m_Collier := TEdibatec_ACCRESO(m_Gamme.Produits.Produit(m_NoColl));
      If m_Collier.Diametre = Diametre Then Begin
        AddChiffrage(m_Collier, 1, cst_Vertical, cst_Chiff_AC);
        m_Erreur := False;
        Break;
      End;
    End;
    If m_Erreur Then Begin
      If TVentil_Caisson(Caisson).TypeColliers = cst_ColliersIsol Then AddChiffrageErreur('Collier isol�', IntToStr(Diametre) + ' mm '{$IfDeF VIM_OPTAIR}, inttostr(1) + ' unit�'{$EndIf})
                                                                Else AddChiffrageErreur('Collier non isol�', IntToStr(Diametre) + ' mm '{$IfDeF VIM_OPTAIR}, inttostr(1) + ' unit�'{$EndIf})
    End;
  End;
  { Chiffrage du collecteur }
  Case TypeCollecteur Of
    1: Begin
//        If Etude.Batiment.IsFabricantVIM or Etude.Batiment.IsFabricantF_A then
              m_Angle     := c_Coll_Angle90;
//        else
        If Etude.Batiment.IsFabricantACT then
              m_Angle     := 0;
      m_NbSorties := 1;
    End;
    2: Begin
  //      If Etude.Batiment.IsFabricantVIM or Etude.Batiment.IsFabricantACT or Etude.Batiment.IsFabricantF_A then
              m_Angle     := c_Coll_Angle180;
      m_NbSorties := 2;
    End;
    3: Begin
//        If Etude.Batiment.IsFabricantVIM or Etude.Batiment.IsFabricantACT or Etude.Batiment.IsFabricantF_A then
              m_Angle     := c_Coll_Angle90;
      m_NbSorties := 2;
    End;
    30: Begin
//        If Etude.Batiment.IsFabricantVIM or Etude.Batiment.IsFabricantACT or Etude.Batiment.IsFabricantF_A then
              m_Angle     := c_Coll_Angle90;
      m_NbSorties := 2;
    End;
    4: Begin
//        If Etude.Batiment.IsFabricantVIM or Etude.Batiment.IsFabricantACT or Etude.Batiment.IsFabricantF_A then
              m_Angle     := c_Coll_Angle90;
      m_NbSorties := 3;
    End;
    5: Begin
        If Etude.Batiment.IsFabricantVIM then
              m_Angle     := 0
        else
//        if Etude.Batiment.IsFabricantACT or Etude.Batiment.IsFabricantF_A then
              m_Angle     := c_Coll_Angle90;
      m_NbSorties := 2;
    End;
    6: Begin
//        If Etude.Batiment.IsFabricantVIM or Etude.Batiment.IsFabricantACT or Etude.Batiment.IsFabricantF_A then
              m_Angle     := c_Coll_Angle90;
      m_NbSorties := 3;
    End;
    7: Begin
//        If Etude.Batiment.IsFabricantVIM or Etude.Batiment.IsFabricantACT or Etude.Batiment.IsFabricantF_A then
              m_Angle     := c_Coll_Angle90;
      m_NbSorties := 3;
    End;
    8: Begin
//        If Etude.Batiment.IsFabricantVIM or Etude.Batiment.IsFabricantACT or Etude.Batiment.IsFabricantF_A then
              m_Angle     := c_Coll_Angle90;
      m_NbSorties := 4;
    End;
    Else Begin
//        If Etude.Batiment.IsFabricantVIM or Etude.Batiment.IsFabricantACT or Etude.Batiment.IsFabricantF_A then
              m_Angle     := c_Coll_Angle180;
      m_NbSorties := 2;
    End;
  End;
m_Matiere := GetMatiereAccessoire;

//  if Etude.Batiment.IsFabricantVIM and (PiquageExpress = C_OUI) then
if (Etude.Batiment.IsFabricantVIM  and (PiquageExpress = C_OUI)) or (not(Etude.Batiment.IsFabricantVIM) and Options.ColonnesPiquagesExpress) then
        Begin
        m_Confluence := c_Confluence_Piquage;
        m_Qte := m_NbSorties;
        m_Angle := c_Coll_Angle90;

                If Etude.Batiment.IsFabricantVIM then
                        Begin
                        m_DiamBrancheLat := m_BrLat.Diametre;
                        End
                else
                        Begin
                        m_DiamBrancheLat := 125;
                        End;
        End
  else
        Begin
        m_Confluence := c_Confluence_Collecteur;
                If Etude.Batiment.IsFabricantVIM then
                        m_DiamBrancheLat := m_BrLat.Diametre
                else
                        begin
                        m_DiamBrancheLat := 125;
//                          if Not(Etude.Batiment.IsFabricantACT) and Not(Etude.Batiment.IsFabricantF_A) then
//                            m_Angle := 0;
                        end;
        m_Qte := 1;
        End;


  m_Classe := BaseEdibatec.Classe(c_Edibatec_Confluences);
  m_Erreur := True;
  For m_NoGam := 0 To m_Classe.Gammes.Count - 1 Do Begin
    m_Gamme := m_Classe.Gammes.Gamme(m_NoGam);
    For m_NoProd := 0 To m_Gamme.Produits.Count - 1 Do Begin
      m_Produit := TEdibatec_Confluence(m_Gamme.Produits.Produit(m_NoProd));

        If Etude.Batiment.IsFabricantVIM then
                Begin
                ConditionValide := (m_Produit.TypConfluence = m_Confluence) And
                (m_Produit.Matiere = m_Matiere) And (m_Produit.DiametreRect = Diametre) And
                ( m_Produit.DiametreLat = m_DiamBrancheLat) And
                ((m_Produit.NbSorties = m_NbSorties) or (m_Confluence = c_Confluence_Piquage)) And (m_Produit.Angle = m_Angle);
                End
        Else
                Begin
                ConditionValide := (m_Produit.TypConfluence = m_Confluence) And
                (m_Produit.Matiere = m_Matiere) And (m_Produit.DiametreRect = Diametre) And
                ((m_Produit.NbSorties = m_NbSorties) or (m_Confluence = c_Confluence_Piquage)) And (m_Produit.Angle = m_Angle);
                End;

      If ConditionValide Then Begin
                If SurTerrasse Then
                        AddChiffrage(m_Produit, m_Qte, cst_Horizontal, cst_Chiff_AC)
                Else
                        AddChiffrage(m_Produit, m_Qte, cst_Vertical, cst_Chiff_AC);
         m_Erreur := False;
         break;
        End;
    End;
  End;
  If m_Erreur Then
        Begin
        If Etude.Batiment.IsFabricantVIM then
        Begin
        m_Erreur := True;
        m_Classe := BaseEdibatec.Classe(c_Edibatec_Confluences);
//        m_Erreur := True;
                For m_NoGam := 0 To m_Classe.Gammes.Count - 1 Do
                        Begin
                        m_Gamme := m_Classe.Gammes.Gamme(m_NoGam);
                                For m_NoProd := 0 To m_Gamme.Produits.Count - 1 Do
                                        Begin
                                        m_Produit := TEdibatec_Confluence(m_Gamme.Produits.Produit(m_NoProd));
                                                If (m_Produit.TypConfluence = c_Confluence_Te) And          // Type confluence OK
                                                   (m_Produit.Matiere = m_Matiere) And //  Mati�re OK
                                                   (m_Produit.DiametreRect = Diametre) And // Diametre rectiligne OK
                                                   ( m_Produit.DiametreLat = m_BrLat.Diametre) And // Diametre lateral OK
                                                   (m_Produit.Angle = EquivalenceAngle) Then
                                                        Begin // Angle bifurcation OK
                                                                If SurTerrasse Then
                                                                        AddChiffrage(m_Produit, 1, cst_Horizontal, cst_Chiff_AC)
                                                                Else
                                                                        AddChiffrage(m_Produit, 1, cst_Vertical, cst_Chiff_AC);

                                                        m_Erreur := False;
//                                                        exit;
                                                        End;
                                        End;
                        End;
        End;


                if m_Erreur then
                begin
                        if m_Confluence = c_Confluence_Piquage then
                                AddChiffrageErreur('Piquage', IntToStr(Diametre) + ' mm ' + NbSortiesLibelle(m_NbSorties) + ' ' + c_Matiere[m_Matiere]{$IfDeF VIM_OPTAIR}, inttostr(m_Qte) + ' unit�(s)'{$EndIf})
                        Else
                                AddChiffrageErreur('Collecteur', IntToStr(Diametre) + ' mm ' + NbSortiesLibelle(m_NbSorties) + ' ' + c_Matiere[m_Matiere]{$IfDeF VIM_OPTAIR}, inttostr(m_Qte) + ' unit�(s)'{$EndIf});
                end;
        End;

If Etude.Batiment.IsFabricantVIM then
         if HasReduction then
                AddQuantitatifReduction;


  { 1 bouchon en bas de la colonne }

//if Etude.Batiment.IsFabricantVIM and (SortieRectiligne <> nil) and (SortieRectiligne.Fils.Count = 0) then
if Etude.Batiment.IsFabricantVIM and (SortieRectiligne <> nil) and ((SortieRectiligne.Fils.Count = 0) or (SortieRectiligne.Fils[0].Fils.Count = 0))then
begin
{
  If TronconGaz Then m_Matiere := c_Alu
                Else m_Matiere := c_Galva;

        if not(Etude.Batiment.IsFabricantVIM) then
                begin
                if Options.GaineVeloduct then
                        m_Matiere := c_Veloduct;
                end;
}
m_Matiere := GetMatiereAccessoire;
  if SortieRectiligne.Fils.Count > 0 then
    m_Gamme := BaseEdibatec.Gamme(BaseEdibatec.Assistant.c_NoGamme_BouchonsMaleMatiere[m_Matiere])
  else
    m_Gamme := BaseEdibatec.Gamme(BaseEdibatec.Assistant.c_NoGamme_BouchonsFemelleMatiere[m_Matiere]);
  m_Erreur := True;
  If m_Gamme <> nil Then For m_NoProd := 0 To m_Gamme.Produits.Count - 1 Do Begin
    m_Bouchon := TEdibatec_ACCRESO(m_Gamme.Produits.Produit(m_NoProd));
    If (m_Bouchon.Diametre = Diametre) And (m_Bouchon.Matiere = m_Matiere) Then Begin
      AddChiffrage(m_Bouchon, 1, cst_Vertical, cst_Chiff_AC);
      m_Erreur := False;
      Break;
    End;
  End;
  If m_Erreur Then
        AddChiffrageErreur('Bouchon', IntToStr(Diametre) + ' mm ' + c_Matiere[m_Matiere]{$IfDeF VIM_OPTAIR}, inttostr(1) + ' unit�'{$EndIf});
end;

End;
{ ************************************************************************************************************************************************** }
Function TVentil_Collecteur.IsEtiquetteVisible : boolean;
Begin
Result := Options.AffEtiq_Collect;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_Collecteur.InitEtiquette;
Var
        ListBouches : TList;
        i : integer;
Begin
  FEtiquette := Reference + ' ' + EtiquetteMatiere;
  If not(Etude.Batiment.IsFabricantVIM) then
  begin

        if Logement <> nil then
                Begin
                FEtiquette := FEtiquette + #13;
                        if Logement.TypeLogement <> c_TypeAutre then
                                FEtiquette := FEtiquette + Logement.LibTypeLogement
                        Else
                                FEtiquette := FEtiquette + Logement.Reference;
                End;
  ListBouches := GetListBouchesFilles;
        for i := 0 to ListBouches.Count - 1 do
                Begin
                FEtiquette := FEtiquette + #13 + TVentil_Bouche(ListBouches[i]).DeterminerTexteEtiquette(True);
                End;
  ListBouches.Destroy;
  end;


        if LienCad <> nil then
                LienCad.IAero_Vilo_Base_SetTextEtiquette(FEtiquette, Options.TypeEtiquette);

  ManageEtiquetteVisible;
End;
{ ************************************************************************************************************************************************** }
Function TVentil_Collecteur.GetListBouchesFilles : TList;

var
        i, j : Integer;
        TempList : TList;
Begin
Result := TList.Create;
        For i := 0 to FilsCalcul.count - 1 Do
                If FilsIsBrancheLaterale(FilsCalcul[I]) Then
                        Begin
                        TempList := TList.Create;
                        FilsCalcul[I].ListeBouchesAval(TempList);
                                for j := 0 to TempList.Count - 1 do
                                        result.Add(TempList[j]);
                        TempList.Destroy;
                        End;

End;
{ ******************************************************************* \ TVentil_Collecteur ************************************************************** }







End.
