Unit Ventil_EA;
 
{$Include 'Ventil_Directives.pas'}

Interface
Uses
  Classes, SysUtils, advGrid, BaseGrid, AdvObj, Math, Graphics,
  BBScad_Interface_Aeraulique,
  Ventil_Const,
  Ventil_Types, Ventil_Troncon, Ventil_Logement, Ventil_SystemeVMC, Ventil_EdibatecProduits, Ventil_Collecteur, Ventil_Utils, 
  Ventil_RegulateurDebit
  ;

Type
{ ************************************************************************************************************************************************** }
  TVentil_EAir = Class(TVentil_Troncon, IAero_Fredo_BoucheBase)
    Constructor Create; Override;
  Private
    FQteAssociations : TStringList;
    FCodesAssociations : TStringList;
    FQMaxSaisi   : Integer;
    FTypePiece   : Integer;
    FQMinSaisi   : Integer;
    FLogCollect  : Integer;
    FQColle      : Integer;
    FLogement    : TVentil_Logement;
    FProduit     : TEdibatec_EA;
    FPressionMini_Iteratif : Array[TTemperatureCalc] of Real;
    FPressionMaxi_Iteratif : Array[TTemperatureCalc] of Real;
    FDebitMini_Iteratif : Real;
    FDebitMaxi_Iteratif : Real;
    Procedure SetTronconGaz_Hygro;
    Procedure CalcDebit(_Iteratif : Boolean);
    Procedure CalcDebitLocalAutre;
    Procedure CalcDebitChoisi;
    Procedure CalcDebitEAChoisie;
    Function  GetSysteme : TVentil_Systeme;
    Function  GetLogement : TVentil_Logement;
    procedure SetQMaxSaisi(const Value: Integer);
    procedure SetQMinSaisi(const Value: Integer);
    procedure SetTypePiece(const Value: Integer);
    Function  SelectionEA: String;
    Function  SelectionGammeClapetCF: String;
    Procedure CalculerQuantitatif_CoupeFeu;
    Function  GetEAAChiffrer: TEdibatec_EA;
    Function  EASysteme: TVentil_EA;
    Function  IsThermoModulante : Boolean;
    Function  IsEASelectionDebitNonFixe : boolean;
    Procedure AffecterTexteEtiquette;
    Function  IsFromGamme(_2Gamme : String) : boolean;
    Function  GetRegulateurParent : TVentil_RegulateurDebit;
    function  isEACoupeFeu : boolean;
    Procedure AfficheAcoustOptima;
    function isAutoReglable : boolean;
    Procedure AfficheAcoustOptair;
  Protected
    class Function NomParDefaut : String; Override;
    Procedure Save; Override;
    Procedure CalculerQuantitatifInternal; Override;
    Procedure IAero_Fredo_Base_RemplacerPar(Objet : IAero_Fredo_Base); Override;
    Function  ImageAssociee: String; Override;
    Function IsEtiquetteVisible : boolean; Override;
    Procedure SelectionneDiametre_SelonMethode; Override;
    function GetLogementTroncon : TVentil_Objet; Override;
  Public
    EASelection: TEdibatec_EA;
    Has_PareFlamme : Integer;
    GammePareFlamme: TEdibatec_Gamme;
    ESA          : Integer;
    DUGD         : Real;
    AssociationsNonTrouvees : TStringList;
    Procedure CalculESA;

    class Function FamilleObjet : String; Override;

    Property Logement    : TVentil_Logement read GetLogement;
    Property Systeme     : TVentil_Systeme  read GetSysteme;
    Property TypePiece   : Integer read FTypePiece write SetTypePiece;
    Property QMinSaisi   : Integer read FQMinSaisi write SetQMinSaisi;
    Property QMaxSaisi   : Integer read FQMaxSaisi write SetQMaxSaisi;
    Property EAAChiffrer : TEdibatec_EA read GetEAAChiffrer;

    Procedure InfosFromCad; Override;
    Procedure ManageEtiquetteVisible; Override;
    Function DeterminerTexteEtiquette : String;

    { Calculs }
    Procedure StoreValues_Iteratif; Override;
    Procedure CalculeFoisonnement(Var _NbCuisHygro: Integer); Override;
    Procedure CalculeNouveauFoisonnement(Var _NbEANonThermoModulante: Integer); Override;
    Procedure CalculeFoisonnement2014(Var _NbDispositifs: Integer); Override;
    Procedure VerifPresenceAlizeIII; Override;
    Procedure VerifPresenceAutoreglable; Override;
    Procedure CalculeDiametreDebitCollectif(_Iteratif : Boolean); Override;
    Procedure CalculeDiametreDebitTertiaire(_Iteratif : Boolean); Override;
    Procedure CalculeRecapColonne(Var _TeSouche: TVentil_Troncon; _Iteratif : Boolean); Override;
    Procedure CalculeDebitsParLogementOld;
    Procedure CalculeDebitsParLogement; Override;
    Procedure CalculeDebitsMax; Override;
    Function GetQmaxPrisEnCompte : integer;

    //Nouveaux avis technique
    Procedure CalculeSommeDebitTempo(var QHRHygro : Real; var QTempHygro : Real; var QMiniWC : Real; var QTempWC : Real); Override;

    function GetReglage(_ValDP : Real) : Real;
    function isDebitReglable : boolean;
    Function GetLPGlobal : Real;

    Procedure Paste(_Source: TVentil_Objet); Override;
    Procedure Load; Override;

    Procedure Affiche (_Grid: TAdvStringGrid); Override;
    Procedure AfficheResultats(_Grid: TAdvStringGrid); Override;
    Procedure Recupere(_Grid: TAdvStringGrid; _Ligne: Integer); Override;
    Function  TypeEdition(_Grid: TAdvStringGrid; _Ligne: Integer): TEditorType; Override;
    Procedure PropEdition(_Grid: TAdvStringGrid; _Ligne: Integer); Override;
    Function  HasComboBox(_Ligne: Integer): Boolean; Override;
    Function  AfficheQuestion(_NoQuestion: Integer): Boolean; Override;
    Function  EllipsClick(_Grid: TAdvStringGrid; _Ligne: Integer): String; Override;

    Function  Collecteur  : TVentil_Collecteur;

    { Interface CAD }
    procedure IAero_Fredo_BoucheBase_GetDimensionsLocal(var Longueur, Largeur, Hauteur : single; var Ouvert : boolean);
    Function IAero_Fredo_BoucheBase_GetTrLocal : single;
    Function IAero_Fredo_BoucheBase_GetPression : single;
    Function IAero_Fredo_BoucheBase_GetDne : Widestring;
    Function IAero_Fredo_BoucheBase_GetLw : Widestring;
    Procedure MajAssociation;
    Procedure SetAssociationDefaut(_Association: TEdibatec_Association; _QteDefaut: Real);
  End;
{ ************************************************************************************************************************************************** }

Implementation
Uses
  Grids,
  Ventil_Firebird, Ventil_Caisson, Ventil_TeSouche, Ventil_Edibatec, Ventil_EdibatecChoixProduit, Ventil_EdibatecChoixGamme, Ventil_Declarations,
  Ventil_SelectionAccessoires, Ventil_Batiment,
  Ventil_SortieToiture,
  BbsgFonc,
  BBS_Message, dialogs;

{ ******************************************************************* TVentil_EAir ****************************************************************** }
Constructor TVentil_EAir.Create;
Begin
  inherited;
  {$IfNDef AERAU_CLIMAWIN}
  if Etude.Batiment.IsFabricantVIM then
          TypePiece      := c_ChoixEA
  else
  {$EndIf}
        begin
        if Etude.Batiment.TypeBat = c_BatimentTertiaire then
                TypePiece      := c_SaisieDebitsEA
        else
                TypePiece      := c_SaisieDebitsEA;
        end;
  QMaxSaisi      := 0;
  QMinSaisi      := 0;
  EASelection:= nil;
  GammePareFlamme:= nil;
  Has_PareFlamme := Etude.Batiment.ClapetsSurBouches;
  NbQuestions    := NbQuestions + c_NbQ_EA;
        if (Etude.Batiment.TypeBat = c_BatimentTertiaire) then
                FLogCollect    := c_Non
        else
                FLogCollect    := c_Oui;

  Flogement      := nil;
  FQColle        := 50;

End;
{ ************************************************************************************************************************************************** }
class Function TVentil_EAir.NomParDefaut : String;
Begin
Result := 'EA';
End;
{ ************************************************************************************************************************************************** }
class Function TVentil_EAir.FamilleObjet : String;
Begin
Result := 'EA';
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_EAir.Paste(_Source: TVentil_Objet);
Begin
  Inherited;
  TypePiece       := TVentil_EAir(_Source).TypePiece;
  NatureGaine     := TVentil_EAir(_Source).NatureGaine;
  Has_PareFlamme  := TVentil_EAir(_Source).Has_PareFlamme;
  HasRegulateur   := TVentil_EAir(_Source).HasRegulateur;
  EASelection := TVentil_EAir(_Source).EASelection;
  FLogCollect     := TVentil_EAir(_Source).FLogCollect;
  FLogement       := TVentil_EAir(_Source).FLogement;
  FQMinSaisi      := TVentil_EAir(_Source).QMinSaisi;
  FQMaxSaisi      := TVentil_EAir(_Source).QMaxSaisi;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_EAir.IAero_Fredo_Base_RemplacerPar(Objet : IAero_Fredo_Base);
Begin
inherited;
TVentil_EAir(Objet.IAero_Fredo_Base_Objet).TypePiece       := TypePiece;
TVentil_EAir(Objet.IAero_Fredo_Base_Objet).NatureGaine     := NatureGaine;
TVentil_EAir(Objet.IAero_Fredo_Base_Objet).Has_PareFlamme  := Has_PareFlamme;
TVentil_EAir(Objet.IAero_Fredo_Base_Objet).HasRegulateur   := HasRegulateur;
TVentil_EAir(Objet.IAero_Fredo_Base_Objet).EASelection := EASelection;
TVentil_EAir(Objet.IAero_Fredo_Base_Objet).FLogCollect     := FLogCollect;
TVentil_EAir(Objet.IAero_Fredo_Base_Objet).FLogement       := FLogement;
TVentil_EAir(Objet.IAero_Fredo_Base_Objet).FQMinSaisi       := QMinSaisi;
TVentil_EAir(Objet.IAero_Fredo_Base_Objet).FQMaxSaisi       := QMaxSaisi;


End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_EAir.Affiche(_Grid: TAdvStringGrid);
Var
  m_Row      : Integer;
  m_Question : Integer;
  m_Str      : String;
Begin
  GetEAAChiffrer;
  Inherited;
  m_Row := GetNbQuestionAfficheTroncon + 1;
  For m_Question := Low(c_LibellesQ_EA) To High(c_LibellesQ_EA) Do If AfficheQuestion(m_Question) Then Begin
    _Grid.Cells[0, m_Row] := c_LibellesQ_EA[m_Question];
    Case m_Question Of
      c_QEA_LgtCollect : m_Str := C_OUINON[FLogCollect];
      c_QEA_Logement   : If IsNotNil(Logement) Then m_Str := Logement.Reference
                                                   Else m_Str := ' *** ';
      c_QEA_Piece      : m_Str := c_NomsPiecesEA[TypePiece];
      c_QEA_AvertissementPiece   : Begin
                                       m_Str := 'SDB-WC est impossible avec ce syst�me';
                                       _Grid.CellProperties[0, m_Row].FontColor := clRed;
                                       _Grid.CellProperties[1, m_Row].FontColor := clRed;
                                       End;
      c_QEA_QMax       : m_Str := IntToStr(QMaxSaisi);
      c_QEA_QMin       : m_Str := IntToStr(QMinSaisi);
      c_QEA_PareFlamme : m_Str := C_OUINON[Has_PareFlamme];
      c_QEA_GammePareFlamme : If GammePareFlamme <> nil Then m_Str := GammePareFlamme.Reference
                                                            Else m_Str := ' *** ';
      c_QEA_Selection  : If EASelection <> nil Then m_Str := EASelection.Reference
                                                       Else m_Str := ' *** ';
      c_QEA_Regulateur  : Begin
                                if HasRegulateur = -1 then
                                        HasRegulateur := 1;
                              m_Str := C_OUINON[HasRegulateur];
                              End;
      c_QEA_RegulateurSel       : If RegulateurDebitChiff <> nil Then m_Str := RegulateurDebitChiff.Reference
                                                       Else m_Str := ' *** ';
      c_QEA_Accessoires : m_Str := 'Clic pour choisir';

      Else m_Str := '';
    End;
    _Grid.Cells[1, m_Row] := m_Str;
    Inc(m_Row);
  End;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_EAir.AfficheAcoustOptima;
Var
  m_NoDb  : Integer;
  m_Col   : Integer;
  m_Row   : Integer;
  m_Tab   : TBBScad_TabAcoustique;
  m_ValSup: Integer;
  m_LpG   : Real;
Begin
  CalculESA;
  {$IfDef ACTHYS_DIMVMBP}
  Etude.TabAcoust.TabVisible := False;
  {$Else}
  {$IfDef MVN_MVNAIR}
  Etude.TabAcoust.TabVisible := False;
  {$Else}
  Etude.TabAcoust.TabVisible := True;
  {$EndIf}
  {$EndIf}
  if LienCad = nil then exit;
  m_Tab := (LienCad As IAero_EntreeAir).IAero_BoucheBase_GetLw;
  m_LpG := (LienCad As IAero_EntreeAir).IAero_BoucheBase_GetLpG;
  For m_Col := 0 To Etude.GridAcoust.ColCount - 1 Do For m_Row := 0 To Etude.GridAcoust.RowCount - 1 Do
    Etude.GridAcoust.CellProperties[m_Col, m_Row].Alignment := taCenter;
  If TronconGaz Then Etude.GridAcoust.Cells[2, 3] := 'Lp maximum, pour une d�pression de 140 Pa'
                Else Etude.GridAcoust.Cells[2, 3] := 'Lp maximum, pour une d�pression de 160 Pa';
  Etude.GridAcoust.Cells[0, 4] := 'Exigence sur le Dn';
  Etude.GridAcoust.MergeCells(0, 0, 2, 1);
  Etude.GridAcoust.MergeCells(0, 1, 2, 1);

  Etude.GridAcoust.MergeCells(0, 2, 2, 1);
  Etude.GridAcoust.MergeCells(2, 2, 6, 1);
  Etude.GridAcoust.MergeCells(0, 2, 2, 2);
  Etude.GridAcoust.MergeCells(2, 3, 6, 1);
  Etude.GridAcoust.MergeCells(0, 4, 3, 1);
  Etude.GridAcoust.MergeCells(3, 4, 6, 1);

  Etude.GridAcoust.Cells[0, 2] := #13 + ' Lp - dB(A)';
  Etude.GridAcoust.CellProperties[0, 2].VAlignment :=  vtaCenter;

  For m_NoDb := Low(m_Tab) To High(m_Tab) Do Begin
    Etude.GridAcoust.CellProperties[1, m_NoDb].Alignment := taCenter;
    Etude.GridAcoust.Cells[0, 1]  := 'Lw (dB)';
    If m_Tab[m_NoDb] > 20 Then Etude.GridAcoust.Cells[m_NoDb, 1] := Float2Str(m_Tab[m_NoDb], 0)
                          Else Etude.GridAcoust.Cells[m_NoDb, 1] := '< 20';
  End;
  { Pour le lp }
  If Logement <> nil Then Begin
    If (TypePiece = c_Cuisine) And (Logement.CuisineFermee = c_NON) Then m_ValSup := 30
                                                                    Else m_ValSup := 35;

    If (m_LpG < m_ValSup) Then Begin
      If m_LpG < 25 Then Etude.GridAcoust.Cells[2, 2] := ' < 25'
                    Else Etude.GridAcoust.Cells[2, 2] := Float2Str(m_LpG, 0);
      Etude.GridAcoust.CellProperties[2, 4].FontColor := ClBlue;
    End Else Begin
      Etude.GridAcoust.Cells[2, 2] := Float2Str(m_LpG, 0) + ' > ' + IntToStr(m_ValSup)  + ' -> besoin d''un silencieux';
      Etude.GridAcoust.CellProperties[2, 4].FontColor := ClRed;
    End;
  End;
  { Pour l'ESA }
  If (TypePiece = c_WC) Or (ESA = -1) Then Begin
    //Etude.GridAcoust.Cells[0, 4] := '';
    Etude.GridAcoust.Cells[3, 4] := 'non calcul�e';
    Etude.GridAcoust.CellProperties[3, 4].FontColor := Etude.GridAcoust.FixedFont.Color;
  End Else Begin
    Etude.GridAcoust.Cells[3, 4] := 'CLASSE ' + c_TABESA[ESA];
    Etude.GridAcoust.CellProperties[2, 4].FontColor := ClBlue;
  End;

End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_EAir.AfficheAcoustOptair;
Const
  cst_bdfreq : Array[1..7] Of string = ('63', '125', '250', '500', '1000', '2000', '4000') ;
Var
  m_NoDb : Integer;
  m_Col  : Integer;
  m_Row  : Integer;
  m_Tab   : TBBScad_TabAcoustique;
Begin
  {$IfDef ACTHYS_DIMVMBP}
  Etude.TabAcoust.TabVisible := False;
  {$Else}
  {$IfDef MVN_MVNAIR}
  Etude.TabAcoust.TabVisible := False;
  {$Else}
  Etude.TabAcoust.TabVisible := True;
  {$EndIf}
  {$EndIf}
  m_Tab := (LienCad As IAero_EntreeAir).IAero_BoucheBase_GetLp;
  For m_Col := 0 To Etude.GridAcoust.ColCount - 1 Do For m_Row := 0 To Etude.GridAcoust.RowCount - 1 Do
    Etude.GridAcoust.CellProperties[m_Col, m_Row].Alignment := taCenter;
    Etude.GridAcoust.Cells[1, 0]  := 'Lp dB(A)';
  For m_NoDb := Low(m_Tab) To High(m_Tab) Do Begin
    Etude.GridAcoust.CellProperties[1, m_NoDb].Alignment := taCenter;
    Etude.GridAcoust.Cells[0, m_NoDb]  := cst_bdfreq[m_NoDb] + ' Hz';
    If m_Tab[m_NoDb] > 20 Then Etude.GridAcoust.Cells[1, m_NoDb] := Float2Str(m_Tab[m_NoDb], 0)
                          Else Etude.GridAcoust.Cells[1, m_NoDb] := '< 20';
  End;
End;
{ ************************************************************************************************************************************************* }
Function TVentil_EAir.GetLPGlobal : Real;
Var
        m_Frequence  : Integer;
        VolumePiece  : Single;
        ArrayL1      : Array[1..7] of Single;
        ArrayL2      : Array[1..7] of Single;
        ArrayL3      : Array[1..7] of Single;
        ArrayL4      : Array[1..7] of Single;
        ArrayL5      : Array[1..7] of Single;
        ArrayL6      : Array[1..7] of Single;
        ArrayL7      : Array[1..7] of Single;
        ArrayL8      : Array[1..7] of Single;
        ArrayL9      : Array[1..7] of Single;
        ArrayL10     : Array[1..7] of Single;
Begin
if LienCad = nil then exit;
//Result := 0;
VolumePiece := 0;

        If IsNotNil(Logement) then
                VolumePiece := Logement.InfosPieces[TypePiece, 1] * Logement.InfosPieces[TypePiece, 2];

                For m_Frequence := Low((LienCad As IAero_EntreeAir).IAero_BoucheBase_GetLp) + 1 to High((LienCad As IAero_EntreeAir).IAero_BoucheBase_GetLp) - 1 do
                                Begin
                                ArrayL1[m_Frequence]  := TVentil_Caisson(Caisson).IAero_Fredo_Caisson_GetSpectreAcoustique[m_Frequence];
                                ArrayL2[m_Frequence]  := TVentil_Caisson(Caisson).IAero_Fredo_Caisson_GetSpectreAcoustique[m_Frequence] - (LienCad As IAero_EntreeAir).IAero_BoucheBase_GetLwAmont[m_Frequence];
                                ArrayL3[m_Frequence]  := (LienCad As IAero_EntreeAir).IAero_BoucheBase_GetDn[m_Frequence];
                                ArrayL4[m_Frequence]  := 10 * Log10(TVentil_Caisson(Caisson).ProduitAssocie.DiametreAspiration / Diametre);
                                ArrayL5[m_Frequence]  := ArrayL1[m_Frequence] - (ArrayL2[m_Frequence] + ArrayL3[m_Frequence] + ArrayL4[m_Frequence]);
                                ArrayL6[m_Frequence]  := (LienCad As IAero_EntreeAir).IAero_BoucheBase_GetReg[m_Frequence];
                                ArrayL7[m_Frequence]  := InterfaceCAD.Acoustique_SommeLog10([ArrayL5[m_Frequence], ArrayL6[m_Frequence]], 0, 1);
                                ArrayL8[m_Frequence]  := -10 * Log10(VolumePiece / 12.5);
                                ArrayL9[m_Frequence]  := InterfaceCAD.Acoustique_PonderationA(m_Frequence - 1);
                                ArrayL10[m_Frequence] := ArrayL7[m_Frequence] + ArrayL8[m_Frequence] + ArrayL9[m_Frequence];
                                End;

                                Result := InterfaceCAD.Acoustique_SommeLog10(ArrayL10, Low(ArrayL10) + 1, High(ArrayL10) - 1);
End;
{ ************************************************************************************************************************************************** }
{$IfDef VIM_OPTAIR}
Procedure TVentil_EAir.AfficheResultats(_Grid: TAdvStringGrid);
Var
  m_Col : Integer;
  m_Row : Integer;
  Add50  : Integer;
  Add70  : Integer;
  Add80  : Integer;
  Add100 : Integer;
Begin
  Inherited;
          If IsNotNil(Logement) Then Begin
    _Grid.Cells[1, 0] := Logement.Reference ;
    _Grid.Cells[2, 0] := Logement.LibTypeLogement;
  End;
  If (TypePiece <= c_NbPiecesSeches) And IsNotNil(Logement) And IsNotNil(GetSysteme) Then Begin
{$IfDef OLD_ATVENTIL}
      if (GetSysteme.EntreeAir[Logement.TypeLogement, TypePiece, Logement.T3Optimise = c_Oui] <> Nil) and (GetSysteme.EntreeAir[Logement.TypeLogement, TypePiece, Logement.T3Optimise = c_Oui].Count > 0) then
    _Grid.Cells[1, 1] := GetSysteme.EntreeAir[Logement.TypeLogement, TypePiece, Logement.T3Optimise = c_Oui][0].GetFirstLibelle;
{$Else}
      if (GetSysteme.ConfigsLogements.GetConfigLogement(Logement.TypeLogement, Logement.IdxConfig).SolutionsEAs[TypePiece][Logement.IdxSolutionEA[TypePiece]] <> Nil) and (GetSysteme.ConfigsLogements.GetConfigLogement(Logement.TypeLogement, Logement.IdxConfig).SolutionsEAs[TypePiece][Logement.IdxSolutionEA[TypePiece]].Count > 0) then
    _Grid.Cells[1, 1] := GetSysteme.ConfigsLogements.GetConfigLogement(Logement.TypeLogement, Logement.IdxConfig).SolutionsEAs[TypePiece][Logement.IdxSolutionEA[TypePiece]][0].GetFirstLibelle;
{$EndIf}
    _Grid.Cells[2, 1] := c_NomsPiecesHumides[TypePiece];
  End Else If TypePiece = c_ChoixEA Then _Grid.Cells[1, 1] := 'Nom EA choisie'
                                        Else _Grid.Cells[1, 1] := 'Saisie des d�bits';
  For m_Col := 0 To Etude.GridAcoust.ColCount - 1 Do For m_Row := 1 To Etude.GridAcoust.RowCount - 1 Do Etude.GridAcoust.Cells[m_Col, m_Row] := '';

  if Etude.Batiment.IsFabricantVIM then
        Begin
        AfficheAcoustOptair;
        Add50  := 50;
        Add70  := 70;
        Add80  := 80;
        Add100 := 100;
        End
  else
        Begin
        AfficheAcoustOptima;
        Add50  := 0;
        Add70  := 0;
        Add80  := 0;
        Add100 := 0;
        End;
{
  If TronconGaz Then Begin
    _Grid.Cells[1, 8] := Float2Str(PdcTotaleMini + Add50, 2) + ' � ' + Float2Str(PdcTotaleMaxi + Add70, 2) + ' Pa';
    _Grid.Cells[2, 8] := Float2Str(PdcTotaleTotaleMini + Add50, 2) + ' � ' + Float2Str(PdcTotaleTotaleMaxi + Add70, 2) + ' Pa';
  End Else If TronconHygro And Not TronconGaz Then Begin
    _Grid.Cells[1, 8] := Float2Str(PdcTotaleMini + Add80, 2) + ' � ' + Float2Str(PdcTotaleMaxi + Add100, 2) + ' Pa';
    _Grid.Cells[2, 8] := Float2Str(PdcTotaleTotaleMini + Add80, 2) + ' � ' + Float2Str(PdcTotaleTotaleMaxi + Add100, 2) + ' Pa';
  End Else Begin
    _Grid.Cells[1, 8] := Float2Str(PdcTotaleMini + Add50, 2) + ' � ' + Float2Str(PdcTotaleMaxi + Add70, 2) + ' Pa';
    _Grid.Cells[2, 8] := Float2Str(PdcTotaleTotaleMini + Add50, 2) + ' � ' + Float2Str(PdcTotaleTotaleMaxi + Add70, 2) + ' Pa';
  End;
  if Etude.Batiment.IsFabricantVIM then
  if (EAAChiffrer <> nil) then
        Begin
        _Grid.Cells[1, 8] := Float2Str(PdcTotaleMini + EAAChiffrer.DP_Mini , 2) + ' � ' + Float2Str(PdcTotaleMaxi + EAAChiffrer.DP_Mini + 20, 2) + ' Pa';
        _Grid.Cells[2, 8] := Float2Str(PdcTotaleTotaleMini + EAAChiffrer.DP_Mini, 2) + ' � ' + Float2Str(PdcTotaleTotaleMaxi + EAAChiffrer.DP_Mini + 20, 2) + ' Pa';
        End;
}
End;
{$Else}
{$IFDEF ACTHYS_DIMVMBP}
Procedure TVentil_EAir.AfficheResultats(_Grid: TAdvStringGrid);
Var
  m_Col : Integer;
  m_Row : Integer;
  Add50  : Integer;
  Add70  : Integer;
  Add80  : Integer;
  Add100 : Integer;
Begin
  Inherited;
          If IsNotNil(Logement) Then Begin
    _Grid.Cells[1, 0] := Logement.Reference ;
    _Grid.Cells[3, 0] := Logement.LibTypeLogement;
  End;
  If (TypePiece <= c_NbPiecesSeches) And IsNotNil(Logement) And IsNotNil(GetSysteme) Then Begin
{$IfDef OLD_ATVENTIL}
    _Grid.Cells[1, 1] := GetSysteme.EntreeAir[Logement.TypeLogement, TypePiece, Logement.T3Optimise = c_Oui][0].GetFirstLibelle;
{$Else}
        if (GetSysteme.ConfigsLogements.GetConfigLogement(Logement.TypeLogement, Logement.IdxConfig).SolutionsEAs[TypePiece][Logement.IdxSolutionEA[TypePiece]] <> Nil) and (GetSysteme.ConfigsLogements.GetConfigLogement(Logement.TypeLogement, Logement.IdxConfig).SolutionsEAs[TypePiece][Logement.IdxSolutionEA[TypePiece]].Count > 0) then
        _Grid.Cells[1, 1] := GetSysteme.ConfigsLogements.GetConfigLogement(Logement.TypeLogement, Logement.IdxConfig).SolutionsEAs[TypePiece][Logement.IdxSolutionEA[TypePiece]][0].GetFirstLibelle;
{$EndIf}
    _Grid.Cells[3, 1] := c_NomsPiecesHumides[TypePiece];
  End Else If TypePiece = c_ChoixEA Then _Grid.Cells[1, 1] := 'Nom EA choisie'
                                        Else _Grid.Cells[1, 1] := 'Saisie des d�bits';
  For m_Col := 0 To Etude.GridAcoust.ColCount - 1 Do For m_Row := 1 To Etude.GridAcoust.RowCount - 1 Do Etude.GridAcoust.Cells[m_Col, m_Row] := '';

  if Etude.Batiment.IsFabricantVIM then
        Begin
        AfficheAcoustOptair;
        Add50  := 50;
        Add70  := 70;
        Add80  := 80;
        Add100 := 100;
        End
  else
        Begin
        AfficheAcoustOptima;
        Add50  := 0;
        Add70  := 0;
        Add80  := 0;
        Add100 := 0;
        End;

  If TronconGaz Then Begin
    _Grid.Cells[1, 9] := Float2Str(PdcTotaleMini + Add50, 2) + ' Pa';
    _Grid.Cells[1, 9] := Float2Str(PdcTotaleMaxi + Add70, 2) + ' Pa';
    _Grid.Cells[3, 9] := Float2Str(PdcTotaleTotaleMini + Add50, 2) + ' Pa';
    _Grid.Cells[4, 9] := Float2Str(PdcTotaleTotaleMaxi + Add70, 2) + ' Pa';
  End Else If TronconHygro And Not TronconGaz Then Begin
    _Grid.Cells[1, 9] := Float2Str(PdcTotaleMini + Add80, 2) + ' Pa';
    _Grid.Cells[2, 9] := Float2Str(PdcTotaleMaxi + Add100, 2) + ' Pa';
    _Grid.Cells[3, 9] := Float2Str(PdcTotaleTotaleMini + Add80, 2) + ' Pa';
    _Grid.Cells[4, 9] := Float2Str(PdcTotaleTotaleMaxi + Add100, 2) + ' Pa';
  End Else Begin
    _Grid.Cells[1, 9] := Float2Str(PdcTotaleMini + Add50, 2) + ' Pa';
    _Grid.Cells[2, 9] := Float2Str(PdcTotaleMaxi + Add70, 2) + ' Pa';
    _Grid.Cells[3, 9] := Float2Str(PdcTotaleTotaleMini + Add50, 2) + ' Pa';
    _Grid.Cells[4, 9] := Float2Str(PdcTotaleTotaleMaxi + Add70, 2) + ' Pa';
  End;
  if Etude.Batiment.IsFabricantVIM then
  if (EAAChiffrer <> nil) then
        Begin
        _Grid.Cells[1, 9] := Float2Str(PdcTotaleMini + EAAChiffrer.DP_Mini , 2) + ' Pa';
        _Grid.Cells[2, 9] := Float2Str(PdcTotaleMaxi + EAAChiffrer.DP_Mini + TVentil_Caisson(Caisson).Get_PerteChargeEAQMinDT2014, 2) + ' Pa';
        _Grid.Cells[3, 9] := Float2Str(PdcTotaleTotaleMini + EAAChiffrer.DP_Mini, 2) + ' Pa';
        _Grid.Cells[4, 9] := Float2Str(PdcTotaleTotaleMaxi + EAAChiffrer.DP_Mini + TVentil_Caisson(Caisson).Get_PerteChargeEAQMinDT2014, 2) + ' Pa';
        End;

//tests r�gis
{
  if Etude.Batiment.IsFabricantACT then
        begin
        _Grid.Cells[1, 8] := Float2Str(PdcVentilMini[ttc20Deg] , 2) + ' � ' + Float2Str(PdcVentilMaxi[ttc20Deg], 2) + ' Pa';
        _Grid.Cells[2, 8] := Float2Str(PdcVentilMini[ttc7Deg] , 2) + ' � ' + Float2Str(PdcVentilMaxi[ttc7Deg], 2) + ' Pa';
        End;
}
End;
{$Else}
{$IFDEF MVN_MVNAIR}
Procedure TVentil_EAir.AfficheResultats(_Grid: TAdvStringGrid);
Var
  m_Col : Integer;
  m_Row : Integer;
  Add50  : Integer;
  Add70  : Integer;
  Add80  : Integer;
  Add100 : Integer;
Begin
  Inherited;
          If IsNotNil(Logement) Then Begin
    _Grid.Cells[1, 0] := Logement.Reference ;
    _Grid.Cells[3, 0] := Logement.LibTypeLogement;
  End;
  If (TypePiece <= c_NbPiecesSeches) And IsNotNil(Logement) And IsNotNil(GetSysteme) Then Begin
{$IfDef OLD_ATVENTIL}
    _Grid.Cells[1, 1] := GetSysteme.EntreeAir[Logement.TypeLogement, TypePiece, Logement.T3Optimise = c_Oui][0].GetFirstLibelle;
{$Else}
    _Grid.Cells[1, 1] := GetSysteme.ConfigsLogements.GetConfigLogement(Logement.TypeLogement, Logement.IdxConfig).SolutionsEAs[TypePiece][Logement.IdxSolutionEA[TypePiece]][0].GetFirstLibelle;
{$EndIf}
    _Grid.Cells[3, 1] := c_NomsPiecesHumides[TypePiece];
  End Else If TypePiece = c_ChoixEA Then _Grid.Cells[1, 1] := 'Nom EA choisie'
                                        Else _Grid.Cells[1, 1] := 'Saisie des d�bits';
  For m_Col := 0 To Etude.GridAcoust.ColCount - 1 Do For m_Row := 1 To Etude.GridAcoust.RowCount - 1 Do Etude.GridAcoust.Cells[m_Col, m_Row] := '';

  if Etude.Batiment.IsFabricantVIM then
        Begin
        AfficheAcoustOptair;
        Add50  := 50;
        Add70  := 70;
        Add80  := 80;
        Add100 := 100;
        End
  else
        Begin
        AfficheAcoustOptima;
        Add50  := 0;
        Add70  := 0;
        Add80  := 0;
        Add100 := 0;
        End;

  If TronconGaz Then Begin
    _Grid.Cells[1, 9] := Float2Str(PdcTotaleMini + Add50, 2) + ' Pa';
    _Grid.Cells[1, 9] := Float2Str(PdcTotaleMaxi + Add70, 2) + ' Pa';
    _Grid.Cells[3, 9] := Float2Str(PdcTotaleTotaleMini + Add50, 2) + ' Pa';
    _Grid.Cells[4, 9] := Float2Str(PdcTotaleTotaleMaxi + Add70, 2) + ' Pa';
  End Else If TronconHygro And Not TronconGaz Then Begin
    _Grid.Cells[1, 9] := Float2Str(PdcTotaleMini + Add80, 2) + ' Pa';
    _Grid.Cells[2, 9] := Float2Str(PdcTotaleMaxi + Add100, 2) + ' Pa';
    _Grid.Cells[3, 9] := Float2Str(PdcTotaleTotaleMini + Add80, 2) + ' Pa';
    _Grid.Cells[4, 9] := Float2Str(PdcTotaleTotaleMaxi + Add100, 2) + ' Pa';
  End Else Begin
    _Grid.Cells[1, 9] := Float2Str(PdcTotaleMini + Add50, 2) + ' Pa';
    _Grid.Cells[2, 9] := Float2Str(PdcTotaleMaxi + Add70, 2) + ' Pa';
    _Grid.Cells[3, 9] := Float2Str(PdcTotaleTotaleMini + Add50, 2) + ' Pa';
    _Grid.Cells[4, 9] := Float2Str(PdcTotaleTotaleMaxi + Add70, 2) + ' Pa';
  End;
  if Etude.Batiment.IsFabricantVIM then
  if (EAAChiffrer <> nil) then
        Begin
        _Grid.Cells[1, 9] := Float2Str(PdcTotaleMini + EAAChiffrer.DP_Mini , 2) + ' Pa';
        _Grid.Cells[2, 9] := Float2Str(PdcTotaleMaxi + EAAChiffrer.DP_Mini + TVentil_Caisson(Caisson).Get_PerteChargeEAQMinDT2014, 2) + ' Pa';
        _Grid.Cells[3, 9] := Float2Str(PdcTotaleTotaleMini + EAAChiffrer.DP_Mini, 2) + ' Pa';
        _Grid.Cells[4, 9] := Float2Str(PdcTotaleTotaleMaxi + EAAChiffrer.DP_Mini + TVentil_Caisson(Caisson).Get_PerteChargeEAQMinDT2014, 2) + ' Pa';
        End;

//tests r�gis
{
  if Etude.Batiment.IsFabricantACT then
        begin
        _Grid.Cells[1, 8] := Float2Str(PdcVentilMini[ttc20Deg] , 2) + ' � ' + Float2Str(PdcVentilMaxi[ttc20Deg], 2) + ' Pa';
        _Grid.Cells[2, 8] := Float2Str(PdcVentilMini[ttc7Deg] , 2) + ' � ' + Float2Str(PdcVentilMaxi[ttc7Deg], 2) + ' Pa';
        End;
}
End;
{$Else}
Procedure TVentil_EAir.AfficheResultats(_Grid: TAdvStringGrid);
Var
  m_Col : Integer;
  m_Row : Integer;
  Add50  : Integer;
  Add70  : Integer;
  Add80  : Integer;
  Add100 : Integer;
Begin
  Inherited;
          If IsNotNil(Logement) Then Begin
    _Grid.Cells[1, 0] := Logement.Reference ;
    _Grid.Cells[2, 0] := Logement.LibTypeLogement;
  End;

  If (TypePiece <= c_NbPiecesSeches) And IsNotNil(Logement) And IsNotNil(GetSysteme) Then Begin

{$IfDef OLD_ATVENTIL}
    _Grid.Cells[1, 1] := GetSysteme.EntreeAir[Logement.TypeLogement, TypePiece, Logement.T3Optimise = c_Oui][0].GetFirstLibelle;
{$Else}
      if (GetSysteme.ConfigsLogements.GetConfigLogement(Logement.TypeLogement, Logement.IdxConfig).SolutionsEAs[TypePiece][Logement.IdxSolutionEA[TypePiece]] <> Nil) and (GetSysteme.ConfigsLogements.GetConfigLogement(Logement.TypeLogement, Logement.IdxConfig).SolutionsEAs[TypePiece][Logement.IdxSolutionEA[TypePiece]].Count > 0) then
    _Grid.Cells[1, 1] := GetSysteme.ConfigsLogements.GetConfigLogement(Logement.TypeLogement, Logement.IdxConfig).SolutionsEAs[TypePiece][Logement.IdxSolutionEA[TypePiece]][0].GetFirstLibelle;

{$EndIf}

    _Grid.Cells[2, 1] := c_NomsPiecesHumides[TypePiece];
  End Else If TypePiece = c_ChoixEA Then _Grid.Cells[1, 1] := 'Nom EA choisie'
                                        Else _Grid.Cells[1, 1] := 'Saisie des d�bits';
  For m_Col := 0 To Etude.GridAcoust.ColCount - 1 Do For m_Row := 1 To Etude.GridAcoust.RowCount - 1 Do Etude.GridAcoust.Cells[m_Col, m_Row] := '';

  if Etude.Batiment.IsFabricantVIM then
        Begin
        AfficheAcoustOptair;
        Add50  := 50;
        Add70  := 70;
        Add80  := 80;
        Add100 := 100;
        End
  else
        Begin
        AfficheAcoustOptima;
        Add50  := 0;
        Add70  := 0;
        Add80  := 0;
        Add100 := 0;
        End;

  If TronconGaz Then Begin
    _Grid.Cells[1, 8] := Float2Str(PdcTotaleMini + Add50, 2) + ' � ' + Float2Str(PdcTotaleMaxi + Add70, 2) + ' Pa';
    _Grid.Cells[2, 8] := Float2Str(PdcTotaleTotaleMini + Add50, 2) + ' � ' + Float2Str(PdcTotaleTotaleMaxi + Add70, 2) + ' Pa';
  End Else If TronconHygro And Not TronconGaz Then Begin
    _Grid.Cells[1, 8] := Float2Str(PdcTotaleMini + Add80, 2) + ' � ' + Float2Str(PdcTotaleMaxi + Add100, 2) + ' Pa';
    _Grid.Cells[2, 8] := Float2Str(PdcTotaleTotaleMini + Add80, 2) + ' � ' + Float2Str(PdcTotaleTotaleMaxi + Add100, 2) + ' Pa';
  End Else Begin
    _Grid.Cells[1, 8] := Float2Str(PdcTotaleMini + Add50, 2) + ' � ' + Float2Str(PdcTotaleMaxi + Add70, 2) + ' Pa';
    _Grid.Cells[2, 8] := Float2Str(PdcTotaleTotaleMini + Add50, 2) + ' � ' + Float2Str(PdcTotaleTotaleMaxi + Add70, 2) + ' Pa';
  End;
  if Etude.Batiment.IsFabricantVIM then
  if (EAAChiffrer <> nil) then
        Begin
        _Grid.Cells[1, 8] := Float2Str(PdcTotaleMini + EAAChiffrer.DP_Mini , 2) + ' � ' + Float2Str(PdcTotaleMaxi + EAAChiffrer.DP_Mini + TVentil_Caisson(Caisson).Get_PerteChargeEAQMinDT2014, 2) + ' Pa';
        _Grid.Cells[2, 8] := Float2Str(PdcTotaleTotaleMini + EAAChiffrer.DP_Mini, 2) + ' � ' + Float2Str(PdcTotaleTotaleMaxi + EAAChiffrer.DP_Mini + TVentil_Caisson(Caisson).Get_PerteChargeEAQMinDT2014, 2) + ' Pa';
        End;

End;
{$EndIf}
{$EndIf}
{$EndIf}
{ ************************************************************************************************************************************************** }
Function TVentil_EAir.AfficheQuestion(_NoQuestion: Integer): Boolean;
Begin
  If _NoQuestion <= c_NbQ_Troncon Then Result := Inherited AfficheQuestion(_NoQuestion)
  Else Case _NoQuestion Of
    c_QEA_LgtCollect        : Begin
                                        if Collecteur = nil then
                                                FLogCollect := c_NON;
                                   Result := (Etude.Batiment.TypeBat in [c_BatimentCollectif, c_BatimentHotel]) and (Collecteur <> nil);
                                   End;
    c_QEA_Logement          : Result := (FLogCollect = c_Non) or (Etude.Batiment.TypeBat = c_BatimentTertiaire);
    c_QEA_Piece             : Result := true;//(Etude.Batiment.TypeBat in [c_BatimentCollectif, c_BatimentHotel]);
    c_QEA_AvertissementPiece:
                                  Begin
                                  Result := False;
                                {$IfNDef AERAU_CLIMAWIN}  
                                    if Etude.Batiment.IsFabricantANJ then
                                        Begin
                                                if (Logement <> Nil) and (TypePiece = c_SdbWC) then
                                                        begin
                                                              if (Systeme.AlizeIII) and (Systeme.HygroGaz) then
                                                                     Result := True
                                                              else
                                                              if (Systeme.AlizeIII) and (Systeme.HygroA) then
                                                                     Begin
                                                                             if (Logement.TypeLogement = c_TypeT3) and (Logement.Optimise) then
                                                                                result := true
                                                                             else
                                                                             if (Logement.TypeLogement >= c_TypeT4) then
                                                                                result := true;
                                                                     End;
                                                        End;
                                        End;
                                    {$EndIf}
                                  End;
    c_QEA_QMax              : Result := (TypePiece = c_SaisieDebitsEA) or IsEASelectionDebitNonFixe;
    c_QEA_QMin              : Result := (TypePiece = c_SaisieDebitsEA);
    c_QEA_PareFlamme        : Result := True;
    c_QEA_GammePareFlamme   : Result := Has_PareFlamme = c_OUI;
    c_QEA_Selection  : Result := TypePiece = c_ChoixEA;
    c_QEA_Regulateur : Result := True;
    c_QEA_RegulateurSel : {$IfDef VIM_OPTAIR} Result := HasRegulateur = c_oui; {$Else} Result := False; {$EndIf}
    c_QEA_Accessoires: {$IfNDef AERAU_CLIMAWIN}
                           if Etude.Batiment.IsFabricantVIM then
                                Result := Associations.Count > 0
                           else
                           {$EndIf}
                                Result := False;


    Else Result := False;
  End;

{$IfNDef AERAU_CLIMAWIN}
  if not(Etude.Batiment.IsFabricantVIM) then
{$EndIf}  
  if _NoQuestion = c_QTroncon_Diametre then Result := False;

End;
{ ************************************************************************************************************************************************** }
Function TVentil_EAir.SelectionEA: String;
Var
  m_CodeProduit : String;
Begin
  If IsNotNil(EASelection) Then m_CodeProduit := EASelection.CodeProduit
                               Else m_CodeProduit := 'VMCENTR';
  m_CodeProduit := SelectionDunProduit(m_CodeProduit);
  If m_CodeProduit <> '' Then Begin
    EASelection :=  TEdibatec_EA(BaseEdibatec.Produit(m_CodeProduit));
    Result := EASelection.Reference;
    if IsEASelectionDebitNonFixe then
        //if (QMaxSaisi > EASelection.QMaxi) or (QMaxSaisi < EASelection.QMini) then
                QMaxSaisi := EASelection.QMaxi;
  End Else Result := ' *** ';
End;
{ ************************************************************************************************************************************************** }
Function TVentil_EAir.SelectionGammeClapetCF: String;
Var
  m_CodeGamme : String;
Begin
  If IsNotNil(GammePareFlamme) Then m_CodeGamme := GammePareFlamme.CodeGamme
                               Else m_CodeGamme := 'CLAPCPFVIM02';
  m_CodeGamme := SelectionDuneGamme(m_CodeGamme);// ChoixGammeDansClasse(Copy(m_CodeGamme, 1, 7), m_CodeGamme);
  If m_CodeGamme <> '' Then Begin
    GammePareFlamme :=  TEdibatec_Gamme(BaseEdibatec.Gamme(m_CodeGamme));
    Result := GammePareFlamme.Reference;
  End Else Result := ' *** ';
End;
{ ************************************************************************************************************************************************** }
Function TVentil_EAir.EllipsClick(_Grid: TAdvStringGrid; _Ligne: Integer): String;
Begin
  Case NoQuestion(_Ligne) Of
    c_QEA_Selection : Result := SelectionEA;
    c_QEA_GammePareFlamme : Result := SelectionGammeClapetCF;
    c_QEA_Accessoires : Begin
      GetEAAChiffrer;
      SelectionDesAccessoires(Self);
      Result := 'Click pour choisir';
    End;
    c_QEA_RegulateurSel : Result := SelectionRegulateurDebitChiff;    
  End;
  Recupere(_Grid, _Ligne);
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_EAir.Recupere(_Grid: TAdvStringGrid; _Ligne: Integer);
Begin                                     
  If NoQuestion(_Ligne) <= c_NbQ_Troncon Then Inherited
  Else If AfficheQuestion(NoQuestion(_Ligne)) Then Case NoQuestion(_Ligne) Of
    c_QEA_LgtCollect : FLogCollect := _Grid.Combobox.ItemIndex;
    c_QEA_Logement   : FLogement := Etude.Batiment.Logements[_Grid.Combobox.ItemIndex];
    c_QEA_Piece  : Begin
      TypePiece := TVentil_TypeCombo(_Grid.Combobox.Items.Objects[_Grid.Combobox.ItemIndex]).idx;
{
      If IsNotNil(Logement) And (Logement.TypeLogement <> c_TypeAutre) Then TypePiece := _Grid.Combobox.ItemIndex + 1
                                                                       Else TypePiece := _Grid.Combobox.ItemIndex + round(c_ChoixEA);
}
{$IfDef OLD_ATVENTIL}
      If (TypePiece = c_Cellier) And Not ( IsNotNil(Systeme) And (Systeme.Gaz) And IsNotNil(Logement) And (Logement.ChaudiereCuisine = c_OUI) ) Then
                Begin
                if Etude.Batiment.IsFabricantVIM then
                BBS_ShowMessage('Le cellier ne peut �tre s�lectionn� qu''en syst�me gaz et avec chaudi�re dans la cuisine (cf. options du logement).', mtInformation, [mbOK], 0);
                TypePiece := c_Cuisine;
                End;
{$Else}
{$EndIf}
      if TypePiece <= c_NbPiecesSeches then
        Reference := c_NomsPiecesEA[TypePiece];
    End;
    c_QEA_QMax                        : QMaxSaisi := _Grid.Ints[1, _Ligne];
    c_QEA_QMin                        : QMinSaisi := _Grid.Ints[1, _Ligne];
    c_QEA_PareFlamme : Has_PareFlamme := _Grid.Combobox.ItemIndex;
    c_QEA_GammePareFlamme : ; // Recuperation dans le ellipsclick
    c_QEA_Selection  : ; // Recuperation dans le ellipsclick
    c_QEA_Regulateur : HasRegulateur  := _Grid.COmbobox.ItemIndex;
    c_QEA_RegulateurSel   : ; // Recuperation dans le ellipsclick    
    c_QEA_Accessoires: ;// REcuperation dans le ellipsclick;

  End;
  Inherited;
  GetEAAChiffrer;
End;
{ ************************************************************************************************************************************************** }
Function TVentil_EAir.TypeEdition(_Grid: TAdvStringGrid; _Ligne: Integer): TEditorType;
Var
  m_NoChoix : Integer;
Begin
  Result := edNone;
  if Etude.Batiment.IsFabricantVIM then
  _Grid.Combobox.DropDownCount := Max(Etude.Batiment.Logements.Count, 8);
  If Etude.Batiment.ChiffrageVerouille  = c_OUI Then Exit;
  //Parfois le GoEditing saute sans raison...
  _Grid.Options := _Grid.Options + [goEditing];
  If NoQuestion(_Ligne) <= c_NbQ_Troncon Then Result := Inherited TypeEdition(_Grid, _Ligne)
  Else If AfficheQuestion(NoQuestion(_Ligne)) Then Case NoQuestion(_Ligne) Of
    c_QEA_LgtCollect: Begin
      Result := edComboList;
    End;
    c_QEA_Logement: If Etude.Batiment.Logements.Count = 0 Then Result := edNone
                                                              Else Begin
                                                                Result := edComboList;
                                                              End;
    c_QEA_Piece  : Begin
      Result := edComboList;
  if Etude.Batiment.IsFabricantVIM then
          _Grid.Combobox.DropDownCount := Max(_Grid.Combobox.Items.Count, 8);
    End;
    c_QEA_QMax   : Result := edPositiveNumeric;
    c_QEA_QMin   : Result := edPositiveNumeric;
    c_QEA_PareFlamme : If Etude.Batiment.ClapetsSurBouches = c_Non Then Begin
      Result := edComboList;
    End Else Result := edNone;
    c_QEA_GammePareFlamme : Result := edEditBtn;
    c_QEA_Selection: Result := edEditBtn;
    c_QEA_Regulateur: Begin
      Result := edComboList;
    End;
    c_QEA_RegulateurSel : Result := edEditBtn;
    c_QEA_Accessoires : Result := edEditBtn;
  End;
if Not(Etude.Batiment.IsFabricantVIM) then
 _Grid.Combobox.DropDownCount := Min(_Grid.Combobox.DropDownCount, 20);
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_EAir.PropEdition(_Grid: TAdvStringGrid; _Ligne: Integer);
Var
  m_NoChoix : Integer;
Begin
  if Etude.Batiment.IsFabricantVIM then
  _Grid.Combobox.DropDownCount := Max(Etude.Batiment.Logements.Count, 8);
  If Etude.Batiment.ChiffrageVerouille  = c_OUI Then Exit;
  If NoQuestion(_Ligne) <= c_NbQ_Troncon Then Inherited
  Else If AfficheQuestion(NoQuestion(_Ligne)) Then Case NoQuestion(_Ligne) Of
    c_QEA_LgtCollect: Begin
      _Grid.ClearComboString;
      For m_NoChoix := Low(c_OUINON) To High(c_OUINON) Do _Grid.AddComboString(c_OUINON[m_NoChoix]);
    End;
    c_QEA_Logement: If Etude.Batiment.Logements.Count = 0 Then begin end
                                                              Else Begin
                                                                _Grid.ClearComboString;
                                                                For m_NoChoix := 0 To Etude.Batiment.Logements.Count - 1 Do _Grid.AddComboString(Etude.Batiment.Logements[m_NoChoix].Reference);
                                                              End;
    c_QEA_Piece  : Begin
      _Grid.ClearComboString;
      If IsNotNil(Logement) And (Logement.TypeLogement <> c_TypeAutre) And (Etude.Batiment.TypeBat = c_BatimentCollectif) Then
        For m_NoChoix := Low(c_NomsPiecesEA) To High(c_NomsPiecesEA) Do _Grid.AddComboStringObject(c_NomsPiecesEA[m_NoChoix], Etude.Batiment.Logements.TypesPiecesSeches[m_NoChoix])
      Else
      If IsNotNil(Logement) And (Etude.Batiment.TypeBat = c_BatimentHotel) Then
        _Grid.AddComboStringObject(c_NomsPiecesEA[c_Chambre], Etude.Batiment.Logements.TypesPiecesSeches[c_Chambre])
      Else
        begin
{$IfNDef AERAU_CLIMAWIN}
        _Grid.AddComboStringObject(c_NomsPiecesEA[c_ChoixEA], Etude.Batiment.Logements.TypesPiecesSeches[c_ChoixEA]);
{$EndIf}
        _Grid.AddComboStringObject(c_NomsPiecesEA[c_SaisieDebitsEA], Etude.Batiment.Logements.TypesPiecesSeches[c_SaisieDebitsEA]);
        end;
  if Etude.Batiment.IsFabricantVIM then
          _Grid.Combobox.DropDownCount := Max(_Grid.Combobox.Items.Count, 8);
    End;
    c_QEA_PareFlamme : If Etude.Batiment.ClapetsSurBouches = c_Non Then Begin
      _Grid.ClearComboString;
      For m_NoChoix := Low(c_OUINON) To High(c_OUINON) Do _Grid.AddComboString(c_OUINON[m_NoChoix]);
    End Else begin end;
    c_QEA_Regulateur: Begin
      _Grid.ClearComboString;
      For m_NoChoix := Low(c_OUINON) To High(c_OUINON) Do _Grid.AddComboString(c_OUINON[m_NoChoix]);
    End;
  End;
if Not(Etude.Batiment.IsFabricantVIM) then
 _Grid.Combobox.DropDownCount := Min(_Grid.Combobox.DropDownCount, 20);
End;
{ ************************************************************************************************************************************************** }
Function TVentil_EAir.HasComboBox(_Ligne: Integer): Boolean;
Begin
  If NoQuestion(_Ligne) <= c_NbQ_Troncon Then Result := Inherited HasComboBox(_Ligne)
  Else Result := AfficheQuestion(NoQuestion(_Ligne)) And (NoQuestion(_Ligne) In[c_QEA_LgtCollect, c_QEA_Logement, c_QEA_Piece, c_QEA_PareFlamme, c_QEA_Regulateur]);
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_EAir.ManageEtiquetteVisible;
Begin 
AffecterTexteEtiquette;
inherited;
End;
{ ************************************************************************************************************************************************** }
Function TVentil_EAir.DeterminerTexteEtiquette : String;
Begin
Result := '';


  If (TypePiece < c_TypeT7) And IsNotNIl(Logement) Then
        begin
                {$IfNDef AERAU_CLIMAWIN}
                if Etude.Batiment.IsFabricantVIM then
                        Begin
                        //_TempText := GetEAAChiffrer.Reference
                        Result := IntToStr(GetEAAChiffrer.QMini) + '/' + IntToStr(GetEAAChiffrer.QMaxi);
                        End
                 else
                {$EndIf}
                        Result := EASysteme.GetFirstLibelle;
        end
  Else
  If (TypePiece = c_ChoixEA) And IsNotNIl(EASelection) and ((Etude.Batiment.IsFabricantVIM and not(isDebitReglable)) or not(Etude.Batiment.IsFabricantVIM)) Then
        Begin
        if IsEASelectionDebitNonFixe then
                Result := EASelection.Reference + #13 + IntToStr(EASelection.QMini) + '/' + IntToStr(QMaxSaisi)
        else
                Result := EASelection.Reference + #13 + IntToStr(EASelection.QMini) + '/' + IntToStr(EASelection.QMaxi);
        End
  Else
  If (TypePiece = c_ChoixEA) And IsNotNIl(EASelection) then
  Result := EASelection.Reference + #13 + IntToStr(QMinSaisi) + '/' + IntToStr(QMaxSaisi)
  else
  Result := IntToStr(QMinSaisi) + '/' + IntToStr(QMaxSaisi);


        if Result <> '' then
                Begin
                if not(Etude.Batiment.IsFabricantVIM) then
                        If (Options.AffEtiq_EAReference) then
                                Result := Reference + #13 + Result;

                {$IfNDef AERAU_CLIMAWIN}
                if Etude.Batiment.IsFabricantVIM then
                        Begin
                        If (TypePiece < c_TypeT7) And IsNotNIl(Logement) Then
                                If not(TypePiece = c_ChoixEA) then
                                        begin
                                        Result := Logement.LibTypeLogement + ' / ' + c_NomsPiecesEA[TypePiece] + #13 + Result;
                                        end
                                else
                                        Begin
                                        End;
                        End;
                {$EndIf}
                End;

End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_EAir.AffecterTexteEtiquette;
var
        _TempText : String;
Begin
if LienCad = nil then exit;

_TempText := DeterminerTexteEtiquette;

        if _TempText <> '' then

                LienCad.IAero_Vilo_Base_SetTextEtiquette(_TempText, Options.TypeEtiquette);

End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_EAir.InfosFromCad;
Begin
  inherited;
  if not(Etude.Batiment.IsFabricantVIM) and not(Etude.Batiment.IsFabricantACT) and not(Etude.Batiment.IsFabricantF_A)  then
          If LongGaine = 0 Then LongGaine := 0.5;
ManageEtiquetteVisible;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_EAir.CalculeFoisonnement(Var _NbCuisHygro: Integer);
Begin
  If IsNotNil(GetSysteme) And GetSysteme.Hygro And (TypePiece = c_Cuisine) Then Inc(_NbCuisHygro);
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_EAir.CalculeNouveauFoisonnement(Var _NbEANonThermoModulante: Integer);
Begin
  if Etude.Batiment.IsFabricantVIM then
        Begin
                If (GetSysteme <> nil) and not(IsThermoModulante) Then
                        Inc(_NbEANonThermoModulante);
        End
  Else
        Begin
                If (GetSysteme <> nil) And GetSysteme.AlizeIII and not(IsThermoModulante) Then
                        Inc(_NbEANonThermoModulante);
        End;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_EAir.CalculeFoisonnement2014(Var _NbDispositifs: Integer);
begin

end;
{ ************************************************************************************************************************************************** }
Procedure TVentil_EAir.CalculeSommeDebitTempo(var QHRHygro : Real; var QTempHygro : Real; var QMiniWC : Real; var QTempWC : Real);
Var
  m_Systeme  : TVentil_Systeme;
  m_EA   : TVentil_EA;
Begin
  If TypePiece <= c_NbPiecesSeches Then
        Begin
        m_Systeme := GetSysteme;
                If IsNotNil(m_Systeme) and GetSysteme.AlizeIII then
                        Begin
                        //Nouveaux avis techniques
                                If (m_Systeme.Hygro) Then
                                        Begin
                                        m_EA := EASysteme;
//TODO                                        
{
                                                if m_EA.Tempo then
                                                        begin
                                                                If TypePiece In [c_WC, c_WCMultiple] Then
                                                                        Begin
                                                                        QMiniWC := QMiniWC + m_EA.DebitMin;
                                                                        QTempWC := QTempWC + m_EA.DebitTemp;
                                                                        End
                                                                else
                                                                if m_EA.Hygro then
                                                                        Begin
                                                                        QHRHygro := QHRHygro + m_EA.DebitHR60;
                                                                        QTempHygro := QTempHygro + m_EA.DebitTemp;
                                                                        End;

                                                        End;
}
                                        End;
                        End;
        End;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_EAir.VerifPresenceAlizeIII;
Begin
  Inherited;
  Reseau.AlizeIII_Present := Reseau.AlizeIII_Present Or ( (Systeme <> nil) And (Systeme.AlizeIII));
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_EAir.VerifPresenceAutoreglable;
Begin
  Inherited;
  Reseau.Autoreglable_Present := Reseau.Autoreglable_Present Or ( (Systeme <> nil) And Not(Systeme.Hygro) and Not(Systeme.HygroGaz));
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_EAir.SelectionneDiametre_SelonMethode;
Begin
  Inherited;
{$IfNDef AERAU_CLIMAWIN}
if Etude.Batiment.IsFabricantVIM then
begin
  If (TypePiece < c_TypeT7) And IsNotNIl(Logement) Then
        Begin
                if Copy(GetEAAChiffrer.CodeProduit, 8, 3) = 'VIM' then
                        Diametre := GetEAAChiffrer.Diametre;
        End
  Else
  If (TypePiece = c_ChoixEA) And IsNotNIl(EASelection) Then
        Begin
                if Copy(EASelection.CodeProduit, 8, 3) = 'VIM' then
                        Diametre := EASelection.Diametre;
        End;

end;
{$EndIf}
End;
{ ************************************************************* ************************************************************************************* }
function TVentil_EAir.GetLogementTroncon : TVentil_Objet;
begin
  Result := Logement;
end;
{ ************************************************************* ************************************************************************************* }
Procedure TVentil_EAir.CalculeDiametreDebitCollectif(_Iteratif : Boolean);
Begin
Inherited;
  GetEAAChiffrer;
  SetTronconGaz_Hygro;
  CalcDebit(_Iteratif);
  SelectionneDiametre_SelonMethode;
  If Diametre > 0 Then Vitesse := DebitMaxi / Diametre / Diametre * 353.67765132  { 3600 * 1000 * 2 * 1000 * 2 / Pi }
                  Else Vitesse := 0;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_EAir.CalculeDiametreDebitTertiaire;
Begin
Inherited;
  GetEAAChiffrer;
  SetTronconGaz_Hygro;
  CalcDebit(_Iteratif);

  {$IfDef VIM_OPTAIR}
{
        If (HasRegulateur = c_Oui) and (RegulateurDebitChiff <> nil) Then
                Begin
                DebitMini := DebitMini + RegulateurDebitChiff.QMini;
                DebitMaxi := DebitMaxi + RegulateurDebitChiff.QMaxi;
                DebitMiniRT := DebitMiniRT + RegulateurDebitChiff.QMini;
                DebitMaxiRT := DebitMaxiRT + RegulateurDebitChiff.QMaxi;
                End;
}                                
  {$EndIf}

  SelectionneDiametre_SelonMethode;
  If Diametre > 0 Then Vitesse := DebitMaxi / Diametre / Diametre * 353.67765132  { 3600 * 1000 * 2 * 1000 * 2 / Pi }
                  Else Vitesse := 0;
End;
{ ************************************************************************************************************************************************** }
Function TVentil_EAir.EASysteme: TVentil_EA;
Var
  m_Systeme : TVentil_Systeme;
  m_Lgt     : TVentil_Logement;
Begin
  m_Systeme := GetSysteme;
  m_Lgt     := Logement;
  If IsNotNil(m_Systeme) And IsNotNil(m_Lgt) Then Case TypePiece Of
{$IfDef OLD_ATVENTIL}
    c_Cuisine..c_WCMultiple: If (Logement.ChaudiereCuisine = c_Non) And m_Systeme.Gaz Then Result := SystemesVMC.SystemeEquivalentNonGaz(m_Systeme).EntreeAir[m_Lgt.TypeLogement, TypePiece, Logement.T3Optimise = c_Oui][0]
                                                                                  Else Result := m_Systeme.EntreeAir[m_Lgt.TypeLogement, TypePiece, Logement.T3Optimise = c_Oui][0];
    c_Cellier : Result := m_Systeme.EntreeAirSiCellier[m_Lgt.TypeLogement, TypePiece, Logement.Optimise][0];
{$Else}
    c_Cuisine..c_NbPiecesHumide: If (Logement.ChaudiereCuisine = c_Non) And m_Systeme.Gaz Then Result := SystemesVMC.SystemeEquivalentNonGaz(m_Systeme).ConfigsLogements.GetConfigLogement(m_Lgt.TypeLogement, m_Lgt.IdxConfig).SolutionsEAs[TypePiece][m_Lgt.IdxSolutionEA[TypePiece]][0]
                                                                                  Else Result := m_Systeme.ConfigsLogements.GetConfigLogement(m_Lgt.TypeLogement, m_Lgt.IdxConfig).SolutionsEAs[TypePiece][m_Lgt.IdxSolutionEA[TypePiece]][0];
{$EndIf}
  End;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_EAir.CalcDebit(_Iteratif : Boolean);
Var
  m_Systeme  : TVentil_Systeme;
  m_Lgt      : TVentil_Logement;
  m_EA   : TVentil_EA;
  m_TeSouche : TVentil_TeSouche;
  IndexCaisson  : Integer;
  m_TempObj  : TVentil_Troncon;
  TempVal1   : Integer;
  TempVal2   : Integer;
  _DebitFuite : Real;
{$IfDef OLD_ATVENTIL}
{$Else}
  cpt : Integer;
{$EndIf}
Begin

  DebitMini := 0;
  DebitMaxi := 0;
{$IfDef OLD_ATVENTIL}
  DebitMiniRT := 0;
{$Else}
    for cpt := Low(DebitMiniRT) to High(DebitMiniRT) do
      DebitMiniRT[cpt] := 0;
{$EndIf}
  QVRepBase := 0;
  DebitMaxiRT := 0;
  If TypePiece <= c_NbPiecesSeches Then
        Begin
        m_Systeme := GetSysteme;

        m_Lgt     := Logement;
                If IsNotNil(m_Systeme) And IsNotNil(m_Lgt) Then
                        Begin
                        IndexCaisson := TVentil_Caisson(Caisson).IndexListCaisson;
                        m_EA := EASysteme;
                                If TypePiece = c_Cuisine Then
                                        Inc(m_Lgt.nb_Cuisine[IndexCaisson])
                                Else
                                If TypePiece In [c_WC, c_WCMultiple] Then
                                        Inc(m_Lgt.nb_WC[IndexCaisson])
                                Else
                                If TypePiece In [c_Sdb, c_sdbWC] Then
                                        Inc(m_Lgt.nb_sdb[IndexCaisson]);


                                If TypePiece = c_SdbWC Then
                                        Inc(m_Lgt.nb_Sdb_WC_Unique[IndexCaisson])
                                else
                                If TypePiece = c_Sdb Then
                                        Inc(m_Lgt.nb_Sdb_Unique[IndexCaisson]);

                                                                
                                If IsNotNil(m_Systeme) and GetSysteme.AlizeIII then
                                        Begin
                                        //Nouveaux avis techniques
                                        m_EA := EASysteme;
                                        DebitMaxi := m_EA.DebitMax;
                                        DebitMini := m_EA.DebitMin;
                                                If (m_Systeme.Hygro) Then
                                                        Begin
//TODO
{
                                                                if m_EA.Tempo then
                                                                        begin
                                                                        m_TempObj := self;
                                                                        while (m_TempObj <> nil) and not(m_TempObj.InheritsFrom(TVentil_TeSouche)) and not(m_TempObj.InheritsFrom(TVentil_Caisson)) do
                                                                                m_TempObj := m_TempObj.Parent;

                                                                                if m_TempObj = nil then
                                                                                        DebitMaxi := m_EA.DebitMax
                                                                                else
                                                                                        Begin
                                                                                                if Etude.Batiment.IsFabricantVIM then
                                                                                                        Begin
                                                                                                        DebitMaxi := Round( TVentil_Caisson(Caisson).TauxFoisonnement / 100 * m_EA.DebitTemp + (1 - TVentil_Caisson(Caisson).TauxFoisonnement / 100) *Max(m_EA.DebitHR60, m_EA.DebitMin) );
                                                                                                        End
                                                                                                Else
                                                                                                        Begin

                                                                                                                if m_TempObj.InheritsFrom(TVentil_TeSouche) then
                                                                                                                        Begin
                                                                                                                        m_TeSouche := TVentil_TeSouche(m_TempObj);
                                                                                                                        m_Caisson  := nil;
                                                                                                                        End
                                                                                                                else
                                                                                                                        Begin
                                                                                                                        m_TeSouche := nil;
                                                                                                                        m_Caisson  := TVentil_Caisson(m_TempObj)
                                                                                                                        End;

                                                                                                                If TypePiece In [c_WC, c_WCMultiple] Then
                                                                                                                        Begin
                                                                                                                                if m_TeSouche = nil then
                                                                                                                                        Begin
                                                                                                                                        TempVal1 := m_Caisson.SommeQtempEAsWCTempo;
                                                                                                                                        TempVal2 := m_Caisson.SommeQminEAsWCTempo;
                                                                                                                                        End
                                                                                                                                else
                                                                                                                                        Begin
                                                                                                                                        TempVal1 := m_TeSouche.SommeQtempEAsWCTempo;
                                                                                                                                        TempVal2 := m_TeSouche.SommeQminEAsWCTempo;
                                                                                                                                        End;
                                                                                                                                if TempVal1 = 0 then
                                                                                                                                        TempVal1 := 1;

                                                                                                                        DebitMaxi := Round((TVentil_Caisson(Caisson).TauxFoisonnement / 100 + (1 - TVentil_Caisson(Caisson).TauxFoisonnement / 100) * TempVal2 / TempVal1) * m_EA.DebitTemp);
                                                                                                                        End
                                                                                                                Else
                                                                                                                if m_EA.Hygro then
                                                                                                                        Begin
                                                                                                                                if m_TeSouche = nil then
                                                                                                                                        Begin
                                                                                                                                        TempVal1 := m_Caisson.SommeQtempEAsHygroTempo;
                                                                                                                                        TempVal2 := m_Caisson.SommeQhrEAsHygroTempo;
                                                                                                                                        End
                                                                                                                                else
                                                                                                                                        Begin
                                                                                                                                        TempVal1 := m_TeSouche.SommeQtempEAsHygroTempo;
                                                                                                                                        TempVal2 := m_TeSouche.SommeQhrEAsHygroTempo;
                                                                                                                                        End;

                                                                                                                                if TempVal1 = 0 then
                                                                                                                                        TempVal1 := 1;
                                                                                                                        DebitMaxi := Round((TVentil_Caisson(Caisson).TauxFoisonnement / 100 + (1 - TVentil_Caisson(Caisson).TauxFoisonnement / 100) * TempVal2 / TempVal1) * m_EA.DebitTemp);
                                                                                                                        End
                                                                                                                else
                                                                                                                        DebitMaxi := m_EA.DebitMax;
                                                                                                        End;
                                                                                        End;
                                                                        end
                                                                else
                                                                        if IsThermoModulante then
                                                                                DebitMaxi := m_EA.Debit
                                                                        else
                                                                        if m_EA.Hygro then
                                                                                DebitMaxi := m_EA.Debit
                                                                        else
                                                                        //if fixeouautoreglable then
                                                                                DebitMaxi := m_EA.Debit;
}
                                                                if m_Systeme.HygroGaz then
                                                                        DUGD := 2 / (TVentil_Caisson(Caisson).TauxFoisonnement / 100)
                                                                else
                                                                        DUGD := 1 / (TVentil_Caisson(Caisson).TauxFoisonnement / 100);


                                                        end;
                                        End
                                else
                                        Begin
                                        DebitMini := m_EA.DebitMin;
                                                If TronconGaz Then
                                                        Begin
                                                        DebitMaxi := Max(m_EA.DebitMax, m_EA.DebitSup);
                                                        DUGD := 2;
                                                        End
                                                Else
                                                        Begin
                                                        DebitMaxi := m_EA.DebitMax;
                                                                If (Not TVentil_Caisson(Caisson).Foisonne5) And (Not TVentil_Caisson(Caisson).Foisonne6) Or (Not TronconHygro) Then
                                                                        DebitMaxi := m_EA.DebitMax
                                                                Else
                                                                If TVentil_Caisson(Caisson).Foisonne5 Then
                                                                        DebitMaxi := m_EA.DebitMax
                                                                Else
                                                                If TVentil_Caisson(Caisson).Foisonne6 Then
                                                                        DebitMaxi := m_EA.DebitMax;

//TODO
{
                                                                if m_EA.Tempo and (TypePiece = c_Cuisine) then
                                                                        DUGD := 1
                                                                else
}                                                                
                                                                        DUGD := 2;
                                                        End;
                                        End;
                        End
        End

  Else
        CalcDebitLocalAutre;

  if not(Etude.Batiment.IsFabricantVIM) then
  Begin
//  _DebitFuite := 0;
  _DebitFuite := Etude.Batiment.DebitFuite / 100 * DebitMaxi ;
  End
  else
  begin
          if Etude.Is_FullVeloduct then
                _DebitFuite := 2/100
          else
                _DebitFuite := 10/100;

  _DebitFuite := _DebitFuite * DebitMaxi;

  {
        If TronconGaz then
                _DebitFuite := DebitMaxi * 10 / 100
        else
                _DebitFuite := DebitMaxi * Etude.Batiment.DebitFuite / 100; //le d�bit de fuite est calcul� � partir du d�bit maxi
  }                
  end;

        if (TypePiece <> c_Cuisine) then
                DUGD := 0;
{$IfDef OLD_ATVENTIL}
  DebitMiniRT := DebitMini;
{$Else}
    for cpt := Low(DebitMiniRT) to High(DebitMiniRT) do
      DebitMiniRT[cpt] := DebitMini;;
{$EndIf}
  QVRepBase := DebitMini;
  DebitMaxiRT := DebitMaxi;
  if (GetSysteme <> nil) and (GetSysteme.Hygro) then
        Begin
//        DebitMiniRT := GetSysteme.Deperdition[Logement.TypeLogement, (Logement.T3Optimise = c_Oui) and not(Logement.Systeme.Gaz), TypePiece = c_SdbWC].Qvrep;
{$IfDef OLD_ATVENTIL}
          if DebitMiniRT = 0 then
            DebitMiniRT := DebitMini;
{$Else}
          for cpt := Low(DebitMiniRT) to High(DebitMiniRT) do
            if DebitMiniRT[cpt] = 0 then
              DebitMiniRT[cpt] := DebitMini;
{$EndIf}
        End;
//          _DebitFuite := 0;
//        if Etude.Batiment.IsFabricantVIM then
          DebitMaxi := Round(DebitMaxi  + _DebitFuite);

  DebitMini := Round(DebitMini  + _DebitFuite);

  QEADebitMaxi := DebitMaxi;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_EAir.CalcDebitEAChoisie;
{$IfDef OLD_ATVENTIL}
{$Else}
Var
  cpt : Integer;
{$EndIf}
Begin
  If IsNotNil(EASelection) Then Begin
    DebitMini := EASelection.QMini;
    DebitMaxi := EASelection.QMaxi;
{$IfDef OLD_ATVENTIL}
    DebitMiniRT := EASelection.QMini;
{$Else}
    for cpt := Low(DebitMiniRT) to High(DebitMiniRT) do
      DebitMiniRT[cpt] := EASelection.QMini;
{$EndIf}
    QVRepBase := EASelection.QMini;
    DebitMaxiRT := EASelection.QMaxi;
  End Else Begin
    DebitMini := 0;
    DebitMaxi := 0;
{$IfDef OLD_ATVENTIL}
    DebitMiniRT := 0;
{$Else}
    for cpt := Low(DebitMiniRT) to High(DebitMiniRT) do
      DebitMiniRT[cpt] := 0;
{$EndIf}
    QVRepBase := 0;
    DebitMaxiRT := 0;
  ENd;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_EAir.CalculeRecapColonne(Var _TeSouche: TVentil_Troncon; _Iteratif : Boolean);
var
        ObjAmettreAJour : TVentil_Troncon;
        PrisEnCompteDTU : Boolean;
        PDCAdd           : Integer;
        cptTemperature  : TTemperatureCalc;
Begin
PrisEnCompteDTU := True;
  Inherited CalculeRecapColonne(_TeSouche, _Iteratif);

  if Etude.Batiment.IsFabricantVIM then
    begin
    PDCAdd := 20;
    end
  else
    PDCAdd := 0;

  if (Etude.DTU2014) and (Caisson <> Nil) and Etude.Batiment.IsFabricantVIM then
    begin
    PDCAdd := PDCAdd + TVentil_Caisson(Caisson).Get_PerteChargeVentDT2014;
    PDCAdd := PDCAdd + TVentil_Caisson(Caisson).Get_PerteChargeObstacleRejetDT2014;
    end;

  If TronconGaz Then Begin
    DeltaP[ttc20deg].MaxAQmax := 140 + PdcTotaleTotaleMaxi + 20;
    DeltaP[ttc20deg].MinAQmax :=  50 + PdcTotaleTotaleMaxi + 20;
    DeltaP[ttc20deg].MaxAQmin := 140 + PdcTotaleTotaleMini + PDCAdd;
    DeltaP[ttc20deg].MinAQmin :=  50 + PdcTotaleTotaleMini + PDCAdd;

    PdcVentil[ttc20Deg].MaxAQmin := 50 + PdcTotaleTotaleMini;
    PdcVentil[ttc20Deg].MaxAQmax := 50 + PdcTotaleTotaleMaxi;
    if Etude.Batiment.IsFabricantVIM then
      begin
      PdcVentil[ttc20Deg].MinAQmax := 50 + PdcTotaleTotaleMaxi + 20;
      PdcVentil[ttc20Deg].minAQmin := 50 + PdcTotaleTotaleMini;
      end
    else
      begin
      PdcVentil[ttc20Deg].MinAQmax := 80 + PdcTotaleTotaleMaxi + 20;
      PdcVentil[ttc20Deg].minAQmin := 80 + PdcTotaleTotaleMini;
      end;

  End Else If TronconHygro And Not TronconGaz Then Begin
    DeltaP[ttc20deg].MaxAQmax := 160 + PdcTotaleTotaleMaxi + 20;
    DeltaP[ttc20deg].MinAQmax :=  80 + PdcTotaleTotaleMaxi + 20;
    DeltaP[ttc20deg].MaxAQmin := 160 + PdcTotaleTotaleMini + PDCAdd;
    DeltaP[ttc20deg].MinAQmin :=  80 + PdcTotaleTotaleMini + PDCAdd;


    PdcVentil[ttc20Deg].MaxAQmin := 80 + PdcTotaleTotaleMini;
    PdcVentil[ttc20Deg].MaxAQmax := 80 + PdcTotaleTotaleMaxi;
    PdcVentil[ttc20Deg].MinAQmax :=  80 + PdcTotaleTotaleMaxi + 20;
    PdcVentil[ttc20Deg].MinAQmin := 80 + PdcTotaleTotaleMini;
  End Else If Not TronconHygro And Not TronconGaz Then Begin

    DeltaP[ttc20deg].MaxAQmax := 160 + PdcTotaleTotaleMaxi + 20;
    DeltaP[ttc20deg].MinAQmax :=  50 + PdcTotaleTotaleMaxi + 20;
    DeltaP[ttc20deg].MaxAQmin := 160 + PdcTotaleTotaleMini + PDCAdd;
    DeltaP[ttc20deg].MinAQmin :=  50 + PdcTotaleTotaleMini + PDCAdd;

    PdcVentil[ttc20Deg].MaxAQmin := 50 + PdcTotaleTotaleMini + 20;
    PdcVentil[ttc20Deg].MaxAQmax := 50 + PdcTotaleTotaleMaxi;
    PdcVentil[ttc20Deg].MinAQmax :=  50 + PdcTotaleTotaleMaxi + 20; { Le 20 correspond � la d�pression pour les entr�es d'air }
    PdcVentil[ttc20Deg].MinAQmin := 50 + PdcTotaleTotaleMini;

  End;

  if Etude.Batiment.IsFabricantVIM  or (Not(Etude.Batiment.IsFabricantVIM) and ((Etude.Batiment.TypeBat = c_BatimentTertiaire) or Systeme.DoubleFlux)) then
  if (EAAChiffrer <> nil) then
        Begin
        DeltaP[ttc20deg].MaxAQmax := EAAChiffrer.DP_Maxi + PdcTotaleTotaleMaxi + 20;
        DeltaP[ttc20deg].MinAQmax :=  EAAChiffrer.DP_Mini + PdcTotaleTotaleMaxi + 20;
        DeltaP[ttc20deg].MaxAQmin := EAAChiffrer.DP_Maxi + PdcTotaleTotaleMini + PDCAdd;
        DeltaP[ttc20deg].MinAQmin :=  EAAChiffrer.DP_Mini + PdcTotaleTotaleMini + PDCAdd;

        PdcVentil[ttc20Deg].MaxAQmin := EAAChiffrer.DP_Mini + PdcTotaleTotaleMini + 20;
        PdcVentil[ttc20Deg].MaxAQmax := EAAChiffrer.DP_Mini + PdcTotaleTotaleMaxi;
        PdcVentil[ttc20Deg].MinAQmax := EAAChiffrer.DP_Mini + PdcTotaleTotaleMaxi + 20;
        PdcVentil[ttc20Deg].MinAQmin := EAAChiffrer.DP_Mini + PdcTotaleTotaleMini;

        end;

  if Etude.Batiment.IsFabricantVIM then
  If Not(Etude.DTUActive) And (TVentil_Caisson(Caisson).Reseau_BouchePdcMinimum <> nil) And (GUIDToString(TVentil_Caisson(Caisson).Reseau_BouchePdcMinimum.Id) = GUIDToString(Self.Id)) then
        Begin
        PrisEnCompteDTU := False;
        End;

if not(Etude.Batiment.IsFabricantVIM)  and Not(Etude.Batiment.TypeBat = c_BatimentTertiaire) and Not(Systeme.DoubleFlux)  then
begin
  if TronconGaz then
  begin
    PdcVentil[ttc20Deg].MaxAQmin := 140 + PdcTotaleTotaleMini;
    PdcVentil[ttc20Deg].MaxAQmax := 140 + PdcTotaleTotaleMaxi;
    PdcVentil[ttc20Deg].MinAQmax := 80 + PdcTotaleTotaleMaxi + 20; { Le 20 correspond � la d�pression pour les entr�es d'air }
    PdcVentil[ttc20Deg].MinAQmin := 80 + PdcTotaleTotaleMini;
  end
  else
  if TronconHygro and not(TronconGaz) then
  begin
    PdcVentil[ttc20Deg].MaxAQmin := 160 + PdcTotaleTotaleMini;
    PdcVentil[ttc20Deg].MaxAQmax := 160 + PdcTotaleTotaleMaxi;
    PdcVentil[ttc20Deg].MinAQmax := 80 + PdcTotaleTotaleMaxi + 20; { Le 20 correspond � la d�pression pour les entr�es d'air }
    PdcVentil[ttc20Deg].MinAQmin := 80 + PdcTotaleTotaleMini;
  end
  else
  if not(TronconHygro) and not(TronconGaz) then
  begin
    PdcVentil[ttc20Deg].MaxAQmin := 160 + PdcTotaleTotaleMini;
    PdcVentil[ttc20Deg].MaxAQmax := 160 + PdcTotaleTotaleMaxi;
    PdcVentil[ttc20Deg].MinAQmax :=  50 + PdcTotaleTotaleMaxi + 20; { Le 20 correspond � la d�pression pour les entr�es d'air }
    PdcVentil[ttc20Deg].MinAQmin := 50 + PdcTotaleTotaleMini;
  end;
End;

  if _Iteratif then
    begin
      if (TVentil_Caisson(Caisson).NbIterationCalcul = 0) then
        TVentil_Caisson(Caisson).FinIterationCalcul := False
      else
      if (TVentil_Caisson(Caisson).NbIterationCalcul > 0) and
         (abs(PdcVentil[ttc20Deg].MaxAQmin - FPressionMini_Iteratif[ttc20Deg]) > 0.5) and
         (abs(PdcVentil[ttc20Deg].MinAQmax - FPressionMaxi_Iteratif[ttc20Deg]) > 0.5) then
          begin
          TVentil_Caisson(Caisson).FinIterationCalcul := False;
          end;
      for cptTemperature := Low(TTemperatureCalc) to High(TTemperatureCalc) do
        begin
        DeltaP[cptTemperature].MaxAQmax := FPressionMaxi_Iteratif[cptTemperature];
        DeltaP[cptTemperature].MinAQmax := FPressionMaxi_Iteratif[cptTemperature];
        DeltaP[cptTemperature].MaxAQmin := FPressionMini_Iteratif[cptTemperature];
        DeltaP[cptTemperature].MinAQmin := FPressionMini_Iteratif[cptTemperature];
        PdcVentil[cptTemperature].MaxAQmin := FPressionMini_Iteratif[cptTemperature];
        PdcVentil[cptTemperature].MaxAQmax := FPressionMaxi_Iteratif[cptTemperature];
        PdcVentil[cptTemperature].MinAQmax := FPressionMaxi_Iteratif[cptTemperature];
        PdcVentil[cptTemperature].MinAQmin := FPressionMini_Iteratif[cptTemperature];
        end;
    end;

  If _TeSouche = nil Then ObjAmettreAJour := Parentcalcul
  else ObjAmettreAJour := _TeSouche;
  If ObjAmettreAJour = nil Then exit;
  // Calcul de la perte de charge mini sur la colonne
  If (ObjAmettreAJour.PdcTotalAvalMaxi < 0.001) Or (ObjAmettreAJour.PdcTotalAvalMini > PdcTotaleTotaleMini) Then Begin
        if PrisEnCompteDTU then
                Begin
                ObjAmettreAJour.PdcRegulierAvalMini   := PdcReguliereTotaleMini;
                ObjAmettreAJour.PdcSingulierAvalMini  := PdcSinguliereTotaleMini;
                ObjAmettreAJour.PdcTotalAvalMini      := PdcTotaleTotaleMini;
                If ObjAmettreAJour.InheritsFrom(TVentil_TeSouche) Then
                TVentil_TeSouche(ObjAmettreAJour).TronconAvalMini := Self;
                End;
  End;
  // Calcul de la perte de charge maxi sur la colonne
  If ObjAmettreAJour.PdcTotalAvalMaxi < PdcTotaleTotaleMaxi Then Begin
    ObjAmettreAJour.PdcRegulierAvalMaxi  := PdcReguliereTotaleMaxi;
    ObjAmettreAJour.PdcSingulierAvalMaxi := PdcSinguliereTotaleMaxi;
    ObjAmettreAJour.PdcTotalAvalMaxi     := PdcTotaleTotaleMaxi;
    If ObjAmettreAJour.InheritsFrom(TVentil_TeSouche) Then    
    TVentil_TeSouche(ObjAmettreAJour).TronconAvalMaxi := Self;
  End;


for cptTemperature  := Low(TTemperatureCalc) to High(TTemperatureCalc) do
begin
  // Report de la perte de charge avec ventil sur la colonne
  If ObjAmettreAJour.PdcVentil[cptTemperature].MinAQmax < PdcVentil[cptTemperature].MinAQmax Then ObjAmettreAJour.PdcVentil[cptTemperature].MinAQmax := PdcVentil[cptTemperature].MinAQmax;
  If ObjAmettreAJour.PdcVentil[cptTemperature].MinAQmin < PdcVentil[cptTemperature].MinAQmin Then ObjAmettreAJour.PdcVentil[cptTemperature].MinAQmin := PdcVentil[cptTemperature].MinAQmin;

if Etude.Batiment.IsFabricantVIM then
begin

  If ObjAmettreAJour.DeltaP[cptTemperature].MaxAQmax > DeltaP[cptTemperature].MaxAQmax Then ObjAmettreAJour.DeltaP[cptTemperature].MaxAQmax := DeltaP[cptTemperature].MaxAQmax;

        if PrisEnCompteDTU then
                If ObjAmettreAJour.DeltaP[cptTemperature].MaxAQmin > DeltaP[cptTemperature].MaxAQmin Then ObjAmettreAJour.DeltaP[cptTemperature].MaxAQmin := DeltaP[cptTemperature].MaxAQmin;

  If (ObjAmettreAJour.DeltaP[cptTemperature].MinAQmax < 0.0001) Or (ObjAmettreAJour.DeltaP[cptTemperature].MinAQmax < DeltaP[cptTemperature].MinAQmax) Then ObjAmettreAJour.DeltaP[cptTemperature].MinAQmax := DeltaP[cptTemperature].MinAQmax;


        if PrisEnCompteDTU then
                If (ObjAmettreAJour.DeltaP[cptTemperature].MinAQmin < 0.0001) Or (ObjAmettreAJour.DeltaP[cptTemperature].MinAQmin < DeltaP[cptTemperature].MinAQmin) Then ObjAmettreAJour.DeltaP[cptTemperature].MinAQmin := DeltaP[cptTemperature].MinAQmin;

        if PrisEnCompteDTU then
          begin
            If (ObjAmettreAJour.PdcVentil[cptTemperature].MaxAQmin >= 30000000000)  Or (ObjAmettreAJour.PdcVentil[cptTemperature].MaxAQmin < PdcVentil[cptTemperature].MaxAQmin) Then ObjAmettreAJour.PdcVentil[cptTemperature].MaxAQmin := PdcVentil[cptTemperature].MaxAQmin;
            If (ObjAmettreAJour.PdcVentil[cptTemperature].MaxAQmax >= 30000000000)  Or (ObjAmettreAJour.PdcVentil[cptTemperature].MaxAQmax < PdcVentil[cptTemperature].MaxAQmin) Then ObjAmettreAJour.PdcVentil[cptTemperature].MaxAQmax := PdcVentil[cptTemperature].MaxAQmax;
          end;

end
else
begin
  If ObjAmettreAJour.DeltaP[cptTemperature].MaxAQmax < DeltaP[cptTemperature].MaxAQmax Then ObjAmettreAJour.DeltaP[cptTemperature].MaxAQmax := DeltaP[cptTemperature].MaxAQmax;


        if PrisEnCompteDTU then
                If ObjAmettreAJour.DeltaP[cptTemperature].MaxAQmin < DeltaP[cptTemperature].MaxAQmin Then ObjAmettreAJour.DeltaP[cptTemperature].MaxAQmin := DeltaP[cptTemperature].MaxAQmin;


  If (ObjAmettreAJour.DeltaP[cptTemperature].MinAQmax < 0.0001) Or (ObjAmettreAJour.DeltaP[cptTemperature].MinAQmax > DeltaP[cptTemperature].MinAQmax) Then ObjAmettreAJour.DeltaP[cptTemperature].MinAQmax := DeltaP[cptTemperature].MinAQmax;

        if PrisEnCompteDTU then
                If (ObjAmettreAJour.DeltaP[cptTemperature].MinAQmin < 0.0001) Or (ObjAmettreAJour.DeltaP[cptTemperature].MinAQmin > DeltaP[cptTemperature].MinAQmin) Then ObjAmettreAJour.DeltaP[cptTemperature].MinAQmin := DeltaP[cptTemperature].MinAQmin;

        if PrisEnCompteDTU then
          begin
          If (ObjAmettreAJour.PdcVentil[cptTemperature].MaxAQmin < 0.0001)  Or (ObjAmettreAJour.PdcVentil[cptTemperature].MaxAQmin > PdcVentil[cptTemperature].MaxAQmin) Then ObjAmettreAJour.PdcVentil[cptTemperature].MaxAQmin := PdcVentil[cptTemperature].MaxAQmin;
          If (ObjAmettreAJour.PdcVentil[cptTemperature].MaxAQmax < 0.0001)  Or (ObjAmettreAJour.PdcVentil[cptTemperature].MaxAQmax > PdcVentil[cptTemperature].MaxAQmax) Then ObjAmettreAJour.PdcVentil[cptTemperature].MaxAQmax := PdcVentil[cptTemperature].MaxAQmax;
          end;
end;
end;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_EAir.CalcDebitChoisi;
Begin
  DebitMini := QMinSaisi;
  DebitMaxi := QMaxSaisi;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_EAir.CalcDebitLocalAutre;
Begin
  If (TypePiece = c_ChoixEA) and not(IsEASelectionDebitNonFixe) Then CalcDebitEAChoisie
                               Else CalcDebitChoisi;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_EAir.StoreValues_Iteratif;
var
  cptTemperature : TTemperatureCalc;
begin
  for cptTemperature := Low(TTemperatureCalc) to High(TTemperatureCalc) do
    begin
    FPressionMini_Iteratif[cptTemperature] := GetPression(false, cptTemperature, true, True);
    FPressionMaxi_Iteratif[cptTemperature] := GetPression(true, cptTemperature, true, True);
    end;
end;
{ ************************************************************************************************************************************************** }
Procedure TVentil_EAir.SetTronconGaz_Hygro;
Var
  m_Systeme : TVentil_Systeme;
Begin
  TronconHygro := False;
  TronconGaz   := False;
  m_Systeme := GetSysteme;
  If IsNotNil(m_Systeme) Then Begin
    TronconGaz   := m_Systeme.Gaz And (TypePiece = c_Cuisine);
    TronconHygro := m_Systeme.Hygro;
  End;
  { Cas des EAs gaz auto en cellier }
  If TronconGaz And IsNotNil(Logement) And (Logement.ChaudiereCuisine <> C_OUI) Then
        TronconGaz := False;
End;
{ ************************************************************************************************************************************************** }
Function TVentil_EAir.IsThermoModulante : Boolean;
Var
  m_Systeme : TVentil_Systeme;
Begin
  Result := false;
  m_Systeme := GetSysteme;
  If IsNotNil(m_Systeme) And IsNotNil(Logement) Then
    Result := m_Systeme.Gaz And (TypePiece = c_Cuisine) and m_Systeme.Hygro and (Logement.ChaudiereCuisine = C_OUI);
End;
{ ************************************************************************************************************************************************** }
Function  TVentil_EAir.IsEASelectionDebitNonFixe : boolean;
Begin
Result := (FTypePiece = c_ChoixEA) and IsNotNil(EASelection) and (EASelection.QMini <> EASelection.QMaxi);
End;
{ ************************************************************************************************************************************************** }
Function TVentil_EAir.GetRegulateurParent : TVentil_RegulateurDebit;
var
        m_TempObj : TVentil_Troncon;
Begin
result := nil;

{
        if Etude.Batiment.TypeBat in [c_BatimentCollectif, c_BatimentHotel] then
                exit;
}                

m_TempObj := self;

while not (m_TempObj.InheritsFrom(TVentil_RegulateurDebit)) do
        if m_TempObj.Parent = nil then
                break
        else
                m_TempObj := m_TempObj.Parent;

        if (m_TempObj <> nil) and m_TempObj.InheritsFrom(TVentil_RegulateurDebit) then
                result := TVentil_RegulateurDebit(m_TempObj);
End;
{ ************************************************************************************************************************************************** }
function TVentil_EAir.isEACoupeFeu : boolean;
Begin
result := isFromGamme('03') or isFromGamme('07');
End;
{ ************************************************************************************************************************************************** }
function TVentil_EAir.isDebitReglable : boolean;
Begin
Result := (TypePiece = c_ChoixEA) And IsNotNIl(EASelection) and ((EASelection.Dzeta <> 0) or (EASelection.Coef_A <> 0));
End;
{ ************************************************************************************************************************************************** }
function TVentil_EAir.GetReglage(_ValDP : Real) : Real;
var
        _Solutions : TStringList;
        i          : Integer;
        _Dzeta     : Real;
Begin
Result := -100;

if not(isDebitReglable) then exit;

_Dzeta := _ValDP / (0.6 * sqr(Vitesse));

_Solutions := TStringList.Create;
GetSolutions3emeDegreDansR(_Solutions, EASelection.Coef_A, EASelection.Coef_B, EASelection.Coef_C, EASelection.Coef_D - _Dzeta);

        if _Solutions.Count > 0 then
                for i := 0 To _Solutions.Count - 1 do
                        if (Str2Float(_Solutions[i]) >= EASelection.Reglage_Min) and (Str2Float(_Solutions[i]) <= EASelection.Reglage_Max) then
                                Begin
                                Result :=Str2Float(_Solutions[i]);
                                break;
                                End;
FreeAndNil(_Solutions);

End;
{ ************************************************************************************************************************************************** }
function TVentil_EAir.IsFromGamme(_2Gamme : String) : boolean;
Begin
result := (TypePiece = c_ChoixEA) And IsNotNIl(EASelection) and (Copy(EASelection.CodeProduit, 11, 2) = _2Gamme);

if Result then exit;

{$IfDef OLD_ATVENTIL}
Result := (TypePiece <= c_NbPiecesSeches) And IsNotNil(Logement) And IsNotNil(GetSysteme) and (Copy(GetSysteme.EntreeAir[Logement.TypeLogement, TypePiece, Logement.T3Optimise = c_Oui][0].Edibatec, 11, 2) = _2Gamme);
{$Else}
Result := (TypePiece <= c_NbPiecesSeches) And IsNotNil(Logement) And IsNotNil(GetSysteme) and (Copy(GetSysteme.ConfigsLogements.GetConfigLogement(Logement.TypeLogement, Logement.IdxConfig).SolutionsEAs[TypePiece][Logement.IdxSolutionEA[TypePiece]][0].Edibatec, 11, 2) = _2Gamme);
{$EndIf}
End;
{ ************************************************************************************************************************************************** }
function TVentil_EAir.isAutoReglable : boolean;
Begin
result := isFromGamme('AA');
End;
{ ************************************************************************************************************************************************** }
Function TVentil_EAir.GetSysteme: TVentil_Systeme;
Begin
  Result := nil;
  If IsNotNil(Logement) Then Result := Logement.Systeme;
End;
{ ************************************************************************************************************************************************** }
Function TVentil_EAir.GetLogement: TVentil_Logement;
Begin
  Result := Nil;
  If FLogCollect = c_NON Then Result := FLogement
                         Else If IsNotNil(Collecteur) And IsNotNil(Collecteur.Logement) Then Result := Collecteur.Logement;
End;
{ ************************************************************************************************************************************************** }
Function TVentil_EAir.Collecteur: TVentil_Collecteur;
Var
  m_Troncon : TVentil_Troncon;
Begin
  Result := Nil;
  m_Troncon := Self;
  While IsNotNil(m_Troncon) And (m_Troncon.Classtype <> TVentil_Collecteur) Do Begin
    m_Troncon := m_Troncon.Parent;
    If isNotNil(m_Troncon) And (m_Troncon.InheritsFrom(TVentil_Collecteur)) Then Result := TVentil_Collecteur(m_Troncon);
  End;
{
  if result = nil then
        FLogCollect    := c_Non
}
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_EAir.Load;
Var
  m_Str : String;
  m_No  : Integer;
Begin
  FTypePiece    := Table_Etude_EA.Donnees.FieldByName('PIECE').AsInteger;
  QMaxSaisi     := Table_Etude_EA.Donnees.FieldByName('QMAX').AsInteger;
  QMinSaisi     := Table_Etude_EA.Donnees.FieldByName('QMIN').AsInteger;
  Has_PareFlamme:= Table_Etude_EA.Donnees.FieldByName('HAS_PAREFLAMME').AsInteger;
  HasRegulateur := Table_Etude_EA.Donnees.FieldByName('HASREGULATEUR').AsInteger;


  m_Str         := Table_Etude_EA.Donnees.FieldByName('CODE_REG').AsWideString;
  If m_Str <> '' Then RegulateurDebitChiff := TEdibatec_RegulateurDebit(BaseEdibatec.Produit(m_Str))
                 Else RegulateurDebitChiff := nil;
  m_Str         := Table_Etude_EA.Donnees.FieldByName('CODE_EA').AsWideString;
  If m_Str <> '' Then EASelection := TEdibatec_EA(BaseEdibatec.Produit(m_Str))
                 Else EASelection := nil;
  m_Str := Table_Etude_EA.Donnees.FieldByName('GAMME_CF').AsWideString;
  If m_Str <> '' Then GammePareFlamme := TEdibatec_Gamme(BaseEdibatec.Gamme(m_Str))
                 Else GammePareFlamme := nil;
  FLogCollect := Table_Etude_EA.Donnees.FieldByName('LOG_COLLECT').AsInteger;
  m_Str       := Table_Etude_EA.Donnees.FieldByName('ID_LOGEMENT').AsWideString;
  For m_No := 0 To Etude.Batiment.Logements.Count - 1 Do If GUIDToString(Etude.Batiment.Logements[m_No].Id) = m_Str Then Begin
    FLogement := Etude.Batiment.Logements[m_No];
    Break;
  End;

  //regarder GetEAAChiffrer pour le chargement asynchrone des quantit�s des associations
  FQteAssociations := TStringList.Create;
  FQteAssociations := StrToStringList(Table_Etude_EA.Donnees.FieldByName('QTEASSOCIATIONS').AsWideString, '|');
  FCodesAssociations := TStringList.Create;
  FCodesAssociations := StrToStringList(Table_Etude_EA.Donnees.FieldByName('CODESASSOCIATIONS').AsWideString, '|');
  AssociationsNonTrouvees := TStringList.Create;


  Inherited;

End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_EAir.Save;
Var        
        m_NoAsso : integer;
        _QteAsso : String;
        _CodesAsso : String;
Begin
  Table_Etude_EA.Donnees.Insert;
  Table_Etude_EA.Donnees.FieldByName('ID_EA').AsWideString := GUIDToString(Id);
  {$IfDef AERAU_CLIMAWIN}
  Table_Etude_EA.Donnees.FieldByName('IDRESEAU').AsWideString := Etude.IdReseau;
  {$EndIf}
  Table_Etude_EA.Donnees.FieldByName('PIECE').AsInteger := TypePiece;
  Table_Etude_EA.Donnees.FieldByName('QMAX').AsInteger := QMaxSaisi;
  Table_Etude_EA.Donnees.FieldByName('QMIN').AsInteger := QMinSaisi;
  If EASelection <> nil Then Table_Etude_EA.Donnees.FieldByName('CODE_EA').AsWideString := EASelection.CodeProduit
                            Else Table_Etude_EA.Donnees.FieldByName('CODE_EA').AsWideString := '';
  If GammePareFlamme <> nil Then Table_Etude_EA.Donnees.FieldByName('GAMME_CF').AsWideString := GammePareFlamme.CodeGamme
                            Else Table_Etude_EA.Donnees.FieldByName('GAMME_CF').AsWideString := '';
  Table_Etude_EA.Donnees.FieldByName('HAS_PAREFLAMME').AsInteger := Has_PareFlamme;
  Table_Etude_EA.Donnees.FieldByName('HASREGULATEUR').AsInteger := HasRegulateur;


    If RegulateurDebitChiff <> nil Then Table_Etude_EA.Donnees.FieldByName('CODE_REG').AsWideString := RegulateurDebitChiff.CodeProduit
                            Else Table_Etude_EA.Donnees.FieldByName('CODE_REG').AsWideString := '';
  Table_Etude_EA.Donnees.FieldByName('LOG_COLLECT').AsInteger := FLogCollect;
  If IsNotNil(Logement) And (FLogCollect = c_NON) Then Table_Etude_EA.Donnees.FieldByName('ID_LOGEMENT').AsWideString := GUIDToString(Logement.Id)
                                                  Else Table_Etude_EA.Donnees.FieldByName('ID_LOGEMENT').AsWideString := '';

  _QteAsso := '';
        If FProduit = nil then
                Begin
                Table_Etude_EA.Donnees.FieldByName('QTEASSOCIATIONS').AsWideString := '';
                Table_Etude_EA.Donnees.FieldByName('CODESASSOCIATIONS').AsWideString := '';
                End
        else
                Begin
                For m_NoAsso := 0 to Associations.Count - 1 do
                        begin
                        _QteAsso := _QteAsso + FloatToStrF(TEdibatec_Association(Associations.Produit(m_NoAsso)).Qte, ffFixed, 2, 2) + '|';
                        _CodesAsso := _CodesAsso + TEdibatec_Association(Associations.Produit(m_NoAsso)).CodeProduit + '|';                        
                        end;
                Table_Etude_EA.Donnees.FieldByName('QTEASSOCIATIONS').AsWideString := _QteAsso;
                Table_Etude_EA.Donnees.FieldByName('CODESASSOCIATIONS').AsWideString := _CodesAsso;
                End;

  Inherited;
                  
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_EAir.CalculerQuantitatifInternal;
Var
  m_EAEdibatec : TEdibatec_EA;
  EmplacementReseau : integer;

  m_Manchette      : TEdibatec_Produit;
Begin

  m_EAEdibatec := GetEAAChiffrer;
  Inherited;

  if SurTerrasse then
        EmplacementReseau := cst_Horizontal
  else
        EmplacementReseau := cst_Vertical;

  If IsNotNil(m_EAEdibatec) Then AddChiffrage(m_EAEdibatec, 1, EmplacementReseau, cst_Chiff_BC)
                                Else If Logement <> nil Then
                                        AddChiffrageErreur('EA', Logement.Reference{$IfDeF VIM_OPTAIR}, inttostr(1) + ' unit�'{$EndIf});
//  If (TypePiece = c_Cuisine) And IsNotNil(Logement) Then Logement.Chiffrage_EntreesAir;
  If ((TypePiece = c_Cuisine) or (Etude.Batiment.TypeBat = c_BatimentTertiaire)) And IsNotNil(Logement) Then Logement.Chiffrage_EntreesAir(Caisson);
  CalculerQuantitatif_CoupeFeu;
  CalculerQuantitatifRegDeb;

if not(Etude.Batiment.IsFabricantVIM) then
        if isAutoReglable then
                Begin
                m_Manchette :=  TEdibatec_ACCRESO(BaseEdibatec.Produit(BaseEdibatec.Assistant.Produit('c_Produit_Manchon_PourBoucheAutoReglable')));
                AddChiffrage(m_Manchette, 1, EmplacementReseau, cst_Chiff_AC);
                End;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_EAir.CalculerQuantitatif_CoupeFeu;
Var
  m_CoupeFeu : TEdibatec_CoupeFeu;
  m_NoProd   : Integer;
  m_Erreur   : Boolean;
Begin
  Inc(TVentil_Caisson(Caisson).NbBouches);
  if Has_PareFlamme = c_Oui then
  Begin
  m_Erreur := True;
          If GammePareFlamme <> nil Then
                Begin
                        For m_NoProd := 0 To GammePareFlamme.Produits.Count - 1 Do
                                Begin
                                m_CoupeFeu := TEdibatec_CoupeFeu(GammePareFlamme.Produits.Produit(m_NoProd));
                                        If (m_CoupeFeu.Diametre = Diametre) Then
                                                Begin // Diametre OK
                                                        If SurTerrasse Then
                                                                AddChiffrage(m_CoupeFeu, 1, cst_Horizontal, cst_Chiff_AC)
                                                        Else
                                                                AddChiffrage(m_CoupeFeu, 1, cst_Vertical, cst_Chiff_AC);
                                                m_Erreur := False;
                                                Break;
                                                End;
                                End;
                End;
          If m_Erreur Then
                AddChiffrageErreur('Clapet coupe-feu', IntToStr(Diametre) + ' mm '{$IfDeF VIM_OPTAIR}, inttostr(1) + ' unit�'{$EndIf});
  End;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_EAir.MajAssociation;
Var
  m_NoAsso : Integer;
Begin
  If FProduit <> nil Then For m_NoAsso := 0 To Associations.Count - 1 Do If TEdibatec_Association(Associations.Produit(m_NoAsso)).Defaut Then
    TEdibatec_Association(Associations.Produit(m_NoAsso)).Qte := TEdibatec_Association(FProduit.Associations.Produit(m_NoAsso)).Qte;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_EAir.SetAssociationDefaut(_Association: TEdibatec_Association; _QteDefaut: Real);
Begin
  If FProduit <> Nil Then TEdibatec_Association(FProduit.Associations.Produit(_Association.CodeProduit)).Qte := _QteDefaut;
End;
{ ************************************************************************************************************************************************** }
Function TVentil_EAir.GetEAAChiffrer: TEdibatec_EA;
Var
  m_EAEdibatec : TEdibatec_EA;
  m_No             : Integer;
Begin
{$IfDef AERAU_CLIMAWIN}
Result := Nil;
Exit;
{$EndIf}
  m_EAEdibatec := nil;
  Case TypePiece Of
{$IfDef OLD_ATVENTIL}
    c_Sejour..c_NbPiecesSeches: If IsNotNil(Logement) Then Begin
      If (Logement.ChaudiereCuisine = c_Non) And (GetSysteme.Gaz) Then m_EAEdibatec := TEdibatec_EA(BaseEdibatec.Produit(SystemesVMC.SystemeEquivalentNonGaz(GetSysteme).EntreeAir[Logement.TypeLogement, TypePiece, Logement.T3Optimise = c_Oui][0].Edibatec))
                                                                  Else m_EAEdibatec := TEdibatec_EA(BaseEdibatec.Produit(GetSysteme.EntreeAir[Logement.TypeLogement, TypePiece, Logement.T3Optimise = c_Oui][0].Edibatec));
    End;
    c_Cellier : If IsNotNIl(Logement) Then m_EAEdibatec := TEdibatec_EA(BaseEdibatec.Produit(GetSysteme.EntreeAirSiCellier[Logement.TypeLogement, TypePiece, Logement.Optimise][0].Edibatec));
{$Else}
    c_Sejour..c_NbPiecesSeches: If IsNotNil(Logement) Then Begin
      If (Logement.ChaudiereCuisine = c_Non) And (GetSysteme.Gaz) Then m_EAEdibatec := TEdibatec_EA(BaseEdibatec.Produit(SystemesVMC.SystemeEquivalentNonGaz(GetSysteme).ConfigsLogements.GetConfigLogement(Logement.TypeLogement, Logement.IdxConfig).SolutionsEAs[TypePiece][Logement.IdxSolutionEA[TypePiece]][0].Edibatec))
                                                                  Else m_EAEdibatec := TEdibatec_EA(BaseEdibatec.Produit(GetSysteme.ConfigsLogements.GetConfigLogement(Logement.TypeLogement, Logement.IdxConfig).SolutionsEAs[TypePiece][Logement.IdxSolutionEA[TypePiece]][0].Edibatec));
    End;
{$EndIf}
  {$IfNDef AERAU_CLIMAWIN}
    c_ChoixEA : m_EAEdibatec := EASelection;
  {$EndIf}    
    Else m_EAEdibatec := nil;
  End;
  If IsNotNil(m_EAEdibatec) Then Begin
    If (FProduit <> m_EAEdibatec) Then Begin
      FProduit := m_EAEdibatec;
      m_EAEdibatec.GetAssociation;
      Associations.InitFrom(m_EAEdibatec.Associations);

        //Chargement asynchrone des quantit�s des associations
        If FQteAssociations <> nil Then
                Begin
                        If (FCodesAssociations <> nil) and (FCodesAssociations.Count > 0) and (Trim(FCodesAssociations[0]) <> '') Then
                                Begin
                                        For m_No := 0 To FCodesAssociations.Count - 1 Do
                                                Begin
                                                        try
                                                                if Associations.Produit(FCodesAssociations[m_No]) <> Nil then
                                                                        begin
                                                                        TEdibatec_Association(Associations.Produit(FCodesAssociations[m_No])).Qte := Str2Float(FQteAssociations[m_No]);
                                                                        end
                                                                else
                                                                        begin
                                                                                if Str2Float(FQteAssociations[m_No]) > 0 then
                                                                                        AssociationsNonTrouvees.Add(Reference + ' : ' + FCodesAssociations[m_No] + ' x' + FQteAssociations[m_No]);
                                                                        end;
                                                        except
                                                        end;
                                                End;
                                End
                        Else
                                Begin
                                        For m_No := 0 To FQteAssociations.Count - 1 Do
                                                Begin
                                                        try
                                                                if Associations.Produit(m_No) <> Nil then
                                                                        TEdibatec_Association(Associations.Produit(m_No)).Qte := Str2Float(FQteAssociations[m_No]);
                                                        except
                                                        end;
                                                End;
                                End;
                FreeAndNil(FQteAssociations);
                FreeAndNil(FCodesAssociations);
                End
        else
                begin
                        if AssociationsNonTrouvees <> Nil then
                                FreeAndNil(AssociationsNonTrouvees);
                end;
    End;
  End Else Begin
    Associations.Clear;
    FProduit := nil;
  End;
  Result := m_EAEdibatec;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_EAir.SetQMaxSaisi(const Value: Integer);
var
        TempValue : Integer;
Begin
  If (TypePiece = c_ChoixEA) And IsNotNIl(EASelection) Then
        TempValue := max(EASelection.QMini, min(Value, EASelection.QMaxi))
  else
  if GetEAAChiffrer <> nil then
        TempValue := max(GetEAAChiffrer.QMini, min(Value, GetEAAChiffrer.QMaxi))
  else
        TempValue := max(Value, QMinSaisi);

        if Etude.Batiment.IsFabricantVIM then
        begin
        if GetRegulateurParent <> nil then
                Begin
                                if GetRegulateurParent.QMaxiPossible + FQMaxSaisi <= 0 then
                                Begin
                                TempValue := 0;
                                QMinSaisi := 0;
                                End
                        else
                                TempValue := min(TempValue, GetRegulateurParent.QMaxiPossible + FQMaxSaisi);
                End;

        if RegulateurDebitChiff <> nil then
                Begin
                                if RegulateurDebitChiff.QMaxi <= 0 then
                                Begin
                                TempValue := 0;
                                QMinSaisi := 0;
                                End
                        else
                                TempValue := min(TempValue, RegulateurDebitChiff.QMaxi);
                End;

        if isDebitReglable then
                FQMinSaisi := TempValue;
        end;

FQMaxSaisi := TempValue;
ManageEtiquetteVisible;

End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_EAir.SetQMinSaisi(const Value: Integer);
var
        TempValue : Integer;
Begin

TempValue := min(Value, QMaxSaisi);

FQMinSaisi := TempValue;
ManageEtiquetteVisible;
End;
{ ************************************************************************************************************************************************** }
Function TVentil_EAir.ImageAssociee: String;
Begin
  Result := 'ENTREEAIR.jpg';
End;
{ ************************************************************************************************************************************************** }
Function TVentil_EAir.IsEtiquetteVisible : boolean;
Begin
Result := Options.AffEtiq_EA;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_EAir.SetTypePiece(const Value: Integer);
Begin
  FTypePiece := Value;
ManageEtiquetteVisible;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_EAir.IAero_Fredo_BoucheBase_GetDimensionsLocal(var Longueur, Largeur, Hauteur: single; var Ouvert: boolean); // Tout en m�tres
Begin
  Ouvert := True;
{$IfDef OLD_ATVENTIL}
  If ((TypePiece < c_Cellier) or (Etude.Batiment.IsFabricantMVN and (TypePiece = c_Cellier)) )And IsNotNil(Logement) And (Logement.TypeLogement < c_TypeAutre) Then
{$Else}
  If (TypePiece <= c_NbPiecesHumide) And IsNotNil(Logement) And (Logement.TypeLogement < c_TypeAutre) Then
{$EndIf}
  Begin
    If Logement.InfosPieces[TypePiece, 2] = 0 Then Ouvert := True
                                              Else Ouvert := Logement.CuisineFermee = c_NON;
    If Not Ouvert Then Begin
      Longueur := sqrt(Logement.InfosPieces[TypePiece, 2]);
      Hauteur  := Logement.InfosPieces[TypePiece, 1];
      Largeur  := sqrt(Logement.InfosPieces[TypePiece, 2]);
    End;
  End;
End;
{ ************************************************************************************************************************************************** }
Function TVentil_EAir.IAero_Fredo_BoucheBase_GetTrLocal : single;
Begin
  Result := 0;
  If IsNotNil(Logement) Then Result := Logement.Tr_Cuisine;
End;
{ ************************************************************************************************************************************************** }
Function TVentil_EAir.IAero_Fredo_BoucheBase_GetPression : single;
Var
  m_EA: TEdibatec_EA;
Begin
  Result := 160;
  m_EA := EAAChiffrer;
  If m_EA <> nil Then Result := m_EA.PMaxi;
End;
{ ************************************************************************************************************************************************** }
Function TVentil_EAir.IAero_Fredo_BoucheBase_GetDne : Widestring;
Var
  m_EA : TEdibatec_EA;
Begin
  Result := '';
  m_EA := EAAChiffrer;
  If m_EA <> nil Then Result := m_EA.Bandes_Dne;
End;
{ ************************************************************************************************************************************************** }
Function TVentil_EAir.IAero_Fredo_BoucheBase_GetLw : Widestring;
Var
  m_EA : TEdibatec_EA;
Begin
  Result := '';
  m_EA := EAAChiffrer;
  If m_EA <> nil Then Result := m_EA.Bandes_Lw;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_EAir.CalculESA;
Var
  m_Surf : Real;
  m_Diam : Integer;
Begin
  ESA := - 1;
  If (TypePiece < c_WC) And (Logement <> nil) And (Logement.InfosPieces[TypePiece, 2] > 0) Then Begin
    m_Surf := Logement.InfosPieces[TypePiece, 2];
    if Collecteur <> nil then
            m_Diam := Collecteur.Diametre
    else
            m_Diam := Diametre;    
    If Logement.CuisineFermee = c_OUI Then Begin
      If m_Surf <= 10 Then Begin
        If m_Diam < 315 Then ESA := c_ESA5Plus
                        Else ESA := c_ESA5;
      End Else Begin
        If m_Diam < 315 Then ESA := c_ESA4Plus
                        Else ESA := c_ESA4;
      End;
    End Else Begin
      If m_Surf < 20 Then Begin
        If m_Diam < 315 Then ESA := c_ESA6Plus
                        Else ESA := c_ESA6;
      End Else If (m_Surf < 30) Then Begin
        If m_Diam < 315 Then ESA := c_ESA5Plus
                        Else ESA := c_ESA5;
      End Else Begin
        If m_Diam < 315 Then ESA := c_ESA4Plus
                        Else ESA := c_ESA4;
      End;
    End;
  End;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_EAir.CalculeDebitsParLogementOld;
Const
  Cst_CdHygroA : Array[1..7] Of Real = (1.16, 1.17, 1.16, 1.15, 1.12, 1.13, 1.14);
Var
  m_Sys: TVentil_Systeme;
{$IfDef OLD_ATVENTIL}
{$Else}
  cpt : Integer;
{$EndIf}
Begin
  Inherited;
  m_Sys := Systeme;

  If (m_Sys <> nil) and ((Etude.Batiment.IsFabricantVIM and Not(isEACoupeFeu)) or not(Etude.Batiment.IsFabricantVIM)) Then
        Begin
            If m_Sys.Hygro Then
                Begin
{$IfDef OLD_ATVENTIL}
                TVentil_Caisson(Caisson).QminH_RT := TVentil_Caisson(Caisson).QminH_RT + DebitMini;
{$Else}
                  for cpt := Low(TVentil_Caisson(Caisson).QminH_RT) to High(TVentil_Caisson(Caisson).QminH_RT) do
                    TVentil_Caisson(Caisson).QminH_RT[cpt] := TVentil_Caisson(Caisson).QminH_RT[cpt] + DebitMini;
{$EndIf}
                TVentil_Caisson(Caisson).QmaxH_RT := TVentil_Caisson(Caisson).QmaxH_RT + DebitMaxi;
                TVentil_Caisson(Caisson).LogementsH := True;
                    If m_Sys.HygroB Then
                        Begin
                                If (Logement.TypeLogement <= c_TypeT7) Then
                                        TVentil_Caisson(Caisson).Rfh := Cst_CdHygroA[Logement.TypeLogement];
                        TVentil_Caisson(Caisson).LogementsHB := True
                        End
                    Else
                        Begin
                                If (Logement.TypeLogement <= c_TypeT7) Then
                                        TVentil_Caisson(Caisson).Rfh := Cst_CdHygroA[Logement.TypeLogement];
                        TVentil_Caisson(Caisson).LogementsHA := True;
                        End;
                End
            Else
            If m_Sys.HygroGaz Then
                Begin
{$IfDef OLD_ATVENTIL}
                TVentil_Caisson(Caisson).QminGH_RT := TVentil_Caisson(Caisson).QminGH_RT + DebitMini;
{$Else}
                  for cpt := Low(TVentil_Caisson(Caisson).QminGH_RT) to High(TVentil_Caisson(Caisson).QminGH_RT) do
                    TVentil_Caisson(Caisson).QminGH_RT[cpt] := TVentil_Caisson(Caisson).QminGH_RT[cpt] + DebitMini;
{$EndIf}
                TVentil_Caisson(Caisson).QmaxGH_RT := TVentil_Caisson(Caisson).QmaxGH_RT + DebitMaxi;
                TVentil_Caisson(Caisson).LogementsGH := True;
                End
            Else
                Begin
{$IfDef OLD_ATVENTIL}
                TVentil_Caisson(Caisson).QminA_RT := TVentil_Caisson(Caisson).QminA_RT + DebitMini;
{$Else}
                  for cpt := Low(TVentil_Caisson(Caisson).QminA_RT) to High(TVentil_Caisson(Caisson).QminA_RT) do
                    TVentil_Caisson(Caisson).QminA_RT[cpt] := TVentil_Caisson(Caisson).QminA_RT[cpt] + DebitMini;
{$EndIf}
                TVentil_Caisson(Caisson).QmaxA_RT := TVentil_Caisson(Caisson).QmaxA_RT + DebitMaxi;
                TVentil_Caisson(Caisson).LogementsA := True;
                      If m_Sys.Gaz Then
                        TVentil_Caisson(Caisson).LogementsG := True;
                End;
        End
  Else
        Begin
        TVentil_Caisson(Caisson).Qautre_RT := TVentil_Caisson(Caisson).Qautre_RT + DebitMaxi;
        TVentil_Caisson(Caisson).LogementsAutre := True;
        End;
End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_EAir.CalculeDebitsParLogement;
Var
  m_Sys: TVentil_Systeme;
{$IfDef OLD_ATVENTIL}
{$Else}
  cpt : Integer;
{$EndIf}
Begin
  Inherited;
  m_Sys := Systeme;

  If (m_Sys <> nil) and ((Etude.Batiment.IsFabricantVIM and Not(isEACoupeFeu)) or not(Etude.Batiment.IsFabricantVIM)) Then
        Begin
            If m_Sys.HygroGaz Then
                Begin
                        If (TypePiece = c_Cuisine) then
                                Begin
                                TVentil_Caisson(Caisson).DUGDGH := TVentil_Caisson(Caisson).DUGDGH + DUGD;
                                End;
{$IfDef OLD_ATVENTIL}
                TVentil_Caisson(Caisson).QminGH_RT := TVentil_Caisson(Caisson).QminGH_RT + DebitMiniRT;
{$Else}
                  for cpt := Low(TVentil_Caisson(Caisson).QminGH_RT) to High(TVentil_Caisson(Caisson).QminGH_RT) do
                    TVentil_Caisson(Caisson).QminGH_RT[cpt] := TVentil_Caisson(Caisson).QminGH_RT[cpt] + DebitMiniRT[cpt];
{$EndIf}
                TVentil_Caisson(Caisson).QminGH_Calc := TVentil_Caisson(Caisson).QminGH_Calc + DebitMini;
                TVentil_Caisson(Caisson).QmaxGH_RT := TVentil_Caisson(Caisson).QmaxGH_RT + DebitMaxiRT;
                TVentil_Caisson(Caisson).QmaxGH_Calc := TVentil_Caisson(Caisson).QmaxGH_Calc + DebitMaxi;
                Inc(TVentil_Caisson(Caisson).NbBouchesGH);
                TVentil_Caisson(Caisson).LogementsGH := True;
                End
            Else
            If m_Sys.Hygro Then
                Begin
                        If (TypePiece = c_Cuisine) then
                                Begin
                                TVentil_Caisson(Caisson).DUGDH := TVentil_Caisson(Caisson).DUGDH + DUGD;
                                End;
{$IfDef OLD_ATVENTIL}
                TVentil_Caisson(Caisson).QminH_RT := TVentil_Caisson(Caisson).QminH_RT + DebitMiniRT;
{$Else}
                  for cpt := Low(TVentil_Caisson(Caisson).QminH_RT) to High(TVentil_Caisson(Caisson).QminH_RT) do
                    TVentil_Caisson(Caisson).QminH_RT[cpt] := TVentil_Caisson(Caisson).QminH_RT[cpt] + DebitMiniRT[cpt];
{$EndIf}
                TVentil_Caisson(Caisson).QminH_Calc := TVentil_Caisson(Caisson).QminH_Calc + DebitMini;
                TVentil_Caisson(Caisson).QmaxH_RT := TVentil_Caisson(Caisson).QmaxH_RT + DebitMaxiRT;
                TVentil_Caisson(Caisson).QmaxH_Calc := TVentil_Caisson(Caisson).QmaxH_Calc + DebitMaxi;
                Inc(TVentil_Caisson(Caisson).NbBouchesH);
                TVentil_Caisson(Caisson).LogementsH := True;
                    If m_Sys.HygroB Then
                        TVentil_Caisson(Caisson).LogementsHB := True
                    Else
                        TVentil_Caisson(Caisson).LogementsHA := True;
                End
            Else
                Begin
{$IfDef OLD_ATVENTIL}
                TVentil_Caisson(Caisson).QminA_RT := TVentil_Caisson(Caisson).QminA_RT + DebitMiniRT;
{$Else}
                  for cpt := Low(TVentil_Caisson(Caisson).QminA_RT) to High(TVentil_Caisson(Caisson).QminA_RT) do
                    TVentil_Caisson(Caisson).QminA_RT[cpt] := TVentil_Caisson(Caisson).QminA_RT[cpt] + DebitMiniRT[cpt];
{$EndIf}
                TVentil_Caisson(Caisson).QminA_Calc := TVentil_Caisson(Caisson).QminA_Calc + DebitMini;
                        If (TypePiece = c_Cuisine) then
                                begin
                                TVentil_Caisson(Caisson).DUGDA := TVentil_Caisson(Caisson).DUGDA + DUGD;
                                end;
                TVentil_Caisson(Caisson).QmaxA_RT := TVentil_Caisson(Caisson).QmaxA_RT + DebitMaxiRT;
                TVentil_Caisson(Caisson).QmaxA_Calc := TVentil_Caisson(Caisson).QmaxA_Calc + DebitMaxi;
                Inc(TVentil_Caisson(Caisson).NbBouchesA);
                TVentil_Caisson(Caisson).LogementsA := True;
                      If m_Sys.Gaz Then
                        TVentil_Caisson(Caisson).LogementsG := True;

                      If m_Sys.Gaz Then
                        Begin
{$IfDef OLD_ATVENTIL}
                        TVentil_Caisson(Caisson).QminGaz_RT := TVentil_Caisson(Caisson).QminGaz_RT + DebitMiniRT;
{$Else}
                          for cpt := Low(TVentil_Caisson(Caisson).QminGaz_RT) to High(TVentil_Caisson(Caisson).QminGaz_RT) do
                            TVentil_Caisson(Caisson).QminGaz_RT[cpt] := TVentil_Caisson(Caisson).QminGaz_RT[cpt] + DebitMiniRT[cpt];
{$EndIf}
                        TVentil_Caisson(Caisson).QminGaz_Calc := TVentil_Caisson(Caisson).QminGaz_Calc + DebitMini;
                        TVentil_Caisson(Caisson).QmaxGaz_RT := TVentil_Caisson(Caisson).QmaxGaz_RT + DebitMaxiRT;
                        TVentil_Caisson(Caisson).QmaxGaz_Calc := TVentil_Caisson(Caisson).QmaxGaz_Calc + DebitMaxi;
                        Inc(TVentil_Caisson(Caisson).NbBouchesGaz);
                        End
                      else
                        Begin
{$IfDef OLD_ATVENTIL}
                        TVentil_Caisson(Caisson).QminAuto_RT := TVentil_Caisson(Caisson).QminAuto_RT + DebitMiniRT;
{$Else}
                          for cpt := Low(TVentil_Caisson(Caisson).QminAuto_RT) to High(TVentil_Caisson(Caisson).QminAuto_RT) do
                            TVentil_Caisson(Caisson).QminAuto_RT[cpt] := TVentil_Caisson(Caisson).QminAuto_RT[cpt] + DebitMiniRT[cpt];
{$EndIf}
                        TVentil_Caisson(Caisson).QminAuto_Calc := TVentil_Caisson(Caisson).QminAuto_Calc + DebitMini;
                        TVentil_Caisson(Caisson).QmaxAuto_RT := TVentil_Caisson(Caisson).QmaxAuto_RT + DebitMaxiRT;
                        TVentil_Caisson(Caisson).QmaxAuto_Calc := TVentil_Caisson(Caisson).QmaxAuto_Calc + DebitMaxi;
                        Inc(TVentil_Caisson(Caisson).NbBouchesAuto);
                        End;
                End;
        End
  Else
        Begin
                if (m_Sys <> nil) and m_Sys.Hygro then
                        Begin
                                If (TypePiece = c_Cuisine) then
                                        Begin
                                        End;
{$IfDef OLD_ATVENTIL}
                        TVentil_Caisson(Caisson).QminAutre_RT := TVentil_Caisson(Caisson).QminAutre_RT + DebitMiniRT;
{$Else}
                          for cpt := Low(TVentil_Caisson(Caisson).QminAutre_RT) to High(TVentil_Caisson(Caisson).QminAutre_RT) do
                            TVentil_Caisson(Caisson).QminAutre_RT[cpt] := TVentil_Caisson(Caisson).QminAutre_RT[cpt] + DebitMiniRT[cpt];
{$EndIf}
                        TVentil_Caisson(Caisson).QminAutre_Calc := TVentil_Caisson(Caisson).QminAutre_Calc + DebitMini;
                        End
                Else
                        Begin
{$IfDef OLD_ATVENTIL}
                        TVentil_Caisson(Caisson).QminAutre_RT := TVentil_Caisson(Caisson).QminAutre_RT + DebitMiniRT;
{$Else}
                          for cpt := Low(TVentil_Caisson(Caisson).QminAutre_RT) to High(TVentil_Caisson(Caisson).QminAutre_RT) do
                            TVentil_Caisson(Caisson).QminAutre_RT[cpt] := TVentil_Caisson(Caisson).QminAutre_RT[cpt] + DebitMiniRT[cpt];
{$EndIf}
                        TVentil_Caisson(Caisson).QminAutre_Calc := TVentil_Caisson(Caisson).QminAutre_Calc + DebitMini;
                        End;

                If (TypePiece = c_Cuisine) then
                        begin
                        TVentil_Caisson(Caisson).DUGDAutre := TVentil_Caisson(Caisson).DUGDAutre + DUGD;
                        end;

        TVentil_Caisson(Caisson).Qautre_RT := TVentil_Caisson(Caisson).Qautre_RT + DebitMaxiRT;
        TVentil_Caisson(Caisson).Qautre_Calc := TVentil_Caisson(Caisson).Qautre_Calc + DebitMaxi;
        Inc(TVentil_Caisson(Caisson).NbBouchesAutre);
        TVentil_Caisson(Caisson).LogementsAutre := True;
        End;


End;
{ ************************************************************************************************************************************************** }
Procedure TVentil_EAir.CalculeDebitsMax;
Begin
  Inherited;
  If not((TypePiece = c_ChoixEA) And IsNotNIl(EASelection)) Then
        QMaxSaisi := QMaxSaisi;
End;
{ ************************************************************************************************************************************************** }
Function TVentil_EAir.GetQmaxPrisEnCompte : integer;
Begin

  If (TypePiece = c_ChoixEA) And IsNotNIl(EASelection) Then
        Begin
        if IsEASelectionDebitNonFixe then
                result := QMaxSaisi
        else
                result := EASelection.QMaxi;
        End
  Else
        result := QMaxSaisi;
End;
{ ******************************************************************* \TVentil_EAir ***************************************************************** }




End.

