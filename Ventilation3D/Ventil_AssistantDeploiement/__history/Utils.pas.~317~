unit Utils;

Interface
Uses
  ShellApi, SysUtils, Types, classes;


{ Op�rations de fichiers }
Procedure Util_CreateFile(_Fichier: String);
Procedure Util_CreateDirectory(_Repertoire: String);
Function Util_RenameFile(_Source, _Destination: String): Boolean;
Function Util_RenameDirectory(_Source, _Destination: String): Boolean;
Procedure Util_CopyFile(_Source, _Destination: String; _ForceCreateDirectories : Boolean = False);
Procedure Util_DeleteFile(_Source: String);
Procedure Util_DeleteFullDirectory(_Source: String);
Function Util_RepertoireSurDisque(_Repertoire: String): Boolean;
Function Util_FichierSurDisque(_Fichier: String): Boolean;
Function Util_TempInternalDir: String;
Function Util_BackUpDir(_SourceDirectory : String): String;
procedure Util_ScruterRepertoire(aRep : String; Var aFileList:TStringList; Var aDirectoryList:TStringList; Recurse: Boolean = True);
function Util_ApplicationVersion(NomSoft:String=''): String;
function Util_FileDate(NomSoft : String): String;
procedure Util_ForceCreateDirectories(_Repertoire : String);
function Util_ExecuteProcess(_ProcessName: string; _Parametres: string = ''; _Invisible: boolean = True; _AttendreFin: boolean = False; _ExecuteInProcessDirectory : Boolean = True; _ExecuteInDirectory : String = ''): Cardinal;
Function Util_GetDateHeureForFileName : String;
Function Util_IsCheminReseau(_Path : String): Boolean;

Implementation
Uses
  IOUtils,
  CAD_ViloUtils,
  Dialogs,
  Windows,
  Forms;

{ ************************************************************************************************************************************************** }
Procedure Util_CreateFile(_Fichier: String);
Begin
  TFile.Create(_Fichier).Destroy;
End;
{ ************************************************************************************************************************************************** }
Procedure Util_CreateDirectory(_Repertoire: String);
Begin
  TDirectory.CreateDirectory(_Repertoire);
End;
{ ************************************************************************************************************************************************** }
Function Util_RenameFile(_Source, _Destination: String): Boolean;
//Var
//  fos : TSHFileOpStruct;
Begin
  Result := RenameFile(_Source, _Destination);
{
  FillChar(fos, SizeOf(fos), 0);
  With fos Do Begin
    wFunc := FO_RENAME;
    pFrom := PChar(_Source + #0);
    pTO   := PChar(_Destination + #0);
    fFlags:= FO_RENAME Or FOF_SILENT;
  End;
  Result := (0 = ShFileOperation(fos));
}
End;
{ ************************************************************************************************************************************************** }
Function Util_RenameDirectory(_Source, _Destination: String): Boolean;
begin
  Result := RenameFile(_Source, _Destination);
end;
{ ************************************************************************************************************************************************** }
Procedure Util_DeleteFile(_Source: String);
Begin
  if Util_FichierSurDisque(_Source) then
    TFile.Delete(_Source);
End;
{ ************************************************************************************************************************************************** }
Procedure Util_DeleteFullDirectory(_Source: String);
begin
  if Util_RepertoireSurDisque(_Source) then
    TDirectory.Delete(_Source, True);
end;
{ ************************************************************************************************************************************************** }
Procedure Util_CopyFile(_Source, _Destination: String; _ForceCreateDirectories : Boolean = False);
Begin
  if not(Util_RepertoireSurDisque(ExtractFilePath(_Destination))) and _ForceCreateDirectories then
    Util_ForceCreateDirectories(ExtractFilePath(_Destination));
  If Util_RepertoireSurDisque(ExtractFilePath(_Destination)) Then
    Begin
    TFile.Copy(_Source, _Destination + ExtractFileName(_Source), True);
    End;
End;
{ ************************************************************************************************************************************************** }
Function Util_RepertoireSurDisque(_Repertoire: String): Boolean;
Begin
  Result := TDirectory.Exists(_Repertoire);
End;
{ ************************************************************************************************************************************************** }
Function Util_FichierSurDisque(_Fichier: String): Boolean;
Begin
  Result := TFile.Exists(_Fichier);
End;
{ ************************************************************************************************************************************************* }
Function Util_TempInternalDir: String;
Var
  m_Str : String;
Begin
  m_Str := ExtractFilePath(Application.ExeName);
  m_Str := m_Str + 'Temp';
  If Not Util_RepertoireSurDisque(m_Str) Then Util_CreateDirectory(m_Str);
  Result := m_Str + '\';
End;
{ ************************************************************************************************************************************************** }
Function Util_BackUpDir(_SourceDirectory : String): String;
Var
  m_Str : String;
Begin
  m_Str := _SourceDirectory;
  m_Str := m_Str + 'BackUp';
  If Not Util_RepertoireSurDisque(m_Str) Then Util_CreateDirectory(m_Str);
  Result := m_Str + '\';
End;
{ ************************************************************************************************************************************************** }
procedure Util_ScruterRepertoire(aRep : String; Var aFileList:TStringList; Var aDirectoryList:TStringList; Recurse: Boolean = True);
var
  cpt : Integer;
  Retour : TStringDynArray;
  TypeRecherche : TSearchOption;
begin

  if Not(Util_RepertoireSurDisque(aRep)) then
    Exit;

  if Recurse then
    TypeRecherche := TSearchOption.soAllDirectories
  else
    TypeRecherche := TSearchOption.soTopDirectoryOnly;

  Retour := TDirectory.GetFiles(aRep, '*', TypeRecherche);
    for cpt := Low(Retour) to High(Retour) do
      aFileList.Add(Retour[cpt]);
  Retour := TDirectory.GetDirectories(aRep, '*', TypeRecherche);
    for cpt := Low(Retour) to High(Retour) do
      aDirectoryList.Add(Retour[cpt]);
end;
{ ************************************************************************************************************************************************** }
function Util_ApplicationVersion(NomSoft:String=''): String;
var
  VerInfoSize, VerValueSize, Dummy: DWord;
  VerInfo: Pointer;
  VerValue: PVSFixedFileInfo;
begin
  //Par d�faut on prend l'exe sinon on peut prendre le fichier pass� en param�tre, ex une dll
  If NomSoft='' Then NomSoft:=ParamStr(0);

  VerInfoSize := GetFileVersionInfoSize(PChar(NomSoft), Dummy);
  {Deux solutions : }
  if VerInfoSize <> 0 then
  {- Les info de version sont inclues }
  begin
    {On alloue de la m�moire pour un pointeur sur les info de version : }
    GetMem(VerInfo, VerInfoSize);
    {On r�cup�re ces informations : }
    GetFileVersionInfo(PChar(NomSoft), 0, VerInfoSize, VerInfo);
    VerQueryValue(VerInfo, '\', Pointer(VerValue), VerValueSize);
    {On traite les informations ainsi r�cup�r�es : }
    with VerValue^ do
    begin
      //Result := '';
      Result := IntTostr(dwFileVersionMS shr 16);
      Result := Result + '.' + IntTostr(dwFileVersionMS and $FFFF);
      Result := Result + '.' + IntTostr(dwFileVersionLS shr 16);
      Result := Result + '.' + IntTostr(dwFileVersionLS and $FFFF);
    end;
    {On lib�re la place pr�c�demment allou�e : }
    FreeMem(VerInfo, VerInfoSize);
  end else
    {- Les infos de version ne sont pas inclues }
    {On d�clenche une exception dans le programme : }
    //raise EAccessViolation.Create('Les informations de version de sont pas inclues');
end;
{ ************************************************************************************************************************************************** }
function Util_FileDate(NomSoft : String): String;
var
  tempDate : TDateTime;
begin
FileAge(NomSoft, tempDate);
Result := formatdatetime('yyyy_mm_dd_hh_nn_ss', tempDate);
end;
{ ************************************************************************************************************************************************** }
procedure Util_ForceCreateDirectories(_Repertoire : String);
{
Var
  cpt : Integer;
  stl : TStringList;
  tempDirectory : String;
  CheminReseau : String;
}
Begin

  TDirectory.CreateDirectory(_Repertoire);
{

  if Util_IsCheminReseau(_Repertoire) then
    CheminReseau := '\\'
  else
    CheminReseau := '';
_Repertoire := StrReplace(_Repertoire, ' ', '%20', false, True);
stl := TStringList.Create;
stl.Delimiter := '\';
stl.DelimitedText := _Repertoire;

if stl.Count = 0 then
  exit;
tempDirectory := '';
  for cpt := 0 to stl.Count - 1 do
    if Trim(stl[cpt]) <> '' then
      begin
      stl[cpt] := StrReplace(stl[cpt], '%20', ' ', false, True);
        if tempDirectory = '' then
          tempDirectory := CheminReseau + stl[cpt]
        else
          tempDirectory := tempDirectory + '\' + stl[cpt];

          If Not(Util_RepertoireSurDisque(tempDirectory)) Then
            Util_CreateDirectory(tempDirectory);
      end;

stl.Destroy;
}
end;
{ ************************************************************************************************************************************************** }
function Util_ExecuteProcess(_ProcessName: string; _Parametres: string = ''; _Invisible: boolean = True; _AttendreFin: boolean = False; _ExecuteInProcessDirectory : Boolean = True; _ExecuteInDirectory : String = ''): Cardinal;
var
  StartInfo: TStartupInfo;
  ProcInfo: TProcessInformation;
  ProcessPWC : PWideChar;
  DirectoryPWC : PWideChar;
begin
  //Simple wrapper for the CreateProcess command
  //returns the process id of the started process.
  FillChar(StartInfo,SizeOf(TStartupInfo),#0);
  FillChar(ProcInfo,SizeOf(TProcessInformation),#0);
  StartInfo.cb := SizeOf(TStartupInfo);

  if _Invisible then begin
    StartInfo.dwFlags := STARTF_USESHOWWINDOW;
    StartInfo.wShowWindow := SW_HIDE;
  end;

  ProcessPWC := PWideChar(_ProcessName + ' ' + _Parametres);
    if _ExecuteInProcessDirectory then
      DirectoryPWC := PWideChar(ExtractFilePath(_ProcessName))
    else
      DirectoryPWC := PWideChar(_ExecuteInDirectory);

  CreateProcess(nil,ProcessPWC,nil,nil,False,
    CREATE_NEW_PROCESS_GROUP + NORMAL_PRIORITY_CLASS,nil,DirectoryPWC,StartInfo,
    ProcInfo);

  Result := ProcInfo.dwProcessId;

  if _AttendreFin then begin
    WaitForSingleObject(ProcInfo.hProcess,Infinite);
    GetExitCodeProcess(ProcInfo.hProcess, Result);
  end;

  //close process & thread handles
  CloseHandle(ProcInfo.hProcess);
  CloseHandle(ProcInfo.hThread);
end;
{ ************************************************************************************************************************************************** }
Function Util_GetDateHeureForFileName : String;
begin
  Result := formatdatetime('yyyy_mm_dd_hh_nn_ss', now);
end;
{ ************************************************************************************************************************************************** }
Function Util_IsCheminReseau(_Path : String): Boolean;
begin
  Result := (Length(_Path) >= 2) and (copy(_Path,1,2) = '\\');
end;
{ ************************************************************************************************************************************************** }
End.
