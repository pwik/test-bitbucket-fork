unit Main;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinBlueprint,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic,
  dxSkinSharp, dxSkinSharpPlus, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinTheAsphaltWorld, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinVS2010, dxSkinWhiteprint, dxSkinXmas2008Blue,
  dxSkinscxPCPainter, dxBarBuiltInMenu, cxPC, dxDockControl, dxDockPanel,
  Vcl.ExtCtrls, cxContainer, cxEdit, Vcl.Menus, Vcl.StdCtrls, cxButtons,
  Vcl.OleCtrls, SHDocVw, cxGroupBox, cxCheckGroup, cxCustomData, cxStyles, cxTL,
  cxLabel, cxCheckBox, cxCalendar, cxTLdxBarBuiltInMenu, cxInplaceContainer,
  cxVGrid,
  Config_XML,
  Logiciels,
  PackagesCommuns,
  PackagesLogiciels,
  Serveurs,
  uCEFApplication,
  uCEFTypes,
  uCEFChromium,
  uCEFWindowParent,
  cxTextEdit,cxButtonEdit, cxSpinEdit, cxFilter, cxData, cxDataStorage,
  cxNavigator, Data.DB, cxDBData, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  cxRadioGroup, cxLocalization, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxSkinsForm;

type

  TFormAssistantDeploiement = class(TForm)
    PanelLogiciels: TPanel;
    PanelDetailLogiciel: TPanel;
    PageControlLogiciel: TcxPageControl;
    TabSheetUpdater: TcxTabSheet;
    TabSheetPatchNote: TcxTabSheet;
    PanelAddRemoveLogiciel: TPanel;
    ButtonAddLogiciel: TcxButton;
    ButtonRemoveLogiciel: TcxButton;
    CheckGroupCibleUpdate: TcxCheckGroup;
    TreeListPackagesUpdater: TcxTreeList;
    cxTreeListUpdatePackageReference: TcxTreeListColumn;
    cxTreeListUpdatePackageSelect: TcxTreeListColumn;
    cxTreeListUpdatePackageDate: TcxTreeListColumn;
    ButtonUpdateServeurs: TcxButton;
    TabSheetProprietesLogiciel: TcxTabSheet;
    ButtonGenerateSetup: TcxButton;
    ButtonSaveProperties: TcxButton;
    CheckGroupOptionsLogiciel: TcxCheckGroup;
    VerticalGridProprietesLogiciel: TcxVerticalGrid;
    ButtonSaveConfigXML: TcxButton;
    CheckGroupOptionsSauvegardeInstallation: TcxCheckGroup;
    CheckGroupOptionUtilitaire: TcxCheckGroup;
    ButtonUploadVersionDevSurServeurDownload: TcxButton;
    ButtonUploadLastSetupSurServeurDownload: TcxButton;
    ButtonGetLienPatchNoteClient: TcxButton;
    CheckGroupListePackagesCommuns: TcxCheckGroup;
    ButtonRecupPackageCommun: TcxButton;
    RadioGroupListeLogiciels: TcxRadioGroup;
    cxLocalizer1: TcxLocalizer;
    ButtonCopyLogiciel: TcxButton;
    dxSkinController1: TdxSkinController;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure ButtonAddLogicielClick(Sender: TObject);
    procedure ButtonRemoveLogicielClick(Sender: TObject);
    procedure ButtonSaveConfigXMLClick(Sender: TObject);
    procedure ButtonSavePropertiesClick(Sender: TObject);
    procedure cxTreeListUpdatePackageSelectPropertiesEditValueChanged(
      Sender: TObject);
    procedure ButtonUpdateServeursClick(Sender: TObject);
    procedure cxTreeListUpdatePackageDatePropertiesChange(Sender: TObject);
    procedure ButtonGenerateSetupClick(Sender: TObject);
    procedure ButtonUploadLastSetupSurServeurDownloadClick(Sender: TObject);
    procedure ButtonUploadVersionDevSurServeurDownloadClick(Sender: TObject);
    procedure ButtonGetLienPatchNoteClientClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure InitialisationPreferencesUtilitaire;
    procedure InitialisationEditeurs;
    procedure CheckGroupOptionUtilitairePropertiesEditValueChanged(
      Sender: TObject);
    procedure CheckGroupOptionsSauvegardeInstallationPropertiesEditValueChanged(
      Sender: TObject);
    procedure RadioGroupListeLogicielsPropertiesEditValueChanged(
      Sender: TObject);
    procedure ButtonRecupPackageCommunClick(Sender: TObject);
    procedure ButtonCopyLogicielClick(Sender: TObject);
    procedure PageControlLogicielPageChanging(Sender: TObject;
      NewPage: TcxTabSheet; var AllowChange: Boolean);
  private
    { D�clarations priv�es }
    CEFWindowParent1: TCEFWindowParent;
    ChromiumPatchNote: TChromium;
    CanSaveConfigFile : Boolean;
    ConfigurationXML : IXMLRoot;
    Logiciels : TListeLogiciels;
    Options : TListeOptions;
    Proprietes : TListeProprietes;
    PackagesCommuns : TListePackagesCommuns;
    PackagesLogiciels : TListePackagesLogiciels;
    Serveurs : TServeurs;
    FolderConfigXML : String;
    NameConfigXML   : String;
    procedure EditButtonPathClick(Sender: TObject; AButtonIndex: Integer);
    procedure EditButtonGuidClick(Sender: TObject; AButtonIndex: Integer);
    function GetPreferenceValue(_Value : String) : Boolean;
    function GetPreferenceState(_Value : String) : TcxCheckBoxState;
    procedure CopierTexteDansPressePapier(_Value : String);
    procedure UpdateTabSheets;
    procedure UpdateTabSheetProprietesLogiciel;
    procedure UpdateTabSheetUpdater;
    procedure UpdateTabSheetPatchNote;
    procedure UpdateGroupBoxLogiciels;
    procedure GetTabSheetProprietesLogiciel;
    procedure SaveConfigurationXMLProprietesLogiciel(_Logiciel : TLogiciel);
    Procedure UpdateNomLogicielPanneauSelectionLogiciel(_Logiciel : TLogiciel);

    procedure SaveConfigXML(_Ask : Boolean);
    procedure Initialisation;
  public
    { D�clarations publiques }
  end;

var
  FormAssistantDeploiement: TFormAssistantDeploiement;
Const
  cst_FichierEmplacementConfig = 'EmplacementConfig.txt';

  cst_GenereSetup   = 0;
  cst_GenereUpdate  = 1;

  cst_PackageReference  = 0;
  cst_PackageChecked    = 1;
  cst_PackageDate       = 2;

  cst_AskSaveConfigFileOnExit  = 0;
  cst_AutoSaveConfigFileOnExit = 1;
  cst_BackUpConfigFile         = 2;

  cst_BackUpSetup                         = 0;
  cst_ExtractSetup                        = 1;
  cst_ExtractSilencieux                   = 2;
  cst_GenerateISSForSetupExecution        = 3;
  cst_SaveConfigLogicielBeforeISSForSetup = 4;
  cst_SaveFileConfigFileOnSetupExecution  = 5;

  cst_EditTexte   = 'Texte';
  cst_EditEntier  = 'Entier';
  cst_EditPath    = 'Path';
  cst_EditGuid    = 'Guid';
  cst_EditDate    = 'Date';

implementation

Uses
  XMLDoc, XMLIntf,
  FileCtrl,
  CAD_ViloUtils,
  Bbs_Message,
  Clipbrd,
  Utils;

{$R *.dfm}
//******************************************************************************
procedure TFormAssistantDeploiement.ButtonAddLogicielClick(Sender: TObject);
begin

  if BBS_ShowMessage('Ajouter un logiciel?',  mtConfirmation, [mbYes,mbNo] ,0) = mrYes then
    begin
    Logiciels.GenereNewLogiciel(ConfigurationXML.Softwares.Add, Options, Proprietes, PackagesLogiciels, PackagesCommuns, Serveurs, mclNew);
    CanSaveConfigFile := True;
    Logiciels.IndexLogicielCourant := Logiciels.Count - 1;
    UpdateGroupBoxLogiciels;
    UpdateTabSheets;
    end;
end;
//******************************************************************************
procedure TFormAssistantDeploiement.ButtonCopyLogicielClick(Sender: TObject);
begin
  if Logiciels.LogicielCourant <> Nil then
    if BBS_ShowMessage('Copier le logiciel s�lectionn�?',  mtConfirmation, [mbYes,mbNo] ,0) = mrYes then
      begin
      ConfigurationXML.Softwares.ChildNodes.Add(Logiciels.LogicielCourant.XML.CloneNode(True));
      Logiciels.GenereNewLogiciel(ConfigurationXML.Softwares[ConfigurationXML.Softwares.Count - 1], Options, Proprietes, PackagesLogiciels, PackagesCommuns, Serveurs, mclClone);
      CanSaveConfigFile := True;
      Logiciels.IndexLogicielCourant := Logiciels.Count - 1;
      UpdateGroupBoxLogiciels;
      UpdateTabSheets;
      end;
end;
//******************************************************************************
procedure TFormAssistantDeploiement.ButtonGenerateSetupClick(Sender: TObject);
begin
  if BBS_ShowMessage('G�n�rer le nouveau script d''installation?',  mtConfirmation, [mbYes,mbNo] ,0) = mrYes then
    begin
    Logiciels.LogicielCourant.GenereScriptISS;
    if BBS_ShowMessage('G�n�rer le Setup?',  mtConfirmation, [mbYes,mbNo] ,0) = mrYes then
    Logiciels.LogicielCourant.GenerationSetup;
    end;
end;
//******************************************************************************
procedure TFormAssistantDeploiement.ButtonRecupPackageCommunClick(
  Sender: TObject);
begin
  Logiciels.LogicielCourant.RecupPackagesCommuns;
end;
//******************************************************************************
procedure TFormAssistantDeploiement.ButtonRemoveLogicielClick(Sender: TObject);
begin
  if BBS_ShowMessage('Supprimer le logiciel s�lectionn�?',  mtConfirmation, [mbYes,mbNo] ,0) = mrYes then
    begin
    Logiciels.SupprimeLogiciel(ConfigurationXML.Softwares, RadioGroupListeLogiciels.ItemIndex);
    CanSaveConfigFile := True;
    UpdateGroupBoxLogiciels;
    UpdateTabSheets;
    end;
end;
//******************************************************************************
procedure TFormAssistantDeploiement.CheckGroupOptionsSauvegardeInstallationPropertiesEditValueChanged(
  Sender: TObject);
begin
  if ConfigurationXML = Nil then exit;
  //�a me parait bizarre mais j'ai rien trouv� d'autre...
  if not CheckGroupOptionsSauvegardeInstallation.IsFocused then
    exit;
  ConfigurationXML.Configuration.PreferencesUtilitaire.BackUpSetup := BoolToStr(CheckGroupOptionsSauvegardeInstallation.States[cst_BackUpSetup] = cbsChecked);
  ConfigurationXML.Configuration.PreferencesUtilitaire.ExtractSetup := BoolToStr(CheckGroupOptionsSauvegardeInstallation.States[cst_ExtractSetup] = cbsChecked);
  ConfigurationXML.Configuration.PreferencesUtilitaire.ExtractSilencieux := BoolToStr(CheckGroupOptionsSauvegardeInstallation.States[cst_ExtractSilencieux] = cbsChecked);
  ConfigurationXML.Configuration.PreferencesUtilitaire.GenerateISSForSetupExecution := BoolToStr(CheckGroupOptionsSauvegardeInstallation.States[cst_GenerateISSForSetupExecution] = cbsChecked);
  ConfigurationXML.Configuration.PreferencesUtilitaire.SaveConfigLogicielBeforeISSForSetup := BoolToStr(CheckGroupOptionsSauvegardeInstallation.States[cst_SaveConfigLogicielBeforeISSForSetup] = cbsChecked);
  ConfigurationXML.Configuration.PreferencesUtilitaire.SaveFileConfigFileOnSetupExecution := BoolToStr(CheckGroupOptionsSauvegardeInstallation.States[cst_SaveFileConfigFileOnSetupExecution] = cbsChecked);
  CanSaveConfigFile := True;
end;
//******************************************************************************
procedure TFormAssistantDeploiement.CheckGroupOptionUtilitairePropertiesEditValueChanged(
      Sender: TObject);
begin
  if ConfigurationXML = Nil then exit;
  //�a me parait bizarre mais j'ai rien trouv� d'autre...
  if not CheckGroupOptionUtilitaire.IsFocused then
    exit;
  ConfigurationXML.Configuration.PreferencesUtilitaire.AskSaveConfigFileOnExit := BoolToStr(CheckGroupOptionUtilitaire.States[cst_AskSaveConfigFileOnExit] = cbsChecked);
  ConfigurationXML.Configuration.PreferencesUtilitaire.AutoSaveConfigFileOnExit := BoolToStr(CheckGroupOptionUtilitaire.States[cst_AutoSaveConfigFileOnExit] = cbsChecked);
  ConfigurationXML.Configuration.PreferencesUtilitaire.BackUpConfigFile := BoolToStr(CheckGroupOptionUtilitaire.States[cst_BackUpConfigFile] = cbsChecked);
  CanSaveConfigFile := True;
end;
//******************************************************************************
procedure TFormAssistantDeploiement.cxTreeListUpdatePackageDatePropertiesChange(
  Sender: TObject);
begin
  TcxCustomEdit(Sender).PostEditValue;
    if TPackageLogiciel(TreeListPackagesUpdater.FocusedNode.Data).Checked then
      TPackageLogiciel(TreeListPackagesUpdater.FocusedNode.Data).NewDate := StrToDate(cxTreeListUpdatePackageDate.EditValue);
end;
//******************************************************************************
procedure TFormAssistantDeploiement.cxTreeListUpdatePackageSelectPropertiesEditValueChanged(
  Sender: TObject);
begin
//Gestion sur le check pour mettre la date du jour
  TcxCustomEdit(Sender).PostEditValue;
  TPackageLogiciel(TreeListPackagesUpdater.FocusedNode.Data).Checked := cxTreeListUpdatePackageSelect.EditValue;
    if TPackageLogiciel(TreeListPackagesUpdater.FocusedNode.Data).Checked then
      begin
      TPackageLogiciel(TreeListPackagesUpdater.FocusedNode.Data).NewDate := Date;
      TreeListPackagesUpdater.FocusedNode.Values[cst_PackageDate] := TPackageLogiciel(TreeListPackagesUpdater.FocusedNode.Data).NewDate;
      end
    else
      TreeListPackagesUpdater.FocusedNode.Values[cst_PackageDate] := TPackageLogiciel(TreeListPackagesUpdater.FocusedNode.Data).SaveDate;
end;
//******************************************************************************
procedure TFormAssistantDeploiement.ButtonSaveConfigXMLClick(Sender: TObject);
begin
  SaveConfigXML(True);
end;
//******************************************************************************
procedure TFormAssistantDeploiement.ButtonSavePropertiesClick(Sender: TObject);
begin
  GetTabSheetProprietesLogiciel;
end;
//******************************************************************************
procedure TFormAssistantDeploiement.ButtonUpdateServeursClick(Sender: TObject);
var
  UploadSetup : Boolean;
  UploadUpdater : Boolean;
  tempMSG : String;
  UpdateToDo : Boolean;
  cpt : Integer;
begin


    UploadSetup    := CheckGroupCibleUpdate.States[cst_GenereSetup] = cbsChecked;
    UploadUpdater  := CheckGroupCibleUpdate.States[cst_GenereUpdate] = cbsChecked;
    UpdateToDo := False;
      for cpt := 0 to Logiciels.LogicielCourant.PackagesLogiciels.Count - 1 do
        if Logiciels.LogicielCourant.PackagesLogiciels[cpt].Checked then
          begin
          UpdateToDo := True;
          Break;
          end;
      if Not(UpdateToDo) then
        begin
        BBS_ShowMessage('Aucune package s�lectionn�');
        Exit;
        end;

      if UploadSetup or UploadUpdater then
        begin
        tempMSG := 'Valider la mise � jour ';
          if UploadSetup then
            begin
            tempMSG := tempMSG + 'du Setup';
              if UploadUpdater then
                tempMSG := tempMSG + ' et ';
            end;
          if UploadUpdater then
            tempMSG := tempMSG + 'de l''Updater';
        tempMSG := tempMSG + '?';
          if BBS_ShowMessage(tempMSG,  mtConfirmation, [mbYes,mbNo] ,0) = mrYes then
            begin
              if UploadSetup then
                begin
                  if GetPreferenceValue(ConfigurationXML.Configuration.PreferencesUtilitaire.SaveConfigLogicielBeforeISSForSetup) then
                    SaveConfigurationXMLProprietesLogiciel(Logiciels.LogicielCourant);
                  if GetPreferenceValue(ConfigurationXML.Configuration.PreferencesUtilitaire.SaveFileConfigFileOnSetupExecution) then
                    SaveConfigXML(False);
                end;
            Logiciels.LogicielCourant.DeploiementUpdate(UploadSetup, UploadUpdater,
                                                               GetPreferenceValue(ConfigurationXML.Configuration.PreferencesUtilitaire.GenerateISSForSetupExecution),
                                                               GetPreferenceValue(ConfigurationXML.Configuration.PreferencesUtilitaire.BackUpSetup),
                                                               GetPreferenceValue(ConfigurationXML.Configuration.PreferencesUtilitaire.ExtractSetup),
                                                               GetPreferenceValue(ConfigurationXML.Configuration.PreferencesUtilitaire.ExtractSilencieux));
            end;
        end
      else
        BBS_ShowMessage('Aucune cible s�lectionn�e.');

end;
//******************************************************************************
procedure TFormAssistantDeploiement.EditButtonPathClick(Sender: TObject; AButtonIndex: Integer);
var
  aDir : string;
begin
    aDir := TcxButtonEdit( Sender ).Text;
    if (SelectDirectory( 'Choisir un r�pertroire', '', aDir ) ) then
      begin
      TcxButtonEdit(Sender).ActiveProperties.ViewStyle := vsNormal;
      TcxButtonEdit( Sender ).EditingText := aDir;
    end;
end;
//******************************************************************************
procedure TFormAssistantDeploiement.EditButtonGuidClick(Sender: TObject; AButtonIndex: Integer);
Var
  GUID: TGUID;
  Retour : String;
begin
  CreateGuid(GUID);
  Retour := GUIDToString(GUID);
  Retour := StrReplace(Retour, '{', '');
  Retour := StrReplace(Retour, '}', '');
  TcxButtonEdit(Sender).EditingText := Retour;
end;
//******************************************************************************
function TFormAssistantDeploiement.GetPreferenceValue(_Value : String) : Boolean;
begin
  if trim(_Value) = '' then
    Result := False
  else
    Result := StrToBool(_Value);
end;
//******************************************************************************
function TFormAssistantDeploiement.GetPreferenceState(_Value : String) : TcxCheckBoxState;
begin
  if trim(_Value) = '' then
    Result := TcxCheckBoxState.cbsUnChecked
  else
  if StrToBool(_Value) then
    Result := TcxCheckBoxState.cbsChecked
  else
    Result := TcxCheckBoxState.cbsUnChecked;
end;
//******************************************************************************
procedure TFormAssistantDeploiement.CopierTexteDansPressePapier(_Value : String);
begin
  Clipboard.AsText := _Value;
  BBS_ShowMessage(_Value + #13 + 'copi� dans le presse-papier');
end;
//******************************************************************************
procedure TFormAssistantDeploiement.ButtonGetLienPatchNoteClientClick(
  Sender: TObject);
begin
    CopierTexteDansPressePapier(Logiciels.LogicielCourant.GetURLPatchNoteClient);
end;
//******************************************************************************
procedure TFormAssistantDeploiement.ButtonUploadLastSetupSurServeurDownloadClick(
  Sender: TObject);
begin
    CopierTexteDansPressePapier(Logiciels.LogicielCourant.UploadLastSetupSurServeurDownload);
end;
//******************************************************************************
procedure TFormAssistantDeploiement.ButtonUploadVersionDevSurServeurDownloadClick(
  Sender: TObject);
begin
    CopierTexteDansPressePapier(Logiciels.LogicielCourant.UploadVersionDevSurServeurDownload);
end;
//******************************************************************************
procedure TFormAssistantDeploiement.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  if GetPreferenceValue(ConfigurationXML.Configuration.PreferencesUtilitaire.AskSaveConfigFileOnExit) then
    SaveConfigXML(True)
  else
  if GetPreferenceValue(ConfigurationXML.Configuration.PreferencesUtilitaire.AutoSaveConfigFileOnExit) then
    SaveConfigXML(False);
end;
//******************************************************************************
procedure TFormAssistantDeploiement.FormCreate(Sender: TObject);
begin
  Initialisation;
end;
//******************************************************************************
procedure TFormAssistantDeploiement.FormDestroy(Sender: TObject);
begin
  ConfigurationXML := Nil;
  Logiciels.Destroy;
  PackagesCommuns.Destroy;
  PackagesLogiciels.Destroy;
  Options.Destroy;
  Proprietes.Destroy;
  Serveurs.Destroy;
  DestroyGlobalCEFApp;
  ChromiumPatchNote.Destroy;
  CEFWindowParent1.Destroy;
end;
//******************************************************************************
procedure TFormAssistantDeploiement.UpdateTabSheets;
begin

  if Logiciels.IndexLogicielCourant > -1 then
    RadioGroupListeLogiciels.ItemIndex := Logiciels.IndexLogicielCourant;


  UpdateTabSheetProprietesLogiciel;
  UpdateTabSheetUpdater;
  UpdateTabSheetPatchNote;

//  PageControlLogiciel.ActivePage := TabSheetProprietesLogiciel;
end;
//******************************************************************************
procedure TFormAssistantDeploiement.UpdateTabSheetProprietesLogiciel;
var
  cpt: Integer;
begin

  TabSheetProprietesLogiciel.TabVisible := Logiciels.LogicielCourant <> Nil;
  if Not(TabSheetProprietesLogiciel.TabVisible) then
    Exit;

  for cpt := 0 to Proprietes.Count - 1 do
    (VerticalGridProprietesLogiciel.Rows[cpt] as TcxEditorRow).Properties.Value := Logiciels.LogicielCourant.GetProperty(Proprietes[cpt].XML.ID);


  for cpt := 0 to Options.Count - 1 do
    if Logiciels.LogicielCourant.GetOption(Options[cpt].XML.ID) then
      CheckGroupOptionsLogiciel.States[cpt] := cbsChecked
    else
      CheckGroupOptionsLogiciel.States[cpt] := cbsUnchecked;

  for cpt := 0 to PackagesCommuns.Count - 1 do
    if Logiciels.LogicielCourant.GetPackageCommun(PackagesCommuns[cpt].XML.ID) then
      CheckGroupListePackagesCommuns.States[cpt] := cbsChecked
    else
      CheckGroupListePackagesCommuns.States[cpt] := cbsUnchecked;

end;
//******************************************************************************
procedure TFormAssistantDeploiement.UpdateTabSheetUpdater;
Var
  cpt : Integer;
begin
    TabSheetUpdater.TabVisible := Logiciels.LogicielCourant <> Nil;


  if Not(TabSheetUpdater.TabVisible) then
    Exit;

  TreeListPackagesUpdater.Clear;

    for cpt := 0 to Logiciels.LogicielCourant.PackagesLogiciels.Count - 1 do
      begin
      TreeListPackagesUpdater.Root.AddChild.Data := Logiciels.LogicielCourant.PackagesLogiciels[cpt];
      TreeListPackagesUpdater.LastNode.Values[cst_PackageReference] := TPackageLogiciel(TreeListPackagesUpdater.LastNode.Data).XML.Nom;
      TreeListPackagesUpdater.LastNode.Values[cst_PackageChecked] := TPackageLogiciel(TreeListPackagesUpdater.LastNode.Data).Checked;
        if TPackageLogiciel(TreeListPackagesUpdater.LastNode.Data).Checked then
          TreeListPackagesUpdater.LastNode.Values[cst_PackageDate] := TPackageLogiciel(TreeListPackagesUpdater.LastNode.Data).NewDate
        else
          TreeListPackagesUpdater.LastNode.Values[cst_PackageDate] := TPackageLogiciel(TreeListPackagesUpdater.LastNode.Data).SaveDate;
      end;
end;
//******************************************************************************
procedure TFormAssistantDeploiement.UpdateTabSheetPatchNote;
begin
    TabSheetPatchNote.TabVisible := Logiciels.LogicielCourant <> Nil;

  if Not(TabSheetPatchNote.TabVisible) then
    Exit;

  ChromiumPatchNote.LoadURL(Logiciels.LogicielCourant.GetURLPatchNoteDev);
//  ChromiumPatchNote.Load('http:\\www.google.com');

end;
//******************************************************************************
procedure TFormAssistantDeploiement.UpdateGroupBoxLogiciels;
Var
  cpt : Integer;
begin
RadioGroupListeLogiciels.Properties.Items.Clear;
    for cpt := 0 to Logiciels.Count - 1 do
      begin
      RadioGroupListeLogiciels.Properties.Items.Add;
      UpdateNomLogicielPanneauSelectionLogiciel(Logiciels[cpt]);
      end;

  ButtonRemoveLogiciel.Enabled := Not((Logiciels.Count = 0) or (Logiciels.LogicielCourant = Nil));
  ButtonCopyLogiciel.Enabled := False;
  ButtonCopyLogiciel.Caption := 'Copier logiciel - Ne fonctionne pas...'
end;
//******************************************************************************
procedure TFormAssistantDeploiement.GetTabSheetProprietesLogiciel;
begin

  if Logiciels.LogicielCourant = Nil then
    exit;

    SaveConfigurationXMLProprietesLogiciel(Logiciels.LogicielCourant);
end;
//******************************************************************************
procedure TFormAssistantDeploiement.SaveConfigurationXMLProprietesLogiciel(_Logiciel : TLogiciel);
var
  cpt: Integer;
begin

  if _Logiciel = Nil then
    Exit;

    for cpt := 0 to Proprietes.Count - 1 do
      _Logiciel.SetProperty(Proprietes[cpt].XML.ID, (VerticalGridProprietesLogiciel.Rows[cpt] as TcxEditorRow).Properties.Value);

    for cpt := 0 to Options.Count - 1 do
      _Logiciel.SetOption(Options[cpt].XML.ID, CheckGroupOptionsLogiciel.States[cpt] = cbsChecked);

    for cpt := 0 to PackagesCommuns.Count - 1 do
      _Logiciel.SetPackageCommun(PackagesCommuns[cpt].XML.ID, CheckGroupListePackagesCommuns.States[cpt] = cbsChecked);

  UpdateNomLogicielPanneauSelectionLogiciel(_Logiciel);

  CanSaveConfigFile := True;
end;
//******************************************************************************
Procedure TFormAssistantDeploiement.UpdateNomLogicielPanneauSelectionLogiciel(_Logiciel : TLogiciel);
begin
  RadioGroupListeLogiciels.Properties.Items[Logiciels.IndexOf(_Logiciel)].Caption := _Logiciel.GetNomLongLogiciel + ' (' + _Logiciel.GetNomIndustriel + ')';
end;
//******************************************************************************
procedure TFormAssistantDeploiement.SaveConfigXML(_Ask : Boolean);
Var
  XMLDoc : IXMLDocument;
  tempDate : String;
  tempNameSave : String;
begin

  if (Not(_Ask) and CanSaveConfigFile) or (BBS_ShowMessage('Sauvegarder le fichier de configuration?',  mtConfirmation, [mbYes,mbNo] ,0) = mrYes) then
    begin
      if GetPreferenceValue(ConfigurationXML.Configuration.PreferencesUtilitaire.BackupConfigFile) then
        begin
        tempDate := Util_FileDate(FolderConfigXML + NameConfigXML);
        tempNameSave := strreplace(NameConfigXML, '.xml', '', false);
//        Util_CopyFile(FolderConfigXML + NameConfigXML, Util_BackUpDir(FolderConfigXML) + tempNameSave + '-' + tempDate + '.xml');
        Util_CopyFile(FolderConfigXML + NameConfigXML, Util_BackUpDir(FolderConfigXML) + tempNameSave + '-' + tempDate + '.xml', true);
        end;
    XMLDoc := TXMLDocument.Create(Self);
    XMLDoc.LoadFromXML(ConfigurationXML.XML);
    XMLDoc.SaveToFile(FolderConfigXML + NameConfigXML);
    XMLDoc := Nil;
    CanSaveConfigFile := False;
    end;
end;
//******************************************************************************
procedure TFormAssistantDeploiement.Initialisation;
Var
  cpt : Integer;
  cpt2 : Integer;
  tempSTL : TStringList;
begin

  GlobalCEFApp := TCefApplication.Create;
  GlobalCEFApp.SingleProcess := True;
  GlobalCEFApp.StartMainProcess;

  CanSaveConfigFile := False;

    if Not(Util_FichierSurDisque(cst_FichierEmplacementConfig)) then
      begin
      BBS_ShowMessage('Pour fonctionner, il faut la pr�sence du fichier ' + cst_FichierEmplacementConfig + ' � c�t� de l''utilitaire.' + #13 +
                      'Celui ci doit contenir le chemin du fichier de configuration XML.' + #13 +
                      'Ce fichier va �tre cr�� automatiquement, il faudra donc renseigner le chemin en question.' );

      Util_CreateFile(cst_FichierEmplacementConfig);
      Application.Terminate;
      end;

    if Util_FichierSurDisque(cst_FichierEmplacementConfig) then
      begin
      tempSTL := TStringList.Create;
      tempSTL.LoadFromFile(cst_FichierEmplacementConfig);
        if (tempSTL.Count > 0) and Util_FichierSurDisque(Trim(tempSTL[0])) then
         begin
         NameConfigXML := ExtractFileName(tempSTL[0]);
         FolderConfigXML := ExtractFilePath(tempSTL[0]);
         end
        else
          begin
          BBS_ShowMessage('Le chemin du fichier de configuration pr�sent dans le fichier ' + cst_FichierEmplacementConfig + ' est incorrect.');
          Application.Terminate;
          end;
      end;

  ConfigurationXML := Loadroot(FolderConfigXML + NameConfigXML);

  Logiciels := TListeLogiciels.Create;
  PackagesCommuns := TListePackagesCommuns.Create;
  PackagesLogiciels := TListePackagesLogiciels.Create;
  Options := TListeOptions.Create;
  Proprietes := TListeProprietes.Create;
  Serveurs := TServeurs.Create;

  Serveurs.XML := ConfigurationXML.Configuration.Serveurs;

    for cpt := 0 to ConfigurationXML.Configuration.Proprietes.Count - 1 do
      Proprietes.AddItem.XML := ConfigurationXML.Configuration.Proprietes[cpt];

    for cpt := 0 to ConfigurationXML.Configuration.Packages_Generaux.Count - 1 do
      PackagesLogiciels.AddItem.XML := ConfigurationXML.Configuration.Packages_Generaux[cpt];

    for cpt := 0 to ConfigurationXML.Configuration.Options.Count - 1 do
      Options.AddItem.XML := ConfigurationXML.Configuration.Options[cpt];

    for cpt := 0 to ConfigurationXML.Configuration.Packages_Communs.Count - 1 do
      PackagesCommuns.AddItem.XML := ConfigurationXML.Configuration.Packages_Communs[cpt];

    for cpt := 0 to ConfigurationXML.Softwares.Count - 1 do
      Logiciels.GenereNewLogiciel(ConfigurationXML.Softwares[cpt], Options, Proprietes, PackagesLogiciels, PackagesCommuns,Serveurs, mclLoad);

    for cpt := 0 to Options.Count - 1 do
      begin
      CheckGroupOptionsLogiciel.Properties.Items.Add;
      CheckGroupOptionsLogiciel.Properties.Items[cpt].Caption := Options[cpt].XML.Nom;
      end;

    for cpt := 0 to PackagesCommuns.Count - 1 do
      begin
      CheckGroupListePackagesCommuns.Properties.Items.Add;
      CheckGroupListePackagesCommuns.Properties.Items[cpt].Caption := PackagesCommuns[cpt].XML.Nom;
      end;

  InitialisationEditeurs;
  InitialisationPreferencesUtilitaire;


    if Logiciels.Count > 0 then
      Logiciels.IndexLogicielCourant := 0;

  UpdateGroupBoxLogiciels;

  UpdateTabSheets;
  PageControlLogiciel.ActivePage := TabSheetProprietesLogiciel;
end;
//******************************************************************************
{
procedure RegisterSchemes(const registrar: ICefSchemeRegistrar);
begin
  registrar.AddCustomScheme('local', True, True, False);
end;
}
//******************************************************************************
procedure TFormAssistantDeploiement.InitialisationEditeurs;
Var
  cpt : Integer;
  myRow : TcxEditorRow;


begin


  GlobalCEFApp.Locale := 'fr';
  cxLocalizer1.FileName := ExtractFilePath(Application.ExeName) + 'CXLOCALIZATION.ini';
  cxLocalizer1.Active := True;
  cxLocalizer1.Locale := 1;
  CEFWindowParent1 := TCEFWindowParent.Create(TabSheetPatchNote);
  CEFWindowParent1.Parent := TabSheetPatchNote;
  CEFWindowParent1.Align := alClient;
//  ChromiumPatchNote := TChromium.Create(TabSheetPatchNote);
  ChromiumPatchNote := TChromium.Create(Self);
  // WebRTC's IP leaking can lowered/avoided by setting these preferences
  // To test this go to https://www.browserleaks.com/webrtc
  ChromiumPatchNote.WebRTCIPHandlingPolicy := hpDisableNonProxiedUDP;
  ChromiumPatchNote.WebRTCMultipleRoutes   := STATE_DISABLED;
  ChromiumPatchNote.WebRTCNonproxiedUDP    := STATE_DISABLED;
  ChromiumPatchNote.CreateBrowser(CEFWindowParent1, '');


    VerticalGridProprietesLogiciel.OptionsBehavior.AlwaysShowEditor := True;
    VerticalGridProprietesLogiciel.OptionsView.ShowButtons := true;
    VerticalGridProprietesLogiciel.OptionsView.ShowEditButtons := ecsbAlways;
    for cpt := 0 to Proprietes.Count - 1 do
      begin
      myRow := VerticalGridProprietesLogiciel.Add(TcxEditorRow) as TcxEditorRow;
      myRow.Properties.Caption := Proprietes[cpt].XML.Nom;
        If UpperCase(Proprietes[cpt].XML.Edit_Type) = UpperCase(cst_EditTexte) then
          begin
          myRow.Properties.EditPropertiesClass := TcxTextEditProperties;
          end
        else
        If UpperCase(Proprietes[cpt].XML.Edit_Type) = UpperCase(cst_EditEntier) then
          begin
          myRow.Properties.EditPropertiesClass := TcxSpinEditProperties;
          myRow.Properties.Options.ShowEditButtons := eisbAlways;
          end
        else
        If UpperCase(Proprietes[cpt].XML.Edit_Type) = UpperCase(cst_EditPath) then
          begin
          myRow.Properties.EditPropertiesClass := TcxbuttonEditProperties;
          myRow.Properties.EditProperties.OnButtonClick := EditButtonPathClick;
          myRow.Properties.Options.ShowEditButtons := eisbAlways;
          end
        else
        If UpperCase(Proprietes[cpt].XML.Edit_Type) = UpperCase(cst_EditGuid) then
          begin
          myRow.Properties.EditPropertiesClass := TcxbuttonEditProperties;
          myRow.Properties.EditProperties.OnButtonClick := EditButtonGuidClick;
          myRow.Properties.Options.ShowEditButtons := eisbAlways;
          End
        else
        If UpperCase(Proprietes[cpt].XML.Edit_Type) = UpperCase(cst_EditDate) then
          begin
          myRow.Properties.EditPropertiesClass := TcxDateEditProperties;
          myRow.Properties.Options.ShowEditButtons := eisbAlways;
          End
        else
          begin
          myRow.Properties.EditPropertiesClass := TcxTextEditProperties;
          end;
      end;

  TreeListPackagesUpdater.OptionsBehavior.AlwaysShowEditor := True;
  TreeListPackagesUpdater.OptionsView.ShowEditButtons := ecsbAlways;


end;
//******************************************************************************
procedure TFormAssistantDeploiement.InitialisationPreferencesUtilitaire;
begin
  CheckGroupOptionUtilitaire.States[cst_AskSaveConfigFileOnExit] := GetPreferenceState(ConfigurationXML.Configuration.PreferencesUtilitaire.AskSaveConfigFileOnExit);
  CheckGroupOptionUtilitaire.States[cst_AutoSaveConfigFileOnExit] := GetPreferenceState(ConfigurationXML.Configuration.PreferencesUtilitaire.AutoSaveConfigFileOnExit);
  CheckGroupOptionUtilitaire.States[cst_BackUpConfigFile] := GetPreferenceState(ConfigurationXML.Configuration.PreferencesUtilitaire.BackUpConfigFile);

  CheckGroupOptionsSauvegardeInstallation.States[cst_BackUpSetup] := GetPreferenceState(ConfigurationXML.Configuration.PreferencesUtilitaire.BackUpSetup);
  CheckGroupOptionsSauvegardeInstallation.States[cst_ExtractSetup] := GetPreferenceState(ConfigurationXML.Configuration.PreferencesUtilitaire.ExtractSetup);
  CheckGroupOptionsSauvegardeInstallation.States[cst_ExtractSilencieux] := GetPreferenceState(ConfigurationXML.Configuration.PreferencesUtilitaire.ExtractSilencieux);
  CheckGroupOptionsSauvegardeInstallation.States[cst_GenerateISSForSetupExecution] := GetPreferenceState(ConfigurationXML.Configuration.PreferencesUtilitaire.GenerateISSForSetupExecution);
  CheckGroupOptionsSauvegardeInstallation.States[cst_SaveConfigLogicielBeforeISSForSetup] := GetPreferenceState(ConfigurationXML.Configuration.PreferencesUtilitaire.SaveConfigLogicielBeforeISSForSetup);
  CheckGroupOptionsSauvegardeInstallation.States[cst_SaveFileConfigFileOnSetupExecution] := GetPreferenceState(ConfigurationXML.Configuration.PreferencesUtilitaire.SaveFileConfigFileOnSetupExecution);
end;
procedure TFormAssistantDeploiement.PageControlLogicielPageChanging(
  Sender: TObject; NewPage: TcxTabSheet; var AllowChange: Boolean);
begin

end;

//******************************************************************************
procedure TFormAssistantDeploiement.RadioGroupListeLogicielsPropertiesEditValueChanged(Sender: TObject);
begin
  Logiciels.IndexLogicielCourant := RadioGroupListeLogiciels.ItemIndex;
  UpdateTabSheets;
end;
//******************************************************************************
end.
