unit Ventil_Avert;

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls, 
  Buttons, ExtCtrls, jpeg;

type
  TFic_Avertissement = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    Memo1: TMemo;
    StaticText1: TStaticText;
    PresentationBbs: TImage;
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure StaticText1Click(Sender: TObject);
    procedure PresentationBbsClick(Sender: TObject);
    procedure Memo1Click(Sender: TObject);
    procedure Panel1Click(Sender: TObject);
    procedure Panel2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  Procedure ShowAvertissment;

var
  Fic_Avertissement: TFic_Avertissement;

implementation
Uses Ventil_Const;
{$R *.dfm}

{ ************************************************************************************************************************************************** }
Procedure ShowAvertissment;
Begin
  Application.CreateForm(TFic_Avertissement, Fic_Avertissement);
  Fic_Avertissement.ShowModal;
  Fic_Avertissement.Repaint; Application.ProcessMessages;
//  Sleep(2000);
  FreeAndNil(Fic_Avertissement);
End;
{ ************************************************************************************************************************************************** }
procedure TFic_Avertissement.FormCreate(Sender: TObject);
begin
Memo1.Lines.Clear;
Memo1.Lines.Add(cst_NomLogiciel + ' est un logiciel d''assistance au dimensionnement du r�seau de VMC');// en habitat collectif.');
Memo1.Lines.Add('Les utilisateurs de ce logiciel sont consid�r�s comme qualifi�s et connaissant parfaitement les techniques, r�glementations et r�gles de l''art.');
Memo1.Lines.Add('');
Memo1.Lines.Add('La conformit� de l''�tude r�alis�e �tant la mission du ma�tre d''oeuvre, en aucun cas ' + cst_NomFabricant + ' ne pourra �tre tenu responsable des r�sultats.');
Memo1.Lines.Add('');
Memo1.Lines.Add('Les r�sultats a�rauliques et techniques fournis par ce logiciel ne sont valables que pour des installations :');
Memo1.Lines.Add('- respectant les pr�conisations des constructeurs et recommandations des DTU 61.1, 68.1 et 68.2');
Memo1.Lines.Add('- r�alis�es avec des produits ' + cst_NomFabricant);
Memo1.Lines.Add('- utilisant des accessoires respectant les normes en vigueur');

PresentationBbs.Picture.LoadFromFile(ExtractFilePath(Application.Exename) + 'Bibliotheque\Progress.bmp');
end;
{ ************************************************************************************************************************************************** }
Procedure TFic_Avertissement.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
Begin
  If Key=27 Then ModalResult := IDOK;
End;
{ ************************************************************************************************************************************************** }
procedure TFic_Avertissement.StaticText1Click(Sender: TObject);
begin
Close;
end;
{ ************************************************************************************************************************************************** }
procedure TFic_Avertissement.PresentationBbsClick(Sender: TObject);
begin
close;
end;
{ ************************************************************************************************************************************************** }
procedure TFic_Avertissement.Memo1Click(Sender: TObject);
begin
close;
end;
{ ************************************************************************************************************************************************** }
procedure TFic_Avertissement.Panel1Click(Sender: TObject);
begin
close;
end;
{ ************************************************************************************************************************************************** }
procedure TFic_Avertissement.Panel2Click(Sender: TObject);
begin
Close;
end;
{ ************************************************************************************************************************************************** }
End.
