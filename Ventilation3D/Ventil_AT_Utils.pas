unit Ventil_AT_Utils;

interface

{$Include 'Ventil_Directives.pas'}

Uses
     Classes,
     Ventil_AT,
     XMLIntf;


Function GetXMLRoot(_IXMLNode : IXMLNODE) : IXMLNODE;
Function GetCaractBoucheFromCode(_Code : String; _XMLRoot : IXMLAvis_Technique_Ventilation) : IXMLEquipements_Bouches_Type_Bouche;
Function GetCaractEAFromCode(_Code : String; _XMLRoot : IXMLAvis_Technique_Ventilation) : IXMLEquipements_Entrees_Type_Entree;
Function GetCaractTypeSolutionFromCode(_Code : String; _XMLRoot : IXMLAvis_Technique_Ventilation) : IXMLType_Solution;
//procedure AddSolutionsToEntrees(Var _PieceSeche : IXMLPieceSeche; _SousSolution : Integer = 0);
Function GetNbConfigSolutionEntrees(Var _PieceSeche : IXMLPieceSeche) : Integer;
Function GetConfigSolutionEntrees(_Code : String; _Index : Integer; _XMLRoot : IXMLAvis_Technique_Ventilation) : IXMLConfig_Solution;
//procedure LoadFilesByMask(const SpecDir, WildCard: string ; Var lst: TStringList );
Procedure FindFiles(Path: String; var ExtList, FilesList:  TStringList;Const SortFlag : Integer = 0; Const  FullPathName : Boolean = false);
function GetParentDir(Directory: String): String;

Implementation

Uses SysUtils,
     System.StrUtils,
     CAD_ViloUtils,
     Windows;

{ ************************************************************************************************************************************************** }
Function GetXMLRoot(_IXMLNode : IXMLNODE) : IXMLNODE;
begin
  Result := _IXMLNode;
  if Result.ParentNode = Nil then Exit;
    while Result.ParentNode.ParentNode <> Nil do
      Result := Result.ParentNode;

end;
{ ************************************************************************************************************************************************** }
Function GetCaractBoucheFromCode(_Code : String; _XMLRoot : IXMLAvis_Technique_Ventilation) : IXMLEquipements_Bouches_Type_Bouche;
Var
  cpt : Integer;
begin
  Result := Nil;
    for cpt := 0 to _XMLRoot.Equipements.Bouches.Count - 1 do
      if Trim(UpperCase(_XMLRoot.Equipements.Bouches.Type_Bouche[cpt].Code)) = Trim(UpperCase(_Code)) then
        Begin
        Result := _XMLRoot.Equipements.Bouches.Type_Bouche[cpt];
        Exit;
        End;
end;
{ ************************************************************************************************************************************************** }
Function GetCaractEAFromCode(_Code : String; _XMLRoot : IXMLAvis_Technique_Ventilation) : IXMLEquipements_Entrees_Type_Entree;
Var
  cpt : Integer;
begin
  Result := Nil;
    for cpt := 0 to _XMLRoot.Equipements.Entrees.Count - 1 do
      if Trim(UpperCase(_XMLRoot.Equipements.Entrees.Type_Entree[cpt].Code)) = Trim(UpperCase(_Code)) then
        Begin
        Result := _XMLRoot.Equipements.Entrees.Type_Entree[cpt];
        Exit;
        End;
end;
{ ************************************************************************************************************************************************** }
Function GetCaractTypeSolutionFromCode(_Code : String; _XMLRoot : IXMLAvis_Technique_Ventilation) : IXMLType_Solution;
Var
  cpt : Integer;
begin
  Result := Nil;
    for cpt := 0 to _XMLRoot.Equipements.Solutions.Count - 1 do
      if Trim(UpperCase(_XMLRoot.Equipements.Solutions.Type_Solution[cpt].Code_Solution)) = Trim(UpperCase(_Code)) then
        Begin
        Result := _XMLRoot.Equipements.Solutions.Type_Solution[cpt];
        Exit;
        End;
end;
{ ************************************************************************************************************************************************** }
{procedure AddSolutionsToEntrees(Var _PieceSeche : IXMLPieceSeche; _SousSolution : Integer = 0);
Var
  cptPieceSolution : Integer;
  cptSolution : Integer;
  CptEntree : Integer;
  Entree : IXMLStructure_Entree;
  XMLRoot : IXMLAvis_Technique_Ventilation;
begin

    if _PieceSeche = nil then
      Exit;

    if _PieceSeche.Entree_Solution.Count > 0 then
      _PieceSeche.Entree.Clear;



  XMLRoot := (GetXMLRoot(_PieceSeche) as IXMLAvis_Technique_Ventilation);

    for cptPieceSolution := 0 to _PieceSeche.Entree_Solution.Count - 1 do
      for CptSolution := 0 to XMLRoot.Equipements.Solutions.Count - 1 do
        if Trim(UpperCase(XMLRoot.Equipements.Solutions.Type_Solution[CptSolution].Code_Solution)) = Trim(UpperCase(_PieceSeche.Entree_Solution[cptPieceSolution].Code)) then
          if (XMLRoot.Equipements.Solutions.Type_Solution[CptSolution].Config_Solution[_SousSolution].ChildNodes.FindNode('Solution_Libre') <> nil) and (XMLRoot.Equipements.Solutions.Type_Solution[CptSolution].Config_Solution[_SousSolution].Solution_Libre > 0) then
            begin
            //Todo
            end
          else
            for CptEntree := 0 to XMLRoot.Equipements.Solutions.Type_Solution[CptSolution].Config_Solution[_SousSolution].Entree.Count - 1 do
              begin
              Entree :=_PieceSeche.Entree.Add;
              Entree.Code := XMLRoot.Equipements.Solutions.Type_Solution[CptSolution].Config_Solution[_SousSolution].Entree[CptEntree].Code;
              Entree.Nombre := _PieceSeche.Entree_Solution[cptPieceSolution].Nombre * XMLRoot.Equipements.Solutions.Type_Solution[CptSolution].Config_Solution[_SousSolution].Entree[CptEntree].Nombre;
              end;
end;
}
{ ************************************************************************************************************************************************** }
Function GetNbConfigSolutionEntrees(Var _PieceSeche : IXMLPieceSeche) : Integer;
Var
  cptSolution : Integer;
  XMLRoot : IXMLAvis_Technique_Ventilation;
begin

  Result := 0;

    if _PieceSeche = nil then
      Exit;

  XMLRoot := (GetXMLRoot(_PieceSeche) as IXMLAvis_Technique_Ventilation);

    for CptSolution := 0 to XMLRoot.Equipements.Solutions.Count - 1 do
      if Trim(UpperCase(XMLRoot.Equipements.Solutions.Type_Solution[CptSolution].Code_Solution)) = Trim(UpperCase(_PieceSeche.Entree_Solution.Code)) then begin
        Result := XMLRoot.Equipements.Solutions.Type_Solution[CptSolution].Config_Solution.Count;
        Exit;
      end;
end;
{ ************************************************************************************************************************************************** }
Function GetConfigSolutionEntrees(_Code : String; _Index : Integer; _XMLRoot : IXMLAvis_Technique_Ventilation) : IXMLConfig_Solution;
Var
  cptSolution : Integer;
begin

  Result := Nil;

      for CptSolution := 0 to _XMLRoot.Equipements.Solutions.Count - 1 do
        if Trim(UpperCase(_XMLRoot.Equipements.Solutions.Type_Solution[CptSolution].Code_Solution)) = Trim(UpperCase(_Code)) then begin
          Result := _XMLRoot.Equipements.Solutions.Type_Solution[CptSolution].Config_Solution[_Index];
          exit;
        end;
end;
{ ************************************************************************************************************************************************** }

{function FileCount( const aFolder: string): Integer;
var
H: THandle;
Data: TWin32FindData;
begin
Result := 0;
H := FindFirstFile(PCHAR(aFolder + '*.*'), Data);
if H <> INVALID_HANDLE_VALUE then
repeat
Inc(Result, Ord(Data.dwFileAttributes and FILE_ATTRIBUTE_DIRECTORY = 0));
until
not FindNextFile(H, Data);
Windows.FindClose(H);
end;}

{Liste (avec ou sans chemin complet) les fichiers d'un r�pertoire filtr�s par leur extension,
ou tous les fichiers si aucune extension pass�e � la procedure.
SortFlag = 0   => Liste tri�e par ordre alphab�tique
SortFlag = 1   => Liste tri�e par date de modification
SortFlag = 2   => Liste tri�e par date de cr�ation}

Procedure FindFiles(Path: String; var ExtList, FilesList:  TStringList;Const SortFlag : Integer = 0; Const  FullPathName : Boolean = false);
  VAR              FileInfo   : TSearchRec;
                         i               : Integer;
                        S               : string;
                        DateFormat : string;


          function IsWanted(FileName : String): Boolean;
            var        i : Integer;
            begin
            result := false;
            if (FileName='.') or (FileName='..') then exit;
            if ExtList.Count=0 then begin
              result := true;
              Exit;
            end;
            for i := 0 to ExtList.Count-1 do begin
              if AnsiUpperCase(ExtractFileExt(FileName))=AnsiUpperCase(ExtList[i]) then begin
                result := true;
                Break;
              end;
            end;
          end;


          function FileTimeToString(FileTime : TFileTime): String;
            var        SysTime    : TSystemTime;
                          LocalTime : TFileTime;
            begin
            result := '-------------------';
            if FileTimeToLocalFileTime( FileTime, LocalTime ) then begin
              if FileTimeToSystemTime( LocalTime, SysTime ) then begin
                result := DateTimeToStr(SystemTimeToDateTime(SysTime));
              end;
            end;
          end;


  BEGIN
  Path := IncludeTrailingPathDelimiter(Path);
  if not DirectoryExists(Path) then Exit;
  DateFormat := FormatSettings.ShortDateFormat;
  FormatSettings.ShortDateFormat := 'yyyy/MM/dd';
  if FindFirst(Path+'*', faAnyFile, FileInfo)=0 then begin
    repeat
      if IsWanted(FileInfo.Name) then
      case SortFlag of
        0 : begin
              if FullPathName then FilesList.Add(Path + FileInfo.Name)
              else FilesList.Add(ReverseString(Decoup(ReverseString(FileInfo.Name), '.', 1)));
            end;
        1 : begin
              if FullPathName then FilesList.Add(DateTimeToStr(FileDateToDateTime(FileInfo.Time)) + Path + FileInfo.Name)
              else FilesList.Add(DateTimeToStr(FileDateToDateTime(FileInfo.Time)) + FileInfo.Name);
            end;
        2 : begin
              if FullPathName then FilesList.Add(FileTimeToString(FileInfo.FindData.ftCreationTime) + Path + FileInfo.Name)
              else FilesList.Add(FileTimeToString(FileInfo.FindData.ftCreationTime) + FileInfo.Name);
            end;
      end;
    Until FindNext(FileInfo)<>0;
  end;
  FormatSettings.ShortDateFormat := DateFormat;//On r�tablit le ShortDateFormat.
  if SortFlag <> 0 then begin
    FilesList.Sort;
    for i := 0 to FilesList.Count-1 do begin
      S := FilesList[i];
      Delete(S,1,19);//On �limine les dates.
      FilesList[i] := S;
    end;
  end;
  //FindClose(FileInfo);
END;

function GetParentDir(Directory: String): String;
var TempStr: String;
begin
  Result := Directory;
  if (Result[Length(Result)] = '\') then
    Delete(Result, Length(Result), 1);
   Result := Copy(Result, 1, LastDelimiter('\', Result) - 1);
end;

end.
