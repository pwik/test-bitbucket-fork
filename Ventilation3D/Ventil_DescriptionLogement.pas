Unit Ventil_DescriptionLogement;
                                    
{$Include 'Ventil_Directives.pas'}

Interface

Uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls, Buttons, ExtCtrls, Grids, BaseGrid, advObj, AdvGrid, Ventil_Logement;

Type
  TFic_DescriptrionLogment = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Memo_Messages: TMemo;
    Grid_DescriptionLogement: TAdvStringGrid;
    procedure Grid_DescriptionLogementGetEditorType(Sender: TObject; ACol, ARow: Integer; var AEditor: TEditorType);
    procedure Grid_DescriptionLogementIsFixedCell(Sender: TObject; ARow, ACol: Integer; var IsFixed: Boolean);
    procedure Grid_DescriptionLogementCellValidate(Sender: TObject; ACol, ARow: Integer; var Value: String; var Valid: Boolean);
    procedure FormCreate(Sender: TObject);
  Private
    Procedure Logement2Grid(Const _Log: TVentil_Logement);
    Procedure Grid2Logement(Var _Log: TVentil_Logement);
  Public
  End;

Var
  Fic_DescriptrionLogment: TFic_DescriptrionLogment;

Procedure DescriptionLogements(_Logement: TVentil_Logement);

Implementation

Uses
  BbsgFonc,
  Ventil_Utils,
  Ventil_Types,
  Ventil_Const;

{$R *.dfm}

{ ************************************************************************************************************************************************** }
Procedure DescriptionLogements(_Logement: TVentil_Logement);
Begin
  Application.CreateForm(TFic_DescriptrionLogment, Fic_DescriptrionLogment);
  {$IFDEF OPTIMA3D}
  Fic_DescriptrionLogment.Grid_DescriptionLogement.ColCount := 4;
  {$ELSE}
  Fic_DescriptrionLogment.Grid_DescriptionLogement.ColCount := 3;
  {$ENDIF}
  Fic_DescriptrionLogment.Logement2Grid(_Logement);
  If Fic_DescriptrionLogment.ShowModal = mrOk Then Fic_DescriptrionLogment.Grid2Logement(_Logement);
  FreeAndNil(Fic_DescriptrionLogment);
End;
{ ************************************************************************************************************************************************** }
Procedure TFic_DescriptrionLogment.Logement2Grid(Const _Log: TVentil_Logement);
Var
  m_NoPiece: Integer;
Begin
  Caption := 'Description de : ' + _Log.Reference;
  Grid_DescriptionLogement.Objects[0,0] := _Log;
  For m_NoPiece := 1 To c_NbPiecesHumide Do
  Begin
    Grid_DescriptionLogement.Cells[1, m_NoPiece] := Float2Str(_Log.InfosPieces[m_NoPiece, 1], 2);
    Grid_DescriptionLogement.Cells[2, m_NoPiece] := Float2Str(_Log.InfosPieces[m_NoPiece, 2], 2);
  End;
  Grid_DescriptionLogement.Cells[3, 1] := Float2Str(_Log.Tr_Cuisine, 1);
End;
{ ************************************************************************************************************************************************** }
Procedure TFic_DescriptrionLogment.Grid2Logement(Var _Log: TVentil_Logement);
Var
  m_NoPiece: Integer;
Begin
  For m_NoPiece := 1 To c_NbPiecesHumide Do
  Begin
    _Log.InfosPieces[m_NoPiece, 1] := Grid_DescriptionLogement.Floats[1, m_NoPiece];
    _Log.InfosPieces[m_NoPiece, 2] := Grid_DescriptionLogement.Floats[2, m_NoPiece];
  End;
  {$IFDEF OPTIMA3D}
  _Log.Tr_Cuisine := Grid_DescriptionLogement.Floats[3, 1];
  {$ENDIF}
End;
{ ************************************************************************************************************************************************** }
Procedure TFic_DescriptrionLogment.Grid_DescriptionLogementGetEditorType(Sender: TObject; ACol, ARow: Integer; var AEditor: TEditorType);
Begin
  AEditor := edNone;
  If (Grid_DescriptionLogement.Objects[0,0] <> Nil) Then Case ACol Of
    1,
    2: If ARow > 0 Then AEditor := edFloat;
    3: If ARow = 1 Then AEditor := edFloat;
  End;
End;
{ ************************************************************************************************************************************************** }
Procedure TFic_DescriptrionLogment.Grid_DescriptionLogementIsFixedCell(Sender: TObject; ARow, ACol: Integer; var IsFixed: Boolean);
Begin
  IsFixed := (ACol = 3) And (ARow > 1);
End;
{ ************************************************************************************************************************************************** }
Procedure TFic_DescriptrionLogment.Grid_DescriptionLogementCellValidate(Sender: TObject; ACol, ARow: Integer; var Value: String; var Valid: Boolean);
Var
  V: Real;
Begin
  If (Grid_DescriptionLogement.Objects[0,0] <> Nil) And (ACol = 3) And (ARow = 1) Then Begin
    V := Str2Float(Value);
    If V < 0.1 Then Value := Float2Str(0.5, 1);
    TVentil_Logement(Grid_DescriptionLogement.Objects[0,0]).TR_Cuisine := StrToFloat(Value);
    Valid := V >= 0.1;
  End;
End;
{ ************************************************************************************************************************************************** }
Procedure TFic_DescriptrionLogment.FormCreate(Sender: TObject);
Var
  m_NoPiece: Integer;
Begin
  Grid_DescriptionLogement.ColWidths[0] := Grid_DescriptionLogement.DefaultColWidth + 60;
  Grid_DescriptionLogement.ColWidths[1] := Grid_DescriptionLogement.DefaultColWidth - 30;
  Grid_DescriptionLogement.ColWidths[2] := Grid_DescriptionLogement.DefaultColWidth - 30;
//  Grid_DescriptionLogement.RowHeights[0] := Grid_DescriptionLogement.DefaultRowHeight * 2;
  For m_NoPiece := 2 To Grid_DescriptionLogement.RowCount - 1 Do Grid_DescriptionLogement.CellProperties[3, m_Nopiece].BrushColor := ClBtnface;
  Grid_DescriptionLogement.Cells[0, 0] := 'Pi�ce';
  Grid_DescriptionLogement.Cells[1, 0] := 'Ht (m)';
  Grid_DescriptionLogement.Cells[2, 0] := 'Surf. (m�)';
  {$IFDEF OPTIMA3D}
  Grid_DescriptionLogement.Cells[3, 0] := 'Tr (sec)';
  {$EndIF}
  For m_NoPiece := Low(c_NomsPiecesHumides) To High(c_NomsPiecesHumides) Do Grid_DescriptionLogement.Cells[0, m_NoPiece] := c_NomsPiecesHumides[m_NoPiece];
End;
{ ************************************************************************************************************************************************** }

End.
