unit VPEngine;

{
   VPENGINE.PAS
   ============

   Common definitions for the "Virtual Print Engine"

   Version 6.1.0.0

   Copyright � 1993 - 2011 IDEAL Software�. All rights reserved.
   
   Source code generated automatically with our internal MK_VCL toolkit.
}

{$ifdef BCB}
   {$ObjExportAll On}
{$endif}

interface

uses
    Windows, Messages,
    SysUtils;     { for Exception-Throwing }

const
    VPEDLL = 'vpee3261.DLL';


type HGLOBAL = THandle;


Const

  { ======================================================================== }
  {                    Editions returned by VpeGetEdition():                 }
  { ======================================================================== }
  VEDITION_COMMUNITY      = 500;
  VEDITION_STANDARD       = 1000;
  VEDITION_ENHANCED       = 2000;
  VEDITION_PROFESSIONAL   = 3000;
  VEDITION_ENTERPRISE     = 4000;
  VEDITION_INTERACTIVE    = 5000;

  { ======================================================================== }
  {                    VpeOpenDoc() Flag Parameters:                         }
  { ======================================================================== }
  VPE_NO_TOOLBAR         = 1;         { Toolbar NOT visible }
  VPE_NO_PRINTBUTTON     = 8;         { Print-Button invisible, VpePrintDoc() works }
  VPE_NO_MAILBUTTON      = 16;        { Mail-Button invisible }
  VPE_NO_SCALEBTNS       = 32;        { No Scale Buttons in Toolbar }
  VPE_GRIDBUTTON         = 64;        { Grid Toolbar-Button visible }
  VPE_NO_MOVEBTNS        = 128;       { Move Buttons invisible }
  VPE_NO_HELPBUTTON      = 256;       { Help-Button invisible }
  VPE_NO_INFOBUTTON      = 512;       { Info-Button invisible }
  VPE_NO_USER_CLOSE      = 1024;      { User can't close VPE:
                                        Close-Button INVISIBLE and Sys-Menu
                                        disabled (if not embedded)
                                        VpeCloseDoc() works! }

  VPE_NO_STATBAR         = 2048;      { Statusbar invisible }
  VPE_NO_PAGESCROLLER    = 4096;      { Page Scroller in Statusbar invisible }
  VPE_NO_STATUSSEG       = 8192;      { Status Segment in Statusbar invisible }

  VPE_NO_RULERS          = 16384;     { Rulers invisible }
  VPE_EMBEDDED           = 32768;     { Preview is made child window of
                                        parent window specified in VpeOpenDoc() }
  VPE_DOCFILE_READONLY   = 65536;     { Document file is opened with read-only permission }
  VPE_FIXED_MESSAGES     = 131072;    { Messages are not registered by the Windows System }

  VPE_NO_OPEN_BUTTON     = 262144;    { No File Open Button }
  VPE_NO_SAVE_BUTTON     = 524288;    { No File Save Button }


  { ======================================================================== }
  {                     VpePreviewDoc() Flag Parameters:                     }
  { ======================================================================== }
  VPE_SHOW_NORMAL        = 1;
  VPE_SHOW_MAXIMIZED     = 2;
  VPE_SHOW_HIDE          = 3;


  { ======================================================================== }
  {                          Background Modes:                               }
  { ======================================================================== }
  VBKG_SOLID       = 0;      { solid background color }
  VBKG_TRANSPARENT = 1;      { transparent background }
  VBKG_GRD_LINE    = 2;      { line gradient background }
  VBKG_GRD_RECT    = 3;      { rectangular gradient background }
  VBKG_GRD_ELLIPSE = 4;      { elliptic gradient background }


  { ======================================================================== }
  {                          Modes for GradientPrint:                        }
  { ======================================================================== }
  VGRD_PRINT_AUTO     = 0;   { if printer is a color printer, the gradient
                               is printed, otherwise the alternative solid color }
  VGRD_PRINT_GRADIENT = 1;   { the gradient is always printed }
  VGRD_PRINT_SOLID    = 2;   { the alternative solid color is always printed }


  { ======================================================================== }
  {                          Hatch Styles:                                   }
  { ======================================================================== }
  hsNone = -1;   { All other hsXyz styles are already defined by VCL units }


  { ======================================================================== }
  {                     Text-Formatting Attributes:                          }
  { ======================================================================== }
  ALIGN_LEFT             = 0;
  ALIGN_RIGHT            = 1;
  ALIGN_CENTER           = 2;
  ALIGN_JUSTIFIED        = 3;
  ALIGN_JUSTIFIED_AB     = 5;
  ALIGN_LEFT_CUT         = 7;



  { ======================================================================== }
  {                    Auto-Break Options:                                   }
  { ======================================================================== }
  AUTO_BREAK_ON        = 0;     { Auto Break on; top and bottom of DefOutRect are used }
  AUTO_BREAK_OFF       = 1;     { Same behaviour as AUTO_BREAK_ON (limited positioning /
                                  rendering), but no Auto Break performed }
  AUTO_BREAK_NO_LIMITS = 2;     { No Auto Break performed, no limited positioning / rendering }
  AUTO_BREAK_FULL      = 3;     { Auto Break on; complete DefOutRect is used }



  { Flags for VpeSetPictureType(): }
  { ============================== }
  PIC_TYPE_AUTO   = 255;   { Auto determination by filename-suffix (default) }
  PIC_TYPE_BMP    = 0;     { Windows-Bitmap }
  PIC_TYPE_WMF    = 5;     { WMF }
  PIC_TYPE_EMF    = 6;     { EMF }
  PIC_TYPE_TIFF   = 64;    { TIF-File, note: *might* require PIC_ALLOWLZW }
  PIC_TYPE_GIF    = 65;    { GIF-File, note: requires PIC_ALLOWLZW }
  PIC_TYPE_PCX    = 66;    { PCX file }
  PIC_TYPE_FLT    = 67;    { Use a Microsoft / Aldus - Filterprogram }
  PIC_TYPE_JPEG   = 68;    { JPEG-File }
  PIC_TYPE_PNG    = 69;    { PNG (Portable Network Graphic) }
  PIC_TYPE_ICO    = 70;    { ICO (Windows Icon) }
  PIC_TYPE_JNG    = 71;    { JNG (JPEG Network Graphics) }
  PIC_TYPE_KOALA  = 72;    { KOA (C64 Koala Graphics) }
  PIC_TYPE_IFF    = 73;    { IFF/LBM (Interchangeable File Format - Amiga/Deluxe Paint) }
  PIC_TYPE_MNG    = 74;    { MNG (Multiple-Image Network Graphics) }
  PIC_TYPE_PBM    = 75;    { PBM (Portable Bitmap [ASCII]) }
  PIC_TYPE_PBM_RAW= 76;    { PBM (Portable Bitmap [RAW]) }
  PIC_TYPE_PCD    = 77;    { PCD (Kodak PhotoCD) }
  PIC_TYPE_PGM    = 78;    { PGM (Portable Greymap [ASCII]) }
  PIC_TYPE_PGM_RAW= 79;    { PGM (Portable Greymap [RAW]) }
  PIC_TYPE_PPM    = 80;    { PPM (Portable Pixelmap [ASCII]) }
  PIC_TYPE_PPM_RAW= 81;    { PPM (Portable Pixelmap [RAW]) }
  PIC_TYPE_RAS    = 82;    { RAS (Sun Raster Image) }
  PIC_TYPE_TARGA  = 83;    { TGA/TARGA (Truevision Targa) }
  PIC_TYPE_WBMP   = 84;    { WAP/WBMP/WBM (Wireless Bitmap) }
  PIC_TYPE_PSD    = 85;    { PSD (Adobe Photoshop) }
  PIC_TYPE_CUT    = 86;    { CUT (Dr. Halo) }
  PIC_TYPE_XBM    = 87;    { XBM (X11 Bitmap Format) }
  PIC_TYPE_XPM    = 88;    { XPM (X11 Pixmap Format) }
  PIC_TYPE_DDS    = 89;    { DDS (DirectX Surface) }
  PIC_TYPE_HDR    = 90;    { HDR (High Dynamic Range Image) }
  PIC_TYPE_FAX_G3 = 91;    { G3  (Raw fax format CCITT G.3) }
  PIC_TYPE_SGI    = 92;    { SGI (SGI Image Format) }




  { ======================================================================== }
  {                        Printer- / Setup-Flags:                           }
  { ======================================================================== }
  PRINTDLG_NEVER  = 0;       { never show setup-dialog (if file_name is NULL, last setting
                               or the setting of the default-printer will be taken) }
  PRINTDLG_ONFAIL = 1;       { show setup-dialog only, if file-read fails }
  PRINTDLG_ALWAYS = 2;       { show setup-dialog always }
  PRINTDLG_FULL   = 4;       { show FULL Dialog (with page range, collation, etc.)
                               This is a flag, i.e. add it to one of the other
                               PRINTDLG_xyz flags
                               (example: PRINTDLG_ALWAYS +  PRINTDLG_FULL)
                               The flag is especially useful, if you hide the
                               Toolbar or the Print-Button. }

  PRINT_ALL               = 0;  { print all pages }
  PRINT_EVEN              = 1;  { print only even pages }
  PRINT_ODD               = 2;  { print only odd pages }
  PRINT_NOABORTDLG        = 4;  { show no abort / progress dialog during printing }
  PRINT_WAIT_COMPLETION   = 8;  { wait until print job has been transfered completely to the printer }
  PRINT_NO_AUTO_PAGE_DIMS = 16; { normally, VPE instructs the printer during printing, to switch the paper dimensions }
                                { automatically (some printers will therefore select another input PaperBin) according to }
                                { the page dimensions of the VPE Document's currently printed page. This flag stops VPE from doing so. }

  PRINTPOS_ABSOLUTE = 0;     { coordinates are absolute to the paper-edges (default) }
  PRINTPOS_RELATIVE = 1;     { coordinates are relative to the first printable }
                             { positon of the printer }


  { ======================================================================== }
  { Notification Messages:                                                   }
  { ======================================================================== }
  VPE_DESTROYWINDOW  = WM_USER + 2306;  { VPE was destroyed - lParam = Doc-Handle }

  VPE_PRINT          = WM_USER + 2307;  { printing: wParam = one of the PRINT_MSG_xyz constants
                                          lParam = Doc-Handle }

  VPE_AUTOPAGEBREAK  = WM_USER + 2308;  { Auto Page Break occured. Engine is on the new page.
                                          This is either a newly generated page, or an
                                          already existing page (if the text was inserted on
                                          a page, that has already pages following) }

  VPE_PRINT_NEWPAGE  = WM_USER + 2309;  { Engine is printing and is exactly
                                          before printing a new page
                                          wParam = page no.
                                          lParam = Doc-Handle }

  VPE_PRINT_DEVDATA  = WM_USER + 2310;  { Engine is printing and is exactly
                                          before printing a new page
                                          wParam = page no.
                                          lParam = Doc-Handle }

  VPE_HELP           = WM_USER + 2311;  { User requests help - lParam = Doc-Handle }
  VPE_CLOSEWINDOW    = WM_USER + 2312;  { Preview was closed, but document not destroyed
                                          lParam = Doc-Handle }
  VPE_OBJECTCLICKED  = WM_USER + 2313;
  VPE_UDO_PAINT      = WM_USER + 2314;  { Notification for User Defined Object
                                          lParam = Doc-Handle [Professional Edition and above] }
  VPE_BEFORE_MAIL    = WM_USER + 2315;  { User pushed eMail-button, but email was not sent yet
                                          lParam = Doc-Handle }
  VPE_AFTER_MAIL     = WM_USER + 2316;  { User pushed eMail-button and email was sent
                                          wParam = VERR_xyz, lParam = Doc-Handle }
  VPE_CANCLOSE       = WM_USER + 2317;  { VPE requests for confirmation if the preview can be closed
                                          lParam = Document Handle
                                          Your application should return: 1 = no, the preview may not be closed; zero otherwise }

                                            { All Control Messages: wParam = Object Handle
                                                                    lParam = Document Handle }
  VPE_CTRL_AFTER_ENTER   = WM_USER + 2318;  { Control received the focus [Interactive Edition only]
                                              Possible return codes: your application should return zero when processing this message }

  VPE_CTRL_CAN_EXIT      = WM_USER + 2319;  { Control requests, if it may loose the focus [Interactive Edition only]
                                              Your Application should return: 1 = no, the control may not loose the focus, zero otherwise }

  VPE_CTRL_AFTER_EXIT    = WM_USER + 2320;  { Control has lost the focus [Interactive Edition only]
                                              Possible return codes: your application should return zero when processing this message }

  VPE_CTRL_AFTER_CHANGE  = WM_USER + 2321;  { Control changed its content [Interactive Edition only]
                                              Possible return codes: your application should return zero when processing this message }

  VPE_FIELD_AFTER_CHANGE = WM_USER + 2322;  { the user changed a Field's value by changing the value of the 
                                              associated control [Interactive Edition only]
                                              wParam = Field Handle
                                              lParam = Document Handle
                                              Possible return codes: your application should return zero when processing this message }

  VPE_BEFORE_OPEN_FILE = WM_USER + 2323;    { the user clicked the Open button in the toolbar - lParam = Doc-Handle
                                              Possible return codes: 0 = continue, 1 = cancel, i.e. do not show the open dialog
                                              Cancelling the operation allows you to display your own open dialog and to import or
                                              create your own document, for example from a database via a memory stream. }

  VPE_AFTER_OPEN_FILE = WM_USER + 2324;     { the file open operation has been completed - wParam = VERR_xyz, lParam = Doc-Handle
                                              Possible return codes: your application should return zero when processing this message }

  VPE_BEFORE_SAVE_FILE = WM_USER + 2325;    { the user clicked the Save button in the toolbar - lParam = Doc-Handle
                                              Possible return codes: 0 = continue, 1 = cancel, i.e. do not show the save dialog
                                              Cancelling the operation allows you to display your own save dialog and to export
                                              your own document, for example to a memory stream and from there to a database. }

  VPE_AFTER_SAVE_FILE = WM_USER + 2326;     { the file save operation has been completed - wParam = VERR_xyz, lParam = Doc-Handle
                                              Possible return codes: your application should return zero when processing this message }



  { WPARAM of message VPE_PRINT: }
  { ============================ }
  PRINT_MSG_ABORT      = 0;  { User aborted }
  PRINT_MSG_START      = 1;  { Print started }
  PRINT_MSG_END        = 2;  { Print ended }
  PRINT_MSG_SETUPABORT = 3;  { User aborted Setup-Dialog }
  PRINT_MSG_SETUPSTART = 4;  { Setup-Dialog started }
  PRINT_MSG_SETUPEND   = 5;  { Setup-Dialog ended }


  { Return codes for VPE_PRINT_xyz messages: }
  { ======================================== }
  PRINT_ACTION_OK     = 0;   { Engine shall continue its work (for all VPE_PRINT_xyz messages the same) }
  PRINT_ACTION_ABORT  = 1;   { for message VPE_PRINT with Action=PRINT_MSG_SETUPSTART: }
                             { Engine shall not start printing }
  PRINT_ACTION_CHANGE = 1;   { for message VPE_PRINT_NEWPAGE and VPE_PRINT_DEVDATA: }
                             { Engine shall use changed Device-Properties }




  { ======================================================================== }
  { Barcodes:                                                                }
  { ======================================================================== }
  BCP_BOTTOM             = 0;  { Barcode Text at Bottom }
  BCP_TOP                = 1;  { Barcode Text at Top }
  BCP_HIDE               = 2;  { Barcode Text Hidden }

  BCT_EAN13              = 1;
  BCT_EAN8               = 2;
  BCT_UPCA               = 3;
  BCT_CODABAR            = 5;
  BCT_CODE39             = 6;
  BCT_2OF5               = 7;
  BCT_INTERLEAVED2OF5    = 8;
  BCT_UPCE               = 9;
  BCT_EAN13_2            = 10;
  BCT_EAN13_5            = 11;
  BCT_EAN8_2             = 12;
  BCT_EAN8_5             = 13;
  BCT_UPCA_2             = 14;
  BCT_UPCA_5             = 15;
  BCT_UPCE_2             = 16;
  BCT_UPCE_5             = 17;
  BCT_EAN128A            = 18;
  BCT_EAN128B            = 19;
  BCT_EAN128C            = 20;
  BCT_CODE93             = 21;
  BCT_POSTNET            = 22;
  BCT_CODE128A           = 23;  { obsolete, use CODE128 }
  BCT_CODE128B           = 24;  { obsolete, use CODE128 }
  BCT_CODE128C           = 25;  { obsolete, use CODE128 }
  BCT_CODE128            = 26;  { set of CODE 128A, B and C, Checkdigit required }
  BCT_EAN128             = 27;  { set of  EAN 128A, B and C, Checkdigit required }
  BCT_EAN2               = 28;  { no Checkdigit }
  BCT_EAN5               = 29;  { no Checkdigit }
  BCT_CODE39EXT          = 30;  { Code 3 of 9 Extended, Checkdigit optional, uThick / uThin }
  BCT_CODE93EXT          = 31;  { Code 93 Extended, Checkdigit optional }
  BCT_RM4SCC             = 32;  { Royal Mail 4 State Customer Code, Checkdigit required }
  BCT_MSI                = 33;  { Checkdigit optional, uThick / uThin }
  BCT_ISBN               = 34;  { International Standard Book Number, Checkdigit required }
  BCT_ISBN_5             = 35;  { ISBN + EAN5 (for pricing), Checkdigit required }
  BCT_IDENTCODE          = 36;  { Identcode of the Deutsche Post AG, Checkdigit required }
  BCT_LEITCODE           = 37;  { Leitcode of the Deutsche Post AG, Checkdigit required }
  BCT_PZN                = 38;  { Pharma Zentral Code, Checkdigit required, uThick / uThin }
  BCT_CODE11             = 39;  { Code 11, Checkdigit optional, uThick / uThin }
  BCT_2OF5MATRIX         = 40;  { 2 of 5 Matrix, Checkdigit optional, uThick / uThin }
  BCT_TELEPENA           = 41;  { Telepen-A, Checkdigit optional }
  BCT_INTELLIGENT_MAIL   = 42;  { Intelligent Mail }


  { ======================================================================== }
  { Positioning Codes:                                                       }
  { ======================================================================== }
  VFREE           = -2147483549;
  VLEFT           = -2147483550;
  VRIGHT          = -2147483551;
  VLEFTMARGIN     = -2147483552;
  VRIGHTMARGIN    = -2147483553;
  VTOP            = -2147483554;
  VBOTTOM         = -2147483555;
  VTOPMARGIN      = -2147483556;
  VBOTTOMMARGIN   = -2147483557;

  VWIDTH          = -100;    { only usable for VpeGet() }
  VHEIGHT         = -101;    { only usable for VpeGet() }
  VRENDERWIDTH    = -102;    { only usable for VpeGet() / VpeSet() }
  VRENDERHEIGHT   = -103;    { only usable for VpeGet() / VpeSet() }

  VUDO_LEFT       = -104;    { only usable for VpeGet() }
  VUDO_RIGHT      = -105;    { only usable for VpeGet() }
  VUDO_TOP        = -106;    { only usable for VpeGet() }
  VUDO_BOTTOM     = -107;    { only usable for VpeGet() }
  VUDO_WIDTH      = -108;    { only usable for VpeGet() }
  VUDO_HEIGHT     = -109;    { only usable for VpeGet() }


  { ========================================================================== }
  { Return Codes of Rendering Functions for Text and RTF:                      }
  { ========================================================================== }
  RENDER_NO_BREAK   = 0;   { NO Auto Break will occur }
  RENDER_BREAK      = 1;   { Auto Break will occur }
  RENDER_SKIP_BREAK = 2;   { Auto Break will occur, but no text will be placed on the actual
                             page, all text will be skipped to the next page
                             (VRENDERWIDTH and VRENDERHEIGHT are not set) }



  { ========================================================================== }
  { Return Codes for VpeIsMAPIInstalled:                                       }
  { ========================================================================== }
   VMAPI_NOT_INSTALLED = 0;   { MAPI not installed }
   VMAPI_INSTALLED     = 1;   { MAPI installed }
   VMAPI_UNSURE        = 2;   { unsure }



  { ========================================================================== }
  { Error-Codes returned by VpeLastError()                                     }
  { Note, that only functions setting the error state will also clear it, if   }
  { no error occured! All other functions will keep the error state untouched. }
  { ========================================================================== }
   VERR_OK              = 0;    { no error }
   VERR_COMMON          = 1;    { common error }
   
   VERR_MEMORY          = 100;  { out of memory }

   VERR_FILE_OPEN       = 200;  { error opening a file; occurs when calling functions like
                                  OpenDocFile(), VpeReadDoc(), VpeWriteDoc(), ReadPrinterSetup(),
                                  Picture(), WriteRTFFile(), etc.
                                  Meaning: The specified file could not be opened, it is either not
                                  existing, the path is not existing, it is locked, incorrect
                                  logical structure (for example corrupted file) }
   VERR_FILE_DOCVERSION = 201;  { Document file has the wrong (higher) version and cannot be opened / read }
   VERR_FILE_CREATE     = 202;  { error creating file; occurs when calling functions like
                                  WriteDoc(), OpenDocFile(), WritePrinterSetup(),
                                  SetupPrinter() (only during write), etc.
                                  Meaning: Illegal path / filename, file locked, disk full }
   VERR_FILE_ACCESS     = 203;  { Access denied (no permission); see also VpeSetDocFileReadOnly() }
   VERR_FILE_READ       = 204;  { error during file read operation }
   VERR_FILE_WRITE      = 205;  { error during file write operation }
   
   VERR_PIC_IMPORT      = 300;  { image could not be imported
                                  Meaning: File or resource not found, file not accessible,
                                  or image structure unreadable / corrupted, or not enough memory }
   VERR_PIC_NOLICENSE   = 301;  { No license for image access
                                  (e.g. TIFF / GIF image is LZW compressed
                                  and flag PIC_ALLOWLZW not used) }
   VERR_PIC_DXFCOORD    = 302;  { For DXF formats, x2 and y2 may not be VFREE at the same time,
                                  either x2 or y2 must be <> VFREE }
   
   VERR_PIC_EXPORT      = 350;  { image could not be exported
                                  Meaning: File could not be created, or not enough memory }

   VERR_MOD_GRAPH_IMP   = 400;  { Error loading Graphics Import Library (Dav3_16.DLL / Dav3_32.DLL) }
   VERR_MOD_GRAPH_PROC  = 401;  { Error loading Graphics Processing Library (Leon3_16.DLL / Leon3_32.DLL) }
   VERR_MOD_BARCODE     = 402;  { Error loading Barcode Library (Easybar.DLL / Ezbar32.DLL) }
   VERR_MOD_CHART       = 403;  { Error loading Chart Library (VChart16.DLL / VChart32.DLL) }
   VERR_MOD_ZLIB		= 404;  { Error loading ZLIB Library (VZLIB32.DLL) }
   VERR_MOD_VPDF        = 405;  { Error loading PDF Export Library (VPDF32.DLL) }
   VERR_MOD_BAR2D       = 406;  { Error loading 2D-Barcode Library (VBAR2D32.DLL) }

   VERR_MAIL_LOAD_MAPI                  = 450;   { Could not load MAPI }
   VERR_MAIL_CREATE                     = 451;   { Could not create temporary file }
   VERR_MAIL_USER_ABORT                 = 452;
   VERR_MAIL_FAILURE                    = 453;
   VERR_MAIL_LOGON_FAILURE              = 454;
   VERR_MAIL_DISK_FULL                  = 455;
   VERR_MAIL_INSUFFICIENT_MEMORY        = 456;
   VERR_MAIL_ACCESS_DENIED              = 457;
   VERR_MAIL_RESERVED                   = 458;
   VERR_MAIL_TOO_MANY_SESSIONS          = 459;
   VERR_MAIL_TOO_MANY_FILES             = 460;
   VERR_MAIL_TOO_MANY_RECIPIENTS        = 461;
   VERR_MAIL_ATTACHMENT_NOT_FOUND       = 462;
   VERR_MAIL_ATTACHMENT_OPEN_FAILURE    = 463;
   VERR_MAIL_ATTACHMENT_WRITE_FAILURE   = 464;
   VERR_MAIL_UNKNOWN_RECIPIENT          = 465;
   VERR_MAIL_BAD_RECIPTYPE              = 466;
   VERR_MAIL_NO_MESSAGES                = 467;
   VERR_MAIL_INVALID_MESSAGE            = 468;
   VERR_MAIL_TEXT_TOO_LARGE             = 469;
   VERR_MAIL_INVALID_SESSION            = 470;
   VERR_MAIL_TYPE_NOT_SUPPORTED         = 471;
   VERR_MAIL_AMBIGUOUS_RECIPIENT        = 472;
   VERR_MAIL_MESSAGE_IN_USE             = 473;
   VERR_MAIL_NETWORK_FAILURE            = 474;
   VERR_MAIL_INVALID_EDITFIELDS         = 475;
   VERR_MAIL_INVALID_RECIPS             = 476;
   VERR_MAIL_NOT_SUPPORTED              = 477;
   
   VERR_ZLIB_STREAM                     = 500;    { Stream Inconsistent }
   VERR_ZLIB_DATA                       = 501;    { Data Corrupt }
   VERR_ZLIB_BUFFER                     = 502;    { Internal Buffer Error }
   VERR_ZLIB_VERSION                    = 503;    { Wrong Version of VZLIB32.DLL }

   VERR_VBAR2D_FORMAT_OUT_OF_RANGE      = 550;
   VERR_VBAR2D_UNDEFINED_ID             = 551;
   VERR_VBAR2D_FORMAT_TOO_LONG          = 552;
   VERR_VBAR2D_FORMAT_OUT_OF_MEMORY     = 553;
   VERR_VBAR2D_FORMAT_DATA_INVALID      = 554;
   VERR_VBAR2D_FORMAT_NOT_ALLOWED       = 555;
   VERR_VBAR2D_DATA_WRONG_LENGTH        = 556;
   VERR_VBAR2D_DATA_ZERO_LENGTH         = 557;
   VERR_VBAR2D_DATA_TOO_SHORT           = 558;
   VERR_VBAR2D_DATA_TOO_LONG            = 559;
   VERR_VBAR2D_INVALID_DATA             = 560;
   VERR_VBAR2D_SQUARE_EDGE_TOO_SMALL    = 561;
   VERR_VBAR2D_SQUARE_TOO_LARGE         = 562;
   VERR_VBAR2D_EDGE_OVER_FORCED         = 563;
   VERR_VBAR2D_SQUARE_ASPECT_SMALL      = 564;
   VERR_VBAR2D_SQUARE_ASPECT_LARGE      = 565;
   VERR_VBAR2D_SQUARE_EVEN_ODD_MATCH    = 566;
   VERR_VBAR2D_INVALID_EDGE             = 567;
   VERR_VBAR2D_SQUARE_EDGE_TOO_LARGE    = 568;
   VERR_VBAR2D_INVALID_ECC              = 569;
   VERR_VBAR2D_INVALID_BORDER           = 570;
   VERR_VBAR2D_SELF_TEST_FAILED         = 571;

   VERR_RTF_BRACES   = 1000;   { RTF: unbalanced braces }
   VERR_RTF_OVERFLOW = 1001;   { RTF: only 16-bit version; generated internal structure > 64 KB }
   VERR_RTF_FONTTBL  = 1002;   { RTF: error parsing font table }
   VERR_RTF_COLORTBL = 1003;   { RTF: error parsing color table }

   VERR_TPL_OWNERSHIP           = 2000;   { Tpl: tried to dump the template to a foreign VPE document (where the template was not loaded into). }
   VERR_TPL_PAGE_ALREADY_DUMPED = 2001;   { Tpl: the page contains interactive objects and already had been dumped.
                                                 A page containing interactive objects may only be dumped once. }
   VERR_TPL_AUTHENTICATION      = 2002;   { Tpl: the template has an authentication key and it has not been validated successfully }


  { ======================================================================== }
  { Flags for Device-Functions                                               }
  { ======================================================================== }
{$ifndef BCB}
  VORIENT_PORTRAIT  = 1;
  VORIENT_LANDSCAPE = 2;
{$endif}

  VRES_DRAFT        = -1;
  VRES_LOW          = -2;
  VRES_MEDIUM       = -3;
  VRES_HIGH         = -4;

  VCOLOR_MONOCHROME = 1;
  VCOLOR_COLOR      = 2;

  VDUP_SIMPLEX      = 1;
  VDUP_VERTICAL     = 2;
  VDUP_HORIZONTAL   = 3;

  VTT_BITMAP        = 1;
  VTT_DOWNLOAD      = 2;
  VTT_SUBDEV        = 3;

{$ifndef BCB}
  VPAPER_A4                  = -1;      { A4 Sheet, 210- by 297-millimeters }
  VPAPER_LETTER              = -2;      { US Letter, 8 1/2- by 11-inches }
  VPAPER_LEGAL               = -3;      { Legal, 8 1/2- by 14-inches }
  VPAPER_CSHEET              = -4;      { C Sheet, 17- by 22-inches }
  VPAPER_DSHEET              = -5;      { D Sheet, 22- by 34-inches }
  VPAPER_ESHEET              = -6;      { E Sheet, 34- by 44-inches }
  VPAPER_LETTERSMALL         = -7;      { Letter Small, 8 1/2- by 11-inches }
  VPAPER_TABLOID             = -8;      { Tabloid, 11- by 17-inches }
  VPAPER_LEDGER              = -9;      { Ledger, 17- by 11-inches }
  VPAPER_STATEMENT           = -10;     { Statement, 5 1/2- by 8 1/2-inches }
  VPAPER_EXECUTIVE           = -11;     { Executive, 7 1/4- by 10 1/2-inches }
  VPAPER_A3                  = -12;     { A3 sheet, 297- by 420-millimeters }
  VPAPER_A4SMALL             = -13;     { A4 small sheet, 210- by 297-millimeters }
  VPAPER_A5                  = -14;     { A5 sheet, 148- by 210-millimeters }
  VPAPER_B4                  = -15;     { B4 sheet, 250- by 354-millimeters }
  VPAPER_B5                  = -16;     { B5 sheet, 182- by 257-millimeter paper }
  VPAPER_FOLIO               = -17;     { Folio, 8 1/2- by 13-inch paper }
  VPAPER_QUARTO              = -18;     { Quarto, 215- by 275-millimeter paper }
  VPAPER_10X14               = -19;     { 10- by 14-inch sheet }
  VPAPER_11X17               = -20;     { 11- by 17-inch sheet }
  VPAPER_NOTE                = -21;     { Note, 8 1/2- by 11-inches }
  VPAPER_ENV_9               = -22;     { #9 Envelope, 3 7/8- by 8 7/8-inches }
  VPAPER_ENV_10              = -23;     { #10 Envelope, 4 1/8- by 9 1/2-inches }
  VPAPER_ENV_11              = -24;     { #11 Envelope, 4 1/2- by 10 3/8-inches }
  VPAPER_ENV_12              = -25;     { #12 Envelope, 4 3/4- by 11-inches }
  VPAPER_ENV_14              = -26;     { #14 Envelope, 5- by 11 1/2-inches }
  VPAPER_ENV_DL              = -27;     { DL Envelope, 110- by 220-millimeters }
  VPAPER_ENV_C5              = -28;     { C5 Envelope, 162- by 229-millimeters }
  VPAPER_ENV_C3              = -29;     { C3 Envelope,  324- by 458-millimeters }
  VPAPER_ENV_C4              = -30;     { C4 Envelope,  229- by 324-millimeters }
  VPAPER_ENV_C6              = -31;     { C6 Envelope,  114- by 162-millimeters }
  VPAPER_ENV_C65             = -32;     { C65 Envelope, 114- by 229-millimeters }
  VPAPER_ENV_B4              = -33;     { B4 Envelope,  250- by 353-millimeters }
  VPAPER_ENV_B5              = -34;     { B5 Envelope,  176- by 250-millimeters }
  VPAPER_ENV_B6              = -35;     { B6 Envelope,  176- by 125-millimeters }
  VPAPER_ENV_ITALY           = -36;     { Italy Envelope, 110- by 230-millimeters }
  VPAPER_ENV_MONARCH         = -37;     { Monarch Envelope, 3 7/8- by 7 1/2-inches }
  VPAPER_ENV_PERSONAL        = -38;     { 6 3/4 Envelope, 3 5/8- by 6 1/2-inches }
  VPAPER_FANFOLD_US          = -39;     { US Std Fanfold, 14 7/8- by 11-inches }
  VPAPER_FANFOLD_STD_GERMAN  = -40;     { German Std Fanfold, 8 1/2- by 12-inches }
  VPAPER_FANFOLD_LGL_GERMAN  = -41;     { German Legal Fanfold, 8 1/2- by 13-inches }
  VPAPER_USER_DEFINED        =   0;     { User-Defined }
  vDIN_A_4                   =  -1;     { for compatibility with older versions }
  vUS_LETTER                 =  -2;     { for compatibility with older versions }

{ #if(WINVER >= 0x0400) }
        VPAPER_ISO_B4              = -42;  { B4 (ISO) 250 x 353 mm              }
        VPAPER_JAPANESE_POSTCARD   = -43;  { Japanese Postcard 100 x 148 mm     }
        VPAPER_9X11                = -44;  { 9 x 11 in                          }
        VPAPER_10X11               = -45;  { 10 x 11 in                         }
        VPAPER_15X11               = -46;  { 15 x 11 in                         }
        VPAPER_ENV_INVITE          = -47;  { Envelope Invite 220 x 220 mm       }
        VPAPER_RESERVED_48         = -48;  { RESERVED= -= -DO NOT USE               }
        VPAPER_RESERVED_49         = -49;  { RESERVED= -= -DO NOT USE               }
        VPAPER_LETTER_EXTRA        = -50;  { Letter Extra 9 \275 x 12 in        }
        VPAPER_LEGAL_EXTRA         = -51;  { Legal Extra 9 \275 x 15 in         }
        VPAPER_TABLOID_EXTRA       = -52;  { Tabloid Extra 11.69 x 18 in        }
        VPAPER_A4_EXTRA            = -53;  { A4 Extra 9.27 x 12.69 in           }
        VPAPER_LETTER_TRANSVERSE   = -54;  { Letter Transverse 8 \275 x 11 in   }
        VPAPER_A4_TRANSVERSE       = -55;  { A4 Transverse 210 x 297 mm         }
        VPAPER_LETTER_EXTRA_TRANSVERSE = -56; { Letter Extra Transverse 9\275 x 12 in }
        VPAPER_A_PLUS              = -57;  { SuperA/SuperA/A4 227 x 356 mm      }
        VPAPER_B_PLUS              = -58;  { SuperB/SuperB/A3 305 x 487 mm      }
        VPAPER_LETTER_PLUS         = -59;  { Letter Plus 8.5 x 12.69 in         }
        VPAPER_A4_PLUS             = -60;  { A4 Plus 210 x 330 mm               }
        VPAPER_A5_TRANSVERSE       = -61;  { A5 Transverse 148 x 210 mm         }
        VPAPER_B5_TRANSVERSE       = -62;  { B5 (JIS) Transverse 182 x 257 mm   }
        VPAPER_A3_EXTRA            = -63;  { A3 Extra 322 x 445 mm              }
        VPAPER_A5_EXTRA            = -64;  { A5 Extra 174 x 235 mm              }
        VPAPER_B5_EXTRA            = -65;  { B5 (ISO) Extra 201 x 276 mm        }
        VPAPER_A2                  = -66;  { A2 420 x 594 mm                    }
        VPAPER_A3_TRANSVERSE       = -67;  { A3 Transverse 297 x 420 mm         }
        VPAPER_A3_EXTRA_TRANSVERSE = -68;  { A3 Extra Transverse 322 x 445 mm   }

{ #if(WINVER >= 0x0500) }
        VPAPER_DBL_JAPANESE_POSTCARD = -69; { Japanese Double Postcard 200 x 148 mm }
        VPAPER_A6                  = -70;  { A6 105 x 148 mm                 }
        VPAPER_JENV_KAKU2          = -71;  { Japanese Envelope Kaku #2       }
        VPAPER_JENV_KAKU3          = -72;  { Japanese Envelope Kaku #3       }
        VPAPER_JENV_CHOU3          = -73;  { Japanese Envelope Chou #3       }
        VPAPER_JENV_CHOU4          = -74;  { Japanese Envelope Chou #4       }
        VPAPER_LETTER_ROTATED      = -75;  { Letter Rotated 11 x 8 1/2 in    }
        VPAPER_A3_ROTATED          = -76;  { A3 Rotated 420 x 297 mm         }
        VPAPER_A4_ROTATED          = -77;  { A4 Rotated 297 x 210 mm         }
        VPAPER_A5_ROTATED          = -78;  { A5 Rotated 210 x 148 mm         }
        VPAPER_B4_JIS_ROTATED      = -79;  { B4 (JIS) Rotated 364 x 257 mm   }
        VPAPER_B5_JIS_ROTATED      = -80;  { B5 (JIS) Rotated 257 x 182 mm   }
        VPAPER_JAPANESE_POSTCARD_ROTATED = -81; { Japanese Postcard Rotated 148 x 100 mm }
        VPAPER_DBL_JAPANESE_POSTCARD_ROTATED = -82; { Double Japanese Postcard Rotated 148 x 200 mm }
        VPAPER_A6_ROTATED          = -83;  { A6 Rotated 148 x 105 mm         }
        VPAPER_JENV_KAKU2_ROTATED  = -84;  { Japanese Envelope Kaku #2 Rotated }
        VPAPER_JENV_KAKU3_ROTATED  = -85;  { Japanese Envelope Kaku #3 Rotated }
        VPAPER_JENV_CHOU3_ROTATED  = -86;  { Japanese Envelope Chou #3 Rotated }
        VPAPER_JENV_CHOU4_ROTATED  = -87;  { Japanese Envelope Chou #4 Rotated }
        VPAPER_B6_JIS              = -88;  { B6 (JIS) 128 x 182 mm           }
        VPAPER_B6_JIS_ROTATED      = -89;  { B6 (JIS) Rotated 182 x 128 mm   }
        VPAPER_12X11               = -90;  { 12 x 11 in                      }
        VPAPER_JENV_YOU4           = -91;  { Japanese Envelope You #4        }
        VPAPER_JENV_YOU4_ROTATED   = -92;  { Japanese Envelope You #4 Rotated}
        VPAPER_P16K                = -93;  { PRC 16K 146 x 215 mm            }
        VPAPER_P32K                = -94;  { PRC 32K 97 x 151 mm             }
        VPAPER_P32KBIG             = -95;  { PRC 32K(Big) 97 x 151 mm        }
        VPAPER_PENV_1              = -96;  { PRC Envelope #1 102 x 165 mm    }
        VPAPER_PENV_2              = -97;  { PRC Envelope #2 102 x 176 mm    }
        VPAPER_PENV_3              = -98;  { PRC Envelope #3 125 x 176 mm    }
        VPAPER_PENV_4              = -99;  { PRC Envelope #4 110 x 208 mm    }
        VPAPER_PENV_5              = -100; { PRC Envelope #5 110 x 220 mm    }
        VPAPER_PENV_6              = -101; { PRC Envelope #6 120 x 230 mm    }
        VPAPER_PENV_7              = -102; { PRC Envelope #7 160 x 230 mm    }
        VPAPER_PENV_8              = -103; { PRC Envelope #8 120 x 309 mm    }
        VPAPER_PENV_9              = -104; { PRC Envelope #9 229 x 324 mm    }
        VPAPER_PENV_10             = -105; { PRC Envelope #10 324 x 458 mm   }
        VPAPER_P16K_ROTATED        = -106; { PRC 16K Rotated                 }
        VPAPER_P32K_ROTATED        = -107; { PRC 32K Rotated                 }
        VPAPER_P32KBIG_ROTATED     = -108; { PRC 32K(Big) Rotated            }
        VPAPER_PENV_1_ROTATED      = -109; { PRC Envelope #1 Rotated 165 x 102 mm }
        VPAPER_PENV_2_ROTATED      = -110; { PRC Envelope #2 Rotated 176 x 102 mm }
        VPAPER_PENV_3_ROTATED      = -111; { PRC Envelope #3 Rotated 176 x 125 mm }
        VPAPER_PENV_4_ROTATED      = -112; { PRC Envelope #4 Rotated 208 x 110 mm }
        VPAPER_PENV_5_ROTATED      = -113; { PRC Envelope #5 Rotated 220 x 110 mm }
        VPAPER_PENV_6_ROTATED      = -114; { PRC Envelope #6 Rotated 230 x 120 mm }
        VPAPER_PENV_7_ROTATED      = -115; { PRC Envelope #7 Rotated 230 x 160 mm }
        VPAPER_PENV_8_ROTATED      = -116; { PRC Envelope #8 Rotated 309 x 120 mm }
        VPAPER_PENV_9_ROTATED      = -117; { PRC Envelope #9 Rotated 324 x 229 mm }
        VPAPER_PENV_10_ROTATED     = -118; { PRC Envelope #10 Rotated 458 x 324 mm }

{$endif}



  { ========================================================================= }
  {                              Paper-Bins                                   }
  { ========================================================================= }
  VBIN_UNTOUCHED     = -1;   { default }
  VBIN_UPPER         = 1;
  VBIN_ONLYONE       = 1;
  VBIN_LOWER         = 2;
  VBIN_MIDDLE        = 3;
  VBIN_MANUAL        = 4;
  VBIN_ENVELOPE      = 5;
  VBIN_ENVMANUAL     = 6;
  VBIN_AUTO          = 7;
  VBIN_TRACTOR       = 8;
  VBIN_SMALLFMT      = 9;
  VBIN_LARGEFMT      = 10;
  VBIN_LARGECAPACITY = 11;
  VBIN_CASSETTE      = 14;



  { ========================================================================= }
  {                              Mail                                         }
  { ========================================================================= }
  VMAIL_ORIG         = 0;           { Recipient is message originator }
  VMAIL_TO           = 1;           { Recipient is a primary recipient }
  VMAIL_CC           = 2;           { Recipient is a copy recipient }
  VMAIL_BCC          = 3;           { Recipient is blind copy recipient }
  VMAIL_RESOLVE_NAME = $80000000;   { try to resolve the name from the mail client's address book }




  { ========================================================================= }
  { Behavior of preview for page forward / backward action                    }
  { (vertical positioning of page in the preview after user                   }
  { moved a page forward / back)                                              }
  { ========================================================================= }
   PREVIEW_STAY    = 0;     { vertical position unaffected }
   PREVIEW_JUMPTOP = 1;     { vertical position on top of page (default) }



  { ========================================================================= }
  {                              Keyboard-Layout                              }
  { ========================================================================= }
  VKEY_SCROLL_LEFT         = 0;
  VKEY_SCROLL_PAGE_LEFT    = 1;
  VKEY_SCROLL_RIGHT        = 2;
  VKEY_SCROLL_PAGE_RIGHT   = 3;
  VKEY_SCROLL_UP           = 4;
  VKEY_SCROLL_PAGE_UP      = 5;
  VKEY_SCROLL_DOWN         = 6;
  VKEY_SCROLL_PAGE_DOWN    = 7;
  VKEY_SCROLL_TOP          = 8;
  VKEY_SCROLL_BOTTOM       = 9;
  VKEY_PRINT               = 10;
  VKEY_MAIL                = 11;
  VKEY_FULL_PAGE           = 12;
  VKEY_PAGE_WIDTH          = 13;
  VKEY_ZOOM_IN             = 14;
  VKEY_ZOOM_OUT            = 15;
  VKEY_GRID                = 16;
  VKEY_PAGE_FIRST          = 17;
  VKEY_PAGE_LEFT           = 18;
  VKEY_PAGE_RIGHT          = 19;
  VKEY_PAGE_LAST           = 20;
  VKEY_HELP                = 21;
  VKEY_INFO                = 22;
  VKEY_CLOSE               = 23;
  VKEY_GOTO_PAGE           = 24;
  VKEY_OPEN                = 25;
  VKEY_SAVE                = 26;
  VKEY_ESCAPE              = 27;



  { ========================================================================= }
  {                             GUI Languages                                 }
  { ========================================================================= }
  VGUI_LANGUAGE_USER_DEFINED = -1;
  VGUI_LANGUAGE_ENGLISH      =  0;
  VGUI_LANGUAGE_GERMAN       =  1;
  VGUI_LANGUAGE_FRENCH       =  2;
  VGUI_LANGUAGE_DUTCH        =  3;
  VGUI_LANGUAGE_SPANISH      =  4;
  VGUI_LANGUAGE_DANISH       =  5;
  VGUI_LANGUAGE_SWEDISH      =  6;
  VGUI_LANGUAGE_FINNISH      =  7;
  VGUI_LANGUAGE_ITALIAN      =  8;
  VGUI_LANGUAGE_NORWEGIAN    =  9;
  VGUI_LANGUAGE_PORTUGUESE   = 10;


  { ======================================================================== }
  { Colors:                                                                  }
  { ======================================================================== }

  COLOR_BLACK     = 0;
  COLOR_DKGRAY    = 8421504;
  COLOR_GRAY      = 12632256;
  COLOR_LTGRAY    = 15132390;
  COLOR_WHITE     = 16777215;

  COLOR_DKRED     = 128;
  COLOR_RED       = 192;
  COLOR_LTRED     = 255;

  COLOR_DKORANGE  = 16639;
  COLOR_ORANGE    = 33023;
  COLOR_LTORANGE  = 49407;

  COLOR_DKYELLOW  = 57568;
  COLOR_YELLOW    = 62194;

  COLOR_LTYELLOW  = 65535;

  COLOR_DKGREEN   = 32768;
  COLOR_GREEN     = 49152;
  COLOR_LTGREEN   = 65280;
  COLOR_HIGREEN   = 8453888;
  COLOR_BLUEGREEN = 8421376;
  COLOR_OLIVE     = 32896;
  COLOR_BROWN     = 20608;

  COLOR_DKBLUE    = 8388608;
  COLOR_BLUE      = 16711680;
  COLOR_LTBLUE    = 16744448;
  COLOR_LTLTBLUE  = 16752640;
  COLOR_HIBLUE    = 16760832;
  COLOR_CYAN      = 16776960;

  COLOR_DKPURPLE  = 8388736;
  COLOR_PURPLE    = 12583104;
  COLOR_MAGENTA   = 16711935;


  { ======================================================================== }
  {                         Charsets:                                        }
  { ======================================================================== }
  csAnsiCharset         = 0;
  csDefaultCharset      = 1;
  csSymbolCharset       = 2;
  csShiftjisCharset     = 128;
  csHangeulCharset      = 129;
  csGB2312Charset       = 134;
  csChineseBig5Charset  = 136;
  csOEMCharset          = 255;
 
  csJohabCharset        = 130;
  csHebrewCharset       = 177;
  csArabicCharset       = 178;
  csGreekCharset        = 161;
  csTurkishCharset      = 162;
  csThaiCharset         = 222;
  csEastEuropeCharset   = 238;
  csRussianCharset      = 204;
  csMacCharset          = 77;
  csBalticCharset       = 186;


  { ======================================================================== }
  {                         Other Definitions:                               }
  { ======================================================================== }
  CM                      = 0;       { Parameters for RulersMeasure }
  INCH                    = 1;

  { ======================================================================== }
  {                         Bookmark Destination Types                       }
  { ======================================================================== }

  VBOOKMARK_DEST_NONE     = 0;         { No Destination }
  VBOOKMARK_DEST_LTZ      = 1;         { Normal Destination (left, top, zoom are used) [default] }
  VBOOKMARK_DEST_FIT      = 2;         { Fit the page in the window (no coordinates used) }
  VBOOKMARK_DEST_FITH     = 3;         { Fit the page in the window horizontally (top coordinate used) }
  VBOOKMARK_DEST_FITV     = 4;         { Fit the page in the window vertically (left coordinate used) }
  VBOOKMARK_DEST_FITR     = 5;         { Fit the page in the specified rectangle (all  coordinates used) }
  VBOOKMARK_DEST_FITB     = 6;         { Fit the page's bounding rectangle in the window }
  VBOOKMARK_DEST_FITBH    = 7;         { Fit the page's bounding rectangle horizontally in the window (top coordinate used) }
  VBOOKMARK_DEST_FITBV    = 8;         { Fit the page's bounding rectangle vertically in the window (left coordinate used) }

  { ======================================================================== }
  {                         Bookmark Style Flags                             }
  { ======================================================================== }
  VBOOKMARK_STYLE_NONE    = 0;         { [default] }
  VBOOKMARK_STYLE_BOLD    = 1;         { (PDF 1.4) }
  VBOOKMARK_STYLE_ITALIC  = 2;         { (PDF 1.4) }
  VBOOKMARK_STYLE_OPEN    = 4;


  { ======================================================================== }
  {                       Picture Export Definitions:                        }
  { ======================================================================== }
  PICEXP_QUALITY_NORMAL   = 0;         { embedded images will have normal quality [default] }
  PICEXP_QUALITY_HIGH     = 0;         { embedded images will have high quality (using Scale2Gray, only available in VPE Professional Edition or higher) }


  { ======================================================================== }
  {                       Document Export Definitions:                       }
  { ======================================================================== }
  VPE_DOC_TYPE_AUTO        = 0;        { VpeWriteDoc() will create VPE, PDF or HTML files, depending on the file suffix [default] }
  VPE_DOC_TYPE_VPE         = 1;        { VpeWriteDoc() will create VPE files, regardless of the file suffix }
  VPE_DOC_TYPE_PDF         = 2;        { VpeWriteDoc() will create PDF files, regardless of the file suffix }
  VPE_DOC_TYPE_HTML        = 3;        { VpeWriteDoc() will create HTML files, regardless of the file suffix }

  VPE_PDF_ACROBAT_4        = 1300;     { Create PDF documents compatible to Acrobat Reader 4 (PDF v1.3) [default] }
  VPE_PDF_ACROBAT_5        = 1400;     { Create PDF documents compatible to Acrobat Reader 5 (PDF v1.4) }


  { ======================================================================== }
  {                       Document Compression Types                         }
  { ======================================================================== }
  DOC_COMPRESS_NONE        = 0;        { no compression }
  DOC_COMPRESS_FLATE       = 1;        { Flate compression (also known as ZLIB Compression) [default] }


  { ======================================================================== }
  {                       Document Encryption Types                          }
  { ======================================================================== }
  DOC_ENCRYPT_NONE         = 0;        { no encryption [default] }
  DOC_ENCRYPT_STREAM       = 1;        { use stream encryption (compatible to PDF 1.3 and higher) }


  { ======================================================================== }
  {       Document access flags, only in effect, when using encryption.      }
  {  Any combination of these flags can be used by just adding their values  }
  { ======================================================================== }
  PDF_ALLOW_NONE       = 0;     { full protection, no flag is raised }

  PDF_ALLOW_PRINT      = 4;     { allow to print the document (see also PDF_ALLOW_HIQ_PRINT) }

  PDF_ALLOW_MODIFY     = 8;     { allow to modify the contents of the document by operations other than }
                                { those controlled by the flags PDF_ALLOW_TA_IFF, PDF_ALLOW_FILL_IFF    }
                                { and PDF_ALLOW_ASSEMBLE }

  PDF_ALLOW_COPY       = 16;    { PDF v1.3: allow to copy or otherwise extract text and graphics from the }
                                {           document, including extracting text and graphics (in support }
                                {           of accessibility to disabled users or for other purposes) }
                                { PDF v1.4: allow to copy or otherwise extract text and graphics from the }
                                {           document by operations other than that controlled by }
                                {           PDF_ALLOW_EXTRACT }

  PDF_ALLOW_TA_IFF     = 32;    { allow to add or modify text annotations, fill in interactive form fields, }
                                { and - if PDF_ALLOW_MODIFY is also used - create or modify interactive form }
                                { fields (including signature fields) }

  PDF_ALLOW_FILL_IFF   = 256;   { PDF v1.4 only: allow to fill in existing interactive form fields }
                                { (including signature fields), even if PDF_ALLOW_TA_IFF is not used }

  PDF_ALLOW_EXTRACT    = 512;   { PDF v1.4 only: allow to extract text and graphics (in support of }
                                { accessibility to disabled users or for other purposes) }

  PDF_ALLOW_ASSEMBLE   = 1024;  { PDF v1.4 only: allow to assemble the document (insert, rotate, or delete }
                                { pages and create bookmarks or thumbnail images), even if PDF_ALLOW_MODIFY }
                                { is not used }

  PDF_ALLOW_HIQ_PRINT  = 2048;  { PDF v1.4 only: allow to print the document to a representation from }
                                { which a faithful digital copy of the PDF content could be generated. }
                                { When this flag is NOT specified (and PDF_ALLOW_PRINT is used), printing }
                                { is limited to a lowlevel representation of the appearance, possibly }
                                { of degraded quality. }

  PDF_ALLOW_ALL        = 3900;  { no protection, all flags are raised }


  { ======================================================================== }
  { Options for VpeSetPictureExportColorDepth():                             }
  { ======================================================================== }
  PICEXP_COLOR_MONO       = 1;
  PICEXP_COLOR_16         = 4;
  PICEXP_COLOR_256        = 8;       { default }
  PICEXP_COLOR_HI         = 16;
  PICEXP_COLOR_TRUE       = 24;


  { ======================================================================== }
  { Options for VpeSetPictureExportDither():                                 }
  { ======================================================================== }
  PICEXP_DITHER_NONE      = 0;       { default }
  PICEXP_DITHER_MONO      = 1;
  PICEXP_DITHER_16        = 2;
  PICEXP_DITHER_256       = 3;
  PICEXP_DITHER_256_WU    = 4;



  { ======================================================================== }
  { Chart Types:                                                             }
  { ======================================================================== }
  VCHART_POINT                    = 0;
  VCHART_LINE                     = 1;
  VCHART_BAR                      = 2;
  VCHART_STACKED_BAR_ABSOLUTE     = 3;
  VCHART_STACKED_BAR_PERCENT      = 4;
  VCHART_3D_BAR                   = 5;
  VCHART_3D_STACKED_BAR_ABSOLUTE  = 6;
  VCHART_3D_STACKED_BAR_PERCENT   = 7;
  VCHART_PIE                      = 8;
  VCHART_3D_PIE                   = 9;
  VCHART_AREA_ABSOLUTE            = 10;
  VCHART_AREA_PERCENT             = 11;


  { ======================================================================== }
  { Symbol Types (used in Point-Chart):                                      }
  { ======================================================================== }
  VCHART_SYMBOL_NONE            = -1;
  VCHART_SYMBOL_SQUARE          = 0;
  VCHART_SYMBOL_TRIANGLE        = 1;
  VCHART_SYMBOL_CIRCLE          = 2;
  VCHART_SYMBOL_CROSS           = 3;
  VCHART_SYMBOL_X               = 4;
  VCHART_SYMBOL_POINT           = 5;


  { ======================================================================== }
  { Legend Positions:                                                        }
  { ======================================================================== }
  VCHART_LEGENDPOS_NONE            = -1;
  VCHART_LEGENDPOS_RIGHT           = 0;
  VCHART_LEGENDPOS_RIGHT_TOP       = 1;
  VCHART_LEGENDPOS_RIGHT_BOTTOM    = 2;
  VCHART_LEGENDPOS_LEFT            = 3;
  VCHART_LEGENDPOS_LEFT_TOP        = 4;
  VCHART_LEGENDPOS_LEFT_BOTTOM     = 5;
  VCHART_LEGENDPOS_TOP             = 6;
  VCHART_LEGENDPOS_BOTTOM          = 7;


  { ======================================================================== }
  { Label States:                                                            }
  { ======================================================================== }
  VCHART_LABEL_NONE   = -1;        { No labels are drawn }
  VCHART_LABEL_USER   = 0;         { User defined labels are drawn }
  VCHART_LABEL_AUTO   = 1;         { Labeling is done automatically }


  { ======================================================================== }
  { Pie Label Types:                                                         }
  { ======================================================================== }
  VCHART_PIE_LABEL_NONE        = 0; { no labels are drawn }
  VCHART_PIE_LABEL_PERCENTAGE  = 1; { the percentage values are drawn }
  VCHART_PIE_LABEL_LEGEND      = 2; { the legend texts are used for the labels }
  VCHART_PIE_LABEL_XLABELS     = 3; { the user-defined x-labels are used }


  { ======================================================================== }
  { Grid Types:                                                              }
  { ======================================================================== }
  VCHART_GRID_NONE         = -1;    { No Grid is drawn }
  VCHART_GRID_BOTH_AXIS     = 0;    { Grid is drawn for both axis }
  VCHART_GRID_X_AXIS        = 1;    { Grid is only drawn for the x-axis }
  VCHART_GRID_Y_AXIS        = 2;    { Grid is only drawn for the y-axis }


	{ ======================================================================== }
	{ VPE Enum Symbols                                                         }
	{ ----------------                                                         }
	{                                                                          }
	{ Generated by mkconst v1.0                                                }
	{ DO NOT MODIFY!                                                           }
	{ ======================================================================== }

	VCHARSET_DEFAULT = 1;
	VCHARSET_SYMBOL = 2;
	VCHARSET_MAC_ROMAN = 77;
	VCHARSET_WIN_ANSI = 0;
	VCHARSET_WIN_HEBREW = 177;
	VCHARSET_WIN_ARABIC = 178;
	VCHARSET_WIN_GREEK = 161;
	VCHARSET_WIN_TURKISH = 162;
	VCHARSET_WIN_VIETNAMESE = 163;
	VCHARSET_WIN_THAI = 222;
	VCHARSET_WIN_EAST_EUROPE = 238;
	VCHARSET_WIN_CYRILLIC = 204;
	VCHARSET_WIN_BALTIC = 186;
	VCHARSET_ISO_LATIN_1 = 50;
	VCHARSET_ISO_LATIN_2 = 51;
	VCHARSET_ISO_LATIN_3 = 52;
	VCHARSET_ISO_LATIN_4 = 53;
	VCHARSET_ISO_CYRILLIC = 54;
	VCHARSET_ISO_ARABIC = 55;
	VCHARSET_ISO_GREEK = 56;
	VCHARSET_ISO_HEBREW = 57;
	VCHARSET_ISO_LATIN_5 = 58;
	VCHARSET_ISO_LATIN_6 = 59;
	VCHARSET_ISO_THAI = 60;
	VCHARSET_ISO_LATIN_7 = 62;
	VCHARSET_ISO_LATIN_8 = 63;
	VCHARSET_ISO_LATIN_9 = 64;
	VRSCID_COPY_OF = 0;
	VRSCID_PAGE_OF = 1;
	VRSCID_BAND = 2;
	VRSCID_ERROR = 3;
	VRSCID_WARNING = 4;
	VRSCID_PRINTING = 5;
	VRSCID_CANCEL = 6;
	VRSCID_OK = 7;
	VRSCID_PRINT_DOC = 8;
	VRSCID_FULL_PAGE = 9;
	VRSCID_PAGE_WIDTH = 10;
	VRSCID_ZOOM_IN = 11;
	VRSCID_ZOOM_OUT = 12;
	VRSCID_FIRST_PAGE = 13;
	VRSCID_PREV_PAGE = 14;
	VRSCID_NEXT_PAGE = 15;
	VRSCID_LAST_PAGE = 16;
	VRSCID_HELP = 17;
	VRSCID_INFORMATION = 18;
	VRSCID_CLOSE_PREVIEW = 19;
	VRSCID_GRID = 20;
	VRSCID_ENTER_PAGENO = 21;
	VRSCID_ZOOM_FACTOR = 22;
	VRSCID_STATUS = 23;
	VRSCID_READY = 24;
	VRSCID_EMAIL = 25;
	VRSCID_OPEN = 26;
	VRSCID_SAVE = 27;
	VRSCID_ERROR_OPEN = 28;
	VRSCID_ERROR_WRITE = 29;
	VRSCID_USAGE = 30;
	VRSCID_MOUSE = 31;
	VRSCID_USAGE_MOUSE = 32;
	VRSCID_KEYBOARD = 33;
	VRSCID_USAGE_KEYBOARD = 34;
	VGUI_THEME_OFFICE2000 = 0;
	VGUI_THEME_OFFICE2003 = 1;
	VGUI_THEME_WHIDBEY = 2;
	VSCALE_MODE_FULL_PAGE = 0;
	VSCALE_MODE_PAGE_WIDTH = 1;
	VSCALE_MODE_ZOOM_TOOL = 2;
	VSCALE_MODE_FREE = 3;
	VENGINE_RENDER_MODE_VER3 = 0;
	VENGINE_RENDER_MODE_VER4 = 1;
	VUNIT_FACTOR_CM = 100000;
	VUNIT_FACTOR_INCH = 254000;
	VUNIT_FACTOR_MM10 = 1000;
	VMAPI_TYPE_NOT_INSTALLED = 0;
	VMAPI_TYPE_EXTENDED = 1;
	VMAPI_TYPE_SIMPLE = 2;
	PICEXP_JPEG_DEFAULT = 0;
	PICEXP_JPEG_HI_QUALITY = 128;
	PICEXP_JPEG_GOOD_QUALITY = 256;
	PICEXP_JPEG_MID_QUALITY = 512;
	PICEXP_JPEG_LO_QUALITY = 1024;
	PICEXP_JPEG_BAD_QUALITY = 2048;
	PICEXP_JPEG_PROGRESSIVE = 8192;
	PICEXP_TIFF_DEFAULT = 0;
	PICEXP_TIFF_PACKBITS = 256;
	PICEXP_TIFF_DEFLATE = 512;
	PICEXP_TIFF_ADOBE_DEFLATE = 1024;
	PICEXP_TIFF_NONE = 2048;
	PICEXP_TIFF_CCITTFAX3 = 4096;
	PICEXP_TIFF_CCITTFAX4 = 8192;
	PICEXP_TIFF_LZW = 16384;
	PICEXP_TIFF_JPEG = 32768;
	PICEXP_TIFF_APPEND = 1073741824;
	PICEXP_GIF_DEFAULT = 0;
	PICEXP_GIF_APPEND = 1073741824;
	PICEXP_BMP_DEFAULT = 0;
	PICEXP_BMP_RLE = 1;
	PICEXP_PNM_DEFAULT = 0;
	PICEXP_PNM_ASCII = 1;
	VBAR2D_ALIGN_CENTER = 0;
	VBAR2D_ALIGN_CENTER_H = 0;
	VBAR2D_ALIGN_LEFT = 1;
	VBAR2D_ALIGN_RIGHT = 2;
	VBAR2D_ALIGN_CENTER_V = 0;
	VBAR2D_ALIGN_TOP = 4;
	VBAR2D_ALIGN_BOTTOM = 8;
	VBAR2D_DATA_MATRIX_ENC_BASE11 = 1;
	VBAR2D_DATA_MATRIX_ENC_BASE27 = 2;
	VBAR2D_DATA_MATRIX_ENC_BASE41 = 3;
	VBAR2D_DATA_MATRIX_ENC_BASE37 = 4;
	VBAR2D_DATA_MATRIX_ENC_ASCII = 5;
	VBAR2D_DATA_MATRIX_ENC_BINARY = 6;
	VBAR2D_DATA_MATRIX_ECC000 = 0;
	VBAR2D_DATA_MATRIX_ECC010 = 1;
	VBAR2D_DATA_MATRIX_ECC040 = 2;
	VBAR2D_DATA_MATRIX_ECC050 = 3;
	VBAR2D_DATA_MATRIX_ECC060 = 4;
	VBAR2D_DATA_MATRIX_ECC070 = 5;
	VBAR2D_DATA_MATRIX_ECC080 = 6;
	VBAR2D_DATA_MATRIX_ECC090 = 7;
	VBAR2D_DATA_MATRIX_ECC100 = 8;
	VBAR2D_DATA_MATRIX_ECC110 = 9;
	VBAR2D_DATA_MATRIX_ECC120 = 10;
	VBAR2D_DATA_MATRIX_ECC130 = 11;
	VBAR2D_DATA_MATRIX_ECC140 = 12;
	VBAR2D_DATA_MATRIX_ECC200 = 26;


var
  DLLInitialized: boolean; { VPE could be loaded? }
  DLLHandle : THandle;     { Handle of VPEx.DLL }

  { =========================== }
  { declare the function types: }
  { =========================== }
type
  TVpeLicense = procedure(hDoc: LongInt; serial1: PChar; serial2: PChar) stdcall;
  TVpeGetVersion = function: Integer stdcall;
  TVpeGetReleaseNumber = function: Integer stdcall;
  TVpeGetBuildNumber = function: Integer stdcall;
  TVpeGetEdition = function: Integer stdcall;
  TVpeOpenDoc = function(hWndParent: HWND; title: PChar; flags: LongInt): LongInt stdcall;
  TVpeOpenDocFile = function(hWndParent: HWND; file_name: PChar; title: PChar; flags: LongInt): LongInt stdcall;
  TVpeCloseDoc = function(hDoc: LongInt): Integer stdcall;
  TVpeSetPageFormat = procedure(hDoc: LongInt; page_dimension: Integer) stdcall;
  TVpeGetPageFormat = function(hDoc: LongInt): Integer stdcall;
  TVpeSetPageWidth = procedure(hDoc: LongInt; page_width: Double) stdcall;
  TVpeSetPageHeight = procedure(hDoc: LongInt; page_height: Double) stdcall;
  TVpeGetPageWidth = function(hDoc: LongInt): Double stdcall;
  TVpeGetPageHeight = function(hDoc: LongInt): Double stdcall;
  TVpeSetPageOrientation = procedure(hDoc: LongInt; orientation: Integer) stdcall;
  TVpeGetPageOrientation = function(hDoc: LongInt): Integer stdcall;
  TVpeSetPaperBin = procedure(hDoc: LongInt; bin: Integer) stdcall;
  TVpeGetPaperBin = function(hDoc: LongInt): Integer stdcall;
  TVpeSetDefOutRect = procedure(hDoc: LongInt; x: Double; y: Double; x2: Double; y2: Double) stdcall;
  TVpeSetOutRect = procedure(hDoc: LongInt; x: Double; y: Double; x2: Double; y2: Double) stdcall;
  TVpeGet = function(hDoc: LongInt; what: Double): Double stdcall;
  TVpeSet = procedure(hDoc: LongInt; what: Double; value: Double) stdcall;
  TVpeStorePos = procedure(hDoc: LongInt) stdcall;
  TVpeRestorePos = procedure(hDoc: LongInt) stdcall;
  TVpeSetPen = procedure(hDoc: LongInt; pen_size: Double; pen_style: Integer; color: TColorRef) stdcall;
  TVpeSetPenSize = procedure(hDoc: LongInt; pen_size: Double) stdcall;
  TVpeGetPenSize = function(hDoc: LongInt): Double stdcall;
  TVpePenSize = procedure(hDoc: LongInt; pen_size: Double) stdcall;
  TVpeSetPenStyle = procedure(hDoc: LongInt; pen_style: Integer) stdcall;
  TVpePenStyle = procedure(hDoc: LongInt; pen_style: Integer) stdcall;
  TVpeSetPenColor = procedure(hDoc: LongInt; color: TColorRef) stdcall;
  TVpePenColor = procedure(hDoc: LongInt; color: TColorRef) stdcall;
  TVpeNoPen = procedure(hDoc: LongInt) stdcall;
  TVpeLine = procedure(hDoc: LongInt; x: Double; y: Double; x2: Double; y2: Double) stdcall;
  TVpePolyLine = function(hDoc: LongInt; size: Integer): LongInt stdcall;
  TVpeAddPolyPoint = procedure(hDoc: LongInt; p: LongInt; x: Double; y: Double) stdcall;
  TVpePolygon = function(hDoc: LongInt; size: Integer): LongInt stdcall;
  TVpeAddPolygonPoint = procedure(hDoc: LongInt; p: LongInt; x: Double; y: Double) stdcall;
  TVpeSetBkgColor = procedure(hDoc: LongInt; color: TColorRef) stdcall;
  TVpeSetBkgGradientStartColor = procedure(hDoc: LongInt; color_start: TColorRef) stdcall;
  TVpeSetBkgGradientEndColor = procedure(hDoc: LongInt; color_end: TColorRef) stdcall;
  TVpeSetBkgGradientRotation = procedure(hDoc: LongInt; angle: Integer) stdcall;
  TVpeSetBkgGradientPrint = procedure(hDoc: LongInt; mode: Integer) stdcall;
  TVpeSetBkgGradientPrintSolidColor = procedure(hDoc: LongInt; color: TColorRef) stdcall;
  TVpeSetTransparentMode = procedure(hDoc: LongInt; on_off: Integer) stdcall;
  TVpeSetBkgMode = procedure(hDoc: LongInt; mode: Integer) stdcall;
  TVpeSetHatchStyle = procedure(hDoc: LongInt; style: Integer) stdcall;
  TVpeSetHatchColor = procedure(hDoc: LongInt; color: TColorRef) stdcall;
  TVpeBox = procedure(hDoc: LongInt; x: Double; y: Double; x2: Double; y2: Double) stdcall;
  TVpeEllipse = procedure(hDoc: LongInt; x: Double; y: Double; x2: Double; y2: Double) stdcall;
  TVpePie = procedure(hDoc: LongInt; x: Double; y: Double; x2: Double; y2: Double; begin_angle: Integer; 
      end_angle: Integer) stdcall;
  TVpeSetBarcodeParms = procedure(hDoc: LongInt; main_text: Integer; add_text: Integer) stdcall;
  TVpeSetBarcodeAlignment = procedure(hDoc: LongInt; alignment: Integer) stdcall;
  TVpeBarcode = procedure(hDoc: LongInt; x: Double; y: Double; x2: Double; y2: Double; 
      code_type: Integer; code: PChar; add_code: PChar) stdcall;
  TVpeGetPictureTypes = procedure(hDoc: LongInt; s: PChar; size: Integer) stdcall;
  TVpePicture = function(hDoc: LongInt; x: Double; y: Double; x2: Double; y2: Double; 
      file_name: PChar): Double stdcall;
  TVpeSetFont = procedure(hDoc: LongInt; name: PChar; size: Integer) stdcall;
  TVpeSelectFont = procedure(hDoc: LongInt; name: PChar; size: Integer) stdcall;
  TVpeSetFontName = procedure(hDoc: LongInt; name: PChar) stdcall;
  TVpeSetFontSize = procedure(hDoc: LongInt; size: Integer) stdcall;
  TVpeSetCharset = procedure(hDoc: LongInt; charset: LongInt) stdcall;
  TVpeSetFontAttr = procedure(hDoc: LongInt; alignment: Integer; bold: Integer; underlined: Integer; 
      italic: Integer; strikeout: Integer) stdcall;
  TVpeSetTextAlignment = procedure(hDoc: LongInt; alignment: Integer) stdcall;
  TVpeSetAlign = procedure(hDoc: LongInt; alignment: Integer) stdcall;
  TVpeSetBold = procedure(hDoc: LongInt; bold: Integer) stdcall;
  TVpeSetUnderlined = procedure(hDoc: LongInt; underlined: Integer) stdcall;
  TVpeSetStrikeOut = procedure(hDoc: LongInt; strikeout: Integer) stdcall;
  TVpeSetItalic = procedure(hDoc: LongInt; italic: Integer) stdcall;
  TVpeSetTextColor = procedure(hDoc: LongInt; color: TColorRef) stdcall;
  TVpePrint = function(hDoc: LongInt; x: Double; y: Double; s: PChar): Double stdcall;
  TVpePrintBox = function(hDoc: LongInt; x: Double; y: Double; s: PChar): Double stdcall;
  TVpeWrite = function(hDoc: LongInt; x: Double; y: Double; x2: Double; y2: Double; s: PChar): Double stdcall;
  TVpeWriteBox = function(hDoc: LongInt; x: Double; y: Double; x2: Double; y2: Double; 
      s: PChar): Double stdcall;
  TVpeDefineHeader = procedure(hDoc: LongInt; x: Double; y: Double; x2: Double; y2: Double; 
      s: PChar) stdcall;
  TVpeDefineFooter = procedure(hDoc: LongInt; x: Double; y: Double; x2: Double; y2: Double; 
      s: PChar) stdcall;
  TVpePageBreak = function(hDoc: LongInt): Integer stdcall;
  TVpeGetPageCount = function(hDoc: LongInt): Integer stdcall;
  TVpeGetCurrentPage = function(hDoc: LongInt): Integer stdcall;
  TVpeGotoPage = function(hDoc: LongInt; page: Integer): Integer stdcall;
  TVpeStoreSet = procedure(hDoc: LongInt; id: Integer) stdcall;
  TVpeUseSet = procedure(hDoc: LongInt; id: Integer) stdcall;
  TVpeRemoveSet = procedure(hDoc: LongInt; id: Integer) stdcall;
  TVpeSetAutoBreak = procedure(hDoc: LongInt; mode: Integer) stdcall;
  TVpeWriteDoc = function(hDoc: LongInt; file_name: PChar): Integer stdcall;
  TVpeReadDoc = function(hDoc: LongInt; file_name: PChar): Integer stdcall;
  TVpeSetRotation = procedure(hDoc: LongInt; angle: Integer) stdcall;
  TVpeSetPictureCacheSize = procedure(size: LongInt) stdcall;
  TVpeSetPicCacheSize = procedure(size: LongInt) stdcall;
  TVpeGetPictureCacheSize = function: LongInt stdcall;
  TVpeGetPicCacheSize = function: LongInt stdcall;
  TVpeGetPictureCacheUsed = function: LongInt stdcall;
  TVpeGetPicCacheUsed = function: LongInt stdcall;
  TVpeSetPrintOffset = procedure(hDoc: LongInt; offset_x: Double; offset_y: Double) stdcall;
  TVpeSetPrintOffsetX = procedure(hDoc: LongInt; offset_x: Double) stdcall;
  TVpeSetPrintOffsetY = procedure(hDoc: LongInt; offset_y: Double) stdcall;
  TVpeGetPrintOffsetX = function(hDoc: LongInt): Double stdcall;
  TVpeGetPrintOffsetY = function(hDoc: LongInt): Double stdcall;
  TVpeGetLastError = function(hDoc: LongInt): LongInt stdcall;
  TVpeRenderPrint = function(hDoc: LongInt; x: Double; y: Double; s: PChar): Integer stdcall;
  TVpeRenderPrintBox = function(hDoc: LongInt; x: Double; y: Double; s: PChar): Integer stdcall;
  TVpeRenderWrite = function(hDoc: LongInt; x: Double; y: Double; x2: Double; y2: Double; 
      s: PChar): Integer stdcall;
  TVpeRenderWriteBox = function(hDoc: LongInt; x: Double; y: Double; x2: Double; y2: Double; 
      s: PChar): Integer stdcall;
  TVpeRenderPicture = procedure(hDoc: LongInt; width: Double; height: Double; file_name: PChar) stdcall;
  TVpeGetPicturePageCount = function(hDoc: LongInt; file_name: PChar): LongInt stdcall;
  TVpeSetPicturePage = procedure(hDoc: LongInt; page_no: LongInt) stdcall;
  TVpeGetPicturePage = function(hDoc: LongInt): LongInt stdcall;
  TVpeSetPictureType = procedure(hDoc: LongInt; picture_type: LongInt) stdcall;
  TVpeSetDocFileReadOnly = procedure(hDoc: LongInt; yes_no: Integer) stdcall;
  TVpeGetPenStyle = function(hDoc: LongInt): Integer stdcall;
  TVpeGetPenColor = function(hDoc: LongInt): TColorRef stdcall;
  TVpeGetBkgMode = function(hDoc: LongInt): Integer stdcall;
  TVpeGetBkgColor = function(hDoc: LongInt): TColorRef stdcall;
  TVpeGetHatchStyle = function(hDoc: LongInt): Integer stdcall;
  TVpeGetHatchColor = function(hDoc: LongInt): TColorRef stdcall;
  TVpeGetBkgGradientStartColor = function(hDoc: LongInt): TColorRef stdcall;
  TVpeGetBkgGradientEndColor = function(hDoc: LongInt): TColorRef stdcall;
  TVpeGetBkgGradientRotation = function(hDoc: LongInt): Integer stdcall;
  TVpeGetFontName = procedure(hDoc: LongInt; value: PChar; size: PUINT) stdcall;
  TVpeGetFontSize = function(hDoc: LongInt): Integer stdcall;
  TVpeGetCharset = function(hDoc: LongInt): LongInt stdcall;
  TVpeGetTextAlignment = function(hDoc: LongInt): Integer stdcall;
  TVpeGetBold = function(hDoc: LongInt): Integer stdcall;
  TVpeGetUnderlined = function(hDoc: LongInt): Integer stdcall;
  TVpeGetStrikeOut = function(hDoc: LongInt): Integer stdcall;
  TVpeGetItalic = function(hDoc: LongInt): Integer stdcall;
  TVpeGetTextColor = function(hDoc: LongInt): TColorRef stdcall;
  TVpeGetRotation = function(hDoc: LongInt): Integer stdcall;
  TVpeSetBarcodeMainTextParms = procedure(hDoc: LongInt; main_text: Integer) stdcall;
  TVpeGetBarcodeMainTextParms = function(hDoc: LongInt): Integer stdcall;
  TVpeSetBarcodeAddTextParms = procedure(hDoc: LongInt; add_text: Integer) stdcall;
  TVpeGetBarcodeAddTextParms = function(hDoc: LongInt): Integer stdcall;
  TVpeGetBarcodeAlignment = function(hDoc: LongInt): Integer stdcall;
  TVpeSetBarcodeAutoChecksum = procedure(hDoc: LongInt; yes_no: Integer) stdcall;
  TVpeGetBarcodeAutoChecksum = function(hDoc: LongInt): Integer stdcall;
  TVpeSetBarcodeThinBar = procedure(hDoc: LongInt; n: Integer) stdcall;
  TVpeGetBarcodeThinBar = function(hDoc: LongInt): Integer stdcall;
  TVpeSetBarcodeThickBar = procedure(hDoc: LongInt; n: Integer) stdcall;
  TVpeGetBarcodeThickBar = function(hDoc: LongInt): Integer stdcall;
  TVpeGetPictureType = function(hDoc: LongInt): LongInt stdcall;
  TVpeGetTransparentMode = function(hDoc: LongInt): Integer stdcall;
  TVpeSetUnderline = procedure(hDoc: LongInt; underline: Integer) stdcall;
  TVpeGetUnderline = function(hDoc: LongInt): Integer stdcall;
  TVpeGetAutoBreak = function(hDoc: LongInt): Integer stdcall;
  TVpeGetDocExportPictureQuality = function(hDoc: LongInt): Integer stdcall;
  TVpeSetDocExportPictureQuality = procedure(hDoc: LongInt; quality: Integer) stdcall;
  TVpeSetDocExportType = procedure(hDoc: LongInt; doc_type: Integer) stdcall;
  TVpeGetDocExportType = function(hDoc: LongInt): Integer stdcall;
  TVpeGetDocExportPictureResolution = function(hDoc: LongInt): Integer stdcall;
  TVpeSetDocExportPictureResolution = procedure(hDoc: LongInt; dpi_resolution: Integer) stdcall;
  TVpeGetFastWebView = function(hDoc: LongInt): Integer stdcall;
  TVpeSetFastWebView = procedure(hDoc: LongInt; yes_no: Integer) stdcall;
  TVpeGetUseTempFiles = function(hDoc: LongInt): Integer stdcall;
  TVpeSetUseTempFiles = procedure(hDoc: LongInt; yes_no: Integer) stdcall;
  TVpeGetCompression = function(hDoc: LongInt): Integer stdcall;
  TVpeSetCompression = procedure(hDoc: LongInt; compression: Integer) stdcall;
  TVpeGetEmbedAllFonts = function(hDoc: LongInt): Integer stdcall;
  TVpeSetEmbedAllFonts = procedure(hDoc: LongInt; yes_no: Integer) stdcall;
  TVpeGetSubsetAllFonts = function(hDoc: LongInt): Integer stdcall;
  TVpeSetSubsetAllFonts = procedure(hDoc: LongInt; yes_no: Integer) stdcall;
  TVpeSetFontControl = procedure(hDoc: LongInt; font_name: PChar; subst_font_name: PChar; 
      do_embed: Integer; do_subset: Integer) stdcall;
  TVpeResetFontControl = procedure(hDoc: LongInt) stdcall;
  TVpeGetProtection = function(hDoc: LongInt): LongInt stdcall;
  TVpeSetProtection = procedure(hDoc: LongInt; protection_flags: LongInt) stdcall;
  TVpeGetEncryption = function(hDoc: LongInt): Integer stdcall;
  TVpeSetEncryption = procedure(hDoc: LongInt; enc_type: Integer) stdcall;
  TVpeGetEncryptionKeyLength = function(hDoc: LongInt): Integer stdcall;
  TVpeSetEncryptionKeyLength = procedure(hDoc: LongInt; key_length: Integer) stdcall;
  TVpeSetUserPassword = procedure(hDoc: LongInt; user_password: PChar) stdcall;
  TVpeSetOwnerPassword = procedure(hDoc: LongInt; owner_password: PChar) stdcall;
  TVpeSetAuthor = procedure(hDoc: LongInt; author: PChar) stdcall;
  TVpeSetTitle = procedure(hDoc: LongInt; title: PChar) stdcall;
  TVpeSetSubject = procedure(hDoc: LongInt; subject: PChar) stdcall;
  TVpeSetKeywords = procedure(hDoc: LongInt; keywords: PChar) stdcall;
  TVpeSetCreator = procedure(hDoc: LongInt; creator: PChar) stdcall;
  TVpeEnableMultiThreading = procedure(hDoc: LongInt; yes_no: Integer) stdcall;
  TVpeGetPDFVersion = function(hDoc: LongInt): LongInt stdcall;
  TVpeSetPDFVersion = procedure(hDoc: LongInt; version: LongInt) stdcall;
  TVpeSetBookmarkDestination = procedure(hDoc: LongInt; bkm_type: Integer; left: Double; 
      top: Double; right: Double; bottom: Double; zoom_factor: Double) stdcall;
  TVpeGetBookmarkStyle = function(hDoc: LongInt): Integer stdcall;
  TVpeSetBookmarkStyle = procedure(hDoc: LongInt; style: Integer) stdcall;
  TVpeGetBookmarkColor = function(hDoc: LongInt): TColorRef stdcall;
  TVpeSetBookmarkColor = procedure(hDoc: LongInt; color: TColorRef) stdcall;
  TVpeAddBookmark = function(hDoc: LongInt; parent: LongInt; title: PChar): LongInt stdcall;
  TVpeExtIntDA = function(hDoc: LongInt; i: LongInt; s: LongInt): LongInt stdcall;
  TVpeSetPictureCache = procedure(hDoc: LongInt; on_off: Integer) stdcall;
  TVpeGetPictureCache = function(hDoc: LongInt): Integer stdcall;
  TVpeSetPictureX2YResolution = procedure(hDoc: LongInt; on_off: Integer) stdcall;
  TVpeGetPictureX2YResolution = function(hDoc: LongInt): Integer stdcall;
  TVpeSetPictureBestFit = procedure(hDoc: LongInt; on_off: Integer) stdcall;
  TVpeGetPictureBestFit = function(hDoc: LongInt): Integer stdcall;
  TVpeSetPictureEmbedInDoc = procedure(hDoc: LongInt; on_off: Integer) stdcall;
  TVpeGetPictureEmbedInDoc = function(hDoc: LongInt): Integer stdcall;
  TVpeSetPictureDrawExact = procedure(hDoc: LongInt; on_off: Integer) stdcall;
  TVpeGetPictureDrawExact = function(hDoc: LongInt): Integer stdcall;
  TVpeSetPictureKeepAspect = procedure(hDoc: LongInt; on_off: Integer) stdcall;
  TVpeGetPictureKeepAspect = function(hDoc: LongInt): Integer stdcall;
  TVpeSetPictureDefaultDPI = procedure(hDoc: LongInt; dpix: Integer; dpiy: Integer) stdcall;
  TVpeSetCornerRadius = procedure(hDoc: LongInt; radius: Double) stdcall;
  TVpeGetCornerRadius = function(hDoc: LongInt): Double stdcall;
  TVpeSetUnitTransformation = procedure(hDoc: LongInt; factor: Double) stdcall;
  TVpeGetUnitTransformation = function(hDoc: LongInt): Double stdcall;
  TVpeSetBkgGradientTriColor = procedure(hDoc: LongInt; on_off: Integer) stdcall;
  TVpeGetBkgGradientTriColor = function(hDoc: LongInt): Integer stdcall;
  TVpeSetBkgGradientMiddleColor = procedure(hDoc: LongInt; color_middle: TColorRef) stdcall;
  TVpeGetBkgGradientMiddleColor = function(hDoc: LongInt): TColorRef stdcall;
  TVpeSetBkgGradientMiddleColorPosition = procedure(hDoc: LongInt; position: Integer) stdcall;
  TVpeGetBkgGradientMiddleColorPosition = function(hDoc: LongInt): Integer stdcall;
  TVpeSetMsgCallback = procedure(hDoc: LongInt; callback: Integer) stdcall;
  TVpeSetFontSubstitution = procedure(hDoc: LongInt; original_font: PChar; subst_font: PChar) stdcall;
  TVpePurgeFontSubstitution = procedure(hDoc: LongInt) stdcall;
  TVpeLicenseU = procedure(hDoc: LongInt; serial1: PChar; serial2: PChar) stdcall;
  TVpeOpenDocU = function(hWndParent: HWND; title: PChar; flags: LongInt): LongInt stdcall;
  TVpeOpenDocFileU = function(hWndParent: HWND; file_name: PChar; title: PChar; flags: LongInt): LongInt stdcall;
  TVpeBarcodeU = procedure(hDoc: LongInt; x: Double; y: Double; x2: Double; y2: Double; 
      code_type: Integer; code: PChar; add_code: PChar) stdcall;
  TVpeGetPictureTypesU = procedure(hDoc: LongInt; s: PChar; size: Integer) stdcall;
  TVpePictureU = function(hDoc: LongInt; x: Double; y: Double; x2: Double; y2: Double; 
      file_name: PChar): Double stdcall;
  TVpeSetFontU = procedure(hDoc: LongInt; name: PChar; size: Integer) stdcall;
  TVpeSelectFontU = procedure(hDoc: LongInt; name: PChar; size: Integer) stdcall;
  TVpeSetFontNameU = procedure(hDoc: LongInt; name: PChar) stdcall;
  TVpePrintU = function(hDoc: LongInt; x: Double; y: Double; s: PChar): Double stdcall;
  TVpePrintBoxU = function(hDoc: LongInt; x: Double; y: Double; s: PChar): Double stdcall;
  TVpeWriteU = function(hDoc: LongInt; x: Double; y: Double; x2: Double; y2: Double; 
      s: PChar): Double stdcall;
  TVpeWriteBoxU = function(hDoc: LongInt; x: Double; y: Double; x2: Double; y2: Double; 
      s: PChar): Double stdcall;
  TVpeDefineHeaderU = procedure(hDoc: LongInt; x: Double; y: Double; x2: Double; y2: Double; 
      s: PChar) stdcall;
  TVpeDefineFooterU = procedure(hDoc: LongInt; x: Double; y: Double; x2: Double; y2: Double; 
      s: PChar) stdcall;
  TVpeWriteDocU = function(hDoc: LongInt; file_name: PChar): Integer stdcall;
  TVpeReadDocU = function(hDoc: LongInt; file_name: PChar): Integer stdcall;
  TVpeRenderPrintU = function(hDoc: LongInt; x: Double; y: Double; s: PChar): Integer stdcall;
  TVpeRenderPrintBoxU = function(hDoc: LongInt; x: Double; y: Double; s: PChar): Integer stdcall;
  TVpeRenderWriteU = function(hDoc: LongInt; x: Double; y: Double; x2: Double; y2: Double; 
      s: PChar): Integer stdcall;
  TVpeRenderWriteBoxU = function(hDoc: LongInt; x: Double; y: Double; x2: Double; y2: Double; 
      s: PChar): Integer stdcall;
  TVpeRenderPictureU = procedure(hDoc: LongInt; width: Double; height: Double; file_name: PChar) stdcall;
  TVpeGetPicturePageCountU = function(hDoc: LongInt; file_name: PChar): LongInt stdcall;
  TVpeGetFontNameU = procedure(hDoc: LongInt; value: PChar; size: PUINT) stdcall;
  TVpeSetFontControlU = procedure(hDoc: LongInt; font_name: PChar; subst_font_name: PChar; 
      do_embed: Integer; do_subset: Integer) stdcall;
  TVpeSetUserPasswordU = procedure(hDoc: LongInt; user_password: PChar) stdcall;
  TVpeSetOwnerPasswordU = procedure(hDoc: LongInt; owner_password: PChar) stdcall;
  TVpeSetAuthorU = procedure(hDoc: LongInt; author: PChar) stdcall;
  TVpeSetTitleU = procedure(hDoc: LongInt; title: PChar) stdcall;
  TVpeSetSubjectU = procedure(hDoc: LongInt; subject: PChar) stdcall;
  TVpeSetKeywordsU = procedure(hDoc: LongInt; keywords: PChar) stdcall;
  TVpeSetCreatorU = procedure(hDoc: LongInt; creator: PChar) stdcall;
  TVpeAddBookmarkU = function(hDoc: LongInt; parent: LongInt; title: PChar): LongInt stdcall;
  TVpeSetFontSubstitutionU = procedure(hDoc: LongInt; original_font: PChar; subst_font: PChar) stdcall;
  TVpeGetWindowHandle = function(hDoc: LongInt): HWND stdcall;
  TVpeWindowHandle = function(hDoc: LongInt): HWND stdcall;
  TVpeEnablePrintSetupDialog = procedure(hDoc: LongInt; enabled: Integer) stdcall;
  TVpeSetGridMode = procedure(hDoc: LongInt; in_foreground: Integer) stdcall;
  TVpeSetGridVisible = procedure(hDoc: LongInt; yes_no: Integer) stdcall;
  TVpeEnableMailButton = procedure(hDoc: LongInt; enabled: Integer) stdcall;
  TVpeEnableCloseButton = procedure(hDoc: LongInt; enabled: Integer) stdcall;
  TVpeEnableHelpRouting = procedure(hDoc: LongInt; enabled: Integer) stdcall;
  TVpeSetPreviewWithScrollers = procedure(hDoc: LongInt; yes_no: Integer) stdcall;
  TVpeSetPaperView = procedure(hDoc: LongInt; on_off: Integer) stdcall;
  TVpeSetScale = procedure(hDoc: LongInt; scale: Double) stdcall;
  TVpeGetScale = function(hDoc: LongInt): Double stdcall;
  TVpeSetScalePercent = procedure(hDoc: LongInt; scale: Integer) stdcall;
  TVpeGetScalePercent = function(hDoc: LongInt): Integer stdcall;
  TVpeSetRulersMeasure = procedure(hDoc: LongInt; rulers_measure: Integer) stdcall;
  TVpeSetupPrinter = function(hDoc: LongInt; file_name: PChar; dialog_control: Integer): Integer stdcall;
  TVpeSetPrintOptions = procedure(hDoc: LongInt; flags: LongInt) stdcall;
  TVpePrintDoc = procedure(hDoc: LongInt; with_setup: Integer) stdcall;
  TVpeIsPrinting = function(hDoc: LongInt): Integer stdcall;
  TVpePreviewDoc = procedure(hDoc: LongInt; rc: PRECT; show_hide: Integer) stdcall;
  TVpePreviewDocSP = procedure(hDoc: LongInt; x: Integer; y: Integer; x2: Integer; y2: Integer; 
      show_hide: Integer) stdcall;
  TVpeCenterPreview = procedure(hDoc: LongInt; width: Integer; height: Integer; parent_window: HWND) stdcall;
  TVpeRefreshDoc = procedure(hDoc: LongInt) stdcall;
  TVpePictureDIB = function(hDoc: LongInt; x: Double; y: Double; x2: Double; y2: Double; 
      hDIB: THandle): Double stdcall;
  TVpePictureResID = function(hDoc: LongInt; x: Double; y: Double; x2: Double; y2: Double; 
      hInstance: Integer; res_id: Integer): Double stdcall;
  TVpePictureResName = function(hDoc: LongInt; x: Double; y: Double; x2: Double; y2: Double; 
      hInstance: Integer; res_name: PChar): Double stdcall;
  TVpeSetPrintPosMode = procedure(hDoc: LongInt; mode: Integer) stdcall;
  TVpeSetPageScrollerTracking = procedure(hDoc: LongInt; on_off: Integer) stdcall;
  TVpeWriteStatusbar = procedure(hDoc: LongInt; text: PChar) stdcall;
  TVpeOpenProgressBar = procedure(hDoc: LongInt) stdcall;
  TVpeSetProgressBar = procedure(hDoc: LongInt; percent: Integer) stdcall;
  TVpeCloseProgressBar = procedure(hDoc: LongInt) stdcall;
  TVpeRenderPictureDIB = procedure(hDoc: LongInt; width: Double; height: Double; hDIB: THandle) stdcall;
  TVpeRenderPictureResID = procedure(hDoc: LongInt; width: Double; height: Double; 
      hInstance: Integer; res_id: Integer) stdcall;
  TVpeRenderPictureResName = procedure(hDoc: LongInt; width: Double; height: Double; 
      hInstance: Integer; res_name: PChar) stdcall;
  TVpeSetDevice = function(hDoc: LongInt; device: PChar): Integer stdcall;
  TVpeGetDevice = procedure(hDoc: LongInt; device: PChar; size: Integer) stdcall;
  TVpeDevEnum = function(hDoc: LongInt): Integer stdcall;
  TVpeGetDevEntry = procedure(hDoc: LongInt; index: Integer; device: PChar; size: Integer) stdcall;
  TVpeSetDevOrientation = function(hDoc: LongInt; orientation: Integer): Integer stdcall;
  TVpeGetDevOrientation = function(hDoc: LongInt): Integer stdcall;
  TVpeSetDevPaperFormat = function(hDoc: LongInt; format: Integer): Integer stdcall;
  TVpeGetDevPaperFormat = function(hDoc: LongInt): Integer stdcall;
  TVpeSetDevPaperWidth = function(hDoc: LongInt; width: Double): Integer stdcall;
  TVpeGetDevPaperWidth = function(hDoc: LongInt): Double stdcall;
  TVpeSetDevPaperHeight = function(hDoc: LongInt; height: Double): Integer stdcall;
  TVpeGetDevPaperHeight = function(hDoc: LongInt): Double stdcall;
  TVpeSetDevScalePercent = function(hDoc: LongInt; scale: Integer): Integer stdcall;
  TVpeGetDevScalePercent = function(hDoc: LongInt): Integer stdcall;
  TVpeSetDevPrintQuality = function(hDoc: LongInt; quality: Integer): Integer stdcall;
  TVpeGetDevPrintQuality = function(hDoc: LongInt): Integer stdcall;
  TVpeSetDevYResolution = function(hDoc: LongInt; yres: Integer): Integer stdcall;
  TVpeGetDevYResolution = function(hDoc: LongInt): Integer stdcall;
  TVpeSetDevColor = function(hDoc: LongInt; color: Integer): Integer stdcall;
  TVpeGetDevColor = function(hDoc: LongInt): Integer stdcall;
  TVpeSetDevDuplex = function(hDoc: LongInt; duplex: Integer): Integer stdcall;
  TVpeGetDevDuplex = function(hDoc: LongInt): Integer stdcall;
  TVpeSetDevTTOption = function(hDoc: LongInt; option: Integer): Integer stdcall;
  TVpeGetDevTTOption = function(hDoc: LongInt): Integer stdcall;
  TVpeDevEnumPaperBins = function(hDoc: LongInt): Integer stdcall;
  TVpeGetDevPaperBinName = procedure(hDoc: LongInt; index: Integer; bin_name: PChar; 
      size: Integer) stdcall;
  TVpeGetDevPaperBinID = function(hDoc: LongInt; index: Integer): Integer stdcall;
  TVpeSetDevPaperBin = function(hDoc: LongInt; bin_id: Integer): Integer stdcall;
  TVpeGetDevPaperBin = function(hDoc: LongInt): Integer stdcall;
  TVpeSetDevCopies = procedure(hDoc: LongInt; copies: Integer) stdcall;
  TVpeGetDevCopies = function(hDoc: LongInt): Integer stdcall;
  TVpeSetDevCollate = procedure(hDoc: LongInt; collate: Integer) stdcall;
  TVpeGetDevCollate = function(hDoc: LongInt): Integer stdcall;
  TVpeGetDevPrinterOffsetX = function(hDoc: LongInt): Double stdcall;
  TVpeGetDevPrinterOffsetY = function(hDoc: LongInt): Double stdcall;
  TVpeGetDevPhysPageWidth = function(hDoc: LongInt): Double stdcall;
  TVpeGetDevPhysPageHeight = function(hDoc: LongInt): Double stdcall;
  TVpeGetDevPrintableWidth = function(hDoc: LongInt): Double stdcall;
  TVpeGetDevPrintableHeight = function(hDoc: LongInt): Double stdcall;
  TVpeSetDevFromPage = procedure(hDoc: LongInt; from_page: Integer) stdcall;
  TVpeGetDevFromPage = function(hDoc: LongInt): Integer stdcall;
  TVpeSetDevToPage = procedure(hDoc: LongInt; to_page: Integer) stdcall;
  TVpeGetDevToPage = function(hDoc: LongInt): Integer stdcall;
  TVpeSetDevToFile = procedure(hDoc: LongInt; to_file: Integer) stdcall;
  TVpeGetDevToFile = function(hDoc: LongInt): Integer stdcall;
  TVpeSetDevFileName = procedure(hDoc: LongInt; file_name: PChar) stdcall;
  TVpeGetDevFileName = procedure(hDoc: LongInt; file_name: PChar; size: Integer) stdcall;
  TVpeSetDevJobName = procedure(hDoc: LongInt; job_name: PChar) stdcall;
  TVpeGetDevJobName = procedure(hDoc: LongInt; job_name: PChar; size: Integer) stdcall;
  TVpeDevSendData = function(hDoc: LongInt; data: PChar; size: LongInt): Integer stdcall;
  TVpeReadPrinterSetup = function(hDoc: LongInt; file_name: PChar): Integer stdcall;
  TVpeWritePrinterSetup = function(hDoc: LongInt; file_name: PChar): Integer stdcall;
  TVpeDefineKey = procedure(hDoc: LongInt; gui_function: Integer; key_code: Integer; 
      add_key_code1: Integer; add_key_code2: Integer) stdcall;
  TVpeSendKey = procedure(hDoc: LongInt; vkey: Integer) stdcall;
  TVpeSetGUILanguage = procedure(hDoc: LongInt; language: Integer) stdcall;
  TVpeSetPreviewCtrl = procedure(hDoc: LongInt; setting: Integer) stdcall;
  TVpeEnableAutoDelete = procedure(hDoc: LongInt; yes_no: Integer) stdcall;
  TVpeClosePreview = procedure(hDoc: LongInt) stdcall;
  TVpeIsPreviewVisible = function(hDoc: LongInt): Integer stdcall;
  TVpeGotoVisualPage = function(hDoc: LongInt; page_no: Integer): Integer stdcall;
  TVpeGetVisualPage = function(hDoc: LongInt): Integer stdcall;
  TVpeDispatchAllMessages = function(hDoc: LongInt): Integer stdcall;
  TVpeSetBusyProgressBar = procedure(hDoc: LongInt; visible: Integer) stdcall;
  TVpeSetMailWithDialog = procedure(hDoc: LongInt; yes_no: Integer) stdcall;
  TVpeSetMailSubject = procedure(hDoc: LongInt; subject: PChar) stdcall;
  TVpeSetMailText = procedure(hDoc: LongInt; text: PChar) stdcall;
  TVpeSetMailSender = procedure(hDoc: LongInt; sender: PChar) stdcall;
  TVpeAddMailReceiver = procedure(hDoc: LongInt; receiver: PChar; recip_class: LongInt) stdcall;
  TVpeClearMailReceivers = procedure(hDoc: LongInt) stdcall;
  TVpeAddMailAttachment = procedure(hDoc: LongInt; path: PChar; file_name: PChar) stdcall;
  TVpeClearMailAttachments = procedure(hDoc: LongInt) stdcall;
  TVpeSetMailAutoAttachDocType = procedure(hDoc: LongInt; doc_type: Integer) stdcall;
  TVpeGetMailAutoAttachDocType = function(hDoc: LongInt): Integer stdcall;
  TVpeIsMAPIInstalled = function(hDoc: LongInt): Integer stdcall;
  TVpeGetMAPIType = function(hDoc: LongInt): Integer stdcall;
  TVpeMailDoc = function(hDoc: LongInt): Integer stdcall;
  TVpeMapMessage = function(hDoc: LongInt; message: UINT): UINT stdcall;
  TVpeSetMinScale = procedure(hDoc: LongInt; min_scale: Double) stdcall;
  TVpeSetMinScalePercent = procedure(hDoc: LongInt; min_scale_percent: Integer) stdcall;
  TVpeSetMaxScale = procedure(hDoc: LongInt; max_scale: Double) stdcall;
  TVpeSetMaxScalePercent = procedure(hDoc: LongInt; max_scale_percent: Integer) stdcall;
  TVpeSetResourceString = procedure(hDoc: LongInt; resource_id: Integer; text: PChar) stdcall;
  TVpeSetGUITheme = procedure(hDoc: LongInt; theme: Integer) stdcall;
  TVpeGetGUITheme = function(hDoc: LongInt): Integer stdcall;
  TVpeSetScaleMode = procedure(hDoc: LongInt; mode: Integer) stdcall;
  TVpeGetScaleMode = function(hDoc: LongInt): Integer stdcall;
  TVpeZoomPreview = procedure(hDoc: LongInt; left: Integer; top: Integer; right: Integer; 
      bottom: Integer) stdcall;
  TVpeSetPreviewPosition = procedure(hDoc: LongInt; left: Integer; top: Integer) stdcall;
  TVpeZoomIn = procedure(hDoc: LongInt) stdcall;
  TVpeZoomOut = procedure(hDoc: LongInt) stdcall;
  TVpeGetOpenFileName = function(hDoc: LongInt; file_name: PChar; size: PUINT): Integer stdcall;
  TVpeSetOpenFileName = procedure(hDoc: LongInt; file_name: PChar) stdcall;
  TVpeGetSaveFileName = function(hDoc: LongInt; file_name: PChar; size: PUINT): Integer stdcall;
  TVpeSetSaveFileName = procedure(hDoc: LongInt; file_name: PChar) stdcall;
  TVpeOpenFileDialog = procedure(hDoc: LongInt) stdcall;
  TVpeSaveFileDialog = procedure(hDoc: LongInt) stdcall;
  TVpeSetEngineRenderMode = procedure(hDoc: LongInt; mode: Integer) stdcall;
  TVpeGetEngineRenderMode = function(hDoc: LongInt): Integer stdcall;
  TVpeSetupPrinterU = function(hDoc: LongInt; file_name: PChar; dialog_control: Integer): Integer stdcall;
  TVpePictureResNameU = function(hDoc: LongInt; x: Double; y: Double; x2: Double; y2: Double; 
      hInstance: Integer; res_name: PChar): Double stdcall;
  TVpeWriteStatusbarU = procedure(hDoc: LongInt; text: PChar) stdcall;
  TVpeRenderPictureResNameU = procedure(hDoc: LongInt; width: Double; height: Double; 
      hInstance: Integer; res_name: PChar) stdcall;
  TVpeSetDeviceU = function(hDoc: LongInt; device: PChar): Integer stdcall;
  TVpeGetDeviceU = procedure(hDoc: LongInt; device: PChar; size: Integer) stdcall;
  TVpeGetDevEntryU = procedure(hDoc: LongInt; index: Integer; device: PChar; size: Integer) stdcall;
  TVpeGetDevPaperBinNameU = procedure(hDoc: LongInt; index: Integer; bin_name: PChar; 
      size: Integer) stdcall;
  TVpeSetDevFileNameU = procedure(hDoc: LongInt; file_name: PChar) stdcall;
  TVpeGetDevFileNameU = procedure(hDoc: LongInt; file_name: PChar; size: Integer) stdcall;
  TVpeSetDevJobNameU = procedure(hDoc: LongInt; job_name: PChar) stdcall;
  TVpeGetDevJobNameU = procedure(hDoc: LongInt; job_name: PChar; size: Integer) stdcall;
  TVpeDevSendDataU = function(hDoc: LongInt; data: PChar; size: LongInt): Integer stdcall;
  TVpeReadPrinterSetupU = function(hDoc: LongInt; file_name: PChar): Integer stdcall;
  TVpeWritePrinterSetupU = function(hDoc: LongInt; file_name: PChar): Integer stdcall;
  TVpeSetMailSubjectU = procedure(hDoc: LongInt; subject: PChar) stdcall;
  TVpeSetMailTextU = procedure(hDoc: LongInt; text: PChar) stdcall;
  TVpeSetMailSenderU = procedure(hDoc: LongInt; sender: PChar) stdcall;
  TVpeAddMailReceiverU = procedure(hDoc: LongInt; receiver: PChar; recip_class: LongInt) stdcall;
  TVpeAddMailAttachmentU = procedure(hDoc: LongInt; path: PChar; file_name: PChar) stdcall;
  TVpeSetResourceStringU = procedure(hDoc: LongInt; resource_id: Integer; text: PChar) stdcall;
  TVpeGetOpenFileNameU = function(hDoc: LongInt; file_name: PChar; size: PUINT): Integer stdcall;
  TVpeSetOpenFileNameU = procedure(hDoc: LongInt; file_name: PChar) stdcall;
  TVpeGetSaveFileNameU = function(hDoc: LongInt; file_name: PChar; size: PUINT): Integer stdcall;
  TVpeSetSaveFileNameU = procedure(hDoc: LongInt; file_name: PChar) stdcall;
  TVpeSetViewable = procedure(hDoc: LongInt; yes_no: Integer) stdcall;
  TVpeSetPrintable = procedure(hDoc: LongInt; yes_no: Integer) stdcall;
  TVpeSetStreamable = procedure(hDoc: LongInt; yes_no: Integer) stdcall;
  TVpeSetShadowed = procedure(hDoc: LongInt; yes_no: Integer) stdcall;
  TVpeWriteRTF = function(hDoc: LongInt; x: Double; y: Double; x2: Double; y2: Double; 
      s: PChar): Double stdcall;
  TVpeWriteBoxRTF = function(hDoc: LongInt; x: Double; y: Double; x2: Double; y2: Double; 
      s: PChar): Double stdcall;
  TVpeWriteRTFFile = function(hDoc: LongInt; x: Double; y: Double; x2: Double; y2: Double; 
      file_name: PChar): Double stdcall;
  TVpeWriteBoxRTFFile = function(hDoc: LongInt; x: Double; y: Double; x2: Double; y2: Double; 
      file_name: PChar): Double stdcall;
  TVpeSetFirstIndent = procedure(hDoc: LongInt; indent: Double) stdcall;
  TVpeSetLeftIndent = procedure(hDoc: LongInt; indent: Double) stdcall;
  TVpeSetRightIndent = procedure(hDoc: LongInt; indent: Double) stdcall;
  TVpeSetSpaceBefore = procedure(hDoc: LongInt; space: Double) stdcall;
  TVpeSetSpaceAfter = procedure(hDoc: LongInt; space: Double) stdcall;
  TVpeSetSpaceBetween = procedure(hDoc: LongInt; space: Double) stdcall;
  TVpeSetDefaultTabSize = procedure(hDoc: LongInt; default_tab_size: Double) stdcall;
  TVpeSetTab = procedure(hDoc: LongInt; tab_position: Double; reserved: Integer) stdcall;
  TVpeClearTab = procedure(hDoc: LongInt; tab_position: Double) stdcall;
  TVpeClearAllTabs = procedure(hDoc: LongInt) stdcall;
  TVpeSetKeepLines = procedure(hDoc: LongInt; yes_no: Integer) stdcall;
  TVpeSetKeepNextParagraph = procedure(hDoc: LongInt; yes_no: Integer) stdcall;
  TVpeSetParagraphControl = procedure(hDoc: LongInt; yes_no: Integer) stdcall;
  TVpeResetParagraph = procedure(hDoc: LongInt) stdcall;
  TVpeSetRTFFont = procedure(hDoc: LongInt; ID: Integer; name: PChar) stdcall;
  TVpeSetRTFColor = procedure(hDoc: LongInt; ID: Integer; color: TColorRef) stdcall;
  TVpeRenderRTF = function(hDoc: LongInt; x: Double; y: Double; x2: Double; y2: Double; 
      s: PChar): Integer stdcall;
  TVpeRenderBoxRTF = function(hDoc: LongInt; x: Double; y: Double; x2: Double; y2: Double; 
      s: PChar): Integer stdcall;
  TVpeRenderRTFFile = function(hDoc: LongInt; x: Double; y: Double; x2: Double; y2: Double; 
      s: PChar): Integer stdcall;
  TVpeRenderBoxRTFFile = function(hDoc: LongInt; x: Double; y: Double; x2: Double; 
      y2: Double; s: PChar): Integer stdcall;
  TVpeSetCharPlacement = procedure(hDoc: LongInt; distance: Double) stdcall;
  TVpeChartDataCreate = function(hDoc: LongInt; columns: Integer; rows: Integer): LongInt stdcall;
  TVpeChartDataAddRow = procedure(hDoc: LongInt; hChartData: LongInt) stdcall;
  TVpeChartDataAddColumn = procedure(hDoc: LongInt; hChartData: LongInt) stdcall;
  TVpeChartDataAddValue = procedure(hDoc: LongInt; hChartData: LongInt; column: Integer; 
      value: Double) stdcall;
  TVpeChartDataAddGap = procedure(hDoc: LongInt; hChartData: LongInt; column: Integer) stdcall;
  TVpeChartDataAddLegend = procedure(hDoc: LongInt; hChartData: LongInt; legend: PChar) stdcall;
  TVpeChartDataAddXLabel = procedure(hDoc: LongInt; hChartData: LongInt; xlabel: PChar) stdcall;
  TVpeChartDataAddYLabel = procedure(hDoc: LongInt; hChartData: LongInt; ylabel: PChar) stdcall;
  TVpeChartDataSetXAxisTitle = procedure(hDoc: LongInt; hChartData: LongInt; x_axis_title: PChar) stdcall;
  TVpeChartDataSetYAxisTitle = procedure(hDoc: LongInt; hChartData: LongInt; y_axis_title: PChar) stdcall;
  TVpeChartDataSetColor = procedure(hDoc: LongInt; hChartData: LongInt; column: Integer; 
      color: TColorRef) stdcall;
  TVpeChartDataSetLineStyle = procedure(hDoc: LongInt; hChartData: LongInt; column: Integer; 
      pen_style: Integer) stdcall;
  TVpeChartDataSetHatchStyle = procedure(hDoc: LongInt; hChartData: LongInt; column: Integer; 
      style: Integer) stdcall;
  TVpeChartDataSetPointType = procedure(hDoc: LongInt; hChartData: LongInt; column: Integer; 
      pointtype: Integer) stdcall;
  TVpeChartDataSetMaximum = procedure(hDoc: LongInt; hChartData: LongInt; maximum: Double) stdcall;
  TVpeChartDataSetMinimum = procedure(hDoc: LongInt; hChartData: LongInt; minimum: Double) stdcall;
  TVpeSetChartTitle = procedure(hDoc: LongInt; title: PChar) stdcall;
  TVpeSetChartSubTitle = procedure(hDoc: LongInt; subtitle: PChar) stdcall;
  TVpeSetChartFootNote = procedure(hDoc: LongInt; footnote: PChar) stdcall;
  TVpeSetChartRow = procedure(hDoc: LongInt; row: Integer) stdcall;
  TVpeSetChartGridBkgColor = procedure(hDoc: LongInt; bkgcolor: TColorRef) stdcall;
  TVpeSetChartGridBkgMode = procedure(hDoc: LongInt; mode: Integer) stdcall;
  TVpeSetChartGridType = procedure(hDoc: LongInt; gridtype: Integer) stdcall;
  TVpeSetChartGridColor = procedure(hDoc: LongInt; gridcolor: TColorRef) stdcall;
  TVpeSetChartXGridStep = procedure(hDoc: LongInt; gridstepx: Integer) stdcall;
  TVpeSetChartYGridStep = procedure(hDoc: LongInt; gridstepy: Double) stdcall;
  TVpeSetChartYAutoGridStep = procedure(hDoc: LongInt) stdcall;
  TVpeSetChartLegendPosition = procedure(hDoc: LongInt; legendpos: Integer) stdcall;
  TVpeSetChartLegendBorderStat = procedure(hDoc: LongInt; legendborderstat: Integer) stdcall;
  TVpeSetChartXLabelState = procedure(hDoc: LongInt; xlabelstate: Integer) stdcall;
  TVpeSetChartXLabelAngle = procedure(hDoc: LongInt; xlabelangle: Integer) stdcall;
  TVpeSetChartYLabelState = procedure(hDoc: LongInt; ylabelstate: Integer) stdcall;
  TVpeSetChartXLabelStartValue = procedure(hDoc: LongInt; xlabelstartvalue: Integer) stdcall;
  TVpeSetChartXLabelStep = procedure(hDoc: LongInt; xlabelstep: Integer) stdcall;
  TVpeSetChartYLabelStep = procedure(hDoc: LongInt; ylabelstep: Integer) stdcall;
  TVpeSetChartYLabelDivisor = procedure(hDoc: LongInt; ylabeldivisor: Double) stdcall;
  TVpeSetChartGridRotation = procedure(hDoc: LongInt; axisangle: Integer) stdcall;
  TVpeSetChartYAxisAngle = procedure(hDoc: LongInt; angleyzaxis: Integer) stdcall;
  TVpeSetChartXAxisAngle = procedure(hDoc: LongInt; anglexzaxis: Integer) stdcall;
  TVpeChart = procedure(hDoc: LongInt; x: Double; y: Double; x2: Double; y2: Double; hChartData: LongInt; 
      chart_type: Integer) stdcall;
  TVpeSetChartBarWidthFactor = procedure(hDoc: LongInt; factor: Double) stdcall;
  TVpeSetChartLineWidthFactor = procedure(hDoc: LongInt; factor: Double) stdcall;
  TVpeSetChartLegendFontSizeFactor = procedure(hDoc: LongInt; factor: Double) stdcall;
  TVpeSetChartXLabelFontSizeFactor = procedure(hDoc: LongInt; factor: Double) stdcall;
  TVpeSetChartYLabelFontSizeFactor = procedure(hDoc: LongInt; factor: Double) stdcall;
  TVpeSetChartTitleFontSizeFactor = procedure(hDoc: LongInt; factor: Double) stdcall;
  TVpeSetChartSubTitleFontSizeFactor = procedure(hDoc: LongInt; factor: Double) stdcall;
  TVpeSetChartFootNoteFontSizeFactor = procedure(hDoc: LongInt; factor: Double) stdcall;
  TVpeSetChartAxisTitleFontSizeFactor = procedure(hDoc: LongInt; factor: Double) stdcall;
  TVpeSetChartPieLegendWithPercent = procedure(hDoc: LongInt; yes_no: Integer) stdcall;
  TVpeSetChartPieLabelType = procedure(hDoc: LongInt; label_type: Integer) stdcall;
  TVpeSetEditProtection = procedure(hDoc: LongInt; reserved: Integer) stdcall;
  TVpeGetEditProtection = function(hDoc: LongInt): Integer stdcall;
  TVpeGetViewable = function(hDoc: LongInt): Integer stdcall;
  TVpeGetPrintable = function(hDoc: LongInt): Integer stdcall;
  TVpeGetStreamable = function(hDoc: LongInt): Integer stdcall;
  TVpeGetShadowed = function(hDoc: LongInt): Integer stdcall;
  TVpeSetExportNonPrintableObjects = procedure(hDoc: LongInt; yes_no: Integer) stdcall;
  TVpeGetExportNonPrintableObjects = function(hDoc: LongInt): Integer stdcall;
  TVpeSetBar2DAlignment = procedure(hDoc: LongInt; alignment: Integer) stdcall;
  TVpeGetBar2DAlignment = function(hDoc: LongInt): Integer stdcall;
  TVpeSetDataMatrixEncodingFormat = procedure(hDoc: LongInt; format: Integer) stdcall;
  TVpeGetDataMatrixEncodingFormat = function(hDoc: LongInt): Integer stdcall;
  TVpeSetDataMatrixEccType = procedure(hDoc: LongInt; ecc_type: Integer) stdcall;
  TVpeGetDataMatrixEccType = function(hDoc: LongInt): Integer stdcall;
  TVpeSetDataMatrixRows = procedure(hDoc: LongInt; rows: Integer) stdcall;
  TVpeGetDataMatrixRows = function(hDoc: LongInt): Integer stdcall;
  TVpeSetDataMatrixColumns = procedure(hDoc: LongInt; columns: Integer) stdcall;
  TVpeGetDataMatrixColumns = function(hDoc: LongInt): Integer stdcall;
  TVpeSetDataMatrixMirror = procedure(hDoc: LongInt; mirror: Integer) stdcall;
  TVpeGetDataMatrixMirror = function(hDoc: LongInt): Integer stdcall;
  TVpeSetDataMatrixBorder = procedure(hDoc: LongInt; border: Integer) stdcall;
  TVpeGetDataMatrixBorder = function(hDoc: LongInt): Integer stdcall;
  TVpeDataMatrix = procedure(hDoc: LongInt; x: Double; y: Double; x2: Double; y2: Double; 
      lpszText: PChar) stdcall;
  TVpeRenderDataMatrix = procedure(hDoc: LongInt; lpszText: PChar; nWidth: Double; 
      nHeight: Double; nModuleWidth: Double) stdcall;
  TVpeMaxiCode = procedure(hDoc: LongInt; x: Double; y: Double; x2: Double; y2: Double; 
      lpszText: PChar) stdcall;
  TVpeRenderMaxiCode = procedure(hDoc: LongInt; lpszText: PChar) stdcall;
  TVpeMaxiCodeEx = procedure(hDoc: LongInt; x: Double; y: Double; x2: Double; y2: Double; 
      nMode: Integer; lpszZip: PChar; nCountryCode: Integer; nServiceClass: Integer; 
      lpszMessage: PChar; nSymbolNumber: Integer; nSymbolCount: Integer) stdcall;
  TVpeRenderMaxiCodeEx = procedure(hDoc: LongInt; nMode: Integer; lpszZip: PChar; 
      nCountryCode: Integer; nServiceClass: Integer; lpszMessage: PChar; nSymbolNumber: Integer; 
      nSymbolCount: Integer) stdcall;
  TVpeSetPDF417ErrorLevel = procedure(hDoc: LongInt; level: Integer) stdcall;
  TVpeGetPDF417ErrorLevel = function(hDoc: LongInt): Integer stdcall;
  TVpeSetPDF417Rows = procedure(hDoc: LongInt; rows: Integer) stdcall;
  TVpeGetPDF417Rows = function(hDoc: LongInt): Integer stdcall;
  TVpeSetPDF417Columns = procedure(hDoc: LongInt; columns: Integer) stdcall;
  TVpeGetPDF417Columns = function(hDoc: LongInt): Integer stdcall;
  TVpePDF417 = procedure(hDoc: LongInt; x: Double; y: Double; x2: Double; y2: Double; 
      lpszText: PChar) stdcall;
  TVpeRenderPDF417 = procedure(hDoc: LongInt; lpszText: PChar; nWidth: Double; nHeight: Double; 
      nModuleWidth: Double) stdcall;
  TVpeSetAztecFlags = procedure(hDoc: LongInt; flags: Integer) stdcall;
  TVpeGetAztecFlags = function(hDoc: LongInt): Integer stdcall;
  TVpeSetAztecControl = procedure(hDoc: LongInt; control: Integer) stdcall;
  TVpeGetAztecControl = function(hDoc: LongInt): Integer stdcall;
  TVpeSetAztecMenu = procedure(hDoc: LongInt; yes_no: Integer) stdcall;
  TVpeGetAztecMenu = function(hDoc: LongInt): Integer stdcall;
  TVpeSetAztecMultipleSymbols = procedure(hDoc: LongInt; count: Integer) stdcall;
  TVpeGetAztecMultipleSymbols = function(hDoc: LongInt): Integer stdcall;
  TVpeSetAztecID = procedure(hDoc: LongInt; id: PChar) stdcall;
  TVpeAztec = procedure(hDoc: LongInt; x: Double; y: Double; x2: Double; y2: Double; lpszText: PChar) stdcall;
  TVpeRenderAztec = procedure(hDoc: LongInt; lpszText: PChar; nWidth: Double; nHeight: Double; 
      nModuleWidth: Double) stdcall;
  TVpeSetPictureScale2Gray = procedure(hDoc: LongInt; on_off: Integer) stdcall;
  TVpeGetPictureScale2Gray = function(hDoc: LongInt): Integer stdcall;
  TVpeSetPictureScale2GrayFloat = procedure(hDoc: LongInt; on_off: Integer) stdcall;
  TVpeGetPictureScale2GrayFloat = function(hDoc: LongInt): Integer stdcall;
  TVpeCreateMemoryStream = function(hDoc: LongInt; chunk_size: LongInt): LongInt stdcall;
  TVpeStreamRead = function(hStream: LongInt; const buffer; size: LongInt): LongInt stdcall;
  TVpeStreamWrite = function(hStream: LongInt; const buffer; size: LongInt): LongInt stdcall;
  TVpeGetStreamSize = function(hStream: LongInt): LongInt stdcall;
  TVpeStreamIsEof = function(hStream: LongInt): Integer stdcall;
  TVpeGetStreamState = function(hStream: LongInt): Integer stdcall;
  TVpeGetStreamPosition = function(hStream: LongInt): LongInt stdcall;
  TVpeStreamSeek = procedure(hStream: LongInt; pos: LongInt) stdcall;
  TVpeStreamSeekEnd = procedure(hStream: LongInt; pos: LongInt) stdcall;
  TVpeStreamSeekRel = procedure(hStream: LongInt; offset: LongInt) stdcall;
  TVpeCloseStream = procedure(hStream: LongInt) stdcall;
  TVpeWriteRTFStream = function(hDoc: LongInt; hStream: LongInt; x: Double; y: Double; 
      x2: Double; y2: Double): Double stdcall;
  TVpeWriteBoxRTFStream = function(hDoc: LongInt; hStream: LongInt; x: Double; y: Double; 
      x2: Double; y2: Double): Double stdcall;
  TVpeRenderRTFStream = function(hDoc: LongInt; hStream: LongInt; x: Double; y: Double; 
      x2: Double; y2: Double): Integer stdcall;
  TVpeRenderBoxRTFStream = function(hDoc: LongInt; hStream: LongInt; x: Double; y: Double; 
      x2: Double; y2: Double): Integer stdcall;
  TVpePictureStream = function(hDoc: LongInt; hStream: LongInt; x: Double; y: Double; 
      x2: Double; y2: Double; identifier: PChar): Double stdcall;
  TVpeRenderPictureStream = procedure(hDoc: LongInt; hStream: LongInt; width: Double; 
      height: Double; identifier: PChar) stdcall;
  TVpeGetPicturePageCountStream = function(hDoc: LongInt; hStream: LongInt): LongInt stdcall;
  TVpeWriteDocStream = function(hDoc: LongInt; hStream: LongInt): Integer stdcall;
  TVpeWriteDocStreamPageRange = function(hDoc: LongInt; hStream: LongInt; from_page: Integer; 
      to_page: Integer): Integer stdcall;
  TVpeWriteDocPageRange = function(hDoc: LongInt; file_name: PChar; from_page: Integer; 
      to_page: Integer): Integer stdcall;
  TVpeReadDocStream = function(hDoc: LongInt; hStream: LongInt): Integer stdcall;
  TVpeReadDocStreamPageRange = function(hDoc: LongInt; hStream: LongInt; from_page: Integer; 
      to_page: Integer): Integer stdcall;
  TVpeReadDocPageRange = function(hDoc: LongInt; file_name: PChar; from_page: Integer; 
      to_page: Integer): Integer stdcall;
  TVpeSetHtmlScale = procedure(hDoc: LongInt; scale: Double) stdcall;
  TVpeGetHtmlScale = function(hDoc: LongInt): Double stdcall;
  TVpeSetHtmlWordSpacing = procedure(hDoc: LongInt; word_spacing: Double) stdcall;
  TVpeGetHtmlWordSpacing = function(hDoc: LongInt): Double stdcall;
  TVpeSetHtmlRtfLineSpacing = procedure(hDoc: LongInt; yes_no: Integer) stdcall;
  TVpeGetHtmlRtfLineSpacing = function(hDoc: LongInt): Integer stdcall;
  TVpeSetHtmlCopyImages = procedure(hDoc: LongInt; yes_no: Integer) stdcall;
  TVpeGetHtmlCopyImages = function(hDoc: LongInt): Integer stdcall;
  TVpeSetHtmlPageBorders = procedure(hDoc: LongInt; yes_no: Integer) stdcall;
  TVpeGetHtmlPageBorders = function(hDoc: LongInt): Integer stdcall;
  TVpeSetHtmlPageSeparators = procedure(hDoc: LongInt; yes_no: Integer) stdcall;
  TVpeGetHtmlPageSeparators = function(hDoc: LongInt): Integer stdcall;
  TVpeSetXmlPictureExport = procedure(hDoc: LongInt; yes_no: Integer) stdcall;
  TVpeGetXmlPictureExport = function(hDoc: LongInt): Integer stdcall;
  TVpeSetOdtTextWidthScale = procedure(hDoc: LongInt; scale: Double) stdcall;
  TVpeGetOdtTextWidthScale = function(hDoc: LongInt): Double stdcall;
  TVpeSetOdtTextPtSizeScale = procedure(hDoc: LongInt; scale: Double) stdcall;
  TVpeGetOdtTextPtSizeScale = function(hDoc: LongInt): Double stdcall;
  TVpeSetOdtLineHeight = procedure(hDoc: LongInt; line_height: Double) stdcall;
  TVpeGetOdtLineHeight = function(hDoc: LongInt): Double stdcall;
  TVpeSetOdtAutoTextboxHeight = procedure(hDoc: LongInt; yes_no: Integer) stdcall;
  TVpeGetOdtAutoTextboxHeight = function(hDoc: LongInt): Integer stdcall;
  TVpeSetOdtPositionProtect = procedure(hDoc: LongInt; yes_no: Integer) stdcall;
  TVpeGetOdtPositionProtect = function(hDoc: LongInt): Integer stdcall;
  TVpeClearPage = procedure(hDoc: LongInt) stdcall;
  TVpeRemovePage = procedure(hDoc: LongInt) stdcall;
  TVpeInsertPage = procedure(hDoc: LongInt) stdcall;
  TVpeSetInsertAtBottomZOrder = procedure(hDoc: LongInt; yes_no: Integer) stdcall;
  TVpeGetInsertAtBottomZOrder = function(hDoc: LongInt): Integer stdcall;
  TVpeWriteRTFU = function(hDoc: LongInt; x: Double; y: Double; x2: Double; y2: Double; 
      s: PChar): Double stdcall;
  TVpeWriteBoxRTFU = function(hDoc: LongInt; x: Double; y: Double; x2: Double; y2: Double; 
      s: PChar): Double stdcall;
  TVpeWriteRTFFileU = function(hDoc: LongInt; x: Double; y: Double; x2: Double; y2: Double; 
      file_name: PChar): Double stdcall;
  TVpeWriteBoxRTFFileU = function(hDoc: LongInt; x: Double; y: Double; x2: Double; 
      y2: Double; file_name: PChar): Double stdcall;
  TVpeSetRTFFontU = procedure(hDoc: LongInt; ID: Integer; name: PChar) stdcall;
  TVpeRenderRTFU = function(hDoc: LongInt; x: Double; y: Double; x2: Double; y2: Double; 
      s: PChar): Integer stdcall;
  TVpeRenderBoxRTFU = function(hDoc: LongInt; x: Double; y: Double; x2: Double; y2: Double; 
      s: PChar): Integer stdcall;
  TVpeRenderRTFFileU = function(hDoc: LongInt; x: Double; y: Double; x2: Double; y2: Double; 
      s: PChar): Integer stdcall;
  TVpeRenderBoxRTFFileU = function(hDoc: LongInt; x: Double; y: Double; x2: Double; 
      y2: Double; s: PChar): Integer stdcall;
  TVpeChartDataAddLegendU = procedure(hDoc: LongInt; hChartData: LongInt; legend: PChar) stdcall;
  TVpeChartDataAddXLabelU = procedure(hDoc: LongInt; hChartData: LongInt; xlabel: PChar) stdcall;
  TVpeChartDataAddYLabelU = procedure(hDoc: LongInt; hChartData: LongInt; ylabel: PChar) stdcall;
  TVpeChartDataSetXAxisTitleU = procedure(hDoc: LongInt; hChartData: LongInt; x_axis_title: PChar) stdcall;
  TVpeChartDataSetYAxisTitleU = procedure(hDoc: LongInt; hChartData: LongInt; y_axis_title: PChar) stdcall;
  TVpeSetChartTitleU = procedure(hDoc: LongInt; title: PChar) stdcall;
  TVpeSetChartSubTitleU = procedure(hDoc: LongInt; subtitle: PChar) stdcall;
  TVpeSetChartFootNoteU = procedure(hDoc: LongInt; footnote: PChar) stdcall;
  TVpeDataMatrixU = procedure(hDoc: LongInt; x: Double; y: Double; x2: Double; y2: Double; 
      lpszText: PChar) stdcall;
  TVpeRenderDataMatrixU = procedure(hDoc: LongInt; lpszText: PChar; nWidth: Double; 
      nHeight: Double; nModuleWidth: Double) stdcall;
  TVpeMaxiCodeU = procedure(hDoc: LongInt; x: Double; y: Double; x2: Double; y2: Double; 
      lpszText: PChar) stdcall;
  TVpeRenderMaxiCodeU = procedure(hDoc: LongInt; lpszText: PChar) stdcall;
  TVpeMaxiCodeExU = procedure(hDoc: LongInt; x: Double; y: Double; x2: Double; y2: Double; 
      nMode: Integer; lpszZip: PChar; nCountryCode: Integer; nServiceClass: Integer; 
      lpszMessage: PChar; nSymbolNumber: Integer; nSymbolCount: Integer) stdcall;
  TVpeRenderMaxiCodeExU = procedure(hDoc: LongInt; nMode: Integer; lpszZip: PChar; 
      nCountryCode: Integer; nServiceClass: Integer; lpszMessage: PChar; nSymbolNumber: Integer; 
      nSymbolCount: Integer) stdcall;
  TVpePDF417U = procedure(hDoc: LongInt; x: Double; y: Double; x2: Double; y2: Double; 
      lpszText: PChar) stdcall;
  TVpeRenderPDF417U = procedure(hDoc: LongInt; lpszText: PChar; nWidth: Double; nHeight: Double; 
      nModuleWidth: Double) stdcall;
  TVpeSetAztecIDU = procedure(hDoc: LongInt; id: PChar) stdcall;
  TVpeAztecU = procedure(hDoc: LongInt; x: Double; y: Double; x2: Double; y2: Double; 
      lpszText: PChar) stdcall;
  TVpeRenderAztecU = procedure(hDoc: LongInt; lpszText: PChar; nWidth: Double; nHeight: Double; 
      nModuleWidth: Double) stdcall;
  TVpePictureStreamU = function(hDoc: LongInt; hStream: LongInt; x: Double; y: Double; 
      x2: Double; y2: Double; identifier: PChar): Double stdcall;
  TVpeRenderPictureStreamU = procedure(hDoc: LongInt; hStream: LongInt; width: Double; 
      height: Double; identifier: PChar) stdcall;
  TVpeWriteDocPageRangeU = function(hDoc: LongInt; file_name: PChar; from_page: Integer; 
      to_page: Integer): Integer stdcall;
  TVpeReadDocPageRangeU = function(hDoc: LongInt; file_name: PChar; from_page: Integer; 
      to_page: Integer): Integer stdcall;
  TVpeSetJpegExportOptions = procedure(hDoc: LongInt; options: LongInt) stdcall;
  TVpeGetJpegExportOptions = function(hDoc: LongInt): LongInt stdcall;
  TVpeSetTiffExportOptions = procedure(hDoc: LongInt; options: LongInt) stdcall;
  TVpeGetTiffExportOptions = function(hDoc: LongInt): LongInt stdcall;
  TVpeSetGifExportOptions = procedure(hDoc: LongInt; options: LongInt) stdcall;
  TVpeGetGifExportOptions = function(hDoc: LongInt): LongInt stdcall;
  TVpeSetBmpExportOptions = procedure(hDoc: LongInt; options: LongInt) stdcall;
  TVpeGetBmpExportOptions = function(hDoc: LongInt): LongInt stdcall;
  TVpeSetPnmExportOptions = procedure(hDoc: LongInt; options: LongInt) stdcall;
  TVpeGetPnmExportOptions = function(hDoc: LongInt): LongInt stdcall;
  TVpeSetPictureExportColorDepth = procedure(hDoc: LongInt; depth: Integer) stdcall;
  TVpeSetPictureExportDither = procedure(hDoc: LongInt; dither: Integer) stdcall;
  TVpePictureExportPage = function(hDoc: LongInt; file_name: PChar; page_no: Integer): Integer stdcall;
  TVpePictureExport = function(hDoc: LongInt; file_name: PChar; page_no: Integer; 
      x: Double; y: Double; x2: Double; y2: Double): Integer stdcall;
  TVpePictureExportPageStream = function(hDoc: LongInt; hStream: LongInt; page_no: Integer): Integer stdcall;
  TVpePictureExportStream = function(hDoc: LongInt; hStream: LongInt; page_no: Integer; 
      x: Double; y: Double; x2: Double; y2: Double): Integer stdcall;
  TVpeEnableClickEvents = procedure(hDoc: LongInt; yes_no: Integer) stdcall;
  TVpeSetObjectID = procedure(hDoc: LongInt; id: Integer) stdcall;
  TVpeGetObjectID = function(hDoc: LongInt): Integer stdcall;
  TVpeCreateUDO = procedure(hDoc: LongInt; x: Double; y: Double; x2: Double; y2: Double; 
      lParam: Integer) stdcall;
  TVpeGetUDOlParam = function(hDoc: LongInt): Integer stdcall;
  TVpeGetUDODC = function(hDoc: LongInt): HDC stdcall;
  TVpeGetUDODrawRect = procedure(hDoc: LongInt; rc: PRECT) stdcall;
  TVpeGetUDOIsPrinting = function(hDoc: LongInt): Integer stdcall;
  TVpeGetUDOIsExporting = function(hDoc: LongInt): Integer stdcall;
  TVpeGetUDODpiX = function(hDoc: LongInt): Integer stdcall;
  TVpeGetUDODpiY = function(hDoc: LongInt): Integer stdcall;
  TVpeSetPrintScale = procedure(hDoc: LongInt; scale: Double) stdcall;
  TVpeGetPrintScale = function(hDoc: LongInt): Double stdcall;
  TVpePictureExportPageU = function(hDoc: LongInt; file_name: PChar; page_no: Integer): Integer stdcall;
  TVpePictureExportU = function(hDoc: LongInt; file_name: PChar; page_no: Integer; 
      x: Double; y: Double; x2: Double; y2: Double): Integer stdcall;
  TVpeLoadTemplate = function(hDoc: LongInt; file_name: PChar): LongInt stdcall;
  TVpeLoadTemplateAuthKey = function(hDoc: LongInt; file_name: PChar; key1: LongInt; 
      key2: LongInt; key3: LongInt; key4: LongInt): LongInt stdcall;
  TVpeUseTemplateSettings = procedure(hDoc: LongInt; hTemplate: LongInt; page: Integer) stdcall;
  TVpeUseTemplateMargins = procedure(hDoc: LongInt; hTemplate: LongInt; page: Integer) stdcall;
  TVpeSetDateTimeIsUTC = procedure(hDoc: LongInt; yes_no: Integer) stdcall;
  TVpeGetDateTimeIsUTC = function(hDoc: LongInt): Integer stdcall;
  TVpeDumpTemplate = function(hDoc: LongInt; hTemplate: LongInt): Integer stdcall;
  TVpeDumpTemplatePage = function(hDoc: LongInt; hTemplate: LongInt; page: Integer): Integer stdcall;
  TVpeGetLastInsertedObject = function(hDoc: LongInt): LongInt stdcall;
  TVpeFormField = function(hDoc: LongInt; x: Double; y: Double; x2: Double; y2: Double; 
      s: PChar): Double stdcall;
  TVpeRenderFormField = function(hDoc: LongInt; x: Double; y: Double; x2: Double; y2: Double; 
      s: PChar): Integer stdcall;
  TVpeSetCharCount = procedure(hDoc: LongInt; count: Integer) stdcall;
  TVpeGetCharCount = function(hDoc: LongInt): Integer stdcall;
  TVpeSetDividerPenSize = procedure(hDoc: LongInt; pen_size: Double) stdcall;
  TVpeGetDividerPenSize = function(hDoc: LongInt): Double stdcall;
  TVpeSetDividerPenColor = procedure(hDoc: LongInt; pen_color: TColorRef) stdcall;
  TVpeGetDividerPenColor = function(hDoc: LongInt): TColorRef stdcall;
  TVpeSetAltDividerNPosition = procedure(hDoc: LongInt; position: Integer) stdcall;
  TVpeGetAltDividerNPosition = function(hDoc: LongInt): Integer stdcall;
  TVpeSetAltDividerPenSize = procedure(hDoc: LongInt; pen_size: Double) stdcall;
  TVpeGetAltDividerPenSize = function(hDoc: LongInt): Double stdcall;
  TVpeSetAltDividerPenColor = procedure(hDoc: LongInt; pen_color: TColorRef) stdcall;
  TVpeGetAltDividerPenColor = function(hDoc: LongInt): TColorRef stdcall;
  TVpeSetBottomLinePenSize = procedure(hDoc: LongInt; pen_size: Double) stdcall;
  TVpeGetBottomLinePenSize = function(hDoc: LongInt): Double stdcall;
  TVpeSetBottomLinePenColor = procedure(hDoc: LongInt; pen_color: TColorRef) stdcall;
  TVpeGetBottomLinePenColor = function(hDoc: LongInt): TColorRef stdcall;
  TVpeSetDividerStyle = procedure(hDoc: LongInt; style: LongInt) stdcall;
  TVpeGetDividerStyle = function(hDoc: LongInt): LongInt stdcall;
  TVpeSetAltDividerStyle = procedure(hDoc: LongInt; style: LongInt) stdcall;
  TVpeGetAltDividerStyle = function(hDoc: LongInt): LongInt stdcall;
  TVpeSetFormFieldFlags = procedure(hDoc: LongInt; flags: LongInt) stdcall;
  TVpeGetFormFieldFlags = function(hDoc: LongInt): LongInt stdcall;
  TVpeSetUDOlParam = procedure(hObject: LongInt; lParam: Integer) stdcall;
  TVpeDeleteObject = procedure(hDoc: LongInt; hObject: LongInt) stdcall;
  TVpeGetFirstObject = function(hDoc: LongInt): LongInt stdcall;
  TVpeGetClickedObject = function(hDoc: LongInt): LongInt stdcall;
  TVpeSetTplMaster = procedure(hTemplate: LongInt; yes_no: Integer) stdcall;
  TVpeGetTplFieldAsString = function(hTemplate: LongInt; data_source_prefix: PChar; 
      field_name: PChar; value: PChar; size: PUINT): Integer stdcall;
  TVpeSetTplFieldAsString = function(hTemplate: LongInt; data_source_prefix: PChar; 
      field_name: PChar; value: PChar): Integer stdcall;
  TVpeGetTplFieldAsInteger = function(hTemplate: LongInt; data_source_prefix: PChar; 
      field_name: PChar; value: PInteger): Integer stdcall;
  TVpeSetTplFieldAsInteger = function(hTemplate: LongInt; data_source_prefix: PChar; 
      field_name: PChar; value: Integer): Integer stdcall;
  TVpeGetTplFieldAsNumber = function(hTemplate: LongInt; data_source_prefix: PChar; 
      field_name: PChar; value: PDouble): Integer stdcall;
  TVpeSetTplFieldAsNumber = function(hTemplate: LongInt; data_source_prefix: PChar; 
      field_name: PChar; value: Double): Integer stdcall;
  TVpeGetTplFieldAsDateTime = function(hTemplate: LongInt; data_source_prefix: PChar; 
      field_name: PChar; year: PInteger; month: PInteger; day: PInteger; hour: PInteger; 
      minute: PInteger; second: PInteger; msec: PInteger): Integer stdcall;
  TVpeSetTplFieldAsDateTime = function(hTemplate: LongInt; data_source_prefix: PChar; 
      field_name: PChar; year: Integer; month: Integer; day: Integer; hour: Integer; minute: Integer; 
      second: Integer; msec: Integer): Integer stdcall;
  TVpeGetTplFieldAsOleDateTime = function(hTemplate: LongInt; data_source_prefix: PChar; 
      field_name: PChar; value: PDouble): Integer stdcall;
  TVpeSetTplFieldAsOleDateTime = function(hTemplate: LongInt; data_source_prefix: PChar; 
      field_name: PChar; value: Double): Integer stdcall;
  TVpeGetTplFieldAsJavaDateTime = function(hTemplate: LongInt; data_source_prefix: PChar; 
      field_name: PChar; value: PDouble): Integer stdcall;
  TVpeSetTplFieldAsJavaDateTime = function(hTemplate: LongInt; data_source_prefix: PChar; 
      field_name: PChar; value: Double): Integer stdcall;
  TVpeGetTplFieldAsBoolean = function(hTemplate: LongInt; data_source_prefix: PChar; 
      field_name: PChar; value: PInteger): Integer stdcall;
  TVpeSetTplFieldAsBoolean = function(hTemplate: LongInt; data_source_prefix: PChar; 
      field_name: PChar; value: Integer): Integer stdcall;
  TVpeGetTplPageCount = function(hTemplate: LongInt): Integer stdcall;
  TVpeGetTplPageWidth = function(hTemplate: LongInt; page: Integer): Double stdcall;
  TVpeSetTplPageWidth = procedure(hTemplate: LongInt; page: Integer; width: Double) stdcall;
  TVpeGetTplPageHeight = function(hTemplate: LongInt; page: Integer): Double stdcall;
  TVpeSetTplPageHeight = procedure(hTemplate: LongInt; page: Integer; height: Double) stdcall;
  TVpeGetTplPageOrientation = function(hTemplate: LongInt; page: Integer): Integer stdcall;
  TVpeSetTplPageOrientation = procedure(hTemplate: LongInt; page: Integer; orientation: Integer) stdcall;
  TVpeGetTplPaperBin = function(hTemplate: LongInt; page: Integer): Integer stdcall;
  TVpeSetTplPaperBin = procedure(hTemplate: LongInt; page: Integer; bin: Integer) stdcall;
  TVpeGetTplLeftMargin = function(hTemplate: LongInt; page: Integer): Double stdcall;
  TVpeSetTplLeftMargin = procedure(hTemplate: LongInt; page: Integer; margin: Double) stdcall;
  TVpeGetTplRightMargin = function(hTemplate: LongInt; page: Integer): Double stdcall;
  TVpeSetTplRightMargin = procedure(hTemplate: LongInt; page: Integer; margin: Double) stdcall;
  TVpeGetTplTopMargin = function(hTemplate: LongInt; page: Integer): Double stdcall;
  TVpeSetTplTopMargin = procedure(hTemplate: LongInt; page: Integer; margin: Double) stdcall;
  TVpeGetTplBottomMargin = function(hTemplate: LongInt; page: Integer): Double stdcall;
  TVpeSetTplBottomMargin = procedure(hTemplate: LongInt; page: Integer; margin: Double) stdcall;
  TVpeGetTplDataSourceCount = function(hTemplate: LongInt): LongInt stdcall;
  TVpeGetTplDataSourceObject = function(hTemplate: LongInt; data_source_index: LongInt): LongInt stdcall;
  TVpeGetTplDataSourceFileName = procedure(hTemplate: LongInt; data_source_index: LongInt; 
      value: PChar; size: PUINT) stdcall;
  TVpeGetTplDataSourcePrefix = procedure(hTemplate: LongInt; data_source_index: LongInt; 
      value: PChar; size: PUINT) stdcall;
  TVpeGetTplDataSourceDescription = procedure(hTemplate: LongInt; data_source_index: LongInt; 
      value: PChar; size: PUINT) stdcall;
  TVpeGetTplFieldCount = function(hTemplate: LongInt; data_source_index: LongInt): LongInt stdcall;
  TVpeGetTplFieldObject = function(hTemplate: LongInt; data_source_index: LongInt; 
      field_index: LongInt): LongInt stdcall;
  TVpeGetTplFieldName = procedure(hTemplate: LongInt; data_source_index: LongInt; 
      field_index: LongInt; value: PChar; size: PUINT) stdcall;
  TVpeGetTplFieldDescription = procedure(hTemplate: LongInt; data_source_index: LongInt; 
      field_index: LongInt; value: PChar; size: PUINT) stdcall;
  TVpeFindTplFieldObject = function(hTemplate: LongInt; data_source_prefix: PChar; 
      field_name: PChar): LongInt stdcall;
  TVpeClearTplFields = procedure(hTemplate: LongInt) stdcall;
  TVpeGetTplVpeObjectCount = function(hTemplate: LongInt; page: Integer): LongInt stdcall;
  TVpeGetTplVpeObject = function(hTemplate: LongInt; page: Integer; index: LongInt): LongInt stdcall;
  TVpeFindTplVpeObject = function(hTemplate: LongInt; object_name: PChar): LongInt stdcall;
  TVpeGetFieldAsString = function(hField: LongInt; value: PChar; size: PUINT): Integer stdcall;
  TVpeSetFieldAsString = function(hField: LongInt; value: PChar): Integer stdcall;
  TVpeGetFieldAsInteger = function(hField: LongInt; value: PInteger): Integer stdcall;
  TVpeSetFieldAsInteger = function(hField: LongInt; value: Integer): Integer stdcall;
  TVpeGetFieldAsNumber = function(hField: LongInt; value: PDouble): Integer stdcall;
  TVpeSetFieldAsNumber = function(hField: LongInt; value: Double): Integer stdcall;
  TVpeGetFieldAsDateTime = function(hField: LongInt; year: PInteger; month: PInteger; 
      day: PInteger; hour: PInteger; minute: PInteger; second: PInteger; msec: PInteger): Integer stdcall;
  TVpeSetFieldAsDateTime = function(hField: LongInt; year: Integer; month: Integer; 
      day: Integer; hour: Integer; minute: Integer; second: Integer; msec: Integer): Integer stdcall;
  TVpeGetFieldAsOleDateTime = function(hField: LongInt; value: PDouble): Integer stdcall;
  TVpeSetFieldAsOleDateTime = function(hField: LongInt; value: Double): Integer stdcall;
  TVpeGetFieldAsJavaDateTime = function(hField: LongInt; value: PDouble): Integer stdcall;
  TVpeSetFieldAsJavaDateTime = function(hField: LongInt; value: Double): Integer stdcall;
  TVpeGetFieldAsBoolean = function(hField: LongInt; value: PInteger): Integer stdcall;
  TVpeSetFieldAsBoolean = function(hField: LongInt; value: Integer): Integer stdcall;
  TVpeGetFieldName = procedure(hField: LongInt; value: PChar; size: PUINT) stdcall;
  TVpeGetFieldDescription = procedure(hField: LongInt; value: PChar; size: PUINT) stdcall;
  TVpeGetFieldDataSourceObject = function(hField: LongInt): LongInt stdcall;
  TVpeGetDataSourceFileName = procedure(hDataSource: LongInt; value: PChar; size: PUINT) stdcall;
  TVpeGetDataSourcePrefix = procedure(hDataSource: LongInt; value: PChar; size: PUINT) stdcall;
  TVpeGetDataSourceDescription = procedure(hDataSource: LongInt; value: PChar; size: PUINT) stdcall;
  TVpeGetDataSourceFieldCount = function(hDataSource: LongInt): LongInt stdcall;
  TVpeGetDataSourceFieldObject = function(hDataSource: LongInt; field_index: LongInt): LongInt stdcall;
  TVpeGetInsertedVpeObjectCount = function(hTplVpeObject: LongInt): LongInt stdcall;
  TVpeGetInsertedVpeObject = function(hTplVpeObject: LongInt; index: LongInt): LongInt stdcall;
  TVpeGetInsertedVpeObjectPageNo = function(hTplVpeObject: LongInt; index: LongInt): Integer stdcall;
  TVpeGetObjName = procedure(hObject: LongInt; value: PChar; size: PUINT) stdcall;
  TVpeGetObjTemplateObject = function(hObject: LongInt): LongInt stdcall;
  TVpeGetObjKind = function(hObject: LongInt): LongInt stdcall;
  TVpeGetObjText = procedure(hObject: LongInt; value: PChar; size: PUINT) stdcall;
  TVpeGetObjResolvedText = procedure(hObject: LongInt; value: PChar; size: PUINT) stdcall;
  TVpeGetObjLeft = function(hObject: LongInt): Double stdcall;
  TVpeGetObjTop = function(hObject: LongInt): Double stdcall;
  TVpeGetObjRight = function(hObject: LongInt): Double stdcall;
  TVpeGetObjBottom = function(hObject: LongInt): Double stdcall;
  TVpeGetObjPictureFileName = procedure(hObject: LongInt; value: PChar; size: PUINT) stdcall;
  TVpeSetObjPictureFileName = procedure(hObject: LongInt; value: PChar) stdcall;
  TVpeGetNextObject = function(hObject: LongInt): LongInt stdcall;
  TVpeGetTplPageObject = function(hTemplate: LongInt; index: LongInt): LongInt stdcall;
  TVpeGetTplPageObjWidth = function(hPage: LongInt): Double stdcall;
  TVpeSetTplPageObjWidth = procedure(hPage: LongInt; width: Double) stdcall;
  TVpeGetTplPageObjHeight = function(hPage: LongInt): Double stdcall;
  TVpeSetTplPageObjHeight = procedure(hPage: LongInt; height: Double) stdcall;
  TVpeGetTplPageObjOrientation = function(hPage: LongInt): Integer stdcall;
  TVpeSetTplPageObjOrientation = procedure(hPage: LongInt; orientation: Integer) stdcall;
  TVpeGetTplPageObjPaperBin = function(hPage: LongInt): Integer stdcall;
  TVpeSetTplPageObjPaperBin = procedure(hPage: LongInt; bin: Integer) stdcall;
  TVpeGetTplPageObjLeftMargin = function(hPage: LongInt): Double stdcall;
  TVpeSetTplPageObjLeftMargin = procedure(hPage: LongInt; margin: Double) stdcall;
  TVpeGetTplPageObjRightMargin = function(hPage: LongInt): Double stdcall;
  TVpeSetTplPageObjRightMargin = procedure(hPage: LongInt; margin: Double) stdcall;
  TVpeGetTplPageObjTopMargin = function(hPage: LongInt): Double stdcall;
  TVpeSetTplPageObjTopMargin = procedure(hPage: LongInt; margin: Double) stdcall;
  TVpeGetTplPageObjBottomMargin = function(hPage: LongInt): Double stdcall;
  TVpeSetTplPageObjBottomMargin = procedure(hPage: LongInt; margin: Double) stdcall;
  TVpeGetTplPageObjVpeObjectCount = function(hPage: LongInt): LongInt stdcall;
  TVpeGetTplPageObjVpeObject = function(hPage: LongInt; index: LongInt): LongInt stdcall;
  TVpeLoadTemplateU = function(hDoc: LongInt; file_name: PChar): LongInt stdcall;
  TVpeLoadTemplateAuthKeyU = function(hDoc: LongInt; file_name: PChar; key1: LongInt; 
      key2: LongInt; key3: LongInt; key4: LongInt): LongInt stdcall;
  TVpeFormFieldU = function(hDoc: LongInt; x: Double; y: Double; x2: Double; y2: Double; 
      s: PChar): Double stdcall;
  TVpeRenderFormFieldU = function(hDoc: LongInt; x: Double; y: Double; x2: Double; 
      y2: Double; s: PChar): Integer stdcall;
  TVpeGetTplFieldAsStringU = function(hTemplate: LongInt; data_source_prefix: PChar; 
      field_name: PChar; value: PChar; size: PUINT): Integer stdcall;
  TVpeSetTplFieldAsStringU = function(hTemplate: LongInt; data_source_prefix: PChar; 
      field_name: PChar; value: PChar): Integer stdcall;
  TVpeGetTplFieldAsIntegerU = function(hTemplate: LongInt; data_source_prefix: PChar; 
      field_name: PChar; value: PInteger): Integer stdcall;
  TVpeSetTplFieldAsIntegerU = function(hTemplate: LongInt; data_source_prefix: PChar; 
      field_name: PChar; value: Integer): Integer stdcall;
  TVpeGetTplFieldAsNumberU = function(hTemplate: LongInt; data_source_prefix: PChar; 
      field_name: PChar; value: PDouble): Integer stdcall;
  TVpeSetTplFieldAsNumberU = function(hTemplate: LongInt; data_source_prefix: PChar; 
      field_name: PChar; value: Double): Integer stdcall;
  TVpeGetTplFieldAsDateTimeU = function(hTemplate: LongInt; data_source_prefix: PChar; 
      field_name: PChar; year: PInteger; month: PInteger; day: PInteger; hour: PInteger; 
      minute: PInteger; second: PInteger; msec: PInteger): Integer stdcall;
  TVpeSetTplFieldAsDateTimeU = function(hTemplate: LongInt; data_source_prefix: PChar; 
      field_name: PChar; year: Integer; month: Integer; day: Integer; hour: Integer; minute: Integer; 
      second: Integer; msec: Integer): Integer stdcall;
  TVpeGetTplFieldAsOleDateTimeU = function(hTemplate: LongInt; data_source_prefix: PChar; 
      field_name: PChar; value: PDouble): Integer stdcall;
  TVpeSetTplFieldAsOleDateTimeU = function(hTemplate: LongInt; data_source_prefix: PChar; 
      field_name: PChar; value: Double): Integer stdcall;
  TVpeGetTplFieldAsJavaDateTimeU = function(hTemplate: LongInt; data_source_prefix: PChar; 
      field_name: PChar; value: PDouble): Integer stdcall;
  TVpeSetTplFieldAsJavaDateTimeU = function(hTemplate: LongInt; data_source_prefix: PChar; 
      field_name: PChar; value: Double): Integer stdcall;
  TVpeGetTplFieldAsBooleanU = function(hTemplate: LongInt; data_source_prefix: PChar; 
      field_name: PChar; value: PInteger): Integer stdcall;
  TVpeSetTplFieldAsBooleanU = function(hTemplate: LongInt; data_source_prefix: PChar; 
      field_name: PChar; value: Integer): Integer stdcall;
  TVpeGetTplDataSourceFileNameU = procedure(hTemplate: LongInt; data_source_index: LongInt; 
      value: PChar; size: PUINT) stdcall;
  TVpeGetTplDataSourcePrefixU = procedure(hTemplate: LongInt; data_source_index: LongInt; 
      value: PChar; size: PUINT) stdcall;
  TVpeGetTplDataSourceDescriptionU = procedure(hTemplate: LongInt; data_source_index: LongInt; 
      value: PChar; size: PUINT) stdcall;
  TVpeGetTplFieldNameU = procedure(hTemplate: LongInt; data_source_index: LongInt; 
      field_index: LongInt; value: PChar; size: PUINT) stdcall;
  TVpeGetTplFieldDescriptionU = procedure(hTemplate: LongInt; data_source_index: LongInt; 
      field_index: LongInt; value: PChar; size: PUINT) stdcall;
  TVpeFindTplFieldObjectU = function(hTemplate: LongInt; data_source_prefix: PChar; 
      field_name: PChar): LongInt stdcall;
  TVpeFindTplVpeObjectU = function(hTemplate: LongInt; object_name: PChar): LongInt stdcall;
  TVpeGetFieldAsStringU = function(hField: LongInt; value: PChar; size: PUINT): Integer stdcall;
  TVpeSetFieldAsStringU = function(hField: LongInt; value: PChar): Integer stdcall;
  TVpeGetFieldNameU = procedure(hField: LongInt; value: PChar; size: PUINT) stdcall;
  TVpeGetFieldDescriptionU = procedure(hField: LongInt; value: PChar; size: PUINT) stdcall;
  TVpeGetDataSourceFileNameU = procedure(hDataSource: LongInt; value: PChar; size: PUINT) stdcall;
  TVpeGetDataSourcePrefixU = procedure(hDataSource: LongInt; value: PChar; size: PUINT) stdcall;
  TVpeGetDataSourceDescriptionU = procedure(hDataSource: LongInt; value: PChar; 
      size: PUINT) stdcall;
  TVpeGetObjNameU = procedure(hObject: LongInt; value: PChar; size: PUINT) stdcall;
  TVpeGetObjTextU = procedure(hObject: LongInt; value: PChar; size: PUINT) stdcall;
  TVpeGetObjResolvedTextU = procedure(hObject: LongInt; value: PChar; size: PUINT) stdcall;
  TVpeGetObjPictureFileNameU = procedure(hObject: LongInt; value: PChar; size: PUINT) stdcall;
  TVpeSetObjPictureFileNameU = procedure(hObject: LongInt; value: PChar) stdcall;
{ TVpeXyz = function(hDoc: LongInt; ...): RET_TYPE }



   { ================================= }
   { declare the functions themselves: }
   { ================================= }
var
  VpeLicense: TVpeLicense;
  VpeGetVersion: TVpeGetVersion;
  VpeGetReleaseNumber: TVpeGetReleaseNumber;
  VpeGetBuildNumber: TVpeGetBuildNumber;
  VpeGetEdition: TVpeGetEdition;
  VpeOpenDoc: TVpeOpenDoc;
  VpeOpenDocFile: TVpeOpenDocFile;
  VpeCloseDoc: TVpeCloseDoc;
  VpeSetPageFormat: TVpeSetPageFormat;
  VpeGetPageFormat: TVpeGetPageFormat;
  VpeSetPageWidth: TVpeSetPageWidth;
  VpeSetPageHeight: TVpeSetPageHeight;
  VpeGetPageWidth: TVpeGetPageWidth;
  VpeGetPageHeight: TVpeGetPageHeight;
  VpeSetPageOrientation: TVpeSetPageOrientation;
  VpeGetPageOrientation: TVpeGetPageOrientation;
  VpeSetPaperBin: TVpeSetPaperBin;
  VpeGetPaperBin: TVpeGetPaperBin;
  VpeSetDefOutRect: TVpeSetDefOutRect;
  VpeSetOutRect: TVpeSetOutRect;
  VpeGet: TVpeGet;
  VpeSet: TVpeSet;
  VpeStorePos: TVpeStorePos;
  VpeRestorePos: TVpeRestorePos;
  VpeSetPen: TVpeSetPen;
  VpeSetPenSize: TVpeSetPenSize;
  VpeGetPenSize: TVpeGetPenSize;
  VpePenSize: TVpePenSize;
  VpeSetPenStyle: TVpeSetPenStyle;
  VpePenStyle: TVpePenStyle;
  VpeSetPenColor: TVpeSetPenColor;
  VpePenColor: TVpePenColor;
  VpeNoPen: TVpeNoPen;
  VpeLine: TVpeLine;
  VpePolyLine: TVpePolyLine;
  VpeAddPolyPoint: TVpeAddPolyPoint;
  VpePolygon: TVpePolygon;
  VpeAddPolygonPoint: TVpeAddPolygonPoint;
  VpeSetBkgColor: TVpeSetBkgColor;
  VpeSetBkgGradientStartColor: TVpeSetBkgGradientStartColor;
  VpeSetBkgGradientEndColor: TVpeSetBkgGradientEndColor;
  VpeSetBkgGradientRotation: TVpeSetBkgGradientRotation;
  VpeSetBkgGradientPrint: TVpeSetBkgGradientPrint;
  VpeSetBkgGradientPrintSolidColor: TVpeSetBkgGradientPrintSolidColor;
  VpeSetTransparentMode: TVpeSetTransparentMode;
  VpeSetBkgMode: TVpeSetBkgMode;
  VpeSetHatchStyle: TVpeSetHatchStyle;
  VpeSetHatchColor: TVpeSetHatchColor;
  VpeBox: TVpeBox;
  VpeEllipse: TVpeEllipse;
  VpePie: TVpePie;
  VpeSetBarcodeParms: TVpeSetBarcodeParms;
  VpeSetBarcodeAlignment: TVpeSetBarcodeAlignment;
  VpeBarcode: TVpeBarcode;
  VpeGetPictureTypes: TVpeGetPictureTypes;
  VpePicture: TVpePicture;
  VpeSetFont: TVpeSetFont;
  VpeSelectFont: TVpeSelectFont;
  VpeSetFontName: TVpeSetFontName;
  VpeSetFontSize: TVpeSetFontSize;
  VpeSetCharset: TVpeSetCharset;
  VpeSetFontAttr: TVpeSetFontAttr;
  VpeSetTextAlignment: TVpeSetTextAlignment;
  VpeSetAlign: TVpeSetAlign;
  VpeSetBold: TVpeSetBold;
  VpeSetUnderlined: TVpeSetUnderlined;
  VpeSetStrikeOut: TVpeSetStrikeOut;
  VpeSetItalic: TVpeSetItalic;
  VpeSetTextColor: TVpeSetTextColor;
  VpePrint: TVpePrint;
  VpePrintBox: TVpePrintBox;
  VpeWrite: TVpeWrite;
  VpeWriteBox: TVpeWriteBox;
  VpeDefineHeader: TVpeDefineHeader;
  VpeDefineFooter: TVpeDefineFooter;
  VpePageBreak: TVpePageBreak;
  VpeGetPageCount: TVpeGetPageCount;
  VpeGetCurrentPage: TVpeGetCurrentPage;
  VpeGotoPage: TVpeGotoPage;
  VpeStoreSet: TVpeStoreSet;
  VpeUseSet: TVpeUseSet;
  VpeRemoveSet: TVpeRemoveSet;
  VpeSetAutoBreak: TVpeSetAutoBreak;
  VpeWriteDoc: TVpeWriteDoc;
  VpeReadDoc: TVpeReadDoc;
  VpeSetRotation: TVpeSetRotation;
  VpeSetPictureCacheSize: TVpeSetPictureCacheSize;
  VpeSetPicCacheSize: TVpeSetPicCacheSize;
  VpeGetPictureCacheSize: TVpeGetPictureCacheSize;
  VpeGetPicCacheSize: TVpeGetPicCacheSize;
  VpeGetPictureCacheUsed: TVpeGetPictureCacheUsed;
  VpeGetPicCacheUsed: TVpeGetPicCacheUsed;
  VpeSetPrintOffset: TVpeSetPrintOffset;
  VpeSetPrintOffsetX: TVpeSetPrintOffsetX;
  VpeSetPrintOffsetY: TVpeSetPrintOffsetY;
  VpeGetPrintOffsetX: TVpeGetPrintOffsetX;
  VpeGetPrintOffsetY: TVpeGetPrintOffsetY;
  VpeGetLastError: TVpeGetLastError;
  VpeRenderPrint: TVpeRenderPrint;
  VpeRenderPrintBox: TVpeRenderPrintBox;
  VpeRenderWrite: TVpeRenderWrite;
  VpeRenderWriteBox: TVpeRenderWriteBox;
  VpeRenderPicture: TVpeRenderPicture;
  VpeGetPicturePageCount: TVpeGetPicturePageCount;
  VpeSetPicturePage: TVpeSetPicturePage;
  VpeGetPicturePage: TVpeGetPicturePage;
  VpeSetPictureType: TVpeSetPictureType;
  VpeSetDocFileReadOnly: TVpeSetDocFileReadOnly;
  VpeGetPenStyle: TVpeGetPenStyle;
  VpeGetPenColor: TVpeGetPenColor;
  VpeGetBkgMode: TVpeGetBkgMode;
  VpeGetBkgColor: TVpeGetBkgColor;
  VpeGetHatchStyle: TVpeGetHatchStyle;
  VpeGetHatchColor: TVpeGetHatchColor;
  VpeGetBkgGradientStartColor: TVpeGetBkgGradientStartColor;
  VpeGetBkgGradientEndColor: TVpeGetBkgGradientEndColor;
  VpeGetBkgGradientRotation: TVpeGetBkgGradientRotation;
  VpeGetFontName: TVpeGetFontName;
  VpeGetFontSize: TVpeGetFontSize;
  VpeGetCharset: TVpeGetCharset;
  VpeGetTextAlignment: TVpeGetTextAlignment;
  VpeGetBold: TVpeGetBold;
  VpeGetUnderlined: TVpeGetUnderlined;
  VpeGetStrikeOut: TVpeGetStrikeOut;
  VpeGetItalic: TVpeGetItalic;
  VpeGetTextColor: TVpeGetTextColor;
  VpeGetRotation: TVpeGetRotation;
  VpeSetBarcodeMainTextParms: TVpeSetBarcodeMainTextParms;
  VpeGetBarcodeMainTextParms: TVpeGetBarcodeMainTextParms;
  VpeSetBarcodeAddTextParms: TVpeSetBarcodeAddTextParms;
  VpeGetBarcodeAddTextParms: TVpeGetBarcodeAddTextParms;
  VpeGetBarcodeAlignment: TVpeGetBarcodeAlignment;
  VpeSetBarcodeAutoChecksum: TVpeSetBarcodeAutoChecksum;
  VpeGetBarcodeAutoChecksum: TVpeGetBarcodeAutoChecksum;
  VpeSetBarcodeThinBar: TVpeSetBarcodeThinBar;
  VpeGetBarcodeThinBar: TVpeGetBarcodeThinBar;
  VpeSetBarcodeThickBar: TVpeSetBarcodeThickBar;
  VpeGetBarcodeThickBar: TVpeGetBarcodeThickBar;
  VpeGetPictureType: TVpeGetPictureType;
  VpeGetTransparentMode: TVpeGetTransparentMode;
  VpeSetUnderline: TVpeSetUnderline;
  VpeGetUnderline: TVpeGetUnderline;
  VpeGetAutoBreak: TVpeGetAutoBreak;
  VpeGetDocExportPictureQuality: TVpeGetDocExportPictureQuality;
  VpeSetDocExportPictureQuality: TVpeSetDocExportPictureQuality;
  VpeSetDocExportType: TVpeSetDocExportType;
  VpeGetDocExportType: TVpeGetDocExportType;
  VpeGetDocExportPictureResolution: TVpeGetDocExportPictureResolution;
  VpeSetDocExportPictureResolution: TVpeSetDocExportPictureResolution;
  VpeGetFastWebView: TVpeGetFastWebView;
  VpeSetFastWebView: TVpeSetFastWebView;
  VpeGetUseTempFiles: TVpeGetUseTempFiles;
  VpeSetUseTempFiles: TVpeSetUseTempFiles;
  VpeGetCompression: TVpeGetCompression;
  VpeSetCompression: TVpeSetCompression;
  VpeGetEmbedAllFonts: TVpeGetEmbedAllFonts;
  VpeSetEmbedAllFonts: TVpeSetEmbedAllFonts;
  VpeGetSubsetAllFonts: TVpeGetSubsetAllFonts;
  VpeSetSubsetAllFonts: TVpeSetSubsetAllFonts;
  VpeSetFontControl: TVpeSetFontControl;
  VpeResetFontControl: TVpeResetFontControl;
  VpeGetProtection: TVpeGetProtection;
  VpeSetProtection: TVpeSetProtection;
  VpeGetEncryption: TVpeGetEncryption;
  VpeSetEncryption: TVpeSetEncryption;
  VpeGetEncryptionKeyLength: TVpeGetEncryptionKeyLength;
  VpeSetEncryptionKeyLength: TVpeSetEncryptionKeyLength;
  VpeSetUserPassword: TVpeSetUserPassword;
  VpeSetOwnerPassword: TVpeSetOwnerPassword;
  VpeSetAuthor: TVpeSetAuthor;
  VpeSetTitle: TVpeSetTitle;
  VpeSetSubject: TVpeSetSubject;
  VpeSetKeywords: TVpeSetKeywords;
  VpeSetCreator: TVpeSetCreator;
  VpeEnableMultiThreading: TVpeEnableMultiThreading;
  VpeGetPDFVersion: TVpeGetPDFVersion;
  VpeSetPDFVersion: TVpeSetPDFVersion;
  VpeSetBookmarkDestination: TVpeSetBookmarkDestination;
  VpeGetBookmarkStyle: TVpeGetBookmarkStyle;
  VpeSetBookmarkStyle: TVpeSetBookmarkStyle;
  VpeGetBookmarkColor: TVpeGetBookmarkColor;
  VpeSetBookmarkColor: TVpeSetBookmarkColor;
  VpeAddBookmark: TVpeAddBookmark;
  VpeExtIntDA: TVpeExtIntDA;
  VpeSetPictureCache: TVpeSetPictureCache;
  VpeGetPictureCache: TVpeGetPictureCache;
  VpeSetPictureX2YResolution: TVpeSetPictureX2YResolution;
  VpeGetPictureX2YResolution: TVpeGetPictureX2YResolution;
  VpeSetPictureBestFit: TVpeSetPictureBestFit;
  VpeGetPictureBestFit: TVpeGetPictureBestFit;
  VpeSetPictureEmbedInDoc: TVpeSetPictureEmbedInDoc;
  VpeGetPictureEmbedInDoc: TVpeGetPictureEmbedInDoc;
  VpeSetPictureDrawExact: TVpeSetPictureDrawExact;
  VpeGetPictureDrawExact: TVpeGetPictureDrawExact;
  VpeSetPictureKeepAspect: TVpeSetPictureKeepAspect;
  VpeGetPictureKeepAspect: TVpeGetPictureKeepAspect;
  VpeSetPictureDefaultDPI: TVpeSetPictureDefaultDPI;
  VpeSetCornerRadius: TVpeSetCornerRadius;
  VpeGetCornerRadius: TVpeGetCornerRadius;
  VpeSetUnitTransformation: TVpeSetUnitTransformation;
  VpeGetUnitTransformation: TVpeGetUnitTransformation;
  VpeSetBkgGradientTriColor: TVpeSetBkgGradientTriColor;
  VpeGetBkgGradientTriColor: TVpeGetBkgGradientTriColor;
  VpeSetBkgGradientMiddleColor: TVpeSetBkgGradientMiddleColor;
  VpeGetBkgGradientMiddleColor: TVpeGetBkgGradientMiddleColor;
  VpeSetBkgGradientMiddleColorPosition: TVpeSetBkgGradientMiddleColorPosition;
  VpeGetBkgGradientMiddleColorPosition: TVpeGetBkgGradientMiddleColorPosition;
  VpeSetMsgCallback: TVpeSetMsgCallback;
  VpeSetFontSubstitution: TVpeSetFontSubstitution;
  VpePurgeFontSubstitution: TVpePurgeFontSubstitution;
  VpeLicenseU: TVpeLicenseU;
  VpeOpenDocU: TVpeOpenDocU;
  VpeOpenDocFileU: TVpeOpenDocFileU;
  VpeBarcodeU: TVpeBarcodeU;
  VpeGetPictureTypesU: TVpeGetPictureTypesU;
  VpePictureU: TVpePictureU;
  VpeSetFontU: TVpeSetFontU;
  VpeSelectFontU: TVpeSelectFontU;
  VpeSetFontNameU: TVpeSetFontNameU;
  VpePrintU: TVpePrintU;
  VpePrintBoxU: TVpePrintBoxU;
  VpeWriteU: TVpeWriteU;
  VpeWriteBoxU: TVpeWriteBoxU;
  VpeDefineHeaderU: TVpeDefineHeaderU;
  VpeDefineFooterU: TVpeDefineFooterU;
  VpeWriteDocU: TVpeWriteDocU;
  VpeReadDocU: TVpeReadDocU;
  VpeRenderPrintU: TVpeRenderPrintU;
  VpeRenderPrintBoxU: TVpeRenderPrintBoxU;
  VpeRenderWriteU: TVpeRenderWriteU;
  VpeRenderWriteBoxU: TVpeRenderWriteBoxU;
  VpeRenderPictureU: TVpeRenderPictureU;
  VpeGetPicturePageCountU: TVpeGetPicturePageCountU;
  VpeGetFontNameU: TVpeGetFontNameU;
  VpeSetFontControlU: TVpeSetFontControlU;
  VpeSetUserPasswordU: TVpeSetUserPasswordU;
  VpeSetOwnerPasswordU: TVpeSetOwnerPasswordU;
  VpeSetAuthorU: TVpeSetAuthorU;
  VpeSetTitleU: TVpeSetTitleU;
  VpeSetSubjectU: TVpeSetSubjectU;
  VpeSetKeywordsU: TVpeSetKeywordsU;
  VpeSetCreatorU: TVpeSetCreatorU;
  VpeAddBookmarkU: TVpeAddBookmarkU;
  VpeSetFontSubstitutionU: TVpeSetFontSubstitutionU;
  VpeGetWindowHandle: TVpeGetWindowHandle;
  VpeWindowHandle: TVpeWindowHandle;
  VpeEnablePrintSetupDialog: TVpeEnablePrintSetupDialog;
  VpeSetGridMode: TVpeSetGridMode;
  VpeSetGridVisible: TVpeSetGridVisible;
  VpeEnableMailButton: TVpeEnableMailButton;
  VpeEnableCloseButton: TVpeEnableCloseButton;
  VpeEnableHelpRouting: TVpeEnableHelpRouting;
  VpeSetPreviewWithScrollers: TVpeSetPreviewWithScrollers;
  VpeSetPaperView: TVpeSetPaperView;
  VpeSetScale: TVpeSetScale;
  VpeGetScale: TVpeGetScale;
  VpeSetScalePercent: TVpeSetScalePercent;
  VpeGetScalePercent: TVpeGetScalePercent;
  VpeSetRulersMeasure: TVpeSetRulersMeasure;
  VpeSetupPrinter: TVpeSetupPrinter;
  VpeSetPrintOptions: TVpeSetPrintOptions;
  VpePrintDoc: TVpePrintDoc;
  VpeIsPrinting: TVpeIsPrinting;
  VpePreviewDoc: TVpePreviewDoc;
  VpePreviewDocSP: TVpePreviewDocSP;
  VpeCenterPreview: TVpeCenterPreview;
  VpeRefreshDoc: TVpeRefreshDoc;
  VpePictureDIB: TVpePictureDIB;
  VpePictureResID: TVpePictureResID;
  VpePictureResName: TVpePictureResName;
  VpeSetPrintPosMode: TVpeSetPrintPosMode;
  VpeSetPageScrollerTracking: TVpeSetPageScrollerTracking;
  VpeWriteStatusbar: TVpeWriteStatusbar;
  VpeOpenProgressBar: TVpeOpenProgressBar;
  VpeSetProgressBar: TVpeSetProgressBar;
  VpeCloseProgressBar: TVpeCloseProgressBar;
  VpeRenderPictureDIB: TVpeRenderPictureDIB;
  VpeRenderPictureResID: TVpeRenderPictureResID;
  VpeRenderPictureResName: TVpeRenderPictureResName;
  VpeSetDevice: TVpeSetDevice;
  VpeGetDevice: TVpeGetDevice;
  VpeDevEnum: TVpeDevEnum;
  VpeGetDevEntry: TVpeGetDevEntry;
  VpeSetDevOrientation: TVpeSetDevOrientation;
  VpeGetDevOrientation: TVpeGetDevOrientation;
  VpeSetDevPaperFormat: TVpeSetDevPaperFormat;
  VpeGetDevPaperFormat: TVpeGetDevPaperFormat;
  VpeSetDevPaperWidth: TVpeSetDevPaperWidth;
  VpeGetDevPaperWidth: TVpeGetDevPaperWidth;
  VpeSetDevPaperHeight: TVpeSetDevPaperHeight;
  VpeGetDevPaperHeight: TVpeGetDevPaperHeight;
  VpeSetDevScalePercent: TVpeSetDevScalePercent;
  VpeGetDevScalePercent: TVpeGetDevScalePercent;
  VpeSetDevPrintQuality: TVpeSetDevPrintQuality;
  VpeGetDevPrintQuality: TVpeGetDevPrintQuality;
  VpeSetDevYResolution: TVpeSetDevYResolution;
  VpeGetDevYResolution: TVpeGetDevYResolution;
  VpeSetDevColor: TVpeSetDevColor;
  VpeGetDevColor: TVpeGetDevColor;
  VpeSetDevDuplex: TVpeSetDevDuplex;
  VpeGetDevDuplex: TVpeGetDevDuplex;
  VpeSetDevTTOption: TVpeSetDevTTOption;
  VpeGetDevTTOption: TVpeGetDevTTOption;
  VpeDevEnumPaperBins: TVpeDevEnumPaperBins;
  VpeGetDevPaperBinName: TVpeGetDevPaperBinName;
  VpeGetDevPaperBinID: TVpeGetDevPaperBinID;
  VpeSetDevPaperBin: TVpeSetDevPaperBin;
  VpeGetDevPaperBin: TVpeGetDevPaperBin;
  VpeSetDevCopies: TVpeSetDevCopies;
  VpeGetDevCopies: TVpeGetDevCopies;
  VpeSetDevCollate: TVpeSetDevCollate;
  VpeGetDevCollate: TVpeGetDevCollate;
  VpeGetDevPrinterOffsetX: TVpeGetDevPrinterOffsetX;
  VpeGetDevPrinterOffsetY: TVpeGetDevPrinterOffsetY;
  VpeGetDevPhysPageWidth: TVpeGetDevPhysPageWidth;
  VpeGetDevPhysPageHeight: TVpeGetDevPhysPageHeight;
  VpeGetDevPrintableWidth: TVpeGetDevPrintableWidth;
  VpeGetDevPrintableHeight: TVpeGetDevPrintableHeight;
  VpeSetDevFromPage: TVpeSetDevFromPage;
  VpeGetDevFromPage: TVpeGetDevFromPage;
  VpeSetDevToPage: TVpeSetDevToPage;
  VpeGetDevToPage: TVpeGetDevToPage;
  VpeSetDevToFile: TVpeSetDevToFile;
  VpeGetDevToFile: TVpeGetDevToFile;
  VpeSetDevFileName: TVpeSetDevFileName;
  VpeGetDevFileName: TVpeGetDevFileName;
  VpeSetDevJobName: TVpeSetDevJobName;
  VpeGetDevJobName: TVpeGetDevJobName;
  VpeDevSendData: TVpeDevSendData;
  VpeReadPrinterSetup: TVpeReadPrinterSetup;
  VpeWritePrinterSetup: TVpeWritePrinterSetup;
  VpeDefineKey: TVpeDefineKey;
  VpeSendKey: TVpeSendKey;
  VpeSetGUILanguage: TVpeSetGUILanguage;
  VpeSetPreviewCtrl: TVpeSetPreviewCtrl;
  VpeEnableAutoDelete: TVpeEnableAutoDelete;
  VpeClosePreview: TVpeClosePreview;
  VpeIsPreviewVisible: TVpeIsPreviewVisible;
  VpeGotoVisualPage: TVpeGotoVisualPage;
  VpeGetVisualPage: TVpeGetVisualPage;
  VpeDispatchAllMessages: TVpeDispatchAllMessages;
  VpeSetBusyProgressBar: TVpeSetBusyProgressBar;
  VpeSetMailWithDialog: TVpeSetMailWithDialog;
  VpeSetMailSubject: TVpeSetMailSubject;
  VpeSetMailText: TVpeSetMailText;
  VpeSetMailSender: TVpeSetMailSender;
  VpeAddMailReceiver: TVpeAddMailReceiver;
  VpeClearMailReceivers: TVpeClearMailReceivers;
  VpeAddMailAttachment: TVpeAddMailAttachment;
  VpeClearMailAttachments: TVpeClearMailAttachments;
  VpeSetMailAutoAttachDocType: TVpeSetMailAutoAttachDocType;
  VpeGetMailAutoAttachDocType: TVpeGetMailAutoAttachDocType;
  VpeIsMAPIInstalled: TVpeIsMAPIInstalled;
  VpeGetMAPIType: TVpeGetMAPIType;
  VpeMailDoc: TVpeMailDoc;
  VpeMapMessage: TVpeMapMessage;
  VpeSetMinScale: TVpeSetMinScale;
  VpeSetMinScalePercent: TVpeSetMinScalePercent;
  VpeSetMaxScale: TVpeSetMaxScale;
  VpeSetMaxScalePercent: TVpeSetMaxScalePercent;
  VpeSetResourceString: TVpeSetResourceString;
  VpeSetGUITheme: TVpeSetGUITheme;
  VpeGetGUITheme: TVpeGetGUITheme;
  VpeSetScaleMode: TVpeSetScaleMode;
  VpeGetScaleMode: TVpeGetScaleMode;
  VpeZoomPreview: TVpeZoomPreview;
  VpeSetPreviewPosition: TVpeSetPreviewPosition;
  VpeZoomIn: TVpeZoomIn;
  VpeZoomOut: TVpeZoomOut;
  VpeGetOpenFileName: TVpeGetOpenFileName;
  VpeSetOpenFileName: TVpeSetOpenFileName;
  VpeGetSaveFileName: TVpeGetSaveFileName;
  VpeSetSaveFileName: TVpeSetSaveFileName;
  VpeOpenFileDialog: TVpeOpenFileDialog;
  VpeSaveFileDialog: TVpeSaveFileDialog;
  VpeSetEngineRenderMode: TVpeSetEngineRenderMode;
  VpeGetEngineRenderMode: TVpeGetEngineRenderMode;
  VpeSetupPrinterU: TVpeSetupPrinterU;
  VpePictureResNameU: TVpePictureResNameU;
  VpeWriteStatusbarU: TVpeWriteStatusbarU;
  VpeRenderPictureResNameU: TVpeRenderPictureResNameU;
  VpeSetDeviceU: TVpeSetDeviceU;
  VpeGetDeviceU: TVpeGetDeviceU;
  VpeGetDevEntryU: TVpeGetDevEntryU;
  VpeGetDevPaperBinNameU: TVpeGetDevPaperBinNameU;
  VpeSetDevFileNameU: TVpeSetDevFileNameU;
  VpeGetDevFileNameU: TVpeGetDevFileNameU;
  VpeSetDevJobNameU: TVpeSetDevJobNameU;
  VpeGetDevJobNameU: TVpeGetDevJobNameU;
  VpeDevSendDataU: TVpeDevSendDataU;
  VpeReadPrinterSetupU: TVpeReadPrinterSetupU;
  VpeWritePrinterSetupU: TVpeWritePrinterSetupU;
  VpeSetMailSubjectU: TVpeSetMailSubjectU;
  VpeSetMailTextU: TVpeSetMailTextU;
  VpeSetMailSenderU: TVpeSetMailSenderU;
  VpeAddMailReceiverU: TVpeAddMailReceiverU;
  VpeAddMailAttachmentU: TVpeAddMailAttachmentU;
  VpeSetResourceStringU: TVpeSetResourceStringU;
  VpeGetOpenFileNameU: TVpeGetOpenFileNameU;
  VpeSetOpenFileNameU: TVpeSetOpenFileNameU;
  VpeGetSaveFileNameU: TVpeGetSaveFileNameU;
  VpeSetSaveFileNameU: TVpeSetSaveFileNameU;
  VpeSetViewable: TVpeSetViewable;
  VpeSetPrintable: TVpeSetPrintable;
  VpeSetStreamable: TVpeSetStreamable;
  VpeSetShadowed: TVpeSetShadowed;
  VpeWriteRTF: TVpeWriteRTF;
  VpeWriteBoxRTF: TVpeWriteBoxRTF;
  VpeWriteRTFFile: TVpeWriteRTFFile;
  VpeWriteBoxRTFFile: TVpeWriteBoxRTFFile;
  VpeSetFirstIndent: TVpeSetFirstIndent;
  VpeSetLeftIndent: TVpeSetLeftIndent;
  VpeSetRightIndent: TVpeSetRightIndent;
  VpeSetSpaceBefore: TVpeSetSpaceBefore;
  VpeSetSpaceAfter: TVpeSetSpaceAfter;
  VpeSetSpaceBetween: TVpeSetSpaceBetween;
  VpeSetDefaultTabSize: TVpeSetDefaultTabSize;
  VpeSetTab: TVpeSetTab;
  VpeClearTab: TVpeClearTab;
  VpeClearAllTabs: TVpeClearAllTabs;
  VpeSetKeepLines: TVpeSetKeepLines;
  VpeSetKeepNextParagraph: TVpeSetKeepNextParagraph;
  VpeSetParagraphControl: TVpeSetParagraphControl;
  VpeResetParagraph: TVpeResetParagraph;
  VpeSetRTFFont: TVpeSetRTFFont;
  VpeSetRTFColor: TVpeSetRTFColor;
  VpeRenderRTF: TVpeRenderRTF;
  VpeRenderBoxRTF: TVpeRenderBoxRTF;
  VpeRenderRTFFile: TVpeRenderRTFFile;
  VpeRenderBoxRTFFile: TVpeRenderBoxRTFFile;
  VpeSetCharPlacement: TVpeSetCharPlacement;
  VpeChartDataCreate: TVpeChartDataCreate;
  VpeChartDataAddRow: TVpeChartDataAddRow;
  VpeChartDataAddColumn: TVpeChartDataAddColumn;
  VpeChartDataAddValue: TVpeChartDataAddValue;
  VpeChartDataAddGap: TVpeChartDataAddGap;
  VpeChartDataAddLegend: TVpeChartDataAddLegend;
  VpeChartDataAddXLabel: TVpeChartDataAddXLabel;
  VpeChartDataAddYLabel: TVpeChartDataAddYLabel;
  VpeChartDataSetXAxisTitle: TVpeChartDataSetXAxisTitle;
  VpeChartDataSetYAxisTitle: TVpeChartDataSetYAxisTitle;
  VpeChartDataSetColor: TVpeChartDataSetColor;
  VpeChartDataSetLineStyle: TVpeChartDataSetLineStyle;
  VpeChartDataSetHatchStyle: TVpeChartDataSetHatchStyle;
  VpeChartDataSetPointType: TVpeChartDataSetPointType;
  VpeChartDataSetMaximum: TVpeChartDataSetMaximum;
  VpeChartDataSetMinimum: TVpeChartDataSetMinimum;
  VpeSetChartTitle: TVpeSetChartTitle;
  VpeSetChartSubTitle: TVpeSetChartSubTitle;
  VpeSetChartFootNote: TVpeSetChartFootNote;
  VpeSetChartRow: TVpeSetChartRow;
  VpeSetChartGridBkgColor: TVpeSetChartGridBkgColor;
  VpeSetChartGridBkgMode: TVpeSetChartGridBkgMode;
  VpeSetChartGridType: TVpeSetChartGridType;
  VpeSetChartGridColor: TVpeSetChartGridColor;
  VpeSetChartXGridStep: TVpeSetChartXGridStep;
  VpeSetChartYGridStep: TVpeSetChartYGridStep;
  VpeSetChartYAutoGridStep: TVpeSetChartYAutoGridStep;
  VpeSetChartLegendPosition: TVpeSetChartLegendPosition;
  VpeSetChartLegendBorderStat: TVpeSetChartLegendBorderStat;
  VpeSetChartXLabelState: TVpeSetChartXLabelState;
  VpeSetChartXLabelAngle: TVpeSetChartXLabelAngle;
  VpeSetChartYLabelState: TVpeSetChartYLabelState;
  VpeSetChartXLabelStartValue: TVpeSetChartXLabelStartValue;
  VpeSetChartXLabelStep: TVpeSetChartXLabelStep;
  VpeSetChartYLabelStep: TVpeSetChartYLabelStep;
  VpeSetChartYLabelDivisor: TVpeSetChartYLabelDivisor;
  VpeSetChartGridRotation: TVpeSetChartGridRotation;
  VpeSetChartYAxisAngle: TVpeSetChartYAxisAngle;
  VpeSetChartXAxisAngle: TVpeSetChartXAxisAngle;
  VpeChart: TVpeChart;
  VpeSetChartBarWidthFactor: TVpeSetChartBarWidthFactor;
  VpeSetChartLineWidthFactor: TVpeSetChartLineWidthFactor;
  VpeSetChartLegendFontSizeFactor: TVpeSetChartLegendFontSizeFactor;
  VpeSetChartXLabelFontSizeFactor: TVpeSetChartXLabelFontSizeFactor;
  VpeSetChartYLabelFontSizeFactor: TVpeSetChartYLabelFontSizeFactor;
  VpeSetChartTitleFontSizeFactor: TVpeSetChartTitleFontSizeFactor;
  VpeSetChartSubTitleFontSizeFactor: TVpeSetChartSubTitleFontSizeFactor;
  VpeSetChartFootNoteFontSizeFactor: TVpeSetChartFootNoteFontSizeFactor;
  VpeSetChartAxisTitleFontSizeFactor: TVpeSetChartAxisTitleFontSizeFactor;
  VpeSetChartPieLegendWithPercent: TVpeSetChartPieLegendWithPercent;
  VpeSetChartPieLabelType: TVpeSetChartPieLabelType;
  VpeSetEditProtection: TVpeSetEditProtection;
  VpeGetEditProtection: TVpeGetEditProtection;
  VpeGetViewable: TVpeGetViewable;
  VpeGetPrintable: TVpeGetPrintable;
  VpeGetStreamable: TVpeGetStreamable;
  VpeGetShadowed: TVpeGetShadowed;
  VpeSetExportNonPrintableObjects: TVpeSetExportNonPrintableObjects;
  VpeGetExportNonPrintableObjects: TVpeGetExportNonPrintableObjects;
  VpeSetBar2DAlignment: TVpeSetBar2DAlignment;
  VpeGetBar2DAlignment: TVpeGetBar2DAlignment;
  VpeSetDataMatrixEncodingFormat: TVpeSetDataMatrixEncodingFormat;
  VpeGetDataMatrixEncodingFormat: TVpeGetDataMatrixEncodingFormat;
  VpeSetDataMatrixEccType: TVpeSetDataMatrixEccType;
  VpeGetDataMatrixEccType: TVpeGetDataMatrixEccType;
  VpeSetDataMatrixRows: TVpeSetDataMatrixRows;
  VpeGetDataMatrixRows: TVpeGetDataMatrixRows;
  VpeSetDataMatrixColumns: TVpeSetDataMatrixColumns;
  VpeGetDataMatrixColumns: TVpeGetDataMatrixColumns;
  VpeSetDataMatrixMirror: TVpeSetDataMatrixMirror;
  VpeGetDataMatrixMirror: TVpeGetDataMatrixMirror;
  VpeSetDataMatrixBorder: TVpeSetDataMatrixBorder;
  VpeGetDataMatrixBorder: TVpeGetDataMatrixBorder;
  VpeDataMatrix: TVpeDataMatrix;
  VpeRenderDataMatrix: TVpeRenderDataMatrix;
  VpeMaxiCode: TVpeMaxiCode;
  VpeRenderMaxiCode: TVpeRenderMaxiCode;
  VpeMaxiCodeEx: TVpeMaxiCodeEx;
  VpeRenderMaxiCodeEx: TVpeRenderMaxiCodeEx;
  VpeSetPDF417ErrorLevel: TVpeSetPDF417ErrorLevel;
  VpeGetPDF417ErrorLevel: TVpeGetPDF417ErrorLevel;
  VpeSetPDF417Rows: TVpeSetPDF417Rows;
  VpeGetPDF417Rows: TVpeGetPDF417Rows;
  VpeSetPDF417Columns: TVpeSetPDF417Columns;
  VpeGetPDF417Columns: TVpeGetPDF417Columns;
  VpePDF417: TVpePDF417;
  VpeRenderPDF417: TVpeRenderPDF417;
  VpeSetAztecFlags: TVpeSetAztecFlags;
  VpeGetAztecFlags: TVpeGetAztecFlags;
  VpeSetAztecControl: TVpeSetAztecControl;
  VpeGetAztecControl: TVpeGetAztecControl;
  VpeSetAztecMenu: TVpeSetAztecMenu;
  VpeGetAztecMenu: TVpeGetAztecMenu;
  VpeSetAztecMultipleSymbols: TVpeSetAztecMultipleSymbols;
  VpeGetAztecMultipleSymbols: TVpeGetAztecMultipleSymbols;
  VpeSetAztecID: TVpeSetAztecID;
  VpeAztec: TVpeAztec;
  VpeRenderAztec: TVpeRenderAztec;
  VpeSetPictureScale2Gray: TVpeSetPictureScale2Gray;
  VpeGetPictureScale2Gray: TVpeGetPictureScale2Gray;
  VpeSetPictureScale2GrayFloat: TVpeSetPictureScale2GrayFloat;
  VpeGetPictureScale2GrayFloat: TVpeGetPictureScale2GrayFloat;
  VpeCreateMemoryStream: TVpeCreateMemoryStream;
  VpeStreamRead: TVpeStreamRead;
  VpeStreamWrite: TVpeStreamWrite;
  VpeGetStreamSize: TVpeGetStreamSize;
  VpeStreamIsEof: TVpeStreamIsEof;
  VpeGetStreamState: TVpeGetStreamState;
  VpeGetStreamPosition: TVpeGetStreamPosition;
  VpeStreamSeek: TVpeStreamSeek;
  VpeStreamSeekEnd: TVpeStreamSeekEnd;
  VpeStreamSeekRel: TVpeStreamSeekRel;
  VpeCloseStream: TVpeCloseStream;
  VpeWriteRTFStream: TVpeWriteRTFStream;
  VpeWriteBoxRTFStream: TVpeWriteBoxRTFStream;
  VpeRenderRTFStream: TVpeRenderRTFStream;
  VpeRenderBoxRTFStream: TVpeRenderBoxRTFStream;
  VpePictureStream: TVpePictureStream;
  VpeRenderPictureStream: TVpeRenderPictureStream;
  VpeGetPicturePageCountStream: TVpeGetPicturePageCountStream;
  VpeWriteDocStream: TVpeWriteDocStream;
  VpeWriteDocStreamPageRange: TVpeWriteDocStreamPageRange;
  VpeWriteDocPageRange: TVpeWriteDocPageRange;
  VpeReadDocStream: TVpeReadDocStream;
  VpeReadDocStreamPageRange: TVpeReadDocStreamPageRange;
  VpeReadDocPageRange: TVpeReadDocPageRange;
  VpeSetHtmlScale: TVpeSetHtmlScale;
  VpeGetHtmlScale: TVpeGetHtmlScale;
  VpeSetHtmlWordSpacing: TVpeSetHtmlWordSpacing;
  VpeGetHtmlWordSpacing: TVpeGetHtmlWordSpacing;
  VpeSetHtmlRtfLineSpacing: TVpeSetHtmlRtfLineSpacing;
  VpeGetHtmlRtfLineSpacing: TVpeGetHtmlRtfLineSpacing;
  VpeSetHtmlCopyImages: TVpeSetHtmlCopyImages;
  VpeGetHtmlCopyImages: TVpeGetHtmlCopyImages;
  VpeSetHtmlPageBorders: TVpeSetHtmlPageBorders;
  VpeGetHtmlPageBorders: TVpeGetHtmlPageBorders;
  VpeSetHtmlPageSeparators: TVpeSetHtmlPageSeparators;
  VpeGetHtmlPageSeparators: TVpeGetHtmlPageSeparators;
  VpeSetXmlPictureExport: TVpeSetXmlPictureExport;
  VpeGetXmlPictureExport: TVpeGetXmlPictureExport;
  VpeSetOdtTextWidthScale: TVpeSetOdtTextWidthScale;
  VpeGetOdtTextWidthScale: TVpeGetOdtTextWidthScale;
  VpeSetOdtTextPtSizeScale: TVpeSetOdtTextPtSizeScale;
  VpeGetOdtTextPtSizeScale: TVpeGetOdtTextPtSizeScale;
  VpeSetOdtLineHeight: TVpeSetOdtLineHeight;
  VpeGetOdtLineHeight: TVpeGetOdtLineHeight;
  VpeSetOdtAutoTextboxHeight: TVpeSetOdtAutoTextboxHeight;
  VpeGetOdtAutoTextboxHeight: TVpeGetOdtAutoTextboxHeight;
  VpeSetOdtPositionProtect: TVpeSetOdtPositionProtect;
  VpeGetOdtPositionProtect: TVpeGetOdtPositionProtect;
  VpeClearPage: TVpeClearPage;
  VpeRemovePage: TVpeRemovePage;
  VpeInsertPage: TVpeInsertPage;
  VpeSetInsertAtBottomZOrder: TVpeSetInsertAtBottomZOrder;
  VpeGetInsertAtBottomZOrder: TVpeGetInsertAtBottomZOrder;
  VpeWriteRTFU: TVpeWriteRTFU;
  VpeWriteBoxRTFU: TVpeWriteBoxRTFU;
  VpeWriteRTFFileU: TVpeWriteRTFFileU;
  VpeWriteBoxRTFFileU: TVpeWriteBoxRTFFileU;
  VpeSetRTFFontU: TVpeSetRTFFontU;
  VpeRenderRTFU: TVpeRenderRTFU;
  VpeRenderBoxRTFU: TVpeRenderBoxRTFU;
  VpeRenderRTFFileU: TVpeRenderRTFFileU;
  VpeRenderBoxRTFFileU: TVpeRenderBoxRTFFileU;
  VpeChartDataAddLegendU: TVpeChartDataAddLegendU;
  VpeChartDataAddXLabelU: TVpeChartDataAddXLabelU;
  VpeChartDataAddYLabelU: TVpeChartDataAddYLabelU;
  VpeChartDataSetXAxisTitleU: TVpeChartDataSetXAxisTitleU;
  VpeChartDataSetYAxisTitleU: TVpeChartDataSetYAxisTitleU;
  VpeSetChartTitleU: TVpeSetChartTitleU;
  VpeSetChartSubTitleU: TVpeSetChartSubTitleU;
  VpeSetChartFootNoteU: TVpeSetChartFootNoteU;
  VpeDataMatrixU: TVpeDataMatrixU;
  VpeRenderDataMatrixU: TVpeRenderDataMatrixU;
  VpeMaxiCodeU: TVpeMaxiCodeU;
  VpeRenderMaxiCodeU: TVpeRenderMaxiCodeU;
  VpeMaxiCodeExU: TVpeMaxiCodeExU;
  VpeRenderMaxiCodeExU: TVpeRenderMaxiCodeExU;
  VpePDF417U: TVpePDF417U;
  VpeRenderPDF417U: TVpeRenderPDF417U;
  VpeSetAztecIDU: TVpeSetAztecIDU;
  VpeAztecU: TVpeAztecU;
  VpeRenderAztecU: TVpeRenderAztecU;
  VpePictureStreamU: TVpePictureStreamU;
  VpeRenderPictureStreamU: TVpeRenderPictureStreamU;
  VpeWriteDocPageRangeU: TVpeWriteDocPageRangeU;
  VpeReadDocPageRangeU: TVpeReadDocPageRangeU;
  VpeSetJpegExportOptions: TVpeSetJpegExportOptions;
  VpeGetJpegExportOptions: TVpeGetJpegExportOptions;
  VpeSetTiffExportOptions: TVpeSetTiffExportOptions;
  VpeGetTiffExportOptions: TVpeGetTiffExportOptions;
  VpeSetGifExportOptions: TVpeSetGifExportOptions;
  VpeGetGifExportOptions: TVpeGetGifExportOptions;
  VpeSetBmpExportOptions: TVpeSetBmpExportOptions;
  VpeGetBmpExportOptions: TVpeGetBmpExportOptions;
  VpeSetPnmExportOptions: TVpeSetPnmExportOptions;
  VpeGetPnmExportOptions: TVpeGetPnmExportOptions;
  VpeSetPictureExportColorDepth: TVpeSetPictureExportColorDepth;
  VpeSetPictureExportDither: TVpeSetPictureExportDither;
  VpePictureExportPage: TVpePictureExportPage;
  VpePictureExport: TVpePictureExport;
  VpePictureExportPageStream: TVpePictureExportPageStream;
  VpePictureExportStream: TVpePictureExportStream;
  VpeEnableClickEvents: TVpeEnableClickEvents;
  VpeSetObjectID: TVpeSetObjectID;
  VpeGetObjectID: TVpeGetObjectID;
  VpeCreateUDO: TVpeCreateUDO;
  VpeGetUDOlParam: TVpeGetUDOlParam;
  VpeGetUDODC: TVpeGetUDODC;
  VpeGetUDODrawRect: TVpeGetUDODrawRect;
  VpeGetUDOIsPrinting: TVpeGetUDOIsPrinting;
  VpeGetUDOIsExporting: TVpeGetUDOIsExporting;
  VpeGetUDODpiX: TVpeGetUDODpiX;
  VpeGetUDODpiY: TVpeGetUDODpiY;
  VpeSetPrintScale: TVpeSetPrintScale;
  VpeGetPrintScale: TVpeGetPrintScale;
  VpePictureExportPageU: TVpePictureExportPageU;
  VpePictureExportU: TVpePictureExportU;
  VpeLoadTemplate: TVpeLoadTemplate;
  VpeLoadTemplateAuthKey: TVpeLoadTemplateAuthKey;
  VpeUseTemplateSettings: TVpeUseTemplateSettings;
  VpeUseTemplateMargins: TVpeUseTemplateMargins;
  VpeSetDateTimeIsUTC: TVpeSetDateTimeIsUTC;
  VpeGetDateTimeIsUTC: TVpeGetDateTimeIsUTC;
  VpeDumpTemplate: TVpeDumpTemplate;
  VpeDumpTemplatePage: TVpeDumpTemplatePage;
  VpeGetLastInsertedObject: TVpeGetLastInsertedObject;
  VpeFormField: TVpeFormField;
  VpeRenderFormField: TVpeRenderFormField;
  VpeSetCharCount: TVpeSetCharCount;
  VpeGetCharCount: TVpeGetCharCount;
  VpeSetDividerPenSize: TVpeSetDividerPenSize;
  VpeGetDividerPenSize: TVpeGetDividerPenSize;
  VpeSetDividerPenColor: TVpeSetDividerPenColor;
  VpeGetDividerPenColor: TVpeGetDividerPenColor;
  VpeSetAltDividerNPosition: TVpeSetAltDividerNPosition;
  VpeGetAltDividerNPosition: TVpeGetAltDividerNPosition;
  VpeSetAltDividerPenSize: TVpeSetAltDividerPenSize;
  VpeGetAltDividerPenSize: TVpeGetAltDividerPenSize;
  VpeSetAltDividerPenColor: TVpeSetAltDividerPenColor;
  VpeGetAltDividerPenColor: TVpeGetAltDividerPenColor;
  VpeSetBottomLinePenSize: TVpeSetBottomLinePenSize;
  VpeGetBottomLinePenSize: TVpeGetBottomLinePenSize;
  VpeSetBottomLinePenColor: TVpeSetBottomLinePenColor;
  VpeGetBottomLinePenColor: TVpeGetBottomLinePenColor;
  VpeSetDividerStyle: TVpeSetDividerStyle;
  VpeGetDividerStyle: TVpeGetDividerStyle;
  VpeSetAltDividerStyle: TVpeSetAltDividerStyle;
  VpeGetAltDividerStyle: TVpeGetAltDividerStyle;
  VpeSetFormFieldFlags: TVpeSetFormFieldFlags;
  VpeGetFormFieldFlags: TVpeGetFormFieldFlags;
  VpeSetUDOlParam: TVpeSetUDOlParam;
  VpeDeleteObject: TVpeDeleteObject;
  VpeGetFirstObject: TVpeGetFirstObject;
  VpeGetClickedObject: TVpeGetClickedObject;
  VpeSetTplMaster: TVpeSetTplMaster;
  VpeGetTplFieldAsString: TVpeGetTplFieldAsString;
  VpeSetTplFieldAsString: TVpeSetTplFieldAsString;
  VpeGetTplFieldAsInteger: TVpeGetTplFieldAsInteger;
  VpeSetTplFieldAsInteger: TVpeSetTplFieldAsInteger;
  VpeGetTplFieldAsNumber: TVpeGetTplFieldAsNumber;
  VpeSetTplFieldAsNumber: TVpeSetTplFieldAsNumber;
  VpeGetTplFieldAsDateTime: TVpeGetTplFieldAsDateTime;
  VpeSetTplFieldAsDateTime: TVpeSetTplFieldAsDateTime;
  VpeGetTplFieldAsOleDateTime: TVpeGetTplFieldAsOleDateTime;
  VpeSetTplFieldAsOleDateTime: TVpeSetTplFieldAsOleDateTime;
  VpeGetTplFieldAsJavaDateTime: TVpeGetTplFieldAsJavaDateTime;
  VpeSetTplFieldAsJavaDateTime: TVpeSetTplFieldAsJavaDateTime;
  VpeGetTplFieldAsBoolean: TVpeGetTplFieldAsBoolean;
  VpeSetTplFieldAsBoolean: TVpeSetTplFieldAsBoolean;
  VpeGetTplPageCount: TVpeGetTplPageCount;
  VpeGetTplPageWidth: TVpeGetTplPageWidth;
  VpeSetTplPageWidth: TVpeSetTplPageWidth;
  VpeGetTplPageHeight: TVpeGetTplPageHeight;
  VpeSetTplPageHeight: TVpeSetTplPageHeight;
  VpeGetTplPageOrientation: TVpeGetTplPageOrientation;
  VpeSetTplPageOrientation: TVpeSetTplPageOrientation;
  VpeGetTplPaperBin: TVpeGetTplPaperBin;
  VpeSetTplPaperBin: TVpeSetTplPaperBin;
  VpeGetTplLeftMargin: TVpeGetTplLeftMargin;
  VpeSetTplLeftMargin: TVpeSetTplLeftMargin;
  VpeGetTplRightMargin: TVpeGetTplRightMargin;
  VpeSetTplRightMargin: TVpeSetTplRightMargin;
  VpeGetTplTopMargin: TVpeGetTplTopMargin;
  VpeSetTplTopMargin: TVpeSetTplTopMargin;
  VpeGetTplBottomMargin: TVpeGetTplBottomMargin;
  VpeSetTplBottomMargin: TVpeSetTplBottomMargin;
  VpeGetTplDataSourceCount: TVpeGetTplDataSourceCount;
  VpeGetTplDataSourceObject: TVpeGetTplDataSourceObject;
  VpeGetTplDataSourceFileName: TVpeGetTplDataSourceFileName;
  VpeGetTplDataSourcePrefix: TVpeGetTplDataSourcePrefix;
  VpeGetTplDataSourceDescription: TVpeGetTplDataSourceDescription;
  VpeGetTplFieldCount: TVpeGetTplFieldCount;
  VpeGetTplFieldObject: TVpeGetTplFieldObject;
  VpeGetTplFieldName: TVpeGetTplFieldName;
  VpeGetTplFieldDescription: TVpeGetTplFieldDescription;
  VpeFindTplFieldObject: TVpeFindTplFieldObject;
  VpeClearTplFields: TVpeClearTplFields;
  VpeGetTplVpeObjectCount: TVpeGetTplVpeObjectCount;
  VpeGetTplVpeObject: TVpeGetTplVpeObject;
  VpeFindTplVpeObject: TVpeFindTplVpeObject;
  VpeGetFieldAsString: TVpeGetFieldAsString;
  VpeSetFieldAsString: TVpeSetFieldAsString;
  VpeGetFieldAsInteger: TVpeGetFieldAsInteger;
  VpeSetFieldAsInteger: TVpeSetFieldAsInteger;
  VpeGetFieldAsNumber: TVpeGetFieldAsNumber;
  VpeSetFieldAsNumber: TVpeSetFieldAsNumber;
  VpeGetFieldAsDateTime: TVpeGetFieldAsDateTime;
  VpeSetFieldAsDateTime: TVpeSetFieldAsDateTime;
  VpeGetFieldAsOleDateTime: TVpeGetFieldAsOleDateTime;
  VpeSetFieldAsOleDateTime: TVpeSetFieldAsOleDateTime;
  VpeGetFieldAsJavaDateTime: TVpeGetFieldAsJavaDateTime;
  VpeSetFieldAsJavaDateTime: TVpeSetFieldAsJavaDateTime;
  VpeGetFieldAsBoolean: TVpeGetFieldAsBoolean;
  VpeSetFieldAsBoolean: TVpeSetFieldAsBoolean;
  VpeGetFieldName: TVpeGetFieldName;
  VpeGetFieldDescription: TVpeGetFieldDescription;
  VpeGetFieldDataSourceObject: TVpeGetFieldDataSourceObject;
  VpeGetDataSourceFileName: TVpeGetDataSourceFileName;
  VpeGetDataSourcePrefix: TVpeGetDataSourcePrefix;
  VpeGetDataSourceDescription: TVpeGetDataSourceDescription;
  VpeGetDataSourceFieldCount: TVpeGetDataSourceFieldCount;
  VpeGetDataSourceFieldObject: TVpeGetDataSourceFieldObject;
  VpeGetInsertedVpeObjectCount: TVpeGetInsertedVpeObjectCount;
  VpeGetInsertedVpeObject: TVpeGetInsertedVpeObject;
  VpeGetInsertedVpeObjectPageNo: TVpeGetInsertedVpeObjectPageNo;
  VpeGetObjName: TVpeGetObjName;
  VpeGetObjTemplateObject: TVpeGetObjTemplateObject;
  VpeGetObjKind: TVpeGetObjKind;
  VpeGetObjText: TVpeGetObjText;
  VpeGetObjResolvedText: TVpeGetObjResolvedText;
  VpeGetObjLeft: TVpeGetObjLeft;
  VpeGetObjTop: TVpeGetObjTop;
  VpeGetObjRight: TVpeGetObjRight;
  VpeGetObjBottom: TVpeGetObjBottom;
  VpeGetObjPictureFileName: TVpeGetObjPictureFileName;
  VpeSetObjPictureFileName: TVpeSetObjPictureFileName;
  VpeGetNextObject: TVpeGetNextObject;
  VpeGetTplPageObject: TVpeGetTplPageObject;
  VpeGetTplPageObjWidth: TVpeGetTplPageObjWidth;
  VpeSetTplPageObjWidth: TVpeSetTplPageObjWidth;
  VpeGetTplPageObjHeight: TVpeGetTplPageObjHeight;
  VpeSetTplPageObjHeight: TVpeSetTplPageObjHeight;
  VpeGetTplPageObjOrientation: TVpeGetTplPageObjOrientation;
  VpeSetTplPageObjOrientation: TVpeSetTplPageObjOrientation;
  VpeGetTplPageObjPaperBin: TVpeGetTplPageObjPaperBin;
  VpeSetTplPageObjPaperBin: TVpeSetTplPageObjPaperBin;
  VpeGetTplPageObjLeftMargin: TVpeGetTplPageObjLeftMargin;
  VpeSetTplPageObjLeftMargin: TVpeSetTplPageObjLeftMargin;
  VpeGetTplPageObjRightMargin: TVpeGetTplPageObjRightMargin;
  VpeSetTplPageObjRightMargin: TVpeSetTplPageObjRightMargin;
  VpeGetTplPageObjTopMargin: TVpeGetTplPageObjTopMargin;
  VpeSetTplPageObjTopMargin: TVpeSetTplPageObjTopMargin;
  VpeGetTplPageObjBottomMargin: TVpeGetTplPageObjBottomMargin;
  VpeSetTplPageObjBottomMargin: TVpeSetTplPageObjBottomMargin;
  VpeGetTplPageObjVpeObjectCount: TVpeGetTplPageObjVpeObjectCount;
  VpeGetTplPageObjVpeObject: TVpeGetTplPageObjVpeObject;
  VpeLoadTemplateU: TVpeLoadTemplateU;
  VpeLoadTemplateAuthKeyU: TVpeLoadTemplateAuthKeyU;
  VpeFormFieldU: TVpeFormFieldU;
  VpeRenderFormFieldU: TVpeRenderFormFieldU;
  VpeGetTplFieldAsStringU: TVpeGetTplFieldAsStringU;
  VpeSetTplFieldAsStringU: TVpeSetTplFieldAsStringU;
  VpeGetTplFieldAsIntegerU: TVpeGetTplFieldAsIntegerU;
  VpeSetTplFieldAsIntegerU: TVpeSetTplFieldAsIntegerU;
  VpeGetTplFieldAsNumberU: TVpeGetTplFieldAsNumberU;
  VpeSetTplFieldAsNumberU: TVpeSetTplFieldAsNumberU;
  VpeGetTplFieldAsDateTimeU: TVpeGetTplFieldAsDateTimeU;
  VpeSetTplFieldAsDateTimeU: TVpeSetTplFieldAsDateTimeU;
  VpeGetTplFieldAsOleDateTimeU: TVpeGetTplFieldAsOleDateTimeU;
  VpeSetTplFieldAsOleDateTimeU: TVpeSetTplFieldAsOleDateTimeU;
  VpeGetTplFieldAsJavaDateTimeU: TVpeGetTplFieldAsJavaDateTimeU;
  VpeSetTplFieldAsJavaDateTimeU: TVpeSetTplFieldAsJavaDateTimeU;
  VpeGetTplFieldAsBooleanU: TVpeGetTplFieldAsBooleanU;
  VpeSetTplFieldAsBooleanU: TVpeSetTplFieldAsBooleanU;
  VpeGetTplDataSourceFileNameU: TVpeGetTplDataSourceFileNameU;
  VpeGetTplDataSourcePrefixU: TVpeGetTplDataSourcePrefixU;
  VpeGetTplDataSourceDescriptionU: TVpeGetTplDataSourceDescriptionU;
  VpeGetTplFieldNameU: TVpeGetTplFieldNameU;
  VpeGetTplFieldDescriptionU: TVpeGetTplFieldDescriptionU;
  VpeFindTplFieldObjectU: TVpeFindTplFieldObjectU;
  VpeFindTplVpeObjectU: TVpeFindTplVpeObjectU;
  VpeGetFieldAsStringU: TVpeGetFieldAsStringU;
  VpeSetFieldAsStringU: TVpeSetFieldAsStringU;
  VpeGetFieldNameU: TVpeGetFieldNameU;
  VpeGetFieldDescriptionU: TVpeGetFieldDescriptionU;
  VpeGetDataSourceFileNameU: TVpeGetDataSourceFileNameU;
  VpeGetDataSourcePrefixU: TVpeGetDataSourcePrefixU;
  VpeGetDataSourceDescriptionU: TVpeGetDataSourceDescriptionU;
  VpeGetObjNameU: TVpeGetObjNameU;
  VpeGetObjTextU: TVpeGetObjTextU;
  VpeGetObjResolvedTextU: TVpeGetObjResolvedTextU;
  VpeGetObjPictureFileNameU: TVpeGetObjPictureFileNameU;
  VpeSetObjPictureFileNameU: TVpeSetObjPictureFileNameU;
{ VpeXyz: TVpeXyz; }



   { ============== }
   { InitializeVPE: }
   { ============== }
implementation

procedure InitializeVPE;
var
  OldErrorMode: Word;

begin
  DLLInitialized := false;

  OldErrorMode := SetErrorMode(SEM_FAILCRITICALERRORS + SEM_NOOPENFILEERRORBOX);
  DLLHandle := LoadLibrary(VPEDLL);
  SetErrorMode(OldErrorMode);

  if DLLHandle <= HINSTANCE_ERROR then
  begin
    Windows.MessageBox(0, 'Could not load VPE-DLL!', 'VPE', MB_OK + MB_ICONSTOP);
  end
  else
  begin
  @VpeLicense := GetProcAddress(DLLHandle, PChar(50));
  @VpeGetVersion := GetProcAddress(DLLHandle, PChar(51));
  @VpeGetReleaseNumber := GetProcAddress(DLLHandle, PChar(52));
  @VpeGetBuildNumber := GetProcAddress(DLLHandle, PChar(53));
  @VpeGetEdition := GetProcAddress(DLLHandle, PChar(54));
  @VpeOpenDoc := GetProcAddress(DLLHandle, PChar(55));
  @VpeOpenDocFile := GetProcAddress(DLLHandle, PChar(56));
  @VpeCloseDoc := GetProcAddress(DLLHandle, PChar(57));
  @VpeSetPageFormat := GetProcAddress(DLLHandle, PChar(58));
  @VpeGetPageFormat := GetProcAddress(DLLHandle, PChar(59));
  @VpeSetPageWidth := GetProcAddress(DLLHandle, PChar(60));
  @VpeSetPageHeight := GetProcAddress(DLLHandle, PChar(61));
  @VpeGetPageWidth := GetProcAddress(DLLHandle, PChar(62));
  @VpeGetPageHeight := GetProcAddress(DLLHandle, PChar(63));
  @VpeSetPageOrientation := GetProcAddress(DLLHandle, PChar(64));
  @VpeGetPageOrientation := GetProcAddress(DLLHandle, PChar(65));
  @VpeSetPaperBin := GetProcAddress(DLLHandle, PChar(66));
  @VpeGetPaperBin := GetProcAddress(DLLHandle, PChar(67));
  @VpeSetDefOutRect := GetProcAddress(DLLHandle, PChar(68));
  @VpeSetOutRect := GetProcAddress(DLLHandle, PChar(69));
  @VpeGet := GetProcAddress(DLLHandle, PChar(70));
  @VpeSet := GetProcAddress(DLLHandle, PChar(71));
  @VpeStorePos := GetProcAddress(DLLHandle, PChar(72));
  @VpeRestorePos := GetProcAddress(DLLHandle, PChar(73));
  @VpeSetPen := GetProcAddress(DLLHandle, PChar(74));
  @VpeSetPenSize := GetProcAddress(DLLHandle, PChar(75));
  @VpeGetPenSize := GetProcAddress(DLLHandle, PChar(76));
  @VpePenSize := GetProcAddress(DLLHandle, PChar(77));
  @VpeSetPenStyle := GetProcAddress(DLLHandle, PChar(78));
  @VpePenStyle := GetProcAddress(DLLHandle, PChar(79));
  @VpeSetPenColor := GetProcAddress(DLLHandle, PChar(80));
  @VpePenColor := GetProcAddress(DLLHandle, PChar(81));
  @VpeNoPen := GetProcAddress(DLLHandle, PChar(82));
  @VpeLine := GetProcAddress(DLLHandle, PChar(83));
  @VpePolyLine := GetProcAddress(DLLHandle, PChar(84));
  @VpeAddPolyPoint := GetProcAddress(DLLHandle, PChar(85));
  @VpePolygon := GetProcAddress(DLLHandle, PChar(86));
  @VpeAddPolygonPoint := GetProcAddress(DLLHandle, PChar(87));
  @VpeSetBkgColor := GetProcAddress(DLLHandle, PChar(88));
  @VpeSetBkgGradientStartColor := GetProcAddress(DLLHandle, PChar(89));
  @VpeSetBkgGradientEndColor := GetProcAddress(DLLHandle, PChar(90));
  @VpeSetBkgGradientRotation := GetProcAddress(DLLHandle, PChar(91));
  @VpeSetBkgGradientPrint := GetProcAddress(DLLHandle, PChar(92));
  @VpeSetBkgGradientPrintSolidColor := GetProcAddress(DLLHandle, PChar(93));
  @VpeSetTransparentMode := GetProcAddress(DLLHandle, PChar(94));
  @VpeSetBkgMode := GetProcAddress(DLLHandle, PChar(95));
  @VpeSetHatchStyle := GetProcAddress(DLLHandle, PChar(96));
  @VpeSetHatchColor := GetProcAddress(DLLHandle, PChar(97));
  @VpeBox := GetProcAddress(DLLHandle, PChar(98));
  @VpeEllipse := GetProcAddress(DLLHandle, PChar(99));
  @VpePie := GetProcAddress(DLLHandle, PChar(100));
  @VpeSetBarcodeParms := GetProcAddress(DLLHandle, PChar(101));
  @VpeSetBarcodeAlignment := GetProcAddress(DLLHandle, PChar(102));
  @VpeBarcode := GetProcAddress(DLLHandle, PChar(103));
  @VpeGetPictureTypes := GetProcAddress(DLLHandle, PChar(104));
  @VpePicture := GetProcAddress(DLLHandle, PChar(105));
  @VpeSetFont := GetProcAddress(DLLHandle, PChar(106));
  @VpeSelectFont := GetProcAddress(DLLHandle, PChar(107));
  @VpeSetFontName := GetProcAddress(DLLHandle, PChar(108));
  @VpeSetFontSize := GetProcAddress(DLLHandle, PChar(109));
  @VpeSetCharset := GetProcAddress(DLLHandle, PChar(110));
  @VpeSetFontAttr := GetProcAddress(DLLHandle, PChar(111));
  @VpeSetTextAlignment := GetProcAddress(DLLHandle, PChar(112));
  @VpeSetAlign := GetProcAddress(DLLHandle, PChar(113));
  @VpeSetBold := GetProcAddress(DLLHandle, PChar(114));
  @VpeSetUnderlined := GetProcAddress(DLLHandle, PChar(115));
  @VpeSetStrikeOut := GetProcAddress(DLLHandle, PChar(116));
  @VpeSetItalic := GetProcAddress(DLLHandle, PChar(117));
  @VpeSetTextColor := GetProcAddress(DLLHandle, PChar(118));
  @VpePrint := GetProcAddress(DLLHandle, PChar(119));
  @VpePrintBox := GetProcAddress(DLLHandle, PChar(120));
  @VpeWrite := GetProcAddress(DLLHandle, PChar(121));
  @VpeWriteBox := GetProcAddress(DLLHandle, PChar(122));
  @VpeDefineHeader := GetProcAddress(DLLHandle, PChar(123));
  @VpeDefineFooter := GetProcAddress(DLLHandle, PChar(124));
  @VpePageBreak := GetProcAddress(DLLHandle, PChar(125));
  @VpeGetPageCount := GetProcAddress(DLLHandle, PChar(126));
  @VpeGetCurrentPage := GetProcAddress(DLLHandle, PChar(127));
  @VpeGotoPage := GetProcAddress(DLLHandle, PChar(128));
  @VpeStoreSet := GetProcAddress(DLLHandle, PChar(129));
  @VpeUseSet := GetProcAddress(DLLHandle, PChar(130));
  @VpeRemoveSet := GetProcAddress(DLLHandle, PChar(131));
  @VpeSetAutoBreak := GetProcAddress(DLLHandle, PChar(132));
  @VpeWriteDoc := GetProcAddress(DLLHandle, PChar(133));
  @VpeReadDoc := GetProcAddress(DLLHandle, PChar(134));
  @VpeSetRotation := GetProcAddress(DLLHandle, PChar(135));
  @VpeSetPictureCacheSize := GetProcAddress(DLLHandle, PChar(136));
  @VpeSetPicCacheSize := GetProcAddress(DLLHandle, PChar(137));
  @VpeGetPictureCacheSize := GetProcAddress(DLLHandle, PChar(138));
  @VpeGetPicCacheSize := GetProcAddress(DLLHandle, PChar(139));
  @VpeGetPictureCacheUsed := GetProcAddress(DLLHandle, PChar(140));
  @VpeGetPicCacheUsed := GetProcAddress(DLLHandle, PChar(141));
  @VpeSetPrintOffset := GetProcAddress(DLLHandle, PChar(142));
  @VpeSetPrintOffsetX := GetProcAddress(DLLHandle, PChar(143));
  @VpeSetPrintOffsetY := GetProcAddress(DLLHandle, PChar(144));
  @VpeGetPrintOffsetX := GetProcAddress(DLLHandle, PChar(145));
  @VpeGetPrintOffsetY := GetProcAddress(DLLHandle, PChar(146));
  @VpeGetLastError := GetProcAddress(DLLHandle, PChar(147));
  @VpeRenderPrint := GetProcAddress(DLLHandle, PChar(148));
  @VpeRenderPrintBox := GetProcAddress(DLLHandle, PChar(149));
  @VpeRenderWrite := GetProcAddress(DLLHandle, PChar(150));
  @VpeRenderWriteBox := GetProcAddress(DLLHandle, PChar(151));
  @VpeRenderPicture := GetProcAddress(DLLHandle, PChar(152));
  @VpeGetPicturePageCount := GetProcAddress(DLLHandle, PChar(153));
  @VpeSetPicturePage := GetProcAddress(DLLHandle, PChar(154));
  @VpeGetPicturePage := GetProcAddress(DLLHandle, PChar(155));
  @VpeSetPictureType := GetProcAddress(DLLHandle, PChar(156));
  @VpeSetDocFileReadOnly := GetProcAddress(DLLHandle, PChar(157));
  @VpeGetPenStyle := GetProcAddress(DLLHandle, PChar(158));
  @VpeGetPenColor := GetProcAddress(DLLHandle, PChar(159));
  @VpeGetBkgMode := GetProcAddress(DLLHandle, PChar(160));
  @VpeGetBkgColor := GetProcAddress(DLLHandle, PChar(161));
  @VpeGetHatchStyle := GetProcAddress(DLLHandle, PChar(162));
  @VpeGetHatchColor := GetProcAddress(DLLHandle, PChar(163));
  @VpeGetBkgGradientStartColor := GetProcAddress(DLLHandle, PChar(164));
  @VpeGetBkgGradientEndColor := GetProcAddress(DLLHandle, PChar(165));
  @VpeGetBkgGradientRotation := GetProcAddress(DLLHandle, PChar(166));
  @VpeGetFontName := GetProcAddress(DLLHandle, PChar(167));
  @VpeGetFontSize := GetProcAddress(DLLHandle, PChar(168));
  @VpeGetCharset := GetProcAddress(DLLHandle, PChar(169));
  @VpeGetTextAlignment := GetProcAddress(DLLHandle, PChar(170));
  @VpeGetBold := GetProcAddress(DLLHandle, PChar(171));
  @VpeGetUnderlined := GetProcAddress(DLLHandle, PChar(172));
  @VpeGetStrikeOut := GetProcAddress(DLLHandle, PChar(173));
  @VpeGetItalic := GetProcAddress(DLLHandle, PChar(174));
  @VpeGetTextColor := GetProcAddress(DLLHandle, PChar(175));
  @VpeGetRotation := GetProcAddress(DLLHandle, PChar(176));
  @VpeSetBarcodeMainTextParms := GetProcAddress(DLLHandle, PChar(177));
  @VpeGetBarcodeMainTextParms := GetProcAddress(DLLHandle, PChar(178));
  @VpeSetBarcodeAddTextParms := GetProcAddress(DLLHandle, PChar(179));
  @VpeGetBarcodeAddTextParms := GetProcAddress(DLLHandle, PChar(180));
  @VpeGetBarcodeAlignment := GetProcAddress(DLLHandle, PChar(181));
  @VpeSetBarcodeAutoChecksum := GetProcAddress(DLLHandle, PChar(182));
  @VpeGetBarcodeAutoChecksum := GetProcAddress(DLLHandle, PChar(183));
  @VpeSetBarcodeThinBar := GetProcAddress(DLLHandle, PChar(184));
  @VpeGetBarcodeThinBar := GetProcAddress(DLLHandle, PChar(185));
  @VpeSetBarcodeThickBar := GetProcAddress(DLLHandle, PChar(186));
  @VpeGetBarcodeThickBar := GetProcAddress(DLLHandle, PChar(187));
  @VpeGetPictureType := GetProcAddress(DLLHandle, PChar(188));
  @VpeGetTransparentMode := GetProcAddress(DLLHandle, PChar(189));
  @VpeSetUnderline := GetProcAddress(DLLHandle, PChar(190));
  @VpeGetUnderline := GetProcAddress(DLLHandle, PChar(191));
  @VpeGetAutoBreak := GetProcAddress(DLLHandle, PChar(192));
  @VpeGetDocExportPictureQuality := GetProcAddress(DLLHandle, PChar(193));
  @VpeSetDocExportPictureQuality := GetProcAddress(DLLHandle, PChar(194));
  @VpeSetDocExportType := GetProcAddress(DLLHandle, PChar(195));
  @VpeGetDocExportType := GetProcAddress(DLLHandle, PChar(196));
  @VpeGetDocExportPictureResolution := GetProcAddress(DLLHandle, PChar(197));
  @VpeSetDocExportPictureResolution := GetProcAddress(DLLHandle, PChar(198));
  @VpeGetFastWebView := GetProcAddress(DLLHandle, PChar(199));
  @VpeSetFastWebView := GetProcAddress(DLLHandle, PChar(200));
  @VpeGetUseTempFiles := GetProcAddress(DLLHandle, PChar(201));
  @VpeSetUseTempFiles := GetProcAddress(DLLHandle, PChar(202));
  @VpeGetCompression := GetProcAddress(DLLHandle, PChar(203));
  @VpeSetCompression := GetProcAddress(DLLHandle, PChar(204));
  @VpeGetEmbedAllFonts := GetProcAddress(DLLHandle, PChar(205));
  @VpeSetEmbedAllFonts := GetProcAddress(DLLHandle, PChar(206));
  @VpeGetSubsetAllFonts := GetProcAddress(DLLHandle, PChar(207));
  @VpeSetSubsetAllFonts := GetProcAddress(DLLHandle, PChar(208));
  @VpeSetFontControl := GetProcAddress(DLLHandle, PChar(209));
  @VpeResetFontControl := GetProcAddress(DLLHandle, PChar(210));
  @VpeGetProtection := GetProcAddress(DLLHandle, PChar(211));
  @VpeSetProtection := GetProcAddress(DLLHandle, PChar(212));
  @VpeGetEncryption := GetProcAddress(DLLHandle, PChar(213));
  @VpeSetEncryption := GetProcAddress(DLLHandle, PChar(214));
  @VpeGetEncryptionKeyLength := GetProcAddress(DLLHandle, PChar(215));
  @VpeSetEncryptionKeyLength := GetProcAddress(DLLHandle, PChar(216));
  @VpeSetUserPassword := GetProcAddress(DLLHandle, PChar(217));
  @VpeSetOwnerPassword := GetProcAddress(DLLHandle, PChar(218));
  @VpeSetAuthor := GetProcAddress(DLLHandle, PChar(219));
  @VpeSetTitle := GetProcAddress(DLLHandle, PChar(220));
  @VpeSetSubject := GetProcAddress(DLLHandle, PChar(221));
  @VpeSetKeywords := GetProcAddress(DLLHandle, PChar(222));
  @VpeSetCreator := GetProcAddress(DLLHandle, PChar(223));
  @VpeEnableMultiThreading := GetProcAddress(DLLHandle, PChar(224));
  @VpeGetPDFVersion := GetProcAddress(DLLHandle, PChar(225));
  @VpeSetPDFVersion := GetProcAddress(DLLHandle, PChar(226));
  @VpeSetBookmarkDestination := GetProcAddress(DLLHandle, PChar(227));
  @VpeGetBookmarkStyle := GetProcAddress(DLLHandle, PChar(228));
  @VpeSetBookmarkStyle := GetProcAddress(DLLHandle, PChar(229));
  @VpeGetBookmarkColor := GetProcAddress(DLLHandle, PChar(230));
  @VpeSetBookmarkColor := GetProcAddress(DLLHandle, PChar(231));
  @VpeAddBookmark := GetProcAddress(DLLHandle, PChar(232));
  @VpeExtIntDA := GetProcAddress(DLLHandle, PChar(233));
  @VpeSetPictureCache := GetProcAddress(DLLHandle, PChar(234));
  @VpeGetPictureCache := GetProcAddress(DLLHandle, PChar(235));
  @VpeSetPictureX2YResolution := GetProcAddress(DLLHandle, PChar(236));
  @VpeGetPictureX2YResolution := GetProcAddress(DLLHandle, PChar(237));
  @VpeSetPictureBestFit := GetProcAddress(DLLHandle, PChar(238));
  @VpeGetPictureBestFit := GetProcAddress(DLLHandle, PChar(239));
  @VpeSetPictureEmbedInDoc := GetProcAddress(DLLHandle, PChar(240));
  @VpeGetPictureEmbedInDoc := GetProcAddress(DLLHandle, PChar(241));
  @VpeSetPictureDrawExact := GetProcAddress(DLLHandle, PChar(242));
  @VpeGetPictureDrawExact := GetProcAddress(DLLHandle, PChar(243));
  @VpeSetPictureKeepAspect := GetProcAddress(DLLHandle, PChar(244));
  @VpeGetPictureKeepAspect := GetProcAddress(DLLHandle, PChar(245));
  @VpeSetPictureDefaultDPI := GetProcAddress(DLLHandle, PChar(246));
  @VpeSetCornerRadius := GetProcAddress(DLLHandle, PChar(247));
  @VpeGetCornerRadius := GetProcAddress(DLLHandle, PChar(248));
  @VpeSetUnitTransformation := GetProcAddress(DLLHandle, PChar(249));
  @VpeGetUnitTransformation := GetProcAddress(DLLHandle, PChar(250));
  @VpeSetBkgGradientTriColor := GetProcAddress(DLLHandle, PChar(251));
  @VpeGetBkgGradientTriColor := GetProcAddress(DLLHandle, PChar(252));
  @VpeSetBkgGradientMiddleColor := GetProcAddress(DLLHandle, PChar(253));
  @VpeGetBkgGradientMiddleColor := GetProcAddress(DLLHandle, PChar(254));
  @VpeSetBkgGradientMiddleColorPosition := GetProcAddress(DLLHandle, PChar(255));
  @VpeGetBkgGradientMiddleColorPosition := GetProcAddress(DLLHandle, PChar(256));
  @VpeSetMsgCallback := GetProcAddress(DLLHandle, PChar(257));
  @VpeSetFontSubstitution := GetProcAddress(DLLHandle, PChar(258));
  @VpePurgeFontSubstitution := GetProcAddress(DLLHandle, PChar(259));
  @VpeLicenseU := GetProcAddress(DLLHandle, PChar(260));
  @VpeOpenDocU := GetProcAddress(DLLHandle, PChar(261));
  @VpeOpenDocFileU := GetProcAddress(DLLHandle, PChar(262));
  @VpeBarcodeU := GetProcAddress(DLLHandle, PChar(263));
  @VpeGetPictureTypesU := GetProcAddress(DLLHandle, PChar(264));
  @VpePictureU := GetProcAddress(DLLHandle, PChar(265));
  @VpeSetFontU := GetProcAddress(DLLHandle, PChar(266));
  @VpeSelectFontU := GetProcAddress(DLLHandle, PChar(267));
  @VpeSetFontNameU := GetProcAddress(DLLHandle, PChar(268));
  @VpePrintU := GetProcAddress(DLLHandle, PChar(269));
  @VpePrintBoxU := GetProcAddress(DLLHandle, PChar(270));
  @VpeWriteU := GetProcAddress(DLLHandle, PChar(271));
  @VpeWriteBoxU := GetProcAddress(DLLHandle, PChar(272));
  @VpeDefineHeaderU := GetProcAddress(DLLHandle, PChar(273));
  @VpeDefineFooterU := GetProcAddress(DLLHandle, PChar(274));
  @VpeWriteDocU := GetProcAddress(DLLHandle, PChar(275));
  @VpeReadDocU := GetProcAddress(DLLHandle, PChar(276));
  @VpeRenderPrintU := GetProcAddress(DLLHandle, PChar(277));
  @VpeRenderPrintBoxU := GetProcAddress(DLLHandle, PChar(278));
  @VpeRenderWriteU := GetProcAddress(DLLHandle, PChar(279));
  @VpeRenderWriteBoxU := GetProcAddress(DLLHandle, PChar(280));
  @VpeRenderPictureU := GetProcAddress(DLLHandle, PChar(281));
  @VpeGetPicturePageCountU := GetProcAddress(DLLHandle, PChar(282));
  @VpeGetFontNameU := GetProcAddress(DLLHandle, PChar(283));
  @VpeSetFontControlU := GetProcAddress(DLLHandle, PChar(284));
  @VpeSetUserPasswordU := GetProcAddress(DLLHandle, PChar(285));
  @VpeSetOwnerPasswordU := GetProcAddress(DLLHandle, PChar(286));
  @VpeSetAuthorU := GetProcAddress(DLLHandle, PChar(287));
  @VpeSetTitleU := GetProcAddress(DLLHandle, PChar(288));
  @VpeSetSubjectU := GetProcAddress(DLLHandle, PChar(289));
  @VpeSetKeywordsU := GetProcAddress(DLLHandle, PChar(290));
  @VpeSetCreatorU := GetProcAddress(DLLHandle, PChar(291));
  @VpeAddBookmarkU := GetProcAddress(DLLHandle, PChar(292));
  @VpeSetFontSubstitutionU := GetProcAddress(DLLHandle, PChar(293));
  @VpeGetWindowHandle := GetProcAddress(DLLHandle, PChar(294));
  @VpeWindowHandle := GetProcAddress(DLLHandle, PChar(295));
  @VpeEnablePrintSetupDialog := GetProcAddress(DLLHandle, PChar(296));
  @VpeSetGridMode := GetProcAddress(DLLHandle, PChar(297));
  @VpeSetGridVisible := GetProcAddress(DLLHandle, PChar(298));
  @VpeEnableMailButton := GetProcAddress(DLLHandle, PChar(299));
  @VpeEnableCloseButton := GetProcAddress(DLLHandle, PChar(300));
  @VpeEnableHelpRouting := GetProcAddress(DLLHandle, PChar(301));
  @VpeSetPreviewWithScrollers := GetProcAddress(DLLHandle, PChar(302));
  @VpeSetPaperView := GetProcAddress(DLLHandle, PChar(303));
  @VpeSetScale := GetProcAddress(DLLHandle, PChar(304));
  @VpeGetScale := GetProcAddress(DLLHandle, PChar(305));
  @VpeSetScalePercent := GetProcAddress(DLLHandle, PChar(306));
  @VpeGetScalePercent := GetProcAddress(DLLHandle, PChar(307));
  @VpeSetRulersMeasure := GetProcAddress(DLLHandle, PChar(308));
  @VpeSetupPrinter := GetProcAddress(DLLHandle, PChar(309));
  @VpeSetPrintOptions := GetProcAddress(DLLHandle, PChar(310));
  @VpePrintDoc := GetProcAddress(DLLHandle, PChar(311));
  @VpeIsPrinting := GetProcAddress(DLLHandle, PChar(312));
  @VpePreviewDoc := GetProcAddress(DLLHandle, PChar(313));
  @VpePreviewDocSP := GetProcAddress(DLLHandle, PChar(314));
  @VpeCenterPreview := GetProcAddress(DLLHandle, PChar(315));
  @VpeRefreshDoc := GetProcAddress(DLLHandle, PChar(316));
  @VpePictureDIB := GetProcAddress(DLLHandle, PChar(317));
  @VpePictureResID := GetProcAddress(DLLHandle, PChar(318));
  @VpePictureResName := GetProcAddress(DLLHandle, PChar(319));
  @VpeSetPrintPosMode := GetProcAddress(DLLHandle, PChar(320));
  @VpeSetPageScrollerTracking := GetProcAddress(DLLHandle, PChar(321));
  @VpeWriteStatusbar := GetProcAddress(DLLHandle, PChar(322));
  @VpeOpenProgressBar := GetProcAddress(DLLHandle, PChar(323));
  @VpeSetProgressBar := GetProcAddress(DLLHandle, PChar(324));
  @VpeCloseProgressBar := GetProcAddress(DLLHandle, PChar(325));
  @VpeRenderPictureDIB := GetProcAddress(DLLHandle, PChar(326));
  @VpeRenderPictureResID := GetProcAddress(DLLHandle, PChar(327));
  @VpeRenderPictureResName := GetProcAddress(DLLHandle, PChar(328));
  @VpeSetDevice := GetProcAddress(DLLHandle, PChar(329));
  @VpeGetDevice := GetProcAddress(DLLHandle, PChar(330));
  @VpeDevEnum := GetProcAddress(DLLHandle, PChar(331));
  @VpeGetDevEntry := GetProcAddress(DLLHandle, PChar(332));
  @VpeSetDevOrientation := GetProcAddress(DLLHandle, PChar(333));
  @VpeGetDevOrientation := GetProcAddress(DLLHandle, PChar(334));
  @VpeSetDevPaperFormat := GetProcAddress(DLLHandle, PChar(335));
  @VpeGetDevPaperFormat := GetProcAddress(DLLHandle, PChar(336));
  @VpeSetDevPaperWidth := GetProcAddress(DLLHandle, PChar(337));
  @VpeGetDevPaperWidth := GetProcAddress(DLLHandle, PChar(338));
  @VpeSetDevPaperHeight := GetProcAddress(DLLHandle, PChar(339));
  @VpeGetDevPaperHeight := GetProcAddress(DLLHandle, PChar(340));
  @VpeSetDevScalePercent := GetProcAddress(DLLHandle, PChar(341));
  @VpeGetDevScalePercent := GetProcAddress(DLLHandle, PChar(342));
  @VpeSetDevPrintQuality := GetProcAddress(DLLHandle, PChar(343));
  @VpeGetDevPrintQuality := GetProcAddress(DLLHandle, PChar(344));
  @VpeSetDevYResolution := GetProcAddress(DLLHandle, PChar(345));
  @VpeGetDevYResolution := GetProcAddress(DLLHandle, PChar(346));
  @VpeSetDevColor := GetProcAddress(DLLHandle, PChar(347));
  @VpeGetDevColor := GetProcAddress(DLLHandle, PChar(348));
  @VpeSetDevDuplex := GetProcAddress(DLLHandle, PChar(349));
  @VpeGetDevDuplex := GetProcAddress(DLLHandle, PChar(350));
  @VpeSetDevTTOption := GetProcAddress(DLLHandle, PChar(351));
  @VpeGetDevTTOption := GetProcAddress(DLLHandle, PChar(352));
  @VpeDevEnumPaperBins := GetProcAddress(DLLHandle, PChar(353));
  @VpeGetDevPaperBinName := GetProcAddress(DLLHandle, PChar(354));
  @VpeGetDevPaperBinID := GetProcAddress(DLLHandle, PChar(355));
  @VpeSetDevPaperBin := GetProcAddress(DLLHandle, PChar(356));
  @VpeGetDevPaperBin := GetProcAddress(DLLHandle, PChar(357));
  @VpeSetDevCopies := GetProcAddress(DLLHandle, PChar(358));
  @VpeGetDevCopies := GetProcAddress(DLLHandle, PChar(359));
  @VpeSetDevCollate := GetProcAddress(DLLHandle, PChar(360));
  @VpeGetDevCollate := GetProcAddress(DLLHandle, PChar(361));
  @VpeGetDevPrinterOffsetX := GetProcAddress(DLLHandle, PChar(362));
  @VpeGetDevPrinterOffsetY := GetProcAddress(DLLHandle, PChar(363));
  @VpeGetDevPhysPageWidth := GetProcAddress(DLLHandle, PChar(364));
  @VpeGetDevPhysPageHeight := GetProcAddress(DLLHandle, PChar(365));
  @VpeGetDevPrintableWidth := GetProcAddress(DLLHandle, PChar(366));
  @VpeGetDevPrintableHeight := GetProcAddress(DLLHandle, PChar(367));
  @VpeSetDevFromPage := GetProcAddress(DLLHandle, PChar(368));
  @VpeGetDevFromPage := GetProcAddress(DLLHandle, PChar(369));
  @VpeSetDevToPage := GetProcAddress(DLLHandle, PChar(370));
  @VpeGetDevToPage := GetProcAddress(DLLHandle, PChar(371));
  @VpeSetDevToFile := GetProcAddress(DLLHandle, PChar(372));
  @VpeGetDevToFile := GetProcAddress(DLLHandle, PChar(373));
  @VpeSetDevFileName := GetProcAddress(DLLHandle, PChar(374));
  @VpeGetDevFileName := GetProcAddress(DLLHandle, PChar(375));
  @VpeSetDevJobName := GetProcAddress(DLLHandle, PChar(376));
  @VpeGetDevJobName := GetProcAddress(DLLHandle, PChar(377));
  @VpeDevSendData := GetProcAddress(DLLHandle, PChar(378));
  @VpeReadPrinterSetup := GetProcAddress(DLLHandle, PChar(379));
  @VpeWritePrinterSetup := GetProcAddress(DLLHandle, PChar(380));
  @VpeDefineKey := GetProcAddress(DLLHandle, PChar(381));
  @VpeSendKey := GetProcAddress(DLLHandle, PChar(382));
  @VpeSetGUILanguage := GetProcAddress(DLLHandle, PChar(383));
  @VpeSetPreviewCtrl := GetProcAddress(DLLHandle, PChar(384));
  @VpeEnableAutoDelete := GetProcAddress(DLLHandle, PChar(385));
  @VpeClosePreview := GetProcAddress(DLLHandle, PChar(386));
  @VpeIsPreviewVisible := GetProcAddress(DLLHandle, PChar(387));
  @VpeGotoVisualPage := GetProcAddress(DLLHandle, PChar(388));
  @VpeGetVisualPage := GetProcAddress(DLLHandle, PChar(389));
  @VpeDispatchAllMessages := GetProcAddress(DLLHandle, PChar(390));
  @VpeSetBusyProgressBar := GetProcAddress(DLLHandle, PChar(391));
  @VpeSetMailWithDialog := GetProcAddress(DLLHandle, PChar(392));
  @VpeSetMailSubject := GetProcAddress(DLLHandle, PChar(393));
  @VpeSetMailText := GetProcAddress(DLLHandle, PChar(394));
  @VpeSetMailSender := GetProcAddress(DLLHandle, PChar(395));
  @VpeAddMailReceiver := GetProcAddress(DLLHandle, PChar(396));
  @VpeClearMailReceivers := GetProcAddress(DLLHandle, PChar(397));
  @VpeAddMailAttachment := GetProcAddress(DLLHandle, PChar(398));
  @VpeClearMailAttachments := GetProcAddress(DLLHandle, PChar(399));
  @VpeSetMailAutoAttachDocType := GetProcAddress(DLLHandle, PChar(400));
  @VpeGetMailAutoAttachDocType := GetProcAddress(DLLHandle, PChar(401));
  @VpeIsMAPIInstalled := GetProcAddress(DLLHandle, PChar(402));
  @VpeGetMAPIType := GetProcAddress(DLLHandle, PChar(403));
  @VpeMailDoc := GetProcAddress(DLLHandle, PChar(404));
  @VpeMapMessage := GetProcAddress(DLLHandle, PChar(405));
  @VpeSetMinScale := GetProcAddress(DLLHandle, PChar(406));
  @VpeSetMinScalePercent := GetProcAddress(DLLHandle, PChar(407));
  @VpeSetMaxScale := GetProcAddress(DLLHandle, PChar(408));
  @VpeSetMaxScalePercent := GetProcAddress(DLLHandle, PChar(409));
  @VpeSetResourceString := GetProcAddress(DLLHandle, PChar(410));
  @VpeSetGUITheme := GetProcAddress(DLLHandle, PChar(411));
  @VpeGetGUITheme := GetProcAddress(DLLHandle, PChar(412));
  @VpeSetScaleMode := GetProcAddress(DLLHandle, PChar(413));
  @VpeGetScaleMode := GetProcAddress(DLLHandle, PChar(414));
  @VpeZoomPreview := GetProcAddress(DLLHandle, PChar(415));
  @VpeSetPreviewPosition := GetProcAddress(DLLHandle, PChar(416));
  @VpeZoomIn := GetProcAddress(DLLHandle, PChar(417));
  @VpeZoomOut := GetProcAddress(DLLHandle, PChar(418));
  @VpeGetOpenFileName := GetProcAddress(DLLHandle, PChar(419));
  @VpeSetOpenFileName := GetProcAddress(DLLHandle, PChar(420));
  @VpeGetSaveFileName := GetProcAddress(DLLHandle, PChar(421));
  @VpeSetSaveFileName := GetProcAddress(DLLHandle, PChar(422));
  @VpeOpenFileDialog := GetProcAddress(DLLHandle, PChar(423));
  @VpeSaveFileDialog := GetProcAddress(DLLHandle, PChar(424));
  @VpeSetEngineRenderMode := GetProcAddress(DLLHandle, PChar(425));
  @VpeGetEngineRenderMode := GetProcAddress(DLLHandle, PChar(426));
  @VpeSetupPrinterU := GetProcAddress(DLLHandle, PChar(427));
  @VpePictureResNameU := GetProcAddress(DLLHandle, PChar(428));
  @VpeWriteStatusbarU := GetProcAddress(DLLHandle, PChar(429));
  @VpeRenderPictureResNameU := GetProcAddress(DLLHandle, PChar(430));
  @VpeSetDeviceU := GetProcAddress(DLLHandle, PChar(431));
  @VpeGetDeviceU := GetProcAddress(DLLHandle, PChar(432));
  @VpeGetDevEntryU := GetProcAddress(DLLHandle, PChar(433));
  @VpeGetDevPaperBinNameU := GetProcAddress(DLLHandle, PChar(434));
  @VpeSetDevFileNameU := GetProcAddress(DLLHandle, PChar(435));
  @VpeGetDevFileNameU := GetProcAddress(DLLHandle, PChar(436));
  @VpeSetDevJobNameU := GetProcAddress(DLLHandle, PChar(437));
  @VpeGetDevJobNameU := GetProcAddress(DLLHandle, PChar(438));
  @VpeDevSendDataU := GetProcAddress(DLLHandle, PChar(439));
  @VpeReadPrinterSetupU := GetProcAddress(DLLHandle, PChar(440));
  @VpeWritePrinterSetupU := GetProcAddress(DLLHandle, PChar(441));
  @VpeSetMailSubjectU := GetProcAddress(DLLHandle, PChar(442));
  @VpeSetMailTextU := GetProcAddress(DLLHandle, PChar(443));
  @VpeSetMailSenderU := GetProcAddress(DLLHandle, PChar(444));
  @VpeAddMailReceiverU := GetProcAddress(DLLHandle, PChar(445));
  @VpeAddMailAttachmentU := GetProcAddress(DLLHandle, PChar(446));
  @VpeSetResourceStringU := GetProcAddress(DLLHandle, PChar(447));
  @VpeGetOpenFileNameU := GetProcAddress(DLLHandle, PChar(448));
  @VpeSetOpenFileNameU := GetProcAddress(DLLHandle, PChar(449));
  @VpeGetSaveFileNameU := GetProcAddress(DLLHandle, PChar(450));
  @VpeSetSaveFileNameU := GetProcAddress(DLLHandle, PChar(451));
  @VpeSetViewable := GetProcAddress(DLLHandle, PChar(1000));
  @VpeSetPrintable := GetProcAddress(DLLHandle, PChar(1001));
  @VpeSetStreamable := GetProcAddress(DLLHandle, PChar(1002));
  @VpeSetShadowed := GetProcAddress(DLLHandle, PChar(1003));
  @VpeWriteRTF := GetProcAddress(DLLHandle, PChar(1004));
  @VpeWriteBoxRTF := GetProcAddress(DLLHandle, PChar(1005));
  @VpeWriteRTFFile := GetProcAddress(DLLHandle, PChar(1006));
  @VpeWriteBoxRTFFile := GetProcAddress(DLLHandle, PChar(1007));
  @VpeSetFirstIndent := GetProcAddress(DLLHandle, PChar(1008));
  @VpeSetLeftIndent := GetProcAddress(DLLHandle, PChar(1009));
  @VpeSetRightIndent := GetProcAddress(DLLHandle, PChar(1010));
  @VpeSetSpaceBefore := GetProcAddress(DLLHandle, PChar(1011));
  @VpeSetSpaceAfter := GetProcAddress(DLLHandle, PChar(1012));
  @VpeSetSpaceBetween := GetProcAddress(DLLHandle, PChar(1013));
  @VpeSetDefaultTabSize := GetProcAddress(DLLHandle, PChar(1014));
  @VpeSetTab := GetProcAddress(DLLHandle, PChar(1015));
  @VpeClearTab := GetProcAddress(DLLHandle, PChar(1016));
  @VpeClearAllTabs := GetProcAddress(DLLHandle, PChar(1017));
  @VpeSetKeepLines := GetProcAddress(DLLHandle, PChar(1018));
  @VpeSetKeepNextParagraph := GetProcAddress(DLLHandle, PChar(1019));
  @VpeSetParagraphControl := GetProcAddress(DLLHandle, PChar(1020));
  @VpeResetParagraph := GetProcAddress(DLLHandle, PChar(1021));
  @VpeSetRTFFont := GetProcAddress(DLLHandle, PChar(1022));
  @VpeSetRTFColor := GetProcAddress(DLLHandle, PChar(1023));
  @VpeRenderRTF := GetProcAddress(DLLHandle, PChar(1024));
  @VpeRenderBoxRTF := GetProcAddress(DLLHandle, PChar(1025));
  @VpeRenderRTFFile := GetProcAddress(DLLHandle, PChar(1026));
  @VpeRenderBoxRTFFile := GetProcAddress(DLLHandle, PChar(1027));
  @VpeSetCharPlacement := GetProcAddress(DLLHandle, PChar(1028));
  @VpeChartDataCreate := GetProcAddress(DLLHandle, PChar(1029));
  @VpeChartDataAddRow := GetProcAddress(DLLHandle, PChar(1030));
  @VpeChartDataAddColumn := GetProcAddress(DLLHandle, PChar(1031));
  @VpeChartDataAddValue := GetProcAddress(DLLHandle, PChar(1032));
  @VpeChartDataAddGap := GetProcAddress(DLLHandle, PChar(1033));
  @VpeChartDataAddLegend := GetProcAddress(DLLHandle, PChar(1034));
  @VpeChartDataAddXLabel := GetProcAddress(DLLHandle, PChar(1035));
  @VpeChartDataAddYLabel := GetProcAddress(DLLHandle, PChar(1036));
  @VpeChartDataSetXAxisTitle := GetProcAddress(DLLHandle, PChar(1037));
  @VpeChartDataSetYAxisTitle := GetProcAddress(DLLHandle, PChar(1038));
  @VpeChartDataSetColor := GetProcAddress(DLLHandle, PChar(1039));
  @VpeChartDataSetLineStyle := GetProcAddress(DLLHandle, PChar(1040));
  @VpeChartDataSetHatchStyle := GetProcAddress(DLLHandle, PChar(1041));
  @VpeChartDataSetPointType := GetProcAddress(DLLHandle, PChar(1042));
  @VpeChartDataSetMaximum := GetProcAddress(DLLHandle, PChar(1043));
  @VpeChartDataSetMinimum := GetProcAddress(DLLHandle, PChar(1044));
  @VpeSetChartTitle := GetProcAddress(DLLHandle, PChar(1045));
  @VpeSetChartSubTitle := GetProcAddress(DLLHandle, PChar(1046));
  @VpeSetChartFootNote := GetProcAddress(DLLHandle, PChar(1047));
  @VpeSetChartRow := GetProcAddress(DLLHandle, PChar(1048));
  @VpeSetChartGridBkgColor := GetProcAddress(DLLHandle, PChar(1049));
  @VpeSetChartGridBkgMode := GetProcAddress(DLLHandle, PChar(1050));
  @VpeSetChartGridType := GetProcAddress(DLLHandle, PChar(1051));
  @VpeSetChartGridColor := GetProcAddress(DLLHandle, PChar(1052));
  @VpeSetChartXGridStep := GetProcAddress(DLLHandle, PChar(1053));
  @VpeSetChartYGridStep := GetProcAddress(DLLHandle, PChar(1054));
  @VpeSetChartYAutoGridStep := GetProcAddress(DLLHandle, PChar(1055));
  @VpeSetChartLegendPosition := GetProcAddress(DLLHandle, PChar(1056));
  @VpeSetChartLegendBorderStat := GetProcAddress(DLLHandle, PChar(1057));
  @VpeSetChartXLabelState := GetProcAddress(DLLHandle, PChar(1058));
  @VpeSetChartXLabelAngle := GetProcAddress(DLLHandle, PChar(1059));
  @VpeSetChartYLabelState := GetProcAddress(DLLHandle, PChar(1060));
  @VpeSetChartXLabelStartValue := GetProcAddress(DLLHandle, PChar(1061));
  @VpeSetChartXLabelStep := GetProcAddress(DLLHandle, PChar(1062));
  @VpeSetChartYLabelStep := GetProcAddress(DLLHandle, PChar(1063));
  @VpeSetChartYLabelDivisor := GetProcAddress(DLLHandle, PChar(1064));
  @VpeSetChartGridRotation := GetProcAddress(DLLHandle, PChar(1065));
  @VpeSetChartYAxisAngle := GetProcAddress(DLLHandle, PChar(1066));
  @VpeSetChartXAxisAngle := GetProcAddress(DLLHandle, PChar(1067));
  @VpeChart := GetProcAddress(DLLHandle, PChar(1068));
  @VpeSetChartBarWidthFactor := GetProcAddress(DLLHandle, PChar(1069));
  @VpeSetChartLineWidthFactor := GetProcAddress(DLLHandle, PChar(1070));
  @VpeSetChartLegendFontSizeFactor := GetProcAddress(DLLHandle, PChar(1071));
  @VpeSetChartXLabelFontSizeFactor := GetProcAddress(DLLHandle, PChar(1072));
  @VpeSetChartYLabelFontSizeFactor := GetProcAddress(DLLHandle, PChar(1073));
  @VpeSetChartTitleFontSizeFactor := GetProcAddress(DLLHandle, PChar(1074));
  @VpeSetChartSubTitleFontSizeFactor := GetProcAddress(DLLHandle, PChar(1075));
  @VpeSetChartFootNoteFontSizeFactor := GetProcAddress(DLLHandle, PChar(1076));
  @VpeSetChartAxisTitleFontSizeFactor := GetProcAddress(DLLHandle, PChar(1077));
  @VpeSetChartPieLegendWithPercent := GetProcAddress(DLLHandle, PChar(1078));
  @VpeSetChartPieLabelType := GetProcAddress(DLLHandle, PChar(1079));
  @VpeSetEditProtection := GetProcAddress(DLLHandle, PChar(1080));
  @VpeGetEditProtection := GetProcAddress(DLLHandle, PChar(1081));
  @VpeGetViewable := GetProcAddress(DLLHandle, PChar(1082));
  @VpeGetPrintable := GetProcAddress(DLLHandle, PChar(1083));
  @VpeGetStreamable := GetProcAddress(DLLHandle, PChar(1084));
  @VpeGetShadowed := GetProcAddress(DLLHandle, PChar(1085));
  @VpeSetExportNonPrintableObjects := GetProcAddress(DLLHandle, PChar(1086));
  @VpeGetExportNonPrintableObjects := GetProcAddress(DLLHandle, PChar(1087));
  @VpeSetBar2DAlignment := GetProcAddress(DLLHandle, PChar(1088));
  @VpeGetBar2DAlignment := GetProcAddress(DLLHandle, PChar(1089));
  @VpeSetDataMatrixEncodingFormat := GetProcAddress(DLLHandle, PChar(1090));
  @VpeGetDataMatrixEncodingFormat := GetProcAddress(DLLHandle, PChar(1091));
  @VpeSetDataMatrixEccType := GetProcAddress(DLLHandle, PChar(1092));
  @VpeGetDataMatrixEccType := GetProcAddress(DLLHandle, PChar(1093));
  @VpeSetDataMatrixRows := GetProcAddress(DLLHandle, PChar(1094));
  @VpeGetDataMatrixRows := GetProcAddress(DLLHandle, PChar(1095));
  @VpeSetDataMatrixColumns := GetProcAddress(DLLHandle, PChar(1096));
  @VpeGetDataMatrixColumns := GetProcAddress(DLLHandle, PChar(1097));
  @VpeSetDataMatrixMirror := GetProcAddress(DLLHandle, PChar(1098));
  @VpeGetDataMatrixMirror := GetProcAddress(DLLHandle, PChar(1099));
  @VpeSetDataMatrixBorder := GetProcAddress(DLLHandle, PChar(1100));
  @VpeGetDataMatrixBorder := GetProcAddress(DLLHandle, PChar(1101));
  @VpeDataMatrix := GetProcAddress(DLLHandle, PChar(1102));
  @VpeRenderDataMatrix := GetProcAddress(DLLHandle, PChar(1103));
  @VpeMaxiCode := GetProcAddress(DLLHandle, PChar(1104));
  @VpeRenderMaxiCode := GetProcAddress(DLLHandle, PChar(1105));
  @VpeMaxiCodeEx := GetProcAddress(DLLHandle, PChar(1106));
  @VpeRenderMaxiCodeEx := GetProcAddress(DLLHandle, PChar(1107));
  @VpeSetPDF417ErrorLevel := GetProcAddress(DLLHandle, PChar(1108));
  @VpeGetPDF417ErrorLevel := GetProcAddress(DLLHandle, PChar(1109));
  @VpeSetPDF417Rows := GetProcAddress(DLLHandle, PChar(1110));
  @VpeGetPDF417Rows := GetProcAddress(DLLHandle, PChar(1111));
  @VpeSetPDF417Columns := GetProcAddress(DLLHandle, PChar(1112));
  @VpeGetPDF417Columns := GetProcAddress(DLLHandle, PChar(1113));
  @VpePDF417 := GetProcAddress(DLLHandle, PChar(1114));
  @VpeRenderPDF417 := GetProcAddress(DLLHandle, PChar(1115));
  @VpeSetAztecFlags := GetProcAddress(DLLHandle, PChar(1116));
  @VpeGetAztecFlags := GetProcAddress(DLLHandle, PChar(1117));
  @VpeSetAztecControl := GetProcAddress(DLLHandle, PChar(1118));
  @VpeGetAztecControl := GetProcAddress(DLLHandle, PChar(1119));
  @VpeSetAztecMenu := GetProcAddress(DLLHandle, PChar(1120));
  @VpeGetAztecMenu := GetProcAddress(DLLHandle, PChar(1121));
  @VpeSetAztecMultipleSymbols := GetProcAddress(DLLHandle, PChar(1122));
  @VpeGetAztecMultipleSymbols := GetProcAddress(DLLHandle, PChar(1123));
  @VpeSetAztecID := GetProcAddress(DLLHandle, PChar(1124));
  @VpeAztec := GetProcAddress(DLLHandle, PChar(1125));
  @VpeRenderAztec := GetProcAddress(DLLHandle, PChar(1126));
  @VpeSetPictureScale2Gray := GetProcAddress(DLLHandle, PChar(1127));
  @VpeGetPictureScale2Gray := GetProcAddress(DLLHandle, PChar(1128));
  @VpeSetPictureScale2GrayFloat := GetProcAddress(DLLHandle, PChar(1129));
  @VpeGetPictureScale2GrayFloat := GetProcAddress(DLLHandle, PChar(1130));
  @VpeCreateMemoryStream := GetProcAddress(DLLHandle, PChar(1131));
  @VpeStreamRead := GetProcAddress(DLLHandle, PChar(1132));
  @VpeStreamWrite := GetProcAddress(DLLHandle, PChar(1133));
  @VpeGetStreamSize := GetProcAddress(DLLHandle, PChar(1134));
  @VpeStreamIsEof := GetProcAddress(DLLHandle, PChar(1135));
  @VpeGetStreamState := GetProcAddress(DLLHandle, PChar(1136));
  @VpeGetStreamPosition := GetProcAddress(DLLHandle, PChar(1137));
  @VpeStreamSeek := GetProcAddress(DLLHandle, PChar(1138));
  @VpeStreamSeekEnd := GetProcAddress(DLLHandle, PChar(1139));
  @VpeStreamSeekRel := GetProcAddress(DLLHandle, PChar(1140));
  @VpeCloseStream := GetProcAddress(DLLHandle, PChar(1141));
  @VpeWriteRTFStream := GetProcAddress(DLLHandle, PChar(1142));
  @VpeWriteBoxRTFStream := GetProcAddress(DLLHandle, PChar(1143));
  @VpeRenderRTFStream := GetProcAddress(DLLHandle, PChar(1144));
  @VpeRenderBoxRTFStream := GetProcAddress(DLLHandle, PChar(1145));
  @VpePictureStream := GetProcAddress(DLLHandle, PChar(1146));
  @VpeRenderPictureStream := GetProcAddress(DLLHandle, PChar(1147));
  @VpeGetPicturePageCountStream := GetProcAddress(DLLHandle, PChar(1148));
  @VpeWriteDocStream := GetProcAddress(DLLHandle, PChar(1149));
  @VpeWriteDocStreamPageRange := GetProcAddress(DLLHandle, PChar(1150));
  @VpeWriteDocPageRange := GetProcAddress(DLLHandle, PChar(1151));
  @VpeReadDocStream := GetProcAddress(DLLHandle, PChar(1152));
  @VpeReadDocStreamPageRange := GetProcAddress(DLLHandle, PChar(1153));
  @VpeReadDocPageRange := GetProcAddress(DLLHandle, PChar(1154));
  @VpeSetHtmlScale := GetProcAddress(DLLHandle, PChar(1155));
  @VpeGetHtmlScale := GetProcAddress(DLLHandle, PChar(1156));
  @VpeSetHtmlWordSpacing := GetProcAddress(DLLHandle, PChar(1157));
  @VpeGetHtmlWordSpacing := GetProcAddress(DLLHandle, PChar(1158));
  @VpeSetHtmlRtfLineSpacing := GetProcAddress(DLLHandle, PChar(1159));
  @VpeGetHtmlRtfLineSpacing := GetProcAddress(DLLHandle, PChar(1160));
  @VpeSetHtmlCopyImages := GetProcAddress(DLLHandle, PChar(1161));
  @VpeGetHtmlCopyImages := GetProcAddress(DLLHandle, PChar(1162));
  @VpeSetHtmlPageBorders := GetProcAddress(DLLHandle, PChar(1163));
  @VpeGetHtmlPageBorders := GetProcAddress(DLLHandle, PChar(1164));
  @VpeSetHtmlPageSeparators := GetProcAddress(DLLHandle, PChar(1165));
  @VpeGetHtmlPageSeparators := GetProcAddress(DLLHandle, PChar(1166));
  @VpeSetXmlPictureExport := GetProcAddress(DLLHandle, PChar(1167));
  @VpeGetXmlPictureExport := GetProcAddress(DLLHandle, PChar(1168));
  @VpeSetOdtTextWidthScale := GetProcAddress(DLLHandle, PChar(1169));
  @VpeGetOdtTextWidthScale := GetProcAddress(DLLHandle, PChar(1170));
  @VpeSetOdtTextPtSizeScale := GetProcAddress(DLLHandle, PChar(1171));
  @VpeGetOdtTextPtSizeScale := GetProcAddress(DLLHandle, PChar(1172));
  @VpeSetOdtLineHeight := GetProcAddress(DLLHandle, PChar(1173));
  @VpeGetOdtLineHeight := GetProcAddress(DLLHandle, PChar(1174));
  @VpeSetOdtAutoTextboxHeight := GetProcAddress(DLLHandle, PChar(1175));
  @VpeGetOdtAutoTextboxHeight := GetProcAddress(DLLHandle, PChar(1176));
  @VpeSetOdtPositionProtect := GetProcAddress(DLLHandle, PChar(1177));
  @VpeGetOdtPositionProtect := GetProcAddress(DLLHandle, PChar(1178));
  @VpeClearPage := GetProcAddress(DLLHandle, PChar(1179));
  @VpeRemovePage := GetProcAddress(DLLHandle, PChar(1180));
  @VpeInsertPage := GetProcAddress(DLLHandle, PChar(1181));
  @VpeSetInsertAtBottomZOrder := GetProcAddress(DLLHandle, PChar(1182));
  @VpeGetInsertAtBottomZOrder := GetProcAddress(DLLHandle, PChar(1183));
  @VpeWriteRTFU := GetProcAddress(DLLHandle, PChar(1184));
  @VpeWriteBoxRTFU := GetProcAddress(DLLHandle, PChar(1185));
  @VpeWriteRTFFileU := GetProcAddress(DLLHandle, PChar(1186));
  @VpeWriteBoxRTFFileU := GetProcAddress(DLLHandle, PChar(1187));
  @VpeSetRTFFontU := GetProcAddress(DLLHandle, PChar(1188));
  @VpeRenderRTFU := GetProcAddress(DLLHandle, PChar(1189));
  @VpeRenderBoxRTFU := GetProcAddress(DLLHandle, PChar(1190));
  @VpeRenderRTFFileU := GetProcAddress(DLLHandle, PChar(1191));
  @VpeRenderBoxRTFFileU := GetProcAddress(DLLHandle, PChar(1192));
  @VpeChartDataAddLegendU := GetProcAddress(DLLHandle, PChar(1193));
  @VpeChartDataAddXLabelU := GetProcAddress(DLLHandle, PChar(1194));
  @VpeChartDataAddYLabelU := GetProcAddress(DLLHandle, PChar(1195));
  @VpeChartDataSetXAxisTitleU := GetProcAddress(DLLHandle, PChar(1196));
  @VpeChartDataSetYAxisTitleU := GetProcAddress(DLLHandle, PChar(1197));
  @VpeSetChartTitleU := GetProcAddress(DLLHandle, PChar(1198));
  @VpeSetChartSubTitleU := GetProcAddress(DLLHandle, PChar(1199));
  @VpeSetChartFootNoteU := GetProcAddress(DLLHandle, PChar(1200));
  @VpeDataMatrixU := GetProcAddress(DLLHandle, PChar(1201));
  @VpeRenderDataMatrixU := GetProcAddress(DLLHandle, PChar(1202));
  @VpeMaxiCodeU := GetProcAddress(DLLHandle, PChar(1203));
  @VpeRenderMaxiCodeU := GetProcAddress(DLLHandle, PChar(1204));
  @VpeMaxiCodeExU := GetProcAddress(DLLHandle, PChar(1205));
  @VpeRenderMaxiCodeExU := GetProcAddress(DLLHandle, PChar(1206));
  @VpePDF417U := GetProcAddress(DLLHandle, PChar(1207));
  @VpeRenderPDF417U := GetProcAddress(DLLHandle, PChar(1208));
  @VpeSetAztecIDU := GetProcAddress(DLLHandle, PChar(1209));
  @VpeAztecU := GetProcAddress(DLLHandle, PChar(1210));
  @VpeRenderAztecU := GetProcAddress(DLLHandle, PChar(1211));
  @VpePictureStreamU := GetProcAddress(DLLHandle, PChar(1212));
  @VpeRenderPictureStreamU := GetProcAddress(DLLHandle, PChar(1213));
  @VpeWriteDocPageRangeU := GetProcAddress(DLLHandle, PChar(1214));
  @VpeReadDocPageRangeU := GetProcAddress(DLLHandle, PChar(1215));
  @VpeSetJpegExportOptions := GetProcAddress(DLLHandle, PChar(1216));
  @VpeGetJpegExportOptions := GetProcAddress(DLLHandle, PChar(1217));
  @VpeSetTiffExportOptions := GetProcAddress(DLLHandle, PChar(1218));
  @VpeGetTiffExportOptions := GetProcAddress(DLLHandle, PChar(1219));
  @VpeSetGifExportOptions := GetProcAddress(DLLHandle, PChar(1220));
  @VpeGetGifExportOptions := GetProcAddress(DLLHandle, PChar(1221));
  @VpeSetBmpExportOptions := GetProcAddress(DLLHandle, PChar(1222));
  @VpeGetBmpExportOptions := GetProcAddress(DLLHandle, PChar(1223));
  @VpeSetPnmExportOptions := GetProcAddress(DLLHandle, PChar(1224));
  @VpeGetPnmExportOptions := GetProcAddress(DLLHandle, PChar(1225));
  @VpeSetPictureExportColorDepth := GetProcAddress(DLLHandle, PChar(1226));
  @VpeSetPictureExportDither := GetProcAddress(DLLHandle, PChar(1227));
  @VpePictureExportPage := GetProcAddress(DLLHandle, PChar(1228));
  @VpePictureExport := GetProcAddress(DLLHandle, PChar(1229));
  @VpePictureExportPageStream := GetProcAddress(DLLHandle, PChar(1230));
  @VpePictureExportStream := GetProcAddress(DLLHandle, PChar(1231));
  @VpeEnableClickEvents := GetProcAddress(DLLHandle, PChar(1232));
  @VpeSetObjectID := GetProcAddress(DLLHandle, PChar(1233));
  @VpeGetObjectID := GetProcAddress(DLLHandle, PChar(1234));
  @VpeCreateUDO := GetProcAddress(DLLHandle, PChar(1235));
  @VpeGetUDOlParam := GetProcAddress(DLLHandle, PChar(1236));
  @VpeGetUDODC := GetProcAddress(DLLHandle, PChar(1237));
  @VpeGetUDODrawRect := GetProcAddress(DLLHandle, PChar(1238));
  @VpeGetUDOIsPrinting := GetProcAddress(DLLHandle, PChar(1239));
  @VpeGetUDOIsExporting := GetProcAddress(DLLHandle, PChar(1240));
  @VpeGetUDODpiX := GetProcAddress(DLLHandle, PChar(1241));
  @VpeGetUDODpiY := GetProcAddress(DLLHandle, PChar(1242));
  @VpeSetPrintScale := GetProcAddress(DLLHandle, PChar(1243));
  @VpeGetPrintScale := GetProcAddress(DLLHandle, PChar(1244));
  @VpePictureExportPageU := GetProcAddress(DLLHandle, PChar(1245));
  @VpePictureExportU := GetProcAddress(DLLHandle, PChar(1246));
  @VpeLoadTemplate := GetProcAddress(DLLHandle, PChar(2000));
  @VpeLoadTemplateAuthKey := GetProcAddress(DLLHandle, PChar(2001));
  @VpeUseTemplateSettings := GetProcAddress(DLLHandle, PChar(2002));
  @VpeUseTemplateMargins := GetProcAddress(DLLHandle, PChar(2003));
  @VpeSetDateTimeIsUTC := GetProcAddress(DLLHandle, PChar(2004));
  @VpeGetDateTimeIsUTC := GetProcAddress(DLLHandle, PChar(2005));
  @VpeDumpTemplate := GetProcAddress(DLLHandle, PChar(2006));
  @VpeDumpTemplatePage := GetProcAddress(DLLHandle, PChar(2007));
  @VpeGetLastInsertedObject := GetProcAddress(DLLHandle, PChar(2008));
  @VpeFormField := GetProcAddress(DLLHandle, PChar(2009));
  @VpeRenderFormField := GetProcAddress(DLLHandle, PChar(2010));
  @VpeSetCharCount := GetProcAddress(DLLHandle, PChar(2011));
  @VpeGetCharCount := GetProcAddress(DLLHandle, PChar(2012));
  @VpeSetDividerPenSize := GetProcAddress(DLLHandle, PChar(2013));
  @VpeGetDividerPenSize := GetProcAddress(DLLHandle, PChar(2014));
  @VpeSetDividerPenColor := GetProcAddress(DLLHandle, PChar(2015));
  @VpeGetDividerPenColor := GetProcAddress(DLLHandle, PChar(2016));
  @VpeSetAltDividerNPosition := GetProcAddress(DLLHandle, PChar(2017));
  @VpeGetAltDividerNPosition := GetProcAddress(DLLHandle, PChar(2018));
  @VpeSetAltDividerPenSize := GetProcAddress(DLLHandle, PChar(2019));
  @VpeGetAltDividerPenSize := GetProcAddress(DLLHandle, PChar(2020));
  @VpeSetAltDividerPenColor := GetProcAddress(DLLHandle, PChar(2021));
  @VpeGetAltDividerPenColor := GetProcAddress(DLLHandle, PChar(2022));
  @VpeSetBottomLinePenSize := GetProcAddress(DLLHandle, PChar(2023));
  @VpeGetBottomLinePenSize := GetProcAddress(DLLHandle, PChar(2024));
  @VpeSetBottomLinePenColor := GetProcAddress(DLLHandle, PChar(2025));
  @VpeGetBottomLinePenColor := GetProcAddress(DLLHandle, PChar(2026));
  @VpeSetDividerStyle := GetProcAddress(DLLHandle, PChar(2027));
  @VpeGetDividerStyle := GetProcAddress(DLLHandle, PChar(2028));
  @VpeSetAltDividerStyle := GetProcAddress(DLLHandle, PChar(2029));
  @VpeGetAltDividerStyle := GetProcAddress(DLLHandle, PChar(2030));
  @VpeSetFormFieldFlags := GetProcAddress(DLLHandle, PChar(2031));
  @VpeGetFormFieldFlags := GetProcAddress(DLLHandle, PChar(2032));
  @VpeSetUDOlParam := GetProcAddress(DLLHandle, PChar(2033));
  @VpeDeleteObject := GetProcAddress(DLLHandle, PChar(2034));
  @VpeGetFirstObject := GetProcAddress(DLLHandle, PChar(2035));
  @VpeGetClickedObject := GetProcAddress(DLLHandle, PChar(2036));
  @VpeSetTplMaster := GetProcAddress(DLLHandle, PChar(2037));
  @VpeGetTplFieldAsString := GetProcAddress(DLLHandle, PChar(2038));
  @VpeSetTplFieldAsString := GetProcAddress(DLLHandle, PChar(2039));
  @VpeGetTplFieldAsInteger := GetProcAddress(DLLHandle, PChar(2040));
  @VpeSetTplFieldAsInteger := GetProcAddress(DLLHandle, PChar(2041));
  @VpeGetTplFieldAsNumber := GetProcAddress(DLLHandle, PChar(2042));
  @VpeSetTplFieldAsNumber := GetProcAddress(DLLHandle, PChar(2043));
  @VpeGetTplFieldAsDateTime := GetProcAddress(DLLHandle, PChar(2044));
  @VpeSetTplFieldAsDateTime := GetProcAddress(DLLHandle, PChar(2045));
  @VpeGetTplFieldAsOleDateTime := GetProcAddress(DLLHandle, PChar(2046));
  @VpeSetTplFieldAsOleDateTime := GetProcAddress(DLLHandle, PChar(2047));
  @VpeGetTplFieldAsJavaDateTime := GetProcAddress(DLLHandle, PChar(2048));
  @VpeSetTplFieldAsJavaDateTime := GetProcAddress(DLLHandle, PChar(2049));
  @VpeGetTplFieldAsBoolean := GetProcAddress(DLLHandle, PChar(2050));
  @VpeSetTplFieldAsBoolean := GetProcAddress(DLLHandle, PChar(2051));
  @VpeGetTplPageCount := GetProcAddress(DLLHandle, PChar(2052));
  @VpeGetTplPageWidth := GetProcAddress(DLLHandle, PChar(2053));
  @VpeSetTplPageWidth := GetProcAddress(DLLHandle, PChar(2054));
  @VpeGetTplPageHeight := GetProcAddress(DLLHandle, PChar(2055));
  @VpeSetTplPageHeight := GetProcAddress(DLLHandle, PChar(2056));
  @VpeGetTplPageOrientation := GetProcAddress(DLLHandle, PChar(2057));
  @VpeSetTplPageOrientation := GetProcAddress(DLLHandle, PChar(2058));
  @VpeGetTplPaperBin := GetProcAddress(DLLHandle, PChar(2059));
  @VpeSetTplPaperBin := GetProcAddress(DLLHandle, PChar(2060));
  @VpeGetTplLeftMargin := GetProcAddress(DLLHandle, PChar(2061));
  @VpeSetTplLeftMargin := GetProcAddress(DLLHandle, PChar(2062));
  @VpeGetTplRightMargin := GetProcAddress(DLLHandle, PChar(2063));
  @VpeSetTplRightMargin := GetProcAddress(DLLHandle, PChar(2064));
  @VpeGetTplTopMargin := GetProcAddress(DLLHandle, PChar(2065));
  @VpeSetTplTopMargin := GetProcAddress(DLLHandle, PChar(2066));
  @VpeGetTplBottomMargin := GetProcAddress(DLLHandle, PChar(2067));
  @VpeSetTplBottomMargin := GetProcAddress(DLLHandle, PChar(2068));
  @VpeGetTplDataSourceCount := GetProcAddress(DLLHandle, PChar(2069));
  @VpeGetTplDataSourceObject := GetProcAddress(DLLHandle, PChar(2070));
  @VpeGetTplDataSourceFileName := GetProcAddress(DLLHandle, PChar(2071));
  @VpeGetTplDataSourcePrefix := GetProcAddress(DLLHandle, PChar(2072));
  @VpeGetTplDataSourceDescription := GetProcAddress(DLLHandle, PChar(2073));
  @VpeGetTplFieldCount := GetProcAddress(DLLHandle, PChar(2074));
  @VpeGetTplFieldObject := GetProcAddress(DLLHandle, PChar(2075));
  @VpeGetTplFieldName := GetProcAddress(DLLHandle, PChar(2076));
  @VpeGetTplFieldDescription := GetProcAddress(DLLHandle, PChar(2077));
  @VpeFindTplFieldObject := GetProcAddress(DLLHandle, PChar(2078));
  @VpeClearTplFields := GetProcAddress(DLLHandle, PChar(2079));
  @VpeGetTplVpeObjectCount := GetProcAddress(DLLHandle, PChar(2080));
  @VpeGetTplVpeObject := GetProcAddress(DLLHandle, PChar(2081));
  @VpeFindTplVpeObject := GetProcAddress(DLLHandle, PChar(2082));
  @VpeGetFieldAsString := GetProcAddress(DLLHandle, PChar(2083));
  @VpeSetFieldAsString := GetProcAddress(DLLHandle, PChar(2084));
  @VpeGetFieldAsInteger := GetProcAddress(DLLHandle, PChar(2085));
  @VpeSetFieldAsInteger := GetProcAddress(DLLHandle, PChar(2086));
  @VpeGetFieldAsNumber := GetProcAddress(DLLHandle, PChar(2087));
  @VpeSetFieldAsNumber := GetProcAddress(DLLHandle, PChar(2088));
  @VpeGetFieldAsDateTime := GetProcAddress(DLLHandle, PChar(2089));
  @VpeSetFieldAsDateTime := GetProcAddress(DLLHandle, PChar(2090));
  @VpeGetFieldAsOleDateTime := GetProcAddress(DLLHandle, PChar(2091));
  @VpeSetFieldAsOleDateTime := GetProcAddress(DLLHandle, PChar(2092));
  @VpeGetFieldAsJavaDateTime := GetProcAddress(DLLHandle, PChar(2093));
  @VpeSetFieldAsJavaDateTime := GetProcAddress(DLLHandle, PChar(2094));
  @VpeGetFieldAsBoolean := GetProcAddress(DLLHandle, PChar(2095));
  @VpeSetFieldAsBoolean := GetProcAddress(DLLHandle, PChar(2096));
  @VpeGetFieldName := GetProcAddress(DLLHandle, PChar(2097));
  @VpeGetFieldDescription := GetProcAddress(DLLHandle, PChar(2098));
  @VpeGetFieldDataSourceObject := GetProcAddress(DLLHandle, PChar(2099));
  @VpeGetDataSourceFileName := GetProcAddress(DLLHandle, PChar(2100));
  @VpeGetDataSourcePrefix := GetProcAddress(DLLHandle, PChar(2101));
  @VpeGetDataSourceDescription := GetProcAddress(DLLHandle, PChar(2102));
  @VpeGetDataSourceFieldCount := GetProcAddress(DLLHandle, PChar(2103));
  @VpeGetDataSourceFieldObject := GetProcAddress(DLLHandle, PChar(2104));
  @VpeGetInsertedVpeObjectCount := GetProcAddress(DLLHandle, PChar(2105));
  @VpeGetInsertedVpeObject := GetProcAddress(DLLHandle, PChar(2106));
  @VpeGetInsertedVpeObjectPageNo := GetProcAddress(DLLHandle, PChar(2107));
  @VpeGetObjName := GetProcAddress(DLLHandle, PChar(2108));
  @VpeGetObjTemplateObject := GetProcAddress(DLLHandle, PChar(2109));
  @VpeGetObjKind := GetProcAddress(DLLHandle, PChar(2110));
  @VpeGetObjText := GetProcAddress(DLLHandle, PChar(2111));
  @VpeGetObjResolvedText := GetProcAddress(DLLHandle, PChar(2112));
  @VpeGetObjLeft := GetProcAddress(DLLHandle, PChar(2113));
  @VpeGetObjTop := GetProcAddress(DLLHandle, PChar(2114));
  @VpeGetObjRight := GetProcAddress(DLLHandle, PChar(2115));
  @VpeGetObjBottom := GetProcAddress(DLLHandle, PChar(2116));
  @VpeGetObjPictureFileName := GetProcAddress(DLLHandle, PChar(2117));
  @VpeSetObjPictureFileName := GetProcAddress(DLLHandle, PChar(2118));
  @VpeGetNextObject := GetProcAddress(DLLHandle, PChar(2119));
  @VpeGetTplPageObject := GetProcAddress(DLLHandle, PChar(2120));
  @VpeGetTplPageObjWidth := GetProcAddress(DLLHandle, PChar(2121));
  @VpeSetTplPageObjWidth := GetProcAddress(DLLHandle, PChar(2122));
  @VpeGetTplPageObjHeight := GetProcAddress(DLLHandle, PChar(2123));
  @VpeSetTplPageObjHeight := GetProcAddress(DLLHandle, PChar(2124));
  @VpeGetTplPageObjOrientation := GetProcAddress(DLLHandle, PChar(2125));
  @VpeSetTplPageObjOrientation := GetProcAddress(DLLHandle, PChar(2126));
  @VpeGetTplPageObjPaperBin := GetProcAddress(DLLHandle, PChar(2127));
  @VpeSetTplPageObjPaperBin := GetProcAddress(DLLHandle, PChar(2128));
  @VpeGetTplPageObjLeftMargin := GetProcAddress(DLLHandle, PChar(2129));
  @VpeSetTplPageObjLeftMargin := GetProcAddress(DLLHandle, PChar(2130));
  @VpeGetTplPageObjRightMargin := GetProcAddress(DLLHandle, PChar(2131));
  @VpeSetTplPageObjRightMargin := GetProcAddress(DLLHandle, PChar(2132));
  @VpeGetTplPageObjTopMargin := GetProcAddress(DLLHandle, PChar(2133));
  @VpeSetTplPageObjTopMargin := GetProcAddress(DLLHandle, PChar(2134));
  @VpeGetTplPageObjBottomMargin := GetProcAddress(DLLHandle, PChar(2135));
  @VpeSetTplPageObjBottomMargin := GetProcAddress(DLLHandle, PChar(2136));
  @VpeGetTplPageObjVpeObjectCount := GetProcAddress(DLLHandle, PChar(2137));
  @VpeGetTplPageObjVpeObject := GetProcAddress(DLLHandle, PChar(2138));
  @VpeLoadTemplateU := GetProcAddress(DLLHandle, PChar(2139));
  @VpeLoadTemplateAuthKeyU := GetProcAddress(DLLHandle, PChar(2140));
  @VpeFormFieldU := GetProcAddress(DLLHandle, PChar(2141));
  @VpeRenderFormFieldU := GetProcAddress(DLLHandle, PChar(2142));
  @VpeGetTplFieldAsStringU := GetProcAddress(DLLHandle, PChar(2143));
  @VpeSetTplFieldAsStringU := GetProcAddress(DLLHandle, PChar(2144));
  @VpeGetTplFieldAsIntegerU := GetProcAddress(DLLHandle, PChar(2145));
  @VpeSetTplFieldAsIntegerU := GetProcAddress(DLLHandle, PChar(2146));
  @VpeGetTplFieldAsNumberU := GetProcAddress(DLLHandle, PChar(2147));
  @VpeSetTplFieldAsNumberU := GetProcAddress(DLLHandle, PChar(2148));
  @VpeGetTplFieldAsDateTimeU := GetProcAddress(DLLHandle, PChar(2149));
  @VpeSetTplFieldAsDateTimeU := GetProcAddress(DLLHandle, PChar(2150));
  @VpeGetTplFieldAsOleDateTimeU := GetProcAddress(DLLHandle, PChar(2151));
  @VpeSetTplFieldAsOleDateTimeU := GetProcAddress(DLLHandle, PChar(2152));
  @VpeGetTplFieldAsJavaDateTimeU := GetProcAddress(DLLHandle, PChar(2153));
  @VpeSetTplFieldAsJavaDateTimeU := GetProcAddress(DLLHandle, PChar(2154));
  @VpeGetTplFieldAsBooleanU := GetProcAddress(DLLHandle, PChar(2155));
  @VpeSetTplFieldAsBooleanU := GetProcAddress(DLLHandle, PChar(2156));
  @VpeGetTplDataSourceFileNameU := GetProcAddress(DLLHandle, PChar(2157));
  @VpeGetTplDataSourcePrefixU := GetProcAddress(DLLHandle, PChar(2158));
  @VpeGetTplDataSourceDescriptionU := GetProcAddress(DLLHandle, PChar(2159));
  @VpeGetTplFieldNameU := GetProcAddress(DLLHandle, PChar(2160));
  @VpeGetTplFieldDescriptionU := GetProcAddress(DLLHandle, PChar(2161));
  @VpeFindTplFieldObjectU := GetProcAddress(DLLHandle, PChar(2162));
  @VpeFindTplVpeObjectU := GetProcAddress(DLLHandle, PChar(2163));
  @VpeGetFieldAsStringU := GetProcAddress(DLLHandle, PChar(2164));
  @VpeSetFieldAsStringU := GetProcAddress(DLLHandle, PChar(2165));
  @VpeGetFieldNameU := GetProcAddress(DLLHandle, PChar(2166));
  @VpeGetFieldDescriptionU := GetProcAddress(DLLHandle, PChar(2167));
  @VpeGetDataSourceFileNameU := GetProcAddress(DLLHandle, PChar(2168));
  @VpeGetDataSourcePrefixU := GetProcAddress(DLLHandle, PChar(2169));
  @VpeGetDataSourceDescriptionU := GetProcAddress(DLLHandle, PChar(2170));
  @VpeGetObjNameU := GetProcAddress(DLLHandle, PChar(2171));
  @VpeGetObjTextU := GetProcAddress(DLLHandle, PChar(2172));
  @VpeGetObjResolvedTextU := GetProcAddress(DLLHandle, PChar(2173));
  @VpeGetObjPictureFileNameU := GetProcAddress(DLLHandle, PChar(2174));
  @VpeSetObjPictureFileNameU := GetProcAddress(DLLHandle, PChar(2175));
{ @VpeXyz := GetProcAddress(DLLHandle, 'VpeXyz'); }

    DLLInitialized := true;

  end;
end;



   { ============ }
   { FinalizeVPE: }
   { ============ }
procedure FinalizeVPE;
begin
  if DLLInitialized then
    FreeLibrary(DLLHandle);
end;


   { =============== }
   { initialization: }
   { =============== }
initialization
  DllInitialized := false;
  DllHandle := 0;

{$IFNDEF WIN32}
  AddExitProc(FinalizeVPE);
{$ENDIF WIN32}

  InitializeVPE;


   { ============= }
   { finalization: }
   { ============= }
{$IFDEF WIN32}
finalization
  FinalizeVPE;
{$ENDIF WIN32}
end.
