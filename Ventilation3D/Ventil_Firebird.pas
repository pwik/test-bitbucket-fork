Unit Ventil_Firebird;
                    
{$Include 'Ventil_Directives.pas'}

Interface
Uses SysUtils, Db, Forms, CatalBdd,
     AccesBdd;

Var

{$IF Defined(OLD_ATVENTIL) or Defined(OLD_CHIFFRAGEVENTIL)}
  Base_SystemesVentilation : TBaseDeDonnees;
{$Else}
{$EndIf}
  Base_Etude               : TBaseDeDonnees;
//  Base_Edibatec            : TBaseDeDonnees;
  Base_FTK                 : TBaseDeDonnees;

{ -------------- Acces aux bases ------------------------ }
Function  Connection_Etude(_NomFichier: String): Boolean;
Procedure Deconnection_Etude;
Function  Connection_Logiciel: Boolean;
Function  Connection_FTK: Boolean;
Procedure Deconnection_Logiciel;
{ -------------- Acces aux tables ----------------------- }
Procedure Chargement_Structure_Etude;
Procedure Chargement_Structure_SystemesVentilation;
Procedure Chargement_Structure_FTK;

Procedure Finalisation_Tables;

Var
{$IfDef OLD_ATVENTIL}
  Table_SystVent_Definition     : TTableDeDonneesBBS;
  Table_SystVent_Bouches        : TTableDeDonneesBBS;
  Table_SystVent_Deperditions   : TTableDeDonneesBBS;
  Table_SystVent_EntreesAir     : TTableDeDonneesBBS;
{$Else}
{$EndIf}
{$IfDef OLD_CHIFFRAGEVENTIL}
  Table_SystVent_CorrespBouches : TTableDeDonneesBBS;
  Table_SystVent_EntreesAirDn   : TTableDeDonneesBBS;
  Table_SystVent_EntreesAirDnVIM   : TTableDeDonneesBBS;
  Table_Syst_Gammes             : TTableDeDonneesBBS;
{$Else}
{$EndIf}

  Table_Biblio_DataCaisson  : TTableDeDonneesBBS;
  Table_Biblio_ImpCaractCaisson   : TTableDeDonneesBBS;
  Table_Biblio_CaissonPressionConstante : TTableDeDonneesBBS;

  Table_Etude_Generale                   : TTableDeDonneesBBS;
  Table_Etude_Batiment                   : TTableDeDonneesBBS;
  Table_Etude_Logement                   : TTableDeDonneesBBS;
  Table_Etude_DimLogement                : TTableDeDonneesBBS;
  Table_Etude_DnLogement                 : TTableDeDonneesBBS;
  Table_Etude_Objet                      : TTableDeDonneesBBS;
  Table_Etude_Troncon                    : TTableDeDonneesBBS;
  Table_Etude_Caisson                    : TTableDeDonneesBBS;
  Table_Etude_Plenum                     : TTableDeDonneesBBS;
  Table_Etude_Bifurcation                : TTableDeDonneesBBS;
  Table_Etude_TeSouche                   : TTableDeDonneesBBS;
  Table_Etude_Colonne                    : TTableDeDonneesBBS;
  Table_Etude_Collecteur                 : TTableDeDonneesBBS;
  Table_Etude_Bouche                     : TTableDeDonneesBBS;
  Table_Etude_EA                         : TTableDeDonneesBBS;
  Table_Etude_RegulateurDebit            : TTableDeDonneesBBS;
  Table_Etude_Silencieux                 : TTableDeDonneesBBS;
  Table_Etude_ChgtDiam                   : TTableDeDonneesBBS;
  Table_Etude_AccGeneric                 : TTableDeDonneesBBS;  
  Table_Etude_ClapetCoupeFeu             : TTableDeDonneesBBS;
  Table_Etude_Coudes                     : TTableDeDonneesBBS;  
  Table_Etude_SortieToit                 : TTableDeDonneesBBS;
  Table_Etude_Registre                   : TTableDeDonneesBBS;
  Table_Etude_TronconDessinConduit       : TTableDeDonneesBBS;
  Table_Etude_TronconDessinSortieCaisson : TTableDeDonneesBBS;
  Table_Etude_TourelleTete               : TTableDeDonneesBBS;
  Table_Etude_TourelleIntermediaire      : TTableDeDonneesBBS;
  Table_Etude_TourellePlenum             : TTableDeDonneesBBS;
  Table_Etude_TourelleBifurcation        : TTableDeDonneesBBS;

  { Chiffrage }
  Table_Etude_Chiffrage_Gen     : TTableDeDonneesBBS;
  Table_Etude_Chiffrage_Elt     : TTableDeDonneesBBS;
  Table_Etude_Chiffrage_Produit : TTableDeDonneesBBS;
  Table_Etude_Chiffrage_Classe  : TTableDeDonneesBBS;

  Table_Etude_DataExport : TTableDeDonneesBBS;

  { Fiches techniques }
  Table_FTK_Bouches    : TTableDeDonneesBBS;
  Table_FTK_Entrees    : TTableDeDonneesBBS;
  Table_FTK_Autres     : TTableDeDonneesBBS;
  Table_FTK_Silencieux : TTableDeDonneesBBS;
  Table_FTK_Caissons   : TTableDeDonneesBBS;
  Table_FTK_PriseRejet : TTableDeDonneesBBS;
  Table_FTK_Grilles    : TTableDeDonneesBBS;
  Table_FTK_Composants : TTableDeDonneesBBS;

Implementation
Uses BbsgTec, CAD_ViloUtils, Ventil_Utils, Ventil_StructureTables{$IfDef AERAU_CLIMAWIN}, BbsBdd{$EndIf};

{ ************************************************************************************************************************************************** }
Function Connection_Etude(_NomFichier: String): Boolean;
Var
  m_Chemin : String;
  m_Nom    : String;
  m_Res    : Boolean;
Begin
  If IsNil(Base_Etude) Then
        Base_Etude := TBaseDeDonnees.Create(TYPEBASE);
  m_Chemin := ExtractFilePath(_NomFichier);
  m_Nom    := ExtractFileName(_NomFichier);
  Base_Etude.Parametre_Base_Locale(m_Chemin, m_Nom, 'SYSDBA', 'masterkey');
//  Base_Etude.Parametre_Base_Locale(m_Chemin, m_Nom, User_BBS, Passwd_BBS);
  m_Res := Base_Etude.Connection;
  If m_Res Then Chargement_Structure_Etude;
  Result := m_res;
End;
{ ************************************************************************************************************************************************** }
Procedure Deconnection_Etude;
Begin
  If IsNotNil(Base_Etude) Then
        Base_Etude.Deconnection;
  FreeAndNil(Base_Etude);
End;
{ ************************************************************************************************************************************************** }
Function Connection_Logiciel: Boolean;
Var
{$IF Defined(OLD_ATVENTIL) or Defined(OLD_CHIFFRAGEVENTIL)}
  m_Chemin : String;
  m_Nom    : String;
{$Else}
{$EndIf}
  m_ResVent: Boolean;
Begin
{$IF Defined(OLD_ATVENTIL) or Defined(OLD_CHIFFRAGEVENTIL)}
  Base_SystemesVentilation := TBaseDeDonnees.Create(TYPEBASE);
  m_Chemin := ExtractFilePath(Application.Exename) + 'Bibliotheque\';
{$IfDef AERAU_CLIMAWIN}
  m_Nom    := 'BIBLIOTHEQUE.BBS';
  Base_SystemesVentilation.Parametre_Base_Locale(m_Chemin, m_Nom, User_BBS, Passwd_BBS);
{$Else}
  m_Nom    := 'BIBLIOTHEQUE.FDB';
//firedac
//  Base_SystemesVentilation.Parametre_Base_Locale(m_Chemin, m_Nom, 'SYSDBA', 'masterkey');
  Base_SystemesVentilation.Parametre_Base_Locale(m_Chemin, m_Nom, User_BBS, Passwd_BBS);
{$EndIf}
  m_ResVent := Base_SystemesVentilation.Connection;
{$Else}
  m_ResVent := True;
{$EndIf}
  If m_ResVent Then Chargement_Structure_SystemesVentilation;
  {$IfDef ANJOS_OPTIMA3D} Try Result := m_ResVent And Connection_FTK; Except Result := m_ResVent; End; {$Else} Result := m_ResVent; {$EndIf}
End;
{ ************************************************************************************************************************************************** }
Function Connection_FTK: Boolean;
Var
  m_Chemin : String;
  m_Nom    : String;
Begin
  Base_FTK := TBaseDeDonnees.Create(TYPEBASE);
  m_Chemin := ExtractFilePath(Application.Exename) + 'Bibliotheque\';
  m_Nom    := 'FTK.FDB';
  Base_FTK.Parametre_Base_Locale(m_Chemin, m_Nom, 'SYSDBA', 'masterkey');
  Result := Base_FTK.Connection;
  If Result Then Chargement_Structure_FTK;
End;
{ ************************************************************************************************************************************************** }
Procedure Deconnection_Logiciel;
Begin
{$IF Defined(OLD_ATVENTIL) or Defined(OLD_CHIFFRAGEVENTIL)}
  If IsNotNil(Base_SystemesVentilation) Then Base_SystemesVentilation.Deconnection;
{$Else}
{$EndIf}
  If IsNotNil(Base_FTK) Then Base_FTK.Deconnection;
End;
{ ************************************************************************************************************************************************** }
Procedure Chargement_Structure_Etude;
Var
  Save_AccesBDD_Verbose : Boolean;
Begin

  Save_AccesBDD_Verbose := AccesBDD_Verbose;
{$IfDef DEBUG}
  AccesBDD_Verbose := DelphiRuning;
{$Else}
  AccesBDD_Verbose := False;
{$EndIf}

  SetStructure_Etude_Generale(Base_Etude, Table_Etude_Generale);
  SetStructure_Etude_Batiment(Base_Etude, Table_Etude_Batiment);
  SetStructure_Etude_Logement(Base_Etude, Table_Etude_Logement);
  SetStructure_Etude_DimLogement(Base_Etude, Table_Etude_DimLogement);
  SetStructure_Etude_DnLogement(Base_Etude, Table_Etude_DnLogement);
  SetStructure_Etude_Objet(Base_Etude, Table_Etude_Objet);
  SetStructure_Etude_Troncon(Base_Etude, Table_Etude_Troncon);
  SetStructure_Etude_Caisson(Base_Etude, Table_Etude_Caisson);
  SetStructure_Etude_Plenum(Base_Etude, Table_Etude_Plenum);
  SetStructure_Etude_Bifurcation(Base_Etude, Table_Etude_Bifurcation);
  SetStructure_Etude_TeSouche(Base_Etude, Table_Etude_TeSouche);
  SetStructure_Etude_Colonne(Base_Etude, Table_Etude_Colonne);
  SetStructure_Etude_Collecteur(Base_Etude, Table_Etude_Collecteur);
  SetStructure_Etude_Bouche(Base_Etude, Table_Etude_Bouche);
  SetStructure_Etude_EA(Base_Etude, Table_Etude_EA);
  SetStructure_Etude_ChgtDiam(Base_Etude, Table_Etude_ChgtDiam);
  SetStructure_Etude_ClapetCoupeFeu(Base_Etude, Table_Etude_ClapetCoupeFeu);
  SetStucture_Etude_Coudes(Base_Etude, Table_Etude_Coudes);
  SetStucture_Etude_AccGeneric(Base_Etude, Table_Etude_AccGeneric);
  SetStructure_Etude_SortieToit(Base_Etude, Table_Etude_SortieToit);
  SetStructure_Etude_RegulateurDebit(Base_Etude, Table_Etude_RegulateurDebit);
  SetStructure_Etude_Silencieux(Base_Etude, Table_Etude_Silencieux);
  SetStructure_Etude_DataExport(Base_Etude, Table_Etude_DataExport);
  SetStructure_Etude_Registre(Base_Etude, Table_Etude_Registre);
  SetStructure_Etude_TronconDessinConduit(Base_Etude, Table_Etude_TronconDessinConduit);
  SetStructure_Etude_TronconDessinSortieCaisson(Base_Etude, Table_Etude_TronconDessinSortieCaisson);
  SetStructure_Etude_TourelleTete(Base_Etude, Table_Etude_TourelleTete);
  SetStructure_Etude_TourelleIntermediaire(Base_Etude, Table_Etude_TourelleIntermediaire);
  SetStructure_Etude_TourellePlenum(Base_Etude, Table_Etude_TourellePlenum);
  SetStructure_Etude_TourelleBifurcation(Base_Etude, Table_Etude_TourelleBifurcation);
  { Chiffrage }
{$IfNDef AERAU_CLIMAWIN}
  SetStructure_Etude_Chiffrage_Gen(Base_Etude, Table_Etude_Chiffrage_Gen);
  SetStructure_Etude_Chiffrage_Elt(Base_Etude, Table_Etude_Chiffrage_Elt);
  SetStructure_Etude_Chiffrage_Produit(Base_Etude, Table_Etude_Chiffrage_Produit);
  SetStructure_Etude_Chiffrage_Classe(Base_Etude, Table_Etude_Chiffrage_Classe);
{$EndIf}

  AccesBDD_Verbose := Save_AccesBDD_Verbose;
End;
{ ************************************************************************************************************************************************** }
Procedure Chargement_Structure_SystemesVentilation;
Var
  Save_AccesBDD_Verbose : Boolean;
Begin

  Save_AccesBDD_Verbose := AccesBDD_Verbose;
{$IfDef DEBUG}
  AccesBDD_Verbose := True;
{$Else}
  AccesBDD_Verbose := False;
{$EndIf}
{$IfDef OLD_ATVENTIL}
  { D�finition }
  SetStructure_SystVent_Definitions(Base_SystemesVentilation, Table_SystVent_Definition);
  { Bouches }
  SetStructure_SystVent_Bouches(Base_SystemesVentilation, Table_SystVent_Bouches);
  { Entr�es d'air }
  SetStructure_SystVent_EntreesAir(Base_SystemesVentilation, Table_SystVent_EntreesAir);
{$IfNDef AERAU_CLIMAWIN}
  { Valeurs de Deperditions pour calculs de coef RT }
  SetStructure_SystVent_Deperditions(Base_SystemesVentilation, Table_SystVent_Deperditions);
{$EndIf}
{$Else}
{$EndIf}

{$IfDef OLD_CHIFFRAGEVENTIL}
  SetStructure_SystVent_BouchesCorresp(Base_SystemesVentilation, Table_SystVent_CorrespBouches);
  { Correspondances Entr�es d'air pour acoust Dn }
  SetStructure_SystVent_EntreesAirDn(Base_SystemesVentilation, Table_SystVent_EntreesAirDn);
  SetStructure_SystVent_EntreesAirDnVIM(Base_SystemesVentilation, Table_SystVent_EntreesAirDnVIM);
  { Gammmes par d�faut }

  SetStructure_SystVent_GammesDefaut(Base_SystemesVentilation, Table_Syst_Gammes);
{$Else}
{$EndIf}
  { Data Caisson }




{$IfNDef AERAU_CLIMAWIN}
  SetStructure_Biblio_DataCaisson(CatBDD.DataBase_Banque, Table_Biblio_DataCaisson);
{$EndIf}
{$IfDef VIM_OPTAIR}
  {$IfNDef AERAU_CLIMAWIN}
  { Data Impression Caisson }
  SetStructure_Biblio_ImpCaractCaisson(CatBDD.DataBase_Banque, Table_Biblio_ImpCaractCaisson);
  SetStructure_Biblio_CaissonPressionConstante(CatBDD.DataBase_Banque, Table_Biblio_CaissonPressionConstante);
  {$EndIf}
{$EndIf}
{$IfDef MVN_MVNAIR}
  {$IfNDef AERAU_CLIMAWIN}
  { Data Impression Caisson }
  SetStructure_Biblio_ImpCaractCaisson(CatBDD.DataBase_Banque, Table_Biblio_ImpCaractCaisson);
  SetStructure_Biblio_CaissonPressionConstante(CatBDD.DataBase_Banque, Table_Biblio_CaissonPressionConstante);
  {$EndIf}
{$EndIf}

  AccesBDD_Verbose := Save_AccesBDD_Verbose;

End;
{ ************************************************************************************************************************************************** }
Procedure Chargement_Structure_FTK;
Var
  Save_AccesBDD_Verbose : Boolean;
Begin

  Save_AccesBDD_Verbose := AccesBDD_Verbose;
{$IfDef DEBUG}
  AccesBDD_Verbose := True;
{$Else}
  AccesBDD_Verbose := False;
{$EndIf}
  { Bouches }
  Set_Structure_FTK_Bouches(Base_FTK, Table_FTK_Bouches);
  { Entr�es d'air }
  Set_Structure_FTK_Entrees(Base_FTK, Table_FTK_Entrees);
  { Autres }
  Set_Structure_FTK_Autres(Base_FTK, Table_FTK_Autres);
  { Silencieux }
  Set_Structure_FTK_Silencieux(Base_FTK, Table_FTK_Silencieux);
  { Caissons }
  Set_Structure_FTK_Caissons(Base_FTK, Table_FTK_Caissons);
  { PriseRejet }
  Set_Structure_FTK_PriseRejet(Base_FTK, Table_FTK_PriseRejet);
  { Grilles }
  Set_Structure_FTK_Grilles(Base_FTK, Table_FTK_Grilles);
  { Composants }
  Set_Structure_FTK_Composants(Base_FTK, Table_FTK_Composants);

  AccesBDD_Verbose := Save_AccesBDD_Verbose;
End;
{ ************************************************************************************************************************************************** }
Procedure Finalisation_Tables;
begin
{$IfDef OLD_ATVENTIL}
  Table_SystVent_Definition.Free;
  Table_SystVent_Bouches.Free;
  Table_SystVent_Deperditions.Free;
  Table_SystVent_EntreesAir.Free;
{$Else}
{$EndIf}
{$IfDef OLD_CHIFFRAGEVENTIL}
  Table_SystVent_CorrespBouches.Free;
  Table_SystVent_EntreesAirDn.Free;
  Table_SystVent_EntreesAirDnVIM.Free;
  Table_Syst_Gammes.Free;
{$Else}
{$EndIf}

  { Fiches techniques }
  Table_FTK_Bouches.Free;
  Table_FTK_Entrees.Free;
  Table_FTK_Autres.Free;
  Table_FTK_Silencieux.Free;
  Table_FTK_Caissons.Free;
  Table_FTK_PriseRejet.Free;
  Table_FTK_Grilles.Free;
  Table_FTK_Composants.Free;

  CatBDD.Destruction_Base;
end;
{ ************************************************************************************************************************************************** }
End.
