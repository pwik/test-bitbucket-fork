object Fic_Code: TFic_Code
  Left = 328
  Top = 264
  BorderStyle = bsToolWindow
  Caption = 'Initialisation du logiciel'
  ClientHeight = 364
  ClientWidth = 460
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object BitBtn1: TBitBtn
    Left = 148
    Top = 335
    Width = 75
    Height = 25
    Kind = bkOK
    NumGlyphs = 2
    TabOrder = 0
  end
  object BitBtn2: TBitBtn
    Left = 236
    Top = 335
    Width = 75
    Height = 25
    Kind = bkCancel
    NumGlyphs = 2
    TabOrder = 1
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 460
    Height = 329
    Align = alTop
    Color = clWhite
    TabOrder = 2
    object Comments: TLabel
      Left = 187
      Top = 131
      Width = 86
      Height = 13
      Caption = 'Code d'#39'acc'#232's :'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      WordWrap = True
      IsControl = True
    end
    object PresentationBbs: TImage
      Left = 96
      Top = 56
      Width = 273
      Height = 73
      Center = True
      Proportional = True
      Stretch = True
      Transparent = True
    end
    object Code_acces: TEdit
      Left = 172
      Top = 144
      Width = 116
      Height = 24
      BevelKind = bkFlat
      BevelOuter = bvNone
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
    end
    object Panel2: TPanel
      Left = 30
      Top = 171
      Width = 400
      Height = 142
      BevelOuter = bvSpace
      Color = 16744576
      TabOrder = 1
      object ProductName: TLabel
        Left = 14
        Top = 16
        Width = 30
        Height = 13
        Caption = 'Nom '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        IsControl = True
      end
      object Version: TLabel
        Left = 14
        Top = 48
        Width = 50
        Height = 13
        Caption = 'Adresse '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        IsControl = True
      end
      object Copyright: TLabel
        Left = 14
        Top = 112
        Width = 59
        Height = 13
        Caption = 'CP et ville'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        IsControl = True
      end
      object Label2: TLabel
        Left = 14
        Top = 80
        Width = 30
        Height = 13
        Caption = 'Suite'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        IsControl = True
      end
      object Nom: TEdit
        Left = 84
        Top = 8
        Width = 300
        Height = 21
        Cursor = crIBeam
        MaxLength = 40
        TabOrder = 0
      end
      object Adresse: TEdit
        Left = 84
        Top = 40
        Width = 300
        Height = 21
        Cursor = crIBeam
        MaxLength = 40
        TabOrder = 1
      end
      object Suite: TEdit
        Left = 84
        Top = 72
        Width = 300
        Height = 21
        Cursor = crIBeam
        MaxLength = 40
        TabOrder = 2
      end
      object Ville: TEdit
        Left = 159
        Top = 104
        Width = 225
        Height = 21
        Cursor = crIBeam
        MaxLength = 40
        TabOrder = 4
      end
      object Code_Postal: TEdit
        Left = 84
        Top = 104
        Width = 47
        Height = 21
        TabOrder = 3
      end
    end
    object StaticText1: TStaticText
      Left = 115
      Top = 32
      Width = 230
      Height = 20
      Caption = 'Informations utilisateur et licence'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
    end
  end
end
