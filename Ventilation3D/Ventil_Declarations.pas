Unit Ventil_Declarations;

{$Include 'Ventil_Directives.pas'}

Interface

Uses
  BBScad_Interface_Aeraulique,
  Ventil_Etude,
  Ventil_SystemeVMC,
  Ventil_EdibatecProduits,
  Ventil_Options,
  Ventil_Const,
  Ventil_Types;

Var
  InterfaceCAD        : IBBScad_Aeraulique;
  ModeAero            : TBBScad_ModeAero;
  Etude               : TVentil_Etude;
  SystemesVMC         : TListeSystemes;
  FabricantsVMC       : TEdibatec_ListeFabricants;
{$IFDEF  VIM_SELECTIONCAISSON}
{$Else}
  DataImpCaractCaisson: TVentil_ListImpCaractCaisson;
{$EndIf}
  EdibatecDispo       : Boolean;
  BaseEdibatec        : TEdibatec_ListeClasses;
  Options             : TVentil_Options;
//  GammesChiffrage     : Array[1..c_NbGammesChiffrage] Of String;
  LogicielDevis       : String;
  VersionRnD          : Boolean = False;
Implementation

End.
