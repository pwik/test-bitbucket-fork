unit Ventil_Utils;

interface

{$Include 'Ventil_Directives.pas'}

Uses ShellApi,
     Windows,
     Ventil_SystemeVMC,
     Types,
     SysUtils,
     Classes,
     StrUtils,
     Math,
     BbsgFonc,
     Controls;


Function XML2VentilBool(_Val : Integer) : Integer;
Function XML2SystemeVMC(_Val : Integer) : TVentil_Systeme;
Function VentilBool2XML(_Val : Integer) : Integer;
Function SystemeVMC2XML(_Val : TVentil_Systeme) : Integer;

Function GetIndiceDiametreEquivalent(_Dim1 : Integer; _Dim2 : Integer) : Integer;
Function GetDiametreEquivalent(_Dim1 : Integer; _Dim2 : Integer) : Integer;
Function GetRealDiametreEquivalent(_Dim1 : Integer; _Dim2 : Integer) : Integer;

Procedure ThemingForm(_Component : TComponent; _IDTheme : Integer; _GererPanel : Boolean = True);

Function GetChrDiam : String;



implementation


Uses
        Graphics, Forms, ExtCtrls, StdCtrls, VirtualTrees, AdvGrid, ComCtrls, AdvToolBar, AdvToolBarStylers,
        BBScad_Interface_Commun,
        Ventil_Declarations,
        Ventil_Const;



{ ************************************************************************************************************************************************** }
Function XML2VentilBool(_Val : Integer) : Integer;
begin
    if XML2Bool(_Val) then
      Result := c_Oui
    else
      Result := c_Non;
end;
{ ************************************************************************************************************************************************** }
Function XML2SystemeVMC(_Val : Integer) : TVentil_Systeme;
begin
  Result := Nil;

  case _Val of
    1 : Result := SystemesVMC.SystemeHygroANonGaz;
    2 : Result := SystemesVMC.SystemeHygroBNonGaz;
    3 : Result := SystemesVMC.SystemeHygroGaz;
    4 : Result := SystemesVMC.SystemeHygroANonGaz;
    5 : Result := SystemesVMC.SystemeAutoNonGaz;
  end;

end;

{ ************************************************************************************************************************************************** }
Function VentilBool2XML(_Val : Integer) : Integer;
begin
  if _val = c_Oui then
    Result := Bool2XML(True)
  else
    Result := Bool2XML(False);
end;
{ ************************************************************************************************************************************************** }
Function SystemeVMC2XML(_Val : TVentil_Systeme) : Integer;
begin

  if _Val.Hygro and _Val.HygroA then
    Result := 1
  else
  if _Val.Hygro and _Val.HygroB then
    Result := 2
  else
  if _Val.Hygro and _Val.HygroGaz then
    Result := 3
  else
  if not(_Val.Hygro) then
    Result := 4
  else
//  if _Val.Hygro and _Val.HygroA then
    Result := 5;
{
//  Result := 2;

  case _Val of
    0 : Result := 1;
    1 : Result := 2;
    2 : Result := 3;
    3 : Result := 4;
    4 : Result := 5;
  end;
}
end;
{ ************************************************************************************************************************************************** }
Function GetIndiceDiametreEquivalent(_Dim1 : Integer; _Dim2 : Integer) : Integer;
Var
  cpt : Integer;
  DiamCalc : Single;
begin
  Result := c_Diam_125;
  DiamCalc := GetRealDiametreEquivalent(_Dim1, _Dim2);

    if DiamCalc <= c_ListeDiametres[Low(c_ListeDiametres)] then
      Exit;

    for cpt := High(c_ListeDiametres) DownTo Low(c_ListeDiametres) do
      if DiamCalc >= c_ListeDiametres[cpt] then
      begin
      Result := Min(High(c_ListeDiametres), Cpt);
      Exit;
      end;


end;
{ ************************************************************************************************************************************************** }
Function GetDiametreEquivalent(_Dim1 : Integer; _Dim2 : Integer) : Integer;
begin
  Result := (c_ListeDiametres[GetIndiceDiametreEquivalent(_Dim1, _Dim2)])
end;
{ ************************************************************************************************************************************************** }
Function GetRealDiametreEquivalent(_Dim1 : Integer; _Dim2 : Integer) : Integer;
begin
  Result := Round(2 * (_Dim1 * _Dim2) / (_Dim1 + _Dim2));
end;



{ ************************************************************************************************************************************************** }
Procedure ThemingForm(_Component : TComponent; _IDTheme : Integer; _GererPanel : Boolean = True);
Var
        i:Integer;
        UsedColor : TColor;
        UsedColorGradientFrom : TColor;
        UsedColorGradientTo : TColor;
        UsedColorTextToolBar : TColor;
Const
        Couleur1 = $00E0C7AD;
        CouleurGradientFrom1 = $00FCE1CB;
        CouleurGradientTo1 = $00E0A57D;
        CouleurTextTooleBar1 = ClWhite;

        Couleur2 = ClWhite;
        CouleurGradientFrom2 = $00CECECE;
        CouleurGradientTo2 = $00DBDBDB;
        CouleurTextTooleBar2 = ClBlack;

        Couleur3 = clMedGray;
        CouleurGradientFrom3 = $00CFF0EA;
        CouleurGradientTo3 = $008CC0B1;
        CouleurTextTooleBar3 = ClBlack;
Begin
        Case _IDTheme of
        c_BatimentCollectif :
                Begin
                UsedColor := Couleur1;
                UsedColorGradientFrom := CouleurGradientFrom1;
                UsedColorGradientTo := CouleurGradientTo1;
                UsedColorTextToolBar := CouleurTextTooleBar1;
                End;
        c_BatimentTertiaire :
                Begin
                UsedColor := Couleur2;
                UsedColorGradientFrom := CouleurGradientFrom2;
                UsedColorGradientTo := CouleurGradientTo2;
                UsedColorTextToolBar := CouleurTextTooleBar2;
                End;
        c_BatimentHotel :
                Begin
                UsedColor := Couleur3;
                UsedColorGradientFrom := CouleurGradientFrom3;
                UsedColorGradientTo := CouleurGradientTo3;
                UsedColorTextToolBar := CouleurTextTooleBar3;
                End;
        End;


        if (_Component is TForm) then
                Begin
                TForm(_Component).Color := UsedColor;
                End
        else
        If (_Component Is TPanel) and _GererPanel Then
                Begin
                TPanel(_Component).Color := UsedColor;
                End
        Else
        If _Component Is TLabel Then
                Begin
                End

        Else
        If _Component Is TPageControl Then
                Begin
                End

        Else
        If _Component Is TTabSheet Then
                Begin
                End

        Else
        If _Component Is TAdvToolBarOfficeStyler Then
                Begin
                TAdvToolBarOfficeStyler(_Component).Color.Color := UsedColorGradientFrom;
                TAdvToolBarOfficeStyler(_Component).Color.ColorTo := UsedColorGradientTo;
                TAdvToolBarOfficeStyler(_Component).CaptionAppearance.CaptionColor := UsedColor;
                TAdvToolBarOfficeStyler(_Component).CaptionAppearance.CaptionColorTo := UsedColor;
                TAdvToolBarOfficeStyler(_Component).CaptionAppearance.CaptionBorderColor := UsedColor;
                TAdvToolBarOfficeStyler(_Component).CaptionAppearance.CaptionTextColor := UsedColorTextToolBar;
                TAdvToolBarOfficeStyler(_Component).DockColor.Color := UsedColorGradientFrom;
                TAdvToolBarOfficeStyler(_Component).DockColor.ColorTo := UsedColorGradientTo;
                End

        Else
        If _Component Is TAdvDockPanel Then
                Begin
                End
        Else
        If _Component Is TVirtualStringTree Then
                Begin
                End

        Else
        If _Component Is TButton Then
                Begin
                End
        Else
        If _Component Is TComboBox Then
                Begin
                End
        Else
        If _Component Is TAdvStringGrid Then
                Begin
                TAdvStringGrid(_Component).ControlLook.FixedGradientFrom := UsedColorGradientFrom;
                TAdvStringGrid(_Component).ControlLook.FixedGradientTo := UsedColorGradientTo;
                End;


        For i:=0 To _Component.ComponentCount-1 Do
                ThemingForm(_Component.Components[i], _IDTheme, _GererPanel);



End;
{ ************************************************************************************************************************************************** }
Function GetChrDiam : String;
begin
  if Options.TypeEtiquette = e3Dm_Vectoriel then
    Result := Chr(Cst_CharDiamEtVct)
  else
  if Options.TypeEtiquette = e3Dm_Bitmap then
    Result := Chr(Cst_CharDiamEtImg);
end;
{ ************************************************************************************************************************************************** }
end.
