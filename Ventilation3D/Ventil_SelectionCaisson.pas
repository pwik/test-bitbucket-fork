Unit Ventil_SelectionCaisson;

{$Include 'Ventil_Directives.pas'}

Interface

Uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls, Dialogs, Buttons, ExtCtrls, Grids, BaseGrid, AdvGrid, ComCtrls, AdvEdit,
     Math,
     Ventil_Troncon,
     Ventil_Bezier, Ventil_EdibatecProduits,
     DBClient, AdvObj;

Const
  Cst_PanelCriteres    = 1;
  Cst_PanelCaisson     = 2;
  Cst_PanelAccessoires = 3;
  Cst_PanelAnnexe      = 4;


Type
{ ************************************************************************************************************************************************** }
  TDlg_SelectionCaisson = class(TForm)
    PanelBas: TPanel;
    Btn_Cancel: TBitBtn;
    Btn_Ok: TBitBtn;
    Panel_Preselection: TPanel;
    Btn_Precedent: TBitBtn;
    Btn_Suivant: TBitBtn;
    Panel1: TPanel;
    GroupBox1: TGroupBox;
    CheckBox_Caisson: TCheckBox;
    CheckBox_Tourelle: TCheckBox;
    GroupBox2: TGroupBox;
    CheckBox_Direct: TCheckBox;
    CheckBox_Poulie: TCheckBox;
    GroupBox3: TGroupBox;
    CheckBox_UneVitesse: TCheckBox;
    CheckBoxDeuxVitesses: TCheckBox;
    GroupBox4: TGroupBox;
    CheckBox_NonAgree: TCheckBox;
    CheckBox_40012: TCheckBox;
    GroupBox5: TGroupBox;
    CheckBox_Monophase: TCheckBox;
    CheckBox_Triphase: TCheckBox;
    Panel_Selection: TPanel;
    Panel3: TPanel;
    GroupBox6: TGroupBox;
    Label_Lib_1: TLabel;
    Label_Lib_2: TLabel;
    Label_Val_1: TLabel;
    Label_Val_2: TLabel;
    Grid_Selection: TAdvStringGrid;
    Panel_Accessoires: TPanel;
    Panel4: TPanel;
    Grid_InfosCaisson: TAdvStringGrid;
    Label_Ventilateur: TLabel;
    Bevel1: TBevel;
    Panel_AccessoiresAnnexes: TPanel;
    Panel5: TPanel;
    PageControl1: TPageControl;
    TabSheetMontage: TTabSheet;
    Radio_Adhesif: TRadioGroup;
    GroupBox7: TGroupBox;
    Edit_VIS13: TAdvEdit;
    Label3: TLabel;
    Label4: TLabel;
    Edit_VIS19: TAdvEdit;
    GroupBox8: TGroupBox;
    Label5: TLabel;
    Label6: TLabel;
    Edit_M0: TAdvEdit;
    Edit_M1: TAdvEdit;
    Check_IsolantMur: TCheckBox;
    Check_Fourreaux: TCheckBox;
    TabSheetBavettEtColliers: TTabSheet;
    TabSheetSupportsTelescopiques: TTabSheet;
    GroupBox9: TGroupBox;
    Grid_Bavettes: TAdvStringGrid;
    GroupBox10: TGroupBox;
    CheckBox_Universel: TCheckBox;
    CheckBox_125300: TCheckBox;
    CheckBox_160300: TCheckBox;
    CheckBox_125100: TCheckBox;
    CheckBox_160100: TCheckBox;
    Radio_Colliers: TRadioGroup;
    Grid_Accessoires: TAdvStringGrid;
    Radio_ColliersSupp: TGroupBox;
    Grid_ColliersSupp: TAdvStringGrid;
    GroupBox_BandePerf: TGroupBox;
    CheckBox_BandePerf: TCheckBox;
    Edit_NbRlxBandePerforee: TAdvEdit;
    Label7: TLabel;
    Edit_NbSupp160300: TAdvEdit;
    Edit_NbSupp125300: TAdvEdit;
    Edit_NbSupp160100: TAdvEdit;
    Edit_NbSupp125100: TAdvEdit;
    Edit_NbSuppUni: TAdvEdit;
    Text_LongueurBandePerf: TStaticText;
    StaticText1: TStaticText;
    CheckBox_ColliersSuppIsoles: TCheckBox;
    CheckBox_DTU: TCheckBox;
    Label_Lib_6: TLabel;
    Label_Val_6: TLabel;
    Label_Val_7: TLabel;
    Label_Lib_7: TLabel;
    RadioBezier: TRadioGroup;
    CheckBoxCompteBavettes: TCheckBox;
    GroupBoxVentilEcoEtDepressostat: TGroupBox;
    CheckBoxEco: TCheckBox;
    CheckBoxNEcoADepr: TCheckBox;
    CheckBoxNEcoSDepr: TCheckBox;
    Btn_RecalculAccesoires: TBitBtn;
    TabSheetTrappesDeVisite: TTabSheet;
    Grid_TrappesVisite: TAdvStringGrid;
    Label_Lib_3: TLabel;
    Label_Val_3: TLabel;
    Label_Lib_8: TLabel;
    Label_Val_8: TLabel;
    Label_Lib_9: TLabel;
    Label_Val_9: TLabel;
    Image_DPVent: TImage;
    TabSheetActhysTerrasse: TTabSheet;
    TabSheetActhysColonnes: TTabSheet;
    TabSheetActhysTrainasse: TTabSheet;
    CheckBoxACT_PST: TCheckBox;
    CheckBoxACT_CablesSuspensionRapide: TCheckBox;
    AdvEditACT_CablesSuspensionRapide: TAdvEdit;
    LabelCableSuspensionRapide: TLabel;
    LabelCollierUniverselTerrasse: TLabel;
    AdvEditACT_CollierUniverselTerrasse: TAdvEdit;
    CheckBoxACT_CollierUniverselTerrasse: TCheckBox;
    CheckBoxACT_TrappesDeVisite: TCheckBox;
    CheckBoxACT_BandeAdhesive: TCheckBox;
    RadioGroupACT_TeSouche: TRadioGroup;
    CheckBoxACT_FourreauDeTraversee: TCheckBox;
    CheckBoxACT_Joint: TCheckBox;
    CheckBoxACT_CollierUniverselColonne: TCheckBox;
    AdvEditACT_CollierUniverselColonne: TAdvEdit;
    LabelCollierUniverselColonne: TLabel;
    RadioGroupACT_Terminaux: TRadioGroup;
    RadioGroupACT_LongueurBarreT2A: TRadioGroup;
    Label_Lib_4: TLabel;
    Label_Lib_5: TLabel;
    Label_Lib_10: TLabel;
    Label_Val_10: TLabel;
    Label_Val_4: TLabel;
    Label_Val_5: TLabel;
    Label_Sep_1: TLabel;
    Label_Sep_2: TLabel;
    Label_Sep_3: TLabel;
    Label_Sep_4: TLabel;
    Label_Sep_5: TLabel;
    Label_Sep_6: TLabel;
    Label_Sep_7: TLabel;
    Label_Sep_8: TLabel;
    Label_Sep_9: TLabel;
    Label_Sep_10: TLabel;
    procedure Btn_SuivantClick(Sender: TObject);
    procedure Btn_PrecedentClick(Sender: TObject);
    procedure Grid_SelectionSelectCell(Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
    procedure Grid_SelectionRowChanging(Sender: TObject; OldRow, NewRow: Integer; var Allow: Boolean);
    procedure Grid_InfosCaissonCanEditCell(Sender: TObject; ARow, ACol: Integer; var CanEdit: Boolean);
    procedure Radio_ColliersClick(Sender: TObject);
    procedure Grid_ColliersSuppIsFixedCell(Sender: TObject; ARow, ACol: Integer; var IsFixed: Boolean);
    procedure Grid_BavettesIsFixedCell(Sender: TObject; ARow, ACol: Integer; var IsFixed: Boolean);
    procedure Grid_TrappesVisiteIsFixedCell(Sender: TObject; ARow, ACol: Integer; var IsFixed: Boolean);
    procedure CheckBox_UniverselClick(Sender: TObject);
    procedure CheckBox_125100Click(Sender: TObject);
    procedure CheckBox_BandePerfClick(Sender: TObject);
    procedure CheckBox_NonAgreeClick(Sender: TObject);
    procedure Grid_SelectionCanEditCell(Sender: TObject; ARow,
      ACol: Integer; var CanEdit: Boolean);
    procedure CheckBoxCompteBavettesClick(Sender: TObject);
    procedure Btn_RecalculAccesoiresClick(Sender: TObject);
    procedure Image_DPVentClick(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure CheckBoxACT_CollierUniverselTerrasseClick(Sender: TObject);
    procedure Grid_SelectionClickSort(Sender: TObject; ACol: Integer);
  Private
    ListeCaissons    : TStringList;
    CaissonReseau    : TVentil_Troncon;
    FPanel_Actif     : TPanel;
    NonAgreePossible : Boolean;
    CanContinueAfterSelection : Boolean;
    Function InitFiche : Boolean;
    Procedure InitChoix;
    Procedure GetChoix;
    Procedure Enable_NonAgree;
    Procedure EnableControls;
    function TestValiditeReseau : Boolean;
    Procedure GoToSelection;
    Procedure GoToAccessoires;
    Procedure GoToAccessoiresAnnexes;
    Procedure AffecteAssociations;
    Procedure GoToPreselection;
    Procedure GenereListeCaissons;
    Procedure AffListeCaissons;
    function  GetPanelActif: TPanel;
    procedure SetPanelActif(const Value: TPanel);
    function  CaissonGetNbPoints(_Caisson: TEdibatec_Caisson) : Integer;
    Function  CaissonValide(_Caisson: TEdibatec_Caisson) : Boolean;
    Function  Caisson_Valide_Options(_Caisson: TEdibatec_Caisson) : Boolean;
    Function  Caisson_Valide_Fonctionnement(_Caisson: TEdibatec_Caisson) : Boolean;
    Function  Caisson_Valide_Selection3Courbes(_Caisson: TEdibatec_Caisson) : Boolean;
    Function  Caisson_Valide_SelectionPlageFonctionnement(_Caisson: TEdibatec_Caisson) : Boolean;
    Function  Caisson_Valide_SelectionDebitConstantOld(_Caisson: TEdibatec_Caisson) : Boolean;
    Function  Caisson_Valide_SelectionDebitConstant(_Caisson: TEdibatec_Caisson; _ForceOld : Boolean = False) : Boolean;
    Function  Caisson_Valide_SelectionPlageFonctionnementEtCourbeLimite(_Caisson: TEdibatec_Caisson) : Boolean;
    Function  Caisson_Valide_Selection3CourbesBergeron(_Caisson: TEdibatec_Caisson) : Boolean;
    Function  Caisson_Valide_SelectionPlageFonctionnementBergeron(_Caisson: TEdibatec_Caisson) : Boolean;
    Function  Caisson_Valide_SelectionDebitConstantBergeron(_Caisson: TEdibatec_Caisson) : Boolean;
    Function  Caisson_Valide_SelectionPlageFonctionnementEtCourbeLimiteBergeron(_Caisson: TEdibatec_Caisson) : Boolean;
    Function  Caisson_Valide_SelectionPlageFonctionnementEcowatt2Bergeron(_Caisson: TEdibatec_Caisson) : Boolean;
    Function  Caisson_Valide_SelectionMVNCourbeMontante(_Caisson: TEdibatec_Caisson) : Boolean;
    Function  Caisson_Valide_SelectionCalculActhys(_Caisson: TEdibatec_Caisson) : Boolean;
    Function  Caisson_Valide_Options_TypeCaisson(_Caisson: TEdibatec_Caisson) : Boolean;
    Function  Caisson_Valide_Options_Entrainement(_Caisson: TEdibatec_Caisson) : Boolean;
    Function  Caisson_Valide_Options_Alimentation(_Caisson: TEdibatec_Caisson) : Boolean;
    Function  Caisson_Valide_Options_TypeMoteur(_Caisson: TEdibatec_Caisson) : Boolean;
    Function  Caisson_Valide_Options_Depressostat(_Caisson: TEdibatec_Caisson) : Boolean;
    Function  Caisson_Valide_Options_Agrementation(_Caisson: TEdibatec_Caisson) : Boolean;
    Function  Caisson_Valide_PositionsSorties(_Caisson: TEdibatec_Caisson) : Boolean;
    Function  Caisson_Valide_DebitMiniMaxi(_Caisson: TEdibatec_Caisson) : Boolean;
    Function  Caisson_Valide_DPMiniMaxi(_Caisson: TEdibatec_Caisson) : Boolean;
    Function  Caisson_Valide_DeltaDP_MVN(_Caisson: TEdibatec_Caisson) : Boolean;

    Procedure AffDebitsCaisson(_Caisson: TVentil_Troncon);
    Procedure GetChoix_Chiffrage;
    Procedure InitChoix_Chiffrage;
    Function IsProduitASSOPAPV(m_ProduitAsso: TEdibatec_Association) : Boolean;
    Function IsCaissonSelectionFromGammeHUCF : boolean;
    Function IsCaissonSelectionFromGammeJBEBorJBHBorJBEBECO(_Caisson: TEdibatec_Caisson) : boolean;
    Procedure Grid_Selection_TrieCol0;
  Public
    CaissonSelection : TEdibatec_Caisson;
    Property Panel_Actif : TPanel Read GetPanelActif write SetPanelActif;
  End;


{ ************************************************************************************************************************************************** }
  TSelectionCaisson_SaveOptions = Class
    CanGet                              : Boolean;
    OldReferenceCaisson                 : String;
    //Crit�res De S�lection Du Ventilateur
    CheckBox_Caisson_Checked            : Boolean;
    CheckBox_Tourelle_Checked           : Boolean;
    CheckBox_Direct_Checked             : Boolean;
    CheckBox_Poulie_Checked             : Boolean;
    CheckBox_UneVitesse_Checked         : Boolean;
    CheckBoxDeuxVitesses_Checked        : Boolean;
    CheckBox_NonAgree_Checked           : Boolean;
    CheckBox_40012_Checked              : Boolean;
    CheckBox_MonoPhase_Checked          : Boolean;
    CheckBox_TriPhase_Checked           : Boolean;
//    Radio_Depressostat_ItemIndex        : Integer;
//    Radio_VentilEco_ItemIndex           : Integer;
//    Radio_EcoEtDepr_ItemIndex           : Integer;
    CheckBoxEco_Checked                 : Boolean;
    CheckBoxNEcoADepr_Checked           : Boolean;
    CheckBoxNEcoSDepr_Checked           : Boolean;

    CheckBox_DTU_Checked                : Boolean;
    RadioBezier_ItemIndex               : integer;
    //Accessoire Associ�s Au Ventilateur
    ListAccessoireVentilo_Qte           : Array of Integer;
    //Accessoires Annexes
    //-->Montage
    Radio_Adhesif_ItemIndex             : integer;
    Edit_Vis13_Text                     : String;
    Edit_Vis19_Text                     : String;
    Edit_M0_Text                        : String;
    Edit_M1_Text                        : String;
    Check_IsolantMur_Checked            : Boolean;
    Check_Fourreaux_Checked             : Boolean;
    //-->Bavettes et colliers
    Radio_Colliers_ItemIndex            : integer;
    Bavette_OuiNon                      : Boolean;
    Bavette_125_Qte                     : Integer;
    Bavette_160_Qte                     : Integer;
    Bavette_200_Qte                     : Integer;
    Bavette_250_Qte                     : Integer;
    Bavette_315_Qte                     : Integer;
    Bavette_355_Qte                     : Integer;
    Bavette_400_Qte                     : Integer;
    Bavette_450_Qte                     : Integer;
    Bavette_500_Qte                     : Integer;
    Bavette_560_Qte                     : Integer;
    Bavette_630_Qte                     : Integer;
    Bavette_710_Qte                     : Integer;

    Trappe_125_Qte                     : Integer;
    Trappe_160_Qte                     : Integer;
    Trappe_200_Qte                     : Integer;
    Trappe_250_Qte                     : Integer;
    Trappe_315_Qte                     : Integer;
    Trappe_355_Qte                     : Integer;
    Trappe_400_Qte                     : Integer;
    Trappe_450_Qte                     : Integer;
    Trappe_500_Qte                     : Integer;
    Trappe_560_Qte                     : Integer;
    Trappe_630_Qte                     : Integer;
    Trappe_710_Qte                     : Integer;
    Trappe_800_Qte                     : Integer;
    Trappe_900_Qte                     : Integer;

    //-->Supports Universels
    CheckBox_Universel_Checked          : Boolean;
    CheckBox_125300_Checked             : Boolean;
    CheckBox_160300_Checked             : Boolean;
    CheckBox_125100_Checked             : Boolean;
    CheckBox_160100_Checked             : Boolean;
    CheckBox_ColliersSuppIsoles_Checked : Boolean;
    Edit_NbSupp160300_Text              : String;
    Edit_NbSupp125300_Text              : String;
    Edit_NbSupp160100_Text              : String;
    Edit_NbSupp125100_Text              : String;
    Edit_NbSuppUni_Text                 : String;
    CheckBox_BandePerf_Checked          : Boolean;
    Edit_NbRlxBandePerforee_Text        : String;
    Collier_125_Qte                     : Integer;
    Collier_160_Qte                     : Integer;
    Collier_200_Qte                     : Integer;
    Collier_250_Qte                     : Integer;
    Collier_315_Qte                     : Integer;
    Collier_355_Qte                     : Integer;
    Collier_400_Qte                     : Integer;
    Collier_450_Qte                     : Integer;
    Collier_500_Qte                     : Integer;
    Collier_560_Qte                     : Integer;
    Collier_630_Qte                     : Integer;
    Collier_710_Qte                     : Integer;
    Collier_800_Qte                     : Integer;
    Collier_900_Qte                     : Integer;

    ACT_PST                             : Boolean;
    ACT_CSR                             : Boolean;
    ACT_CSR_Nb_Text                     : String;
    ACT_CUT                             : Boolean;
    ACT_CUT_Nb_Text                     : String;
    ACT_Trappes                         : Boolean;
    ACT_ROA                             : Boolean;
    ACT_TeSouche                        : Integer;
    ACT_FT                              : Boolean;
    ACT_JTD                             : Boolean;
    ACT_CUC                             : Boolean;
    ACT_CUC_Nb_Text                     : String;
    ACT_Terminaux                       : Integer;
    ACT_LgT2A                           : Integer;

    Private
        Procedure InitOptionsCriteresSelectionFiche(_Fiche : TDlg_SelectionCaisson);
        Procedure InitOptionsAccessoiresAssociesFiche(_Fiche : TDlg_SelectionCaisson);
        Procedure InitOptionsAccessoiresAnnexesFiche(_Fiche : TDlg_SelectionCaisson);
        Procedure InitOptionsFiche(_Fiche : TDlg_SelectionCaisson);
        Procedure SaveOptionsCriteresSelectionFiche(_Fiche : TDlg_SelectionCaisson);
        Procedure SaveOptionsAccessoiresAssociesFiche(_Fiche : TDlg_SelectionCaisson);
        Procedure SaveOptionsAccessoiresAnnexesFiche(_Fiche : TDlg_SelectionCaisson);
        Procedure SaveOptionsFiche(_Fiche : TDlg_SelectionCaisson);
    Public
        Procedure Copier(_Source : TSelectionCaisson_SaveOptions);
        Procedure Sauvegarder(_Donnees : TClientDataSet);
        Procedure Ouvrir(_Donnees : TClientDataSet);        
        Constructor Create;
  End;


  Function CalculVitessePoint(_Point: TPointDebitPression; _Caisson: TEdibatec_Caisson; _Mini: Boolean ): Real;

Var
  Dlg_SelectionCaisson: TDlg_SelectionCaisson;


Function SelectionDuCaisson(Const _Caisson: TVentil_Troncon; _CaissonEdibatec: TEdibatec_Caisson): TEdibatec_Caisson;

implementation
Uses
  {$IFDEF  VIM_SELECTIONCAISSON}
  VIM_SelectionCaisson,
  {$ENDIF}
  Ventil_Colonne,
  DateUtils,
  Ventil_Edibatec, Ventil_Bouche, Ventil_Const, Ventil_Declarations, Ventil_Commun, Ventil_TronconDessin, Ventil_SortieToiture, Ventil_Utils,
  AdvSmoothMessageDialog,
  AdvStyleIf,
  Ventil_TourelleTete,
  Ventil_Caisson,
  Ventil_TeSouche,
  BBScad_Interface_Aeraulique,
  BbsgFonc,
  BBS_Message;
{$R *.dfm}


{ **************************************************************************************************************************************************** }
Function CalculDpCourbe(Const _Point: TPointDebitPression; Const _SurCourbe: TPointsControleBezier): Real;
Var
  m_P1Courbe,
  m_P2Courbe: TPointDebitPression;
  a, b      : Real;
Begin
  EncadrePoint(_SurCourbe, _Point, m_P1Courbe, m_P2Courbe);
  a := (m_P1Courbe.DP - m_P2Courbe.DP) / (m_P1Courbe.Q - m_P2Courbe.Q);
  b := m_P1Courbe.DP - a * m_P1Courbe.Q;
  Result := a * _Point.Q + b;
  If Result < 1 Then Result := 0;
End;
{ **************************************************************************************************************************************************** }
Function CalculQCourbe(Const _Point: TPointDebitPression; Const _SurCourbe: TPointsControleBezier): Real;
Var
  m_P1Courbe,
  m_P2Courbe: TPointDebitPression;
  a, b      : Real;
Begin
  EncadrePoint(_SurCourbe, _Point, m_P1Courbe, m_P2Courbe);
  a := (m_P1Courbe.DP - m_P2Courbe.DP) / (m_P1Courbe.Q - m_P2Courbe.Q);
  b := m_P1Courbe.DP - a * m_P1Courbe.Q;
  Result := (_Point.DP - b) / a;
  If Result < 1 Then Result := 0;
End;
{ **************************************************************************************************************************************************** }
Function CalculVitessePoint(_Point: TPointDebitPression; _Caisson: TEdibatec_Caisson; _Mini: Boolean): Real;
Var
  m_P1Courbe,
  m_P2Courbe: TPointDebitPression;
  a, b, c   : Real;
  Delta     : Real; // Pour l'equation cQ� - aQ - b = 0;
  R1, R2    : Real;
  QRes      : Real;
  Courbe    : String;
Begin
  If Dlg_SelectionCaisson.RadioBezier.ItemIndex = 0 Then Begin
    If _Mini Then Courbe := _Caisson.CourbeDebitPressionMini
             Else Courbe := _Caisson.CourbeDebitPressionMaxi;
    EncadrePoint(StringToPointsBezier_4points(Courbe), _Point, m_P1Courbe, m_P2Courbe);
  End Else Begin
    If _Mini Then EncadrePoint(StringToPointsBezier_XPoints(_Caisson.ListeDebitsMinis, _Caisson.ListeDPMinis), _Point, m_P1Courbe, m_P2Courbe)
             Else EncadrePoint(StringToPointsBezier_XPoints(_Caisson.ListeDebitsMaxis, _Caisson.ListeDPMaxis), _Point, m_P1Courbe, m_P2Courbe);
  End;
  
  a := (m_P1Courbe.DP - m_P2Courbe.DP) / (m_P1Courbe.Q - m_P2Courbe.Q);
  b := m_P1Courbe.DP - a * m_P1Courbe.Q;
  c := _Point.DP / (Sqr(_Point.Q));
  Delta := Sqr(a) + (4 * b * c);
  R1 := (a - Sqrt(Delta)) / (2 * c);
  R2 := (a + Sqrt(Delta)) / (2 * c);
  QRes := Max(R1, R2);
  If _Mini Then Result := _Caisson.VitesseMini * ( _Point.Q / QRes)
           Else Result := _Caisson.VitesseMaxi * ( _Point.Q / QRes );
        End;
{ **************************************************************************************************************************************************** }
Procedure TDlg_SelectionCaisson.InitChoix_Chiffrage;
Const
  cst_RowHeaderColliers1 : Array[1..4] Of String = ('� 315', '� 355', '� 400', '� 450');
  cst_RowHeaderColliers2 : Array[1..4] Of String = ('� 500', '� 560', '� 630', '� 710');  
  cst_RowHeaderColliers3 : Array[1..4] Of String = ('� 800', '� 900', '', '');
  cst_RowHeaderTrappes1 : Array[1..4] Of String = ('� 315', '� 355', '� 400', '� 450');
  cst_RowHeaderTrappes2 : Array[1..4] Of String = ('� 500', '� 560', '� 630', '� 710');
  cst_RowHeaderTrappes3 : Array[1..4] Of String = ('� 800', '� 900', '', '');
  cst_RowHeader1 : Array[1..3] Of String = ('� 250', '� 315', '� 355');         
  cst_RowHeader2 : Array[1..3] Of String = ('� 400', '� 450', '� 500');
  cst_RowHeader3 : Array[1..3] Of String = ('� 560', '� 630', '� 710');
Var
  m_Row : Integer;
Begin
  Radio_Adhesif.Enabled := Etude.Batiment.MatiereAccessoires = c_AccessoiresGalva;
  If Radio_Adhesif.Enabled Then Radio_Adhesif.ItemIndex := TVentil_Caisson(CaissonReseau).ChoixAdhesif;
  Edit_VIS13.IntValue := TVentil_Caisson(TVentil_Caisson(CaissonReseau)).Nb_Vis13;
  Edit_VIS19.IntValue := TVentil_Caisson(TVentil_Caisson(CaissonReseau)).Nb_Vis19;
  { Isolant pour travers�e de mur }
  Check_IsolantMur.Checked := TVentil_Caisson(TVentil_Caisson(CaissonReseau)).IsolantTraversee;
  { Fourreaux lisses }
  Check_Fourreaux.Checked := TVentil_Caisson(TVentil_Caisson(CaissonReseau)).AvecFourreaux;
  { Isolants M0 et M1 }
  Edit_M0.IntValue := TVentil_Caisson(TVentil_Caisson(CaissonReseau)).Nb_IsolM0;
  Edit_M1.IntValue := TVentil_Caisson(TVentil_Caisson(CaissonReseau)).Nb_IsolM1;
  { Colliers }
  Radio_Colliers.ItemIndex := TVentil_Caisson(TVentil_Caisson(CaissonReseau)).TypeColliers;
  Radio_ColliersSupp.Enabled := TVentil_Caisson(TVentil_Caisson(CaissonReseau)).TypeColliers <> cst_ColliersSans;
  { Colliers suppl�mentaires }
  For m_Row := 1 To 4 Do Begin
    Grid_ColliersSupp.Ints[1, m_Row] := TVentil_Caisson(CaissonReseau).NbSupportsTerrasse[m_Row];
    Grid_ColliersSupp.Cells[2, m_Row] := cst_RowHeaderColliers1[m_Row];
    Grid_ColliersSupp.Ints[3, m_Row] := TVentil_Caisson(CaissonReseau).NbSupportsTerrasse[m_Row + 4];
    Grid_ColliersSupp.Cells[4, m_Row] := cst_RowHeaderColliers2[m_Row];
//    Grid_ColliersSupp.Ints[5, m_Row] := Etude.Batiment.Reseaux.ColliersSup[m_Row + 8];
    Grid_ColliersSupp.Ints[5, m_Row] := TVentil_Caisson(CaissonReseau).NbSupportsTerrasse[m_Row + 8];
    If m_Row < 3 Then Begin
      Grid_ColliersSupp.Cells[6, m_Row] := cst_RowHeaderColliers3[m_Row];
      Grid_ColliersSupp.Ints[7, m_Row] := TVentil_Caisson(CaissonReseau).NbSupportsTerrasse[m_Row + 12];
    End;
  End;

  Grid_ColliersSupp.MergeCells(6, 3, 2, 2);
  { Bavettes }
  CheckBoxCompteBavettes.Checked := TVentil_Caisson(CaissonReseau).CompteBavettes;
  Grid_Bavettes.Visible := CheckBoxCompteBavettes.Checked;
  For m_Row := 1 To 3 Do Begin
    Grid_Bavettes.Ints[1, m_Row]  := TVentil_Caisson(CaissonReseau).Bavettes[m_Row];
    Grid_Bavettes.Cells[2, m_Row] := cst_RowHeader1[m_Row];
    Grid_Bavettes.Ints[3, m_Row]  := TVentil_Caisson(CaissonReseau).Bavettes[m_Row + 3];
    Grid_Bavettes.Cells[4, m_Row] := cst_RowHeader2[m_Row];
    Grid_Bavettes.Ints[5, m_Row]  := TVentil_Caisson(CaissonReseau).Bavettes[m_Row + 6];
    Grid_Bavettes.Cells[6, m_Row] := cst_RowHeader3[m_Row];
    Grid_Bavettes.Ints[7, m_Row]  := TVentil_Caisson(CaissonReseau).Bavettes[m_Row + 9];
  End;
  { Trappes }
  For m_Row := 1 To 4 Do Begin
    Grid_TrappesVisite.Ints[1, m_Row] := TVentil_Caisson(CaissonReseau).Trappes[m_Row];
    Grid_TrappesVisite.Cells[2, m_Row] := cst_RowHeaderTrappes1[m_Row];
    Grid_TrappesVisite.Ints[3, m_Row] := TVentil_Caisson(CaissonReseau).Trappes[m_Row + 4];
    Grid_TrappesVisite.Cells[4, m_Row] := cst_RowHeaderTrappes2[m_Row];
//    Grid_TrappesVisite.Ints[5, m_Row] := Etude.Batiment.Reseaux.Trappes[m_Row + 8];
    Grid_TrappesVisite.Ints[5, m_Row] := TVentil_Caisson(CaissonReseau).Trappes[m_Row + 8];
    If m_Row < 3 Then Begin
      Grid_TrappesVisite.Cells[6, m_Row] := cst_RowHeaderTrappes3[m_Row];
      Grid_TrappesVisite.Ints[7, m_Row] := TVentil_Caisson(CaissonReseau).Trappes[m_Row + 12];
    End;
  End;

  Grid_ColliersSupp.MergeCells(6, 3, 2, 2);
  { Supports terrasse }
  Edit_NbSuppUni.IntValue    := IntSumTab(TVentil_Caisson(CaissonReseau).NbSupportsTerrasse);

  Edit_NbSupp125100.Visible := False;
  Edit_NbSupp160100.Visible := False;
  Edit_NbSupp125300.Visible := False;
  Edit_NbSupp160300.Visible := False;

  Edit_NbSupp125100.IntValue := 0;
  Edit_NbSupp160100.IntValue := 0;
  Edit_NbSupp125300.IntValue := 0;
  Edit_NbSupp160300.IntValue := 0;

  CheckBox_125100.Visible := False;
  CheckBox_160100.Visible := False;
  CheckBox_125300.Visible := False;
  CheckBox_160300.Visible := False;

  CheckBox_125100.Checked := False;
  CheckBox_160100.Checked := False;
  CheckBox_125300.Checked := False;
  CheckBox_160300.Checked := False;

  CheckBox_BandePerf.Enabled := true;
  
  { Bande perfor�e }
  Text_LongueurBandePerf.Caption      := '(' + Float2Str(TVentil_Caisson(CaissonReseau).LgBandePerforee, 2) + ' m)';
  Edit_NbRlxBandePerforee.IntValue    := TVentil_Caisson(CaissonReseau).NbRlxBandePerCalc;
  CheckBox_ColliersSuppIsoles.Checked := TVentil_Caisson(CaissonReseau).ColliersSupIsoles;


  CheckBoxACT_PST.Enabled := CheckBoxACT_CollierUniverselTerrasse.Checked;
End;
{ **************************************************************************************************************************************************** }
Procedure TDlg_SelectionCaisson.GetChoix_Chiffrage;
Var
  m_NoDiam : Integer;
  m_Row    : Integer;
Begin
  { Adh�sif }
  If Not Radio_Adhesif.Enabled Then TVentil_Caisson(CaissonReseau).ChoixAdhesif := cst_AdhesifSans
                               Else TVentil_Caisson(CaissonReseau).ChoixAdhesif := Radio_Adhesif.ItemIndex;
  { VIS }
  TVentil_Caisson(TVentil_Caisson(CaissonReseau)).Nb_Vis13 := Edit_VIS13.IntValue;
  TVentil_Caisson(TVentil_Caisson(CaissonReseau)).Nb_Vis19 := Edit_VIS19.IntValue;
  { Isolant pour travers�e de mur }
  TVentil_Caisson(TVentil_Caisson(CaissonReseau)).IsolantTraversee := Check_IsolantMur.Checked;
  { Fourreaux lisses }
  TVentil_Caisson(TVentil_Caisson(CaissonReseau)).AvecFourreaux := Check_Fourreaux.Checked;
  { Isolants M0 et M1 }
  TVentil_Caisson(TVentil_Caisson(CaissonReseau)).Nb_IsolM0 := Edit_M0.IntValue;
  TVentil_Caisson(TVentil_Caisson(CaissonReseau)).Nb_IsolM1 := Edit_M1.IntValue;
  { Colliers }
  TVentil_Caisson(TVentil_Caisson(CaissonReseau)).TypeColliers := Radio_Colliers.ItemIndex;
  { Colliers suppl�mentaires }
  If CheckBox_Universel.Checked Then For m_Row := 1 To 4 Do Begin
    TVentil_Caisson(TVentil_Caisson(CaissonReseau)).ColliersSupTerrasse[m_Row] := Grid_ColliersSupp.Ints[1, m_Row];
    TVentil_Caisson(TVentil_Caisson(CaissonReseau)).ColliersSupTerrasse[m_Row + 4]  := Grid_ColliersSupp.Ints[3, m_Row];
    TVentil_Caisson(TVentil_Caisson(CaissonReseau)).ColliersSupTerrasse[m_Row + 8]  := Grid_ColliersSupp.Ints[5, m_Row];
    If m_Row < 3 Then TVentil_Caisson(TVentil_Caisson(CaissonReseau)).ColliersSupTerrasse[m_Row + 12] := Grid_ColliersSupp.Ints[7, m_Row];
  End Else
    begin
    For m_NoDiam := Low(TVentil_Caisson(TVentil_Caisson(CaissonReseau)).ColliersSupTerrasse) To High(TVentil_Caisson(TVentil_Caisson(CaissonReseau)).ColliersSupTerrasse) Do TVentil_Caisson(TVentil_Caisson(CaissonReseau)).ColliersSupTerrasse[m_NoDiam] := 0;
    For m_NoDiam := Low(TVentil_Caisson(TVentil_Caisson(CaissonReseau)).ColliersSupColonne) To High(TVentil_Caisson(TVentil_Caisson(CaissonReseau)).ColliersSupColonne) Do TVentil_Caisson(TVentil_Caisson(CaissonReseau)).ColliersSupColonne[m_NoDiam] := 0;
    end;
  { Bavettes }
  TVentil_Caisson(TVentil_Caisson(CaissonReseau)).CompteBavettes := CheckBoxCompteBavettes.Checked;
  For m_Row := 1 To 3 Do Begin
    TVentil_Caisson(TVentil_Caisson(CaissonReseau)).Bavettes[m_Row] := Grid_Bavettes.Ints[1, m_Row];
    TVentil_Caisson(TVentil_Caisson(CaissonReseau)).Bavettes[m_Row + 3] := Grid_Bavettes.Ints[3, m_Row];
    TVentil_Caisson(TVentil_Caisson(CaissonReseau)).Bavettes[m_Row + 6] := Grid_Bavettes.Ints[5, m_Row];
    TVentil_Caisson(TVentil_Caisson(CaissonReseau)).Bavettes[m_Row + 9] := Grid_Bavettes.Ints[7, m_Row];
  End;
  { Trappes }
  if Not(Etude.Batiment.IsFabricantACT)  then
  For m_Row := 1 To 4 Do Begin
    TVentil_Caisson(TVentil_Caisson(CaissonReseau)).Trappes[m_Row] := Grid_TrappesVisite.Ints[1, m_Row];
    TVentil_Caisson(TVentil_Caisson(CaissonReseau)).Trappes[m_Row + 4]  := Grid_TrappesVisite.Ints[3, m_Row];
    TVentil_Caisson(TVentil_Caisson(CaissonReseau)).Trappes[m_Row + 8]  := Grid_TrappesVisite.Ints[5, m_Row];
    If m_Row < 3 Then TVentil_Caisson(TVentil_Caisson(CaissonReseau)).Trappes[m_Row + 12] := Grid_TrappesVisite.Ints[7, m_Row];
  End;
  { Supports terrasse }
  If CheckBox_Universel.Checked Then TVentil_Caisson(TVentil_Caisson(CaissonReseau)).NbSupportsUni := Edit_NbSuppUni.IntValue
                                Else TVentil_Caisson(TVentil_Caisson(CaissonReseau)).NbSupportsUni := 0;
  If CheckBox_125100.Checked Then TVentil_Caisson(TVentil_Caisson(CaissonReseau)).NbSupports125100 := Edit_NbSupp125100.IntValue
                             Else TVentil_Caisson(TVentil_Caisson(CaissonReseau)).NbSupports125100 := 0;
  If CheckBox_160100.Checked Then TVentil_Caisson(TVentil_Caisson(CaissonReseau)).NbSupports160100 := Edit_NbSupp160100.IntValue
                             Else TVentil_Caisson(TVentil_Caisson(CaissonReseau)).NbSupports160100 := 0;
  If CheckBox_125300.Checked Then TVentil_Caisson(TVentil_Caisson(CaissonReseau)).NbSupports125300 := Edit_NbSupp125300.IntValue
                             Else TVentil_Caisson(TVentil_Caisson(CaissonReseau)).NbSupports125300 := 0;
  If CheckBox_160300.Checked Then TVentil_Caisson(TVentil_Caisson(CaissonReseau)).NbSupports160300 := Edit_NbSupp160300.IntValue
                             Else TVentil_Caisson(TVentil_Caisson(CaissonReseau)).NbSupports160300 := 0;
  { Bande perfor�e }
  If CheckBox_BandePerf.Checked Then TVentil_Caisson(TVentil_Caisson(CaissonReseau)).NbRlxBdPerf := Edit_NbRlxBandePerforee.IntValue
                                Else TVentil_Caisson(TVentil_Caisson(CaissonReseau)).NbRlxBdPerf := 0;
  TVentil_Caisson(TVentil_Caisson(CaissonReseau)).ColliersSupIsoles := CheckBox_ColliersSuppIsoles.Checked;


  { Acthys}
    TVentil_Caisson(CaissonReseau).NbSuspensionsRapides                := 0;
    TVentil_Caisson(CaissonReseau).ACT_PST                             := CheckBoxACT_PST.Checked;
    TVentil_Caisson(CaissonReseau).ACT_CSR                             := CheckBoxACT_CablesSuspensionRapide.Checked;
    TVentil_Caisson(CaissonReseau).ACT_CSR_Nb                          := AdvEditACT_CablesSuspensionRapide.IntValue;
    TVentil_Caisson(CaissonReseau).ACT_CUT                             := CheckBoxACT_CollierUniverselTerrasse.Checked;
    TVentil_Caisson(CaissonReseau).ACT_CUT_Nb                          := AdvEditACT_CollierUniverselTerrasse.IntValue;
    TVentil_Caisson(CaissonReseau).ACT_Trappes                         := CheckBoxACT_TrappesDeVisite.Checked;
    TVentil_Caisson(CaissonReseau).ACT_ROA                             := CheckBoxACT_BandeAdhesive.Checked;
    TVentil_Caisson(CaissonReseau).ACT_TeSouche                        := RadioGroupACT_TeSouche.ItemIndex;
    TVentil_Caisson(CaissonReseau).ACT_FT                              := CheckBoxACT_FourreauDeTraversee.Checked;
    TVentil_Caisson(CaissonReseau).ACT_JTD                             := CheckBoxACT_Joint.Checked;
    TVentil_Caisson(CaissonReseau).ACT_CUC                             := CheckBoxACT_CollierUniverselColonne.Checked;
    TVentil_Caisson(CaissonReseau).ACT_CUC_Nb                          := AdvEditACT_CollierUniverselColonne.IntValue;
    TVentil_Caisson(CaissonReseau).ACT_Terminaux                       := RadioGroupACT_Terminaux.ItemIndex;
    TVentil_Caisson(CaissonReseau).ACT_LgT2A                           := RadioGroupACT_LongueurBarreT2A.ItemIndex;
End;
{ **************************************************************************************************************************************************** }
Function SelectionDuCaisson(Const _Caisson: TVentil_Troncon; _CaissonEdibatec: TEdibatec_Caisson): TEdibatec_Caisson;
Var
  m_No: Integer;
Begin

  if TVentil_Caisson(_Caisson).FixePlagePression = c_Oui then
    begin
    BBS_ShowMessage(_Caisson.Reference + ' ne peut pas �tre s�lectionner car une plage de pression fixe lui a �t� assign�e', mtError, [mbOK], 0);
    TVentil_Caisson(_Caisson).ProduitAssocie := Nil;
    Result := nil;
    Exit;
    end;


InterfaceCAD.Aero_Vilo2Fredo_HighlightObjects(nil);

        if Etude <> nil then
                Etude.AutoSaveAutorisee := False;
  Etude.Batiment.Reseaux.CalculerQuantitatif(Etude.Batiment);
  Dlg_SelectionCaisson := TDlg_SelectionCaisson.Create(Application);
  Dlg_SelectionCaisson.CaissonSelection := _CaissonEdibatec;
  Dlg_SelectionCaisson.CaissonReseau    := TVentil_Troncon(_Caisson);
    if not(Dlg_SelectionCaisson.InitFiche) then
      begin
      Result := Nil;
        if Etude <> nil then
          Etude.AutoSaveAutorisee := True;
      FreeAndNil(Dlg_SelectionCaisson);
      Exit;
      end;
  TVentil_Caisson(_Caisson).SaveOptions_SelectionCaisson.InitOptionsFiche(Dlg_SelectionCaisson);
  If Dlg_SelectionCaisson.ShowModal = idOk Then Begin
    Result := Dlg_SelectionCaisson.CaissonSelection.Duplicate;
    {  Association du produit caisson et ses accessoires}
    TVentil_Caisson(_Caisson).ProduitAssocie := Result;
//    TVentil_Caisson(_Caisson).Associations.InitFrom(TVentil_Caisson(_Caisson).ProduitAssocie.Associations);
//    Dlg_SelectionCaisson.Grid_Accessoires.SortByColumn(0);

    For m_No := 0 To _Caisson.FilsCalcul.Count - 1 do
        if _Caisson.FilsCalcul[m_No].InheritsFrom(TVentil_SortieToit) then
                TVentil_SortieToit(_Caisson.FilsCalcul[m_No]).MiseAJourDiametre;

      For m_No := 0 To _Caisson.Fils.Count - 1 do
         Begin
//            if TVentil_TronconDessin_SortieCaisson(_Caisson.Fils[m_No]).Fils.Count > 0 then
              begin
                if TVentil_TronconDessin_SortieCaisson(_Caisson.Fils[m_No]).TypeSortie = c_CaissonSortie_Refoulement then
                        Begin
                          if Result.DiametreRejet > 0 then
                            TVentil_TronconDessin_SortieCaisson(_Caisson.Fils[m_No]).Diametre := Result.DiametreRejet;
                        End
                else
                        Begin
                          if Result.DiametreAspiration > 0 then
                            begin
                            TVentil_TronconDessin_SortieCaisson(_Caisson.Fils[m_No]).Diametre := Result.DiametreAspiration;
                              if Etude.Batiment.IsFabricantVIM and _Caisson.InheritsFrom(TVentil_TourelleTete) and (_Caisson.GetFirstChildrenClass(TVentil_Colonne) <> Nil) then
                                _Caisson.GetFirstChildrenClass(TVentil_Colonne).Diametre := Result.DiametreAspiration;
                            end;
                        End;
              TVentil_TronconDessin_SortieCaisson(_Caisson.Fils[m_No]).LienCad.IAero_Vilo_Base_SetDiametre(TVentil_TronconDessin_SortieCaisson(_Caisson.Fils[m_No]).Diametre);
                if Etude.Batiment.IsFabricantVIM and _Caisson.InheritsFrom(TVentil_TourelleTete) and (_Caisson.GetFirstChildrenClass(TVentil_Colonne) <> Nil) then
                  _Caisson.GetFirstChildrenClass(TVentil_Colonne).LienCad.IAero_Vilo_Base_SetDiametre(TVentil_TronconDessin_SortieCaisson(_Caisson.Fils[m_No]).Diametre);
              end;
//        Aero_Vilo2Fredo_UpdateChangementsDeDiametre(Nil);
        End;


    if Etude.Batiment.IsFabricantACT
    or Etude.Batiment.IsFabricantMVN
    then
      begin
        TVentil_Caisson(_Caisson).SaisieDimensions := C_oui;
        TVentil_Caisson(_Caisson).Largeur := Result.Largeur;
        TVentil_Caisson(_Caisson).Longueur := Result.Longueur;
        TVentil_Caisson(_Caisson).Hauteur := Result.Hauteur;
      end;

//    _Caisson.Associations.Clear;
    For m_No := 1 To Dlg_SelectionCaisson.Grid_Accessoires.RowCount - 1 Do If Dlg_SelectionCaisson.Grid_Accessoires.Objects[0, m_No] <> nil Then
        Begin
        TEdibatec_Association(Dlg_SelectionCaisson.Grid_Accessoires.Objects[0, m_No]).Qte := Dlg_SelectionCaisson.Grid_Accessoires.Ints[2, m_No];
        TEdibatec_Association(Dlg_SelectionCaisson.CaissonSelection.Associations.Produit(m_No - 1)).Qte := TEdibatec_Association(Dlg_SelectionCaisson.Grid_Accessoires.Objects[0, m_No]).Qte;
//        if TEdibatec_Association(Dlg_SelectionCaisson.Grid_Accessoires.Objects[0, m_No]).Qte > 0 then
//        _Caisson.Associations.AddObject(TEdibatec_Association(Dlg_SelectionCaisson.Grid_Accessoires.Objects[0, m_No]).CodeProduit, TEdibatec_Association(Dlg_SelectionCaisson.Grid_Accessoires.Objects[0, m_No]));
        End;
    _Caisson.Associations.InitFrom(Dlg_SelectionCaisson.CaissonSelection.Associations);
    Dlg_SelectionCaisson.GetChoix_Chiffrage;
    TVentil_Caisson(_Caisson).SaveOptions_SelectionCaisson.SaveOptionsFiche(Dlg_SelectionCaisson);
    
//modif dtu
                Etude.DeltaCalculeMax := abs(TVentil_Caisson(_Caisson).DPVentilateur[ttc20deg].MinAQmax - TVentil_Caisson(_Caisson).Get_PerteChargeEAQMinDT2014 - TVentil_Caisson(_Caisson).ProduitAssocie.DpCalcQMax);
                Etude.DeltaCalculeMin := abs(TVentil_Caisson(_Caisson).DPVentilateur[ttc20deg].MinAQmin - TVentil_Caisson(_Caisson).Get_PerteChargeEAQMinDT2014 - TVentil_Caisson(_Caisson).ProduitAssocie.DpCalcQMin);
{
        if not Dlg_SelectionCaisson.CheckBox_DTU.Checked then
                Begin
                Etude.DeltaCalculeMax := abs(_Caisson.DPVentilateur.MinAQmax - 20 - _Caisson.ProduitAssocie.DpCalcQMax);
                Etude.DeltaCalculeMin := abs(_Caisson.DPVentilateur.MinAQmin - 20 - _Caisson.ProduitAssocie.DpCalcQMin);
                End
        Else
                Begin
                Etude.DeltaCalculeMax := abs(_Caisson.DPVentilateur.MinAQmax - _Caisson.ProduitAssocie.DpCalcQMax);
                Etude.DeltaCalculeMin := abs(_Caisson.DPVentilateur.MinAQmin - _Caisson.ProduitAssocie.DpCalcQMin);
                End;
}
    if Etude.SelectedObject <> nil then
        Etude.SelectedObject.AfficheResultats(Etude.GridRes);
  End Else Begin
    TVentil_Caisson(_Caisson).ProduitAssocie := Nil;
    Result := nil;
  End;
  Dlg_SelectionCaisson.GetChoix;
  FreeAndNil(Dlg_SelectionCaisson);
        if Etude <> nil then
                Begin
                Etude.Calculer;
                Etude.AutoSaveAutorisee := True;
                End;
End;
{ **************************************************************************************************************************************************** }

{ ************************************************************************ TDlg_SelectionCaisson ****************************************************** }
Procedure TDlg_SelectionCaisson.EnableControls;
Begin
 if Etude.Batiment.IsFabricantVIM or Etude.Batiment.IsFabricantMVN then
  begin

    Btn_Precedent.Enabled := Panel_Actif.Tag > Cst_PanelCriteres;
    Btn_Suivant.Enabled   := Panel_Actif.Tag < Cst_PanelAnnexe;
    Btn_Ok.Enabled        := Panel_Actif = Panel_AccessoiresAnnexes;

    if (Panel_Actif = Panel_Selection) and not(CanContinueAfterSelection) then
          Btn_Suivant.Enabled := False;
    end
  else
  begin
(*
    Btn_Precedent.visible := false;
    Btn_Suivant.visible := false;
    Btn_Ok.Enabled        := (Panel_Actif = Panel_Selection) and Grid_Selection.Visible and (Grid_Selection.Row > 0);
*)

    Btn_Precedent.Enabled := Panel_Actif.Tag > Cst_PanelCaisson;
    Btn_Suivant.Enabled   := Panel_Actif.Tag < Cst_PanelAnnexe;
    Btn_Ok.Enabled        := Panel_Actif = Panel_AccessoiresAnnexes;

    if (Panel_Actif = Panel_Selection) and not(CanContinueAfterSelection) then
          Btn_Suivant.Enabled := False;

    end;

End;
{ **************************************************************************************************************************************************** }
Function TDlg_SelectionCaisson.InitFiche : Boolean;
Var
  m_NoComp : Integer;
{$IFDEF  VIM_SELECTIONCAISSON}
  m_OptairRTConso : TExportImportOptairRTConso;
{$ENDIF}
Begin
  Result := True;
  Enable_NonAgree;
  InitChoix;
  Height   := 395;
  Width    := Panel_Preselection.Width + 5;
  Position := poScreenCenter;
  For m_NoComp := 0 To ComponentCount - 1 Do If Components[m_NoComp].InheritsFrom(TPanel) Then Begin
    If (TPanel(Components[m_NoComp]).Tag In [Cst_PanelCriteres..Cst_PanelAnnexe]) And (TPanel(Components[m_NoComp]) <> FPanel_Actif) Then
      TPanel(Components[m_NoComp]).Left := 0;
      TPanel(Components[m_NoComp]).Top  := 0;
  End;

  Btn_RecalculAccesoires.Visible := False;
  if Etude.Batiment.IsFabricantVIM or Etude.Batiment.IsFabricantMVN then
    begin
    {$IFDEF  VIM_SELECTIONCAISSON}
      if not(TestValiditeReseau) then
        begin
        Result := False;
        Exit;
        end;
    m_OptairRTConso := TExportImportOptairRTConso.Create;
      if m_OptairRTConso.Launch(CaissonReseau, Self) then
        begin
        FreeAndNil(m_OptairRTConso);
        GoToAccessoiresAnnexes;
        Btn_Suivant.Visible := False;
        Btn_Precedent.Visible := False;
        Btn_Cancel.Visible := False;
        Btn_Ok.Left := Round((PanelBas.Width - Btn_Ok.Width) / 2);
        end
      else
        begin
        FreeAndNil(m_OptairRTConso);
        Result := False;
        Exit;
        end;
    {$Else}
    Panel_Actif := Panel_Preselection;
    Btn_Suivant.Enabled := True;
    {$ENDIF}
    end
  else
    begin
    Panel_Actif := Panel_Selection;
    //pr�initialiser tous les boutons du panel preselection
    GoToSelection;
    end;
  AutoSize := True;
  EnableControls;
  InitChoix_Chiffrage;


  TabSheetMontage.TabVisible               := Not(Etude.Batiment.IsFabricantACT);
  TabSheetBavettEtColliers.TabVisible      := Not(Etude.Batiment.IsFabricantACT);
  TabSheetSupportsTelescopiques.TabVisible := Not(Etude.Batiment.IsFabricantACT);
  TabSheetTrappesDeVisite.TabVisible       := Not(Etude.Batiment.IsFabricantACT);
  TabSheetActhysTerrasse.TabVisible        := Etude.Batiment.IsFabricantACT;
  TabSheetActhysColonnes.TabVisible        := Etude.Batiment.IsFabricantACT;
  TabSheetActhysTrainasse.TabVisible       := Etude.Batiment.IsFabricantACT;

    if Etude.Batiment.IsFabricantACT then
      PageControl1.ActivePage := TabSheetActhysTerrasse
    else
      PageControl1.ActivePage := TabSheetMontage;


  Caption := 'Options de chiffrage - ' + CaissonReseau.Reference;
End;
{ **************************************************************************************************************************************************** }
procedure TDlg_SelectionCaisson.Image_DPVentClick(Sender: TObject);
Var
        MsgHelpBox : TAdvSmoothMessageDialog;
        cpt : integer;
Begin


MsgHelpBox := TAdvSmoothMessageDialog.Create(Nil);

MsgHelpBox.EnableKeyEvents := False;
MsgHelpBox.Buttons.Add;
MsgHelpBox.Buttons[0].Spacing := 0;
MsgHelpBox.Buttons[0].Caption := 'Ok';
MsgHelpBox.Buttons[0].Color := clBtnFace;
MsgHelpBox.Buttons[0].ColorDown := clBtnFace;
MsgHelpBox.Buttons[0].ColorFocused := clBtnFace;
MsgHelpBox.Buttons[0].HoverColor := 16120314;
MsgHelpBox.Caption := 'Aide';
MsgHelpBox.CaptionLocation := hlTopCenter;
MsgHelpBox.CaptionHeight := 19;
MsgHelpBox.Position := poMainFormCenter;
MsgHelpBox.SetComponentStyle(tsWhidbey);
MsgHelpBox.Fill.Opacity := 255;
MsgHelpBox.Fill.OpacityTo := 255;
MsgHelpBox.Fill.OpacityMirror := 255;
MsgHelpBox.Fill.OpacityMirrorTo := 255;

MsgHelpBox.HTMLText.Text := 'Pour s''affranchir de la Dp addionnelle li�e � l''effet du vent, il faut que ' +
                            'la position du rejet soit verticale et surmont�e par une sortie de toiture de type "chapeau chinois"';

MsgHelpBox.Execute;
MsgHelpBox.Destroy;
end;
{ **************************************************************************************************************************************************** }
Procedure TDlg_SelectionCaisson.InitChoix;
Begin
  { Type de ventilateur }
  CheckBox_Caisson.Checked   := Str2Int(TVentil_Caisson(TVentil_Caisson(CaissonReseau)).Chiff_TYPE_VENTILATEUR[0]) = c_OUI;
  CheckBox_Tourelle.Checked  := Str2Int(TVentil_Caisson(TVentil_Caisson(CaissonReseau)).Chiff_TYPE_VENTILATEUR[1]) = c_OUI;
  { Entrainement }
  CheckBox_Direct.Checked := Str2Int(TVentil_Caisson(TVentil_Caisson(CaissonReseau)).Chiff_ENTRAINEMENT[0]) = c_OUI;
  CheckBox_Poulie.Checked := Str2Int(TVentil_Caisson(TVentil_Caisson(CaissonReseau)).Chiff_ENTRAINEMENT[1]) = c_OUI;
  { Vitesses }
  CheckBox_UneVitesse.Checked  := Str2Int(TVentil_Caisson(TVentil_Caisson(CaissonReseau)).Chiff_VITESSE[0]) = c_OUI;
  CheckBoxDeuxVitesses.Checked := Str2Int(TVentil_Caisson(TVentil_Caisson(CaissonReseau)).Chiff_VITESSE[1]) = c_OUI;

  { Agr�ment }
  CheckBox_NonAgree.Enabled := NonAgreePossible;
  CheckBox_NonAgree.Checked := CheckBox_NonAgree.Enabled And (Str2Int(TVentil_Caisson(TVentil_Caisson(CaissonReseau)).Chiff_AGREMENT[0]) = c_OUI);

  CheckBox_40012.Checked    := Str2Int(TVentil_Caisson(TVentil_Caisson(CaissonReseau)).Chiff_AGREMENT[1]) = c_OUI;

        if (Etude.Batiment.TypeBat = c_BatimentTertiaire) then
                if ( Etude.Batiment.TypeTertiaire = c_TertiaireVMC)  then
                        Begin
                                if NonAgreePossible then
                                        Begin
                                        CheckBox_NonAgree.Checked := true;
                                        CheckBox_NonAgree.Enabled := true;
                                        CheckBox_40012.Checked    := false;
                                        CheckBox_40012.Enabled    := true;
                                        End
                                else
                                        Begin
                                        CheckBox_NonAgree.Checked := false;
                                        CheckBox_NonAgree.Enabled := False;
                                        CheckBox_40012.Checked    := true;
                                        CheckBox_40012.Enabled    := False;
                                        End;

                        End
                Else
                        Begin
                        CheckBox_NonAgree.Checked := true;
                        CheckBox_NonAgree.Enabled := true;
                        CheckBox_40012.Checked    := true;
                        CheckBox_40012.Enabled    := true;
                        End;


        if Etude.Batiment.TypeBat = c_BatimentTertiaire then
                CheckBox_DTU.Checked  := false;
  CheckBox_DTU.Enabled  := not(Etude.Batiment.TypeBat = c_BatimentTertiaire);


  { Alimentation }
  CheckBox_Monophase.Checked := Str2Int(TVentil_Caisson(TVentil_Caisson(CaissonReseau)).Chiff_ALIMENTATION[0]) = c_OUI;
  CheckBox_Triphase.Checked  := Str2Int(TVentil_Caisson(TVentil_Caisson(CaissonReseau)).Chiff_ALIMENTATION[1]) = c_OUI;
  { D�pressostat }
  If (Etude.Batiment.TypeBat in [c_BatimentCollectif, c_BatimentHotel]) and (Etude.Has_VMCGaz) Then Begin
//    TVentil_Caisson(TVentil_Caisson(CaissonReseau)).Chiff_DEPRESSOSTAT := c_Sans;
    TVentil_Caisson(TVentil_Caisson(CaissonReseau)).Chiff_NECOADEPR    := c_Sans;
    TVentil_Caisson(TVentil_Caisson(CaissonReseau)).Chiff_NECOSDEPR    := c_Sans;
    TVentil_Caisson(TVentil_Caisson(CaissonReseau)).Chiff_VENTILECO    := c_Sans;

    CheckBoxNEcoSDepr.Checked := False;
    CheckBoxNEcoSDepr.Visible := False;
//    RadioGroupVentilEcoEtDepressostat.Items.Clear;
//    RadioGroupVentilEcoEtDepressostat.Items.Add('Ventil. Eco');
//    RadioGroupVentilEcoEtDepressostat.Items.Add('Ventil. non Eco avec Depr. + inter. mont�s');

//    RadioGroupDepressostat.Enabled := False;
//    RadioGroupVentilEco.Enabled := False;
//    RadioGroupDepressostat.ItemIndex := TVentil_Caisson(TVentil_Caisson(CaissonReseau)).Chiff_DEPRESSOSTAT;
//    RadioGroupVentilEco.ItemIndex := TVentil_Caisson(TVentil_Caisson(CaissonReseau)).Chiff_VENTILECO;
  End Else Begin
    CheckBoxNEcoSDepr.Visible := True;  
//    RadioGroupVentilEcoEtDepressostat.Items.Clear;
//    RadioGroupVentilEcoEtDepressostat.Items.Add('Ventil. Eco');
//    RadioGroupVentilEcoEtDepressostat.Items.Add('Ventil. non Eco avec Depr. + inter. mont�s');
//    RadioGroupVentilEcoEtDepressostat.Items.Add('Ventil. non Eco sans Depr. + inter. mont�s');

//    RadioGroupDepressostat.Enabled := True;
//    RadioGroupVentilEco.Enabled := True;
//    RadioGroupDepressostat.ItemIndex := TVentil_Caisson(TVentil_Caisson(CaissonReseau)).Chiff_DEPRESSOSTAT;
//    RadioGroupVentilEco.ItemIndex := TVentil_Caisson(TVentil_Caisson(CaissonReseau)).Chiff_VENTILECO;
  End;
End;
{ **************************************************************************************************************************************************** }
Procedure TDlg_SelectionCaisson.GetChoix;
Begin
  { Type de ventilateur }
  TVentil_Caisson(TVentil_Caisson(CaissonReseau)).Chiff_TYPE_VENTILATEUR[0] := IntToStr(Bool2OUINON(CheckBox_Caisson.Checked));
  TVentil_Caisson(TVentil_Caisson(CaissonReseau)).Chiff_TYPE_VENTILATEUR[1] := IntToStr(Bool2OUINON(CheckBox_Tourelle.Checked));
  { Entrainement }
  TVentil_Caisson(TVentil_Caisson(CaissonReseau)).Chiff_ENTRAINEMENT[0] := IntToStr(Bool2OUINON(CheckBox_Direct.Checked));
  TVentil_Caisson(TVentil_Caisson(CaissonReseau)).Chiff_ENTRAINEMENT[1] := IntToStr(Bool2OUINON(CheckBox_Poulie.Checked));
  { Vitesses }
  TVentil_Caisson(TVentil_Caisson(CaissonReseau)).Chiff_VITESSE[0] := IntToStr(Bool2OUINON(CheckBox_UneVitesse.Checked));
  TVentil_Caisson(TVentil_Caisson(CaissonReseau)).Chiff_VITESSE[1] := IntToStr(Bool2OUINON(CheckBoxDeuxVitesses.Checked));
  { Agr�ment }
  TVentil_Caisson(TVentil_Caisson(CaissonReseau)).Chiff_AGREMENT[0] := IntToStr(Bool2OUINON(CheckBox_NonAgree.Checked));
  TVentil_Caisson(TVentil_Caisson(CaissonReseau)).Chiff_AGREMENT[1] := IntToStr(Bool2OUINON(CheckBox_40012.Checked));
  { Alimentation }
  TVentil_Caisson(TVentil_Caisson(CaissonReseau)).Chiff_ALIMENTATION[0] := IntToStr(Bool2OUINON(CheckBox_Monophase.Checked));
  TVentil_Caisson(TVentil_Caisson(CaissonReseau)).Chiff_ALIMENTATION[1]:= IntToStr(Bool2OUINON(CheckBox_Triphase.Checked));
  { D�pressostat }
//  TVentil_Caisson(TVentil_Caisson(CaissonReseau)).Chiff_DEPRESSOSTAT := RadioGroupDepressostat.ItemIndex;
//  TVentil_Caisson(TVentil_Caisson(CaissonReseau)).Chiff_VENTILECO := RadioGroupVentilEco.ItemIndex;

        if CheckBoxEco.Checked then
                TVentil_Caisson(TVentil_Caisson(CaissonReseau)).Chiff_VENTILECO := C_Avec
        else
                TVentil_Caisson(TVentil_Caisson(CaissonReseau)).Chiff_VENTILECO := C_Sans;
        if CheckBoxNEcoADepr.Checked then
                TVentil_Caisson(TVentil_Caisson(CaissonReseau)).Chiff_NECOADEPR := C_Avec
        else
                TVentil_Caisson(TVentil_Caisson(CaissonReseau)).Chiff_NECOADEPR := C_Sans;
        if CheckBoxNEcoSDepr.Checked then
                TVentil_Caisson(TVentil_Caisson(CaissonReseau)).Chiff_NECOSDEPR := C_Avec
        else
                TVentil_Caisson(TVentil_Caisson(CaissonReseau)).Chiff_NECOSDEPR := C_Sans;
{
        if CheckBoxEco.Checked and (CheckBoxNEcoADepr.Checked or CheckBoxNEcoSDepr.Checked) then
                TVentil_Caisson(TVentil_Caisson(CaissonReseau)).Chiff_VENTILECO := C_Tous
        else
                Begin
                        if CheckBoxEco.Checked then
                                TVentil_Caisson(TVentil_Caisson(CaissonReseau)).Chiff_VENTILECO := C_Avec
                        else
                                TVentil_Caisson(TVentil_Caisson(CaissonReseau)).Chiff_VENTILECO := C_Sans;
                End;

        if (CheckBoxEco.Checked or CheckBoxNEcoADepr.Checked) and CheckBoxNEcoSDepr.Checked then
                TVentil_Caisson(TVentil_Caisson(CaissonReseau)).Chiff_DEPRESSOSTAT := c_Tous
        else
                Begin
                        if CheckBoxNEcoSDepr.Checked then
                                TVentil_Caisson(TVentil_Caisson(CaissonReseau)).Chiff_DEPRESSOSTAT := c_Sans
                        else
                                TVentil_Caisson(TVentil_Caisson(CaissonReseau)).Chiff_DEPRESSOSTAT := C_Avec;
                End;
}

//          if RadioGroupVentilEcoEtDepressostat.ItemIndex in [0, 1] then
//                TVentil_Caisson(TVentil_Caisson(CaissonReseau)).Chiff_DEPRESSOSTAT := c_Avec
//          else
//                TVentil_Caisson(TVentil_Caisson(CaissonReseau)).Chiff_DEPRESSOSTAT := c_Sans;
//          if RadioGroupVentilEcoEtDepressostat.ItemIndex = 0 then
//                TVentil_Caisson(TVentil_Caisson(CaissonReseau)).Chiff_VENTILECO := c_Avec
//          else
//                TVentil_Caisson(TVentil_Caisson(CaissonReseau)).Chiff_VENTILECO := c_Sans;
End;
{ **************************************************************************************************************************************************** }
Procedure TDlg_SelectionCaisson.Enable_NonAgree;
Var
  m_No : Integer;
Begin
  NonAgreePossible := True;
  For m_No := 0 To Etude.Batiment.Composants.Count - 1 Do
    If (Etude.Batiment.Composants[m_No].Caisson = TVentil_Caisson(TVentil_Caisson(CaissonReseau))) And (Etude.Batiment.Composants[m_No].InheritsFrom(TVentil_Bouche)) Then
      NonAgreePossible := NonAgreePossible And (TVentil_Bouche(Etude.Batiment.Composants[m_No]).Has_PareFlamme = c_Oui);
End;
{ **************************************************************************************************************************************************** }
Procedure TDlg_SelectionCaisson.Btn_SuivantClick(Sender: TObject);
Begin
  Case Panel_Actif.Tag Of
    Cst_PanelCriteres    : GoToSelection;
    Cst_PanelCaisson     : if Etude.Batiment.IsFabricantACT then
                            GoToAccessoiresAnnexes
                           else
                            GoToAccessoires;
    Cst_PanelAccessoires : GoToAccessoiresAnnexes;
  End;
  EnableControls;
End;
{ **************************************************************************************************************************************************** }
Procedure TDlg_SelectionCaisson.Btn_PrecedentClick(Sender: TObject);
Begin
  Case Panel_Actif.Tag Of
    Cst_PanelCaisson     : GoToPreselection;
    Cst_PanelAccessoires : GoToSelection;
    Cst_PanelAnnexe      : if Etude.Batiment.IsFabricantACT then
                            GoToSelection
                           else
                            GoToAccessoires;
  End;
  EnableControls;
End;
{ **************************************************************************************************************************************************** }
Procedure TDlg_SelectionCaisson.GoToAccessoires;
Var
  m_No      : Integer;
  m_NbAspi  : Integer;
  m_NbRejet : Integer;
Begin
  Btn_RecalculAccesoires.Visible := False;
  Panel_Actif := Panel_Accessoires;
  Label_Ventilateur.Caption := CaissonSelection.Reference;
  m_NbAspi  := 0;
  m_NbRejet := 0;
  For m_No := 0 To TVentil_Caisson(TVentil_Caisson(CaissonReseau)).FilsCalcul.Count - 1 Do
        If TVentil_Caisson(TVentil_Caisson(CaissonReseau)).FilsCalcul[m_No].VersSortieToit Then
                Begin
                m_NbAspi := TVentil_Caisson(TVentil_Caisson(CaissonReseau)).FilsCalcul.Count - 1;
                m_NbRejet:= 1;
                break;
                End
        Else
                m_NbAspi := TVentil_Caisson(TVentil_Caisson(CaissonReseau)).FilsCalcul.Count;

  Grid_InfosCaisson.Ints[1, 1] := m_NbAspi;
  Grid_InfosCaisson.Ints[1, 2] := m_NbRejet;

  Grid_InfosCaisson.Cells[2, 1] := IntToStr(CaissonSelection.DiametreAspiration) + ' mm';
  Grid_InfosCaisson.Cells[2, 2] := IntToStr(CaissonSelection.DiametreRejet) + ' mm';


  AffecteAssociations;
  Grid_Accessoires.SortByColumn(0);

End;
{ **************************************************************************************************************************************************** }
Procedure TDlg_SelectionCaisson.GoToAccessoiresAnnexes;
Begin
  Panel_Actif := Panel_AccessoiresAnnexes;
  Btn_RecalculAccesoires.Visible := True;
End;
{ **************************************************************************************************************************************************** }
Procedure TDlg_SelectionCaisson.GoToPreselection;
Begin
  Panel_Actif := Panel_Preselection;
  CanContinueAfterSelection := True;
End;
{ **************************************************************************************************************************************************** }
function TDlg_SelectionCaisson.TestValiditeReseau : Boolean;
var
        MsgRegistres : String;
        m_NoRegistre : Integer;
        ListTronconsRegistresCad : IInterfaceList;
begin
  Result := False;
    If Etude.Batiment.Reseaux.HasDiametreHorsVitesse then
      BBS_ShowMessage('Certains tron�ons sont en dehors des vitesses limites.', mtError, [mbOK], 0)
    Else
    If (Etude.Batiment.Reseaux.ListeBesoinRegistre <> nil) and (Etude.Batiment.Reseaux.ListeBesoinRegistre.Count > 0) Then
        Begin
        Hide;
        Close;
        ListTronconsRegistresCad := TInterfaceList.Create;
        MsgRegistres := 'Certains tron�ons n�cessitent la pause de registre aupr�s des objets s�lectionn�s :';
                For m_NoRegistre := 0 to Etude.Batiment.Reseaux.ListeBesoinRegistre.Count - 1 Do
                        Begin
                        MsgRegistres := MsgRegistres +#13 + '- ' + Etude.Batiment.Reseaux.ListeBesoinRegistre[m_NoRegistre].Reference;
                                if Etude.Batiment.Reseaux.ListeBesoinRegistre[m_NoRegistre].InheritsFrom(TVentil_TeSouche) then
                                        MsgRegistres := MsgRegistres + ' (en amont)'
                                else
                                        MsgRegistres := MsgRegistres + ' (en aval)';
                        ListTronconsRegistresCad.Add(Etude.Batiment.Reseaux.ListeBesoinRegistre[m_NoRegistre].LienCad);
                        End;

        InterfaceCAD.Aero_Vilo2Fredo_HighlightObjects(ListTronconsRegistresCad);

        BBS_ShowMessage(MsgRegistres , mtError, [mbOK], 0);
        ListTronconsRegistresCad := Nil;
        End
    else
      Result := True;
end;
{ **************************************************************************************************************************************************** }
Procedure TDlg_SelectionCaisson.GoToSelection;
var
        CanContinue  : Boolean;
        MsgRegistres : String;
        m_NoRegistre : Integer;

        ListTronconsRegistresCad : IInterfaceList;
Begin

  Btn_RecalculAccesoires.Visible := False;

CanContinue := False;
{
  if not(CheckBox_DTU.Checked) then
        Grid_Selection.HideColumn(4);
}
//  If Panel_Actif = Panel_Preselection Then
  Begin
    GetChoix;
    if CheckBox_DTU.Checked=False then
        Etude.DTUActive := False
    else
        Etude.DTUActive := True;
    Etude.Calculer;
    CanContinue := False;
    GenereListeCaissons;
    if (CheckBoxEco.Checked = false) and (CheckBoxNEcoADepr.Checked = false) and (CheckBoxNEcoSDepr.Checked = false) then
            BBS_ShowMessage('Au moins une option de l''encadr� "' + GroupBoxVentilEcoEtDepressostat.Caption +'" doit �tre s�lectionn�', mtError, [mbOK], 0)
    else
    If Not(TestValiditeReseau) then
      begin
      //rien � faire
      end
    Else
    If ListeCaissons.Count > 0 Then
        Begin
        AffListeCaissons;
        CanContinue := True;
        End
    Else
        Begin
          if Etude.Batiment.IsFabricantVIM or Etude.Batiment.IsFabricantMVN then
            BBS_ShowMessage('Aucun caisson ne correspond aux crit�res de s�lection.', mtError, [mbOK], 0)
          else
            BBS_ShowMessage('Aucun caisson ne correspond � votre r�seau.', mtError, [mbOK], 0);
        End;
    if CheckBox_DTU.Checked then
        Begin
        Etude.DTUActive := True;
        Etude.Calculer;
        End;                               
  End;

  Panel_Actif := Panel_Selection;
  If CanContinue Then
        Begin
        Grid_Selection.Visible := True;
        CanContinueAfterSelection := True;
        Grid_Selection_TrieCol0;
        Grid_Selection.Row := 1;
        End
  else
        Begin
        AffDebitsCaisson(TVentil_Caisson(TVentil_Caisson(CaissonReseau)));
        Grid_Selection.Visible := False;
        CanContinueAfterSelection := False;
        End
End;
{ **************************************************************************************************************************************************** }
Procedure TDlg_SelectionCaisson.AffListeCaissons;
Var
  m_NoCaisson : Integer;
  m_Row       : Integer;
  m_Col       : Integer;
Begin
        if Etude.Batiment.TypeBat = c_BatimentTertiaire then
              Grid_Selection.ColCount := 4
        else
              Grid_Selection.ColCount := 6;
{
  Grid_Selection.WordWraps[4, 0] := True;
  Grid_Selection.WordWraps[0, 0] := True;
  Grid_Selection.WordWrap := True;
}
  Grid_Selection.Cells[0, 0] := 'D�signation';
  Grid_Selection.Cells[1, 0] := 'Prix';
  Grid_Selection.Cells[2, 0] := 'DP (Pa)';
  Grid_Selection.Cells[3, 0] := 'Vitesse';
  Grid_Selection.Cells[4, 0] := 'Conso. RT (W/m�/h)';
  Grid_Selection.Cells[5, 0] := 'Puissance Th-C (W-Th-C)';
{
  if Etude.DTU2014 then
    Grid_Selection.Cells[4, 0] := 'Coef RT2012 (W/m�/h)'
  else
    Grid_Selection.Cells[4, 0] := 'Coef RT2005 (W/m�/h)';
}
  if Etude.Batiment.IsFabricantACT or Etude.Batiment.IsFabricantMVN or Etude.Batiment.IsFabricantPNO then
    begin
    Grid_Selection.ColWidths[1] := 0;
      if not(Etude.Batiment.IsFabricantPNO) then
    Grid_Selection.ColWidths[2] := 0;
    Grid_Selection.ColWidths[3] := 0;
    Grid_Selection.AutoSizeCol(4);
    Grid_Selection.AutoSizeCol(5);
    end
  else
    begin
    Grid_Selection.ColWidths[4] := 120;
    end;


  For m_Row := 1 To Grid_Selection.RowCount - 1 Do Begin
    Grid_Selection.Objects[0, m_Row] := nil;
    For m_Col := 0 To Grid_Selection.ColCount - 1 Do Grid_Selection.Cells[m_Col, m_Row] := '';
  End;
  Grid_Selection.RowCount := 2;
  For m_NoCaisson := 0 To ListeCaissons.Count - 1 Do
  Begin
    If m_NoCaisson >= Grid_Selection.RowCount - 1 Then Grid_Selection.RowCount := Grid_Selection.RowCount + 1;
    Grid_Selection.Objects[0, m_NoCaisson + 1] := TEdibatec_Caisson(ListeCaissons.Objects[m_NoCaisson]);
    Grid_Selection.Cells[0, m_NoCaisson + 1] := TEdibatec_Caisson(ListeCaissons.Objects[m_NoCaisson]).Reference;
//    Grid_Selection.Cells[1, m_NoCaisson + 1] := Float2Str(TEdibatec_Caisson(ListeCaissons.Objects[m_NoCaisson]).Prix, 2) + ' �';
    Grid_Selection.Cells[1, m_NoCaisson + 1] := Float2Str(TEdibatec_Caisson(ListeCaissons.Objects[m_NoCaisson]).Prix, 2);
    Grid_Selection.Cells[2, m_NoCaisson + 1] := Float2Str(TEdibatec_Caisson(ListeCaissons.Objects[m_NoCaisson]).DpCalcQMax, 0);
//    Grid_Selection.Cells[2, m_NoCaisson + 1] := Float2Str(TEdibatec_Caisson(ListeCaissons.Objects[m_NoCaisson]).DpCalcQMin, 0);
    If TEdibatec_Caisson(ListeCaissons.Objects[m_NoCaisson]).Entrainement = c_Edibatec_Caisson_Entrainement_Direct Then
      Grid_Selection.Cells[3, m_NoCaisson + 1] := TEdibatec_Caisson(ListeCaissons.Objects[m_NoCaisson]).Position
    Else
    if TEdibatec_Caisson(ListeCaissons.Objects[m_NoCaisson]).TypeSelection = c_CaissonSelect_DebitCst then
        Grid_Selection.Cells[3, m_NoCaisson + 1] := TEdibatec_Caisson(ListeCaissons.Objects[m_NoCaisson]).Position
    else
        Grid_Selection.Cells[3, m_NoCaisson + 1] := TEdibatec_Caisson(ListeCaissons.Objects[m_NoCaisson]).Position  + ' tr/min';
    Grid_Selection.Cells[4, m_NoCaisson + 1] := Float2Str(TEdibatec_Caisson(ListeCaissons.Objects[m_NoCaisson]).CoeffRT, 2);

    if Etude.Batiment.IsFabricantACT or Etude.Batiment.IsFabricantMVN or Etude.Batiment.IsFabricantPNO then
      Grid_Selection.Cells[5, m_NoCaisson + 1] := Float2Str(TEdibatec_Caisson(ListeCaissons.Objects[m_NoCaisson]).PMoy, 2);


    If ListeCaissons.Objects[m_NoCaisson] = CaissonSelection Then Grid_Selection.Row := m_NoCaisson + 1;
  End;

    if Etude.Batiment.IsFabricantLIN then
      begin
      Grid_Selection.HideColumns(1, Grid_Selection.ColCount);
      GroupBox6.Visible := False;
      end;

    if Not(Etude.Batiment.IsFabricantACT or Etude.Batiment.IsFabricantMVN or Etude.Batiment.IsFabricantPNO) then
      Grid_Selection.HideColumn(5);

    if Etude.Batiment.IsFabricantPNO then
      begin
      Grid_Selection.HideColumn(1);
      Grid_Selection.HideColumn(3);
      end;

    if not(CheckBox_DTU.Checked) then
        Grid_Selection.HideColumn(4);

  AffDebitsCaisson(TVentil_Caisson(TVentil_Caisson(CaissonReseau)));

  Grid_Selection_TrieCol0;
  Grid_Selection.Row := 1;

  If IsNil(CaissonSelection) Then CaissonSelection := TEdibatec_Caisson(Grid_Selection.Objects[0, 1]);


    For m_NoCaisson := 0 To ListeCaissons.Count - 1 Do
        if TEdibatec_Caisson(Grid_Selection.Objects[0, m_NoCaisson + 1]).CodeProduit = TVentil_Caisson(CaissonReseau).SaveOptions_SelectionCaisson.OldReferenceCaisson then
                Begin
                Grid_Selection.Row := m_NoCaisson + 1;
                Break;
                End;
  
End;
{ **************************************************************************************************************************************************** }
Procedure TDlg_SelectionCaisson.GenereListeCaissons;
Var
  m_NoGamme   : Integer;
  m_NoProduit : Integer;
  m_Gamme     : TEdibatec_Gamme;
Begin
  FreeAndNil(ListeCaissons);
  ListeCaissons := TStringList.Create;
  If BaseEdibatec.Classe(c_Edibatec_Caisson).Gammes.Count > 0 Then Begin
    For m_NoGamme := 0 To BaseEdibatec.Classe(c_Edibatec_Caisson).Gammes.Count - 1 Do Begin
      m_Gamme := BaseEdibatec.Gamme(BaseEdibatec.IndexOf(c_Edibatec_Caisson), m_NoGamme);
      For m_NoProduit := 0 To m_Gamme.Produits.Count - 1 Do
      If CaissonValide(TEdibatec_Caisson(m_Gamme.Produits.Objects[m_NoProduit])) Then
        ListeCaissons.AddObject(TEdibatec_Caisson(m_Gamme.Produits.Objects[m_NoProduit]).CodeProduit, TEdibatec_Caisson(m_Gamme.Produits.Objects[m_NoProduit]));
    End;
  End;
End;
{ **************************************************************************************************************************************************** }
Function TDlg_SelectionCaisson.GetPanelActif: TPanel;
Begin
  Result := FPanel_Actif;
End;
{ **************************************************************************************************************************************************** }
Procedure TDlg_SelectionCaisson.SetPanelActif(const Value: TPanel);
Var
  m_NoComp : Integer;
Begin
  FPanel_Actif := Value;
  FPanel_Actif.Show;
  For m_NoComp := 0 To ComponentCount - 1 Do If Components[m_NoComp].InheritsFrom(TPanel) Then Begin
    If (TPanel(Components[m_NoComp]).Tag In [Cst_PanelCriteres..Cst_PanelAnnexe]) And (TPanel(Components[m_NoComp]) <> FPanel_Actif) Then
      TPanel(Components[m_NoComp]).Hide;
  End;
End;
{ **************************************************************************************************************************************************** }
function  TDlg_SelectionCaisson.CaissonGetNbPoints(_Caisson: TEdibatec_Caisson) : Integer;
Begin
  Result := _Caisson.ListeDebitsMinis.Count;
  Result := Max(Result,_Caisson.ListeDPMinis.Count);
  Result := Max(Result,_Caisson.ListePuissMinis.Count);
End;
{ **************************************************************************************************************************************************** }
Function TDlg_SelectionCaisson.CaissonValide(_Caisson: TEdibatec_Caisson): Boolean;
Begin
  Result := Caisson_Valide_Options(_Caisson) and Caisson_Valide_Fonctionnement(_Caisson) and Caisson_Valide_DPMiniMaxi(_Caisson) and Caisson_Valide_DeltaDP_MVN(_Caisson);
End;
{ **************************************************************************************************************************************************** }
Function TDlg_SelectionCaisson.Caisson_Valide_Options(_Caisson: TEdibatec_Caisson) : Boolean;
Begin
  Result := Caisson_Valide_Options_TypeCaisson(_Caisson);
  Result:=  Result And Caisson_Valide_Options_Entrainement(_Caisson);
  Result:=  Result And Caisson_Valide_Options_Alimentation(_Caisson);
  Result:=  Result And Caisson_Valide_Options_TypeMoteur(_Caisson);
  Result:=  Result And Caisson_Valide_Options_Depressostat(_Caisson);
  Result:=  Result And Caisson_Valide_Options_Agrementation(_Caisson);
  Result:=  Result And Caisson_Valide_DebitMiniMaxi(_Caisson);
    if Etude.Batiment.IsFabricantVIM or Etude.Batiment.IsFabricantMVN then
      Result:=  Result And Caisson_Valide_PositionsSorties(_Caisson);

End;
{ **************************************************************************************************************************************************** }
Function TDlg_SelectionCaisson.Caisson_Valide_Options_TypeCaisson(_Caisson: TEdibatec_Caisson) : Boolean;
Var
  m_Valid : Boolean;
Begin
  m_Valid := (Str2Int(TVentil_Caisson(TVentil_Caisson(CaissonReseau)).Chiff_TYPE_VENTILATEUR[c_Edibatec_Caisson_Type_Caisson - 1]) = c_OUI) And (_Caisson.TypeVentilateur = c_Edibatec_Caisson_Type_Caisson);
  m_Valid := m_Valid Or
            ((Str2Int(TVentil_Caisson(TVentil_Caisson(CaissonReseau)).Chiff_TYPE_VENTILATEUR[c_Edibatec_Caisson_Type_Tourelle - 1]) = c_OUI) And (_Caisson.TypeVentilateur = c_Edibatec_Caisson_Type_Tourelle));
  Result := m_Valid;
End;
{ **************************************************************************************************************************************************** }
Function TDlg_SelectionCaisson.Caisson_Valide_Options_Entrainement(_Caisson: TEdibatec_Caisson) : Boolean;
Var
  m_Valid : Boolean;
Begin
  m_Valid := (Str2Int(TVentil_Caisson(TVentil_Caisson(CaissonReseau)).Chiff_ENTRAINEMENT[c_Edibatec_Caisson_Entrainement_Direct - 1]) = c_OUI) And (_Caisson.Entrainement = c_Edibatec_Caisson_Entrainement_Direct);
  m_Valid := m_Valid Or
            ((Str2Int(TVentil_Caisson(TVentil_Caisson(CaissonReseau)).Chiff_ENTRAINEMENT[c_Edibatec_Caisson_Entrainement_DirectPoulie - 1]) = c_OUI) And (_Caisson.Entrainement = c_Edibatec_Caisson_Entrainement_DirectPoulie));
  Result := m_Valid;
End;
{ **************************************************************************************************************************************************** }
Function TDlg_SelectionCaisson.Caisson_Valide_Options_Alimentation(_Caisson: TEdibatec_Caisson) : Boolean;
Var
  m_Valid : Boolean;
Begin
  m_Valid := (Str2Int(TVentil_Caisson(TVentil_Caisson(CaissonReseau)).Chiff_ALIMENTATION[c_Edibatec_Caisson_Alimentation_MonoPhase - 1]) = c_OUI) And (_Caisson.Alimentation = c_Edibatec_Caisson_Alimentation_MonoPhase);
  m_Valid := m_Valid Or
            ((Str2Int(TVentil_Caisson(TVentil_Caisson(CaissonReseau)).Chiff_ALIMENTATION[c_Edibatec_Caisson_Alimentation_TriPhase - 1]) = c_OUI) And (_Caisson.Alimentation = c_Edibatec_Caisson_Alimentation_TriPhase));
  Result := m_Valid;
End;
{ **************************************************************************************************************************************************** }
Function TDlg_SelectionCaisson.Caisson_Valide_Options_TypeMoteur(_Caisson: TEdibatec_Caisson) : Boolean;
Var
  m_Valid : Boolean;
Begin
  m_Valid := (Str2Int(TVentil_Caisson(TVentil_Caisson(CaissonReseau)).Chiff_VITESSE[c_Edibatec_Caisson_Moteur_1Vitesse - 1]) = c_OUI) And (_Caisson.TypeDeMoteur = c_Edibatec_Caisson_Moteur_1Vitesse);
  m_Valid := m_Valid Or
            ((Str2Int(TVentil_Caisson(TVentil_Caisson(CaissonReseau)).Chiff_VITESSE[c_Edibatec_Caisson_Moteur_2Vitesse - 1]) = c_OUI) And (_Caisson.TypeDeMoteur = c_Edibatec_Caisson_Moteur_2Vitesse));
  Result := m_Valid;
End;
{ **************************************************************************************************************************************************** }
Function TDlg_SelectionCaisson.Caisson_Valide_Options_Depressostat(_Caisson: TEdibatec_Caisson) : Boolean;
Var
  m_Valid : Boolean;
  m_Valid2 : Boolean;
  m_Valid3 : Boolean;
Begin

  m_Valid  := (TVentil_Caisson(TVentil_Caisson(CaissonReseau)).Chiff_VENTILECO = c_Avec) And (_Caisson.TypeSelection = c_CaissonSelect_DebitCst);
  m_Valid2 := (TVentil_Caisson(TVentil_Caisson(CaissonReseau)).Chiff_NECOADEPR = c_Avec) And ((_Caisson.AvecDepressostat = c_Edibatec_Caisson_Depressostat_OUI) And Not(_Caisson.TypeSelection = c_CaissonSelect_DebitCst));
  m_Valid3 := (TVentil_Caisson(TVentil_Caisson(CaissonReseau)).Chiff_NECOSDEPR = c_Avec) And ((_Caisson.AvecDepressostat = c_Edibatec_Caisson_Depressostat_NON) And Not(_Caisson.TypeSelection = c_CaissonSelect_DebitCst));

  Result := m_Valid Or m_Valid2 Or m_Valid3;


    if (Etude.Batiment.TypeBat in [c_BatimentCollectif, c_BatimentHotel]) then
      Result := Result and (_Caisson.AvecDepressostat = c_Edibatec_Caisson_Depressostat_OUI);

{
  m_Valid := (TVentil_Caisson(TVentil_Caisson(CaissonReseau)).Chiff_DEPRESSOSTAT in [c_Tous, c_Avec]) And (_Caisson.AvecDepressostat = c_Edibatec_Caisson_Depressostat_OUI);
  m_Valid := m_Valid Or
            ((TVentil_Caisson(TVentil_Caisson(CaissonReseau)).Chiff_DEPRESSOSTAT in [c_Tous, c_Sans]) And (_Caisson.AvecDepressostat = c_Edibatec_Caisson_Depressostat_NON));

  m_Valid2 :=  (TVentil_Caisson(TVentil_Caisson(CaissonReseau)).Chiff_VENTILECO in [c_Tous,  c_Avec]) And ((_Caisson.AvecDepressostat = c_Edibatec_Caisson_Depressostat_OUI) And (_Caisson.TypeSelection = c_CaissonSelect_DebitCst));
  m_Valid2 :=  m_Valid2 Or
              ((TVentil_Caisson(TVentil_Caisson(CaissonReseau)).Chiff_VENTILECO in [c_Tous,  c_Sans]) And Not((_Caisson.AvecDepressostat = c_Edibatec_Caisson_Depressostat_OUI) And (_Caisson.TypeSelection = c_CaissonSelect_DebitCst)));

  Result := m_Valid And m_Valid2;
}
End;
{ **************************************************************************************************************************************************** }
Function TDlg_SelectionCaisson.Caisson_Valide_Options_Agrementation(_Caisson: TEdibatec_Caisson) : Boolean;
Var
  m_Valid : Boolean;
Begin
  m_Valid := (Str2Int(TVentil_Caisson(TVentil_Caisson(CaissonReseau)).Chiff_AGREMENT[c_Edibatec_Caisson_Agrement_NonAgree - 1]) = c_OUI) And (_Caisson.ClassementFeu = c_Edibatec_Caisson_Agrement_Libelle[c_Edibatec_Caisson_Agrement_NonAgree]);
  m_Valid := m_Valid Or
            ((Str2Int(TVentil_Caisson(TVentil_Caisson(CaissonReseau)).Chiff_AGREMENT[c_Edibatec_Caisson_Agrement_Agree4001h2C4 - 1]) = c_OUI) And ((_Caisson.ClassementFeu = c_Edibatec_Caisson_Agrement_Libelle[c_Edibatec_Caisson_Agrement_Agree4001h2C4]) or
                                                                                                               ((_Caisson.ClassementFeu = c_Edibatec_Caisson_Agrement_Libelle[c_Edibatec_Caisson_Agrement_Agree4001h]) and not((Etude.Batiment.TypeBat = c_BatimentTertiaire) and (Etude.Batiment.TypeTertiaire = c_TertiaireVMC))) or
                                                                                                               ((_Caisson.ClassementFeu = c_Edibatec_Caisson_Agrement_Libelle[c_Edibatec_Caisson_Agrement_Agree4002h]) and not((Etude.Batiment.TypeBat = c_BatimentTertiaire) and (Etude.Batiment.TypeTertiaire = c_TertiaireVMC))))
                                                                                                               );
  Result := m_Valid;
End;
{ **************************************************************************************************************************************************** }
Function TDlg_SelectionCaisson.Caisson_Valide_PositionsSorties(_Caisson: TEdibatec_Caisson) : Boolean;

        procedure MirroirSorties(var MyArray : Array of Integer);
        Var
                Cpt : Integer;
        Begin
                For Cpt := 0 To Length(MyArray) - 1 Do
                        Begin
                                Case MyArray[Cpt] of
                                        1 : MyArray[Cpt] := 6;
                                        2 : ;
                                        3 : ;
                                        4 : ;
                                        5 : ;
                                        6 : MyArray[Cpt] := 1;
                                End;
                        End;
        End;

        procedure InverseSorties180(var MyArray : Array of Integer);
        Var
                Cpt : Integer;
        Begin
                For Cpt := 0 To Length(MyArray) - 1 Do
                        Begin
                                Case MyArray[Cpt] of
                                        1 : MyArray[Cpt] := 6;
                                        2 : MyArray[Cpt] := 5;
                                        3 : ;
                                        4 : ;
                                        5 : MyArray[Cpt] := 2;
                                        6 : MyArray[Cpt] := 1;
                                End;
                        End;
        End;

        procedure InverseSorties90(var MyArray : Array of Integer);
        Var
                Cpt : Integer;
        Begin
                For Cpt := 0 To Length(MyArray) - 1 Do
                        Begin
                                Case MyArray[Cpt] of
                                        1 : MyArray[Cpt] := 2;
                                        2 : MyArray[Cpt] := 6;
                                        3 : ;
                                        4 : MyArray[Cpt] := 4;
                                        5 : MyArray[Cpt] := 1;
                                        6 : MyArray[Cpt] := 5;
                                End;
                        End;
        End;

        Function TestSorties(MyArray, ArrayCaisson : Array Of Integer) : Boolean;
        Var
                cptCR       : Integer;
                cpt_C       : Integer;
                CanContinue : Boolean;
        Begin
        Result := True;
                for cptCR := 0 to Length(MyArray) - 1 do
                        Begin
                        CanContinue := False;
                                for cpt_C := 0 to Length(ArrayCaisson) - 1 do
                                        if MyArray[cptCR] =ArrayCaisson[cpt_C] then
                                                begin
                                                CanContinue := true;
                                                break;
                                                end;
                                if CanContinue = false then
                                        Begin
                                        Result := False;
                                        Exit;
                                        End;
                        End;
        Result := True;
        End;


        Function TestEnLigne(MyArrayA, MyArrayB : Array of Integer) : Boolean;
        Var
                Cpt : Integer;
                AlignPetitsCotes : Integer;
                AlignGrandsCotes : Integer;
        Begin
        Result := False;
        AlignPetitsCotes := 0;
        AlignGrandsCotes := 0;

                For Cpt := 0 To Length(MyArrayA) - 1 Do
                        Begin
                                Case MyArrayA[Cpt] of
                                        1 : inc(AlignPetitsCotes);
                                        2 : inc(AlignGrandsCotes);
                                        3 : ;
                                        4 : ;
                                        5 : inc(AlignGrandsCotes);
                                        6 : inc(AlignPetitsCotes);
                                End;
                        End;

                For Cpt := 0 To Length(MyArrayB) - 1 Do
                        Begin
                                Case MyArrayB[Cpt] of
                                        1 : inc(AlignPetitsCotes);
                                        2 : inc(AlignGrandsCotes);
                                        3 : ;
                                        4 : ;
                                        5 : inc(AlignGrandsCotes);
                                        6 : inc(AlignPetitsCotes);
                                End;
                        End;                        

        Result := (AlignPetitsCotes = 2) Or (AlignGrandsCotes = 2);
        End;

        Function TestEnLigneBis(MyArrayA: Array of Integer) : Boolean;
        Var
                Cpt : Integer;
                AlignPetitsCotes : Integer;
                AlignGrandsCotes : Integer;
        Begin
        Result := False;
        AlignPetitsCotes := 0;
        AlignGrandsCotes := 0;

                For Cpt := 0 To Length(MyArrayA) - 1 Do
                        Begin
                                Case MyArrayA[Cpt] of
                                        1 : inc(AlignPetitsCotes);
                                        2 : inc(AlignGrandsCotes);
                                        3 : ;
                                        4 : ;
                                        5 : inc(AlignGrandsCotes);
                                        6 : inc(AlignPetitsCotes);
                                End;
                        End;

        Result := (AlignPetitsCotes = 2) Or (AlignGrandsCotes = 2);
        End;

Var
  m_NoSortie            : Integer;

  _PositionsAspi        : Array Of Integer;
  _PositionsRefoulement : Array Of Integer;
  _nbAspi               : integer;
  _nbRefoulement        : integer;



Begin

result := false;

        if (Length(_Caisson.PositionAspiration) = 0) or (Length(_Caisson.PositionRefoulement) = 0) then
                Begin
                result := False;
                exit;
                End;


_nbRefoulement := 0;
_nbAspi        := 0;


    if CaissonReseau.InheritsFrom(TVentil_TourelleTete) then
      begin
      _nbAspi := 1;
      _nbRefoulement := 0;
      SetLength(_PositionsAspi, 1);
      SetLength(_PositionsRefoulement, 0);
      _PositionsAspi[0] := 3;
      result := true;
      end
    else
      begin
        For m_NoSortie := 0 To TVentil_Caisson(TVentil_Caisson(CaissonReseau)).FilsCalcul.Count - 1 Do
            Begin
                    If TVentil_Caisson(TVentil_Caisson(CaissonReseau)).FilsCalcul[m_NoSortie].VersSortieToit Then
                            inc(_nbRefoulement)
                    else
                            inc(_nbAspi);
            End;

            if (Length(_Caisson.PositionAspiration) >= _nbAspi) and (Length(_Caisson.PositionRefoulement) >= _nbRefoulement) then
                    Begin
                    result := true;
    //                exit;
                    End;

    //exit;

            if Not(Result) then
                    Exit;
    SetLength(_PositionsAspi, 0);
    SetLength(_PositionsRefoulement, 0);


        //R�cup�ration des sorties du caisson
        For m_NoSortie := 0 To TVentil_Caisson(TVentil_Caisson(CaissonReseau)).Fils.Count - 1 Do
            Begin
                    If (TVentil_Caisson(TVentil_Caisson(CaissonReseau)).Fils[m_NoSortie].Fils <> nil) And (TVentil_Caisson(TVentil_Caisson(CaissonReseau)).Fils[m_NoSortie].Fils.count > 0) and (TVentil_TronconDessin_SortieCaisson(TVentil_Caisson(TVentil_Caisson(CaissonReseau)).Fils[m_NoSortie]).TypeSortie = c_CaissonSortie_Refoulement) Then
                            Begin
                            SetLength(_PositionsRefoulement, Length(_PositionsRefoulement) + 1);
                            _PositionsRefoulement[Length(_PositionsRefoulement) - 1] := TVentil_TronconDessin_SortieCaisson(TVentil_Caisson(TVentil_Caisson(CaissonReseau)).Fils[m_NoSortie]).GetNumeroSortieVIM;
                            End
                  else
                    If (TVentil_Caisson(TVentil_Caisson(CaissonReseau)).Fils[m_NoSortie].Fils <> nil) And (TVentil_Caisson(TVentil_Caisson(CaissonReseau)).Fils[m_NoSortie].Fils.count > 0) and (TVentil_TronconDessin_SortieCaisson(TVentil_Caisson(TVentil_Caisson(CaissonReseau)).Fils[m_NoSortie]).TypeSortie = c_CaissonSortie_Aspiration) Then
                            Begin
                            SetLength(_PositionsAspi, Length(_PositionsAspi) + 1);
                            _PositionsAspi[Length(_PositionsAspi) - 1] := TVentil_TronconDessin_SortieCaisson(TVentil_Caisson(TVentil_Caisson(CaissonReseau)).Fils[m_NoSortie]).GetNumeroSortieVIM;
                            End;
            End;
    end;



//test de la postion des sorties
Result := TestSorties(_PositionsRefoulement, _Caisson.PositionRefoulement) And TestSorties(_PositionsAspi, _Caisson.PositionAspiration);

        //si le test est ok, pas la peine de continuer
        if Result Then
                Exit;

//inversion de sens du caisson (car � la saisie, le caisson �tant sym�trique, on conna�t le haut/bas du caisson, mais pas l'orientation)                
InverseSorties180(_PositionsRefoulement);
InverseSorties180(_PositionsAspi);

//nouveau test de position avec les sorties invers�es
Result := TestSorties(_PositionsRefoulement, _Caisson.PositionRefoulement) And TestSorties(_PositionsAspi, _Caisson.PositionAspiration);

        if Result Then
                Exit;

        if TestEnLigne(_PositionsAspi, _PositionsRefoulement) then
                Begin
                InverseSorties90(_PositionsRefoulement);
                InverseSorties90(_PositionsAspi);
                Result := TestSorties(_PositionsRefoulement, _Caisson.PositionRefoulement) And TestSorties(_PositionsAspi, _Caisson.PositionAspiration);

                        if Result Then
                                Exit;

                InverseSorties180(_PositionsRefoulement);
                InverseSorties180(_PositionsAspi);

                Result := TestSorties(_PositionsRefoulement, _Caisson.PositionRefoulement) And TestSorties(_PositionsAspi, _Caisson.PositionAspiration);

                        if Result Then
                                Exit;

                End
        Else
                Begin
                InverseSorties90(_PositionsRefoulement);
                InverseSorties90(_PositionsAspi);
                Result := TestSorties(_PositionsRefoulement, _Caisson.PositionRefoulement) And TestSorties(_PositionsAspi, _Caisson.PositionAspiration);

                        if Result Then
                                Exit;

                InverseSorties90(_PositionsRefoulement);
                InverseSorties90(_PositionsAspi);
                Result := TestSorties(_PositionsRefoulement, _Caisson.PositionRefoulement) And TestSorties(_PositionsAspi, _Caisson.PositionAspiration);

                        if Result Then
                                Exit;

                InverseSorties90(_PositionsRefoulement);
                InverseSorties90(_PositionsAspi);
                Result := TestSorties(_PositionsRefoulement, _Caisson.PositionRefoulement) And TestSorties(_PositionsAspi, _Caisson.PositionAspiration);

                        if Result Then
                                Exit;

                MirroirSorties(_PositionsRefoulement);
                MirroirSorties(_PositionsAspi);

                Result := TestSorties(_PositionsRefoulement, _Caisson.PositionRefoulement) And TestSorties(_PositionsAspi, _Caisson.PositionAspiration);

                        if Result Then
                                Exit;

                InverseSorties90(_PositionsRefoulement);
                InverseSorties90(_PositionsAspi);
                Result := TestSorties(_PositionsRefoulement, _Caisson.PositionRefoulement) And TestSorties(_PositionsAspi, _Caisson.PositionAspiration);

                        if Result Then
                                Exit;

                InverseSorties90(_PositionsRefoulement);
                InverseSorties90(_PositionsAspi);
                Result := TestSorties(_PositionsRefoulement, _Caisson.PositionRefoulement) And TestSorties(_PositionsAspi, _Caisson.PositionAspiration);

                        if Result Then
                                Exit;

                InverseSorties90(_PositionsRefoulement);
                InverseSorties90(_PositionsAspi);
                Result := TestSorties(_PositionsRefoulement, _Caisson.PositionRefoulement) And TestSorties(_PositionsAspi, _Caisson.PositionAspiration);
                
                        if Result Then
                                Exit;
                End;


        //ajout pour mail du 08/01/2013
        If IsCaissonSelectionFromGammeJBEBorJBHBorJBEBECO(_Caisson) then
                Begin
                Result := TestEnLigneBis(_PositionsAspi);
                        if Result then
                                Exit;
                End;



End;
{ **************************************************************************************************************************************************** }
Function TDlg_SelectionCaisson.Caisson_Valide_DebitMiniMaxi(_Caisson: TEdibatec_Caisson) : Boolean;
Begin
  Result := True; // (Etude.Batiment.Reseaux.GetCaisson.DebitMini > _Caisson.DebitMinimal) And (Etude.Batiment.Reseaux.GetCaisson.DebitMaxi < _Caisson.DebitMaximal);
//  Result := (TVentil_Caisson(TVentil_Caisson(CaissonReseau)).DebitMini > _Caisson.DebitMinimal) And (TVentil_Caisson(TVentil_Caisson(CaissonReseau)).DebitMaxi < _Caisson.DebitMaximal);
End;
{ **************************************************************************************************************************************************** }
Function TDlg_SelectionCaisson.Caisson_Valide_DPMiniMaxi(_Caisson: TEdibatec_Caisson) : Boolean;
Begin

result := true;

  if Not(Etude.Batiment.IsFabricantVIM) and Not(Etude.Batiment.IsFabricantPNO) then
    Exit;

if not(Dlg_SelectionCaisson.CheckBox_DTU.Checked) then
begin
//**result := _Caisson.DpCalcQMax >= (TVentil_Caisson(TVentil_Caisson(CaissonReseau)).DPVentilateur.MinAQmax - Supp20);
//**result := result And (_Caisson.DpCalcQMax <= (TVentil_Caisson(TVentil_Caisson(CaissonReseau)).DPVentilateur.MaxAQmax - Supp20));
end
else
begin
//**result := (_Caisson.DpCalcQMax >= max(TVentil_Caisson(TVentil_Caisson(CaissonReseau)).DPVentilateur.MinAQmax, TVentil_Caisson(TVentil_Caisson(CaissonReseau)).DPVentilateur.MinAQmin)  - Supp20);
  if Etude.Batiment.IsFabricantPNO then
    begin
    result := result and (_Caisson.DpCalcQMin <= min(TVentil_Caisson(TVentil_Caisson(CaissonReseau)).Reseau_PdCVentil[ttc20Deg].MaxAQmax, TVentil_Caisson(TVentil_Caisson(CaissonReseau)).Reseau_PdCVentil[ttc20Deg].MaxAQmin));
    result := result and (_Caisson.DpCalcQMax >= max(TVentil_Caisson(TVentil_Caisson(CaissonReseau)).Reseau_PdCVentil[ttc20Deg].MinAQmax, TVentil_Caisson(TVentil_Caisson(CaissonReseau)).Reseau_PdCVentil[ttc20Deg].MinAQmin));
    end
  else
    result := result and (_Caisson.DpCalcQMin <= min(TVentil_Caisson(TVentil_Caisson(CaissonReseau)).DPVentilateur[ttc20Deg].MaxAQmax, TVentil_Caisson(TVentil_Caisson(CaissonReseau)).DPVentilateur[ttc20Deg].MaxAQmin));
end;
End;
{ **************************************************************************************************************************************************** }
Function TDlg_SelectionCaisson.Caisson_Valide_DeltaDP_MVN(_Caisson: TEdibatec_Caisson) : Boolean;
begin
Result := True;
  if Etude.Batiment.IsFabricantMVN then
    begin
    //caissons ExthEco et MCCEco et ExthEcowatt
    if _Caisson.TypeSelection in [c_CaissonSelect_DebitCst] then
      if Etude.Batiment.SystemeCollectif.Hygro then
        if Abs(Ceil(TVentil_Caisson(CaissonReseau).DPVentilateur[ttc20Deg].MaxAQmin) - Ceil(TVentil_Caisson(CaissonReseau).DPVentilateur[ttc20Deg].MaxAQmax)) > 3 then
          begin
          Result := False;
          end;

    //on en profite aussi pour v�rifier que le d�bit maxi du caisson est bien au dessus du d�bit maxi rt
      if StrToFloat(_Caisson.ListeDebitsMaxis[_Caisson.ListeDebitsMaxis.Count - 1]) < TVentil_Caisson(CaissonReseau).DebitMaxiRT * 1.1 then
        Result := False;
    end;
end;
{ **************************************************************************************************************************************************** }
Function TDlg_SelectionCaisson.Caisson_Valide_Selection3Courbes(_Caisson: TEdibatec_Caisson) : Boolean;
Var
  DpCourbeAQmin: Real;
  DpCourbeAQmax: Real;
  m_NoCourbe   : Integer;
  CaissonValide: Boolean;
  m_Str        : String;
  m_Stl1,
  m_Stl2,
  m_Stl3       : TStringList;
  PtsBezier         : TPointsControleBezier;
  m_PointQMinDpMin  : TPointDebitPression;
  m_PointQMaxDpMin  : TPointDebitPression;
  m_PointQMinDpMax  : TPointDebitPression;
  m_PointQMaxDpMax  : TPointDebitPression;
  m_Cond1           : Boolean;
  m_CondDTU         : Boolean;
  PuissCourbeAQmin: Real;
  PuissCourbeAQmax: Real;
Begin

//  Result := Caisson_Valide_Selection3CourbesBergeron(_Caisson);
//  Exit;
  DpCourbeAQMax := 0;
  DpCourbeAQMin := 0;
  _Caisson.Position := '';

  m_PointQMaxDpMin.Q  := TVentil_Caisson(TVentil_Caisson(CaissonReseau)).DebitMaxi;
  m_PointQMaxDpMin.DP := TVentil_Caisson(TVentil_Caisson(CaissonReseau)).DPVentilateur[ttc20Deg].MinAQmax;
  m_PointQMaxDpMax.Q  := TVentil_Caisson(TVentil_Caisson(CaissonReseau)).DebitMaxi;
  m_PointQMaxDpMax.DP := TVentil_Caisson(TVentil_Caisson(CaissonReseau)).DPVentilateur[ttc20Deg].MaxAQmax;

  m_PointQMinDpMin.Q  := TVentil_Caisson(TVentil_Caisson(CaissonReseau)).DebitMini;
  m_PointQMinDpMin.DP := TVentil_Caisson(TVentil_Caisson(CaissonReseau)).DPVentilateur[ttc20Deg].MinAQmin;
  m_PointQMinDpMax.Q  := TVentil_Caisson(TVentil_Caisson(CaissonReseau)).DebitMini;
  m_PointQMinDpMax.DP := TVentil_Caisson(TVentil_Caisson(CaissonReseau)).DPVentilateur[ttc20Deg].MaxAQmin;

//modif dtu
//  if Not CheckBox_DTU.Checked then
        Begin
        m_PointQMaxDpMin.DP := m_PointQMaxDpMin.DP;// - 20;
        m_PointQMaxDpMax.DP := m_PointQMaxDpMax.DP;// - 20;

        m_PointQMinDpMin.DP := m_PointQMinDpMin.DP;// - 20;
        m_PointQMinDpMax.DP := m_PointQMinDpMax.DP;// - 20;
        End;

  For m_NoCourbe := 1 To 3 Do
  Begin
    Case m_NoCourbe Of
      1: Begin
        m_Str := _Caisson.CourbeDebitPressionMini;
        m_Stl1 := _Caisson.ListeDebitsMinis;
        m_Stl2 := _Caisson.ListeDPMinis;
        m_Stl3 := _Caisson.ListePuissMinis;
      End;
      2: Begin
        m_Str := _Caisson.CourbeDebitPressionInter;
        m_Stl1 := _Caisson.ListeDebitsInt;
        m_Stl2 := _Caisson.ListeDPInt;
        m_Stl3 := _Caisson.ListePuissInt;
      End;
      3: Begin
        m_Str := _Caisson.CourbeDebitPressionMaxi;
        m_Stl1 := _Caisson.ListeDebitsMaxis;
        m_Stl2 := _Caisson.ListeDPMaxis;
        m_Stl3 := _Caisson.ListePuissMaxis;
      End;
    Else Begin
        m_Stl1 := nil;
        m_Stl2 := nil;
        m_Stl3 := nil;
        m_Str  := '';
      End;
    End;

    If (m_Stl1 <> nil) And (m_Stl1.Count > 0) And (m_Stl2 <> nil) And (m_Stl2.Count > 0) {And (m_Str <> '') }Then
    Begin
      If RadioBezier.ItemIndex = 0 Then PtsBezier := StringToPointsBezier_4points(m_Str)
                                   Else PtsBezier := StringToPointsBezier_Xpoints(m_Stl1, m_Stl2);
      DpCourbeAQMax := CalculDpCourbe(m_PointQMaxDpMax, PtsBezier);
      DpCourbeAQMin := CalculDpCourbe(m_PointQMinDpMax, PtsBezier);


      if Etude.Batiment.TypeBat = c_BatimentTertiaire then
        CheckBox_DTU.Checked  := false;
        m_CondDTU := (m_PointQMaxDpMin.DP <= DpCourbeAQmax) And (DpCourbeAQmax <= m_PointQMaxDpMax.DP);

{
        m_Cond1   := (DpCourbeAQMin <= m_PointQMinDpMax.DP);

//        m_Cond1 := m_Cond1 and (_Caisson.DpCalcQMax <= DpCourbeAQMax);

                if (Etude.Batiment.TypeBat <> c_BatimentTertiaire) then
                        Begin
//                        m_Cond1 := m_Cond1 and (DpCourbeAQMin <= _Caisson.DpCalcQMin);
                                                                                    
                        m_Cond1 := m_Cond1 and(m_PointQminDpMin.DP <= DpCourbeAQMin);
                        End;

}

        m_Cond1   := (m_PointQMaxDpMin.DP <= DpCourbeAQMax);
        m_Cond1 := m_Cond1 and (DpCourbeAQMax <= m_PointQMaxDpMax.DP);

        //ajout de la condition DTU. Selon S. Bergeron, pas de contr�le sur d�bit mini si non conformit� au DTU
                if (Etude.Batiment.TypeBat <> c_BatimentTertiaire) and CheckBox_DTU.Checked then
                        Begin
                        m_Cond1 := m_Cond1 and(m_PointQMinDpMin.DP <= DpCourbeAQMin);
                        m_Cond1 := m_Cond1 and (DpCourbeAQMin <= m_PointQMinDpMax.DP);
                        End;



                If CheckBox_DTU.Checked Then CaissonValide := m_Cond1 And m_CondDTU
                        Else CaissonValide := m_Cond1;
    End
    Else CaissonValide := False;        

    CaissonValide := CaissonValide And Not IsNan(DpCourbeAQMax) And (DpCourbeAQMax <> 0);

    If CaissonValide Then
    Begin
      Case m_NoCourbe Of
        1: _Caisson.Position := _Caisson.PositionMini;
        2: _Caisson.Position := _Caisson.PositionInter;
        3: _Caisson.Position := _Caisson.PositionMaxi;
      End;
      _Caisson.PositionChoisie := m_NoCourbe;
      _Caisson.Selection11Points := RadioBezier.ItemIndex = 1;
      _Caisson.CoeffRT := TVentil_Caisson(TVentil_Caisson(CaissonReseau)).CoeffRt(_Caisson);
      _Caisson.DpCalcQMax  := DpCourbeAQMax;
      _Caisson.DpCalcQMin  := DpCourbeAQMin;


      If RadioBezier.ItemIndex = 0 Then PtsBezier := StringToPointsBezier_4points(m_Str)
                                   Else PtsBezier := StringToPointsBezier_Xpoints(m_Stl1, m_Stl3);

      PuissCourbeAQMax := CalculDpCourbe(m_PointQMaxDpMax, PtsBezier);
      PuissCourbeAQMin := CalculDpCourbe(m_PointQMinDpMax, PtsBezier);

{
       CaissonValide := PuissCourbeAQMax <= DpCourbeAQMax;
                if (Etude.Batiment.TypeBat <> c_BatimentTertiaire) then
                        CaissonValide :=  CaissonValide And (PuissCourbeAQMin <= DpCourbeAQMin);
}

      _Caisson.PuissCalcQMax  := PuissCourbeAQMax;
      _Caisson.PuissCalcQMin  := PuissCourbeAQMin;

      _Caisson.CourbeDebitCalc := m_Stl1;
      _Caisson.CourbeDpCalc    := m_Stl2;
      _Caisson.CourbePuissCalc := m_Stl3;
      _Caisson.CourbeDebitCalcCourbeMontante := TStringList.Create;
      _Caisson.CourbeDebitCalcCourbeMontante.Clear;
      
        if CaissonValide then
                Break;
    End;
  End;
  Result := CaissonValide;
End;
{ **************************************************************************************************************************************************** }
Function TDlg_SelectionCaisson.Caisson_Valide_SelectionPlageFonctionnement(_Caisson: TEdibatec_Caisson) : Boolean;
Var
  m_Point   : TPointDebitPression;
  Cond1, Cond2: Boolean;
  Vit_DpMaxQMax,
  Vit_DpmaxQmin,
  Vit_DpMinQMax,
  Vit_DpminQmin: Real;
  VMinTrouvee, VMaxTrouvee: Real;
  cpt : Integer;
  MyVitesse : Real;
  myDP      : Real;
  myPUI     : Real;
  ValMaxAQMax : Real;
  ValMinAQMax : Real;
  ValMaxAQMin : Real;
  ValMinAQMin : Real;
  Ratio : Real;
Begin

  _Caisson.Position := '';
  m_Point.Dp := 0;

  ValMaxAQMax := TVentil_Caisson(TVentil_Caisson(CaissonReseau)).DPVentilateur[ttc20Deg].MaxAQmax;
  ValMinAQMax := TVentil_Caisson(TVentil_Caisson(CaissonReseau)).DPVentilateur[ttc20Deg].MinAQmax;
  ValMaxAQMin := TVentil_Caisson(TVentil_Caisson(CaissonReseau)).DPVentilateur[ttc20Deg].MaxAQmin;
  ValMinAQMin := TVentil_Caisson(TVentil_Caisson(CaissonReseau)).DPVentilateur[ttc20Deg].MinAQmin;

//modif dtu
//        if not CheckBox_DTU.Checked then
                Begin
                ValMaxAQMax := ValMaxAQMax;// - 20;
                ValMinAQMax := ValMinAQMax;// - 20;
                ValMaxAQMin := ValMaxAQMin;// - 20;
                ValMinAQMin := ValMinAQMin;// - 20;
                End;

  { Vitesses des 2 points � d�bit maxi }
  m_Point.Q := TVentil_Caisson(TVentil_Caisson(CaissonReseau)).DebitMaxi;
  m_Point.DP:= ValMaxAQMax;
//modif panol 21/05/2019
//  Vit_DpMaxQMax := CalculVitessePoint(m_Point, _Caisson, true);
  Vit_DpMaxQMax := CalculVitessePoint(m_Point, _Caisson, False);
  m_Point.DP:= ValMinAQMax;
//modif panol 21/05/2019
//  Vit_DpMinQMax := CalculVitessePoint(m_Point, _Caisson, true);
  Vit_DpMinQMax := CalculVitessePoint(m_Point, _Caisson, False);

  { Vitesses des 2 points � d�bit mini }
  m_Point.Q := TVentil_Caisson(TVentil_Caisson(CaissonReseau)).DebitMini;
  m_Point.DP:= ValMaxAQmin;
  Vit_DpMaxQMin := CalculVitessePoint(m_Point, _Caisson, true);
  m_Point.DP:= ValMinAQmin;
  Vit_DpMinQMin := CalculVitessePoint(m_Point, _Caisson, true);

  Cond1 := (Vit_DpMaxQMax > 0) And (Vit_DpMinQMax > 0);

  if CheckBox_DTU.Checked then
          Cond2 := (Vit_DpMaxQMin > 0) And (Vit_DpMinQMin > 0)
  else
          Cond2 := true;


{
  if CheckBox_DTU.Checked then
        begin
          // On prend comme vitesse mini le max des Vit_DpminQmin, Vit_DpminQmax et Vmin caisson
          VMinTrouvee := Max(_Caisson.VitesseMini, Max(Vit_DpMinQMin, Vit_DpMinQMax));
          // On prend comme vitesse maxi le min des vmax et Vmax caisson
          VMaxTrouvee := Min(_Caisson.VitesseMaxi, Min(Vit_DpMaxQMin, Vit_DpmaxQmax));
        end
  else
}  
        begin
          // On prend comme vitesse mini le max des Vit_DpminQmin, Vit_DpminQmax et Vmin caisson
          VMinTrouvee := Max(_Caisson.VitesseMini, Vit_DpMinQMax);
          // On prend comme vitesse maxi le min des vmax et Vmax caisson
          VMaxTrouvee := Min(_Caisson.VitesseMaxi, Vit_DpmaxQmax);
        end;

  Result := Cond1 And Cond2 And (VMinTrouvee <= VMaxTrouvee);
  If Result Then Begin
    // Puis on calcule la dp r�elle pour le point � QMin
    _Caisson.DpCalcQMin := ValMinAQmin * Sqr(VMinTrouvee/Vit_DpMinQMin);
    // Puis on calcule la dp r�elle pour le point � QMax
  _Caisson.DpCalcQMax := ValMinAQmax * Sqr(VMinTrouvee/Vit_DpMinQMax);
    _Caisson.Position := Float2Str(VMinTrouvee, 0);
    _Caisson.Selection11Points :=  RadioBezier.ItemIndex = 1;
    If _Caisson.DpCalcQMax < ValMinAQmax Then Result := False;
//    _Caisson.CoeffRT  := TVentil_Caisson(TVentil_Caisson(CaissonReseau)).CoeffRt(_Caisson);


    _Caisson.CourbeDebitCalc := TStringList.Create;
    _Caisson.CourbeDpCalc := TStringList.Create;
    _Caisson.CourbePuissCalc := TStringList.Create;
    _Caisson.CourbeDebitCalcCourbeMontante := TStringList.Create;

        For Cpt := 0 to _Caisson.ListeDebitsMinis.Count - 1 do
                Begin

                _Caisson.CourbeDebitCalc.add(_Caisson.ListeDebitsMinis[Cpt]);

                m_Point.Q := Str2Float(_Caisson.ListeDebitsMinis[Cpt]);
                m_Point.DP:= Str2Float(_Caisson.ListeDPMinis[Cpt]);
                MyVitesse := CalculVitessePoint(m_Point, _Caisson, true);


                MyDP := m_Point.DP * Sqr(VMinTrouvee/MyVitesse);
                _Caisson.CourbeDpCalc.Add(Float2Str(MyDP, 2));
                MyPUI := Str2Float(_Caisson.ListePuissMinis[Cpt]) * Sqr(VMinTrouvee/MyVitesse);
                _Caisson.CourbePuissCalc.Add(Float2Str(MyPUI, 2));


                Ratio := (MyDP - Str2Float(_Caisson.ListeDPMinis[Cpt])) / (Str2Float(_Caisson.ListeDPMaxis[Cpt]) - Str2Float(_Caisson.ListeDPMinis[Cpt]));
                Ratio := Max(0, Ratio);

                _Caisson.CourbeDebitCalc[Cpt] := Float2Str(Str2Float(_Caisson.ListeDebitsMinis[Cpt]) + (Str2Float(_Caisson.ListeDebitsMaxis[Cpt]) - Str2Float(_Caisson.ListeDebitsMinis[Cpt])) * Ratio, 0);
                _Caisson.CourbeDpCalc[Cpt]    := Float2Str(Str2Float(_Caisson.ListeDPMinis[Cpt])    + (Str2Float(_Caisson.ListeDPMaxis[Cpt])     - Str2Float(_Caisson.ListeDPMinis[Cpt])) * Ratio, 0);
                _Caisson.CourbePuissCalc[Cpt] := Float2Str(Str2Float(_Caisson.ListePuissMinis[Cpt]) + (Str2Float(_Caisson.ListePuissMaxis[Cpt])  - Str2Float(_Caisson.ListePuissMinis[Cpt])) * Ratio, 0);

                End;

 _Caisson.CoeffRT  := TVentil_Caisson(TVentil_Caisson(CaissonReseau)).CoeffRt(_Caisson, _Caisson.CourbeDebitCalc, _Caisson.CourbePuissCalc);

  End;
End;
{ **************************************************************************************************************************************************** }
Function  TDlg_SelectionCaisson.Caisson_Valide_SelectionDebitConstantOld(_Caisson: TEdibatec_Caisson) : Boolean;
        Procedure GetEquationDroite(_Pt1X, _Pt1Y, _Pt2X, _Pt2Y : Real; var _Coef, _Origine : Real);
        Begin
        _Coef :=  (_Pt2Y - _Pt1Y) / (_Pt2X - _Pt1X);
        _Origine := _Pt1Y - _Coef * _Pt1X;
        End;

        Function CalculeDebitSurDroite(_Pt1Debit, _pt1DP, _Pt2Debit, Pt2DP, _ValDP : Real) : Real;
        var
                CoefDir : Real;
                OrdonneeOrigine : Real;
        Begin
        GetEquationDroite(_Pt1Debit, _pt1DP, _Pt2Debit, Pt2DP, CoefDir, OrdonneeOrigine);
        Result := (_ValDP - OrdonneeOrigine) / CoefDir;
        End;

        Function CalculeDPSurDroite(_Pt1Debit, _pt1DP, _Pt2Debit, Pt2DP, _ValDebit : Real) : Real;
        var
                CoefDir : Real;
                OrdonneeOrigine : Real;
        Begin
        GetEquationDroite(_Pt1Debit, _pt1DP, _Pt2Debit, Pt2DP, CoefDir, OrdonneeOrigine);
        Result := CoefDir * _ValDebit + OrdonneeOrigine;
        End;


        Function DetermineDPAQCible(ListeDebits, ListeDPS : TStringList; ValeurAEncadrer, ValeurRemplacement : Real) : Real;
        var
                MyNoPoint : Integer;
        Begin
        Result := ValeurRemplacement;
                for MyNoPoint := 1 to ListeDebits.Count - 1 do
                        Begin
                                if Str2Float(ListeDebits[MyNoPoint]) > ValeurAEncadrer then
                                        Begin
                                                if MyNoPoint > 0 then
                                                        Result := CalculeDPSurDroite(Str2Float(ListeDebits[MyNoPoint]), Str2Float(ListeDPS[MyNoPoint]),
                                                                                                Str2Float(ListeDebits[MyNoPoint - 1]), Str2Float(ListeDPS[MyNoPoint - 1]),
                                                                                                ValeurAEncadrer)
                                                else
                                                        Result := ValeurRemplacement;
                                        Break;
                                        End;
                        End;
        End;

        Procedure TesteTranche(Var _Tranche : Integer; Var _DP : Real; _Q : Real);
        Begin
                if _Tranche = - 1 then
                        Begin
                                if _DP < Str2Float(_Caisson.ListeDPMinis[0]) then
                                        Begin
                                        _Tranche := 1;
                                        _DP := CalculeDPSurDroite(Str2Float(_Caisson.ListeDebitsMinis[_Tranche - 1]), Str2Float(_Caisson.ListeDPMinis[_Tranche - 1]), Str2Float(_Caisson.ListeDebitsMaxis[_Tranche - 1]) , Str2Float(_Caisson.ListeDPMaxis[_Tranche - 1]), _Q);
                                        End
                        Else
                                if _DP > Str2Float(_Caisson.ListeDPMinis[_Caisson.ListeDPMinis.Count - 1]) then
                                        Begin
                                        _Tranche := (_Caisson.ListeDPMinis.Count - 1);
                                        _DP := CalculeDPSurDroite(Str2Float(_Caisson.ListeDebitsMinis[_Tranche]), Str2Float(_Caisson.ListeDPMinis[_Tranche]), Str2Float(_Caisson.ListeDebitsMaxis[_Tranche]) , Str2Float(_Caisson.ListeDPMaxis[_Tranche]), _Q);
                                        End;
                        End;
        End;

        Function CalculeReglage(_Tranche, _NbPositionReglage : Integer; _DPCible : Real) : Real;
        Begin
        Result := 0.5 * Round(_NbPositionReglage * (_DPCible - Str2Float(_Caisson.ListeDPMinis[_Tranche - 1])) / (Str2Float(_Caisson.ListeDPMaxis[_Tranche - 1]) - Str2Float(_Caisson.ListeDPMinis[_Tranche - 1])) + 0.5);
        End;

var

        _NoPoint               : Integer;

        QMinCible              : Real;
        DPMinQMinCible         : Real;
        DPMaxQMinCible         : Real;
        QMaxCible              : Real;
        DPMinQMaxCible         : Real;
        DPMaxQMaxCible         : Real;

        QMinPosMin             : Real;
        QMinPosMax             : Real;
        QMaxPosMin             : Real;
        QMaxPosMax             : Real;

        DPPosMinAQMinCible     : Real;
        DPPosMaxAQMinCible     : Real;
        DPPosMinAQMaxCible     : Real;
        DPPosMaxAQMaxCible     : Real;

        PQMinCibleN1PosMin     : TPointDebitPression;
        PQMinCibleN1PosMax     : TPointDebitPression;
        PQMinCibleN2PosMin     : TPointDebitPression;
        PQMinCibleN2PosMax     : TPointDebitPression;

        PQMaxCibleN1PosMin     : TPointDebitPression;
        PQMaxCibleN1PosMax     : TPointDebitPression;
        PQMaxCibleN2PosMin     : TPointDebitPression;
        PQMaxCibleN2PosMax     : TPointDebitPression;

        DPN1AQminCible         : Real;
        DPN2AQminCible         : Real;

        DPN1AQmaxCible         : Real;
        DPN2AQmaxCible         : Real;

        TrancheDPMinQMinCible  : Integer;
        TrancheDPMaxQMinCible  : Integer;
        TrancheDPMinQMaxCible  : Integer;
        TrancheDPMaxQMaxCible  : Integer;

        PositionMini           : Real;
        PositionMaxi           : Real;
        NbPositionReglage      : Integer;
        
        ReglageDPMinQMinCible  : Real;
        ReglageDPMaxQMinCible  : Real;
        ReglageDPMinQMaxCible  : Real;
        ReglageDPMaxQMaxCible  : Real;

        ReglageMin             : Real;
        ReglageMax             : Real;
        Reglage                : Real;

        CourbeDebit            : TStringList;
        CourbeDP               : TStringList;
        CourbePuissance        : TStringList;

        TempDebit              : Real;
        TempDP                 : Real;
        TempPuissance          : Real;

        ResultatDPQminCible    : Real;
        ResultatDPQmaxCible    : Real;
        ResultatPQminCible     : Real;
        ResultatPQmaxCible     : Real;
Begin

result := false;
ResultatDPQmaxCible := 0;
ResultatDPQminCible := 0;
ResultatPQmaxCible := 0;
ResultatPQminCible := 0;


QMinCible              := TVentil_Caisson(TVentil_Caisson(CaissonReseau)).Reseau_DebitMinimum;
DPMinQMinCible         := TVentil_Caisson(TVentil_Caisson(CaissonReseau)).DPVentilateur[ttc20Deg].MinAQmin;
DPMaxQMinCible         := TVentil_Caisson(TVentil_Caisson(CaissonReseau)).DPVentilateur[ttc20Deg].MaxAQmin;
QMaxCible              := TVentil_Caisson(TVentil_Caisson(CaissonReseau)).Reseau_DebitMaximum;
DPMinQMaxCible         := TVentil_Caisson(TVentil_Caisson(CaissonReseau)).DPVentilateur[ttc20Deg].MinAQmax;
DPMaxQMaxCible         := TVentil_Caisson(TVentil_Caisson(CaissonReseau)).DPVentilateur[ttc20Deg].MaxAQmax;

//modif dtu
//        if Not CheckBox_DTU.Checked then
                Begin
                DPMinQMinCible         := DPMinQMinCible;// - 20;
                DPMaxQMinCible         := DPMaxQMinCible;// - 20;
                DPMinQMaxCible         := DPMinQMaxCible;// - 20;
                DPMaxQMaxCible         := DPMaxQMaxCible;// - 20;
                End;


QMinPosMin := Str2Float(_Caisson.ListeDebitsMinis[0]);
QMaxPosMin := Str2Float(_Caisson.ListeDebitsMinis[0]);
        for _NoPoint := 0 to _Caisson.ListeDebitsMinis.Count - 1 do
                Begin
                QMinPosMin := Min(QMinPosMin, Str2Float(_Caisson.ListeDebitsMinis[_NoPoint]));
                QMaxPosMin := Max(QMaxPosMin, Str2Float(_Caisson.ListeDebitsMinis[_NoPoint]));
                End;

QMinPosMax := Str2Float(_Caisson.ListeDebitsMaxis[0]);
QMaxPosMax := Str2Float(_Caisson.ListeDebitsMaxis[0]);
        for _NoPoint := 0 to _Caisson.ListeDebitsMaxis.Count - 1 do
                Begin
                QMinPosMax := Min(QMinPosMax, Str2Float(_Caisson.ListeDebitsMaxis[_NoPoint]));
                QMaxPosMax := Max(QMaxPosMax, Str2Float(_Caisson.ListeDebitsMaxis[_NoPoint]));
                End;

//ajout de la condition DTU. Selon S. Bergeron, pas de contr�le sur d�bit mini si non conformit� au DTU
if (CheckBox_DTU.Checked) then
begin
if (Min(QMinPosMin, QMinPosMax) <= QMinCible) and (QMinCible <= Max(QMaxPosMin, QMaxPosMax)) then
        Begin
        //on continue
        End
else
        Begin
        Result := False;
        exit;
        End;
end;        

DPMinQMinCible := Max(DPMinQMinCible, DetermineDPAQCible(_Caisson.ListeDebitsMinis, _Caisson.ListeDPMinis, QMinCible, 0));
DPMaxQMinCible := Min(DPMaxQMinCible, DetermineDPAQCible(_Caisson.ListeDebitsMaxis, _Caisson.ListeDPMaxis, QMinCible, 30000000000000000));
DPMinQMaxCible := Max(DPMinQMaxCible, DetermineDPAQCible(_Caisson.ListeDebitsMinis, _Caisson.ListeDPMinis, QMaxCible, 0));
DPMaxQMaxCible := Min(DPMaxQMaxCible, DetermineDPAQCible(_Caisson.ListeDebitsMaxis, _Caisson.ListeDPMaxis, QMaxCible, 30000000000000000));


TrancheDPMinQMinCible := - 1;
TrancheDPMaxQMinCible := - 1;
TrancheDPMinQMaxCible := - 1;
TrancheDPMaxQMaxCible := - 1;

        for _NoPoint := 0 to _Caisson.ListePuissMinis.Count - 1 do
                Begin
                        if _NoPoint < _Caisson.ListePuissMinis.Count - 1 then
                                Begin
                                        if (Min(Str2Float(_Caisson.ListeDebitsMinis[_NoPoint]), Str2Float(_Caisson.ListeDebitsMaxis[_NoPoint])) <= QMinCible) And
                                           (QMinCible <= Max(Str2Float(_Caisson.ListeDebitsMinis[_NoPoint + 1]), Str2Float(_Caisson.ListeDebitsMaxis[_NoPoint + 1]))) then
                                                Begin
                                                PQMinCibleN1PosMin.DP := Str2Float(_Caisson.ListeDPMinis[_NoPoint]);
                                                PQMinCibleN1PosMin.Q  := Str2Float(_Caisson.ListeDebitsMinis[_NoPoint]);
                                                PQMinCibleN1PosMax.DP := Str2Float(_Caisson.ListeDPMaxis[_NoPoint]);
                                                PQMinCibleN1PosMax.Q  := Str2Float(_Caisson.ListeDebitsMaxis[_NoPoint]);
                                                PQMinCibleN2PosMin.DP := Str2Float(_Caisson.ListeDPMinis[_NoPoint + 1]);
                                                PQMinCibleN2PosMin.Q  := Str2Float(_Caisson.ListeDebitsMinis[_NoPoint + 1]);
                                                PQMinCibleN2PosMax.DP := Str2Float(_Caisson.ListeDPMaxis[_NoPoint + 1]);
                                                PQMinCibleN2PosMax.Q  := Str2Float(_Caisson.ListeDebitsMaxis[_NoPoint + 1]);

                                                DPN1AQminCible := CalculeDPSurDroite(PQMinCibleN1PosMin.Q, PQMinCibleN1PosMin.DP, PQMinCibleN1PosMax.Q, PQMinCibleN1PosMax.DP, QMinCible);
                                                DPN2AQminCible := CalculeDPSurDroite(PQMinCibleN2PosMin.Q, PQMinCibleN2PosMin.DP, PQMinCibleN2PosMax.Q, PQMinCibleN2PosMax.DP, QMinCible);

                                                        if (DPN1AQminCible <= DPMinQMinCible) and
                                                           (DPMinQMinCible <= DPN2AQminCible) then
                                                                Begin
                                                                TrancheDPMinQMinCible := Min(10, Max(1, _NoPoint + 1));
                                                                End;

                                                        if (DPN1AQminCible <= DPMaxQMinCible) and
                                                           (DPMaxQMinCible <= DPN2AQminCible) then
                                                                Begin
                                                                TrancheDPMaxQMinCible := Min(10, Max(1, _NoPoint + 1));
                                                                End;

                                                End;

                                        if (Min(Str2Float(_Caisson.ListeDebitsMinis[_NoPoint]), Str2Float(_Caisson.ListeDebitsMaxis[_NoPoint])) <= QMaxCible) And
                                           (QMaxCible <= Max(Str2Float(_Caisson.ListeDebitsMinis[_NoPoint + 1]), Str2Float(_Caisson.ListeDebitsMaxis[_NoPoint + 1]))) then
                                                Begin
                                                PQMaxCibleN1PosMin.DP := Str2Float(_Caisson.ListeDPMinis[_NoPoint]);
                                                PQMaxCibleN1PosMin.Q  := Str2Float(_Caisson.ListeDebitsMinis[_NoPoint]);
                                                PQMaxCibleN1PosMax.DP := Str2Float(_Caisson.ListeDPMaxis[_NoPoint]);
                                                PQMaxCibleN1PosMax.Q  := Str2Float(_Caisson.ListeDebitsMaxis[_NoPoint]);
                                                PQMaxCibleN2PosMin.DP := Str2Float(_Caisson.ListeDPMinis[_NoPoint + 1]);
                                                PQMaxCibleN2PosMin.Q  := Str2Float(_Caisson.ListeDebitsMinis[_NoPoint + 1]);
                                                PQMaxCibleN2PosMax.DP := Str2Float(_Caisson.ListeDPMaxis[_NoPoint + 1]);
                                                PQMaxCibleN2PosMax.Q  := Str2Float(_Caisson.ListeDebitsMaxis[_NoPoint + 1]);

                                                DPN1AQmaxCible := CalculeDPSurDroite(PQMaxCibleN1PosMin.Q, PQMaxCibleN1PosMin.DP, PQMaxCibleN1PosMax.Q, PQMaxCibleN1PosMax.DP, QMaxCible);
                                                DPN2AQmaxCible := CalculeDPSurDroite(PQMaxCibleN2PosMin.Q, PQMaxCibleN2PosMin.DP, PQMaxCibleN2PosMax.Q, PQMaxCibleN2PosMax.DP, QMaxCible);

                                                        if (DPN1AQmaxCible <= DPMinQMaxCible) and
                                                           (DPMinQMinCible <= DPN2AQmaxCible) then
                                                                Begin
                                                                TrancheDPMinQMaxCible := Min(10, Max(1, _NoPoint + 1));
                                                                End;

                                                        if (DPN1AQmaxCible <= DPMaxQMaxCible) and
                                                           (DPMaxQMaxCible <= DPN2AQmaxCible) then
                                                                Begin
                                                                TrancheDPMaxQMaxCible := Min(10, Max(1, _NoPoint + 1));
                                                                End;


                                                End;
                                End;


                End;

TesteTranche(TrancheDPMinQMinCible, DPMinQMinCible, QMinCible);
TesteTranche(TrancheDPMaxQMinCible, DPMaxQMinCible, QMinCible);
TesteTranche(TrancheDPMinQMaxCible, DPMinQMaxCible, QMaxCible);
TesteTranche(TrancheDPMaxQMaxCible, DPMaxQMaxCible, QMaxCible);

  if Etude.Batiment.IsFabricantVIM or Etude.Batiment.IsFabricantMVN then
    begin
    PositionMini := Str2Float(Copy(StringReplace(_Caisson.PositionMini, ',', '.', [rfReplaceAll]), 4, Length(_Caisson.PositionMini) - 3));
    PositionMaxi := Str2Float(Copy(StringReplace(_Caisson.PositionMaxi, ',', '.', [rfReplaceAll]), 4, Length(_Caisson.PositionMaxi) - 3));
    end
  else
    begin
    PositionMini := 1;
    PositionMaxi := 3;
    end;
NbPositionReglage := round(2*(PositionMaxi - PositionMini));

if (TrancheDPMinQMinCible = -1) or
   (TrancheDPMaxQMinCible = -1) or
   (TrancheDPMinQMaxCible = -1) or
   (TrancheDPMaxQMaxCible = -1) then exit;

ReglageDPMinQMinCible := CalculeReglage(TrancheDPMinQMinCible, NbPositionReglage, DPMinQMinCible);
ReglageDPMaxQMinCible := CalculeReglage(TrancheDPMaxQMinCible, NbPositionReglage, DPMaxQMinCible);
ReglageDPMinQMaxCible := CalculeReglage(TrancheDPMinQMaxCible, NbPositionReglage, DPMinQMaxCible);
ReglageDPMaxQMaxCible := CalculeReglage(TrancheDPMaxQMaxCible, NbPositionReglage, DPMaxQMaxCible);

//Modification valid�e par Stephane Bergeron
if not(CheckBox_DTU.Checked) then
begin
ReglageMin := Max(ReglageDPMinQMaxCible, PositionMini);
ReglageMax := Min(ReglageDPMaxQMaxCible, PositionMaxi);
end
else
begin
ReglageMin := Max(Max(ReglageDPMinQMinCible, ReglageDPMinQMaxCible), PositionMini);
ReglageMax := Min(Min(ReglageDPMaxQMinCible, ReglageDPMaxQMaxCible), PositionMaxi);
end;
        if ReglageMin <= ReglageMax then
                Reglage := ReglageMin
        else
                Begin
                Result := False;
                Exit;
                End;


CourbeDebit     := TStringList.Create;
CourbeDP        := TStringList.Create;
CourbePuissance := TStringList.Create;

        for _NoPoint := 0 to _Caisson.ListePuissMinis.Count - 1 do
                Begin
                TempDebit := Str2Float(_Caisson.ListeDebitsMinis[_NoPoint]) + Reglage / (0.5 * NbPositionReglage) * (Str2Float(_Caisson.ListeDebitsMaxis[_NoPoint]) - Str2Float(_Caisson.ListeDebitsMinis[_NoPoint]));
                CourbeDebit.Add(Float2Str(TempDebit, 0));
                TempDP := Str2Float(_Caisson.ListeDPMinis[_NoPoint]) + Reglage / (0.5 * NbPositionReglage) * (Str2Float(_Caisson.ListeDPMaxis[_NoPoint]) - Str2Float(_Caisson.ListeDPMinis[_NoPoint]));
                CourbeDP.Add(Float2Str(TempDP, 0));
                TempPuissance := Str2Float(_Caisson.ListePuissMinis[_NoPoint]) + Reglage / (0.5 * NbPositionReglage) * (Str2Float(_Caisson.ListePuissMaxis[_NoPoint]) - Str2Float(_Caisson.ListePuissMinis[_NoPoint]));
                CourbePuissance.Add(Float2Str(TempPuissance, 0));
                End;

                if (Etude.Batiment.TypeBat <> c_BatimentTertiaire) then
                        if Str2Float(CourbeDebit[0]) > QMinCible then
                                exit;

        if Str2Float(CourbeDebit[CourbeDebit.Count - 1]) < QMaxCible then
                exit;

        for _NoPoint := 1 to CourbeDebit.Count - 1 do
                Begin
                        if Str2Float(CourbeDebit[_NoPoint]) >= QMinCible then
                                Begin
                                ResultatDPQminCible := Str2Float(CourbeDP[_NoPoint - 1]) + (QMinCible - Str2Float(CourbeDebit[_NoPoint - 1])) / (Str2Float(CourbeDebit[_NoPoint]) - Str2Float(CourbeDebit[_NoPoint - 1])) * (Str2Float(CourbeDP[_NoPoint]) - Str2Float(CourbeDP[_NoPoint - 1]));
                                ResultatPQminCible  := Str2Float(CourbePuissance[_NoPoint - 1]) + (QMinCible - Str2Float(CourbeDebit[_NoPoint - 1])) / (Str2Float(CourbeDebit[_NoPoint]) - Str2Float(CourbeDebit[_NoPoint - 1])) * (Str2Float(CourbePuissance[_NoPoint]) - Str2Float(CourbePuissance[_NoPoint - 1]));
                                Break;
                                End;
                End;

        for _NoPoint := 1 to CourbeDebit.Count - 1 do
                Begin
                        if Str2Float(CourbeDebit[_NoPoint]) >= QMaxCible then
                                Begin
                                ResultatDPQmaxCible := Str2Float(CourbeDP[_NoPoint - 1]) + (QMaxCible - Str2Float(CourbeDebit[_NoPoint - 1])) / (Str2Float(CourbeDebit[_NoPoint]) - Str2Float(CourbeDebit[_NoPoint - 1])) * (Str2Float(CourbeDP[_NoPoint]) - Str2Float(CourbeDP[_NoPoint - 1]));
                                ResultatPQmaxCible  := Str2Float(CourbePuissance[_NoPoint - 1]) + (QMaxCible - Str2Float(CourbeDebit[_NoPoint - 1])) / (Str2Float(CourbeDebit[_NoPoint]) - Str2Float(CourbeDebit[_NoPoint - 1])) * (Str2Float(CourbePuissance[_NoPoint]) - Str2Float(CourbePuissance[_NoPoint - 1]));
                                Break;
                                End;
                End;

Result := True;

_Caisson.Selection11Points :=  RadioBezier.ItemIndex = 1;
_Caisson.DpCalcQMax  := ResultatDPQmaxCible;
_Caisson.DpCalcQMin  := ResultatDPQminCible;
        if Int(Reglage)=Reglage then
        _Caisson.Position := Float2Str(Reglage, 0)
        else
        _Caisson.Position := Float2Str(Reglage, 1);

_Caisson.CoeffRT := TVentil_Caisson(TVentil_Caisson(CaissonReseau)).CoeffRt(_Caisson, CourbeDebit, CourbePuissance);
_Caisson.Position := 'Pos ' + _Caisson.Position;

_Caisson.PuissCalcQMax  := ResultatPQmaxCible;
_Caisson.PuissCalcQMin  := ResultatPQminCible;

_Caisson.CourbeDpCalc    := CourbeDP;
_Caisson.CourbeDebitCalc := CourbeDebit;
_Caisson.CourbePuissCalc := CourbePuissance;
_Caisson.CourbeDebitCalcCourbeMontante := TStringList.Create;
_Caisson.CourbeDebitCalcCourbeMontante.Clear;

{
CourbeDebit.Destroy;
CourbeDP.Destroy;
CourbePuissance.Destroy;
}
End;
{ **************************************************************************************************************************************************** }
Function  TDlg_SelectionCaisson.Caisson_Valide_SelectionDebitConstant(_Caisson: TEdibatec_Caisson; _ForceOld : Boolean = False) : Boolean;

        Function DeltaPe(_Q : Real) : Real;
        Begin
        result := ( _Caisson.Ventil_PressionConstante.EA_X3 * Power(_Q, 3) + _Caisson.Ventil_PressionConstante.EA_X2 * Power(_Q, 2) + _Caisson.Ventil_PressionConstante.EA_X1 * _Q + _Caisson.Ventil_PressionConstante.EA_X0) / Power(10, 18);
        End;

        Function DeltaPmin(_Q : Real) : Real;
        Begin
        result :=  ( _Caisson.Ventil_PressionConstante.MinA_X1 * _Q +  _Caisson.Ventil_PressionConstante.MinA_X0) / Power(10, 18);
        End;

        Function Pe(_Q : Real) : Real;
        Begin
        result := ( _Caisson.Ventil_PressionConstante.EP_X2 * Power(_Q, 2) + _Caisson.Ventil_PressionConstante.EP_X1 * _Q + _Caisson.Ventil_PressionConstante.EP_X0) / Power(10, 18);
        End;

        Function Pmin(_Q : Real) : Real;
        Begin
        result := ( _Caisson.Ventil_PressionConstante.MinP_X2 * Power(_Q, 2) + _Caisson.Ventil_PressionConstante.MinP_X1 * _Q + _Caisson.Ventil_PressionConstante.MinP_X0) / Power(10, 18);
        End;

        Function Rp(_Qp, _Deltap : Real) : Real;
        Begin
        result := _Deltap -  DeltaPmin(_Qp);
        End;

Var
        ConditionReglage       : Boolean;

        QMinCible              : Real;
        DPMinQMinCible         : Real;
        DPMaxQMinCible         : Real;
        QMaxCible              : Real;
        DPMinQMaxCible         : Real;
        DPMaxQMaxCible         : Real;

        QpCible                : Real;
        DeltapCible            : Real;

        RpCalcul               : Real;
        DeltaPeCalcul          : Real;
        DeltaPeCalculQmin          : Real;
        DeltaPeCalculQmax          : Real;
        PeCalcul               : Real;
        DeltaPminCalcul        : Real;
        PminCalcul             : Real;
        PpCalcul               : Real;
        PpCalculQmin               : Real;
        PpCalculQmax               : Real;

CourbeDebit     : TStringList;
CourbeDP        : TStringList;
CourbePuissance : TStringList;
_NoPoint : Integer;
TempDebit              : Single;
TempDP                 : Single;
TempPuissance          : Single;
Pas                    : Single;
Ratio                  : Real;

Begin
  if (Etude.Batiment.IsFabricantVIM or Etude.Batiment.IsFabricantMVN) and Not(_ForceOld) then
    begin
    Result := Caisson_Valide_SelectionDebitConstantBergeron(_Caisson);
    Exit;
    end;

Result := False;

QMinCible              := TVentil_Caisson(TVentil_Caisson(CaissonReseau)).Reseau_DebitMinimum;
DPMinQMinCible         := TVentil_Caisson(TVentil_Caisson(CaissonReseau)).DPVentilateur[ttc20Deg].MinAQmin;
DPMaxQMinCible         := TVentil_Caisson(TVentil_Caisson(CaissonReseau)).DPVentilateur[ttc20Deg].MaxAQmin;
QMaxCible              := TVentil_Caisson(TVentil_Caisson(CaissonReseau)).Reseau_DebitMaximum;
DPMinQMaxCible         := TVentil_Caisson(TVentil_Caisson(CaissonReseau)).DPVentilateur[ttc20Deg].MinAQmax;
DPMaxQMaxCible         := TVentil_Caisson(TVentil_Caisson(CaissonReseau)).DPVentilateur[ttc20Deg].MaxAQmax;

  if Etude.Batiment.IsFabricantVIM then
    begin
    DPMinQMinCible         := DPMinQMinCible;// - 20;
    DPMaxQMinCible         := DPMaxQMinCible;// - 20;
    DPMinQMaxCible         := DPMinQMaxCible;// - 20;
    DPMaxQMaxCible         := DPMaxQMaxCible;// - 20;
    end;


//if CheckBox_DTU.Checked then
begin
ConditionReglage       := False;
QpCible                := QMaxCible;
DeltapCible            := DPMinQMaxCible;



        if (_Caisson.Ventil_PressionConstante.QMin <= QpCible) and ( QpCible <= _Caisson.Ventil_PressionConstante.QMax) then
                Begin
                RpCalcul := Rp(QpCible, DeltapCible);
                        if (0 <= RpCalcul) and (RpCalcul <> (_Caisson.Ventil_PressionConstante.RMax - _Caisson.Ventil_PressionConstante.RMin) ) then
                                ConditionReglage := True;
                End;


        if not(ConditionReglage){ and CheckBox_DTU.Checked }then
                Exit;

DeltaPeCalcul    := DeltaPe(QpCible);
PeCalcul         := Pe(QpCible);
DeltaPminCalcul  := DeltaPmin(QpCible);
PminCalcul       := Pmin(QpCible);

//demande modif Bergeron du 10/02/2012
//PpCalcul := PminCalcul + (PeCalcul - PminCalcul) * (DeltapCible  - DeltaPminCalcul) / (DeltaPeCalcul  - DeltaPminCalcul);
PpCalcul := PminCalcul + (PeCalcul - PminCalcul) * Power((DeltapCible  - DeltaPminCalcul) / (DeltaPeCalcul  - DeltaPminCalcul), 3/2);

//DeltaPeCalculQmax := DeltaPeCalcul;
DeltaPeCalculQmax := DeltaPminCalcul + RpCalcul;
PpCalculQmax := PpCalcul;

end;

ConditionReglage       := False;
QpCible                := QMinCible;
DeltapCible            := DPMinQMinCible;



        if (_Caisson.Ventil_PressionConstante.QMin <= QpCible) and ( QpCible <= _Caisson.Ventil_PressionConstante.QMax) then
                Begin
//                RpCalcul := Rp(QpCible, DelatpCible);
                        if (0 <= RpCalcul) and (RpCalcul <> (_Caisson.Ventil_PressionConstante.RMax - _Caisson.Ventil_PressionConstante.RMin) ) then
                                ConditionReglage := True;
                End;


        if not(ConditionReglage) and CheckBox_DTU.Checked  then
                Exit;

DeltaPeCalcul    := DeltaPe(QpCible);
PeCalcul         := Pe(QpCible);
DeltaPminCalcul  := DeltaPmin(QpCible);
PminCalcul       := Pmin(QpCible);

//demande modif Bergeron du 10/02/2012
//PpCalcul := PminCalcul + (PeCalcul - PminCalcul) * (DeltapCible  - DeltaPminCalcul) / (DeltaPeCalcul  - DeltaPminCalcul);
PpCalcul := PminCalcul + (PeCalcul - PminCalcul) * Power((DeltapCible  - DeltaPminCalcul) / (DeltaPeCalcul  - DeltaPminCalcul), 3/2);

//DeltaPeCalculQmin := DeltaPeCalcul;
DeltaPeCalculQmin := DeltaPminCalcul + RpCalcul;
//DeltaPeCalculQmax := DeltaPmin(QMaxCible) + RpCalcul;
PpCalculQmin := PpCalcul;



Pas := (_Caisson.Ventil_PressionConstante.QMax - _Caisson.Ventil_PressionConstante.QMin) / 10;
CourbeDebit     := TStringList.Create;
CourbeDP        := TStringList.Create;
CourbePuissance := TStringList.Create;

Ratio := -1;

        for _NoPoint := 0 to CaissonGetNbPoints(_Caisson) - 1 do
                Begin

                TempDebit := Str2Float(_Caisson.ListeDebitsMinis[_NoPoint]);

                DeltaPminCalcul  := StrToFloat(_Caisson.ListeDPMinis[_NoPoint]);
                DeltaPeCalcul    := StrToFloat(_Caisson.ListeDPMaxis[_NoPoint]);

                PminCalcul       := StrToFloat(_Caisson.ListePuissMinis[_NoPoint]);
                PeCalcul         := StrToFloat(_Caisson.ListePuissMaxis[_NoPoint]);

//                DeltapCible := DeltaPmin(TempDebit);

//demande modif Bergeron du 10/02/2012
//                PpCalcul := PminCalcul + (PeCalcul - PminCalcul) * (DeltapCible  - DeltaPminCalcul) / (DeltaPeCalcul  - DeltaPminCalcul);
                PpCalcul := PminCalcul + (PeCalcul - PminCalcul) * Power((DeltapCible  - DeltaPminCalcul) / (DeltaPeCalcul  - DeltaPminCalcul), 3/2);

                TempDP := DeltaPminCalcul + RpCalcul ;
                TempPuissance := PpCalcul;

                CourbeDebit.Add(Float2Str(TempDebit, 0));
                CourbeDP.Add(Float2Str(TempDP, 0));
                CourbePuissance.Add(Float2Str(TempPuissance, 0));


                Ratio := (TempDP - Str2Float(_Caisson.ListeDPMinis[_NoPoint])) / (Str2Float(_Caisson.ListeDPMaxis[_NoPoint]) - Str2Float(_Caisson.ListeDPMinis[_NoPoint]));
                Ratio := Max(0, Ratio);

                CourbeDebit[_NoPoint]     := Float2Str(Str2Float(_Caisson.ListeDebitsMinis[_NoPoint]) + (Str2Float(_Caisson.ListeDebitsMaxis[_NoPoint]) - Str2Float(_Caisson.ListeDebitsMinis[_NoPoint])) * Ratio, 0);
                CourbeDP[_NoPoint]        := Float2Str(Str2Float(_Caisson.ListeDPMinis[_NoPoint])     + (Str2Float(_Caisson.ListeDPMaxis[_NoPoint])     - Str2Float(_Caisson.ListeDPMinis[_NoPoint])) * Ratio, 0);
//demande modif Bergeron du 10/02/2012
//                CourbePuissance[_NoPoint] := Float2Str(Str2Float(_Caisson.ListePuissMinis[_NoPoint])  + (Str2Float(_Caisson.ListePuissMaxis[_NoPoint])  - Str2Float(_Caisson.ListePuissMinis[_NoPoint])) * Power(Ratio, 3/2), 0);
                CourbePuissance[_NoPoint] := Float2Str(Str2Float(_Caisson.ListePuissMinis[_NoPoint])  + (Str2Float(_Caisson.ListePuissMaxis[_NoPoint])  - Str2Float(_Caisson.ListePuissMinis[_NoPoint])) * Power(Ratio, 3/2), 0);



                End;


                if (Etude.Batiment.TypeBat <> c_BatimentTertiaire)  and CheckBox_DTU.Checked  then
                        if Str2Float(CourbeDebit[0]) > QMinCible then
                                exit;

        if Str2Float(CourbeDebit[CourbeDebit.Count - 1]) < QMaxCible then
                exit;

  if Etude.Batiment.IsFabricantVIM then
    begin

    if DeltaPmin(QMinCible) > DeltaPeCalculQmin then exit;
    if DeltaPe(QMinCible) < DeltaPeCalculQmin then exit;

    if DeltaPmin(QMaxCible) > DeltaPeCalculQmax then exit;
    if DeltaPe(QMaxCible) < DeltaPeCalculQmax then exit;

    end;


Result := True;
_Caisson.Selection11Points :=  RadioBezier.ItemIndex = 1;
_Caisson.DpCalcQMax  := DeltaPeCalculQmax;
_Caisson.DpCalcQMin  := DeltaPeCalculQmin;

_Caisson.Position := '';

_Caisson.CoeffRT := TVentil_Caisson(TVentil_Caisson(CaissonReseau)).CoeffRt(_Caisson, CourbeDebit, CourbePuissance);
//_Caisson.CoeffRT := TVentil_Caisson(TVentil_Caisson(CaissonReseau)).CoeffRt(_Caisson);
//_Caisson.Position := 'Pos ' + _Caisson.Position;

_Caisson.PuissCalcQMax  := PpCalculQmax;
_Caisson.PuissCalcQMin  := PpCalculQmin;


_Caisson.CourbeDpCalc    := CourbeDP;
_Caisson.CourbeDebitCalc := CourbeDebit;
_Caisson.CourbePuissCalc := CourbePuissance;
_Caisson.CourbeDebitCalcCourbeMontante := TStringList.Create;
_Caisson.CourbeDebitCalcCourbeMontante.Clear;

End;
{ **************************************************************************************************************************************************** }
Function fde(RS_x, RS_C3, RS_C2,RS_C1,RS_c0,RS_P10 : Single) : Single;
Var
  RL_result : Single;
begin
  RL_result := RS_C3 * Power(RS_x, 3) + RS_C2 * Power(RS_x, 2) + RS_C1 * RS_x + RS_c0;
  RL_result := RoundTo(RL_result / Power(10, RS_P10), 0);
  Result := RL_result;
end;
{ **************************************************************************************************************************************************** }
//Procedure Calcule_Reglage(_Caisson: TEdibatec_Caisson; _CaissonReseau : TVentil_Caisson; Var RL_reglage : Single; Var RL_ReglageMax : Single);
Function Calcule_Reglage(_Caisson: TEdibatec_Caisson; _CaissonReseau : TVentil_Caisson) : Single;
Var
RS_DpMin : Single;
RS_DpMax : Single;
RS_qmaxDTU : Single;
RL_reglage :Single;
RL_ReglageMax : Single;
begin
{
QMinCible              := TVentil_Caisson(TVentil_Caisson(CaissonReseau)).Reseau_DebitMinimum;
DPMinQMinCible         := TVentil_Caisson(TVentil_Caisson(CaissonReseau)).DPVentilateur[ttc20Deg].MinAQmin;
DPMaxQMinCible         := TVentil_Caisson(TVentil_Caisson(CaissonReseau)).DPVentilateur[ttc20Deg].MaxAQmin;
}
RS_DpMin         := _CaissonReseau.DPVentilateur[ttc20Deg].MinAQmax;
RS_DpMax         := _CaissonReseau.DPVentilateur[ttc20Deg].MaxAQmax;
RS_qmaxDTU       := _CaissonReseau.Reseau_DebitMaximum;

RL_reglage :=_Caisson.Ventil_PressionConstante.RMin + RS_DpMin - fde(RS_qmaxDTU,_Caisson.Ventil_PressionConstante.MinA_X3, _Caisson.Ventil_PressionConstante.MinA_X2,_Caisson.Ventil_PressionConstante.MinA_x1, _Caisson.Ventil_PressionConstante.MinA_x0, 18);
RL_ReglageMax := _Caisson.Ventil_PressionConstante.RMin + RS_DpMax - fde(RS_qmaxDTU, _Caisson.Ventil_PressionConstante.MinA_X3, _Caisson.Ventil_PressionConstante.MinA_X2, _Caisson.Ventil_PressionConstante.MinA_x1, _Caisson.Ventil_PressionConstante.MinA_x0, 18);
  if (RL_reglage > _Caisson.Ventil_PressionConstante.RMax) or (RL_ReglageMax < _Caisson.Ventil_PressionConstante.RMin) then
    begin
    Result := -1;
    Exit;
    end;

  if (RL_reglage < _Caisson.Ventil_PressionConstante.RMin) then
     RL_reglage := _Caisson.Ventil_PressionConstante.RMin;

{
  if (RL_reglageMax > _Caisson.Ventil_PressionConstante.RMax) then
     RL_reglageMax := _Caisson.Ventil_PressionConstante.RMax;
}

Result := RL_reglage - _Caisson.Ventil_PressionConstante.RMin;
//RL_reglageMax := RL_reglageMax - _Caisson.Ventil_PressionConstante.RMax;

end;
{ **************************************************************************************************************************************************** }
Procedure Charge_Tableaux(_Caisson: TEdibatec_Caisson);
Var
  EL_ind : Integer;
  RL_Delta, RL_Q : Single;
begin
    if _caisson.Reference = 'MCC ECO 21D' then
    _caisson.Reference := _caisson.Reference;
  _Caisson.ListeDebitsMaxis.Clear;
  _Caisson.ListeDPMaxis.Clear;
  _Caisson.ListePuissMaxis.Clear;
  _Caisson.ListeDebitsMinis.Clear;
  _Caisson.ListeDPMinis.Clear;
  _Caisson.ListePuissMinis.Clear;

  For EL_ind := 0 to 11 - 1 do
    begin
    RL_Delta := (_Caisson.Ventil_PressionConstante.QMax - _Caisson.Ventil_PressionConstante.QMin) / 10;
    RL_Q := RoundTo(_Caisson.Ventil_PressionConstante.QMin + (EL_ind) * RL_Delta, 0);
    _Caisson.ListeDebitsMaxis.Add(Float2Str(RL_Q, 2));
    _Caisson.ListeDebitsMinis.Add(Float2Str(RL_Q, 2));
//    _Caisson.ListeDPMaxis.Add(Float2Str(Max(fde(RL_Q, _Caisson.Ventil_PressionConstante.EA_X3, _Caisson.Ventil_PressionConstante.EA_X2, _Caisson.Ventil_PressionConstante.EA_X1, _Caisson.Ventil_PressionConstante.EA_X0, 18), fde(RL_Q, 0, 0, _Caisson.Ventil_PressionConstante.MinA_X1, _Caisson.Ventil_PressionConstante.MinA_X0,18)), 2));
//verif bergeron
    _Caisson.ListeDPMaxis.Add(Float2Str(Max(fde(RL_Q, _Caisson.Ventil_PressionConstante.EA_X3, _Caisson.Ventil_PressionConstante.EA_X2, _Caisson.Ventil_PressionConstante.EA_X1, _Caisson.Ventil_PressionConstante.EA_X0, 18), fde(RL_Q, _Caisson.Ventil_PressionConstante.MinA_x3, _Caisson.Ventil_PressionConstante.MinA_x2, _Caisson.Ventil_PressionConstante.MinA_X1, _Caisson.Ventil_PressionConstante.MinA_X0,18)), 2));
    _Caisson.ListeDPMinis.Add(Float2Str(fde(RL_Q, _Caisson.Ventil_PressionConstante.MinA_X3, _Caisson.Ventil_PressionConstante.MinA_X2, _Caisson.Ventil_PressionConstante.MinA_X1, _Caisson.Ventil_PressionConstante.MinA_X0,18), 2));
    _Caisson.ListePuissMaxis.Add(Float2Str(Max(fde(RL_Q, _Caisson.Ventil_PressionConstante.EP_X3, _Caisson.Ventil_PressionConstante.EP_X2, _Caisson.Ventil_PressionConstante.EP_X1, _Caisson.Ventil_PressionConstante.EP_X0,18), fde(RL_Q, _Caisson.Ventil_PressionConstante.MinP_X3, _Caisson.Ventil_PressionConstante.MinP_X2, _Caisson.Ventil_PressionConstante.MinP_X1, _Caisson.Ventil_PressionConstante.MinP_X0,18)), 2));
    _Caisson.ListePuissMinis.Add(Float2Str(fde(RL_Q, _Caisson.Ventil_PressionConstante.MinP_X3, _Caisson.Ventil_PressionConstante.MinP_X2, _Caisson.Ventil_PressionConstante.MinP_X1, _Caisson.Ventil_PressionConstante.MinP_X0,18), 2));
    end;

end;
{ **************************************************************************************************************************************************** }
type
  SG_TriPoint = record
      Q : Single;
      DP : Single;
      P : Single;
  end;
{ **************************************************************************************************************************************************** }
Procedure Calcule_Puissance(SP_PMin : SG_TriPoint; Var SP_PCalc : SG_TriPoint; SP_Pmax : SG_TriPoint);
Var
  Rl_DeltaDpMax, RL_DeltaPMax,Rl_deltaDp, RL_DeltaP : Single;
begin
  Rl_DeltaDpMax := 0;
  RL_DeltaPMax := 0;
  Rl_deltaDp := 0;
  RL_DeltaP := 0;

  Rl_DeltaDpMax := SP_Pmax.dp-SP_PMin.dp;

  RL_DeltaPMax :=SP_Pmax.p-SP_PMin.p;
  Rl_deltaDp :=SP_PCalc.dp-SP_PMin.dp;
    If Rl_DeltaDpMax<>0 then
      begin
        If Rl_DeltaDpMax>0 then
          SP_PCalc.p :=SP_PMin.p+Abs(RL_DeltaPMax)*Power(Abs(Rl_deltaDp/Rl_DeltaDpMax), 3/2)
        Else
          SP_PCalc.p :=SP_PMin.p-Abs(RL_DeltaPMax)*Power(Abs(Rl_deltaDp/Rl_DeltaDpMax), 3/2);

        If SP_PCalc.p>SP_Pmax.p then
          SP_PCalc.p :=SP_Pmax.p;
        If SP_PCalc.p<SP_PMin.p then
          SP_PCalc.p :=SP_PMin.p;
      end
    else
      SP_PCalc.p :=SP_PMin.p;

SP_PCalc.p :=RoundTo(SP_PCalc.p,0);
end;
{ **************************************************************************************************************************************************** }
Function  TDlg_SelectionCaisson.Caisson_Valide_SelectionDebitConstantBergeron(_Caisson: TEdibatec_Caisson) : Boolean;
Var
  RS_DPTrouveeQMax : Single;
  RS_DPTrouveeQmin : Single;
  RS_PTrouveeQmax : Single;
  RS_PTrouveeQmin : Single;
  BL_Trouve : Boolean;
  RL_Reglage : Single;
  EL_ind : Integer;
  SL_PointMin, SL_PointMax, SL_PointCalc : SG_TriPoint;
  CourbeDebit     : TStringList;
  CourbeDP        : TStringList;
  CourbePuissance : TStringList;
  RS_qminDTU : Single;
  RS_qmaxDTU : Single;
  RS_qminRT : Single;
  RS_qmaxRT : Single;
begin

  BL_Trouve := False;

{$IfDef OLD_ATVENTIL}
  RS_qminRT         := TVentil_Caisson(CaissonReseau).DebitMiniRT;
{$Else}
  RS_qminRT         := TVentil_Caisson(CaissonReseau).DebitMiniRT[_Caisson.ClasseCDEP];
{$EndIf}
  RS_qmaxRT         := TVentil_Caisson(CaissonReseau).DebitMaxiRT;
  RS_qminDTU        := TVentil_Caisson(CaissonReseau).Reseau_DebitMinimum;
  RS_qmaxDTU        := TVentil_Caisson(CaissonReseau).Reseau_DebitMaximum;


  Charge_Tableaux(_Caisson);

    if Etude.Batiment.IsFabricantMVN then
      RL_Reglage := 1
    else
      RL_Reglage := Calcule_Reglage(_Caisson, TVentil_Caisson(CaissonReseau));


    if (RL_Reglage < 0) then
      begin
      Result := False;
      Exit;
      end;

  CourbeDebit     := TStringList.Create;
  CourbeDP        := TStringList.Create;
  CourbePuissance := TStringList.Create;


    for EL_ind := 0 to CaissonGetNbPoints(_Caisson) - 1 do
      begin
      SL_PointMax.Q := Str2Float(_Caisson.ListeDebitsMaxis[El_ind]);
      SL_PointMax.Dp :=Str2Float(_Caisson.ListeDPMaxis[El_ind]);
      SL_PointMax.P :=Str2Float(_Caisson.ListePuissMaxis[El_ind]);
      SL_PointMin.Q :=Str2Float(_Caisson.ListeDebitsMinis[El_ind]);
      SL_PointMin.Dp :=Str2Float(_Caisson.ListeDPMinis[El_ind]);
      SL_PointMin.P :=Str2Float(_Caisson.ListePuissMinis[El_ind]);

      CourbeDebit.Add(_Caisson.ListeDebitsMaxis[El_ind]);
//      CourbeDp.Add(Float2Str(RoundTo(fde(Str2Float(_Caisson.ListeDebitsMaxis[EL_ind]), 0, 0, _Caisson.Ventil_PressionConstante.MinA_x1, _Caisson.Ventil_PressionConstante.MinA_x0 + RL_Reglage * Power(10, 18), 18), 0), 2));
//verif bergeron
      CourbeDp.Add(Float2Str(RoundTo(fde(Str2Float(_Caisson.ListeDebitsMaxis[EL_ind]), _Caisson.Ventil_PressionConstante.MinA_x3, _Caisson.Ventil_PressionConstante.MinA_x2, _Caisson.Ventil_PressionConstante.MinA_x1, _Caisson.Ventil_PressionConstante.MinA_x0 + RL_Reglage * Power(10, 18), 18), 0), 2));
//      CourbePuissance.Add('0');

      SL_PointCalc.Q := Str2Float(CourbeDebit[EL_ind]);
      SL_PointCalc.Dp := Str2Float(CourbeDP[EL_ind]);
      SL_PointCalc.P := 0;

      Calcule_Puissance(SL_PointMin, SL_PointCalc, SL_PointMax);

      CourbePuissance.Add(Float2Str(SL_PointCalc.P, 2));

        if EL_ind < CaissonGetNbPoints(_Caisson) - 1 then
          begin
            if (Str2Float(_Caisson.ListeDebitsMaxis[EL_ind]) <= RS_qmaxDTU) and (RS_qmaxDTU <= Str2Float(_Caisson.ListeDebitsMaxis[EL_ind+1])) then
              begin
              SL_PointMin.Q :=RS_qmaxDTU;
              SL_PointMax.Q :=RS_qmaxDTU;
              SL_PointCalc.Q :=RS_qmaxDTU;
              SL_PointMin.Dp :=RoundTo(fde(RS_qmaxDTU,_Caisson.Ventil_PressionConstante.MinA_x3,_Caisson.Ventil_PressionConstante.MinA_x2,_Caisson.Ventil_PressionConstante.MinA_x1, _Caisson.Ventil_PressionConstante.MinA_x0, 18),0);
              SL_PointCalc.Dp :=RoundTo(fde(RS_qmaxDTU,_Caisson.Ventil_PressionConstante.MinA_x3,_Caisson.Ventil_PressionConstante.MinA_x2, _Caisson.Ventil_PressionConstante.MinA_x1, _Caisson.Ventil_PressionConstante.MinA_x0 + RL_Reglage * Power(10,18),18),0);
              SL_PointMax.Dp :=RoundTo(fde(RS_qmaxDTU,_Caisson.Ventil_PressionConstante.EA_x3,_Caisson.Ventil_PressionConstante.EA_x2,_Caisson.Ventil_PressionConstante.EA_x1,_Caisson.Ventil_PressionConstante.EA_x0,18),0);
                If (SL_PointMin.Dp<=SL_PointCalc.Dp) and (SL_PointCalc.Dp<=SL_PointMax.Dp) then
                  begin
                  RS_DPTrouveeQMax :=SL_PointCalc.Dp;
                  BL_Trouve := True;

                    if RS_qminDTU<>0 then
                      begin
                        if RS_qminDTU>=Str2Float(CourbeDebit[0]) then
                          begin
                          SL_PointMin.Q:=RS_qminDTU;
                          SL_PointMax.Q:=RS_qminDTU;
                          SL_PointCalc.Q:=RS_qminDTU;
                          SL_PointMin.Dp:=RoundTo(fde(RS_qminDTU,_Caisson.Ventil_PressionConstante.MinA_x3,_Caisson.Ventil_PressionConstante.MinA_x2,_Caisson.Ventil_PressionConstante.MinA_x1,_Caisson.Ventil_PressionConstante.MinA_x0,18),0);
                          SL_PointCalc.Dp:=RoundTo(fde(RS_qminDTU,_Caisson.Ventil_PressionConstante.MinA_x3,_Caisson.Ventil_PressionConstante.MinA_x2,_Caisson.Ventil_PressionConstante.MinA_x1,_Caisson.Ventil_PressionConstante.MinA_x0+RL_Reglage*Power(10,18),18),0);
                          SL_PointMax.Dp:=RoundTo(fde(RS_qminDTU,_Caisson.Ventil_PressionConstante.EA_x3,_Caisson.Ventil_PressionConstante.EA_x2,_Caisson.Ventil_PressionConstante.EA_x1,_Caisson.Ventil_PressionConstante.EA_x0,18),0);
                          RS_DPTrouveeQmin:=SL_PointCalc.Dp; //; RS_PTrouv�eQmin=SL_PointCalc:P
                          BL_trouve:=True;// stop
                          end
                        else
                          begin
                          BL_trouve := False;
                          end;
                      end;
                  end;
              end;
          end;
      end;

    If BL_trouve then
      RS_PTrouveeQmax := 0;


    For EL_ind := 0 to CaissonGetNbPoints(_Caisson) - 2 do
      begin
        if (Str2Float(_Caisson.ListeDebitsMaxis[EL_ind])<=RS_qmaxRT) and (RS_qmaxRT<=Str2Float(_Caisson.ListeDebitsMaxis[EL_ind + 1])) then
          begin
          SL_PointMin.Q:=RS_qmaxRT;
          SL_PointMax.Q:=RS_qmaxRT;
          SL_PointCalc.Q:=RS_qmaxRT;
          SL_PointMin.Dp:=RoundTo(fde(RS_qmaxRT,_Caisson.Ventil_PressionConstante.MinA_x3,_Caisson.Ventil_PressionConstante.MinA_x2,_Caisson.Ventil_PressionConstante.MinA_x1,_Caisson.Ventil_PressionConstante.MinA_x0,18),0);
          SL_PointCalc.Dp:=RoundTo(fde(RS_qmaxRT,_Caisson.Ventil_PressionConstante.MinA_x3,_Caisson.Ventil_PressionConstante.MinA_x2,_Caisson.Ventil_PressionConstante.MinA_x1,_Caisson.Ventil_PressionConstante.MinA_x0+RL_Reglage*Power(10,18),18),0);
          SL_PointMax.Dp:=RoundTo(fde(RS_qmaxRT,_Caisson.Ventil_PressionConstante.EA_x3,_Caisson.Ventil_PressionConstante.EA_x2,_Caisson.Ventil_PressionConstante.EA_x1,_Caisson.Ventil_PressionConstante.EA_x0,18),0);
          SL_PointMin.P:=RoundTo(fde(RS_qmaxRT,_Caisson.Ventil_PressionConstante.MinP_x3,_Caisson.Ventil_PressionConstante.MinP_x2,_Caisson.Ventil_PressionConstante.MinP_x1,_Caisson.Ventil_PressionConstante.MinP_x0,18),0);
          SL_PointMax.P:=RoundTo(fde(RS_qmaxRT,_Caisson.Ventil_PressionConstante.EP_x3,_Caisson.Ventil_PressionConstante.EP_x2,_Caisson.Ventil_PressionConstante.EP_x1,_Caisson.Ventil_PressionConstante.EP_x0,18),0);
          Calcule_Puissance(SL_PointMin, SL_PointCalc, SL_PointMax);
          RS_PTrouveeQmax:=Min(SL_PointCalc.P,SL_PointMax.P);
            if RS_qminRT<>0 then
              begin
                if RS_qminRT>=Str2Float(CourbeDebit[0]) then
                  begin
                  SL_PointMin.Q:=RS_qminRT;
                  SL_PointMax.Q:=RS_qminRT;
                  SL_PointCalc.Q:=RS_qminRT;
                  SL_PointMin.Dp:=fde(RS_qminRT,_Caisson.Ventil_PressionConstante.MinA_x3,_Caisson.Ventil_PressionConstante.MinA_x2,_Caisson.Ventil_PressionConstante.MinA_x1,_Caisson.Ventil_PressionConstante.MinA_x0,18);
                  SL_PointCalc.Dp:=fde(RS_qminRT,_Caisson.Ventil_PressionConstante.MinA_x3,_Caisson.Ventil_PressionConstante.MinA_x2,_Caisson.Ventil_PressionConstante.MinA_x1,_Caisson.Ventil_PressionConstante.MinA_x0+RL_Reglage*Power(10,18),18);
                  SL_PointMax.Dp:=fde(RS_qminRT,_Caisson.Ventil_PressionConstante.EA_x3,_Caisson.Ventil_PressionConstante.EA_x2,_Caisson.Ventil_PressionConstante.EA_x1,_Caisson.Ventil_PressionConstante.EA_x0,18);
                  SL_PointMin.P:=RoundTo(fde(RS_qminRT,_Caisson.Ventil_PressionConstante.MinP_x3,_Caisson.Ventil_PressionConstante.MinP_x2,_Caisson.Ventil_PressionConstante.MinP_x1,_Caisson.Ventil_PressionConstante.MinP_x0,18),0);
                  SL_PointMax.P:=RoundTo(fde(RS_qminRT,_Caisson.Ventil_PressionConstante.EP_x3,_Caisson.Ventil_PressionConstante.EP_x2,_Caisson.Ventil_PressionConstante.EP_x1,_Caisson.Ventil_PressionConstante.EP_x0,18),0);
                  Calcule_Puissance(SL_PointMin, SL_PointCalc, SL_PointMax);
                  RS_PTrouveeQmin:=Min(SL_PointCalc.P,SL_PointMax.P);
                  end;
              end;
          end;
      end;


  if RS_PTrouveeQmax=0 then
    BL_trouve:=False;

  Result := BL_trouve;

  if Not(Result) then
    Exit;
_Caisson.Selection11Points :=  RadioBezier.ItemIndex = 1;
_Caisson.DpCalcQMax  := RS_DPTrouveeQMax;
_Caisson.DpCalcQMin  := RS_DPTrouveeQmin;

_Caisson.Position := '';

_Caisson.CoeffRT := TVentil_Caisson(TVentil_Caisson(CaissonReseau)).CoeffRt(_Caisson, CourbeDebit, CourbePuissance);
//_Caisson.CoeffRT := TVentil_Caisson(TVentil_Caisson(CaissonReseau)).CoeffRt(_Caisson);
//_Caisson.Position := 'Pos ' + _Caisson.Position;

_Caisson.PuissCalcQMax  := RS_PTrouveeQmax;
_Caisson.PuissCalcQMin  := RS_PTrouveeQMin;


_Caisson.CourbeDpCalc    := CourbeDP;
_Caisson.CourbeDebitCalc := CourbeDebit;
_Caisson.CourbePuissCalc := CourbePuissance;
_Caisson.CourbeDebitCalcCourbeMontante := TStringList.Create;
_Caisson.CourbeDebitCalcCourbeMontante.Clear;

end;
{ **************************************************************************************************************************************************** }
Function f_lin_de(X1, X2, Y1, Y2, XBase : Single): Real;
Var
  a, b      : Real;
Begin
  a := (Y1 - Y2) / (X2 - X1);
  b := Y1 - a * X1;
  Result := a * XBase + b;
End;
{ **************************************************************************************************************************************************** }
Function  TDlg_SelectionCaisson.Caisson_Valide_Selection3CourbesBergeron(_Caisson: TEdibatec_Caisson) : Boolean;
Var
  RS_DPTrouveeQMax : Single;
  RS_DPTrouveeQmin : Single;
  RS_PTrouveeQmax : Single;
  RS_PTrouveeQmin : Single;
  BL_Valide : Boolean;
  BL_Valide1Qmax, BL_Valide2Qmax, BL_valide3Qmax : Boolean;
  BL_valQmax : Boolean;
  EL_ind, EL_ind2 : Integer;
  RL_DpCalc : Single;
  EL_IndRetenuQmax : Integer;
  EL_NumCourbeQmax : Integer;
  EL_NumCourbeQmin : Integer;
  TGL_Debit : TStringList;
  TGL_DP : TStringList;
  TGL_P : TStringList;
  RS_qminDTU : Single;
  RS_qmaxDTU : Single;
  RS_qminRT : Single;
  RS_qmaxRT : Single;
  RS_DpMin : Single;
  RS_DpMax : Single;
begin
BL_Valide := False;
BL_Valide1Qmax := False;
BL_Valide2Qmax := False;
BL_valide3Qmax := False;
BL_valQmax := False;
EL_IndRetenuQmax := 4;
EL_NumCourbeQmax := 4;
EL_NumCourbeQmin := 4;

{$IfDef OLD_ATVENTIL}
  RS_qminRT         := TVentil_Caisson(CaissonReseau).DebitMiniRT;
{$Else}
  RS_qminRT         := TVentil_Caisson(CaissonReseau).DebitMiniRT[_Caisson.ClasseCDEP];
{$EndIf}
  RS_qmaxRT         := TVentil_Caisson(CaissonReseau).DebitMaxiRT;
  RS_qminDTU        := TVentil_Caisson(CaissonReseau).Reseau_DebitMinimum;
  RS_qmaxDTU        := TVentil_Caisson(CaissonReseau).Reseau_DebitMaximum;
  RS_DpMin         := TVentil_Caisson(CaissonReseau).DPVentilateur[ttc20Deg].MinAQmax;
  RS_DpMax         := TVentil_Caisson(CaissonReseau).DPVentilateur[ttc20Deg].MaxAQmax;

EL_ind := 0;
  While (BL_Valide1Qmax = False) and (EL_ind <= CaissonGetNbPoints(_Caisson) - 2) do
    begin
      For EL_ind2 := 1 to 3 do
        begin
          Case EL_ind2 of
            1 : begin
                BL_valQmax := False;
                TGL_Debit := _Caisson.ListeDebitsMinis;
                TGL_DP    := _Caisson.ListeDPMinis;
                TGL_P     := _Caisson.ListePuissMinis;
                end;
            2 : begin
                BL_valQmax := BL_Valide1Qmax;
                TGL_Debit := _Caisson.ListeDebitsInt;
                TGL_DP    := _Caisson.ListeDPInt;
                TGL_P     := _Caisson.ListePuissInt;
                end;
            3 : begin
                BL_valQmax := BL_Valide1Qmax or BL_Valide2Qmax;
                TGL_Debit := _Caisson.ListeDebitsMaxis;
                TGL_DP    := _Caisson.ListeDPMaxis;
                TGL_P     := _Caisson.ListePuissMaxis;
                end;
          End;
          if (TGL_Debit.Count > 0) and (Str2Float(TGL_Debit[0]) <> 0) and (BL_valQmax = False) then
            begin
              if (RS_qmaxDTU >= Str2Float(TGL_Debit[EL_ind])) and (RS_qmaxDTU <= Str2Float(TGL_Debit[EL_ind+1])) then
                begin
                  if (RS_qminDTU = 0) or ((RS_qminDTU >= Str2Float(TGL_Debit[0])) and (RS_qminDTU <= Str2Float(TGL_Debit[CaissonGetNbPoints(_Caisson) - 1]))) then
                    begin
                    RL_DpCalc := f_lin_de(Str2Float(TGL_Debit[EL_ind]), Str2Float(TGL_Debit[EL_ind+1]), Str2Float(TGL_Dp[EL_ind]), Str2Float(TGL_Dp[EL_ind+1]), RS_qmaxDTU);
                      if (RL_DpCalc >= RS_DpMin) and (RL_DpCalc <= RS_DpMax) then
                        begin
                          Case EL_ind2 of
                            1 : BL_Valide1Qmax := True;
                            2 : BL_Valide2Qmax := True;
                            3 : BL_Valide3Qmax := True;
                          End;
                        RS_DPTrouveeQMax := RoundTo(RL_DpCalc, 0);
                        RS_PTrouveeQmax := RoundTo(f_lin_de(Str2Float(TGL_Debit[EL_ind]), Str2Float(TGL_Debit[EL_ind+1]),Str2Float(TGL_P[EL_ind]), Str2Float(TGL_P[EL_ind+1]), RS_qmaxDTU), 0);
                          if (EL_NumCourbeQmax <> 1) then
                            begin
                            EL_IndRetenuQmax := EL_ind;
                            EL_NumCourbeQmax := EL_ind2;
                            end;
                        End;
                    End;
                End;
            End;
        End;
    inc(EL_ind);
    end;
  BL_Valide := BL_Valide1Qmax or BL_Valide2Qmax or BL_valide3Qmax;

  if EL_NumCourbeQmax <> 4 then
    begin
      Case EL_NumCourbeQmax of
        1 : begin
            _Caisson.Position := _Caisson.PositionMini;
            TGL_Debit := _Caisson.ListeDebitsMinis;
            TGL_DP    := _Caisson.ListeDPMinis;
            TGL_P     := _Caisson.ListePuissMinis;
            end;
        2 : begin
            _Caisson.Position := _Caisson.PositionInter;
            TGL_Debit := _Caisson.ListeDebitsInt;
            TGL_DP    := _Caisson.ListeDPInt;
            TGL_P     := _Caisson.ListePuissInt;
            end;
        3 : begin
            _Caisson.Position := _Caisson.PositionMaxi;
            TGL_Debit := _Caisson.ListeDebitsMaxis;
            TGL_DP    := _Caisson.ListeDPMaxis;
            TGL_P     := _Caisson.ListePuissMaxis;
            end;
      end;

    RS_DPTrouveeQMax := RoundTo(f_lin_de(Str2Float(TGL_Debit[EL_IndRetenuQmax]), Str2Float(TGL_Debit[EL_IndRetenuQmax+1]), Str2Float(TGL_Dp[EL_IndRetenuQmax]), Str2Float(TGL_Dp[EL_IndRetenuQmax+1]), RS_qmaxDTU),0);
    RS_PTrouveeQmax := RoundTo(f_lin_de(Str2Float(TGL_Debit[EL_IndRetenuQmax]), Str2Float(TGL_Debit[EL_IndRetenuQmax+1]), Str2Float(TGL_P[EL_IndRetenuQmax]), Str2Float(TGL_P[EL_IndRetenuQmax+1]), RS_qmaxRT), 0);
      if RS_qminDTU <> 0 then
        begin
        EL_ind := 1;
        BL_valQmax := False;
          While (BL_valQmax = False) and (EL_ind <= CaissonGetNbPoints(_Caisson) - 2) do
            begin
              if (RS_qminDTU >= Str2Float(TGL_Debit[EL_ind])) and (RS_qminDTU <= Str2Float(TGL_Debit[EL_ind+1])) then
                begin
                RS_DPTrouveeQmin := RoundTo(f_lin_de(Str2Float(TGL_Debit[EL_ind]), Str2Float(TGL_Debit[EL_ind+1]), Str2Float(TGL_Dp[EL_ind]), Str2Float(TGL_Dp[EL_ind+1]), RS_qminDTU), 0);
                BL_valQmax := True;
                end;
            Inc(EL_ind);
            End;
        End;

      if RS_qminRT <> 0 then
        begin
        EL_ind := 1;
        BL_valQmax := False;
          While (BL_valQmax = False) and (EL_ind <= CaissonGetNbPoints(_Caisson) - 2) do
            begin
              if (RS_qminRT >= Str2Float(TGL_Debit[EL_ind])) and (RS_qminRT <= Str2Float(TGL_Debit[EL_ind+1])) then
                begin
                RS_PTrouveeQmin := RoundTo(f_lin_de(Str2Float(TGL_Debit[EL_ind]), Str2Float(TGL_Debit[EL_ind+1]), Str2Float(TGL_P[EL_ind]), Str2Float(TGL_P[EL_ind+1]), RS_qminRT), 0);
                BL_valQmax := True;
                end;
            Inc(EL_ind);
            end;
        end;
    end;

  BL_Valide := BL_Valide1Qmax or BL_Valide2Qmax or BL_valide3Qmax;
Result := BL_Valide;

  if Not(Result) then
    Exit;
_Caisson.DpCalcQMax  := RS_DPTrouveeQMax;
_Caisson.DpCalcQMin  := RS_DPTrouveeQmin;
_Caisson.CoeffRT := TVentil_Caisson(TVentil_Caisson(CaissonReseau)).CoeffRt(_Caisson, TGL_Debit, TGL_P);
_Caisson.PuissCalcQMax  := RS_PTrouveeQmax;
_Caisson.PuissCalcQMin  := RS_PTrouveeQMin;
_Caisson.CourbeDpCalc    := TGL_DP;
_Caisson.CourbeDebitCalc := TGL_Debit;
_Caisson.CourbePuissCalc := TGL_P;
_Caisson.CourbeDebitCalcCourbeMontante := TStringList.Create;
_Caisson.CourbeDebitCalcCourbeMontante.Clear;
end;
{ **************************************************************************************************************************************************** }
Function  TDlg_SelectionCaisson.Caisson_Valide_SelectionPlageFonctionnementBergeron(_Caisson: TEdibatec_Caisson) : Boolean;
begin
end;
{ **************************************************************************************************************************************************** }
Function  TDlg_SelectionCaisson.Caisson_Valide_SelectionPlageFonctionnementEtCourbeLimiteBergeron(_Caisson: TEdibatec_Caisson) : Boolean;
begin

end;
{ **************************************************************************************************************************************************** }
Function  TDlg_SelectionCaisson.Caisson_Valide_SelectionPlageFonctionnementEcowatt2Bergeron(_Caisson: TEdibatec_Caisson) : Boolean;
begin

end;
{ **************************************************************************************************************************************************** }
Function  TDlg_SelectionCaisson.Caisson_Valide_SelectionMVNCourbeMontante(_Caisson: TEdibatec_Caisson) : Boolean;
Var
  RS_DPTrouveeQMax : Single;
  RS_DPTrouveeQmin : Single;
  RS_PTrouveeQmax : Single;
  RS_PTrouveeQmin : Single;
  BL_Trouve : Boolean;
  RL_Reglage : Single;
  EL_ind : Integer;
  SL_PointMin, SL_PointMax, SL_PointCalc : SG_TriPoint;
  CourbeDebit     : TStringList;
  CourbeDP        : TStringList;
  CourbePuissance : TStringList;
  RS_qminDTU : Single;
  RS_qmaxDTU : Single;
  RS_qminRT : Single;
  RS_qmaxRT : Single;
begin

  if Not(Etude.Batiment.SystemeCollectif.Hygro) then
    Begin
    Result := False;
    Exit;
    End;

  BL_Trouve := False;

{$IfDef OLD_ATVENTIL}
  RS_qminRT         := TVentil_Caisson(CaissonReseau).DebitMiniRT;
{$Else}
  RS_qminRT         := TVentil_Caisson(CaissonReseau).DebitMiniRT[_Caisson.ClasseCDEP];
{$EndIf}
  RS_qmaxRT         := TVentil_Caisson(CaissonReseau).DebitMaxiRT;
  RS_qminDTU        := TVentil_Caisson(CaissonReseau).Reseau_DebitMinimum;
  RS_qmaxDTU        := TVentil_Caisson(CaissonReseau).Reseau_DebitMaximum;

    if (_Caisson.Ventil_PressionConstante.QMin > RS_qminDTU) or
        (_Caisson.Ventil_PressionConstante.QMax < RS_qmaxDTU) then
          Exit;


  Charge_Tableaux(_Caisson);

    if Etude.Batiment.IsFabricantMVN then
      RL_Reglage := 1
    else
      RL_Reglage := Calcule_Reglage(_Caisson, TVentil_Caisson(CaissonReseau));


    if (RL_Reglage < 0) then
      begin
      Result := False;
      Exit;
      end;

  CourbeDebit     := TStringList.Create;
  CourbeDP        := TStringList.Create;
  CourbePuissance := TStringList.Create;


    for EL_ind := 0 to CaissonGetNbPoints(_Caisson) - 1 do
      begin
      SL_PointMax.Q := Str2Float(_Caisson.ListeDebitsMaxis[El_ind]);
      SL_PointMax.Dp :=Str2Float(_Caisson.ListeDPMaxis[El_ind]);
      SL_PointMax.P :=Str2Float(_Caisson.ListePuissMaxis[El_ind]);
      SL_PointMin.Q :=Str2Float(_Caisson.ListeDebitsMinis[El_ind]);
      SL_PointMin.Dp :=Str2Float(_Caisson.ListeDPMinis[El_ind]);
      SL_PointMin.P :=Str2Float(_Caisson.ListePuissMinis[El_ind]);

      CourbeDebit.Add(_Caisson.ListeDebitsMaxis[El_ind]);
//      CourbeDp.Add(Float2Str(RoundTo(fde(Str2Float(_Caisson.ListeDebitsMaxis[EL_ind]), 0, 0, _Caisson.Ventil_PressionConstante.MinA_x1, _Caisson.Ventil_PressionConstante.MinA_x0 + RL_Reglage * Power(10, 18), 18), 0), 2));
//verif bergeron
      CourbeDp.Add(Float2Str(RoundTo(fde(Str2Float(_Caisson.ListeDebitsMaxis[EL_ind]), _Caisson.Ventil_PressionConstante.MinA_x3, _Caisson.Ventil_PressionConstante.MinA_x2, _Caisson.Ventil_PressionConstante.MinA_x1, _Caisson.Ventil_PressionConstante.MinA_x0 + RL_Reglage * Power(10, 18), 18), 0), 2));
//      CourbePuissance.Add('0');

      SL_PointCalc.Q := Str2Float(CourbeDebit[EL_ind]);
      SL_PointCalc.Dp := Str2Float(CourbeDP[EL_ind]);
      SL_PointCalc.P := 0;

      Calcule_Puissance(SL_PointMin, SL_PointCalc, SL_PointMax);

      CourbePuissance.Add(Float2Str(SL_PointCalc.P, 2));

        if EL_ind < CaissonGetNbPoints(_Caisson) - 1 then
          begin
            if (Str2Float(_Caisson.ListeDebitsMaxis[EL_ind]) <= RS_qmaxDTU) and (RS_qmaxDTU <= Str2Float(_Caisson.ListeDebitsMaxis[EL_ind+1])) then
              begin
              SL_PointMin.Q :=RS_qmaxDTU;
              SL_PointMax.Q :=RS_qmaxDTU;
              SL_PointCalc.Q :=RS_qmaxDTU;
              SL_PointMin.Dp :=RoundTo(fde(RS_qmaxDTU,_Caisson.Ventil_PressionConstante.MinA_x3,_Caisson.Ventil_PressionConstante.MinA_x2,_Caisson.Ventil_PressionConstante.MinA_x1, _Caisson.Ventil_PressionConstante.MinA_x0, 18),0);
              SL_PointCalc.Dp :=RoundTo(fde(RS_qmaxDTU,_Caisson.Ventil_PressionConstante.MinA_x3,_Caisson.Ventil_PressionConstante.MinA_x2, _Caisson.Ventil_PressionConstante.MinA_x1, _Caisson.Ventil_PressionConstante.MinA_x0 + RL_Reglage * Power(10,18),18),0);
              SL_PointMax.Dp :=RoundTo(fde(RS_qmaxDTU,_Caisson.Ventil_PressionConstante.EA_x3,_Caisson.Ventil_PressionConstante.EA_x2,_Caisson.Ventil_PressionConstante.EA_x1,_Caisson.Ventil_PressionConstante.EA_x0,18),0);
                If (SL_PointMin.Dp<=SL_PointCalc.Dp) and (SL_PointCalc.Dp<=SL_PointMax.Dp) then
                  begin
                  RS_DPTrouveeQMax :=SL_PointCalc.Dp;
                  BL_Trouve := True;

                    if RS_qminDTU<>0 then
                      begin
                        if RS_qminDTU>=Str2Float(CourbeDebit[0]) then
                          begin
                          SL_PointMin.Q:=RS_qminDTU;
                          SL_PointMax.Q:=RS_qminDTU;
                          SL_PointCalc.Q:=RS_qminDTU;
                          SL_PointMin.Dp:=RoundTo(fde(RS_qminDTU,_Caisson.Ventil_PressionConstante.MinA_x3,_Caisson.Ventil_PressionConstante.MinA_x2,_Caisson.Ventil_PressionConstante.MinA_x1,_Caisson.Ventil_PressionConstante.MinA_x0,18),0);
                          SL_PointCalc.Dp:=RoundTo(fde(RS_qminDTU,_Caisson.Ventil_PressionConstante.MinA_x3,_Caisson.Ventil_PressionConstante.MinA_x2,_Caisson.Ventil_PressionConstante.MinA_x1,_Caisson.Ventil_PressionConstante.MinA_x0+RL_Reglage*Power(10,18),18),0);
                          SL_PointMax.Dp:=RoundTo(fde(RS_qminDTU,_Caisson.Ventil_PressionConstante.EA_x3,_Caisson.Ventil_PressionConstante.EA_x2,_Caisson.Ventil_PressionConstante.EA_x1,_Caisson.Ventil_PressionConstante.EA_x0,18),0);
                          RS_DPTrouveeQmin:=SL_PointCalc.Dp; //; RS_PTrouv�eQmin=SL_PointCalc:P
                          BL_trouve:=True;// stop
                          end
                        else
                          begin
                          BL_trouve := False;
                          end;
                      end;
                  end;
              end;
          end;
      end;

    If BL_trouve then
      RS_PTrouveeQmax := 0;


    For EL_ind := 0 to CaissonGetNbPoints(_Caisson) - 2 do
      begin
        if (Str2Float(_Caisson.ListeDebitsMaxis[EL_ind])<=RS_qmaxRT) and (RS_qmaxRT<=Str2Float(_Caisson.ListeDebitsMaxis[EL_ind + 1])) then
          begin
          SL_PointMin.Q:=RS_qmaxRT;
          SL_PointMax.Q:=RS_qmaxRT;
          SL_PointCalc.Q:=RS_qmaxRT;
          SL_PointMin.Dp:=RoundTo(fde(RS_qmaxRT,_Caisson.Ventil_PressionConstante.MinA_x3,_Caisson.Ventil_PressionConstante.MinA_x2,_Caisson.Ventil_PressionConstante.MinA_x1,_Caisson.Ventil_PressionConstante.MinA_x0,18),0);
          SL_PointCalc.Dp:=RoundTo(fde(RS_qmaxRT,_Caisson.Ventil_PressionConstante.MinA_x3,_Caisson.Ventil_PressionConstante.MinA_x2,_Caisson.Ventil_PressionConstante.MinA_x1,_Caisson.Ventil_PressionConstante.MinA_x0+RL_Reglage*Power(10,18),18),0);
          SL_PointMax.Dp:=RoundTo(fde(RS_qmaxRT,_Caisson.Ventil_PressionConstante.EA_x3,_Caisson.Ventil_PressionConstante.EA_x2,_Caisson.Ventil_PressionConstante.EA_x1,_Caisson.Ventil_PressionConstante.EA_x0,18),0);
          SL_PointMin.P:=RoundTo(fde(RS_qmaxRT,_Caisson.Ventil_PressionConstante.MinP_x3,_Caisson.Ventil_PressionConstante.MinP_x2,_Caisson.Ventil_PressionConstante.MinP_x1,_Caisson.Ventil_PressionConstante.MinP_x0,18),0);
          SL_PointMax.P:=RoundTo(fde(RS_qmaxRT,_Caisson.Ventil_PressionConstante.EP_x3,_Caisson.Ventil_PressionConstante.EP_x2,_Caisson.Ventil_PressionConstante.EP_x1,_Caisson.Ventil_PressionConstante.EP_x0,18),0);
          Calcule_Puissance(SL_PointMin, SL_PointCalc, SL_PointMax);
          RS_PTrouveeQmax:=Min(SL_PointCalc.P,SL_PointMax.P);
            if RS_qminRT<>0 then
              begin
                if RS_qminRT>=Str2Float(CourbeDebit[0]) then
                  begin
                  SL_PointMin.Q:=RS_qminRT;
                  SL_PointMax.Q:=RS_qminRT;
                  SL_PointCalc.Q:=RS_qminRT;
                  SL_PointMin.Dp:=fde(RS_qminRT,_Caisson.Ventil_PressionConstante.MinA_x3,_Caisson.Ventil_PressionConstante.MinA_x2,_Caisson.Ventil_PressionConstante.MinA_x1,_Caisson.Ventil_PressionConstante.MinA_x0,18);
                  SL_PointCalc.Dp:=fde(RS_qminRT,_Caisson.Ventil_PressionConstante.MinA_x3,_Caisson.Ventil_PressionConstante.MinA_x2,_Caisson.Ventil_PressionConstante.MinA_x1,_Caisson.Ventil_PressionConstante.MinA_x0+RL_Reglage*Power(10,18),18);
                  SL_PointMax.Dp:=fde(RS_qminRT,_Caisson.Ventil_PressionConstante.EA_x3,_Caisson.Ventil_PressionConstante.EA_x2,_Caisson.Ventil_PressionConstante.EA_x1,_Caisson.Ventil_PressionConstante.EA_x0,18);
                  SL_PointMin.P:=RoundTo(fde(RS_qminRT,_Caisson.Ventil_PressionConstante.MinP_x3,_Caisson.Ventil_PressionConstante.MinP_x2,_Caisson.Ventil_PressionConstante.MinP_x1,_Caisson.Ventil_PressionConstante.MinP_x0,18),0);
                  SL_PointMax.P:=RoundTo(fde(RS_qminRT,_Caisson.Ventil_PressionConstante.EP_x3,_Caisson.Ventil_PressionConstante.EP_x2,_Caisson.Ventil_PressionConstante.EP_x1,_Caisson.Ventil_PressionConstante.EP_x0,18),0);
                  Calcule_Puissance(SL_PointMin, SL_PointCalc, SL_PointMax);
                  RS_PTrouveeQmin:=Min(SL_PointCalc.P,SL_PointMax.P);
                  end;
              end;
          end;
      end;


  if RS_PTrouveeQmax=0 then
    BL_trouve:=False;

  Result := BL_trouve;

//  if Not(Result) then
//    Exit;

Result := True;

_Caisson.Selection11Points :=  RadioBezier.ItemIndex = 1;
_Caisson.DpCalcQMax  := TVentil_Caisson(CaissonReseau).DPVentilateur[ttc20Deg].MaxAQmax;
_Caisson.DpCalcQMin  := TVentil_Caisson(CaissonReseau).DPVentilateur[ttc20Deg].MinAQmax;

_Caisson.Position := '';

_Caisson.CoeffRT := TVentil_Caisson(TVentil_Caisson(CaissonReseau)).CoeffRt(_Caisson, CourbeDebit, CourbePuissance);

_Caisson.PuissCalcQMax  := RS_PTrouveeQmax;
_Caisson.PuissCalcQMin  := RS_PTrouveeQMin;


//_Caisson.CourbeDpCalc    := CourbeDP;
_Caisson.CourbeDebitCalc := CourbeDebit;
_Caisson.CourbePuissCalc := CourbePuissance;

  _Caisson.CourbeDebitCalcCourbeMontante := TStringList.Create;

  _Caisson.CourbeDebitCalcCourbeMontante.Clear;
  _Caisson.CourbeDebitCalcCourbeMontante.Add(Float2Str(RS_qminDTU, 2));
  _Caisson.CourbeDebitCalcCourbeMontante.Add(Float2Str((RS_qminDTU + RS_qmaxDTU) / 2, 2));
  _Caisson.CourbeDebitCalcCourbeMontante.Add(Float2Str((RS_qminDTU + RS_qmaxDTU) / 2, 2));
  _Caisson.CourbeDebitCalcCourbeMontante.Add(Float2Str(RS_qmaxDTU, 2));


  _Caisson.CourbeDpCalc := TStringList.Create;
  _Caisson.CourbeDpCalc.Clear;
  _Caisson.CourbeDpCalc.Add(Float2Str(TVentil_Caisson(CaissonReseau).DPVentilateur[ttc20Deg].MinAQmax, 2));
  _Caisson.CourbeDpCalc.Add(Float2Str(TVentil_Caisson(CaissonReseau).DPVentilateur[ttc20Deg].MinAQmax, 2));
  _Caisson.CourbeDpCalc.Add(Float2Str(TVentil_Caisson(CaissonReseau).DPVentilateur[ttc20Deg].MaxAQmax, 2));
  _Caisson.CourbeDpCalc.Add(Float2Str(TVentil_Caisson(CaissonReseau).DPVentilateur[ttc20Deg].MaxAQmax, 2));

end;
{

var
  RS_qminDTU : Single;
  RS_qmaxDTU : Single;
  RS_qminRT : Single;
  RS_qmaxRT : Single;
begin

  RS_qminRT         := TVentil_Caisson(CaissonReseau).DebitMiniRT;
  RS_qmaxRT         := TVentil_Caisson(CaissonReseau).DebitMaxiRT;
  RS_qminDTU        := TVentil_Caisson(CaissonReseau).Reseau_DebitMinimum;
  RS_qmaxDTU        := TVentil_Caisson(CaissonReseau).Reseau_DebitMaximum;

   Result := Caisson_Valide_SelectionDebitConstant(_Caisson, False);
//   Result := RS_qmaxDTU < _Caisson.DebitMaxiLimite;
   Result := True;
    if Not(Result) then
      Exit;

_Caisson.Selection11Points :=  RadioBezier.ItemIndex = 1;

_Caisson.DpCalcQMax  := TVentil_Caisson(CaissonReseau).DPVentilateur[ttc20Deg].MaxAQmax;
_Caisson.DpCalcQMin  := TVentil_Caisson(CaissonReseau).DPVentilateur[ttc20Deg].MinAQmax;

_Caisson.Position := '';

//_Caisson.CoeffRT := TVentil_Caisson(TVentil_Caisson(CaissonReseau)).CoeffRt(_Caisson, CourbeDebit, CourbePuissance);

//_Caisson.PuissCalcQMax  := RS_PTrouveeQmax;
//_Caisson.PuissCalcQMin  := RS_PTrouveeQMin;



  _Caisson.CourbeDebitCalcCourbeMontante := TStringList.Create;

  _Caisson.CourbeDebitCalcCourbeMontante.Clear;
  _Caisson.CourbeDebitCalcCourbeMontante.Add(Float2Str(TVentil_Caisson(CaissonReseau).Reseau_DebitMinimum, 2));
  _Caisson.CourbeDebitCalcCourbeMontante.Add(Float2Str((TVentil_Caisson(CaissonReseau).Reseau_DebitMinimum + TVentil_Caisson(CaissonReseau).Reseau_DebitMaximum) / 2, 2));
  _Caisson.CourbeDebitCalcCourbeMontante.Add(Float2Str((TVentil_Caisson(CaissonReseau).Reseau_DebitMinimum + TVentil_Caisson(CaissonReseau).Reseau_DebitMaximum) / 2, 2));
  _Caisson.CourbeDebitCalcCourbeMontante.Add(Float2Str(TVentil_Caisson(CaissonReseau).Reseau_DebitMaximum, 2));


  _Caisson.CourbeDpCalc := TStringList.Create;
  _Caisson.CourbeDpCalc.Clear;
  _Caisson.CourbeDpCalc.Add(Float2Str(TVentil_Caisson(CaissonReseau).DPVentilateur[ttc20Deg].MinAQmax, 2));
  _Caisson.CourbeDpCalc.Add(Float2Str(TVentil_Caisson(CaissonReseau).DPVentilateur[ttc20Deg].MinAQmax, 2));
  _Caisson.CourbeDpCalc.Add(Float2Str(TVentil_Caisson(CaissonReseau).DPVentilateur[ttc20Deg].MaxAQmax, 2));
  _Caisson.CourbeDpCalc.Add(Float2Str(TVentil_Caisson(CaissonReseau).DPVentilateur[ttc20Deg].MaxAQmax, 2));


  _Caisson.CourbeDebitCalc := TStringList.Create;

  _Caisson.CourbeDebitCalc.Clear;
  _Caisson.CourbeDebitCalc.Add(Float2Str(TVentil_Caisson(CaissonReseau).Reseau_DebitMinimum, 2));
  _Caisson.CourbeDebitCalc.Add(Float2Str((TVentil_Caisson(CaissonReseau).Reseau_DebitMinimum + TVentil_Caisson(CaissonReseau).Reseau_DebitMaximum) / 2, 2));
  _Caisson.CourbeDebitCalc.Add(Float2Str((TVentil_Caisson(CaissonReseau).Reseau_DebitMinimum + TVentil_Caisson(CaissonReseau).Reseau_DebitMaximum) / 2, 2));
  _Caisson.CourbeDebitCalc.Add(Float2Str(TVentil_Caisson(CaissonReseau).Reseau_DebitMaximum, 2));

  _Caisson.CourbePuissCalc := TStringList.Create;
  _Caisson.CourbePuissCalc.Clear;
  _Caisson.CourbePuissCalc.Add(Float2Str(TVentil_Caisson(CaissonReseau).DPVentilateur[ttc20Deg].MinAQmax, 2));
  _Caisson.CourbePuissCalc.Add(Float2Str(TVentil_Caisson(CaissonReseau).DPVentilateur[ttc20Deg].MinAQmax, 2));
  _Caisson.CourbePuissCalc.Add(Float2Str(TVentil_Caisson(CaissonReseau).DPVentilateur[ttc20Deg].MaxAQmax, 2));
  _Caisson.CourbePuissCalc.Add(Float2Str(TVentil_Caisson(CaissonReseau).DPVentilateur[ttc20Deg].MaxAQmax, 2));

  _Caisson.CoeffRT  := TVentil_Caisson(TVentil_Caisson(CaissonReseau)).CoeffRt(_Caisson, _Caisson.CourbeDebitCalc, _Caisson.CourbePuissCalc);

end;
}
{ **************************************************************************************************************************************************** }
Function  TDlg_SelectionCaisson.Caisson_Valide_SelectionPlageFonctionnementEtCourbeLimite(_Caisson: TEdibatec_Caisson) : Boolean;
Var
        CaissonValide : Boolean;
        DpCourbeAQmin: Real;
        DpCourbeAQmax: Real;
        m_PointQMinDpMax       : TPointDebitPression;
        m_PointQMaxDpMax       : TPointDebitPression;
        PtsBezier              : TPointsControleBezier;
Begin
Result := Caisson_Valide_SelectionPlageFonctionnement(_Caisson);

        if Result = false then
                exit;

m_PointQMinDpMax.Q  := TVentil_Caisson(TVentil_Caisson(CaissonReseau)).DebitMini;
m_PointQMinDpMax.DP := TVentil_Caisson(TVentil_Caisson(CaissonReseau)).DPVentilateur[ttc20Deg].MaxAQmin;
m_PointQMaxDpMax.Q  := TVentil_Caisson(TVentil_Caisson(CaissonReseau)).DebitMaxi;
m_PointQMaxDpMax.DP := TVentil_Caisson(TVentil_Caisson(CaissonReseau)).DPVentilateur[ttc20Deg].MaxAQmax;

//modif dtu
//        if not CheckBox_DTU.Checked then
                Begin
                m_PointQMinDpMax.DP := m_PointQMinDpMax.DP;// - 20;
                m_PointQMaxDpMax.DP := m_PointQMaxDpMax.DP;// - 20;
                End;

PtsBezier := StringToPointsBezier_Xpoints(_Caisson.ListeDebitsLimites, _Caisson.ListeDPLimites);
DpCourbeAQMax := CalculDpCourbe(m_PointQMaxDpMax, PtsBezier);
DpCourbeAQMin := CalculDpCourbe(m_PointQMinDpMax, PtsBezier);

CaissonValide :=  Not IsNan(DpCourbeAQMax) And (DpCourbeAQMax <> 0) and (_Caisson.DpCalcQMax <= DpCourbeAQMax);
        if (Etude.Batiment.TypeBat <> c_BatimentTertiaire) then
                CaissonValide :=  CaissonValide and (_Caisson.DpCalcQMin >= DpCourbeAQMin);

        if CaissonValide then
                Begin
                Result := true;
                _Caisson.DpCalcQMax  := DpCourbeAQMax;
                _Caisson.DpCalcQMin  := DpCourbeAQMin;

                _Caisson.CourbeDpCalc    := _Caisson.ListeDPLimites;
                _Caisson.CourbeDebitCalc := _Caisson.ListeDebitsLimites;
//manque la courbe PuissancesLimites 
                _Caisson.CourbePuissCalc := _Caisson.ListePuissMinis;
                _Caisson.CourbeDebitCalcCourbeMontante := TStringList.Create;
                _Caisson.CourbeDebitCalcCourbeMontante.Clear;
                End;
End;
{ **************************************************************************************************************************************************** }
Function  TDlg_SelectionCaisson.Caisson_Valide_SelectionCalculActhys(_Caisson: TEdibatec_Caisson) : Boolean;
  function CalculePressionFonctionnement(_Q : Real) : Real;
  begin
{$IfDef OLD_ATVENTIL}
    Result := TVentil_Caisson(TVentil_Caisson(CaissonReseau)).GetPression(_Q);
{$Else}
    Result := TVentil_Caisson(TVentil_Caisson(CaissonReseau)).GetPression(_Q, _Caisson.ClasseCDEP);
{$EndIf}
  end;
begin

    Result := True;

      if YearOf(Etude.Batiment.SystemeCollectif.Date) >= 2016 then
        begin
          if (Pos('MATRYS1200', UpperCase(_Caisson.CodeArticle)) = 1) then
            begin
            Result := False;
            end;
        end
      else
        begin
        Result := False;
          if (Pos('MATRYS1200', UpperCase(_Caisson.CodeArticle)) = 1) or (Pos('MATRYS2500', UpperCase(_Caisson.CodeArticle)) = 1) then
            begin
            Result := True;
            end;
        end;

      if Result = False then
        Exit;


    _Caisson.CoeffRT := TVentil_Caisson(TVentil_Caisson(CaissonReseau)).CoeffRtActhys(_Caisson);

    _Caisson.DpCalcQMax := CalculePressionFonctionnement(TVentil_Caisson(TVentil_Caisson(CaissonReseau)).Reseau_DebitMaximum);
    _Caisson.DpCalcQMin := CalculePressionFonctionnement(TVentil_Caisson(TVentil_Caisson(CaissonReseau)).Reseau_DebitMinimum);


end;
{ **************************************************************************************************************************************************** }
Function TDlg_SelectionCaisson.Caisson_Valide_Fonctionnement(_Caisson: TEdibatec_Caisson) : Boolean;
Begin
  Case _Caisson.TypeSelection Of
    c_CaissonSelect_3Courbes            : Result := Caisson_Valide_Selection3Courbes(_Caisson);
    c_CaissonSelect_PageFct             : Result := Caisson_Valide_SelectionPlageFonctionnement(_Caisson);
    c_CaissonSelect_DebitCst            : if Etude.Batiment.IsFabricantVIM or Etude.Batiment.IsFabricantMVN then
                                            Result := Caisson_Valide_SelectionDebitConstant(_Caisson)
                                          else
//                                            Result := Caisson_Valide_SelectionDebitConstant(_Caisson);
                                            Result := Caisson_Valide_SelectionDebitConstantOld(_Caisson);
    c_CaissonSelect_PlageEtCourbeLimite : if Etude.Batiment.IsFabricantMVN then
                                            Result := Caisson_Valide_SelectionMVNCourbeMontante(_Caisson)
                                          else
                                            Result := Caisson_Valide_SelectionPlageFonctionnementEtCourbeLimite(_Caisson);
    c_CaissonSelect_CalculActhys        : Result := Caisson_Valide_SelectionCalculActhys(_Caisson);
    Else Result := False;
  End;
End;
{ **************************************************************************************************************************************************** }
Procedure TDlg_SelectionCaisson.AffDebitsCaisson(_Caisson: TVentil_Troncon);
  Procedure Initialisation_Panneau;
  begin
  Label_Lib_1.Caption   := 'D�bit mini';
  Label_Lib_2.Caption   := 'D�bit maxi';
  Label_Lib_3.Caption   := 'D�bit limite';
  Label_Lib_4.Caption   := '';
  Label_Lib_5.Caption   := '';
  Label_Lib_6.Caption   := 'Dp min';
  Label_Lib_7.Caption   := 'Dp min';
  Label_Lib_8.Caption   := 'Dp vent';
  Label_Lib_9.Caption   := 'Dp rejet';
  Label_Lib_10.Caption  := '';
  end;

  Procedure Finalisation_Panneau(_LabelValReference_Help_DPVent : TLabel = Nil);
  begin
  Label_Sep_1.Visible   := Label_Lib_1.caption <> '';
  Label_Sep_2.Visible   := Label_Lib_2.caption <> '';
  Label_Sep_3.Visible   := Label_Lib_3.caption <> '';
  Label_Sep_4.Visible   := Label_Lib_4.caption <> '';
  Label_Sep_5.Visible   := Label_Lib_5.caption <> '';
  Label_Sep_6.Visible   := Label_Lib_6.caption <> '';
  Label_Sep_7.Visible   := Label_Lib_7.caption <> '';
  Label_Sep_8.Visible   := Label_Lib_8.caption <> '';
  Label_Sep_9.Visible   := Label_Lib_9.caption <> '';
  Label_Sep_10.Visible  := Label_Lib_10.caption <> '';

  Label_Val_1.Visible   := Label_Lib_1.caption <> '';
  Label_Val_2.Visible   := Label_Lib_2.caption <> '';
  Label_Val_3.Visible   := Label_Lib_3.caption <> '';
  Label_Val_4.Visible   := Label_Lib_4.caption <> '';
  Label_Val_5.Visible   := Label_Lib_5.caption <> '';
  Label_Val_6.Visible   := Label_Lib_6.caption <> '';
  Label_Val_7.Visible   := Label_Lib_7.caption <> '';
  Label_Val_8.Visible   := Label_Lib_8.caption <> '';
  Label_Val_9.Visible   := Label_Lib_9.caption <> '';
  Label_Val_10.Visible  := Label_Lib_10.caption <> '';

    if _LabelValReference_Help_DPVent <> Nil then
      begin
      Image_DPVent.Visible  := _LabelValReference_Help_DPVent.Visible;
      Image_DPVent.Top      := _LabelValReference_Help_DPVent.Top;
      Image_DPVent.Left     := _LabelValReference_Help_DPVent.Left + _LabelValReference_Help_DPVent.Width + 15;
      end
    else
      Image_DPVent.Visible := False;

  end;

  Procedure Remplissage_ACT(Var _LabelValReference_Help_DPVent : TLabel);
  var
    AfficheMinAQMin : Real;
    AfficheMinAQMax : Real;
  begin
    if Etude.Batiment.TypeBat = c_BatimentTertiaire then
      Label_Val_1.Caption := '--- m�/h'
    else
      Label_Val_1.Caption := Float2Str(TVentil_Caisson(_Caisson).Reseau_DebitMinimum, 0) + ' m�/h';

  Label_Val_2.Caption := Float2Str(TVentil_Caisson(_Caisson).Reseau_DebitMaximum, 0) + ' m�/h';

  AfficheMinAQMin := TVentil_Caisson(_Caisson).DPVentilateur[ttc20Deg].MaxAQmin;
  AfficheMinAQMax := TVentil_Caisson(_Caisson).DPVentilateur[ttc20Deg].MaxAQmax;
  Label_Lib_6.Caption := 'Pmin :';
  Label_Lib_7.Caption := 'Pmax :';
  Label_Val_6.Caption := IntToStr(Ceil(AfficheMinAQMin)) + ' Pa';
  Label_Val_7.Caption := IntToStr(Ceil(AfficheMinAQMax)) + ' Pa';

    if Not(Etude.Batiment.IsFabricantACT) then

      Label_Lib_3.Caption := '';
  Label_Val_3.Caption := Float2Str(_Caisson.DebitLimite, 0) + ' m�/h';

  Label_Lib_8.Caption := '';
  Label_Lib_9.Caption := '';

  _LabelValReference_Help_DPVent := Nil;
  end;

  procedure Remplissage_PNO(Var _LabelValReference_Help_DPVent : TLabel);
  begin
  Label_Lib_1.Left  := 15;
  Label_Lib_2.Left  := Label_Lib_1.Left;
  Label_Lib_3.Left  := Label_Lib_1.Left;
  Label_Lib_4.Left  := Label_Lib_1.Left;
  Label_Lib_5.Left  := Label_Lib_1.Left;

  Label_Lib_6.Left  := 280;
  Label_Lib_7.Left  := Label_Lib_6.Left;
  Label_Lib_8.Left  := Label_Lib_6.Left;
  Label_Lib_9.Left  := Label_Lib_6.Left;
  Label_Lib_10.Left := Label_Lib_6.Left;



  Label_Lib_1.Caption := 'D�bit mini avec fuite';
    if Etude.Batiment.TypeBat = c_BatimentTertiaire then
      Label_Val_1.Caption := '--- m�/h'
    else
      Label_Val_1.Caption := Float2Str(TVentil_Caisson(_Caisson).Reseau_DebitMinimum, 0) + ' m�/h';

  Label_Lib_2.Caption := '';

  Label_Lib_3.Caption := 'D�bit maxi avec fuite';
  Label_Val_3.Caption := Float2Str(TVentil_Caisson(_Caisson).Reseau_DebitMaximum, 0) + ' m�/h';

  Label_Lib_4.Caption := '';

  Label_Lib_5.Caption := 'Dp vent';
  Label_Val_5.Caption := Float2Str(TVentil_Caisson(_Caisson).Get_PerteChargeVentDT2014, 0) + ' Pa';

  Label_Lib_6.Caption := 'PdC mini � d�bit mini';
  Label_Val_6.Caption := Float2Str(TVentil_Caisson(_Caisson).Reseau_PdcVentil[ttc20Deg].MinAQmin, 1) + ' Pa';
  Label_Lib_7.Caption := 'PdC maxi � d�bit mini';
  Label_Val_7.Caption := Float2Str(TVentil_Caisson(_Caisson).Reseau_PdcVentil[ttc20Deg].MaxAQmin, 1) + ' Pa';
  Label_Lib_8.Caption := 'PdC mini � d�bit maxi';
  Label_Val_8.Caption := Float2Str(TVentil_Caisson(_Caisson).Reseau_PdcVentil[ttc20Deg].MinAQmax, 1) + ' Pa';
  Label_Lib_9.Caption := 'PdC maxi � d�bit maxi';
  Label_Val_9.Caption := Float2Str(TVentil_Caisson(_Caisson).Reseau_PdcVentil[ttc20Deg].MaxAQmax, 1) + ' Pa';

  Label_Lib_10.Caption := 'Dp rejet (obstacle)';
  Label_Val_10.Caption := Float2Str(TVentil_Caisson(_Caisson).Get_PerteChargeObstacleRejetDT2014, 0) + ' Pa';

  _LabelValReference_Help_DPVent := Label_Val_5;
  end;

  procedure Remplissage_All(Var _LabelValReference_Help_DPVent : TLabel);
  var
    AfficheMinAQMin : Real;
    AfficheMinAQMax : Real;
  begin

    if Etude.Batiment.TypeBat = c_BatimentTertiaire then
      Label_Val_1.Caption := '--- m�/h'
    else
      Label_Val_1.Caption := Float2Str(TVentil_Caisson(_Caisson).Reseau_DebitMinimum, 0) + ' m�/h';

  Label_Val_2.Caption := Float2Str(TVentil_Caisson(_Caisson).Reseau_DebitMaximum, 0) + ' m�/h';

    if Etude.Batiment.IsFabricantMVN then
      begin
      AfficheMinAQMin := TVentil_Caisson(_Caisson).DPVentilateur[ttc20Deg].MaxAQmin;
      AfficheMinAQMax := TVentil_Caisson(_Caisson).DPVentilateur[ttc20Deg].MaxAQmax;
      end
    else
      begin
      AfficheMinAQMin := TVentil_Caisson(_Caisson).DPVentilateur[ttc20Deg].MinAQmin;
      AfficheMinAQMax := TVentil_Caisson(_Caisson).DPVentilateur[ttc20Deg].MinAQmax;
      end;

  Label_Val_6.Caption := Float2Str(AfficheMinAQMin, 0) + ' Pa';
  Label_Val_7.Caption := Float2Str(AfficheMinAQMax, 0) + ' Pa';
  Label_Lib_3.Caption := '';

    if Etude.Batiment.IsFabricantMVN then
      begin
      Label_Lib_8.Caption := '';
      Label_Lib_9.Caption := '';
      end;

    if Etude.DTUActive then
      begin
      Label_Val_8.Caption := Float2Str(TVentil_Caisson(_Caisson).Get_PerteChargeVentDT2014, 0) + ' Pa';
      Label_Val_9.Caption := Float2Str(TVentil_Caisson(_Caisson).Get_PerteChargeObstacleRejetDT2014, 0) + ' Pa';
      end
    else
      begin
      Label_Val_8.Caption := '0 Pa';
      Label_Val_9.Caption := '0 Pa';
      end;

  _LabelValReference_Help_DPVent := Label_Val_8;

  end;

var
  LabelValReference_Help_DPVent : TLabel;
Begin

  Initialisation_Panneau;

    if Etude.Batiment.IsFabricantACT then
      Remplissage_ACT(LabelValReference_Help_DPVent)
    else
    if Etude.Batiment.IsFabricantPNO then
      Remplissage_PNO(LabelValReference_Help_DPVent)
    else
      Remplissage_All(LabelValReference_Help_DPVent);

  Finalisation_Panneau(LabelValReference_Help_DPVent);
End;
{ **************************************************************************************************************************************************** }
Procedure TDlg_SelectionCaisson.Grid_SelectionSelectCell(Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
Begin
  CaissonSelection := TEdibatec_Caisson(Grid_Selection.Objects[0, ARow]);
End;
{ **************************************************************************************************************************************************** }
Procedure TDlg_SelectionCaisson.Grid_SelectionRowChanging(Sender: TObject; OldRow, NewRow: Integer; var Allow: Boolean);
Begin
{
  If Grid_Selection.Objects[0, NewRow] <> Nil Then Begin
    If Not Label_DebitMini.Visible Then Label_DebitMini.Show;
    If Not Label_DebitMaxi.Visible Then Label_DebitMaxi.Show;
      if Etude.Batiment.IsFabricantACT then
        If Not Label_DebitLimite.Visible Then Label_DebitLimite.Show;
  End Else Begin
    Label_DebitMini.Hide;
    Label_DebitMaxi.Hide;
    Label_DebitLimite.Hide;
  End;
}
End;
{ **************************************************************************************************************************************************** }
Function TDlg_SelectionCaisson.IsProduitASSOPAPV(m_ProduitAsso : TEdibatec_Association) : Boolean;
var
        m_NoPAPV : Integer;
Begin
result := false;
if m_ProduitAsso = nil then exit;

        for m_NoPAPV := Low(BaseEdibatec.Assistant.Tab_PAPV) to High(BaseEdibatec.Assistant.Tab_PAPV) do
                If m_ProduitAsso.VraiProduit.CodeProduit = BaseEdibatec.Assistant.Tab_PAPV[m_NoPAPV] Then
                        Begin
                        Result := true;
                        exit;
                        End;

End;
procedure TDlg_SelectionCaisson.PageControl1Change(Sender: TObject);
begin

end;

{ **************************************************************************************************************************************************** }
Function TDlg_SelectionCaisson.IsCaissonSelectionFromGammeHUCF : boolean;
Begin
result := false;
if CaissonSelection = nil then exit;

        if Copy(CaissonSelection.CodeProduit, 1, 12) = BaseEdibatec.Assistant.Gamme('cst_CaissonGammeHUCF') then
                result := true;
End;
{ **************************************************************************************************************************************************** }
Function TDlg_SelectionCaisson.IsCaissonSelectionFromGammeJBEBorJBHBorJBEBECO(_Caisson: TEdibatec_Caisson) : boolean;
Begin
result := false;
if _Caisson = nil then exit;

        if (Copy(_Caisson.CodeProduit, 1, 12) = BaseEdibatec.Assistant.Gamme('cst_CaissonGammeJBEBJBHBJBEBECO1')) or (Copy(_Caisson.CodeProduit, 1, 12) = BaseEdibatec.Assistant.Gamme('cst_CaissonGammeJBEBJBHBJBEBECO2')) then
                result := true;
End;
{ **************************************************************************************************************************************************** }
Procedure TDlg_SelectionCaisson.Grid_Selection_TrieCol0;
var
  cpt : integer;
begin
    if etude.Batiment.IsFabricantPNO then
      begin
      Grid_Selection.BeginUpdate;
        For cpt := 1 To Grid_Selection.RowCount - 1 Do
            Grid_Selection.Cells[0, cpt] := Float2Str(TEdibatec_Caisson(Grid_Selection.Objects[0, cpt]).DebitMaximalVMax, 2);
      Grid_Selection.Sort(0, Grid_Selection.SortSettings.Direction);
        For cpt := 1 To Grid_Selection.RowCount - 1 Do
            Grid_Selection.Cells[0, cpt] := TEdibatec_Caisson(Grid_Selection.Objects[0, cpt]).Reference;
      Grid_Selection.EndUpdate;
      end
    else
      Grid_Selection.SortByColumn(0);

end;
{ **************************************************************************************************************************************************** }
Procedure TDlg_SelectionCaisson.AffecteAssociations;
Var
//  I: Integer;
  m_ProduitAsso: TEdibatec_Association;
  m_NoAsso     : Integer;
  m_CurRow     : Integer;
  m_Col, m_Row : Integer;
Begin
  CaissonSelection.GetAssociation;
  For m_Row := 1 To Grid_Accessoires.RowCount - 1 Do Begin
   Grid_Accessoires.Objects[0, m_Row] := nil;
   For m_Col := 0 To Grid_Accessoires.ColCount - 1 Do Grid_Accessoires.Cells[m_Col, m_Row] := '';
  End;
  Grid_Accessoires.RowCount := 2;
  m_CurRow := 1;
  For m_NoAsso := 0 To CaissonSelection.Associations.Count - 1 Do Begin
    m_ProduitAsso := TEdibatec_Association(CaissonSelection.Associations.Produit(m_NoAsso));
    If m_CurRow >= Grid_Accessoires.RowCount Then Grid_Accessoires.RowCount := Grid_Accessoires.RowCount + 1;
    Grid_Accessoires.Objects[0, m_CurRow] := m_ProduitAsso;
    Grid_Accessoires.Cells[0, m_CurRow] := m_ProduitAsso.ProduitAssocie;
    Grid_Accessoires.Cells[1, m_CurRow] := m_ProduitAsso.RefProduitAssocie;
    Grid_Accessoires.Ints[2, m_CurRow]  := 0;
    Inc(m_CurRow);
  End;

  TVentil_Caisson(CaissonReseau).SaveOptions_SelectionCaisson.InitOptionsAccessoiresAssociesFiche(Dlg_SelectionCaisson);

        if IsCaissonSelectionFromGammeHUCF and TVentil_Caisson(TVentil_Caisson(CaissonReseau)).HasSortieToit then
                For m_NoAsso := 1 To Grid_Accessoires.RowCount - 1 do
                        if (Grid_Accessoires.Objects[0, m_NoAsso] <> nil) and IsProduitASSOPAPV(TEdibatec_Association(Grid_Accessoires.Objects[0, m_NoAsso])) then
                                Grid_Accessoires.Ints[2, m_NoAsso]  := max(1, Grid_Accessoires.Ints[2, m_NoAsso]);

End;
{ **************************************************************************************************************************************************** }
procedure TDlg_SelectionCaisson.Grid_SelectionCanEditCell(Sender: TObject;
  ARow, ACol: Integer; var CanEdit: Boolean);
begin
  CanEdit := False
end;
{ **************************************************************************************************************************************************** }
procedure TDlg_SelectionCaisson.Grid_SelectionClickSort(Sender: TObject;
  ACol: Integer);
var
  cpt : integer;
begin
  Inherited;

    if (ACol = 0) then
      Grid_Selection_TrieCol0;

end;
{ **************************************************************************************************************************************************** }
Procedure TDlg_SelectionCaisson.Grid_InfosCaissonCanEditCell(Sender: TObject; ARow, ACol: Integer; var CanEdit: Boolean);
Begin
  CanEdit := (Grid_Accessoires.Objects[0, Arow] <> nil) And (ACol = 2);
End;
{ **************************************************************************************************************************************************** }
Procedure TDlg_SelectionCaisson.Radio_ColliersClick(Sender: TObject);
Begin
  Radio_ColliersSupp.Enabled := Radio_Colliers.ItemIndex > 0;
End;
{ **************************************************************************************************************************************************** }
Procedure TDlg_SelectionCaisson.Grid_ColliersSuppIsFixedCell(Sender: TObject; ARow, ACol: Integer; var IsFixed: Boolean);
Begin
  IsFixed := (ACol In [0, 2, 4, 6]) Or ( (ACol = 7) And (ARow In [3,4])) ;
End;
{ **************************************************************************************************************************************************** }
Procedure TDlg_SelectionCaisson.Grid_BavettesIsFixedCell(Sender: TObject; ARow, ACol: Integer; var IsFixed: Boolean);
Begin
  IsFixed := (ACol In [0, 2, 4, 6]);
End;
{ **************************************************************************************************************************************************** }
procedure TDlg_SelectionCaisson.Grid_TrappesVisiteIsFixedCell(Sender: TObject; ARow, ACol: Integer; var IsFixed: Boolean);
begin
  IsFixed := (ACol In [0, 2, 4, 6]) Or ( (ACol = 7) And (ARow In [3,4])) ;
end;
{ **************************************************************************************************************************************************** }
Procedure TDlg_SelectionCaisson.CheckBox_UniverselClick(Sender: TObject);
Begin
  Grid_ColliersSupp.Enabled := CheckBox_Universel.Checked;
  Edit_NbSuppUni.Visible := CheckBox_Universel.Checked;
  CheckBox_ColliersSuppIsoles.Visible := CheckBox_Universel.Checked;;
End;
{ **************************************************************************************************************************************************** }
Procedure TDlg_SelectionCaisson.CheckBox_125100Click(Sender: TObject);
Begin

  CheckBox_BandePerf.Enabled := true;

  Edit_NbSupp125100.Visible  := CheckBox_125100.Checked;
  Edit_NbSupp160100.Visible  := CheckBox_160100.Checked;
  Edit_NbSupp125300.Visible  := CheckBox_125300.Checked;
  Edit_NbSupp160300.Visible  := CheckBox_160300.Checked;
End;
{ **************************************************************************************************************************************************** }
Procedure TDlg_SelectionCaisson.CheckBox_BandePerfClick(Sender: TObject);
Begin
  Edit_NbRlxBandePerforee.Visible := CheckBox_BandePerf.Checked;
  StaticText1.Visible             := CheckBox_BandePerf.Checked;
  Text_LongueurBandePerf.Visible  := CheckBox_BandePerf.Checked;
End;
{ **************************************************************************************************************************************************** }
procedure TDlg_SelectionCaisson.CheckBox_NonAgreeClick(Sender: TObject);
begin

        if (Etude.Batiment.TypeBat = c_BatimentTertiaire) then
                if ( Etude.Batiment.TypeTertiaire = c_TertiaireVMC) and NonAgreePossible then
                        Begin
                                if not (CheckBox_40012.Checked) then
                                        Begin
                                        CheckBox_NonAgree.Checked := True;
                                        CheckBox_NonAgree.Enabled := False;
                                        End
                                Else
                                        CheckBox_NonAgree.Enabled := True;
                        End

end;
{ **************************************************************************************************************************************************** }
Constructor TSelectionCaisson_SaveOptions.Create;
Begin
CanGet := false;
End;
{ **************************************************************************************************************************************************** }
Procedure TSelectionCaisson_SaveOptions.Sauvegarder(_Donnees : TClientDataSet);
var
        TempString : String;
        cpt        : Integer;
Begin

        With _Donnees Do
                Begin
                FieldByName(UpperCase('SC_CanGet')).AsInteger := Bool2Int(CanGet);
                FieldByName(UpperCase('SC_OldReferenceCaisson')).AsWideString := Trim(OldReferenceCaisson);
                //Crit�res De S�lection Du Ventilateur
                FieldByName(UpperCase('SC_CB_Caisson_Checked')).AsInteger := Bool2Int(CheckBox_Caisson_Checked);
                FieldByName(UpperCase('SC_CB_Tourelle_Checked')).AsInteger := Bool2Int(CheckBox_Tourelle_Checked);
                FieldByName(UpperCase('SC_CB_Direct_Checked')).AsInteger := Bool2Int(CheckBox_Direct_Checked);
                FieldByName(UpperCase('SC_CB_Poulie_Checked')).AsInteger := Bool2Int(CheckBox_Poulie_Checked);
                FieldByName(UpperCase('SC_CB_UneVitesse_Checked')).AsInteger := Bool2Int(CheckBox_UneVitesse_Checked);
                FieldByName(UpperCase('SC_CBDeuxVitesses_Checked')).AsInteger := Bool2Int(CheckBoxDeuxVitesses_Checked);
                FieldByName(UpperCase('SC_CB_NonAgree_Checked')).AsInteger := Bool2Int(CheckBox_NonAgree_Checked);
                FieldByName(UpperCase('SC_CB_40012_Checked')).AsInteger := Bool2Int(CheckBox_40012_Checked);
                FieldByName(UpperCase('SC_CB_MonoPhase_Checked')).AsInteger := Bool2Int(CheckBox_MonoPhase_Checked);
                FieldByName(UpperCase('SC_CB_TriPhase_Checked')).AsInteger := Bool2Int(CheckBox_TriPhase_Checked);
//                FieldByName(UpperCase('SC_R_Depressostat_II')).AsInteger := Radio_Depressostat_ItemIndex;
//                FieldByName(UpperCase('SC_R_VentilEco_II')).AsInteger := Radio_VentilEco_ItemIndex;
//                FieldByName(UpperCase('SC_R_EcoEtDepr')).AsInteger := Radio_EcoEtDepr_ItemIndex;
                FieldByName(UpperCase('SC_CB_ECO_Checked')).AsInteger := Bool2Int(CheckBoxEco_Checked);
                FieldByName(UpperCase('SC_CB_NECOADEPR_Checked')).AsInteger := Bool2Int(CheckBoxNEcoADepr_Checked);
                FieldByName(UpperCase('SC_CB_NECOSDEPR_Checked')).AsInteger := Bool2Int(CheckBoxNEcoSDepr_Checked);

                FieldByName(UpperCase('SC_CB_DTU_Checked')).AsInteger := Bool2Int(CheckBox_DTU_Checked);
                FieldByName(UpperCase('SC_RadioBezier_ItemIndex')).AsInteger := RadioBezier_ItemIndex;
                //Accessoire Associ�s Au Ventilateur
                        TempString := '';
                                for cpt := 0 to Length(ListAccessoireVentilo_Qte) - 1 do
                                 Begin
                                 TempString := TempString + IntToStr(ListAccessoireVentilo_Qte[cpt]);
                                        if cpt <> Length(ListAccessoireVentilo_Qte) then
                                                TempString := TempString + '|';
                                 End;
                        FieldByName(UpperCase('SC_ListAccessoireVentilo_Qte')).AsWideString := TempString;
                //Accessoires Annexes
                //-->Montage
                FieldByName(UpperCase('SC_Radio_Adhesif_ItemIndex')).AsInteger := Radio_Adhesif_ItemIndex;
                FieldByName(UpperCase('SC_E_Vis13_Text')).AsWideString := Edit_Vis13_Text;
                FieldByName(UpperCase('SC_E_Vis19_Text')).AsWideString := Edit_Vis19_Text;
                FieldByName(UpperCase('SC_E_M0_Text')).AsWideString := Edit_M0_Text;
                FieldByName(UpperCase('SC_E_M1_Text')).AsWideString := Edit_M1_Text;
                FieldByName(UpperCase('SC_Check_IsolantMur_Checked')).AsInteger := Bool2Int(Check_IsolantMur_Checked);
                FieldByName(UpperCase('SC_Check_Fourreaux_Checked')).AsInteger := Bool2Int(Check_Fourreaux_Checked);
                //-->Bavettes et colliers
                FieldByName(UpperCase('SC_Radio_Colliers_ItemIndex')).AsInteger := Radio_Colliers_ItemIndex;
                FieldByName(UpperCase('SC_Bavette_OUINON')).AsInteger  := Bool2Int(Bavette_OuiNon);
                FieldByName(UpperCase('SC_Bavette_125_Qte')).AsInteger := Bavette_125_Qte;
                FieldByName(UpperCase('SC_Bavette_160_Qte')).AsInteger := Bavette_160_Qte;
                FieldByName(UpperCase('SC_Bavette_200_Qte')).AsInteger := Bavette_200_Qte;
                FieldByName(UpperCase('SC_Bavette_250_Qte')).AsInteger := Bavette_250_Qte;
                FieldByName(UpperCase('SC_Bavette_315_Qte')).AsInteger := Bavette_315_Qte;
                FieldByName(UpperCase('SC_Bavette_355_Qte')).AsInteger := Bavette_355_Qte;
                FieldByName(UpperCase('SC_Bavette_400_Qte')).AsInteger := Bavette_400_Qte;
                FieldByName(UpperCase('SC_Bavette_450_Qte')).AsInteger := Bavette_450_Qte;
                FieldByName(UpperCase('SC_Bavette_500_Qte')).AsInteger := Bavette_500_Qte;
                FieldByName(UpperCase('SC_Bavette_560_Qte')).AsInteger := Bavette_560_Qte;
                FieldByName(UpperCase('SC_Bavette_630_Qte')).AsInteger := Bavette_630_Qte;
                FieldByName(UpperCase('SC_Bavette_710_Qte')).AsInteger := Bavette_710_Qte;
                //-->Trappes de visite
                FieldByName(UpperCase('SC_Trappe_125_Qte')).AsInteger := Trappe_125_Qte;
                FieldByName(UpperCase('SC_Trappe_160_Qte')).AsInteger := Trappe_160_Qte;
                FieldByName(UpperCase('SC_Trappe_200_Qte')).AsInteger := Trappe_200_Qte;
                FieldByName(UpperCase('SC_Trappe_250_Qte')).AsInteger := Trappe_250_Qte;
                FieldByName(UpperCase('SC_Trappe_315_Qte')).AsInteger := Trappe_315_Qte;
                FieldByName(UpperCase('SC_Trappe_355_Qte')).AsInteger := Trappe_355_Qte;
                FieldByName(UpperCase('SC_Trappe_400_Qte')).AsInteger := Trappe_400_Qte;
                FieldByName(UpperCase('SC_Trappe_450_Qte')).AsInteger := Trappe_450_Qte;
                FieldByName(UpperCase('SC_Trappe_500_Qte')).AsInteger := Trappe_500_Qte;
                FieldByName(UpperCase('SC_Trappe_560_Qte')).AsInteger := Trappe_560_Qte;
                FieldByName(UpperCase('SC_Trappe_630_Qte')).AsInteger := Trappe_630_Qte;
                FieldByName(UpperCase('SC_Trappe_710_Qte')).AsInteger := Trappe_710_Qte;
                FieldByName(UpperCase('SC_Trappe_800_Qte')).AsInteger := Trappe_800_Qte;
                FieldByName(UpperCase('SC_Trappe_900_Qte')).AsInteger := Trappe_900_Qte;
                //-->Supports Universels
                FieldByName(UpperCase('SC_CB_Universel_Checked')).AsInteger := Bool2Int(CheckBox_Universel_Checked);
                FieldByName(UpperCase('SC_CB_125300_Checked')).AsInteger := Bool2Int(CheckBox_125300_Checked);
                FieldByName(UpperCase('SC_CB_160300_Checked')).AsInteger := Bool2Int(CheckBox_160300_Checked);
                FieldByName(UpperCase('SC_CB_125100_Checked')).AsInteger := Bool2Int(CheckBox_125100_Checked);
                FieldByName(UpperCase('SC_CB_160100_Checked')).AsInteger := Bool2Int(CheckBox_160100_Checked);
                FieldByName(UpperCase('SC_CB_ColliersSI_Checked')).AsInteger := Bool2Int(CheckBox_ColliersSuppIsoles_Checked);
                FieldByName(UpperCase('SC_E_NbSupp160300_Text')).AsWideString := Edit_NbSupp160300_Text;
                FieldByName(UpperCase('SC_E_NbSupp125300_Text')).AsWideString := Edit_NbSupp125300_Text;
                FieldByName(UpperCase('SC_E_NbSupp160100_Text')).AsWideString := Edit_NbSupp160100_Text;
                FieldByName(UpperCase('SC_E_NbSupp125100_Text')).AsWideString := Edit_NbSupp125100_Text;
                FieldByName(UpperCase('SC_E_NbSuppUni_Text')).AsWideString := Edit_NbSuppUni_Text;
                FieldByName(UpperCase('SC_CB_BandePerf_Checked')).AsInteger := Bool2Int(CheckBox_BandePerf_Checked);
                FieldByName(UpperCase('SC_E_NbRlxBandePerforee_Text')).AsWideString := Edit_NbRlxBandePerforee_Text;
                FieldByName(UpperCase('SC_Collier_125_Qte')).AsInteger := Collier_125_Qte;
                FieldByName(UpperCase('SC_Collier_160_Qte')).AsInteger := Collier_160_Qte;
                FieldByName(UpperCase('SC_Collier_200_Qte')).AsInteger := Collier_200_Qte;
                FieldByName(UpperCase('SC_Collier_250_Qte')).AsInteger := Collier_250_Qte;
                FieldByName(UpperCase('SC_Collier_315_Qte')).AsInteger := Collier_315_Qte;
                FieldByName(UpperCase('SC_Collier_355_Qte')).AsInteger := Collier_355_Qte;
                FieldByName(UpperCase('SC_Collier_400_Qte')).AsInteger := Collier_400_Qte;
                FieldByName(UpperCase('SC_Collier_450_Qte')).AsInteger := Collier_450_Qte;
                FieldByName(UpperCase('SC_Collier_500_Qte')).AsInteger := Collier_500_Qte;
                FieldByName(UpperCase('SC_Collier_560_Qte')).AsInteger := Collier_560_Qte;
                FieldByName(UpperCase('SC_Collier_630_Qte')).AsInteger := Collier_630_Qte;
                FieldByName(UpperCase('SC_Collier_710_Qte')).AsInteger := Collier_710_Qte;
                FieldByName(UpperCase('SC_Collier_800_Qte')).AsInteger := Collier_800_Qte;
                FieldByName(UpperCase('SC_Collier_900_Qte')).AsInteger := Collier_900_Qte;

                  if Etude.Batiment.IsFabricantACT then
                  begin
                FieldByName(UpperCase('ACT_PST')).AsInteger         := Bool2Int(ACT_PST);
                FieldByName(UpperCase('ACT_CSR')).AsInteger         := Bool2Int(ACT_CSR);
                FieldByName(UpperCase('ACT_CSR_Nb_Text')).AsWideString   := ACT_CSR_Nb_Text;
                FieldByName(UpperCase('ACT_CUT')).AsInteger         := Bool2Int(ACT_CUT);
                FieldByName(UpperCase('ACT_CUT_Nb_Text')).AsWideString   := ACT_CUT_Nb_Text;
                FieldByName(UpperCase('ACT_Trappes')).AsInteger     := Bool2Int(ACT_Trappes);
                FieldByName(UpperCase('ACT_ROA')).AsInteger         := Bool2Int(ACT_ROA);
                FieldByName(UpperCase('ACT_TeSouche')).AsInteger    := ACT_TeSouche;
                FieldByName(UpperCase('ACT_FT')).AsInteger          := Bool2Int(ACT_FT);
                FieldByName(UpperCase('ACT_JTD')).AsInteger         := Bool2Int(ACT_JTD);
                FieldByName(UpperCase('ACT_CUC')).AsInteger         := Bool2Int(ACT_CUC);
                FieldByName(UpperCase('ACT_CUC_Nb_Text')).AsWideString   := ACT_CUC_Nb_Text;
                FieldByName(UpperCase('ACT_Terminaux')).AsInteger   := ACT_Terminaux;
                FieldByName(UpperCase('ACT_LgT2A')).AsInteger       := ACT_LgT2A;
                  end;
                End;

End;
{ **************************************************************************************************************************************************** }
Procedure TSelectionCaisson_SaveOptions.Ouvrir(_Donnees : TClientDataSet);
var
        TempStringList : TStringList;
        cpt            : integer;
Begin

        With _Donnees Do
                Begin
                CanGet := Int2Bool(FieldByName(UpperCase('SC_CanGet')).AsInteger);
                OldReferenceCaisson := Trim(FieldByName(UpperCase('SC_OldReferenceCaisson')).AsWideString);
                //Crit�res De S�lection Du Ventilateur
                CheckBox_Caisson_Checked := Int2Bool(FieldByName(UpperCase('SC_CB_Caisson_Checked')).AsInteger);
                CheckBox_Tourelle_Checked := Int2Bool(FieldByName(UpperCase('SC_CB_Tourelle_Checked')).AsInteger);
                CheckBox_Direct_Checked := Int2Bool(FieldByName(UpperCase('SC_CB_Direct_Checked')).AsInteger);
                CheckBox_Poulie_Checked := Int2Bool(FieldByName(UpperCase('SC_CB_Poulie_Checked')).AsInteger);
                CheckBox_UneVitesse_Checked := Int2Bool(FieldByName(UpperCase('SC_CB_UneVitesse_Checked')).AsInteger);
                CheckBoxDeuxVitesses_Checked := Int2Bool(FieldByName(UpperCase('SC_CBDeuxVitesses_Checked')).AsInteger);
                CheckBox_NonAgree_Checked := Int2Bool(FieldByName(UpperCase('SC_CB_NonAgree_Checked')).AsInteger);
                CheckBox_40012_Checked := Int2Bool(FieldByName(UpperCase('SC_CB_40012_Checked')).AsInteger);
                CheckBox_MonoPhase_Checked := Int2Bool(FieldByName(UpperCase('SC_CB_MonoPhase_Checked')).AsInteger);
                CheckBox_TriPhase_Checked := Int2Bool(FieldByName(UpperCase('SC_CB_TriPhase_Checked')).AsInteger);
                try
//                Radio_Depressostat_ItemIndex := FieldByName(UpperCase('SC_R_Depressostat_II')).AsInteger;
//                Radio_VentilEco_ItemIndex := FieldByName(UpperCase('SC_R_VentilEco_II')).AsInteger;
//                Radio_EcoEtDepr_ItemIndex := FieldByName(UpperCase('SC_R_EcoEtDepr')).AsInteger;
                CheckBoxEco_Checked := Int2Bool(FieldByName(UpperCase('SC_CB_ECO_Checked')).AsInteger);
                CheckBoxNEcoADepr_Checked := Int2Bool(FieldByName(UpperCase('SC_CB_NECOADEPR_Checked')).AsInteger);
                CheckBoxNEcoSDepr_Checked := Int2Bool(FieldByName(UpperCase('SC_CB_NECOSDEPR_Checked')).AsInteger);
                except
                //permet de r�cup�rer les anciennes sauvegardes
//                Radio_Depressostat_ItemIndex := FieldByName(UpperCase('SC_CB_Depressostat_Checked')).AsInteger + 1;
//                Radio_VentilEco_ItemIndex := FieldByName(UpperCase('SC_CB_VentilEco_Checked')).AsInteger + 1;

                end;
                CheckBox_DTU_Checked := Int2Bool(FieldByName(UpperCase('SC_CB_DTU_Checked')).AsInteger);
                RadioBezier_ItemIndex := FieldByName(UpperCase('SC_RadioBezier_ItemIndex')).AsInteger;
                //Accessoire Associ�s Au Ventilateur
                        TempStringList := TStringList.Create;
                        TempStringList:=  StrToStringList(StringReplace(FieldByName(UpperCase('SC_ListAccessoireVentilo_Qte')).AsWideString, ' ', '', [rfReplaceAll, rfIgnoreCase]), '|');
                        SetLength(ListAccessoireVentilo_Qte, TempStringList.Count);
                                for cpt := 0 to TempStringList.Count - 1 do
                                        ListAccessoireVentilo_Qte[cpt] := Str2Int(TempStringList[cpt]);
                        FreeAndNil(TempStringList);
                //Accessoires Annexes
                //-->Montage
                Radio_Adhesif_ItemIndex := FieldByName(UpperCase('SC_Radio_Adhesif_ItemIndex')).AsInteger;
                Edit_Vis13_Text := FieldByName(UpperCase('SC_E_Vis13_Text')).AsWideString;
                Edit_Vis19_Text := FieldByName(UpperCase('SC_E_Vis19_Text')).AsWideString;
                Edit_M0_Text := FieldByName(UpperCase('SC_E_M0_Text')).AsWideString;
                Edit_M1_Text := FieldByName(UpperCase('SC_E_M1_Text')).AsWideString;
                Check_IsolantMur_Checked := Int2Bool(FieldByName(UpperCase('SC_Check_IsolantMur_Checked')).AsInteger);
                Check_Fourreaux_Checked := Int2Bool(FieldByName(UpperCase('SC_Check_Fourreaux_Checked')).AsInteger);
                //-->Bavettes et colliers
                Radio_Colliers_ItemIndex := FieldByName(UpperCase('SC_Radio_Colliers_ItemIndex')).AsInteger;
                Bavette_OuiNon  := Int2Bool(FieldByName(UpperCase('SC_Bavette_OUINON')).AsInteger);
                Bavette_125_Qte := FieldByName(UpperCase('SC_Bavette_125_Qte')).AsInteger;
                Bavette_160_Qte := FieldByName(UpperCase('SC_Bavette_160_Qte')).AsInteger;
                Bavette_200_Qte := FieldByName(UpperCase('SC_Bavette_200_Qte')).AsInteger;
                Bavette_250_Qte := FieldByName(UpperCase('SC_Bavette_250_Qte')).AsInteger;
                Bavette_315_Qte := FieldByName(UpperCase('SC_Bavette_315_Qte')).AsInteger;
                Bavette_355_Qte := FieldByName(UpperCase('SC_Bavette_355_Qte')).AsInteger;
                Bavette_400_Qte := FieldByName(UpperCase('SC_Bavette_400_Qte')).AsInteger;
                Bavette_450_Qte := FieldByName(UpperCase('SC_Bavette_450_Qte')).AsInteger;
                Bavette_500_Qte := FieldByName(UpperCase('SC_Bavette_500_Qte')).AsInteger;
                Bavette_560_Qte := FieldByName(UpperCase('SC_Bavette_560_Qte')).AsInteger;
                Bavette_630_Qte := FieldByName(UpperCase('SC_Bavette_630_Qte')).AsInteger;
                Bavette_710_Qte := FieldByName(UpperCase('SC_Bavette_710_Qte')).AsInteger;
                //-->Trappes de visite
                Trappe_125_Qte := FieldByName(UpperCase('SC_Trappe_125_Qte')).AsInteger;
                Trappe_160_Qte := FieldByName(UpperCase('SC_Trappe_160_Qte')).AsInteger;
                Trappe_200_Qte := FieldByName(UpperCase('SC_Trappe_200_Qte')).AsInteger;
                Trappe_250_Qte := FieldByName(UpperCase('SC_Trappe_250_Qte')).AsInteger;
                Trappe_315_Qte := FieldByName(UpperCase('SC_Trappe_315_Qte')).AsInteger;
                Trappe_355_Qte := FieldByName(UpperCase('SC_Trappe_355_Qte')).AsInteger;
                Trappe_400_Qte := FieldByName(UpperCase('SC_Trappe_400_Qte')).AsInteger;
                Trappe_450_Qte := FieldByName(UpperCase('SC_Trappe_450_Qte')).AsInteger;
                Trappe_500_Qte := FieldByName(UpperCase('SC_Trappe_500_Qte')).AsInteger;
                Trappe_560_Qte := FieldByName(UpperCase('SC_Trappe_560_Qte')).AsInteger;
                Trappe_630_Qte := FieldByName(UpperCase('SC_Trappe_630_Qte')).AsInteger;
                Trappe_710_Qte := FieldByName(UpperCase('SC_Trappe_710_Qte')).AsInteger;
                Trappe_800_Qte := FieldByName(UpperCase('SC_Trappe_800_Qte')).AsInteger;
                Trappe_900_Qte := FieldByName(UpperCase('SC_Trappe_900_Qte')).AsInteger;
                //-->Supports Universels
                CheckBox_Universel_Checked := Int2Bool(FieldByName(UpperCase('SC_CB_Universel_Checked')).AsInteger);
                CheckBox_125300_Checked := Int2Bool(FieldByName(UpperCase('SC_CB_125300_Checked')).AsInteger);
                CheckBox_160300_Checked := Int2Bool(FieldByName(UpperCase('SC_CB_160300_Checked')).AsInteger);
                CheckBox_125100_Checked := Int2Bool(FieldByName(UpperCase('SC_CB_125100_Checked')).AsInteger);
                CheckBox_160100_Checked := Int2Bool(FieldByName(UpperCase('SC_CB_160100_Checked')).AsInteger);
                CheckBox_ColliersSuppIsoles_Checked := Int2Bool(FieldByName(UpperCase('SC_CB_ColliersSI_Checked')).AsInteger);
                Edit_NbSupp160300_Text := FieldByName(UpperCase('SC_E_NbSupp160300_Text')).AsWideString;
                Edit_NbSupp125300_Text := FieldByName(UpperCase('SC_E_NbSupp125300_Text')).AsWideString;
                Edit_NbSupp160100_Text := FieldByName(UpperCase('SC_E_NbSupp160100_Text')).AsWideString;
                Edit_NbSupp125100_Text := FieldByName(UpperCase('SC_E_NbSupp125100_Text')).AsWideString;
                Edit_NbSuppUni_Text := FieldByName(UpperCase('SC_E_NbSuppUni_Text')).AsWideString;
                CheckBox_BandePerf_Checked := Int2Bool(FieldByName(UpperCase('SC_CB_BandePerf_Checked')).AsInteger);
                Edit_NbRlxBandePerforee_Text := FieldByName(UpperCase('SC_E_NbRlxBandePerforee_Text')).AsWideString;
                Collier_125_Qte := FieldByName(UpperCase('SC_Collier_125_Qte')).AsInteger;
                Collier_160_Qte := FieldByName(UpperCase('SC_Collier_160_Qte')).AsInteger;
                Collier_200_Qte := FieldByName(UpperCase('SC_Collier_200_Qte')).AsInteger;
                Collier_250_Qte := FieldByName(UpperCase('SC_Collier_250_Qte')).AsInteger;
                Collier_315_Qte := FieldByName(UpperCase('SC_Collier_315_Qte')).AsInteger;
                Collier_355_Qte := FieldByName(UpperCase('SC_Collier_355_Qte')).AsInteger;
                Collier_400_Qte := FieldByName(UpperCase('SC_Collier_400_Qte')).AsInteger;
                Collier_450_Qte := FieldByName(UpperCase('SC_Collier_450_Qte')).AsInteger;
                Collier_500_Qte := FieldByName(UpperCase('SC_Collier_500_Qte')).AsInteger;
                Collier_560_Qte := FieldByName(UpperCase('SC_Collier_560_Qte')).AsInteger;
                Collier_630_Qte := FieldByName(UpperCase('SC_Collier_630_Qte')).AsInteger;
                Collier_710_Qte := FieldByName(UpperCase('SC_Collier_710_Qte')).AsInteger;
                Collier_800_Qte := FieldByName(UpperCase('SC_Collier_800_Qte')).AsInteger;
                Collier_900_Qte := FieldByName(UpperCase('SC_Collier_900_Qte')).AsInteger;

                  if Etude.Batiment.IsFabricantACT then
                  begin
                ACT_PST         := Int2Bool(FieldByName(UpperCase('ACT_PST')).AsInteger);
                ACT_CSR         := Int2Bool(FieldByName(UpperCase('ACT_CSR')).AsInteger);
                ACT_CSR_Nb_Text      := FieldByName(UpperCase('ACT_CSR_Nb_Text')).AsWideString;
                ACT_CUT         := Int2Bool(FieldByName(UpperCase('ACT_CUT')).AsInteger);
                ACT_CUT_Nb_Text      := FieldByName(UpperCase('ACT_CUT_Nb_Text')).AsWideString;
                ACT_Trappes     := Int2Bool(FieldByName(UpperCase('ACT_Trappes')).AsInteger);
                ACT_ROA         := Int2Bool(FieldByName(UpperCase('ACT_ROA')).AsInteger);
                ACT_TeSouche    := FieldByName(UpperCase('ACT_TeSouche')).AsInteger;
                ACT_FT          := Int2Bool(FieldByName(UpperCase('ACT_FT')).AsInteger);
                ACT_JTD         := Int2Bool(FieldByName(UpperCase('ACT_JTD')).AsInteger);
                ACT_CUC         := Int2Bool(FieldByName(UpperCase('ACT_CUC')).AsInteger);
                ACT_CUC_Nb_Text      := FieldByName(UpperCase('ACT_CUC_Nb_Text')).AsWideString;
                ACT_Terminaux   := FieldByName(UpperCase('ACT_Terminaux')).AsInteger;
                ACT_LgT2A       := FieldByName(UpperCase('ACT_LgT2A')).AsInteger;
                  end;
                End;

End;
{ **************************************************************************************************************************************************** }
Procedure TSelectionCaisson_SaveOptions.InitOptionsFiche(_Fiche : TDlg_SelectionCaisson);
Begin

        if _Fiche = nil then
                exit;

        if CanGet = false then
                exit;

InitOptionsCriteresSelectionFiche(_Fiche);
InitOptionsAccessoiresAssociesFiche(_Fiche);
InitOptionsAccessoiresAnnexesFiche(_Fiche);

End;
{ **************************************************************************************************************************************************** }
Procedure TSelectionCaisson_SaveOptions.InitOptionsCriteresSelectionFiche(_Fiche : TDlg_SelectionCaisson);
Begin

        with _Fiche do
                begin
                //Crit�res De S�lection Du Ventilateur
                       if CheckBox_Caisson.Enabled And CheckBox_Caisson.Visible then
                                CheckBox_Caisson.Checked            := CheckBox_Caisson_Checked;
                       if CheckBox_Tourelle.Enabled And CheckBox_Tourelle.Visible then
                                CheckBox_Tourelle.Checked           := CheckBox_Tourelle_Checked;
                       if CheckBox_Direct.Enabled And CheckBox_Direct.Visible then
                                CheckBox_Direct.Checked             := CheckBox_Direct_Checked;
                       if CheckBox_Poulie.Enabled And CheckBox_Poulie.Visible then
                                CheckBox_Poulie.Checked             := CheckBox_Poulie_Checked;
                       if CheckBox_UneVitesse.Enabled And CheckBox_UneVitesse.Visible then
                                CheckBox_UneVitesse.Checked         := CheckBox_UneVitesse_Checked;
                       if CheckBoxDeuxVitesses.Enabled And CheckBoxDeuxVitesses.Visible then
                                CheckBoxDeuxVitesses.Checked        := CheckBoxDeuxVitesses_Checked;
                       if CheckBox_NonAgree.Enabled And CheckBox_NonAgree.Visible then
                                CheckBox_NonAgree.Checked           := CheckBox_NonAgree_Checked;
                       if CheckBox_40012.Enabled And CheckBox_40012.Visible then
                                CheckBox_40012.Checked              := CheckBox_40012_Checked;
                       if CheckBox_MonoPhase.Enabled And CheckBox_MonoPhase.Visible then
                                CheckBox_MonoPhase.Checked          := CheckBox_Monophase_Checked;
                       if CheckBox_TriPhase.Enabled And CheckBox_TriPhase.Visible then
                                CheckBox_TriPhase.Checked           := CheckBox_Triphase_Checked;
{
                       if RadioGroupDepressostat.Enabled And RadioGroupDepressostat.Visible then
                                RadioGroupDepressostat.ItemIndex       := Radio_Depressostat_ItemIndex;
                       if RadioGroupVentilEco.Enabled And RadioGroupVentilEco.Visible then
                                RadioGroupVentilEco.ItemIndex       := Radio_VentilEco_ItemIndex;
}
{
                       if RadioGroupVentilEcoEtDepressostat.Enabled And RadioGroupVentilEcoEtDepressostat.Visible then
                                RadioGroupVentilEcoEtDepressostat.ItemIndex       := Radio_EcoEtDepr_ItemIndex;
}

                       if CheckBoxEco.Enabled And CheckBoxEco.Visible then
                                CheckBoxEco.Checked           := CheckBoxEco_Checked;
                       if CheckBoxNEcoADepr.Enabled And CheckBoxNEcoADepr.Visible then
                                CheckBoxNEcoADepr.Checked           := CheckBoxNEcoADepr_Checked;
                       if CheckBoxNEcoSDepr.Enabled And CheckBoxNEcoSDepr.Visible then
                                CheckBoxNEcoSDepr.Checked           := CheckBoxNEcoSDepr_Checked;


                       if CheckBox_DTU.Enabled And CheckBox_DTU.Visible then
                                CheckBox_DTU.Checked                := CheckBox_DTU_Checked;
                       if RadioBezier.Enabled And RadioBezier.Visible then
                                RadioBezier.ItemIndex               := RadioBezier_ItemIndex;
                End;

End;
{ **************************************************************************************************************************************************** }
Procedure TSelectionCaisson_SaveOptions.InitOptionsAccessoiresAssociesFiche(_Fiche : TDlg_SelectionCaisson);
var
        No_Element : Integer;
Begin

        if CanGet = false then
                exit;
                
        if _Fiche.CaissonSelection = nil then
                exit;


        if _Fiche.CaissonSelection.CodeProduit <> OldReferenceCaisson then
                exit;

        With _Fiche do
                Begin
                //Accessoire Associ�s Au Ventilateur
                Grid_Accessoires.SortByColumn(0);
                        for No_Element := 1 to Length(ListAccessoireVentilo_Qte) do
                                Begin
                                Grid_Accessoires.Ints[2, No_Element] := ListAccessoireVentilo_Qte[No_Element - 1];
                                End;
                end;    // with

End;
{ **************************************************************************************************************************************************** }
Procedure TSelectionCaisson_SaveOptions.InitOptionsAccessoiresAnnexesFiche(_Fiche : TDlg_SelectionCaisson);
Begin

        with  _Fiche do
                begin
                //Accessoires Annexes
                //-->Montage
                        if Radio_Adhesif.Enabled And Radio_Adhesif.Visible then
                                Radio_Adhesif.ItemIndex             := Radio_Adhesif_ItemIndex;
                        if Edit_VIS13.Enabled And Edit_VIS13.Visible then
                                Edit_VIS13.Text                     := Edit_VIS13_Text;
                        if Edit_Vis19.Enabled And Edit_Vis19.Visible then
                                Edit_Vis19.Text                     := Edit_VIS19_Text;
                        if Edit_M0.Enabled And Edit_M0.Visible then
                                Edit_M0.Text                        := Edit_M0_Text;
                        if Edit_M1.Enabled And Edit_M1.Visible then
                                Edit_M1.Text                        := Edit_M1_Text;
                        if Check_IsolantMur.Enabled And Check_IsolantMur.Visible then
                                Check_IsolantMur.Checked            := Check_IsolantMur_Checked;
                        if Check_Fourreaux.Enabled And Check_Fourreaux.Visible then
                                Check_Fourreaux.Checked             := Check_Fourreaux_Checked;
                //-->Bavettes et colliers
                        if Radio_Colliers.Enabled And Radio_Colliers.Visible then
                                Radio_Colliers.ItemIndex            := Radio_Colliers_ItemIndex;
                        CheckBoxCompteBavettes.Checked := Bavette_OuiNon;
                        if Grid_Bavettes.Enabled and Grid_Bavettes.Visible then
                                Begin
                                Grid_Bavettes.Ints[1, 1] := Bavette_125_Qte;
                                Grid_Bavettes.Ints[1, 2] := Bavette_160_Qte;
                                Grid_Bavettes.Ints[1, 3] := Bavette_200_Qte;
                                Grid_Bavettes.Ints[3, 1] := Bavette_250_Qte;
                                Grid_Bavettes.Ints[3, 2] := Bavette_315_Qte;
                                Grid_Bavettes.Ints[3, 3] := Bavette_355_Qte;
                                Grid_Bavettes.Ints[5, 1] := Bavette_400_Qte;
                                Grid_Bavettes.Ints[5, 2] := Bavette_450_Qte;
                                Grid_Bavettes.Ints[5, 3] := Bavette_500_Qte;
                                Grid_Bavettes.Ints[7, 1] := Bavette_560_Qte;
                                Grid_Bavettes.Ints[7, 2] := Bavette_630_Qte;
                                Grid_Bavettes.Ints[7, 3] := Bavette_710_Qte;
                                End;

                //-->Trappes de visite
                Grid_TrappesVisite.Ints[1, 1] := Trappe_125_Qte;
                Grid_TrappesVisite.Ints[1, 2] := Trappe_160_Qte;
                Grid_TrappesVisite.Ints[1, 3] := Trappe_200_Qte;
                Grid_TrappesVisite.Ints[1, 4] := Trappe_250_Qte;
                Grid_TrappesVisite.Ints[3, 1] := Trappe_315_Qte;
                Grid_TrappesVisite.Ints[3, 2] := Trappe_355_Qte;
                Grid_TrappesVisite.Ints[3, 3] := Trappe_400_Qte;
                Grid_TrappesVisite.Ints[3, 4] := Trappe_450_Qte;
                Grid_TrappesVisite.Ints[5, 1] := Trappe_500_Qte;
                Grid_TrappesVisite.Ints[5, 2] := Trappe_560_Qte;
                Grid_TrappesVisite.Ints[5, 3] := Trappe_630_Qte;
                Grid_TrappesVisite.Ints[5, 4] := Trappe_710_Qte;
                Grid_TrappesVisite.Ints[7, 1] := Trappe_800_Qte;
                Grid_TrappesVisite.Ints[7, 2] := Trappe_900_Qte;


                //-->Supports Universels
                        if CheckBox_Universel.Enabled And CheckBox_Universel.Visible then
                                CheckBox_Universel.Checked          := CheckBox_Universel_Checked;
                        if CheckBox_ColliersSuppIsoles.Enabled And CheckBox_ColliersSuppIsoles.Visible then
                                CheckBox_ColliersSuppIsoles.Checked := CheckBox_ColliersSuppIsoles_Checked;
                        if Edit_NbSuppUni.Enabled And Edit_NbSuppUni.Visible then
                                Edit_NbSuppUni.Text                 := Edit_NbSuppUni_Text;
                        if CheckBox_BandePerf.Enabled And CheckBox_BandePerf.Visible then
                                CheckBox_BandePerf.Checked          := CheckBox_BandePerf_Checked;
                        if Edit_NbRlxBandePerforee.Enabled And Edit_NbRlxBandePerforee.Visible then
                                Edit_NbRlxBandePerforee.Text        := Edit_NbRlxBandePerforee_Text;

//pour r�initialiser le comptage auto des colliers, on ne remet pas les derniers param�tres saisies
//fait suite � une demande d'�ric duverger concernant les r�gles de calcul                                
{
                        if Grid_ColliersSupp.Enabled and Grid_ColliersSupp.Visible then
                                Begin
                                Grid_ColliersSupp.Ints[1, 1] := Collier_125_Qte;
                                Grid_ColliersSupp.Ints[1, 2] := Collier_160_Qte;
                                Grid_ColliersSupp.Ints[1, 3] := Collier_200_Qte;
                                Grid_ColliersSupp.Ints[1, 4] := Collier_250_Qte;
                                Grid_ColliersSupp.Ints[3, 1] := Collier_315_Qte;
                                Grid_ColliersSupp.Ints[3, 2] := Collier_355_Qte;
                                Grid_ColliersSupp.Ints[3, 3] := Collier_400_Qte;
                                Grid_ColliersSupp.Ints[3, 4] := Collier_450_Qte;
                                Grid_ColliersSupp.Ints[5, 1] := Collier_500_Qte;
                                Grid_ColliersSupp.Ints[5, 2] := Collier_560_Qte;
                                Grid_ColliersSupp.Ints[5, 3] := Collier_630_Qte;
                                Grid_ColliersSupp.Ints[5, 4] := Collier_710_Qte;
                                Grid_ColliersSupp.Ints[7, 1] := Collier_800_Qte;
                                Grid_ColliersSupp.Ints[7, 2] := Collier_900_Qte;
                                End;
}
                CheckBoxACT_PST.Checked := ACT_PST;
                CheckBoxACT_CablesSuspensionRapide.Checked := ACT_CSR;
                AdvEditACT_CablesSuspensionRapide.Text := ACT_CSR_Nb_Text;
                CheckBoxACT_CollierUniverselTerrasse.Checked := ACT_CUT;
                AdvEditACT_CollierUniverselTerrasse.Text := ACT_CUT_Nb_Text;
                CheckBoxACT_TrappesDeVisite.Checked := ACT_Trappes;
                CheckBoxACT_BandeAdhesive.Checked := ACT_ROA;
                RadioGroupACT_TeSouche.ItemIndex := ACT_TeSouche;
                CheckBoxACT_FourreauDeTraversee.Checked := ACT_FT;
                CheckBoxACT_Joint.Checked := ACT_JTD;
                CheckBoxACT_CollierUniverselColonne.Checked := ACT_CUC;
                AdvEditACT_CollierUniverselColonne.Text := ACT_CUC_Nb_Text;
                RadioGroupACT_Terminaux.ItemIndex := ACT_Terminaux;
                RadioGroupACT_LongueurBarreT2A.ItemIndex := ACT_LgT2A;

                end;    // with

End;
{ **************************************************************************************************************************************************** }
Procedure TSelectionCaisson_SaveOptions.SaveOptionsFiche(_Fiche : TDlg_SelectionCaisson);
Begin

        if _Fiche = nil then
                exit;

//OldReferenceCaisson := _Fiche.TVentil_Caisson(TVentil_Caisson(CaissonReseau)).ProduitAssocie.CodeProduit;
OldReferenceCaisson := _Fiche.CaissonSelection.CodeProduit;
SaveOptionsCriteresSelectionFiche(_Fiche);
SaveOptionsAccessoiresAssociesFiche(_Fiche);
SaveOptionsAccessoiresAnnexesFiche(_Fiche);

CanGet := True;

End;
{ **************************************************************************************************************************************************** }
Procedure TSelectionCaisson_SaveOptions.Copier(_Source : TSelectionCaisson_SaveOptions);
begin
    CanGet                                         := _Source.CanGet;
    OldReferenceCaisson                            := _Source.OldReferenceCaisson;

    CheckBox_Caisson_Checked                       := _Source.CheckBox_Caisson_Checked;
    CheckBox_Tourelle_Checked                      := _Source.CheckBox_Tourelle_Checked;
    CheckBox_Direct_Checked                        := _Source.CheckBox_Direct_Checked;
    CheckBox_Poulie_Checked                        := _Source.CheckBox_Poulie_Checked;
    CheckBox_UneVitesse_Checked                    := _Source.CheckBox_UneVitesse_Checked;
    CheckBoxDeuxVitesses_Checked                   := _Source.CheckBoxDeuxVitesses_Checked;
    CheckBox_NonAgree_Checked                      := _Source.CheckBox_NonAgree_Checked;
    CheckBox_40012_Checked                         := _Source.CheckBox_40012_Checked;
    CheckBox_MonoPhase_Checked                     := _Source.CheckBox_MonoPhase_Checked;
    CheckBox_TriPhase_Checked                      := _Source.CheckBox_TriPhase_Checked;

    CheckBoxEco_Checked                            := _Source.CheckBoxEco_Checked;
    CheckBoxNEcoADepr_Checked                      := _Source.CheckBoxNEcoADepr_Checked;
    CheckBoxNEcoSDepr_Checked                      := _Source.CheckBoxNEcoSDepr_Checked;

    CheckBox_DTU_Checked                           := _Source.CheckBox_DTU_Checked;
    RadioBezier_ItemIndex                          := _Source.RadioBezier_ItemIndex;

    ListAccessoireVentilo_Qte                      := _Source.ListAccessoireVentilo_Qte;

    Radio_Adhesif_ItemIndex                        := _Source.Radio_Adhesif_ItemIndex;
    Edit_Vis13_Text                                := _Source.Edit_Vis13_Text;
    Edit_Vis19_Text                                := _Source.Edit_Vis19_Text;
    Edit_M0_Text                                   := _Source.Edit_M0_Text;
    Edit_M1_Text                                   := _Source.Edit_M1_Text;
    Check_IsolantMur_Checked                       := _Source.Check_IsolantMur_Checked;
    Check_Fourreaux_Checked                        := _Source.Check_Fourreaux_Checked;

    Radio_Colliers_ItemIndex                       := _Source.Radio_Colliers_ItemIndex;
    Bavette_OuiNon                                 := _Source.Bavette_OuiNon;
    Bavette_125_Qte                                := _Source.Bavette_125_Qte;
    Bavette_160_Qte                                := _Source.Bavette_160_Qte;
    Bavette_200_Qte                                := _Source.Bavette_200_Qte;
    Bavette_250_Qte                                := _Source.Bavette_250_Qte;
    Bavette_315_Qte                                := _Source.Bavette_315_Qte;
    Bavette_355_Qte                                := _Source.Bavette_355_Qte;
    Bavette_400_Qte                                := _Source.Bavette_400_Qte;
    Bavette_450_Qte                                := _Source.Bavette_450_Qte;
    Bavette_500_Qte                                := _Source.Bavette_500_Qte;
    Bavette_560_Qte                                := _Source.Bavette_560_Qte;
    Bavette_630_Qte                                := _Source.Bavette_630_Qte;
    Bavette_710_Qte                                := _Source.Bavette_710_Qte;

    Trappe_125_Qte                                 := _Source.Trappe_125_Qte;
    Trappe_160_Qte                                 := _Source.Trappe_160_Qte;
    Trappe_200_Qte                                 := _Source.Trappe_200_Qte;
    Trappe_250_Qte                                 := _Source.Trappe_250_Qte;
    Trappe_315_Qte                                 := _Source.Trappe_315_Qte;
    Trappe_355_Qte                                 := _Source.Trappe_355_Qte;
    Trappe_400_Qte                                 := _Source.Trappe_400_Qte;
    Trappe_450_Qte                                 := _Source.Trappe_450_Qte;
    Trappe_500_Qte                                 := _Source.Trappe_500_Qte;
    Trappe_560_Qte                                 := _Source.Trappe_560_Qte;
    Trappe_630_Qte                                 := _Source.Trappe_630_Qte;
    Trappe_710_Qte                                 := _Source.Trappe_710_Qte;
    Trappe_800_Qte                                 := _Source.Trappe_800_Qte;
    Trappe_900_Qte                                 := _Source.Trappe_900_Qte;


    CheckBox_Universel_Checked                     := _Source.CheckBox_Universel_Checked;
    CheckBox_125300_Checked                        := _Source.CheckBox_125300_Checked;
    CheckBox_160300_Checked                        := _Source.CheckBox_160300_Checked;
    CheckBox_125100_Checked                        := _Source.CheckBox_125100_Checked;
    CheckBox_160100_Checked                        := _Source.CheckBox_160100_Checked;
    CheckBox_ColliersSuppIsoles_Checked            := _Source.CheckBox_ColliersSuppIsoles_Checked;
    Edit_NbSupp160300_Text                         := _Source.Edit_NbSupp160300_Text;
    Edit_NbSupp125300_Text                         := _Source.Edit_NbSupp125300_Text;
    Edit_NbSupp160100_Text                         := _Source.Edit_NbSupp160100_Text;
    Edit_NbSupp125100_Text                         := _Source.Edit_NbSupp125100_Text;
    Edit_NbSuppUni_Text                            := _Source.Edit_NbSuppUni_Text;
    CheckBox_BandePerf_Checked                     := _Source.CheckBox_BandePerf_Checked;
    Edit_NbRlxBandePerforee_Text                   := _Source.Edit_NbRlxBandePerforee_Text;
    Collier_125_Qte                                := _Source.Collier_125_Qte;
    Collier_160_Qte                                := _Source.Collier_160_Qte;
    Collier_200_Qte                                := _Source.Collier_200_Qte;
    Collier_250_Qte                                := _Source.Collier_250_Qte;
    Collier_315_Qte                                := _Source.Collier_315_Qte;
    Collier_355_Qte                                := _Source.Collier_355_Qte;
    Collier_400_Qte                                := _Source.Collier_400_Qte;
    Collier_450_Qte                                := _Source.Collier_450_Qte;
    Collier_500_Qte                                := _Source.Collier_500_Qte;
    Collier_560_Qte                                := _Source.Collier_560_Qte;
    Collier_630_Qte                                := _Source.Collier_630_Qte;
    Collier_710_Qte                                := _Source.Collier_710_Qte;
    Collier_800_Qte                                := _Source.Collier_800_Qte;
    Collier_900_Qte                                := _Source.Collier_900_Qte;

    ACT_PST         := _Source.ACT_PST;
    ACT_CSR         := _Source.ACT_CSR;
    ACT_CSR_Nb_Text      := _Source.ACT_CSR_Nb_Text;
    ACT_CUT         := _Source.ACT_CUT;
    ACT_CUT_Nb_Text      := _Source.ACT_CUT_Nb_Text;
    ACT_Trappes     := _Source.ACT_Trappes;
    ACT_ROA         := _Source.ACT_ROA;
    ACT_TeSouche    := _Source.ACT_TeSouche;
    ACT_FT          := _Source.ACT_FT;
    ACT_JTD         := _Source.ACT_JTD;
    ACT_CUC         := _Source.ACT_CUC;
    ACT_CUC_Nb_Text      := _Source.ACT_CUC_Nb_Text;
    ACT_Terminaux   := _Source.ACT_Terminaux;
    ACT_LgT2A       := _Source.ACT_LgT2A;
end;
{ **************************************************************************************************************************************************** }
Procedure TSelectionCaisson_SaveOptions.SaveOptionsCriteresSelectionFiche(_Fiche : TDlg_SelectionCaisson);
Begin

        with _Fiche do
                begin
                //Crit�res De S�lection Du Ventilateur
                CheckBox_Caisson_Checked            := CheckBox_Caisson.Checked;
                CheckBox_Tourelle_Checked           := CheckBox_Tourelle.Checked;
                CheckBox_Direct_Checked             := CheckBox_Direct.Checked;
                CheckBox_Poulie_Checked             := CheckBox_Poulie.Checked;
                CheckBox_UneVitesse_Checked         := CheckBox_UneVitesse.Checked;
                CheckBoxDeuxVitesses_Checked        := CheckBoxDeuxVitesses.Checked;
                CheckBox_NonAgree_Checked           := CheckBox_NonAgree.Checked;
                CheckBox_40012_Checked              := CheckBox_40012.Checked;
                CheckBox_MonoPhase_Checked          := CheckBox_Monophase.Checked;
                CheckBox_TriPhase_Checked           := CheckBox_Triphase.Checked;
//                Radio_Depressostat_ItemIndex        := RadioGroupDepressostat.ItemIndex;
//                Radio_VentilEco_ItemIndex           := RadioGroupVentilEco.ItemIndex;
//                Radio_EcoEtDepr_ItemIndex           := RadioGroupVentilEcoEtDepressostat.ItemIndex;
                CheckBoxEco_Checked                 := CheckBoxEco.Checked;
                CheckBoxNEcoADepr_Checked           := CheckBoxNEcoADepr.Checked;
                CheckBoxNEcoSDepr_Checked           := CheckBoxNEcoSDepr.Checked;
                CheckBox_DTU_Checked                := CheckBox_DTU.Checked;
                RadioBezier_ItemIndex               := RadioBezier.ItemIndex;
                End;

End;
{ **************************************************************************************************************************************************** }
Procedure TSelectionCaisson_SaveOptions.SaveOptionsAccessoiresAssociesFiche(_Fiche : TDlg_SelectionCaisson);
var
        No_Element : Integer;
Begin

        With _Fiche do
                Begin
                //Accessoire Associ�s Au Ventilateur

                Grid_Accessoires.SortByColumn(0);

                SetLength(ListAccessoireVentilo_Qte, Grid_Accessoires.RowCount - 1);
                        for No_Element := 1 to Grid_Accessoires.RowCount - 1 do
                                ListAccessoireVentilo_Qte[No_Element - 1] := Grid_Accessoires.Ints[2, No_Element];
                end;    // with

End;
{ **************************************************************************************************************************************************** }
Procedure TSelectionCaisson_SaveOptions.SaveOptionsAccessoiresAnnexesFiche(_Fiche : TDlg_SelectionCaisson);
Begin

        With _Fiche do
                Begin
                //Accessoires Annexes
                //-->Montage
                Radio_Adhesif_ItemIndex             := Radio_Adhesif.ItemIndex;
                Edit_Vis13_Text                     := Edit_VIS13.Text;
                Edit_Vis19_Text                     := Edit_VIS19.Text;
                Edit_M0_Text                        := Edit_M0.Text;
                Edit_M1_Text                        := Edit_M1.Text;
                Check_IsolantMur_Checked            := Check_IsolantMur.Checked;
                Check_Fourreaux_Checked             := Check_Fourreaux.Checked;
                //-->Bavettes et colliers
                Radio_Colliers_ItemIndex            := Radio_Colliers.ItemIndex;
                Bavette_OuiNon                      := CheckBoxCompteBavettes.Checked;
                Bavette_125_Qte                     := Grid_Bavettes.Ints[1, 1];
                Bavette_160_Qte                     := Grid_Bavettes.Ints[1, 2];
                Bavette_200_Qte                     := Grid_Bavettes.Ints[1, 3];
                Bavette_250_Qte                     := Grid_Bavettes.Ints[3, 1];
                Bavette_315_Qte                     := Grid_Bavettes.Ints[3, 2];
                Bavette_355_Qte                     := Grid_Bavettes.Ints[3, 3];
                Bavette_400_Qte                     := Grid_Bavettes.Ints[5, 1];
                Bavette_450_Qte                     := Grid_Bavettes.Ints[5, 2];
                Bavette_500_Qte                     := Grid_Bavettes.Ints[5, 3];
                Bavette_560_Qte                     := Grid_Bavettes.Ints[7, 1];
                Bavette_630_Qte                     := Grid_Bavettes.Ints[7, 2];
                Bavette_710_Qte                     := Grid_Bavettes.Ints[7, 3];
                //-->Trappes de visite
                Trappe_125_Qte                      := Grid_TrappesVisite.Ints[1, 1];
                Trappe_160_Qte                      := Grid_TrappesVisite.Ints[1, 2];
                Trappe_200_Qte                      := Grid_TrappesVisite.Ints[1, 3];
                Trappe_250_Qte                      := Grid_TrappesVisite.Ints[1, 4];
                Trappe_315_Qte                      := Grid_TrappesVisite.Ints[3, 1];
                Trappe_355_Qte                      := Grid_TrappesVisite.Ints[3, 2];
                Trappe_400_Qte                      := Grid_TrappesVisite.Ints[3, 3];
                Trappe_450_Qte                      := Grid_TrappesVisite.Ints[3, 4];
                Trappe_500_Qte                      := Grid_TrappesVisite.Ints[5, 1];
                Trappe_560_Qte                      := Grid_TrappesVisite.Ints[5, 2];
                Trappe_630_Qte                      := Grid_TrappesVisite.Ints[5, 3];
                Trappe_710_Qte                      := Grid_TrappesVisite.Ints[5, 4];
                Trappe_800_Qte                      := Grid_TrappesVisite.Ints[7, 1];
                Trappe_900_Qte                      := Grid_TrappesVisite.Ints[7, 2];
                //-->Supports Universels
                CheckBox_Universel_Checked          := CheckBox_Universel.Checked;
                CheckBox_ColliersSuppIsoles_Checked := CheckBox_ColliersSuppIsoles.Checked;
                Edit_NbSuppUni_Text                 := Edit_NbSuppUni.Text;
                CheckBox_BandePerf_Checked          := CheckBox_BandePerf.Checked;
                Edit_NbRlxBandePerforee_Text        := Edit_NbRlxBandePerforee.Text;

                Collier_125_Qte := Grid_ColliersSupp.Ints[1, 1];
                Collier_160_Qte := Grid_ColliersSupp.Ints[1, 2];
                Collier_200_Qte := Grid_ColliersSupp.Ints[1, 3];
                Collier_250_Qte := Grid_ColliersSupp.Ints[1, 4];
                Collier_315_Qte := Grid_ColliersSupp.Ints[3, 1];
                Collier_355_Qte := Grid_ColliersSupp.Ints[3, 2];
                Collier_400_Qte := Grid_ColliersSupp.Ints[3, 3];
                Collier_450_Qte := Grid_ColliersSupp.Ints[3, 4];
                Collier_500_Qte := Grid_ColliersSupp.Ints[5, 1];
                Collier_560_Qte := Grid_ColliersSupp.Ints[5, 2];
                Collier_630_Qte := Grid_ColliersSupp.Ints[5, 3];
                Collier_710_Qte := Grid_ColliersSupp.Ints[5, 4];
                Collier_800_Qte := Grid_ColliersSupp.Ints[7, 1];
                Collier_900_Qte := Grid_ColliersSupp.Ints[7, 2];

                ACT_PST         := CheckBoxACT_PST.Checked;
                ACT_CSR         := CheckBoxACT_CablesSuspensionRapide.Checked;
                ACT_CSR_Nb_Text      := AdvEditACT_CablesSuspensionRapide.Text;
                ACT_CUT         := CheckBoxACT_CollierUniverselTerrasse.Checked;
                ACT_CUT_Nb_Text      := AdvEditACT_CollierUniverselTerrasse.Text;
                ACT_Trappes     := CheckBoxACT_TrappesDeVisite.Checked;
                ACT_ROA         := CheckBoxACT_BandeAdhesive.Checked;
                ACT_TeSouche    := RadioGroupACT_TeSouche.ItemIndex;
                ACT_FT          := CheckBoxACT_FourreauDeTraversee.Checked;
                ACT_JTD         := CheckBoxACT_Joint.Checked;
                ACT_CUC         := CheckBoxACT_CollierUniverselColonne.Checked;
                ACT_CUC_Nb_Text      := AdvEditACT_CollierUniverselColonne.Text;
                ACT_Terminaux   := RadioGroupACT_Terminaux.ItemIndex;
                ACT_LgT2A       := RadioGroupACT_LongueurBarreT2A.ItemIndex;

                end;    // with
End;
{ **************************************************************************************************************************************************** }
procedure TDlg_SelectionCaisson.CheckBoxACT_CollierUniverselTerrasseClick(
  Sender: TObject);
begin
  CheckBoxACT_PST.Enabled := CheckBoxACT_CollierUniverselTerrasse.Checked;
end;
{ **************************************************************************************************************************************************** }
procedure TDlg_SelectionCaisson.CheckBoxCompteBavettesClick(
  Sender: TObject);
begin
Grid_Bavettes.Visible := CheckBoxCompteBavettes.Checked;
end;
{ **************************************************************************************************************************************************** }
procedure TDlg_SelectionCaisson.Btn_RecalculAccesoiresClick(
  Sender: TObject);
begin
InitChoix_Chiffrage;
end;
{ **************************************************************************************************************************************************** }
end.
